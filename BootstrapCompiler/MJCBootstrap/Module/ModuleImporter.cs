﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.General;

namespace MJC.Module
{
	class ModuleImporter
	{
		public static Module Import(string file)
		{
			byte[] data = File.ReadAllBytes(file);
			ByteBuffer buffer = new ByteBuffer(data);
			Module module = new Module(buffer);

			return module;
		}

		public static Module Import(byte[] data)
		{
			ByteBuffer buffer = new ByteBuffer(data);
			Module module = new Module(buffer);

			return module;
		}

		public static Module Import(ByteBuffer buffer)
		{
			Module module = new Module(buffer);

			return module;
		}

		public static Dictionary<string, Module> ImportModulesAndRequiredImports(string moduleName, Dictionary<string, Module> loadedModules)
		{
			Dictionary<string, Module> modules = new Dictionary<string, Module>();
			Stack<string> RequiredModules = new Stack<string>();

			List<string> dirs = new List<string>{ CmdLine.OutDirectory };
			dirs.AddRange(CmdLine.IncludeDirs);

			Dictionary<string, Module> availableModules = new Dictionary<string, Module>();
			foreach (string dir in dirs)
			{
				foreach (string fileName in Directory.EnumerateFiles(dir, "*.mjm", SearchOption.AllDirectories))
				{
					ByteBuffer buffer = new ByteBuffer(File.ReadAllBytes(fileName));
					Module mod = new Module(buffer);

					availableModules.Add(mod.ModuleName, mod);
				}
			}

			RequiredModules.Push(moduleName);
			while (RequiredModules.Count != 0)
			{
				string modName = RequiredModules.Pop();

				if (!modules.ContainsKey(modName))
				{
					if (loadedModules.TryGetValue(modName, out Module loadedMod))
					{
						modules.Add(modName, loadedMod);
						foreach (string reqMod in loadedMod.RequiredModules)
						{
							if (!modules.ContainsKey(reqMod))
								RequiredModules.Push(reqMod);
						}
						continue;
					}

					if (availableModules.TryGetValue(modName, out Module availableMod))
					{
						modules.Add(modName, availableMod);
						foreach (string reqMod in availableMod.RequiredModules)
						{
							if (!modules.ContainsKey(reqMod))
								RequiredModules.Push(reqMod);
						}
						continue;
					}

					ErrorSystem.Error($"Failed to find module '{modName}'");
				}
			}




			return modules;
		}

	}
}
