﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MJC.General;
using MJC.ILBackend;
using MJC.ILBackend.ByteCode;
using MJC.IRBackend;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC.Module
{
	public enum LTOType
	{
		Obj,
		IR,
		IL,
	}

	class ModuleGenerator
	{
		public static Module Generate(List<SyntaxTree.SyntaxTree> trees, List<string> objFiles, string packageName, string moduleName, bool isDynamic, LTOType lto)
		{
			Module module = new Module();
			module.Header.Magic = ModuleHeader.CorrectMagic;
			module.Header.Package = packageName;
			module.Header.Module = moduleName;

			List<Identifier> baseScopeNames = new List<Identifier>();

			if (!string.IsNullOrWhiteSpace(packageName))
			{
				string[] packageParts = packageName.Split('.');
				foreach (string part in packageParts)
					baseScopeNames.Add(new NameIdentifier(part));
			}

			baseScopeNames.Add(new NameIdentifier(moduleName));

			List<Symbol> symbols = new List<Symbol>();
			List<string> aliases = new List<string>();

			SymbolTable symbolTable = trees[0].Symbols;

			SymbolTable baseTable = symbolTable.FindTable(new Scope(baseScopeNames));

			// Add entries to the module
			Stack<SymbolTable> tableStack = new Stack<SymbolTable>();
			tableStack.Push(baseTable);

			while (tableStack.Count > 0)
			{
				SymbolTable tmp = tableStack.Pop();

				foreach (KeyValuePair<Identifier, List<Symbol>> pair in tmp.Symbols)
				{
					foreach (Symbol symbol in pair.Value)
					{
						symbols.Add(symbol);
					}

					/*foreach (Symbol symbol in pair.Value)
					{
						...
						else if (symbol.CompileAttribs.ContainsKey(ForeignCompileAttribute.Id))
						{
							ForeignCompileAttribute compileAttribute = symbol.CompileAttribs[ForeignCompileAttribute.Id] as ForeignCompileAttribute;
							if (compileAttribute?.Library != null)
							{
								entry.FileName = compileAttribute.Library;
								importEntries.Add(entry);
							}

						}
					}*/
				}

				foreach (KeyValuePair<Identifier, SymbolTable> pair in tmp.SubTables)
				{
					tableStack.Push(pair.Value);
				}
			}

			// Collect modules for IL bytecode
			module.ILCompUnit = new ILCompUnit();
			foreach (SyntaxTree.SyntaxTree tree in trees)
			{
				ILCompUnit compUnit = tree.CompUnit;
				aliases.AddRange(compUnit.Aliases);
				module.ILCompUnit.MergeInto(tree.CompUnit);
			}

			// Create table entries and add them to the correct table
			Dictionary<string, List<TableEntry>> entries = CreateSymbolTables(symbols, lto, module);

			foreach (KeyValuePair<string, List<TableEntry>> entry in entries)
			{
				Table table = new Table();
				table.Iden = entry.Key;
				table.Entries = entry.Value;

				module.Tables.Add(table.Iden, table);
			}

			CreateAliasTable(aliases, module);

			module.Header.NumSymbolTables = (short)module.Tables.Count;

			// Since the bootstrap compiler will only interpret the IL, we have no .o files to output and will therefore not add any .o files  the module
			/*HashSet<string> addedFiles = new HashSet<string>();

			// Add the required object files to the module
			if (entries.TryGetValue("pub", out List<TableEntry> publicEntries))
			{
				foreach (SymbolEntry entry in publicEntries)
				{
					if ((entry.Info.Flags & SymbolInfoFlags.Extern) == 0 &&
					    entry.FileName.EndsWith(".o") &&
						!addedFiles.Contains(entry.FileName))
					{
						string objPath = "";
						if (!string.IsNullOrEmpty(CmdLine.IntermediateDirectory))
							objPath = CmdLine.IntermediateDirectory;

						byte[] data = File.ReadAllBytes(objPath + entry.FileName);
						ObjectFile file = new ObjectFile();
						file.FileName = entry.FileName;
						file.Data = data;
						module.ObjectFiles.Add(file);
						addedFiles.Add(entry.FileName);
					}
				}

			}

			if (entries.TryGetValue("glo", out List<TableEntry> globalEntries))
			{
				foreach (SymbolEntry entry in globalEntries)
				{
					if (entry.FileName.EndsWith(".o") && !addedFiles.Contains(entry.FileName))
					{
						string objPath = "";
						if (!string.IsNullOrEmpty(CmdLine.IntermediateDirectory))
							objPath = CmdLine.IntermediateDirectory;

						byte[] data = File.ReadAllBytes(objPath + entry.FileName);
						ObjectFile file = new ObjectFile();
						file.FileName = entry.FileName;
						file.Data = data;
						module.ObjectFiles.Add(file);
						addedFiles.Add(entry.FileName);
					}
				}
			}

			module.Header.NumObjectFiles = (short) module.ObjectFiles.Count;*/

			// Update IL bytecode module count
			//if (module.ILCompUnit.TemplateFunctions.Count != 0)
			// Always add the code as IL, to be able to interpret
			{
				ILCompUnit compUnit = module.ILCompUnit;

				string bytecodeFile = compUnit.ILModule.Split('.').Last() + ".mjilb";

				string actPath = CmdLine.IntermediateDirectory + Helpers.GetModuleFilePath(compUnit.ILModule);
				actPath += Path.GetFileName(bytecodeFile);

				compUnit.ByteCodeModule = ILByteCodeEncoding.Encode(compUnit, compUnit.TemplateFunctions);

				Helpers.EnsureDirectoryExists(Path.GetDirectoryName(actPath));
				using (FileStream stream = new FileStream(actPath, FileMode.Create))
				using (BinaryWriter writer = new BinaryWriter(stream))
					compUnit.ByteCodeModule.Write(writer);

				module.Header.Flags |= ModuleFlags.HasTemplateBytecode;
			}

			return module;
		}

		public static void ModuleOut(Module module, string file)
		{
			string dir = Path.GetDirectoryName(file);
			if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir))
				Directory.CreateDirectory(dir);

			string fileName = Path.GetFileNameWithoutExtension(file);
			using (FileStream fileStream = new FileStream(file, FileMode.Create))
			using (BinaryWriter writer = new BinaryWriter(fileStream))
			{
				module.Write(writer, fileName);
			}
		}

		static Dictionary<string, List<TableEntry>> CreateSymbolTables(List<Symbol> symbols, LTOType lto, Module module)
		{
			Dictionary<string, List<TableEntry>> entries = new Dictionary<string, List<TableEntry>>();

			foreach (Symbol symbol in symbols)
			{
				switch (symbol.Kind)
				{
				case SymbolKind.Struct:
					ProcessStruct(symbol, entries);
					break;
				case SymbolKind.Interface:
					ProcessInterface(symbol, entries, module);
					break;
				case SymbolKind.Union:
					ProcessUnion(symbol, entries);
					break;
				case SymbolKind.Enum:
				{
					EnumSymbolType enumType = symbol.Type as EnumSymbolType;
					if (enumType.IsSimpleEnum)
						ProcessEnum(symbol, entries, module);
					else
						ProcessAdtEnum(symbol, entries, module);
				}
					break;
				case SymbolKind.Variable:
				{
					if (symbol.Parent == null)
						ProcessGlobal(symbol, entries, lto);
					break;
				}
				case SymbolKind.Typedef:
					break;
				case SymbolKind.TypeAlias:
					break;
				case SymbolKind.Function:
				case SymbolKind.Method:
					ProcessFunctionEntry(symbol, entries, lto);
					break;
				case SymbolKind.Delegate:
					ProcessDelegate(symbol, entries);
					break;
				}
			}

			return entries;
		}

		static void ProcessFunctionEntry(Symbol symbol, Dictionary<string, List<TableEntry>> entries, LTOType lto)
		{
			string localExt = ".mjilb";
			/*switch (lto)
			{
			default:
			case LTOType.Obj:
				localExt = ".o";
				break;
			case LTOType.IR:
				localExt = ".mjirb";
				break;
			case LTOType.IL:
				localExt = ".mjilb";
				break;
			}

			if (symbol.TemplateSymbol != null)
				localExt = ".mjilb";*/

			SymbolEntry entry = new SymbolEntry();
			entry.MangledName = symbol.MangledName;
			if (!entry.MangledName.StartsWith("_M"))
			{
				string mangledType = NameMangling.MangleType(symbol.Type);
				entry.MangledType = mangledType;
				entry.Info.Flags |= SymbolInfoFlags.Unmangled;
			}

			if (symbol.CompileAttribs.ContainsKey(ForeignCompileAttribute.Id))
			{
				entry.Info.Flags |= SymbolInfoFlags.Extern;
			}
			
			if (symbol.Visibility == SemanticVisibility.Public)
			{
				if (!entries.ContainsKey("pub"))
					entries.Add("pub", new List<TableEntry>());

				List<TableEntry> pubEntries = entries["pub"];
				
				if ((entry.Info.Flags & SymbolInfoFlags.Extern) == 0)
					entry.FileName = symbol.ContainingFile + localExt;

				pubEntries.Add(entry);
			}
			else if (symbol.Visibility == SemanticVisibility.Export)
			{
				if (!entries.ContainsKey("exp"))
					entries.Add("exp", new List<TableEntry>());

				List<TableEntry> expEntries = entries["exp"];

				// TODO: System specific dynamic extension .dll or .so
				if ((entry.Info.Flags & SymbolInfoFlags.Extern) == 0)
					entry.FileName = symbol.ContainingFile + ".dll";

				expEntries.Add(entry);
			}
			else if (symbol.Visibility == SemanticVisibility.Package)
			{
				if (!entries.ContainsKey("pak"))
					entries.Add("pak", new List<TableEntry>());

				List<TableEntry> pakEntries = entries["pak"];

				if ((entry.Info.Flags & SymbolInfoFlags.Extern) == 0)
					entry.FileName = symbol.ContainingFile + localExt;

				pakEntries.Add(entry);
			}
		}

		static void ProcessGlobal(Symbol symbol, Dictionary<string, List<TableEntry>> entries, LTOType lto)
		{
			string localExt = ".mjilb";
			/*switch (lto)
			{
			default:
			case LTOType.Obj:
				localExt = ".o";
				break;
			case LTOType.IR:
				localExt = ".mjirb";
				break;
			case LTOType.IL:
				localExt = ".mjilb";
				break;
			}

			if (symbol.TemplateSymbol != null)
				localExt = ".mjilb";*/

			SymbolEntry entry = new SymbolEntry();
			entry.MangledName = symbol.MangledName;
			if (!entry.MangledName.StartsWith("_M"))
			{
				string mangledType = NameMangling.MangleType(symbol.Type);
				entry.MangledType = mangledType;
				entry.Info.Flags |= SymbolInfoFlags.Unmangled;
			}

			if (symbol.CompileAttribs.ContainsKey(ForeignCompileAttribute.Id))
			{
				entry.Info.Flags |= SymbolInfoFlags.Extern;
			}

			entry.FileName = symbol.ContainingFile + localExt;

			if (!entries.ContainsKey("glo"))
				entries.Add("glo", new List<TableEntry>());

			List<TableEntry> gloEntries = entries["glo"];

			gloEntries.Add(entry);
		}

		static void ProcessStruct(Symbol symbol, Dictionary<string, List<TableEntry>> entries)
		{
			TypeEntry entry = new TypeEntry();

			entry.Identifier = symbol.MangledName;

			foreach (Symbol child in symbol.Children)
			{
				// TODO: Set info
				TypeChild typeChild = new TypeChild(new SymbolInfo(), child.MangledName);
				entry.Children.Add(typeChild);
			}

			if (!entries.ContainsKey("str"))
				entries.Add("str", new List<TableEntry>());

			List<TableEntry> strEntries = entries["str"];
			strEntries.Add(entry);
		}

		static void ProcessUnion(Symbol symbol, Dictionary<string, List<TableEntry>> entries)
		{
			TypeEntry entry = new TypeEntry();

			entry.Identifier = symbol.MangledName;

			foreach (Symbol child in symbol.Children)
			{
				// TODO: Set info
				TypeChild typeChild = new TypeChild(new SymbolInfo(), child.MangledName);
				entry.Children.Add(typeChild);
			}

			if (!entries.ContainsKey("uni"))
				entries.Add("uni", new List<TableEntry>());

			List<TableEntry> uniEntries = entries["uni"];
			uniEntries.Add(entry);
		}

		static void ProcessEnum(Symbol symbol, Dictionary<string, List<TableEntry>> entries, Module module)
		{
			EnumEntry entry = new EnumEntry();
			entry.Identifier = symbol.MangledName;
			ILEnum ilEnum = module.ILCompUnit.GetEnum(symbol.MangledName);

			foreach (Symbol child in symbol.Children)
			{
				ILEnumMember member = ilEnum.GetMember((child.Identifier as NameIdentifier).Iden);
				EnumMember typeChild = new EnumMember(child.MangledName, member.EnumVal ?? 0, null);
				entry.Members.Add(typeChild);
			}

			if (!entries.ContainsKey("enu"))
				entries.Add("enu", new List<TableEntry>());

			List<TableEntry> enuEntries = entries["enu"];
			enuEntries.Add(entry);
		}

		static void ProcessAdtEnum(Symbol symbol, Dictionary<string, List<TableEntry>> entries, Module module)
		{
			EnumEntry entry = new EnumEntry();
			entry.Identifier = symbol.MangledName;
			ILEnum ilEnum = module.ILCompUnit.GetEnum(symbol.MangledName);

			foreach (Symbol child in symbol.Children)
			{
				ILEnumMember member = ilEnum.GetMember((child.Identifier as NameIdentifier).Iden);
				EnumMember typeChild = new EnumMember(child.MangledName, member.EnumVal ?? 0, member.AdtInit);
				entry.Members.Add(typeChild);
			}

			if (!entries.ContainsKey("adt"))
				entries.Add("adt", new List<TableEntry>());

			List<TableEntry> adtEntries = entries["adt"];
			adtEntries.Add(entry);
		}

		static void ProcessInterface(Symbol symbol, Dictionary<string, List<TableEntry>> entries, Module module)
		{
			TypeEntry entry = new TypeEntry();
			entry.Identifier = symbol.MangledName;

			foreach (Symbol child in symbol.Children)
			{
				// TODO: Set info
				TypeChild typeChild = new TypeChild(new SymbolInfo(), child.MangledName);
				entry.Children.Add(typeChild);
			}

			if (!entries.ContainsKey("int"))
				entries.Add("int", new List<TableEntry>());

			List<TableEntry> interfaceEntries = entries["int"];
			interfaceEntries.Add(entry);
		}

		static void ProcessDelegate(Symbol symbol, Dictionary<string, List<TableEntry>> entries)
		{
			SymbolEntry entry = new SymbolEntry(); // TODO: Should there be a delegate specific table entry?

			entry.MangledName = symbol.MangledName;

			AttributeFlags flags = AttributeFlags.None;
			if (symbol.CompileAttribs.ContainsKey(FuncPtrAttribute.Id))
				flags |= AttributeFlags.DelegateFuncPtr;

			if (flags != AttributeFlags.None)
				entry.Info.AttributeIndex = AddAttributeEntry(flags, entries);

			if (!entries.ContainsKey("del"))
				entries.Add("del", new List<TableEntry>());

			List<TableEntry> adtEntries = entries["del"];
			adtEntries.Add(entry);
		}

		static void CreateAliasTable(List<string> aliases, Module module)
		{
			if (aliases.Count == 0)
				return;

			List<TableEntry> entries = new List<TableEntry>();
			entries.Capacity = aliases.Count;
			foreach (string alias in aliases)
			{
				SymbolEntry entry = new SymbolEntry();
				entry.MangledName = alias;
				entry.FileName = "";
				entries.Add(entry);
			}

			Table modTable = new Table();
			modTable.Iden = "ali";
			modTable.Entries = entries;

			module.Tables.Add(modTable.Iden, modTable);
		}

		static uint AddAttributeEntry(AttributeFlags flags, Dictionary<string, List<TableEntry>> entries)
		{
			AttributeTableEntry attribEntry = new AttributeTableEntry();
			attribEntry.Flags = flags;

			if (!entries.ContainsKey("atr"))
				entries.Add("atr", new List<TableEntry>());

			List<TableEntry> atrTable = entries["atr"];
			uint idx = (uint)atrTable.Count;

			atrTable.Add(attribEntry);

			return idx;
		}

	}
}
