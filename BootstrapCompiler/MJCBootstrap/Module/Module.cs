﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.General;
using MJC.ILBackend;
using MJC.ILBackend.ByteCode;
using MJC.ILBackend.General;
using MJC.IRBackend;

namespace MJC.Module
{
	public class Module
	{
		public ModuleHeader Header = new ModuleHeader();
		public List<string> RequiredModules = new List<string>();
		public Dictionary<string, Table> Tables = new Dictionary<string, Table>();
		public List<ObjectFile> ObjectFiles = new List<ObjectFile>();

		public ILCompUnit ILCompUnit;
		public byte[] ILCbBlob;
		//public IRModule IRModule;
		public List<Symbol> Symbols = new List<Symbol>();

		public string ModuleName
		{
			get
			{
				string tmp = "";
				if (!string.IsNullOrEmpty(Header.Package))
					tmp += Header.Package + '.';
				return tmp + Header.Module;
			}
		}

		public Module()
		{ }

		public Module(ByteBuffer buffer)
		{
			int pos = 0;
			Header = new ModuleHeader(buffer);

			for (int i = 0; i < Header.RequiredModuleCount; i++)
			{
				buffer.ReadNullTerminated(out string reqMod);
				RequiredModules.Add(reqMod);
			}

			for (int i = 0; i < Header.NumSymbolTables; i++)
			{
				Table table = new Table(buffer);
				Tables.Add(table.Iden, table);
			}

			if ((Header.Flags & ModuleFlags.HasTemplateBytecode) != 0)
			{
				buffer.ReadNullTerminated(out string fileName);
				buffer.Read(out int fileSize);
				buffer.Read(out ILCbBlob, fileSize);
			}

			for (int i = 0; i < Header.NumObjectFiles; i++)
			{
				ObjectFile file = new ObjectFile(buffer);
				ObjectFiles.Add(file);
			}
		}

		public void Write(BinaryWriter writer, string fileName)
		{
			Header.Write(writer);

			foreach (string reqMod in RequiredModules)
			{
				ByteUtils.WriteNullTerminatedCStr(writer, reqMod);
			}

			foreach (KeyValuePair<string, Table> pair in Tables)
			{
				pair.Value.Write(writer);
			}
			
			if (ILCompUnit.ByteCodeModule != null)
			{
				ILByteCodeModule bcModule = ILCompUnit.ByteCodeModule;
				ByteUtils.WriteNullTerminatedCStr(writer, fileName + ".mjilb");
				writer.Write((uint) 0); // bytecode size dummy

				int beginLoc = (int) writer.BaseStream.Position;
				bcModule.Write(writer);
				int endPos = (int) writer.BaseStream.Position;

				writer.Seek(beginLoc - 4, SeekOrigin.Begin);
				int len = endPos - beginLoc;
				writer.Write(len);
				writer.Seek(0, SeekOrigin.End);
			}

			foreach (ObjectFile file in ObjectFiles)
			{
				file.Write(writer);
			}

			Header.FinalizeWrite(writer);
		}

		public bool TryGetTable(string iden, out Table table)
		{
			return Tables.TryGetValue(iden, out table);
		}

		public void UpdateTypes(SymbolTable table)
		{
			CompilerContext pseudoContext = new CompilerContext();
			pseudoContext.Symbols = table;
			foreach (Symbol symbol in Symbols)
			{
				if (symbol.Type != null)
				{
					symbol.Type = symbol.Type.GetUpdatedType(null, Scope.Empty, pseudoContext);
				}
			}
		}

		public void UpdateChildren()
		{
			foreach (Symbol symbol in Symbols)
			{
				ScopeVariable parentScopeVar = new ScopeVariable(symbol.Scope);
				Symbol parent = Symbols.Find(s => { return s.ScopeVar == parentScopeVar; });

				if (parent != null)
				{
					parent.Children.Add(symbol);
					symbol.Parent = parent;
				}
			}
		}

		public void GenerateILCompUnit(CompilerContext context)
		{
			ILBuilder builder = new ILBuilder();
			builder.BeginLoaded();
			builder.BuildModuleDirective(Header.Package, Header.Module);
			
			foreach (Symbol symbol in Symbols)
			{
				switch (symbol.Kind)
				{
				case SymbolKind.Struct:
				{
					ILAttributes attribs = ILHelpers.GetAttributes(symbol, null);
					List<ILTemplateParam> templateParams = null;
					
					ScopeVariable aggrScopeVar = symbol.ScopeVar;
					if (aggrScopeVar.Name is TemplateDefinitionIdentifier defIden)
					{
						templateParams = new List<ILTemplateParam>();
						foreach (TemplateParameter param in defIden.Parameters)
						{
							ILTemplateParam ilParam = new ILTemplateParam(param.ValueName, param.Type, param.TypeSpecialization, param.ValueSpecialization, param.ValueDefault);
							templateParams.Add(ilParam);
						}
					}

					builder.CreateAggregate(ILAggregateType.Struct, aggrScopeVar, symbol.MangledName, symbol, attribs, templateParams, null);

					foreach (Symbol child in symbol.Children)
					{
						if (child.Kind == SymbolKind.Variable)
						{
							builder.BuildAggregateVariable(aggrScopeVar, child.Identifier, child.MangledName, child.Type, null);
						}
					}

					break;
				}
				case SymbolKind.Interface:
					// TODO
					break;
				case SymbolKind.Union:
				{
					ILAttributes attribs = ILHelpers.GetAttributes(symbol, null);
					List<ILTemplateParam> templateParams = null;

					ScopeVariable aggrScopeVar = symbol.ScopeVar;
					if (aggrScopeVar.Name is TemplateDefinitionIdentifier defIden)
					{
						templateParams = new List<ILTemplateParam>();
						foreach (TemplateParameter param in defIden.Parameters)
						{
							ILTemplateParam ilParam = new ILTemplateParam(param.ValueName, param.Type, param.TypeSpecialization, param.ValueSpecialization, param.ValueDefault);
							templateParams.Add(ilParam);
						}
					}

					builder.CreateAggregate(ILAggregateType.Union, aggrScopeVar, symbol.MangledName, symbol, attribs, templateParams, null);

					foreach (Symbol child in symbol.Children)
					{
						if (child.Kind == SymbolKind.Variable)
						{
							builder.BuildAggregateVariable(aggrScopeVar, child.Identifier, child.MangledName, child.Type, null);
						}
					}

					break;
				}
				case SymbolKind.Enum:
				{
					ILAttributes attribs = ILHelpers.GetAttributes(symbol, null);
					List<ILTemplateParam> templateParams = null;

					ScopeVariable enumScopeVar = symbol.ScopeVar;
					if (enumScopeVar.Name is TemplateDefinitionIdentifier defIden)
					{
						templateParams = new List<ILTemplateParam>();
						foreach (TemplateParameter param in defIden.Parameters)
						{
							ILTemplateParam ilParam = new ILTemplateParam(param.ValueName, param.Type, param.TypeSpecialization, param.ValueSpecialization, param.ValueDefault);
							templateParams.Add(ilParam);
						}
					}

					EnumSymbolType enumType = symbol.Type as EnumSymbolType;
					ILEnum ilEnum = builder.CreateEnum(enumScopeVar, symbol.MangledName, symbol, attribs, templateParams, enumType.BaseType, null);

					foreach (Symbol child in symbol.Children)
					{
						NameIdentifier nameIden = child.Identifier as NameIdentifier;
						ILEnumMember member = new ILEnumMember(nameIden.Iden, null, child.Type, null /*TODO*/);
						ilEnum.Members.Add(member);
					}

					break;
				}
				case SymbolKind.Variable:
				{
					if (symbol.Parent == null)
					{
						ILAttributes attribs = ILHelpers.GetAttributes(symbol, null);
						builder.BuildGlobal(symbol.MangledName, symbol.Type, "", attribs, null);
						// TODO: Value
					}

					break;
				}
				case SymbolKind.LocalVar:
					break;
				case SymbolKind.Typedef:
					// TODO
					break;
				case SymbolKind.TypeAlias:
				{
					// Needed for public export
					builder.BuildAlias(symbol.MangledName, null);
					break;
				}
				case SymbolKind.Function:
				{
					ILAttributes attribs = ILHelpers.GetAttributes(symbol, null);
					List<ILTemplateParam> templateParams = null;
					// TODO
					
					builder.BeginFunction(symbol.ScopeVar, symbol.MangledName, symbol.Type as FunctionSymbolType, attribs, templateParams, false, null);
					builder.EndFunction();
					break;
				}
				case SymbolKind.Method:
				{
					ILAttributes attribs = ILHelpers.GetAttributes(symbol, null);
					List<ILTemplateParam> templateParams = null;
					// TODO
					builder.BeginFunction(symbol.ScopeVar, symbol.MangledName, symbol.Type as FunctionSymbolType, attribs, templateParams, false, null);
					builder.EndFunction();

					/*FunctionSymbolType funcType = symbol.Type as FunctionSymbolType;
					SymbolType recType = funcType.ReceiverType;
					if (recType is ReferenceSymbolType refType)
						recType = refType.BaseType;
					AggregateSymbolType recAggrType = recType as AggregateSymbolType;
					ScopeVariable recScopeVar = recAggrType.Identifier;

					if (!builder.HasVTable(recScopeVar))
						builder.AddVTable(new VTable(recScopeVar, recAggrType.MangledIdentifier));
					builder.AddVTableMethod(recScopeVar, symbol.Identifier, funcType, symbol.MangledName);*/
					break;
				}
				case SymbolKind.Receiver:
					break;
				case SymbolKind.Delegate:
					break;
				}
			}

			ILCompUnit = builder.End();

			ILByteCodeModule bcModule = ILByteCodeDecoding.DecodeModule(ILCbBlob, context, Header.Package, Header.Module);
			ILCompUnit.ByteCodeModule = bcModule;

			// Update functions with block loaded from bytecode
			foreach (KeyValuePair<string, ILFunction> pair in ILCompUnit.Functions)
			{
				ILFunction bcFunc = bcModule.ILFunctions.Find(f => { return f.MangledIdentifier == pair.Key; });
				if (bcFunc != null)
				{
					ILFunction func = pair.Value;
					func.Blocks = bcFunc.Blocks;
				}
			}
		}

		public void GenerateIRModule()
		{

		}
	}

	[Flags]
	public enum ModuleFlags : ushort
	{

		// LTO
		LTOObj				= 0b0000_0000_0000_0000,
		LTOIR				= 0b0000_0000_0000_0100,
		LTOIL				= 0b0000_0000_0000_1000,

		StaticOnly			= 0b0000_0000_0001_0000,
		DynamicOnly			= 0b0000_0000_0010_0000,

		HasTemplates		= 0b0000_0001_0000_0000,

		HasTemplateBytecode	= 0b1000_0000_0000_0000,
	}

	public class ModuleHeader
	{
		public static uint CorrectMagic = 0x4D4A4D80;

		public uint Magic;
		public uint FileSize;
		public ModuleFlags Flags;

		public string Package;
		public string Module;

		public short RequiredModuleCount;
		public short NumSymbolTables;
		public short NumObjectFiles;

		public ModuleHeader()
		{
		}

		public ModuleHeader(ByteBuffer buffer)
		{
			buffer.Read(out Magic);
			buffer.Read(out FileSize);
			buffer.Read(out ushort flags);
			Flags = (ModuleFlags)flags;
			
			buffer.ReadNullTerminated(out Package);
			buffer.ReadNullTerminated(out Module);
			
			buffer.Read(out RequiredModuleCount);
			buffer.Read(out NumSymbolTables);
			buffer.Read(out NumObjectFiles);
		}

		public void Write(BinaryWriter writer)
		{
			writer.Write(Magic);
			writer.Write((uint)0); // dummy file size
			writer.Write((ushort)Flags);

			ByteUtils.WriteNullTerminatedCStr(writer, Package);
			ByteUtils.WriteNullTerminatedCStr(writer, Module);

			writer.Write(RequiredModuleCount);
			writer.Write(NumSymbolTables);
			writer.Write(NumObjectFiles);
		}

		public void FinalizeWrite(BinaryWriter writer)
		{
			writer.Seek(4, SeekOrigin.Begin);
			writer.Write((uint)writer.BaseStream.Length);
		}
	}

	public class Table
	{
		public string Iden;
		public List<TableEntry> Entries = new List<TableEntry>();

		public Table()
		{ }

		public Table(ByteBuffer buffer)
		{
			buffer.ReadNullTerminated(out Iden);
			buffer.Read(out int numEntries);

			switch (Iden)
			{
			case "exp":
			case "pub":
			case "pak":
			case "imp":
			case "glo":
			case "ali":
			case "del":
			{
				for (int i = 0; i < numEntries; i++)
				{
					SymbolEntry entry = new SymbolEntry(buffer);
					Entries.Add(entry);
				}
				break;
			}
			case "str":
			case "uni":
			{
				for (int i = 0; i < numEntries; i++)
				{
					TypeEntry entry = new TypeEntry(buffer);
					Entries.Add(entry);
				}
				break;
			}
			case "enu":
			case "adt":
			{
				for (int i = 0; i < numEntries; i++)
				{
					EnumEntry entry = new EnumEntry(buffer);
					Entries.Add(entry);
				}
				break;
			}
			case "atr":
			{
				for (int i = 0; i < numEntries; i++)
				{
					AttributeTableEntry entry = new AttributeTableEntry(buffer);
					Entries.Add(entry);
				}
				break;
			}
			}
			
		}

		public void Write(BinaryWriter writer)
		{
			ByteUtils.WriteNullTerminatedCStr(writer, Iden);
			writer.Write(Entries.Count);

			foreach (TableEntry entry in Entries)
			{
				entry.Write(writer);
			}
		}
	}

	public class TableEntry
	{
		public virtual void Write(BinaryWriter writer)
		{
		}
	}
	
	public class SymbolEntry : TableEntry
	{
		public SymbolInfo Info;
		public string MangledName;
		public string MangledType;
		public string FileName = "";

		public SymbolEntry()
		{
			Info = new SymbolInfo();
		}

		public SymbolEntry(ByteBuffer buffer)
		{
			Info = new SymbolInfo(buffer);
			buffer.ReadNullTerminated(out MangledName);

			if ((Info.Flags & SymbolInfoFlags.Unmangled) != 0)
			{
				buffer.ReadNullTerminated(out MangledType);
			}

			buffer.ReadNullTerminated(out FileName);
		}

		public override void Write(BinaryWriter writer)
		{
			Info.Write(writer);

			ByteUtils.WriteNullTerminatedCStr(writer, MangledName);
			if ((Info.Flags & SymbolInfoFlags.Unmangled) != 0)
				ByteUtils.WriteNullTerminatedCStr(writer, MangledType);
			ByteUtils.WriteNullTerminatedCStr(writer, FileName);
		}
	}

	public class TypeEntry : TableEntry
	{
		public SymbolInfo Info;
		public string Identifier;
		public List<TypeChild> Children = new List<TypeChild>();

		public TypeEntry()
		{
			Info = new SymbolInfo();
		}

		public TypeEntry(ByteBuffer buffer)
		{
			Info = new SymbolInfo(buffer);
			buffer.ReadNullTerminated(out Identifier);
			buffer.Read(out int numChildren);

			for (int i = 0; i < numChildren; i++)
			{
				TypeChild child = new TypeChild(buffer);
				Children.Add(child);
			}
		}

		public override void Write(BinaryWriter writer)
		{
			Info.Write(writer);

			ByteUtils.WriteNullTerminatedCStr(writer, Identifier);
			writer.Write(Children.Count);

			foreach (TypeChild child in Children)
			{
				child.Write(writer);
			}
		}
	}

	public class TypeChild
	{
		public SymbolInfo Info;
		public string Identifier;

		public TypeChild()
		{ }

		public TypeChild(SymbolInfo info, string iden)
		{
			Info = info;
			Identifier = iden;
		}

		public TypeChild(ByteBuffer buffer)
		{
			Info = new SymbolInfo(buffer);
			buffer.ReadNullTerminated(out Identifier);
		}

		public void Write(BinaryWriter writer)
		{
			Info.Write(writer);
			ByteUtils.WriteNullTerminatedCStr(writer, Identifier);
		}
	}

	public class EnumEntry : TableEntry
	{
		public SymbolInfo Info;
		public string Identifier;
		public List<EnumMember> Members;

		public EnumEntry()
		{
			Info = new SymbolInfo();
			Members = new List<EnumMember>();
		}

		public EnumEntry(ByteBuffer buffer)
		{
			Info = new SymbolInfo(buffer);
			buffer.ReadNullTerminated(out Identifier);
			buffer.Read(out int memberCount);

			List<EnumMember> members = new List<EnumMember>(memberCount);
			for (uint i = 0; i < memberCount; i++)
			{
				EnumMember member = new EnumMember(buffer);
				members.Add(member);
			}
		}

		public override void Write(BinaryWriter writer)
		{
			Info.Write(writer);
			ByteUtils.WriteNullTerminatedCStr(writer, Identifier);
			writer.Write(Members.Count);
			foreach (EnumMember member in Members)
			{
				member.Write(writer);
			}
		}
	}

	public class EnumMember
	{
		public string Identifier;
		public long Value;
		public string AdtInit;

		public EnumMember(string iden, long val, string adtInit)
		{
			Identifier = iden;
			Value = val;
			AdtInit = adtInit;
		}

		public EnumMember(ByteBuffer buffer)
		{
			buffer.ReadNullTerminated(out Identifier);
			buffer.Read(out Value);
			buffer.ReadNullTerminated(out AdtInit);
		}

		public void Write(BinaryWriter writer)
		{
			ByteUtils.WriteNullTerminatedCStr(writer, Identifier);
			writer.Write(Value);
			ByteUtils.WriteNullTerminatedCStr(writer, AdtInit ?? "");
		}
	}

	public class ObjectFile
	{
		public string FileName;
		public byte[] Data;

		public ObjectFile()
		{ }

		public ObjectFile(ByteBuffer buffer)
		{
			buffer.ReadNullTerminated(out FileName);
			buffer.Read(out int fileSize);
			buffer.Read(out Data, fileSize);
		}

		public void Write(BinaryWriter writer)
		{
			ByteUtils.WriteNullTerminatedCStr(writer, FileName);
			writer.Write(Data.Length);
			writer.Write(Data);
		}
	}

	[Flags]
	public enum SymbolInfoFlags : byte
	{
		None		= 0b0000_0000,
		Unmangled	= 0b0000_0001,
		Extern		= 0b0000_0010,
	}

	public class SymbolInfo
	{
		public SymbolInfoFlags Flags;
		public uint AttributeIndex;

		public SymbolInfo()
		{ }

		public SymbolInfo(ByteBuffer buffer)
		{
			buffer.Read(out uint encoded);
			Flags = (SymbolInfoFlags) (encoded >> 24);
			AttributeIndex = encoded & 0x00FF_FFFF;
		}

		public void Write(BinaryWriter writer)
		{
			uint encoded = (uint) Flags << 24 & AttributeIndex;
			writer.Write(encoded);
		}
	}

	[Flags]
	public enum AttributeFlags : ushort
	{
		None			= 0,
		Const			= 1 << 0,
		CConst			= 1 << 1,
		Immutable		= 1 << 2,

		DelegateFuncPtr	= 1 << 8,
		ArrayPtr		= 1 << 9,
		StackArray		= 1 << 10,
	}

	public class AttributeTableEntry : TableEntry
	{
		public AttributeFlags Flags;

		public AttributeTableEntry()
		{ }

		public AttributeTableEntry(ByteBuffer buffer)
		{
			buffer.Read(out ushort flags);
			Flags = (AttributeFlags) flags;
		}

		public override void Write(BinaryWriter writer)
		{
			writer.Write((ushort)Flags);
		}
	}

}
