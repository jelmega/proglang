﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MJC.Module;

namespace MJC
{
	public static class CmdLine
	{
		public static string SourceDirectory = null;
		public static string IntermediateDirectory = "intermediate";
		public static string OutDirectory = "out";
		public static List<string> Files = new List<string>();
		public static string OutputFile = null;
		public static List<string> IncludeDirs = new List<string>();

		public static bool Compile = true;
		public static bool Assemble = true;
		public static bool Link = true;
		public static bool GenerateModule = true; // Default since compiler interprets now
		public static bool DynamicModule = false;

		public static string DefaultModule = "main";

		public static string Architecture = null;

		// Used to decided for x64 instead of arch
		public static bool IsX64 = true;

		public static string ModuleToInterpret = null;
		//public static bool UseLLVM = false;

		public static List<string> AdditionalDebugValues = new List<string>();
		public static List<string> AdditionalVersionValues = new List<string>();

		public static bool OutLex = false;
		public static bool OutAst = false;
		public static bool OutAstObf = false;
		public static bool OutAstFull = false;

		public static bool Stats = false;

		public static LTOType LTO = LTOType.Obj;

		public static void Parse(string[] cmdLine)
		{
			if (cmdLine.Length == 0)
			{
				Compile = false;
				PrintHelp();
				return;
			}

			for (int i = 0; i < cmdLine.Length; i++)
			{
				switch (cmdLine[i])
				{
				case "-help":
				case "-?":
				{
					Compile = false;
					Assemble = false;
					Link = false;
					PrintHelp();
					break;
				}
				case "--sourcedir":
				{
					SourceDirectory = cmdLine[++i].Trim('"');
					if (!SourceDirectory.EndsWith('/') && !SourceDirectory.EndsWith('\\'))
						SourceDirectory += '/';
					break;
				}
				case "--interdir":
				{
					IntermediateDirectory = cmdLine[++i].Trim('"');
					if (string.IsNullOrWhiteSpace(IntermediateDirectory))
						IntermediateDirectory = "intermediate";
					if (!IntermediateDirectory.EndsWith('/') && !IntermediateDirectory.EndsWith('\\'))
						IntermediateDirectory += '/';
					break;
				}
				case "--outdir":
				{
					OutDirectory = cmdLine[++i].Trim('"');
					if (!OutDirectory.EndsWith('/') && !OutDirectory.EndsWith('\\'))
						OutDirectory += '/';

					if (!Directory.Exists(OutDirectory))
						Directory.CreateDirectory(OutDirectory);
					break;
				}
				case "-c":
				{
					Link = false;
					break;
				}
				case "-s":
				{
					Assemble = false;
					Link = false;
					break;
				}
				case "-m":
				{
					GenerateModule = true;
					Link = false;
					break;
				}
				case "-d":
				{
					DynamicModule = true;
					GenerateModule = true;
					Link = false;
					break;
				}
				case "-o":
				{
					OutputFile = cmdLine[++i].Trim('"');
					break;
				}
				case "-i":
				{
					IncludeDirs.Add(cmdLine[i].Trim('"'));
					break;
				}
				case "-D":
				{
					AdditionalDebugValues.Add(cmdLine[i]);
					break;
				}
				case "-V":
				{
					AdditionalVersionValues.Add(cmdLine[i]);
					break;
				}

				case "-march":
				{
					++i;
					Architecture = cmdLine[i].ToLower();
					break;
				}
				case "-interp":
				{
					++i;
					ModuleToInterpret = cmdLine[i];
					break;
				}
				/*case "-llvm":
				{
					UseLLVM = true;
					break;
				}*/
				case "-lto":
				{
					++i;
					string strVal = cmdLine[i];
					int val = int.Parse(strVal);
					LTO = (LTOType) val;
					break;
				}

				case "--lex":
				{
					OutLex = true;
					break;
				}
				case "--ast":
				{
					OutAst = true;
					break;
				}
				case "--ast-obf":
				{
					OutAstObf = true;
					break;
				}
				case "--ast-full":
				{
					OutAstFull = true;
					break;
				}
				case "--stat":
				{
					Stats = true;
					break;
				}
				default:
				{
					if (!cmdLine[i].StartsWith('-'))
						Files.Add(cmdLine[i].Trim('"'));
					break;
				}
				}
			}

			AddSourceFiles(SourceDirectory);
		}

		private static void AddSourceFiles(string dir)
		{
			if (dir == null || !Directory.Exists(dir))
				return;

			IEnumerable<string> files = Directory.EnumerateFiles(dir, "*.mj", SearchOption.AllDirectories).ToArray();
			Files.AddRange(files);
		}

		private static void PrintHelp()
		{
			string str = "";

			str += "Use: 'dotnet mjc [<options>] [<sources>] [<options>]'\n";
			str += "<sources>: Source files (--sourcedir can be used instead of separate files)\n";
			str += "when compiling and linking, all source files that make up the module are required\n";
			str += "\nGeneral options:\n";
			str += "\n";
			str += "-help, --help    : print help\n";
			str += "--sourcedir <dir>: Specifies the directory containing the source files\n";
			str += "--interdir <dir> : Specifies the directory to output the intermediate files to\n";
			str += "--outdir <dir>   : Specifies the directory to output the output files to\n";
			str += "-c               : Compile the source files, but do not link\n";
			str += "-s               : Compile the source files, but do not assemble, this will output an assembly file\n";
			str += "-o <file>        : Location of output file, regardless of the type of file that is produced, only available when using 1 input file\n";
			str += "-i <dir>         : Add a directory containing import modules for the compiler to use\n";
			str += "-m               : Generate a module instead of creating an executable\n";
			str += "-d               : Generate a dynamic module instead of creating an executable\n";
			str += "-lto <val>       : Specify the link time optimization level\n";
			str += "                   - 0: No LTO       (code compiler to object code)\n";
			str += "                   - 1: Minimal LTO  (code compiled to IR)\n";
			str += "                   - 2: Standard LTO (code compiler to IL)\n";
			str += "                   - 3: Slow LTO     (code compiled to unoptimized IL)\n";

			str += "\nCompile-time options:\n";
			str += "\n";
			str += "-D name          : Add a value to the debug conditional\n";
			str += "-V name          : Add a value to the version conditional\n";

			str += "\nIntermediate output:\n";
			str += "\n";
			str += "--lex            : Output lexer output\n";
			str += "--ast            : Output abstract syntax tree\n";
			str += "--ast-obf        : Output abstract syntax tree as obfuscated code\n";
			str += "--ast-full       : Output abstract syntax tree as full code\n";

			str += "\nUtility options:\n";
			str += "\n";
			str += "--stat           : Output compiler statistics\n";

			Console.WriteLine(str);
		}
	}
}
