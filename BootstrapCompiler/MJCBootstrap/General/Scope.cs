﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.SyntaxTree;

namespace MJC.General
{
	public class Scope
	{
		public List<Identifier> Names = new List<Identifier>();

		public Identifier LastSubScope
		{
			get
			{
				if (Names.Count == 0)
					return null;
				return Names.Last();
			}
			set
			{
				if (Names.Count > 0)
					Names[Names.Count - 1] = value;
			}
		}

		public static readonly Scope Empty = new Scope();

		public Scope()
		{
		}

		public Scope(Scope other)
		{
			Names.AddRange(other.Names);
		}

		public Scope(ScopeVariable other)
		{
			if (other.Scope?.Names != null)
				Names.AddRange(other.Scope.Names);
			Names.Add(other.Name);
		}

		public Scope(List<Identifier> names)
		{
			Names.AddRange(names);
		}

		public Scope(Identifier scopeName)
		{
			Names.Add(scopeName);
		}

		public Scope(NameSyntax syntax)
		{
			if (syntax is QualifiedNameSyntax qualifiedName)
			{
				Identifier name = null;
				NameSyntax left = qualifiedName;
				bool add = qualifiedName.SeperationToken.Type == TokenType.ColonColon;
				while (left != null)
				{
					if (left is QualifiedNameSyntax qualName)
					{
						if (add)
						{
							name = Identifier.GetIdentifier(qualName.Right);
							Names.Add(name);
						}
						else if (qualName.SeperationToken.Type == TokenType.ColonColon)
						{
							add = true;
						}

						left = qualName.Left;
					}
					else if (add && left is SimpleNameSyntax simpleName)
					{
						name = Identifier.GetIdentifier(simpleName);
						Names.Add(name);
						left = null;
					}
					else
					{
						// End of scope
						left = null;
					}
				}

				Names.Reverse();
			}
			else if (syntax is SimpleNameSyntax idenName)
			{
				Names.Add(Identifier.GetIdentifier(idenName));
			}
		}

		public Scope GetSubScope(Scope baseScope)
		{
			if (Names.Count <= baseScope.Names.Count)
				return new Scope();

			int index = baseScope.Names.Count;
			int len = Names.Count - index;
			return new Scope(Names.GetRange(index, len));
		}

		public Scope GetClosestSubScope(Scope baseScope)
		{
			int maxCount = Math.Min(Names.Count, baseScope.Names.Count);
			
			int index = 0;
			for (; index < maxCount; index++)
			{
				if (Names[index] != baseScope.Names[index])
					break;
			}
			
			int len = Names.Count - index;
			return new Scope(Names.GetRange(index, len));
		}

		public Scope GetBaseScope()
		{
			if (Names.Count <= 1)
				return new Scope();
			return new Scope(Names.GetRange(0, Names.Count - 1));
		}

		public bool ContainsScope(Scope scope)
		{
			if (scope.Names.Count > Names.Count)
				return false;

			for (int i = 0; i < scope.Names.Count; i++)
			{
				if (scope.Names[i] != Names[i])
					return false;
			}

			return true;
		}

		public Scope GetBaseTemplateScope()
		{
			Scope scope = new Scope();
			foreach (Identifier identifier in Names)
			{
				scope.Names.Add(identifier.GetBaseIdentifier());
			}
			return scope;
		}

		public bool IsEmpty()
		{
			return Names == null || Names.Count == 0;
		}

		public override string ToString()
		{
			return Names?.ToListString("::") ?? "";
		}

		public static bool operator ==(Scope scope0, Scope scope1)
		{
			if (ReferenceEquals(scope0, null) || ReferenceEquals(scope1, null))
				return ReferenceEquals(scope0, null) && ReferenceEquals(scope1, null);

			if (scope0.Names.Count != scope1.Names.Count)
				return false;

			for (int i = 0; i < scope0.Names.Count; i++)
			{
				if (scope0.Names[i] != scope1.Names[i])
					return false;
			}

			return true;
		}

		public static bool operator !=(Scope scope0, Scope scope1)
		{
			return !(scope0 == scope1);
		}

		public override bool Equals(object obj)
		{
			if (obj is Scope scope)
				return this == scope;
			return false;
		}

		protected bool Equals(Scope other)
		{
			return Equals(Names, other.Names);
		}

		public override int GetHashCode()
		{
			int hash = 0;
			foreach (Identifier name in Names)
			{
				hash *= 397;
				hash ^= name.GetHashCode();
			}
			return hash;
		}

		public int GetDistance(Scope baseScope)
		{
			int indent = 0;
			while (indent < Names.Count && indent < baseScope.Names.Count)
			{
				if (Names[indent] == baseScope.Names[indent])
					++indent;
				else
					break;
			}

			return Names.Count - indent;
		}
	}

	public class ScopeVariable
	{
		public Scope Scope;
		public Identifier Name;

		public static readonly ScopeVariable Empty = new ScopeVariable(Scope.Empty, new NameIdentifier(""));

		public ScopeVariable()
		{
		}

		public ScopeVariable(Scope scope, Identifier name)
		{
			Scope = scope;
			Name = name;
		}

		public ScopeVariable(Identifier name)
		{
			Scope = new Scope();
			Name = name;
		}

		public ScopeVariable(Identifier[] nameParts)
		{
			List<Identifier> scopeNames = new List<Identifier>();
			scopeNames.AddRange(nameParts);
			scopeNames.RemoveLast();
			Scope = new Scope(scopeNames);
			Name = nameParts.Last();
		}

		public ScopeVariable(Scope scope)
		{
			Scope = scope.GetBaseScope();
			Name = scope.LastSubScope;
		}

		public ScopeVariable(ScopeVariable other)
		{
			List<Identifier> scopeNames = new List<Identifier>();
			scopeNames.AddRange(other.Scope.Names);
			Scope = new Scope(scopeNames);
			Name = other.Name;
		}

		public ScopeVariable GetBaseTemplate()
		{
			return new ScopeVariable(Scope, Name.GetBaseIdentifier());
		}

		public override string ToString()
		{
			if (Scope != null)
				return Scope.ToString() + "::" + Name.ToString();
			return Name.ToString();
		}

		public static bool operator ==(ScopeVariable var0, ScopeVariable var1)
		{
			if (ReferenceEquals(var0, null) || ReferenceEquals(var1, null))
				return ReferenceEquals(var0, null) && ReferenceEquals(var1, null);

			return var0.Scope == var1.Scope && var0.Name == var1.Name;
		}

		public static bool operator !=(ScopeVariable var0, ScopeVariable var1)
		{
			return !(var0 == var1);
		}

		public override bool Equals(object obj)
		{
			if (obj is ScopeVariable var)
				return this == var;
			return false;
		}

		protected bool Equals(ScopeVariable other)
		{
			return Equals(Scope, other.Scope) && string.Equals(Name, other.Name);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((Scope != null ? Scope.GetHashCode() : 0) * 397) ^ (Name != null ? Name.GetHashCode() : 0);
			}
		}
	}
}
