﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.General
{
	public class ByteBuffer
	{
		private byte[] _blob;
		private int _pos;

		public ByteBuffer()
		{ }

		public ByteBuffer(byte[] blob, int startPos = 0)
		{
			_blob = blob;
			_pos = startPos;
		}

		public void New(int size)
		{
			_blob = new byte[size];
			_pos = 0;
		}

		public bool Read(out sbyte val)
		{
			val = 0;
			if (_pos >= _blob.Length)
				return false;

			val = (sbyte)_blob[_pos];
			++_pos;
			return true;
		}

		public bool Read(out byte val)
		{
			val = 0;
			if (_pos >= _blob.Length)
				return false;

			val = _blob[_pos];
			++_pos;
			return true;
		}

		public bool Read(out short val)
		{
			val = 0;
			if (_pos + sizeof(short) > _blob.Length)
				return false;

			val = BitConverter.ToInt16(_blob, _pos);
			_pos += sizeof(short);
			return true;
		}

		public bool Read(out ushort val)
		{
			val = 0;
			if (_pos + sizeof(ushort) > _blob.Length)
				return false;

			val = BitConverter.ToUInt16(_blob, _pos);
			_pos += sizeof(ushort);
			return true;
		}

		public bool Read(out int val)
		{
			val = 0;
			if (_pos + sizeof(int) > _blob.Length)
				return false;

			val = BitConverter.ToInt32(_blob, _pos);
			_pos += sizeof(int);
			return true;
		}

		public bool Read(out uint val)
		{
			val = 0;
			if (_pos + sizeof(uint) > _blob.Length)
				return false;

			val = BitConverter.ToUInt32(_blob, _pos);
			_pos += sizeof(uint);
			return true;
		}

		public bool Read(out long val)
		{
			val = 0;
			if (_pos + sizeof(long) > _blob.Length)
				return false;

			val = BitConverter.ToInt64(_blob, _pos);
			_pos += sizeof(long);
			return true;
		}

		public bool Read(out ulong val)
		{
			val = 0;
			if (_pos + sizeof(ulong) > _blob.Length)
				return false;

			val = BitConverter.ToUInt64(_blob, _pos);
			_pos += sizeof(ulong);
			return true;
		}

		public bool ReadNullTerminated(out string val)
		{
			val = null;

			int end = Array.IndexOf(_blob, (byte)0, _pos);
			if (end == -1)
				return false;

			int len = end - _pos;
			val = Encoding.ASCII.GetString(_blob, _pos, len);
			_pos = end + 1;

			return true;
		}

		public bool Read(out byte[] blob, int size)
		{
			blob = null;

			if (_pos + size > _blob.Length)
				return false;

			blob = new byte[size];
			Array.Copy(_blob, _pos, blob, 0, size);
			return true;
		}

		public bool Seek(int pos)
		{
			if (pos >= _blob.Length)
				return false;

			_pos = pos;
			return true;
		}

	}
}
