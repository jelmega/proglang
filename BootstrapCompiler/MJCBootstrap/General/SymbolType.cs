﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MJC.SyntaxTree;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC.General
{
	public enum BuiltinTypes
	{
		Unkown,
		Void,
		Bool,
		I8,
		I16,
		I32,
		I64,
		ISize,
		U8,
		U16,
		U32,
		U64,
		USize,
		F32,
		F64,
		Char,
		WChar,
		Rune,
		String,
		StringLiteral,
		Null,
		Any,
		Count
	}

	[Flags]
	public enum TypeAttributes
	{
		None		= 0b0000_0000_0000_0000,
		Const		= 0b0000_0000_0000_0001,
		Immutable	= 0b0000_0000_0000_0010,
		Mutable		= 0b0000_0000_0000_0100,


		StackArray	= 0b0000_0001_0000_0000,
	}
	
	public class SymbolType
	{
		#region static dictionaries
		private static Dictionary<TypeAttributes, BuiltinSymbolType>[] _builtinTypes;
		private static Dictionary<SymbolType, Dictionary<TypeAttributes, PointerSymbolType>> _pointerTypes 
			= new Dictionary<SymbolType, Dictionary<TypeAttributes, PointerSymbolType>>();
		private static Dictionary<SymbolType, Dictionary<TypeAttributes,ReferenceSymbolType>> _refTypes 
			= new Dictionary<SymbolType, Dictionary<TypeAttributes, ReferenceSymbolType>>();
		private static Dictionary<SymbolType, Dictionary<TypeAttributes, NullableSymbolType>> _nullTypes 
			= new Dictionary<SymbolType, Dictionary<TypeAttributes, NullableSymbolType>>();
		private static Dictionary<SymbolType, Dictionary<TypeAttributes, Dictionary<SimpleExpressionSyntax, ArraySymbolType>>> _arrayTypes 
			= new Dictionary<SymbolType, Dictionary<TypeAttributes, Dictionary<SimpleExpressionSyntax, ArraySymbolType>>>();
		private static Dictionary<SymbolType, Dictionary<TypeAttributes, VariadicSymbolType>> _variadicTypes
			= new Dictionary<SymbolType, Dictionary<TypeAttributes, VariadicSymbolType>>();
		private static Dictionary<SymbolType, Dictionary<TypeAttributes, MemoryLocSymbolType>> _memLocTypes 
			= new Dictionary<SymbolType, Dictionary<TypeAttributes, MemoryLocSymbolType>>();
		private static Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, EnumSymbolType>>> _enumTypes 
			= new Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, EnumSymbolType>>>();
		private static Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes,AggregateSymbolType>>> _aggrTypes 
			= new Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, AggregateSymbolType>>>();
		private static Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, InterfaceSymbolType>>> _interfaceTypes 
			= new Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, InterfaceSymbolType>>>();
		private static Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, SelfSymbolType>>> _selfTypes
			= new Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, SelfSymbolType>>>();
		private static Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, UnknownSymbolType>>> _unknownTypes
			= new Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, UnknownSymbolType>>>();
		private static Dictionary<SymbolType, Dictionary<TypeAttributes, Dictionary<byte, BitFieldSymbolType>>> _bitfieldTypes
			= new Dictionary<SymbolType, Dictionary<TypeAttributes, Dictionary<byte, BitFieldSymbolType>>>();
		private static Dictionary<int, List<TupleSymbolType>> _tupleTypes
			= new Dictionary<int, List<TupleSymbolType>>();
		private static Dictionary<int, List<FunctionSymbolType>> _funcTypes 
			= new Dictionary<int, List<FunctionSymbolType>>();
		private static Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, DelegateSymbolType>>> _delTypes
			= new Dictionary<Scope, Dictionary<string, Dictionary<TypeAttributes, DelegateSymbolType>>>();
		private static Dictionary<Scope, Dictionary<string, Dictionary<int, Dictionary<TypeAttributes, TemplateParamSymbolType>>>> _templateParamTypes
			= new Dictionary<Scope, Dictionary<string, Dictionary<int, Dictionary<TypeAttributes, TemplateParamSymbolType>>>>();
		private static Dictionary<Symbol, Dictionary<Scope, Dictionary<Identifier, Dictionary<TypeAttributes, TemplateInstanceSymbolType>>>> _templateInstanceTypes
			= new Dictionary<Symbol, Dictionary<Scope, Dictionary<Identifier, Dictionary<TypeAttributes, TemplateInstanceSymbolType>>>>();
		#endregion

		public TypeAttributes Attributes { protected set; get; }

		static SymbolType()
		{
			_builtinTypes = new Dictionary<TypeAttributes, BuiltinSymbolType>[(int)BuiltinTypes.Count];
		}

		public static SymbolType AttributedType(SymbolType type, TypeAttributes attributes)
		{
			switch (type)
			{
			case AggregateSymbolType aggregateType:
				return AggregateType(aggregateType.Identifier, aggregateType.Type, aggregateType.Symbol, attributes);
			case ArraySymbolType arrayType:
				return ArrayType(arrayType.BaseType, arrayType.SizeExpr, arrayType.ArraySize, attributes);
			case BuiltinSymbolType builtinType:
				return BuiltinType(builtinType.Builtin, attributes);
			case EnumSymbolType enumType:
				return EnumType(enumType.Identifier, enumType.BaseType, enumType.Symbol, attributes);
			case FunctionSymbolType functionType:
				return FunctionType(functionType.ReceiverType, functionType.ParamTypes, functionType.ReturnType, attributes);
			case DelegateSymbolType delegateType:
				return DelegateType(delegateType.Identifier, delegateType.ParamTypes, delegateType.ReturnType, delegateType.Symbol, attributes);
			case InterfaceSymbolType interfaceType:
				return InterfaceType(interfaceType.Identifier, interfaceType.Symbol, attributes);
			case MemoryLocSymbolType memoryLocType:
				return MemoryLocType(memoryLocType.BaseType, attributes);
			case NullableSymbolType nullableType:
				return NullableType(nullableType.BaseType, attributes);
			case VariadicSymbolType variadicType:
				return VariadicType(variadicType.BaseType, attributes);
			case PointerSymbolType pointerType:
				return PointerType(pointerType.BaseType, attributes);
			case ReferenceSymbolType referenceType:
				return ReferenceType(referenceType.BaseType, attributes);
			case TupleSymbolType tupleType:
				return TupleType(tupleType.SubTypes, attributes);
			case SelfSymbolType selfType:
				return SelfType(selfType.Identifier, attributes);
			case UnknownSymbolType unknownType:
				return UnknownType(unknownType.Identifier, attributes);
			case BitFieldSymbolType bitFieldType:
				return BitFieldType(bitFieldType.BaseType, bitFieldType.Bits, attributes);
			case TemplateParamSymbolType templateParamType:
				return TemplateParamType(templateParamType.Iden, templateParamType.Index, attributes);
			case TemplateInstanceSymbolType templateInstanceType:
				return TemplateInstanceType(templateInstanceType.Iden, templateInstanceType.TemplateSymbol, attributes);
			default:
				return type;
			}
		}

		public static BuiltinSymbolType BuiltinType(BuiltinTypes type, TypeAttributes attribs = TypeAttributes.None)
		{
			if (type == BuiltinTypes.ISize)
				type = CmdLine.IsX64 ? BuiltinTypes.I64 : BuiltinTypes.I32;
			else if (type == BuiltinTypes.USize)
				type = CmdLine.IsX64 ? BuiltinTypes.U64 : BuiltinTypes.U32;

			if (_builtinTypes[(int) type] == null)
				_builtinTypes[(int)type] = new Dictionary<TypeAttributes, BuiltinSymbolType>();

			Dictionary<TypeAttributes, BuiltinSymbolType> subDict = _builtinTypes[(int) type];

			if (!subDict.ContainsKey(attribs))
				subDict.Add(attribs, new BuiltinSymbolType(type, attribs));

			return subDict[attribs];
		}
		
		public static BuiltinSymbolType BuiltinType(int bytes, BuiltinTypes baseType, TypeAttributes attribs = TypeAttributes.None)
		{
			BuiltinTypes type = BuiltinTypes.Unkown;
			switch (baseType)
			{
			case BuiltinTypes.Char:
			{
				if (bytes == 1)
					type = BuiltinTypes.Char;
				else if (bytes == 2)
					type = BuiltinTypes.WChar;
				else
					type = BuiltinTypes.Rune;
				break;
			}
			case BuiltinTypes.I8:
			{
				if (bytes == 1)
					type = BuiltinTypes.I8;
				else if (bytes == 2)
					type = BuiltinTypes.I16;
				else if (bytes == 4)
					type = BuiltinTypes.I32;
				else
					type = BuiltinTypes.I64;
				break;
			}
			case BuiltinTypes.U8:
			{
				if (bytes == 1)
					type = BuiltinTypes.U8;
				else if (bytes == 2)
					type = BuiltinTypes.U16;
				else if (bytes == 4)
					type = BuiltinTypes.U32;
				else
					type = BuiltinTypes.U64;
				break;
			}
			case BuiltinTypes.F32:
			{
				if (bytes == 4)
					type = BuiltinTypes.F32;
				else
					type = BuiltinTypes.F64;
				break;
			}
			case BuiltinTypes.ISize:
				type = CmdLine.IsX64 ? BuiltinTypes.I64 : BuiltinTypes.I32;
				break;
			case BuiltinTypes.USize:
				type = CmdLine.IsX64 ? BuiltinTypes.U64 : BuiltinTypes.U32;
				break;
			}

			return BuiltinType(type, attribs);
		}
		
		public static PointerSymbolType PointerType(SymbolType baseType, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_pointerTypes.ContainsKey(baseType))
				_pointerTypes.Add(baseType, new Dictionary<TypeAttributes, PointerSymbolType>());

			Dictionary<TypeAttributes, PointerSymbolType> subDict = _pointerTypes[baseType];

			if (!subDict.ContainsKey(attribs))
				subDict.Add(attribs, new PointerSymbolType(baseType, attribs));

			return subDict[attribs];
		}

		public static ReferenceSymbolType ReferenceType(SymbolType baseType, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_refTypes.ContainsKey(baseType))
				_refTypes.Add(baseType, new Dictionary<TypeAttributes, ReferenceSymbolType>());

			Dictionary<TypeAttributes, ReferenceSymbolType> subDict = _refTypes[baseType];

			if (!subDict.ContainsKey(attribs))
				subDict.Add(attribs, new ReferenceSymbolType(baseType, attribs));

			return subDict[attribs];
		}

		public static NullableSymbolType NullableType(SymbolType baseType, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_nullTypes.ContainsKey(baseType))
				_nullTypes.Add(baseType, new Dictionary<TypeAttributes, NullableSymbolType>());

			Dictionary<TypeAttributes, NullableSymbolType> subDict = _nullTypes[baseType];

			if (!subDict.ContainsKey(attribs))
				subDict.Add(attribs, new NullableSymbolType(baseType, attribs));

			return subDict[attribs];
		}

		public static VariadicSymbolType VariadicType(SymbolType baseType, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_variadicTypes.ContainsKey(baseType))
				_variadicTypes.Add(baseType, new Dictionary<TypeAttributes, VariadicSymbolType>());

			Dictionary<TypeAttributes, VariadicSymbolType> subDict = _variadicTypes[baseType];

			if (!subDict.ContainsKey(attribs))
				subDict.Add(attribs, new VariadicSymbolType(baseType, attribs));

			return subDict[attribs];
		}

		// TODO: Differentiating between array types depending on the size expression can cause major issues later
		public static ArraySymbolType ArrayType(SymbolType baseType, SimpleExpressionSyntax sizeExpr, ulong arrSize, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_arrayTypes.ContainsKey(baseType))
				_arrayTypes.Add(baseType, new Dictionary<TypeAttributes, Dictionary<SimpleExpressionSyntax, ArraySymbolType>>());

			Dictionary<TypeAttributes, Dictionary<SimpleExpressionSyntax, ArraySymbolType>> subDict0 = _arrayTypes[baseType];

			if (!subDict0.ContainsKey(attribs))
				subDict0.Add(attribs, new Dictionary<SimpleExpressionSyntax, ArraySymbolType>());

			Dictionary<SimpleExpressionSyntax, ArraySymbolType> subDict1 = subDict0[attribs];

			SimpleExpressionSyntax key = sizeExpr ?? Nothing<SimpleExpressionSyntax>.Value;
			if (!subDict1.ContainsKey(key))
				subDict1.Add(key, new ArraySymbolType(baseType, sizeExpr, arrSize, attribs));

			return subDict1[key];
		}

		public static TupleSymbolType TupleType(List<SymbolType> subTypes, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_tupleTypes.ContainsKey(subTypes.Count))
				_tupleTypes.Add(subTypes.Count, new List<TupleSymbolType>());

			List<TupleSymbolType> subList = _tupleTypes[subTypes.Count];

			foreach (TupleSymbolType tupleType in subList)
			{
				if (tupleType.Attributes != attribs)
					continue;

				bool found = true;
				for (var index = 0; index < tupleType.SubTypes.Count; index++)
				{
					SymbolType subType0 = tupleType.SubTypes[index];
					SymbolType subType1 = subTypes[index];

					if (subType0 != subType1)
					{
						found = false;
						break;
					}
				}

				if (found)
					return tupleType;
			}

			TupleSymbolType tmpType = new TupleSymbolType(subTypes, attribs);
			subList.Add(tmpType);
			return tmpType;
		}
		
		public static FunctionSymbolType FunctionType(SymbolType receiverType, List<SymbolType> paramTypes, SymbolType returnType, TypeAttributes attribs = TypeAttributes.None)
		{
			int paramCount = paramTypes?.Count ?? 0;
			if (!_funcTypes.ContainsKey(paramCount))
				_funcTypes.Add(paramCount, new List<FunctionSymbolType>());

			List<FunctionSymbolType> subList = _funcTypes[paramCount];

			foreach (FunctionSymbolType funcType in subList)
			{
				if (funcType.Attributes != attribs || 
				    funcType.ReturnType != returnType || 
				    funcType.ReceiverType != receiverType)
					continue;

				if (paramCount != 0)
				{
					bool found = true;
					for (var i = 0; i < funcType.ParamTypes.Count; i++)
					{
						SymbolType paramType0 = funcType.ParamTypes[i];
						SymbolType paramType1 = paramTypes[i];

						if (paramType0 != paramType1)
						{
							found = false;
							break;
						}
					}

					if (found)
						return funcType;
				}
				else
				{
					return funcType;
				}
			}

			FunctionSymbolType tmpType = new FunctionSymbolType(receiverType, paramTypes, returnType, attribs);
			subList.Add(tmpType);
			return tmpType;
		}

		public static DelegateSymbolType DelegateType(ScopeVariable scopeVar, List<SymbolType> paramTypes, SymbolType returnType, TypeAttributes attribs = TypeAttributes.None)
		{
			return DelegateType(scopeVar, paramTypes, returnType, null, attribs);
		}

		protected static DelegateSymbolType DelegateType(ScopeVariable scopeVar, List<SymbolType> paramTypes, SymbolType returnType, Symbol symbol, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_delTypes.ContainsKey(scopeVar.Scope))
				_delTypes.Add(scopeVar.Scope, new Dictionary<string, Dictionary<TypeAttributes, DelegateSymbolType>>());

			Dictionary<string, Dictionary<TypeAttributes, DelegateSymbolType>> subDict0 = _delTypes[scopeVar.Scope];

			string name = scopeVar.Name.GetNonTemplateName();
			if (!subDict0.ContainsKey(name))
				subDict0.Add(name, new Dictionary<TypeAttributes, DelegateSymbolType>());

			Dictionary<TypeAttributes, DelegateSymbolType> subDict1 = subDict0[name];

			foreach (KeyValuePair<TypeAttributes, DelegateSymbolType> pair in subDict1)
			{
				if (pair.Value.ParamTypes == null && paramTypes != null)
					pair.Value.ParamTypes = paramTypes;
				if (pair.Value.ReturnType == null && returnType != null)
					pair.Value.ReturnType = returnType;
			}

			if (subDict1.TryGetValue(attribs, out DelegateSymbolType delType))
			{
				return delType;
			}

			DelegateSymbolType tmpType = new DelegateSymbolType(scopeVar, paramTypes, returnType, symbol, attribs);
			subDict1.Add(attribs, tmpType);
			return tmpType;
		}

		public static DelegateSymbolType DelegateType(ScopeVariable scopeVar, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_delTypes.ContainsKey(scopeVar.Scope))
				_delTypes.Add(scopeVar.Scope, new Dictionary<string, Dictionary<TypeAttributes, DelegateSymbolType>>());

			Dictionary<string, Dictionary<TypeAttributes, DelegateSymbolType>> subDict0 = _delTypes[scopeVar.Scope];

			string name = scopeVar.Name.GetNonTemplateName();
			if (!subDict0.ContainsKey(name))
				subDict0.Add(name, new Dictionary<TypeAttributes, DelegateSymbolType>());

			Dictionary<TypeAttributes, DelegateSymbolType> subDict1 = subDict0[name];

			if (subDict1.TryGetValue(attribs, out DelegateSymbolType delType))
				return delType;

			DelegateSymbolType tmpType = new DelegateSymbolType(scopeVar, null, null, attribs);
			subDict1.Add(attribs, tmpType);
			return tmpType;
		}

		public static MemoryLocSymbolType MemoryLocType(SymbolType baseType, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_memLocTypes.ContainsKey(baseType))
				_memLocTypes.Add(baseType, new Dictionary<TypeAttributes, MemoryLocSymbolType>());

			Dictionary<TypeAttributes, MemoryLocSymbolType> subDict = _memLocTypes[baseType];

			if (!subDict.ContainsKey(attribs))
				subDict.Add(attribs, new MemoryLocSymbolType(baseType, attribs));

			return subDict[attribs];
		}

		public static EnumSymbolType EnumType(ScopeVariable scopeVar, BuiltinSymbolType baseType, TypeAttributes attribs = TypeAttributes.None)
		{
			return EnumType(scopeVar, baseType, null, attribs);
		}

		protected static EnumSymbolType EnumType(ScopeVariable scopeVar, BuiltinSymbolType baseType, Symbol symbol, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_enumTypes.ContainsKey(scopeVar.Scope))
				_enumTypes.Add(scopeVar.Scope, new Dictionary<string, Dictionary<TypeAttributes, EnumSymbolType>>());

			Dictionary<string, Dictionary<TypeAttributes, EnumSymbolType>> subDict0 = _enumTypes[scopeVar.Scope];

			string name = scopeVar.Name.GetNonTemplateName();
			if (!subDict0.ContainsKey(name))
				subDict0.Add(name, new Dictionary<TypeAttributes, EnumSymbolType>());

			Dictionary<TypeAttributes, EnumSymbolType> subDict1 = subDict0[name];

			if (!subDict1.ContainsKey(attribs))
				subDict1.Add(attribs, new EnumSymbolType(scopeVar, baseType, symbol, attribs));

			return subDict1[attribs];
		}

		public static AggregateSymbolType AggregateType(ScopeVariable scopeVar, AggregateTypes type, TypeAttributes attribs = TypeAttributes.None)
		{
			return AggregateType(scopeVar, type, null, attribs);
		}

		protected static AggregateSymbolType AggregateType(ScopeVariable scopeVar, AggregateTypes type, Symbol symbol, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_aggrTypes.ContainsKey(scopeVar.Scope))
				_aggrTypes.Add(scopeVar.Scope, new Dictionary<string, Dictionary<TypeAttributes, AggregateSymbolType>>());

			Dictionary<string, Dictionary<TypeAttributes, AggregateSymbolType>> subDict0 = _aggrTypes[scopeVar.Scope];

			string name = scopeVar.Name.GetNonTemplateName();
			if (!subDict0.ContainsKey(name))
				subDict0.Add(name, new Dictionary<TypeAttributes, AggregateSymbolType>());

			Dictionary<TypeAttributes, AggregateSymbolType> subDict1 = subDict0[name];

			if (!subDict1.ContainsKey(attribs))
				subDict1.Add(attribs, new AggregateSymbolType(scopeVar, type, symbol, attribs));

			return subDict1[attribs];
		}

		public static InterfaceSymbolType InterfaceType(ScopeVariable scopeVar, TypeAttributes attribs = TypeAttributes.None)
		{
			return InterfaceType(scopeVar, null, attribs);
		}

		static InterfaceSymbolType InterfaceType(ScopeVariable scopeVar, Symbol symbol, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_interfaceTypes.ContainsKey(scopeVar.Scope))
				_interfaceTypes.Add(scopeVar.Scope, new Dictionary<string, Dictionary<TypeAttributes, InterfaceSymbolType>>());

			Dictionary<string, Dictionary<TypeAttributes, InterfaceSymbolType>> subDict0 = _interfaceTypes[scopeVar.Scope];

			string name = scopeVar.Name.GetNonTemplateName();
			if (!subDict0.ContainsKey(name))
				subDict0.Add(name, new Dictionary<TypeAttributes, InterfaceSymbolType>());

			Dictionary<TypeAttributes, InterfaceSymbolType> subDict1 = subDict0[name];

			if (!subDict1.ContainsKey(attribs))
				subDict1.Add(attribs, new InterfaceSymbolType(scopeVar, symbol, attribs));

			return subDict1[attribs];
		}

		public static SelfSymbolType SelfType(Scope scope, Identifier identifier, TypeAttributes attribs = TypeAttributes.None)
		{
			return SelfType(new ScopeVariable(scope, identifier), attribs);
		}

		public static SelfSymbolType SelfType(ScopeVariable scopeVar, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_selfTypes.ContainsKey(scopeVar.Scope))
				_selfTypes.Add(scopeVar.Scope, new Dictionary<string, Dictionary<TypeAttributes, SelfSymbolType>>());

			Dictionary<string, Dictionary <TypeAttributes, SelfSymbolType>> subDict0 = _selfTypes[scopeVar.Scope];

			string name = scopeVar.Name.GetNonTemplateName();
			if (!subDict0.ContainsKey(name))
				subDict0.Add(name, new Dictionary<TypeAttributes, SelfSymbolType>());

			Dictionary<TypeAttributes, SelfSymbolType> subDict1 = subDict0[name];

			if (!subDict1.ContainsKey(attribs))
				subDict1.Add(attribs, new SelfSymbolType(scopeVar, attribs));

			return subDict1[attribs];
		}

		public static BitFieldSymbolType BitFieldType(SymbolType baseType, byte bits, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_bitfieldTypes.ContainsKey(baseType))
				_bitfieldTypes.Add(baseType, new Dictionary<TypeAttributes, Dictionary<byte, BitFieldSymbolType>>());

			Dictionary<TypeAttributes, Dictionary<byte, BitFieldSymbolType>> subDict = _bitfieldTypes[baseType];

			if (!subDict.ContainsKey(attribs))
				subDict.Add(attribs, new Dictionary<byte, BitFieldSymbolType>());

			Dictionary<byte, BitFieldSymbolType> subDict2 = subDict[attribs];

			if (!subDict2.ContainsKey(bits))
				subDict2.Add(bits, new BitFieldSymbolType(baseType, bits, attribs));

			return subDict2[bits];
		}
		
		public static UnknownSymbolType UnknownType(Scope scope, Identifier identifier, TypeAttributes attribs = TypeAttributes.None)
		{
			return UnknownType(new ScopeVariable(scope, identifier), attribs);
		}

		public static UnknownSymbolType UnknownType(ScopeVariable scopeVar, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_unknownTypes.ContainsKey(scopeVar.Scope))
				_unknownTypes.Add(scopeVar.Scope, new Dictionary<string, Dictionary<TypeAttributes, UnknownSymbolType>>());

			Dictionary<string, Dictionary<TypeAttributes, UnknownSymbolType>> subDict0 = _unknownTypes[scopeVar.Scope];

			string name = scopeVar.Name.GetNonTemplateName();
			if (!subDict0.ContainsKey(name))
				subDict0.Add(name, new Dictionary<TypeAttributes, UnknownSymbolType>());

			Dictionary<TypeAttributes, UnknownSymbolType> subDict1 = subDict0[name];

			if (!subDict1.ContainsKey(attribs))
				subDict1.Add(attribs, new UnknownSymbolType(scopeVar, attribs));

			return subDict1[attribs];
		}

		public static TemplateParamSymbolType TemplateParamType(Scope scope, Identifier iden, int index, TypeAttributes attribs = TypeAttributes.None)
		{
			return TemplateParamType(new ScopeVariable(scope, iden), index, attribs);
		}

		public static TemplateParamSymbolType TemplateParamType(ScopeVariable scopeVar, int index, TypeAttributes attribs = TypeAttributes.None)
		{
			if (!_templateParamTypes.ContainsKey(scopeVar.Scope))
				_templateParamTypes.Add(scopeVar.Scope, new Dictionary<string, Dictionary<int, Dictionary<TypeAttributes, TemplateParamSymbolType>>>());

			Dictionary<string, Dictionary<int, Dictionary<TypeAttributes, TemplateParamSymbolType>>> subDict0 = _templateParamTypes[scopeVar.Scope];

			string name = scopeVar.Name.GetNonTemplateName();
			if (!subDict0.ContainsKey(name))
				subDict0.Add(name, new Dictionary<int, Dictionary<TypeAttributes, TemplateParamSymbolType>>());

			Dictionary<int, Dictionary<TypeAttributes, TemplateParamSymbolType>> subDict1 = subDict0[name];
			if (!subDict1.ContainsKey(index))
				subDict1.Add(index, new Dictionary<TypeAttributes, TemplateParamSymbolType>());

			Dictionary<TypeAttributes, TemplateParamSymbolType> subDict2 = subDict1[index];

			if (!subDict2.ContainsKey(attribs))
				subDict2.Add(attribs, new TemplateParamSymbolType(scopeVar, index, attribs));

			return subDict2[attribs];
		}

		public static TemplateInstanceSymbolType TemplateInstanceType(Scope scope, Identifier iden, Symbol templateSym, TypeAttributes attribs = TypeAttributes.None)
		{
			return TemplateInstanceType(new ScopeVariable(scope, iden), templateSym, attribs);
		}

		public static TemplateInstanceSymbolType TemplateInstanceType(ScopeVariable scopeVar, Symbol templateSym, TypeAttributes attribs = TypeAttributes.None)
		{
			Symbol symbol = templateSym ?? Nothing<Symbol>.Value;
			if (!_templateInstanceTypes.ContainsKey(symbol))
				_templateInstanceTypes.Add(symbol, new Dictionary<Scope, Dictionary<Identifier, Dictionary<TypeAttributes, TemplateInstanceSymbolType>>>());

			Dictionary<Scope, Dictionary<Identifier, Dictionary<TypeAttributes, TemplateInstanceSymbolType>>> subDict0 = _templateInstanceTypes[symbol];

			if (!subDict0.ContainsKey(scopeVar.Scope))
				subDict0.Add(scopeVar.Scope, new Dictionary<Identifier, Dictionary<TypeAttributes, TemplateInstanceSymbolType>>());

			Dictionary<Identifier, Dictionary<TypeAttributes, TemplateInstanceSymbolType>> subDict1 = subDict0[scopeVar.Scope];

			Identifier iden = scopeVar.Name;
			if (!subDict1.ContainsKey(iden))
				subDict1.Add(iden, new Dictionary<TypeAttributes, TemplateInstanceSymbolType>());

			Dictionary<TypeAttributes, TemplateInstanceSymbolType> subDict2 = subDict1[iden];

			if (!subDict2.ContainsKey(attribs))
				subDict2.Add(attribs, new TemplateInstanceSymbolType(scopeVar, templateSym, attribs));

			return subDict2[attribs];
		}

		public virtual bool HasBuiltinConvertion(SymbolType to)
		{
			return false;
		}

		public virtual SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			return this;
		}

		public virtual SymbolType GetBaseType()
		{
			return null;
		}

		public virtual Identifier GetIdentifier()
		{
			return null;
		}

		public SymbolType GetInnerType()
		{
			SymbolType curType = this;
			SymbolType baseType = GetBaseType();

			while (baseType != null)
			{
				curType = baseType;
				baseType = curType.GetBaseType();
			}

			return curType;
		}

		public List<T> GetSubTypesOf<T>() where T : SymbolType
		{
			List<T> subTypes = new List<T>();
			if (this is T tmpT)
			{
				subTypes.Add(tmpT);
			}
			
			SymbolType baseType = GetBaseType();

			while (baseType != null)
			{
				if (baseType is T)

				if (baseType is FunctionSymbolType funcType)
				{
					List<T> tmp = funcType.ReceiverType?.GetSubTypesOf<T>();
					if (tmp != null)
						subTypes.AddRange(tmp);

					tmp = funcType.ReturnType?.GetSubTypesOf<T>();
					if (tmp != null)
						subTypes.AddRange(tmp);

					if (funcType.ParamTypes != null)
					{
						foreach (SymbolType paramType in funcType.ParamTypes)
						{
							tmp = paramType.GetSubTypesOf<T>();
							if (tmp != null)
								subTypes.AddRange(tmp);
						}
					}
				}
				else if (baseType is TupleSymbolType tupleType)
				{
					foreach (SymbolType paramType in tupleType.SubTypes)
					{
						List<T> tmp = paramType.GetSubTypesOf<T>();
						if (tmp != null)
							subTypes.AddRange(tmp);
					}
					}
				baseType = baseType.GetBaseType();
			}

			return subTypes;
		}

		public virtual string ToILString()
		{
			return ToString();
		}

		protected static SymbolType UpdateType(SymbolType type, Scope implScope, Scope scope, CompilerContext table)
		{
			if (type is TypeofSymbolType typeofSyntax)
			{
				return AttributedType(typeofSyntax.Identifier.Context.Type, type.Attributes);
			}
			else if (type is UnknownSymbolType unknownType)
			{
				Symbol sym = null;

				// If defined inside of an impl block, check for impl template params for
				if (implScope != null)
					sym = table.FindType(implScope, unknownType.Identifier);

				if (sym == null)
					sym = table.FindType(scope, unknownType.Identifier);

				return AttributedType(sym.Type, type.Attributes);
			}
			else if (type is SelfSymbolType selfType)
			{
				Symbol sym = null;

				if (implScope != null)
					sym = table.FindType(implScope, selfType.Identifier);

				if (sym == null)
					sym = table.FindType(scope, selfType.Identifier);
				return AttributedType(sym.Type, type.Attributes);
			}
			else
				return type.GetUpdatedType(implScope, scope, table);
		}

		public string GetAttributeString()
		{
			string str = "";
			if ((Attributes & TypeAttributes.Const) != 0)
				str += "const ";
			if ((Attributes & TypeAttributes.Immutable) != 0)
				str += "immutable";
			if ((Attributes & TypeAttributes.Mutable) != 0)
				str += "mutable";

			return str;
		}

	}

	public class BuiltinSymbolType : SymbolType
	{
		public BuiltinTypes Builtin;

		public BuiltinSymbolType(BuiltinTypes type, TypeAttributes attribs)
		{
			Builtin = type;
			Attributes = attribs;
		}

		public override string ToString()
		{
			if (Builtin == BuiltinTypes.Any)
				return $"{GetAttributeString()}Any";

			return $"{GetAttributeString()}{Builtin.ToString().ToLower()}";
		}

		public override Identifier GetIdentifier()
		{
			return new NameIdentifier(ToString());
		}

		public int GetNumBytes()
		{
			switch (Builtin)
			{
			case BuiltinTypes.Bool:
			case BuiltinTypes.I8:
			case BuiltinTypes.U8:
			case BuiltinTypes.Char:
				return 1;
			case BuiltinTypes.I16:
			case BuiltinTypes.U16:
			case BuiltinTypes.WChar:
				return 2;
			case BuiltinTypes.I32:
			case BuiltinTypes.U32:
			case BuiltinTypes.F32:
			case BuiltinTypes.Rune:
				return 4;
			case BuiltinTypes.U64:
			case BuiltinTypes.I64:
			case BuiltinTypes.F64:
				return 8;
			case BuiltinTypes.ISize:
			case BuiltinTypes.USize:
			case BuiltinTypes.String:
				return -1;
			default:
				return 0;
			}
		}

		public BuiltinTypes GetBaseBuiltinType()
		{
			switch (Builtin)
			{
			case BuiltinTypes.Bool:
				return BuiltinTypes.Bool;
			case BuiltinTypes.String:
				return BuiltinTypes.String;
			case BuiltinTypes.Char:
			case BuiltinTypes.WChar:
			case BuiltinTypes.Rune:
				return BuiltinTypes.Char;
			case BuiltinTypes.U8:
			case BuiltinTypes.U16:
			case BuiltinTypes.U32:
			case BuiltinTypes.U64:
				return BuiltinTypes.U8;
			case BuiltinTypes.F32:
			case BuiltinTypes.F64:
				return BuiltinTypes.F32;
			case BuiltinTypes.ISize:
			case BuiltinTypes.USize:
				return Builtin;
			default:
				return BuiltinTypes.I8;
			}
		}

		public override bool HasBuiltinConvertion(SymbolType to)
		{
			// TODO: Incomplete
			if (to is BuiltinSymbolType toType)
			{
				BuiltinTypes baseType = GetBaseBuiltinType();
				BuiltinTypes toBaseType = toType.GetBaseBuiltinType();

				if (baseType == toBaseType)
					return true;
				if (toBaseType == BuiltinTypes.String)
				{
					return baseType == BuiltinTypes.Char || baseType == BuiltinTypes.String;
				}

				if (toBaseType == BuiltinTypes.Char)
					return false;

				return baseType != BuiltinTypes.Bool;
			}
			else if (to is NullableSymbolType nullable)
			{
				if (Builtin == BuiltinTypes.Null)
					return true;
			}
			else if (to is PointerSymbolType ptr)
			{
				if (Builtin == BuiltinTypes.Null)
					return true;

				if (Builtin == BuiltinTypes.StringLiteral)
				{
					if (ptr.BaseType is BuiltinSymbolType toBaseType)
					{
						return toBaseType.Builtin == BuiltinTypes.Char ||
						       toBaseType.Builtin == BuiltinTypes.WChar ||
						       toBaseType.Builtin == BuiltinTypes.Rune;
					}
				}
			}

			return base.HasBuiltinConvertion(to);
		}

		public SymbolType GetCommonType(BuiltinSymbolType other)
		{
			BuiltinTypes baseType = GetBaseBuiltinType();
			BuiltinTypes toBaseType = other.GetBaseBuiltinType();

			int bytes = Math.Max(GetNumBytes(), other.GetNumBytes());

			if (baseType == BuiltinTypes.F32 || toBaseType == BuiltinTypes.F32)
			{
				return SymbolType.BuiltinType(bytes, BuiltinTypes.F32);
			}
			if (baseType == BuiltinTypes.USize || toBaseType == BuiltinTypes.USize)
			{
				if (bytes == 8 && !CmdLine.IsX64)
					return SymbolType.BuiltinType(8, BuiltinTypes.U8);
				return SymbolType.BuiltinType(BuiltinTypes.USize);
			}
			if (baseType == BuiltinTypes.U8 || toBaseType == BuiltinTypes.U8)
			{
				return SymbolType.BuiltinType(bytes, BuiltinTypes.U8);
			}
			if (baseType == BuiltinTypes.Char || toBaseType == BuiltinTypes.Char)
			{
				return SymbolType.BuiltinType(bytes, BuiltinTypes.Char);
			}
			if (baseType == BuiltinTypes.ISize || toBaseType == BuiltinTypes.ISize)
			{
				if (bytes == 8 && !CmdLine.IsX64)
					return SymbolType.BuiltinType(8, BuiltinTypes.I8);
				return SymbolType.BuiltinType(BuiltinTypes.USize);
			}
			return SymbolType.BuiltinType(bytes, BuiltinTypes.I8);
		}
	}

	public class PointerSymbolType : SymbolType
	{
		public SymbolType BaseType;

		public PointerSymbolType(SymbolType baseType, TypeAttributes attribs)
		{
			BaseType = baseType;
			Attributes = attribs;
		}

		public override string ToString()
		{
			return $"{GetAttributeString()}*{BaseType}";
		}

		public override string ToILString()
		{
			return $"{GetAttributeString()}*{BaseType.ToILString()}";
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			SymbolType baseType = UpdateType(BaseType, implScope, scope, context);
			return SymbolType.PointerType(baseType, Attributes);
		}

		public override SymbolType GetBaseType()
		{
			return BaseType;
		}
	}

	public class ReferenceSymbolType : SymbolType
	{
		public SymbolType BaseType;

		public ReferenceSymbolType(SymbolType baseType, TypeAttributes attribs)
		{
			BaseType = baseType;
			Attributes = attribs;
		}

		public override string ToString()
		{
			return $"{GetAttributeString()}&{BaseType}";
		}

		public override string ToILString()
		{
			return $"{GetAttributeString()}&{BaseType.ToILString()}";
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			SymbolType baseType = UpdateType(BaseType, implScope, scope, context);
			return SymbolType.ReferenceType(baseType, Attributes);
		}

		public override SymbolType GetBaseType()
		{
			return BaseType;
		}
	}

	public class NullableSymbolType : SymbolType
	{
		public SymbolType BaseType;

		public NullableSymbolType(SymbolType baseType, TypeAttributes attribs)
		{
			BaseType = baseType;
			Attributes = attribs;
		}

		public override string ToString()
		{
			return $"{GetAttributeString()}?{BaseType}";
		}

		public override string ToILString()
		{
			return $"{GetAttributeString()}?{BaseType.ToILString()}";
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			SymbolType baseType = UpdateType(BaseType, implScope, scope, context);
			return SymbolType.NullableType(baseType, Attributes);
		}

		public override SymbolType GetBaseType()
		{
			return BaseType;
		}
	}

	public class VariadicSymbolType : SymbolType
	{
		public SymbolType BaseType;

		public VariadicSymbolType(SymbolType baseType, TypeAttributes attribs)
		{
			BaseType = baseType;
			Attributes = attribs;
		}

		public override string ToString()
		{
			return $"{GetAttributeString()}{BaseType}...";
		}

		public override string ToILString()
		{
			return $"{GetAttributeString()}{BaseType.ToILString()}...";
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			SymbolType baseType = UpdateType(BaseType, implScope, scope, context);
			return SymbolType.VariadicType(baseType, Attributes);
		}

		public override SymbolType GetBaseType()
		{
			return BaseType;
		}
	}

	public class ArraySymbolType : SymbolType
	{
		public SymbolType BaseType;

		public SimpleExpressionSyntax SizeExpr;
		public string ArraySizeFunc;
		public ulong ArraySize = ulong.MaxValue;

		public ArraySymbolType(SymbolType baseType, SimpleExpressionSyntax sizeExpr, ulong arrSize, TypeAttributes attribs)
		{
			BaseType = baseType;
			SizeExpr = sizeExpr;
			ArraySize = arrSize;
			Attributes = attribs;
		}

		public override string ToString()
		{
			if (ArraySize == ulong.MaxValue)
				return $"{GetAttributeString()}[]{BaseType}";
			return $"{GetAttributeString()}[{ArraySize}]{BaseType}";
		}

		public override string ToILString()
		{
			if (ArraySize == ulong.MaxValue)
				return $"{GetAttributeString()}[]{BaseType.ToILString()}";
			return $"{GetAttributeString()}[{ArraySize}]{BaseType.ToILString()}";
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			SymbolType baseType = UpdateType(BaseType, implScope, scope, context);
			return SymbolType.ArrayType(baseType, SizeExpr, ArraySize, Attributes);
		}

		public override SymbolType GetBaseType()
		{
			return BaseType;
		}
	}

	public class TupleSymbolType : SymbolType
	{
		public List<SymbolType> SubTypes = new List<SymbolType>();

		public TupleSymbolType()
		{
		}

		public TupleSymbolType(List<SymbolType> subTypes, TypeAttributes attribs)
		{
			SubTypes = subTypes;
			Attributes = attribs;
		}

		public override string ToString()
		{
			string str = null;
			foreach (SymbolType type in SubTypes)
			{
				if (str == null)
					str = $"({type}";
				else
					str += $", {type}";
			}
			return GetAttributeString() + str + ')';
		}

		public override string ToILString()
		{
			string str = null;
			foreach (SymbolType type in SubTypes)
			{
				if (str == null)
					str = $"({type.ToILString()}";
				else
					str += $", {type.ToILString()}";
			}
			return str + ')';
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			List<SymbolType> subTypes = new List<SymbolType>(SubTypes.Count);
			for (var i = 0; i < SubTypes.Count; i++)
			{
				SymbolType baseType = UpdateType(SubTypes[i], implScope, scope, context);
				subTypes.Add(baseType);
			}

			return SymbolType.TupleType(subTypes, Attributes);
		}
	}

	public class FunctionSymbolType : SymbolType
	{
		public SymbolType ReceiverType = null;
		public SymbolType ReturnType = null;
		public List<SymbolType> ParamTypes = null;
		public BitArray HasDefaultValues = null;

		public FunctionSymbolType(SymbolType receiverType, List<SymbolType> paramTypes, SymbolType returnType, TypeAttributes attribs)
		{
			ReceiverType = receiverType;
			ParamTypes = paramTypes;
			ReturnType = returnType;
			Attributes = attribs;
		}

		public override string ToString()
		{
			string paramStr = null;

			if (ParamTypes != null)
			{
				foreach (SymbolType type in ParamTypes)
				{
					if (paramStr == null)
						paramStr = $"{type}";
					else
						paramStr += $", {type}";
				}
			}


			string str = "";
			if (ReceiverType != null)
				str += $"({ReceiverType})";

			str += $"({paramStr}) -> ";

			if (ReturnType is TupleSymbolType)
				return str + $"{ReturnType}";

			return GetAttributeString() + str + $"({ReturnType})";
		}

		public override string ToILString()
		{
			string paramStr = null;

			if (ParamTypes != null)
			{
				foreach (SymbolType type in ParamTypes)
				{
					if (paramStr == null)
						paramStr = $"{type.ToILString()}";
					else
						paramStr += $", {type.ToILString()}";
				}
			}

			string str = "";
			if (ReceiverType != null)
				str += $"({ReceiverType.ToILString()})";

			str += $"({paramStr}) -> ";

			if (ReturnType is TupleSymbolType)
				return str + $"{ReturnType.ToILString()}";

			return GetAttributeString() + str + $"({ReturnType?.ToILString()})";
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			SymbolType receiverType = ReceiverType != null ? UpdateType(ReceiverType, implScope, scope, context) : null;
			SymbolType returnType = ReturnType != null ? UpdateType(ReturnType, implScope, scope, context) : null;
			List<SymbolType> paramTypes = new List<SymbolType>();

			if (ParamTypes != null)
			{
				for (var i = 0; i < ParamTypes.Count; i++)
				{
					SymbolType paramType = UpdateType(ParamTypes[i], implScope, scope, context);
					paramTypes.Add(paramType);
				}
			}

			return SymbolType.FunctionType(receiverType, paramTypes, returnType, Attributes);
		}
	}

	public class DelegateSymbolType : SymbolType
	{
		public ScopeVariable Identifier;
		public SymbolType ReturnType = null;
		public List<SymbolType> ParamTypes = null;
		public Symbol Symbol;

		public DelegateSymbolType(ScopeVariable iden, List<SymbolType> paramTypes, SymbolType returnType, TypeAttributes attribs)
		{
			Identifier = iden;
			ParamTypes = paramTypes;
			ReturnType = returnType;
			Attributes = attribs;
		}

		public DelegateSymbolType(ScopeVariable iden, List<SymbolType> paramTypes, SymbolType returnType, Symbol symbol, TypeAttributes attribs)
		{
			Identifier = iden;
			ParamTypes = paramTypes;
			ReturnType = returnType;
			Attributes = attribs;
		}

		public string MangledIdentifier
		{
			get { return Symbol?.MangledName ?? ""; }
		}

		public override string ToString()
		{
			string paramStr = null;

			if (ParamTypes != null)
			{
				foreach (SymbolType type in ParamTypes)
				{
					if (paramStr == null)
						paramStr = $"{type}";
					else
						paramStr += $", {type}";
				}
			}


			string str = "";
			if (Identifier.ToString() != "")
				str += Identifier.ToString() + ' ';

			str += $"({paramStr}) -> ";

			if (ReturnType is TupleSymbolType)
				return str + $"{ReturnType}";

			return GetAttributeString() + str + $"({ReturnType})";
		}

		public override string ToILString()
		{
			return MangledIdentifier;
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			SymbolType returnType = UpdateType(ReturnType, implScope, scope, context);
			List<SymbolType> paramTypes = new List<SymbolType>();

			if (ParamTypes != null)
			{
				for (var i = 0; i < ParamTypes.Count; i++)
				{
					SymbolType paramType = UpdateType(ReturnType, implScope, scope, context);
					paramTypes.Add(paramType);
				}
			}

			// TODO
			if (Identifier.ToString() != "" && Symbol == null)
			{
				Symbol = context.FindType(scope, Identifier);
			}

			return SymbolType.DelegateType(Identifier, paramTypes, returnType, Symbol, Attributes);
		}
	}

	public class TypeofSymbolType : SymbolType
	{
		public SimpleExpressionSyntax Identifier;

		public override string ToString()
		{
			return $"{GetAttributeString()}typeof({Identifier})";
		}
	}

	public class MemoryLocSymbolType : SymbolType
	{
		public SymbolType BaseType;

		public MemoryLocSymbolType(SymbolType baseType, TypeAttributes attribs)
		{
			BaseType = baseType;
			Attributes = attribs;
		}

		public override string ToString()
		{
			return $"{GetAttributeString()}^{BaseType}";
		}

		public override string ToILString()
		{
			return $"{GetAttributeString()}^{BaseType.ToILString()}";
		}

		public override SymbolType GetBaseType()
		{
			return BaseType;
		}
	}

	public class EnumSymbolType : SymbolType
	{
		public ScopeVariable Identifier;
		public BuiltinSymbolType BaseType;
		public Symbol Symbol;
		public bool IsSimpleEnum;

		public string MangledIdentifier
		{
			get { return Symbol?.MangledName ?? ""; }
		}

		public EnumSymbolType(ScopeVariable identifier, BuiltinSymbolType baseType, TypeAttributes attribs)
		{
			Identifier = identifier;
			BaseType = baseType;
			Attributes = attribs;
		}

		public EnumSymbolType(ScopeVariable identifier, BuiltinSymbolType baseType, Symbol symbol, TypeAttributes attribs)
		{
			Identifier = identifier;
			BaseType = baseType;
			Attributes = attribs;
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			if (Symbol == null)
			{
				Symbol = context.FindType(scope, Identifier);
			}
			return this;
		}

		public void UpdateSimpleEnum()
		{
			IsSimpleEnum = true;
			foreach (Symbol child in Symbol.Children)
			{
				if (child.Kind == SymbolKind.EnumMember &&
				    child.Type != null)
				{
					IsSimpleEnum = false;
					break;
				}
			}
		}

		public override string ToString()
		{
			return GetAttributeString() + Identifier.ToString();
		}

		public override string ToILString()
		{
			return GetAttributeString() + MangledIdentifier;
		}

		public override Identifier GetIdentifier()
		{
			return Identifier.Name;
		}
	}

	public enum AggregateTypes
	{
		Unknown,
		Struct,
		Union
	}

	public class AggregateSymbolType : SymbolType
	{
		public ScopeVariable Identifier;
		public AggregateTypes Type;
		private Symbol _symbol;

		public Symbol Symbol
		{
			get { return _symbol; }
			set
			{
				_symbol = value;
				switch (_symbol.Kind)
				{
				case SymbolKind.Struct: Type = AggregateTypes.Struct;	break;
				case SymbolKind.Union:	Type = AggregateTypes.Union;	break;
				}
			}
		}

		public string MangledIdentifier
		{
			get { return _symbol?.MangledName ?? ""; }
		}

		public AggregateSymbolType(ScopeVariable identifier, AggregateTypes type, Symbol symbol, TypeAttributes attribs)
		{
			Identifier = identifier;
			Type = type;
			Attributes = attribs;

			if (symbol != null)
			{
				_symbol = symbol;
				switch (_symbol.Kind)
				{
				case SymbolKind.Struct: Type = AggregateTypes.Struct; break;
				case SymbolKind.Union: Type = AggregateTypes.Union; break;
				}
			}
		}

		public override string ToString()
		{
			return GetAttributeString() + Identifier.ToString();
		}

		public override string ToILString()
		{
			return MangledIdentifier;
		}

		public override Identifier GetIdentifier()
		{
			return Identifier.Name;
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			Symbol = context.FindDefinition(scope, Identifier, -1);
			return this;
		}
	}

	public class InterfaceSymbolType : SymbolType
	{
		public ScopeVariable Identifier;
		public Symbol Symbol;

		public string MangledIdentifier
		{
			get { return Symbol?.MangledName ?? ""; }
		}

		public InterfaceSymbolType(ScopeVariable identifier, TypeAttributes attribs)
		{
			Identifier = identifier;
			Attributes = attribs;
		}

		public InterfaceSymbolType(ScopeVariable identifier, Symbol symbol, TypeAttributes attribs)
		{
			Identifier = identifier;
			Attributes = attribs;
			Symbol = symbol;
		}

		public override string ToString()
		{
			return GetAttributeString() + Identifier.ToString();
		}

		public override string ToILString()
		{
			return MangledIdentifier;
		}

		public override Identifier GetIdentifier()
		{
			return Identifier.Name;
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			Symbol = context.FindDefinition(scope, Identifier, -1);
			return this;
		}
	}

	public class UnknownSymbolType : SymbolType
	{
		public ScopeVariable Identifier;

		public UnknownSymbolType(ScopeVariable identifier, TypeAttributes attribs = TypeAttributes.None)
		{
			Identifier = identifier;
			Attributes = attribs;
		}

		public override string ToString()
		{
			return $"{GetAttributeString()}__unknown__{Identifier}";
		}

		public override Identifier GetIdentifier()
		{
			return Identifier.Name;
		}

		public static bool operator ==(UnknownSymbolType type0, UnknownSymbolType type1)
		{
			if (ReferenceEquals(null, type0) || ReferenceEquals(type1, null))
				return ReferenceEquals(null, type0) && ReferenceEquals(type1, null);

			return type0.Identifier == type1.Identifier;
		}

		public static bool operator !=(UnknownSymbolType type0, UnknownSymbolType type1)
		{
			return !(type0 == type1);
		}

		protected bool Equals(UnknownSymbolType other)
		{
			return base.Equals(other) && string.Equals(Identifier, other.Identifier);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((UnknownSymbolType)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (base.GetHashCode() * 397) ^ (Identifier != null ? Identifier.GetHashCode() : 0);
			}
		}
	}

	public class SelfSymbolType : SymbolType
	{
		public ScopeVariable Identifier;

		public SelfSymbolType(ScopeVariable iden, TypeAttributes attribs)
		{
			Identifier = iden;
			Attributes = attribs;
		}

		public override string ToString()
		{
			return GetAttributeString() + "Self";
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			Symbol symbol = context.FindType(scope, Identifier);
			return symbol?.Type ?? this;
		}
	}

	// TODO: Make bitfields part of attributes
	public class BitFieldSymbolType : SymbolType
	{
		public SymbolType BaseType;
		public byte Bits;

		public BitFieldSymbolType(SymbolType baseType, byte bits, TypeAttributes attribs)
		{
			BaseType = baseType;
			Attributes = attribs;
		}

		public override string ToString()
		{
			return $"{GetAttributeString()}{BaseType}.{Bits}";
		}

		public override string ToILString()
		{
			return $"{GetAttributeString()}{BaseType.ToILString()}.{Bits}";
		}

		public override SymbolType GetBaseType()
		{
			return BaseType;
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			SymbolType baseType = UpdateType(BaseType, implScope, scope, context);
			return SymbolType.BitFieldType(baseType, Bits, Attributes);
		}
	}

	public class TemplateParamSymbolType : SymbolType
	{
		public ScopeVariable Iden;
		public int Index;

		public TemplateParamSymbolType(ScopeVariable iden, int index, TypeAttributes attribs)
		{
			Iden = iden;
			Index = index;
			Attributes = attribs;
		}

		public override string ToString()
		{
			return $"{GetAttributeString()}{Iden.Name}";
		}

		public override string ToILString()
		{
			return $"{GetAttributeString()}{Iden.Name}";
		}

		public override Identifier GetIdentifier()
		{
			return Iden.Name;
		}
	}

	public class TemplateInstanceSymbolType : SymbolType
	{
		public ScopeVariable Iden;
		public Symbol TemplateSymbol;
		public string MangledName;

		public TemplateInstanceSymbolType(ScopeVariable iden, Symbol templateSym, TypeAttributes attribs)
		{
			Iden = iden;
			Attributes = attribs;
			TemplateSymbol = templateSym;
		}

		public override string ToString()
		{
			return GetAttributeString() + Iden.ToString();
		}

		public override string ToILString()
		{
			if (MangledName != null)
				return MangledName;

			string name = $"{GetAttributeString()}{TemplateSymbol.MangledName}!<";

			TemplateInstanceIdentifier instIden = Iden.Name as TemplateInstanceIdentifier;
			for (var i = 0; i < instIden.Arguments.Count; i++)
			{
				TemplateArgument arg = instIden.Arguments[i];

				if (i != 0)
					name += ", ";

				if (arg.Value != null)
				{
					name += $"%{arg.Value} : ${arg.Type.ToILString()}";
				}
				else if(arg.ValueFunc != null)
				{
					name += $"@{arg.ValueFunc} : ${arg.Type.ToILString()}";
				}
				else
				{
					name += '$' + arg.Type.ToILString();
				}
			}

			return GetAttributeString() + name + '>';
		}

		public override Identifier GetIdentifier()
		{
			return Iden.Name;
		}

		public override SymbolType GetUpdatedType(Scope implScope, Scope scope, CompilerContext context)
		{
			if (TemplateSymbol != null)
				return this;

			Symbol templateSym = context.FindTemplate(scope, Iden);
			return TemplateInstanceType(Iden, templateSym, Attributes);
		}
	}
}
