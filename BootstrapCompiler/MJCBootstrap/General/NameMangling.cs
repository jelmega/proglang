﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.SyntaxTree;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC.General
{
	// Name mangling is independent from the compiler and can be changed with another implementation, depending on future ABI changes

	public static class NameMangling
	{
		// TODO: Add backtracking
		public static string Mangle(Symbol symbol)
		{
			if (symbol.MangledName != null)
				return symbol.MangledName;

			if (symbol.Identifier is NameIdentifier idenName && 
			    idenName.Iden == "main" && 
			    symbol.Kind == SymbolKind.Function)
				return "_main";

			string mangled = MangleName(symbol);

			switch (symbol.Kind)
			{
			case SymbolKind.Struct:
				mangled += "TS";
				break;
			case SymbolKind.Interface:
				mangled += "TI";
				break;
			case SymbolKind.Union:
				mangled += "TU";
				break;
			case SymbolKind.Enum:
				mangled += "TE";
				mangled += MangleType((symbol.Type as EnumSymbolType).BaseType);
				break;
			case SymbolKind.Delegate:
				mangled += "TD";
				break;
			case SymbolKind.Variable:
				mangled += MangleType(symbol.Type, true);
				break;
			case SymbolKind.EnumMember:
			{
				if (symbol.Type != null)
					mangled += MangleType(symbol.Type);
				break;
			}
			case SymbolKind.Typedef:
				mangled += "TT";
				mangled += MangleType(symbol.Type);
				break;
			case SymbolKind.TypeAlias:
			{
				mangled += "TL";
				if (symbol.Type != null)
					mangled += MangleType(symbol.Type);
				break;
			}
			case SymbolKind.Function:
			case SymbolKind.Method:
				mangled += MangleFunctionType(symbol.Type as FunctionSymbolType);
				break;
			case SymbolKind.TemplateInstance:
				mangled += "TJ";
				break;
			case SymbolKind.TemplateParam:
			{
				mangled += "TK";
				mangled += symbol.TemplateIdx.ToString();

				if (!(symbol.Type is TemplateParamSymbolType))
				{
					mangled += '_';
					mangled += MangleType(symbol.Type);
				}
				break;
			}
			}

			return mangled;
		}

		public static string MangleName(Symbol symbol)
		{
			Scope scope = symbol.Scope;
			string mangled = "_M";
			foreach (Identifier name in scope.Names)
			{
				mangled += MangleIdentifier(name);
			}

			return mangled + MangleIdentifier(symbol.Identifier);
		}

		public static string MangleScopeVar(ScopeVariable scopeVar)
		{
			Scope scope = scopeVar.Scope;
			string mangled = "";
			foreach (Identifier name in scope.Names)
			{
				mangled += MangleIdentifier(name);
			}

			return mangled + MangleIdentifier(scopeVar.Name);
		}

		public static string MangleIdentifier(Identifier iden)
		{
			if (iden is NameIdentifier idenName)
			{
				return $"{idenName.Iden.Length}{idenName.Iden}";
			}
			else if (iden is TemplateInstanceIdentifier inst)
			{
				string mangled = $"__I{inst.Iden.Length}{inst.Iden}{inst.Arguments.Count}";
				foreach (TemplateArgument arg in inst.Arguments)
				{
					if (arg.Value == null)
					{
						mangled += 'T';
						mangled += MangleType(arg.Type);
					}
					else
					{
						mangled += 'V';
						mangled += MangleType(arg.Type);
						mangled += arg.Value.Length;
						mangled += '_';
						mangled += arg.Value;
					}
					mangled += 'Z';
				} 
				return mangled;
			}
			else if (iden is TemplateDefinitionIdentifier def)
			{
				string mangled = $"__T{def.Iden.Length}{def.Iden}{def.Parameters.Count}";
				foreach (TemplateParameter param in def.Parameters)
				{
					if (param.ValueName == null)
					{
						mangled += "T" + MangleType(param.Type, templateParentIden: def);
						if (param.TypeSpecialization != null)
							mangled += 'G' + MangleType(param.TypeSpecialization, templateParentIden: def);
						if (param.TypeDefault != null)
							mangled += 'H' + MangleType(param.TypeDefault);
					}
					else
					{
						mangled += $"V{param.ValueName.Length}{param.ValueName}" + MangleType(param.Type, templateParentIden: def);
						if (param.ValueSpecialization != null)
							mangled += $"G{param.ValueSpecialization.Length}_{param.ValueSpecialization}";
						if (param.ValueDefault != null)
							mangled += $"H{param.ValueDefault.Length}_{param.ValueDefault}";
					}
					mangled += 'Z';
				}
				return mangled;
			}

			return "";
		}

		private static string MangleFunctionType(FunctionSymbolType type)
		{
			string mangled = "F";

			// TODO: Calling convention
			mangled += 'm'; // <- Temp

			// TODO: Function attributes


			if (type.ReceiverType != null)
			{
				mangled += 'O';
				mangled += MangleType(type.ReceiverType);
			}

			if (type.ParamTypes != null && type.ParamTypes.Count > 0)
			{
				foreach (SymbolType param in type.ParamTypes)
				{
					// TODO: parameter attributes

					mangled += MangleType(param);
				}
			}

			SymbolType last = type.ParamTypes?.LastOrDefault();
			if (last is VariadicSymbolType)
				mangled += 'Y';
			else
				mangled += 'Z';

			if (type.ReturnType != null)
				mangled += MangleType(type.ReturnType);

			return mangled;
		}

		public static string MangleType(SymbolType type, bool isValue = false, Identifier templateParentIden = null)
		{
			string mangled = isValue ? "V" : "";

			if ((type.Attributes & TypeAttributes.Const) != 0)
				mangled += "Nc";
			if ((type.Attributes & TypeAttributes.Immutable) != 0)
				mangled += "Ni";
			if (((type.Attributes & TypeAttributes.Mutable) != 0))
				mangled += "Nm";

			switch (type)
			{
			case FunctionSymbolType functionType:
			{
				mangled += MangleFunctionType(functionType);
				break;
			}
			case ArraySymbolType arrayType:
			{
				mangled += 'A';
				mangled += MangleType(arrayType.BaseType, templateParentIden: templateParentIden);
				break;
			}
			case PointerSymbolType pointerType:
			{
				mangled += 'P';
				mangled += MangleType(pointerType.BaseType, templateParentIden: templateParentIden);
				break;
			}
			case ReferenceSymbolType refType:
			{
				mangled += 'R';
				mangled += MangleType(refType.BaseType, templateParentIden: templateParentIden);
				break;
			}
			case TupleSymbolType tupleType:
			{
				mangled += 'M';
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					mangled += MangleType(subType, templateParentIden: templateParentIden);
				}
				mangled += 'Z';
				break;
			}
			case BuiltinSymbolType builtinType:
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.Bool:
					mangled += 'b';
					break;
				case BuiltinTypes.I8:
					mangled += 'g';
					break;
				case BuiltinTypes.I16:
					mangled += 's';
					break;
				case BuiltinTypes.I32:
					mangled += 'i';
					break;
				case BuiltinTypes.I64:
					mangled += 'l';
					break;
				case BuiltinTypes.U8:
					mangled += 'h';
					break;
				case BuiltinTypes.U16:
					mangled += 't';
					break;
				case BuiltinTypes.U32:
					mangled += 'j';
					break;
				case BuiltinTypes.U64:
					mangled += 'm';
					break;
				case BuiltinTypes.F32:
					mangled += 'f';
					break;
				case BuiltinTypes.F64:
					mangled += 'd';
					break;
				case BuiltinTypes.Char:
					mangled += 'c';
					break;
				case BuiltinTypes.WChar:
					mangled += 'w';
					break;
				case BuiltinTypes.Rune:
					mangled += 'r';
					break;
				case BuiltinTypes.String:
				case BuiltinTypes.StringLiteral:
					mangled += 'u';
					break;
				case BuiltinTypes.Void:
					mangled += 'v';
					break;
				}

				break;
			}
			case AggregateSymbolType aggregateType:
			{
				switch (aggregateType.Type)
				{
				case AggregateTypes.Struct:
					mangled += 'S';
					break;
				case AggregateTypes.Union:
					mangled += 'U';
					break;
				}

				if (string.IsNullOrEmpty(aggregateType.MangledIdentifier))
				{
					Symbol aggregateSym = aggregateType.Symbol;
					aggregateSym.MangledName = Mangle(aggregateSym);
				}

				string mangleIden = aggregateType.MangledIdentifier;
				mangled += mangleIden.Remove(mangleIden.Length - 2);
				break;
			}
			case InterfaceSymbolType interfaceType:
			{
				mangled += 'I';

				if (string.IsNullOrEmpty(interfaceType.MangledIdentifier))
				{
					Symbol interfaceSym = interfaceType.Symbol;
					interfaceSym.MangledName = Mangle(interfaceSym);
				}

				string mangleIden = interfaceType.MangledIdentifier;
				mangled += mangleIden.Remove(mangleIden.Length - 2);
				break;
			}
			case EnumSymbolType enumType:
			{
				mangled += 'E';

				if (string.IsNullOrEmpty(enumType.MangledIdentifier))
				{
					Symbol aggregateSym = enumType.Symbol;
					aggregateSym.MangledName = Mangle(aggregateSym);
				}

				string mangleIden = enumType.MangledIdentifier;
				mangled += mangleIden.Remove(mangleIden.Length - 2);
				break;
			}
			case MemoryLocSymbolType memoryLoc:
				// A MemoryLoc is interpreted as a pointer for the name mangling
				mangled += 'P' + MangleType(memoryLoc.BaseType, templateParentIden: templateParentIden);
				break;
			case DelegateSymbolType delegateType:
			{
				mangled += 'D';
				string mangleIden = delegateType.MangledIdentifier;
				mangled += mangleIden.Remove(mangleIden.Length - 2);
				break;
			}
			case BitFieldSymbolType bitfield:
			{
				mangled += 'B';
				mangled += MangleType(bitfield.BaseType, templateParentIden: templateParentIden);
				mangled += bitfield.Bits.ToString();
				mangled += 'Z';
				break;
			}
			case TemplateInstanceSymbolType instType:
			{
				mangled += "J";
				mangled += MangleScopeVar(instType.Iden);
				break;
			}
			case TemplateParamSymbolType paramType:
			{
				mangled += 'K';
				mangled += paramType.Index.ToString();
				mangled += '_';

				ScopeVariable scopeVar = paramType.Iden;
				if (templateParentIden != scopeVar.Scope.LastSubScope)
					mangled += MangleScopeVar(scopeVar);
				else
					mangled += MangleIdentifier(scopeVar.Name);
				break;
			}
			}

			return mangled;
		}


		public static void Demangle(string mangled, out Scope scope, out Symbol symbol)
		{
			scope = null;
			symbol = null;

			if (!mangled.StartsWith("_M"))
				return;

			// demangle name
			int pos = 2;
			ScopeVariable scopeVar = DemangleScopeVariable(mangled, ref pos);

			// Demangle type
			(SymbolType type, SymbolKind kind) = DemangleType(mangled, ref pos, scopeVar);

			// TODO: attribs
			symbol = new Symbol(scopeVar.Scope, null, scopeVar.Name, kind, SemanticVisibility.Default, SemanticAttributes.None);
			symbol.Type = type;
			symbol.MangledName = mangled;

			if (type is AggregateSymbolType aggrType && aggrType.Symbol == null)
				aggrType.Symbol = symbol;

			if (symbol.Kind == SymbolKind.TemplateParam)
			{
				ScopeVariable paramScopeVar = new ScopeVariable(scopeVar.Scope);
				TemplateDefinitionIdentifier defIden = paramScopeVar.Name as TemplateDefinitionIdentifier;

				if (type is TemplateParamSymbolType paramType)
				{
					symbol.TemplateIdx = paramType.Index;
				}
				else
				{
					NameIdentifier paramName = scopeVar.Name as NameIdentifier;
					TemplateParameter param = defIden.Parameters.Find(p => { return p.ValueName == paramName.Iden; });
					symbol.TemplateIdx = param.Index;
				}
			}

			scope = scopeVar.Scope;
		}

		public static ScopeVariable DemangleScopeVariable(string mangled, ref int pos)
		{
			if (mangled.ExistsAtLoc(pos, "_M"))
				pos += 2;

			Identifier iden = null;
			List<Identifier> scopeNames = new List<Identifier>();
			while (pos < mangled.Length && (char.IsDigit(mangled[pos]) || mangled[pos] == '_'))
			{
				if (iden != null)
					scopeNames.Add(iden);
				iden = DemangleIdentifier(mangled, ref pos);
			}
			if (scopeNames.Count > 0)
				return new ScopeVariable(new Scope(scopeNames), iden);
			return new ScopeVariable(iden);
		}

		public static Identifier DemangleIdentifier(string mangled, ref int offset)
		{
			if (mangled.ExistsAtLoc(offset, "_M"))
				offset += 2;

			if (mangled.ExistsAtLoc(offset, "__I"))
			{
				offset += 3;
				int len = GetElemLength(mangled, ref offset);
				string name = mangled.Substring(offset, len);
				offset += len;
				
				int numArgs = GetElemLength(mangled, ref offset);
				TemplateInstanceIdentifier iden = new TemplateInstanceIdentifier(name, numArgs);
				for (int i = 0; i < numArgs; i++)
				{
					char c = mangled[offset];
					++offset;
					if (c == 'V')
					{
						(SymbolType type, _) = DemangleType(mangled, ref offset);

						int valLen = GetElemLength(mangled, ref offset);
						++offset; // skip '_'
						string value = mangled.Substring(offset, valLen);
						offset += valLen;

						TemplateArgument arg = new TemplateArgument(i, type, value);
						iden.Arguments[i] = arg;
						++offset; // skip Z
					}
					else if (c == 'T')
					{
						(SymbolType type, _) = DemangleType(mangled, ref offset);
						TemplateArgument arg = new TemplateArgument(i, type);
						iden.Arguments[i] = arg;
						++offset; // skip Z
					}
				}

				return iden;
			}
			else if (mangled.ExistsAtLoc(offset, "__T"))
			{
				offset += 3;
				int len = GetElemLength(mangled, ref offset);
				string name = mangled.Substring(offset, len);
				offset += len;

				int numParams = GetElemLength(mangled, ref offset);
				TemplateDefinitionIdentifier iden = new TemplateDefinitionIdentifier(name, numParams);
				for (int i = 0; i < numParams; i++)
				{
					char c = mangled[offset];
					++offset;
					if (c == 'V')
					{
						int valLen = GetElemLength(mangled, ref offset);
						string paramName = mangled.Substring(offset, valLen);
						offset += valLen;

						(SymbolType type, _) = DemangleType(mangled, ref offset);

						string valueSepcialization = null;
						if (mangled[offset] == 'G')
						{
							++offset;
							valLen = GetElemLength(mangled, ref offset);
							++offset; // skip '_'
							valueSepcialization = mangled.Substring(offset, valLen);
							offset += valLen;
						}

						string valueDefault = null;
						if (mangled[offset] == 'H')
						{
							++offset;
							valLen = GetElemLength(mangled, ref offset);
							++offset; // skip '_'
							valueDefault = mangled.Substring(offset, valLen);
							offset += valLen;
						}

						++offset; // skip Z
						TemplateParameter param = new TemplateParameter(paramName, type, i, valueSepcialization, valueDefault);
						iden.Parameters[i] = param;
					}
					else if (c == 'T')
					{
						(SymbolType type, _) = DemangleType(mangled, ref offset);

						SymbolType typeSpecialization = null;
						if (mangled[offset] == 'G')
						{
							++offset;
							(typeSpecialization, _) = DemangleType(mangled, ref offset);
						}

						SymbolType typeDefault = null;
						if (mangled[offset] == 'H')
						{
							++offset;
							(typeDefault, _) = DemangleType(mangled, ref offset);
						}

						++offset; // skip Z
						TemplateParameter param = new TemplateParameter(type, i, typeSpecialization, typeDefault);
						iden.Parameters[i] = param;
					}
				}

				return iden;
			}
			else
			{
				int len = GetElemLength(mangled, ref offset);
				string name = mangled.Substring(offset, len);
				offset += len;

				return new NameIdentifier(name);
			}
		}

		public static int GetElemLength(string mangled, ref int offset)
		{
			string count = "";
			while (offset < mangled.Length && Char.IsDigit(mangled[offset]))
			{
				count += mangled[offset];
				++offset;
			}
			return Int32.Parse(count);
		}

		public static SymbolType DemangleType(string mangled)
		{
			int dummy = 0;
			(SymbolType type, _) = DemangleType(mangled, ref dummy);
			return type;
		}

		public static (SymbolType, SymbolKind) DemangleType(string mangled, ref int pos, ScopeVariable symScopeVar = null)
		{
			// Special case: enum member
			if (pos == mangled.Length)
				return (null, SymbolKind.EnumMember);

			if (pos == -1)
			{
				int idx = 2;
				while (true)
				{
					string count = "";
					while (Char.IsDigit(mangled[idx]))
					{
						count += mangled[idx++];
					}
					if (count.Length == 0)
						break;

					int len = Int32.Parse(count);

					idx += len;
				}
				pos = idx;
			}

			TypeAttributes typeAttribs = TypeAttributes.None;
			while (mangled[pos] == 'N')
			{
				++pos;
				switch (mangled[pos])
				{
				case 'c': typeAttribs |= TypeAttributes.Const; break;
				case 'i': typeAttribs |= TypeAttributes.Immutable; break;
				}

				++pos;
			}
			
			switch (mangled[pos])
			{
			case 'F':
			{
				++pos;
				char callConv = mangled[pos];
				++pos;

				SymbolType recieverType = null;
				if (mangled[pos] == 'O')
				{
					++pos;
					(recieverType, _) = DemangleType(mangled, ref pos);
				}

				List<SymbolType> argTypes = IsMangleParamClose(mangled[pos]) ? null : new List<SymbolType>();
				while (!IsMangleParamClose(mangled[pos]))
				{
					(SymbolType type, _) = DemangleType(mangled, ref pos);
					argTypes.Add(type);
				}
				++pos;

				SymbolType retType = null;
				if (pos < mangled.Length)
				{
					(retType, _) = DemangleType(mangled, ref pos);
				}

				return (SymbolType.FunctionType(recieverType, argTypes, retType, typeAttribs), recieverType == null ? SymbolKind.Function : SymbolKind.Method);
			}
			case 'V':
			{
				++pos;
				(SymbolType resType, _) = DemangleType(mangled, ref pos);
				return (resType, SymbolKind.Variable);
			}
			case 'A':
			{
				++pos;
				(SymbolType subType, _) = DemangleType(mangled, ref pos);
				return (SymbolType.ArrayType(subType, null /*TODO*/, ulong.MaxValue /*TODO*/, typeAttribs), SymbolKind.Unknown);
			}
			case 'P':
			{
				++pos;
				(SymbolType subType, _) = DemangleType(mangled, ref pos);
				return (SymbolType.PointerType(subType, typeAttribs), SymbolKind.Unknown);
			}
			case 'R':
			{
				++pos;
				(SymbolType subType, _) = DemangleType(mangled, ref pos);
				return (SymbolType.ReferenceType(subType, typeAttribs), SymbolKind.Unknown);
			}
			case 'M':
			{
				++pos;
				List<SymbolType> subTypes = new List<SymbolType>();
				while (mangled[pos] != 'Z')
				{
					(SymbolType subType, _) = DemangleType(mangled, ref pos);
					subTypes.Add(subType);
				}
				++pos;
				return (SymbolType.TupleType(subTypes, typeAttribs), SymbolKind.Unknown);
			}
			case 'O':
			{
				++pos;
				if (pos == mangled.Length)
					return (null, SymbolKind.Receiver);
				(SymbolType receiverType, _) = DemangleType(mangled, ref pos);
				return (receiverType, SymbolKind.Receiver);
			}
			case 'S':
			{
				++pos;
				if (pos == mangled.Length)
					return (null, SymbolKind.Struct);
				ScopeVariable scopeVar = DemangleScopeVariable(mangled, ref pos);
				return (SymbolType.AggregateType(scopeVar, AggregateTypes.Struct, typeAttribs), SymbolKind.Struct);
			}
			case 'I':
			{
				++pos;
				if (pos == mangled.Length)
					return (null, SymbolKind.Interface);
				ScopeVariable scopeVar = DemangleScopeVariable(mangled, ref pos);
				return (SymbolType.InterfaceType(scopeVar, typeAttribs), SymbolKind.Interface);
			}
			case 'U':
			{
				++pos;
				if (pos == mangled.Length)
					return (null, SymbolKind.Union);
				ScopeVariable scopeVar = DemangleScopeVariable(mangled, ref pos);
				return (SymbolType.AggregateType(scopeVar, AggregateTypes.Union, typeAttribs), SymbolKind.Union);
			}
			case 'E':
			{
				++pos;
				if (pos == mangled.Length)
					return (null, SymbolKind.Enum);
				ScopeVariable scopeVar = DemangleScopeVariable(mangled, ref pos);
				// TODO: BaseType
				return (SymbolType.EnumType(scopeVar, null, typeAttribs), SymbolKind.Enum);
			}
			case 'D':
			{
				++pos;
				if (pos == mangled.Length)
					return (null, SymbolKind.Delegate);
				ScopeVariable scopeVar = DemangleScopeVariable(mangled, ref pos);
				return (SymbolType.DelegateType(scopeVar, typeAttribs), SymbolKind.Delegate);
			}
			case 'B':
			{
				++pos;
				(SymbolType baseType, _) = DemangleType(mangled, ref pos);
				string bitsStr = "";
				while (mangled[pos] != 'Z')
				{
					bitsStr += mangled[pos];
					++pos;
				}
				byte bits = byte.Parse(bitsStr);
				return (SymbolType.BitFieldType(baseType, bits, typeAttribs), SymbolKind.Unknown);
			}
			case 'J':
			{
				++pos;
				ScopeVariable scopeVar = DemangleScopeVariable(mangled, ref pos);
				// passing null for the templateSym, will use Nothing<Symbol>.Value
				return (SymbolType.TemplateInstanceType(scopeVar, null, typeAttribs), SymbolKind.TemplateInstance);
			}
			case 'K':
			{
				++pos;
				int paramIdx = GetElemLength(mangled, ref pos);
				++pos; // skip _
				ScopeVariable scopeVar = DemangleScopeVariable(mangled, ref pos);
				return (SymbolType.TemplateParamType(scopeVar, paramIdx, typeAttribs), SymbolKind.TemplateParam);
			}
			case 'T':
			{
				++pos;
				switch (mangled[pos])
				{
				case 'S':
				{
					++pos;
					AggregateSymbolType type = SymbolType.AggregateType(symScopeVar, AggregateTypes.Struct);
					return (type, SymbolKind.Struct);
				}
				case 'U':
				{
					++pos;
					AggregateSymbolType type = SymbolType.AggregateType(symScopeVar, AggregateTypes.Union);
					return (type, SymbolKind.Union);
				}
				case 'I':
				{
					// TODO
					++pos;
					return (null, SymbolKind.Interface);
				}
				case 'E':
				{
					++pos;
					(SymbolType baseType, _) = DemangleType(mangled, ref pos);
					EnumSymbolType rype = SymbolType.EnumType(symScopeVar, baseType as BuiltinSymbolType);
					return (rype, SymbolKind.Enum);
				}
				case 'D':
				{
					// TODO
					++pos;
					return (null, SymbolKind.Delegate);
				}
				case 'J':
				{
					// TODO
					++pos;
					return (null, SymbolKind.TemplateInstance);
				}
				case 'K':
				{
					++pos;
					int idx = GetElemLength(mangled, ref pos);
					SymbolType type;
					if (mangled.Length > pos)
					{
						++pos;
						(type, _) = DemangleType(mangled, ref pos);
					}
					else
					{
						type = SymbolType.TemplateParamType(symScopeVar, idx);
					}
					return (type, SymbolKind.TemplateParam);
				}

				case 'L':
				{
					SymbolType type = null;
					if (pos == mangled.Length)
						(type, _) = DemangleType(mangled, ref pos);
					return (type, SymbolKind.TypeAlias);
				}
				default:
					return (null, SymbolKind.Unknown);
				}
			}
			default:
			{
				BuiltinSymbolType builtin = GetBuiltin(mangled[pos], typeAttribs);
				++pos;
				return (builtin, SymbolKind.Unknown);
			}
			}
		}


		private static bool IsMangleParamClose(char c)
		{
			return c == 'Z' || c == 'X' || c == 'Y';
		}

		private static BuiltinSymbolType GetBuiltin(char c, TypeAttributes attribs)
		{
			switch (c)
			{
				case 'v': return SymbolType.BuiltinType(BuiltinTypes.Void  , attribs);
				case 'g': return SymbolType.BuiltinType(BuiltinTypes.I8    , attribs);
				case 'h': return SymbolType.BuiltinType(BuiltinTypes.U8    , attribs);
				case 's': return SymbolType.BuiltinType(BuiltinTypes.I16   , attribs);
				case 't': return SymbolType.BuiltinType(BuiltinTypes.U16   , attribs);
				case 'i': return SymbolType.BuiltinType(BuiltinTypes.I32   , attribs);
				case 'j': return SymbolType.BuiltinType(BuiltinTypes.U32   , attribs);
				case 'l': return SymbolType.BuiltinType(BuiltinTypes.I64   , attribs);
				case 'm': return SymbolType.BuiltinType(BuiltinTypes.U64   , attribs);
				case 'f': return SymbolType.BuiltinType(BuiltinTypes.F32   , attribs);
				case 'd': return SymbolType.BuiltinType(BuiltinTypes.F64   , attribs);
				case 'b': return SymbolType.BuiltinType(BuiltinTypes.Bool  , attribs);
				case 'c': return SymbolType.BuiltinType(BuiltinTypes.Char  , attribs);
				case 'w': return SymbolType.BuiltinType(BuiltinTypes.WChar , attribs);
				case 'r': return SymbolType.BuiltinType(BuiltinTypes.Rune  , attribs);
				case 'u': return SymbolType.BuiltinType(BuiltinTypes.String, attribs);
				case 'n': return SymbolType.BuiltinType(BuiltinTypes.Null  , attribs);
				default: return null;
			}
		}


	}
}
