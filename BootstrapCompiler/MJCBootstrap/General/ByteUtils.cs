﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace MJC.General
{
	public static class ByteUtils
	{
		// Write

		public static bool WriteToBuffer(byte[] buffer, int pos, byte value)
		{
			if (pos >= buffer.Length)
				return false;

			buffer[pos] = value;
			return true;
		}

		public static bool WriteToBuffer(byte[] buffer, int pos, short value)
		{
			if (pos + 2 > buffer.Length)
				return false;

			byte[] tmp = BitConverter.GetBytes(value);

			buffer[pos + 0] = tmp[0];
			buffer[pos + 1] = tmp[1];
			return true;
		}

		public static bool WriteToBuffer(byte[] buffer, int pos, ushort value)
		{
			if (pos + 2 > buffer.Length)
				return false;

			byte[] tmp = BitConverter.GetBytes(value);

			buffer[pos + 0] = tmp[0];
			buffer[pos + 1] = tmp[1];
			return true;
		}

		public static bool WriteToBuffer(byte[] buffer, int pos, int value)
		{
			if (pos + 4 > buffer.Length)
				return false;

			byte[] tmp = BitConverter.GetBytes(value);

			buffer[pos + 0] = tmp[0];
			buffer[pos + 1] = tmp[1];
			buffer[pos + 2] = tmp[2];
			buffer[pos + 3] = tmp[3];
			return true;
		}

		public static bool WriteToBuffer(byte[] buffer, int pos, uint value)
		{
			if (pos + 4 > buffer.Length)
				return false;

			byte[] tmp = BitConverter.GetBytes(value);

			buffer[pos + 0] = tmp[0];
			buffer[pos + 1] = tmp[1];
			buffer[pos + 2] = tmp[2];
			buffer[pos + 3] = tmp[3];
			return true;
		}

		public static bool WriteToBuffer(byte[] buffer, int pos, long value)
		{
			if (pos + 8 > buffer.Length)
				return false;

			byte[] tmp = BitConverter.GetBytes(value);

			buffer[pos + 0] = tmp[0];
			buffer[pos + 1] = tmp[1];
			buffer[pos + 2] = tmp[2];
			buffer[pos + 3] = tmp[3];
			buffer[pos + 4] = tmp[4];
			buffer[pos + 5] = tmp[5];
			buffer[pos + 6] = tmp[6];
			buffer[pos + 7] = tmp[7];
			return true;
		}

		public static bool WriteToBuffer(byte[] buffer, int pos, ulong value)
		{
			if (pos + 8 > buffer.Length)
				return false;

			byte[] tmp = BitConverter.GetBytes(value);

			buffer[pos + 0] = tmp[0];
			buffer[pos + 1] = tmp[1];
			buffer[pos + 2] = tmp[2];
			buffer[pos + 3] = tmp[3];
			buffer[pos + 4] = tmp[4];
			buffer[pos + 5] = tmp[5];
			buffer[pos + 6] = tmp[6];
			buffer[pos + 7] = tmp[7];
			return true;
		}

		public static bool WriteToBuffer(byte[] buffer, int pos, byte[] value)
		{
			if (pos + value.Length >= buffer.Length)
				return false;

			Array.Copy(value, 0, buffer, pos, value.Length);
			return true;
		}

		// Read
		public static bool ReadFromBuffer(byte[] buffer, int pos, out byte value)
		{
			value = 0;
			if (pos >= buffer.Length)
				return false;

			value = buffer[pos];
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, ref int pos, out byte value)
		{
			value = 0;
			if (pos >= buffer.Length)
				return false;

			value = buffer[pos];
			++pos;
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, int pos, out short value)
		{
			value = 0;
			if (pos + 2 > buffer.Length)
				return false;

			value = BitConverter.ToInt16(buffer, pos);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, ref int pos, out short value)
		{
			value = 0;
			if (pos + 2 > buffer.Length)
				return false;

			value = BitConverter.ToInt16(buffer, pos);
			pos += sizeof(short);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, int pos, out ushort value)
		{
			value = 0;
			if (pos + 2 > buffer.Length)
				return false;

			value = BitConverter.ToUInt16(buffer, pos);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, ref int pos, out ushort value)
		{
			value = 0;
			if (pos + 2 > buffer.Length)
				return false;

			value = BitConverter.ToUInt16(buffer, pos);
			pos += sizeof(ushort);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, int pos, out int value)
		{
			value = 0;
			if (pos + 4 > buffer.Length)
				return false;

			value = BitConverter.ToInt32(buffer, pos);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, ref int pos, out int value)
		{
			value = 0;
			if (pos + 4 > buffer.Length)
				return false;

			value = BitConverter.ToInt32(buffer, pos);
			pos += sizeof(int);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, int pos, out uint value)
		{
			value = 0;
			if (pos + 4 > buffer.Length)
				return false;

			value = BitConverter.ToUInt32(buffer, pos);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, ref int pos, out uint value)
		{
			value = 0;
			if (pos + 4 > buffer.Length)
				return false;

			value = BitConverter.ToUInt32(buffer, pos);
			pos += sizeof(uint);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, int pos, out long value)
		{
			value = 0;
			if (pos + 8 > buffer.Length)
				return false;

			value = BitConverter.ToInt64(buffer, pos);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, ref int pos, out long value)
		{
			value = 0;
			if (pos + 8 > buffer.Length)
				return false;

			value = BitConverter.ToInt64(buffer, pos);
			pos += sizeof(long);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, int pos, out ulong value)
		{
			value = 0;
			if (pos + 8 > buffer.Length)
				return false;

			value = BitConverter.ToUInt64(buffer, pos);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, ref int pos, out ulong value)
		{
			value = 0;
			if (pos + 8 > buffer.Length)
				return false;

			value = BitConverter.ToUInt64(buffer, pos);
			pos += sizeof(ulong);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, int pos, int length, out byte[] value)
		{
			value = null;
			if (pos + length > buffer.Length)
				return false;

			value = new byte[length];
			Array.Copy(buffer, pos, value, 0, length);
			return true;
		}

		public static bool ReadFromBuffer(byte[] buffer, ref int pos, int length, out byte[] value)
		{
			value = null;
			if (pos + length > buffer.Length)
				return false;

			value = new byte[length];
			Array.Copy(buffer, pos, value, 0, length);
			pos += length;
			return true;
		}

		// Struct
		public static bool WriteToBuffer<T>(byte[] buffer, int pos, T value) where T : struct
		{
			int size = Marshal.SizeOf<T>();
			if (pos + size > buffer.Length)
				return false;

			IntPtr ptr = Marshal.AllocHGlobal(size);
			Marshal.StructureToPtr(value, ptr, true);
			Marshal.Copy(ptr, buffer, pos, size);
			Marshal.FreeHGlobal(ptr);

			return true;
		}

		public static bool WriteNullTerminatedCStr(byte[] buffer, int pos, string str)
		{
			int size = str.Length + 1;
			if (pos + size > buffer.Length)
				return false;

			byte[] bytes = Encoding.ASCII.GetBytes(str);
			Array.Copy(bytes, 0, buffer, pos, str.Length);
			buffer[pos + str.Length] = 0;

			return true;
		}

		public static bool ReadFromBuffer<T>(byte[] buffer, int pos, out T value, int size = -1) where T : struct
		{
			value = new T();
			size = size == -1 ? Marshal.SizeOf<T>() : size;
			if (pos + size > buffer.Length)
				return false;

			IntPtr ptr = Marshal.AllocHGlobal(size);
			Marshal.Copy(buffer, pos, ptr, size);
			value = (T)Marshal.PtrToStructure(ptr, value.GetType());
			Marshal.FreeHGlobal(ptr);

			return true;
		}

		public static bool ReadNullTerminatedCStr(byte[] buffer, int pos, out string str)
		{
			str = "";

			if (pos > buffer.Length)
				return false;

			// Find end
			int end = Array.IndexOf(buffer, (byte)0, pos);
			if (end == -1)
				return false;

			int len = end - pos;
			str = Encoding.ASCII.GetString(buffer, pos, len);

			return true;
		}

		public static bool ReadNullTerminatedCStr(byte[] buffer, ref int pos, out string str)
		{
			str = "";

			if (pos > buffer.Length)
				return false;

			// Find end
			int end = Array.IndexOf(buffer, (byte)0, pos);
			if (end == -1)
				return false;

			int len = end - pos;
			str = Encoding.ASCII.GetString(buffer, pos, len);

			pos = end + 1;
			return true;
		}

		public static byte[] ToBytes(string str)
		{
			return Encoding.ASCII.GetBytes(str);
		}



		public static void WriteNullTerminatedCStr(BinaryWriter writer, string str)
		{
			writer.Write(Encoding.ASCII.GetBytes(str));
			writer.Write((byte)0);
		}
	}
}
