﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MJC.General
{
	public struct LineInfo
	{
		public string Text;
		public int LineNum;
		public TextSpan Span;

		public LineInfo(string text, int lineNum, TextSpan fullspan)
		{
			Text = text;
			LineNum = lineNum;
			Span = fullspan;
		}
	}

	public class ErrorSystemMetadata
	{
		public string SourceFile;
		public string Source;
		public List<LineInfo> Lines = new List<LineInfo>();

		public ErrorSystemMetadata(string sourceFile, string source)
		{
			SourceFile = sourceFile;
			Source = source;

			char[] separators = { '\r', '\n' };
			int idx = Source.IndexOfAny(separators);
			int prevIdx = 0;
			int lineNum = 1;
			while (idx != -1)
			{
				if (idx < Source.Length - 1 && Source[idx] == '\r' && Source[idx + 1] == '\n')
					idx += 2;
				else
					++idx;

				string text = Source.Substring(prevIdx, idx - prevIdx);
				int start = prevIdx;

				prevIdx = idx;
				idx = Source.IndexOfAny(separators, prevIdx);

				LineInfo info = new LineInfo(text, lineNum, new TextSpan(start, prevIdx - start - 1));
				Lines.Add(info);

				++lineNum;
			}
		}

		public LineInfo GetLineInfo(int charIdx)
		{
			// Binary search
			int middle = Lines.Count / 2;
			int idx = middle;

			while (true)
			{
				TextSpan span = Lines[idx].Span;

				if (span.Contains(charIdx))
				{
					return Lines[idx];
				}

				if (middle == 1)
					return new LineInfo("--- invalid line ---", -1, null);

				middle = (middle + 1) / 2;
				if (charIdx < span.Start)
				{
					idx -= middle;
				}
				else
				{
					idx += middle;
				}
			}
		}
	}

	public static class ErrorSystem
	{
		public static ErrorSystemMetadata Metadata;
		public static int NumErrors;

		public static void AstError(TextSpan span, string message)
		{
			if (Metadata == null || span == null)
				return;

			LineInfo info = Metadata.GetLineInfo(span.Start);
			int column = span.Start - info.Span.Start;
			// TODO: multi-line errors

			Console.Write($"{Metadata.SourceFile}({info.LineNum},{column + 1}): ");
			Console.ForegroundColor = ConsoleColor.Red;
			Console.Write("ERROR");
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.WriteLine($": {message}");

			if (info.LineNum != -1)
			{
				int curLine = Console.CursorTop;
				Console.Write(info.Text);
				if (curLine == Console.CursorTop)
					Console.CursorTop++;
				
				string underline = "".PadLeft(span.Length - 1, '~');

				string underlinePadding = "";
				for (int i = 0; i < column; i++)
				{
					if (info.Text[i] == '\t')
						underlinePadding += '\t';
					else
						underlinePadding += ' ';
				}

				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine($"{underlinePadding}^{underline}");
				Console.ForegroundColor = ConsoleColor.Gray;
			}

			
			++NumErrors;
		}

		public static void Error(string message)
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.Write("ERROR");
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.WriteLine($": {message}");
		}

		public static void Fatal(string message)
		{	
			Console.ForegroundColor = ConsoleColor.DarkRed;
			Console.Write("FATAL");
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.WriteLine($": {message}");

			Console.ReadLine();

			Environment.Exit(-1);
		}
	}
}
