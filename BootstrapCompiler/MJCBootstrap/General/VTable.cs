﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.ILBackend.General;

namespace MJC.General
{
	public class VTable
	{
		public ScopeVariable Iden;
		public string MangledIden;
		public Dictionary<string, List<VTableMethod>> Methods = new Dictionary<string, List<VTableMethod>>();
		public Dictionary<ScopeVariable, VTableSection> Sections = new Dictionary<ScopeVariable, VTableSection>();
		public List<VTableMethod> OrderedMethods = new List<VTableMethod>();

		public VTable(ScopeVariable iden, string mangled)
		{
			Iden = iden;
			MangledIden = mangled;
		}

		public void AddMethod(VTableMethod method, List<Symbol> interfaces)
		{
			string str = method.Iden.GetSimpleName();
			if (!Methods.ContainsKey(str))
				Methods.Add(str, new List<VTableMethod>());

			Methods[str].Add(method);

			if (interfaces != null && interfaces.Count > 0)
			{
				foreach (Symbol interfaceSym in interfaces)
				{
					Symbol methodSym = interfaceSym.Children.Find(c =>
					{
						return c.Kind == SymbolKind.Method &&
						       c.Identifier == method.Iden;// &&
						       //c.Type == method.Type;
					});

					if (methodSym != null)
					{
						if (!Sections.ContainsKey(interfaceSym.ScopeVar))
							Sections.Add(interfaceSym.ScopeVar, new VTableSection(interfaceSym.ScopeVar, interfaceSym.MangledName));

						Sections[interfaceSym.ScopeVar].AddMethod(method);
						return;
					}
				}
			}

			// Add to 'empty' section
			if (!Sections.ContainsKey(ScopeVariable.Empty))
				Sections.Add(ScopeVariable.Empty, new VTableSection(ScopeVariable.Empty, null));

			Sections[ScopeVariable.Empty].AddMethod(method);
		}

		public List<VTableMethod> GetMethods(string iden)
		{
			Methods.TryGetValue(iden, out List<VTableMethod> methods);
			return methods;
		}

		public VTableMethod GetMethod(string iden, FunctionSymbolType funcType)
		{
			bool res = Methods.TryGetValue(iden, out List<VTableMethod> methods);
			if (!res)
				return null;

			foreach (VTableMethod method in methods)
			{
				if (method.Type == funcType)
					return method;
			}

			return null;
		}

		public VTableMethod GetNonOverloadedMethod(Identifier iden)
		{
			bool res = Methods.TryGetValue(iden.GetSimpleName(), out List<VTableMethod> methods);
			if (!res)
				return null;

			if (methods.Count == 1)
				return methods[0];
			return null;
		}

		public VTableMethod GetMethod(string iden, FunctionSymbolType funcType, ILTemplateInstance instance)
		{
			bool res = Methods.TryGetValue(iden, out List<VTableMethod> methods);
			if (!res)
				return null;

			foreach (VTableMethod method in methods)
			{
				SymbolType tmp = ILTemplateHelpers.GetInstancedType(method.Type, instance);
				if (tmp == funcType)
					return method;
			}

			return null;
		}

		public List<VTableMethod> GetMethodsStartingWith(string name)
		{
			List<VTableMethod> methods = new List<VTableMethod>();
			foreach (KeyValuePair<string, List<VTableMethod>> pair in Methods)
			{
				if (pair.Key.StartsWith(name))
					methods.AddRange(pair.Value);
			}

			if (methods.Count == 0)
				return null;
			return methods;
		}


		public void UpdateMethodOrder(CompilerContext ctx)
		{
			if (OrderedMethods.Count > 0)
				return;

			// When there is a section that isn't the non-interface section, there are sections that need to be processed
			if (Sections.Count > 1 || !Sections.ContainsKey(ScopeVariable.Empty))
			{
				foreach (KeyValuePair<ScopeVariable, VTableSection> pair in Sections)
				{
					if (pair.Key == ScopeVariable.Empty)
						continue;

					VTableSection section = pair.Value;
					//Symbol interfaceSym = ctx.Symbols.FindType(Scope.Empty, section.InterfaceIden);
					VTable interfaceVTable = ctx.GetVTable(section.InterfaceIden);

					// Update interface vtable order first
					interfaceVTable.UpdateMethodOrder(ctx);

					// Update section order
					foreach (VTableMethod orderedMethod in interfaceVTable.OrderedMethods)
					{
						List<VTableMethod> availableTables = section.Methods[orderedMethod.Iden.GetSimpleName()];
						VTableMethod sectionMethod = availableTables.Find(m => { return m.Iden == orderedMethod.Iden; //&& 
						                                                                /*m.Type == orderedMethod.Type;*/ });

						section.OrderedMethods.Add(sectionMethod);
						OrderedMethods.Add(sectionMethod);
					}
				}

				// Add all functions that weren't in a section to the end of the vtable
				if (Sections.TryGetValue(ScopeVariable.Empty, out VTableSection nonInterfaceSection))
				{
					foreach (KeyValuePair<string, List<VTableMethod>> pair in nonInterfaceSection.Methods)
					{
						foreach (VTableMethod method in pair.Value)
						{
							OrderedMethods.Add(method);
						}
					}
				}
			}
			else
			{
				// No interfaces, so just order the vtable in no particular order
				foreach (KeyValuePair<string, List<VTableMethod>> pair in Methods)
				{
					foreach (VTableMethod method in pair.Value)
					{
						OrderedMethods.Add(method);
					}
				}
			}


		}
		public List<VTableSection> GetSections(List<SymbolType> interfaceTypes)
		{
			List<VTableSection> sections = new List<VTableSection>();
			foreach (KeyValuePair<ScopeVariable, VTableSection> pair in Sections)
			{
				VTableSection section = pair.Value;

				SymbolType foundType = interfaceTypes.Find(it => { return it.ToILString() == section.Mangled; });

				if (foundType != null)
				{
					sections.Add(section);
				}
			}

			return sections;
		}
	}

	public class VTableSection
	{
		public ScopeVariable InterfaceIden;
		public string Mangled;
		public Dictionary<string, List<VTableMethod>> Methods = new Dictionary<string, List<VTableMethod>>();
		public List<VTableMethod> OrderedMethods = new List<VTableMethod>();

		public VTableSection(ScopeVariable iden, string mangled)
		{
			InterfaceIden = iden;
			Mangled = mangled;
		}

		public void AddMethod(VTableMethod method)
		{
			string str = method.Iden.GetSimpleName();
			if (!Methods.ContainsKey(str))
				Methods.Add(str, new List<VTableMethod>());

			Methods[str].Add(method);
		}
	}

	public class VTableMethod
	{
		public Identifier Iden;
		public FunctionSymbolType Type;
		public Symbol Symbol;

		private string _mangledName;

		public string MangledFunc
		{
			get
			{
				return Symbol?.MangledName ?? _mangledName;
			}
			set { _mangledName = MangledFunc; }
		}

		public VTableMethod(Symbol symbol)
		{
			Symbol = symbol;
			Iden = symbol.Identifier;
			Type = symbol.Type as FunctionSymbolType;
		}

		public VTableMethod(Identifier iden, FunctionSymbolType funcType, string mangled)
		{
			Iden = iden;
			Type = funcType;
			_mangledName = mangled;
		}

		public override string ToString()
		{
			return $"{Iden} {Type} : @{MangledFunc}";
		}
	}

	public class TemplateVTables
	{
		public VTable BaseTable;
		public List<VTable> PartSpecTables = new List<VTable>();
		public VTable FullSpecTable;
	}
}
