﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.SyntaxTree;

namespace MJC.General
{
	public class Identifier
	{
		public static Identifier GetIdentifier(SimpleNameSyntax syntax)
		{
			if (syntax is IdentifierNameSyntax idenName)
				return new NameIdentifier(idenName);
			if (syntax is TemplateNameSyntax templateName)
				return new TemplateInstanceIdentifier(templateName);

			return null;
		}

		public virtual string GetSimpleName()
		{
			return null;
		}

		public virtual string GetNonTemplateName()
		{
			return null;
		}

		public virtual Identifier GetBaseIdentifier()
		{
			return this;
		}

		public virtual Identifier GetSymTableScopeIden()
		{
			return this;
		}

		public static bool operator ==(Identifier iden0, Identifier iden1)
		{
			if (iden0 is NameIdentifier name0 && iden1 is NameIdentifier name1)
				return name0 == name1;
			if (iden0 is TemplateInstanceIdentifier inst0 && iden1 is TemplateInstanceIdentifier inst1)
				return inst0 == inst1;
			if (iden0 is TemplateDefinitionIdentifier def0 && iden1 is TemplateDefinitionIdentifier def1)
				return def0 == def1;
			return ReferenceEquals(iden0, iden1);
		}

		public static bool operator !=(Identifier iden0, Identifier iden1)
		{
			return !(iden0 == iden1);
		}

		protected bool Equals(Identifier other)
		{
			return this == other;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Identifier)obj);
		}

		public override int GetHashCode()
		{
			if (this is NameIdentifier name)
				return name.GetHashCode();
			if (this is TemplateInstanceIdentifier inst)
				return inst.GetHashCode();
			if (this is TemplateDefinitionIdentifier def)
				return def.GetHashCode();
			return 0;
		}

	}

	public class NameIdentifier : Identifier
	{
		public string Iden;

		public NameIdentifier(string iden)
		{
			Iden = iden;
		}

		public NameIdentifier(IdentifierNameSyntax syntax)
		{
			Iden = syntax.Identifier.Text;
		}

		public override string GetSimpleName()
		{
			return Iden;
		}

		public override string GetNonTemplateName()
		{
			return Iden;
		}

		public override string ToString()
		{
			return Iden;
		}

		public static bool operator ==(NameIdentifier iden0, NameIdentifier iden1)
		{
			if (ReferenceEquals(iden0, null) || ReferenceEquals(iden1, null))
				return ReferenceEquals(iden0, null) && ReferenceEquals(iden1, null);

			return iden0.Iden == iden1.Iden;
		}

		public static bool operator !=(NameIdentifier iden0, NameIdentifier iden1)
		{
			return !(iden0 == iden1);
		}

		protected bool Equals(NameIdentifier other)
		{
			return this == other;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((NameIdentifier) obj);
		}

		public override int GetHashCode()
		{
			return Iden.GetHashCode();
		}
	}

	public class TemplateDefinitionIdentifier : Identifier
	{
		public string Iden;
		public List<TemplateParameter> Parameters = new List<TemplateParameter>();

		public TemplateDefinitionIdentifier(string iden, int numParams)
		{
			Iden = iden;
			Parameters.Resize(numParams);
		}

		public TemplateDefinitionIdentifier(string iden, List<TemplateParameter> parameters)
		{
			Iden = iden;
			Parameters = parameters;
		}

		public override string GetSimpleName()
		{
			return Iden;
		}

		public override string GetNonTemplateName()
		{
			return $"__T{Parameters.Count}_{Iden}";
		}

		public override Identifier GetBaseIdentifier()
		{
			return new TemplateDefinitionIdentifier(Iden, Parameters.Count);
		}

		public override Identifier GetSymTableScopeIden()
		{
			TemplateDefinitionIdentifier tmp = new TemplateDefinitionIdentifier(Iden, Parameters.Count);
			for (var i = 0; i < tmp.Parameters.Count; i++)
			{
				TemplateParameter param = tmp.Parameters[i];
				TemplateParameter origParam = Parameters[i];

				if (origParam.ValueName != null)
					param.Type = origParam.Type;
			}

			return tmp;
		}

		public override string ToString()
		{
			return $"{Iden}<{Parameters.ToListString(", ")}>";
		}

		public static bool operator ==(TemplateDefinitionIdentifier iden0, TemplateDefinitionIdentifier iden1)
		{
			if (ReferenceEquals(iden0, null) || ReferenceEquals(iden1, null))
				return ReferenceEquals(iden0, null) && ReferenceEquals(iden1, null);

			if (iden0.Iden != iden1.Iden ||
				iden0.Parameters.Count != iden1.Parameters.Count)
				return false;
			
			for (var i = 0; i < iden0.Parameters.Count; i++)
			{
				if (iden0.Parameters[i] != iden1.Parameters[i])
					return false;
			}

			return true;
		}

		public static bool operator !=(TemplateDefinitionIdentifier iden0, TemplateDefinitionIdentifier iden1)
		{
			return !(iden0 == iden1);
		}

		protected bool Equals(TemplateDefinitionIdentifier other)
		{
			return this == other;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((TemplateDefinitionIdentifier)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = Iden.GetHashCode();
				/*if (Parameters != null)
				{
					foreach (TemplateParameter param in Parameters)
					{
						hashCode = (hashCode * 397) ^ param.GetHashCode();
					}
				}*/

				hashCode = (hashCode * 397) ^ Parameters.Count;

				return hashCode;
			}
		}
	}

	public class TemplateParameter
	{
		public SymbolType Type;
		public int Index;

		// Value parameter
		public string ValueName;
		public string ValueSpecialization;
		public SimpleExpressionSyntax ValueSpecializationExpr;
		public string ValueDefault;
		public SimpleExpressionSyntax ValueDefaultExpr;

		// Type parameter
		public SymbolType TypeSpecialization;
		public SymbolType TypeDefault;

		// Types which can be inferred from the template parameter
		public List<int> InferredParams;

		public TemplateParameter()
		{ }

		public TemplateParameter(int index)
		{
			Index = index;
		}

		public TemplateParameter(string valName, int index)
		{ 
			ValueName = valName;
			Index = index;
		}

		public TemplateParameter(SymbolType type, int index)
		{
			Type = type;
			Index = index;
		}

		public TemplateParameter(string valName, SymbolType type, int index)
		{
			ValueName = valName;
			Type = type;
			Index = index;
		}

		public TemplateParameter(string valName, SymbolType type, int index, string valSpecialization, string valDefault)
		{
			ValueName = valName;
			Type = type;
			Index = index;
			ValueSpecialization = valSpecialization;
			ValueDefault = valDefault;
		}

		public TemplateParameter(SymbolType type, int index, SymbolType typeSpecialization, SymbolType typeDefault)
		{
			Type = type;
			Index = index;
			TypeSpecialization = typeSpecialization;
			TypeDefault = typeDefault;
		}

		public bool HasDefaultValue()
		{
			return TypeDefault != null ||
			       ValueDefault != null ||
			       ValueDefaultExpr != null;
		}

		public bool HasSpecialization()
		{
			return TypeSpecialization != null ||
			       ValueSpecialization != null ||
				   ValueSpecializationExpr != null;
		}

		public override string ToString()
		{
			string str = "";

			if (ValueName != null)
			{
				str += $"{ValueName}:{Type}";
				if (ValueSpecialization != null)
					str += ":=" + ValueSpecialization.ToString();
				else if (ValueSpecializationExpr != null)
					str += ":=" + ValueSpecializationExpr.ToString();
				if (ValueDefault != null)
					str += "=" + ValueDefault.ToString();
				else if (ValueDefaultExpr != null)
					str += "=" + ValueDefaultExpr.ToString();
			}
			else
			{
				str += $"{Type}";
				if (TypeSpecialization != null)
					str += ":=" + TypeSpecialization.ToString();
				if (TypeDefault != null)
					str += "=" + TypeDefault.ToString();
			}

			return str;
		}

		protected bool Equals(TemplateParameter other)
		{
			// If either one has no type assigned, return true
			if (Type == null || other.Type == null)
				return true;
			
			if (ValueName != null)
			{
				if (ValueSpecialization != null || other.ValueSpecialization != null)
				{
					if (ValueSpecialization != other.ValueSpecialization)
						return false;
				}
				if (ValueSpecializationExpr != null || other.ValueSpecializationExpr != null)
				{
					if (ValueSpecializationExpr != other.ValueSpecializationExpr)
						return false;
				}
			}
			else
			{
				if (Type != other.Type)
					return false;
				if (TypeSpecialization != other.TypeSpecialization)
					return false;
			}

			return true;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((TemplateParameter) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = (Type != null ? Type.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ Index;
				hashCode = (hashCode * 397) ^ (ValueName != null ? ValueName.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (ValueSpecialization != null ? ValueSpecialization.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (ValueDefault != null ? ValueDefault.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (TypeSpecialization != null ? TypeSpecialization.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (TypeDefault != null ? TypeDefault.GetHashCode() : 0);
				return hashCode;
			}
		}

		public static bool operator ==(TemplateParameter left, TemplateParameter right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(TemplateParameter left, TemplateParameter right)
		{
			return !Equals(left, right);
		}
	}

	public class TemplateInstanceIdentifier : Identifier
	{
		public string Iden;
		public List<TemplateArgument> Arguments = new List<TemplateArgument>();
		public TemplateDefinitionIdentifier BaseIden;

		public TemplateInstanceIdentifier(string iden, int numArgs)
		{
			Iden = iden;
			Arguments.Resize(numArgs);

			for (var i = 0; i < Arguments.Count; i++)
			{
				TemplateArgument arg = Arguments[i];
				arg.Index = i;
			}

			BaseIden = new TemplateDefinitionIdentifier(Iden, Arguments.Count);
		}

		public TemplateInstanceIdentifier(TemplateDefinitionIdentifier baseIden)
		{
			BaseIden = baseIden;
			Iden = baseIden.Iden;
			Arguments.Resize(BaseIden.Parameters.Count);

			for (var i = 0; i < Arguments.Count; i++)
			{
				TemplateArgument arg = Arguments[i];
				arg.Index = i;
			}
		}

		public TemplateInstanceIdentifier(TemplateNameSyntax syntax)
		{
			Iden = syntax.Identifier.Text;
			Arguments.Resize(syntax.Arguments.Count);

			for (var i = 0; i < Arguments.Count; i++)
			{
				TemplateArgument arg = Arguments[i];
				arg.Index = i;
			}

			BaseIden = new TemplateDefinitionIdentifier(Iden, Arguments.Count);
		}

		public override string GetSimpleName()
		{
			return Iden;
		}

		public override string GetNonTemplateName()
		{
			return $"__T{Arguments.Count}_{Iden}";
		}

		public override Identifier GetBaseIdentifier()
		{
			return BaseIden;
		}

		public override Identifier GetSymTableScopeIden()
		{
			TemplateDefinitionIdentifier tmp = new TemplateDefinitionIdentifier(Iden, Arguments.Count);
			for (var i = 0; i < tmp.Parameters.Count; i++)
			{
				TemplateParameter param = tmp.Parameters[i];
				TemplateParameter origParam = BaseIden.Parameters[i];

				if (origParam.ValueName != null)
					param.Type = origParam.Type;
			}

			return tmp;
		}

		public override string ToString()
		{
			return $"{Iden}!<{Arguments.ToListString(", ")}>";
		}

		public static bool operator ==(TemplateInstanceIdentifier iden0, TemplateInstanceIdentifier iden1)
		{
			if (ReferenceEquals(iden0, null) || ReferenceEquals(iden1, null))
				return ReferenceEquals(iden0, null) && ReferenceEquals(iden1, null);

			if (iden0.Iden != iden1.Iden ||
			    iden0.Arguments.Count != iden1.Arguments.Count)
				return false;

			for (int i = 0; i < iden0.Arguments.Count; i++)
			{
				TemplateArgument arg0 = iden0.Arguments[i];
				TemplateArgument arg1 = iden1.Arguments[i];

				if (arg0 != arg1)
					return false;
			}

			return true;
		}

		public static bool operator !=(TemplateInstanceIdentifier iden0, TemplateInstanceIdentifier iden1)
		{
			return !(iden0 == iden1);
		}

		protected bool Equals(TemplateInstanceIdentifier other)
		{
			return this == other;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((TemplateInstanceIdentifier)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = Iden.GetHashCode();
				/*foreach (TemplateArgument argument in Arguments)
				{
					hashCode = (hashCode * 397) ^ argument.GetHashCode();
				}*/
				// Don't hash the argument values (allow for 'modifiable' key)
				hashCode = (hashCode * 397) ^ Arguments.Count;
				return hashCode;
			}
		}
	}

	public class TemplateArgument
	{
		public int Index;
		public SymbolType Type;
		public string ValueFunc;
		public string Value;
		public SymbolType AssociatedBaseType;

		public TemplateArgument()
		{ }

		public TemplateArgument(int index, SymbolType type, string value = null)
		{
			Index = index;
			Type = type;
			Value = value;
		}

		public override string ToString()
		{
			if (Value != null)
				return $"{Value} : ${Type}";
			if (ValueFunc != null)
				return $"@{ValueFunc} : ${Type}";
			return $"${Type.ToString()}";
		}

		protected bool Equals(TemplateArgument other)
		{
			return Index == other.Index && Equals(Type, other.Type) && string.Equals(Value, other.Value);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((TemplateArgument) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = Index;
				hashCode = (hashCode * 397) ^ (Type != null ? Type.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (Value != null ? Value.GetHashCode() : 0);
				return hashCode;
			}
		}

		public static bool operator ==(TemplateArgument left, TemplateArgument right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(TemplateArgument left, TemplateArgument right)
		{
			return !Equals(left, right);
		}
	}
}
