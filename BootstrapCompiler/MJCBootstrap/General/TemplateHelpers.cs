﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.ILBackend;
using MJC.ILBackend.General;
using MJC.SyntaxTree;

namespace MJC.General
{
	public enum TemplateValueParamCategory
	{
		Unknown,
		Bool,
		Integer,
		FloatingPoint,
		Char,
		String,
	}

	public static class TemplateHelpers
	{
		public static SymbolType GetTemplateBaseType(SymbolType type, Symbol symbol)
		{
			switch (type)
			{
			case ReferenceSymbolType refType:
			{
				SymbolType baseType = GetTemplateBaseType(refType.BaseType, symbol);
				return SymbolType.ReferenceType(baseType, type.Attributes);
			}
			case PointerSymbolType ptrType:
			{
				SymbolType baseType = GetTemplateBaseType(ptrType.BaseType, symbol);
				return SymbolType.PointerType(baseType, type.Attributes);
			}
			case ArraySymbolType arrType:
			{
				SymbolType baseType = GetTemplateBaseType(arrType.BaseType, symbol);
				return SymbolType.ArrayType(baseType, arrType.SizeExpr, arrType.ArraySize, type.Attributes);
			}
			case BitFieldSymbolType bitFieldType:
			{
				SymbolType baseType = GetTemplateBaseType(bitFieldType.BaseType, symbol);
				return SymbolType.BitFieldType(baseType, bitFieldType.Bits, type.Attributes);
			}
			case DelegateSymbolType delType:
			{
				SymbolType retType = GetTemplateBaseType(delType.ReturnType, symbol);
				List<SymbolType> paramTypes = null;
				if (delType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType paramType in delType.ParamTypes)
					{
						SymbolType tmp = GetTemplateBaseType(paramType, symbol);
						paramTypes.Add(tmp);
					}
				}

				return SymbolType.DelegateType(delType.Identifier.GetBaseTemplate(), paramTypes, retType, type.Attributes);
			}
			case NullableSymbolType nullType:
			{
				SymbolType baseType = GetTemplateBaseType(nullType.BaseType, symbol);
				return SymbolType.NullableType(baseType, type.Attributes);
			}
			case TemplateInstanceSymbolType instType:
			{
				return instType.TemplateSymbol.Type;
			}
			case FunctionSymbolType funcType:
			{
				SymbolType recType = GetTemplateBaseType(funcType.ReceiverType, symbol);
				SymbolType retType = GetTemplateBaseType(funcType.ReturnType, symbol);
				List<SymbolType> paramTypes = null;
				if (funcType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType paramType in funcType.ParamTypes)
					{
						SymbolType tmp = GetTemplateBaseType(paramType, symbol);
						paramTypes.Add(tmp);
					}
				}

				return SymbolType.FunctionType(recType, paramTypes, retType, type.Attributes);
			}
			case MemoryLocSymbolType memoryLocType:
			{
				SymbolType baseType = GetTemplateBaseType(memoryLocType.BaseType, symbol);
				return SymbolType.MemoryLocType(baseType, type.Attributes);
			}
			case TemplateParamSymbolType paramType:
			{
				List<Symbol> paramSyms = symbol.GetChildrenWithType<TemplateParamSymbolType>();
				Symbol paramSym = paramSyms.Find(s =>
				{
					TemplateParamSymbolType tpType = s.Type as TemplateParamSymbolType;
					if (tpType.Index == paramType.Index)
						return true;
					return false;
				});

				return paramSym.Type;
			}
			case TupleSymbolType tupleType:
			{
				List<SymbolType> subTypes = null;
				if (tupleType.SubTypes != null)
				{
					subTypes = new List<SymbolType>();
					foreach (SymbolType paramType in tupleType.SubTypes)
					{
						SymbolType tmp = GetTemplateBaseType(paramType, symbol);
						subTypes.Add(tmp);
					}
				}

				return SymbolType.TupleType(subTypes, type.Attributes);
			}
			case VariadicSymbolType variadicType:
			{
				SymbolType baseType = GetTemplateBaseType(variadicType.BaseType, symbol);
				return SymbolType.VariadicType(baseType, type.Attributes);
			}
			default:
				return type;
			}
		}

		public static bool DoTemplateDefInstMatch(TemplateInstanceIdentifier inst, TemplateDefinitionIdentifier def)
		{
			if (inst.Arguments.Count != def.Parameters.Count)
				return false;

			Dictionary<SymbolType, SymbolType> typeMapping = new Dictionary<SymbolType, SymbolType>();

			for (int i = 0; i < inst.Arguments.Count; i++)
			{
				TemplateArgument arg = inst.Arguments[i];
				TemplateParameter param = def.Parameters[i];

				if (param.ValueName != null)
				{
					// If specialized, check if specializations match
					if (param.ValueSpecialization != null && arg.Value != param.ValueSpecialization)
						return false;
				}
				else
				{
					// If specialized, check if the types match
					if (param.TypeSpecialization != null && arg.Type != param.TypeSpecialization)
						return false;

					// If a specialization, where a template param gets replaced by another, occurs,
					// check if both given params are the same type,
					// else add the types to the mapping, in case template param is reused
					if (typeMapping.TryGetValue(param.Type, out SymbolType type))
					{
						if (type != arg.Type)
							return false;
					}
					else
					{
						typeMapping.Add(param.Type, arg.Type);
					}
				}
			}

			return true;
		}

		public static TemplateVTables GetValidVTables(List<VTable> tables, TemplateInstanceIdentifier instIden)
		{
			TemplateVTables vTables = new TemplateVTables();

			foreach (VTable table in tables)
			{
				if (table.Iden.Name is TemplateDefinitionIdentifier defIden)
				{
					if (TemplateHelpers.DoTemplateDefInstMatch(instIden, defIden))
					{
						bool isFullSpec = true, isPartialSpec = false, isValid = true;

						for (var i = 0; i < defIden.Parameters.Count; i++)
						{
							TemplateParameter parameter = defIden.Parameters[i];
							TemplateArgument arg = instIden.Arguments[i];
							if (parameter.TypeSpecialization != null)
							{
								if (parameter.TypeSpecialization != arg.Type)
								{
									isValid = false;
									break;
								}

								isPartialSpec = true;
							}
							else if (parameter.ValueSpecialization != null)
							{
								if (parameter.ValueSpecialization != arg.Value)
								{
									isValid = false;
									break;
								}

								isPartialSpec = true;
							}
							else
							{
								isFullSpec = false;
							}
						}

						if (isValid)
						{
							if (isFullSpec)
								vTables.FullSpecTable = table;
							else if (isPartialSpec)
								vTables.PartSpecTables.Add(table);
							else
								vTables.BaseTable = table;
						}
					}
				}
			}

			return vTables;
		}

		public static List<VTableMethod> GetInstanceMethods(ScopeVariable instScopeVar, Symbol baseSymbol, Dictionary<ScopeVariable, List<VTable>> vtables)
		{
			List<VTable> vtableOverloads = vtables[baseSymbol.ScopeVar];
			TemplateInstanceIdentifier instIden = instScopeVar.Name as TemplateInstanceIdentifier;

			// Find all valid vtables
			TemplateVTables validTables = GetValidVTables(vtableOverloads, instIden);
			List<VTableMethod> methods = new List<VTableMethod>();
			HashSet<Identifier> specialFunctions = new HashSet<Identifier>();

			// TODO: Method templates
			// Add fully specialized
			if (validTables.FullSpecTable != null)
			{
				foreach (KeyValuePair<string, List<VTableMethod>> pair in validTables.FullSpecTable.Methods)
				{
					foreach (VTableMethod method in pair.Value)
					{
						if (method.Iden is NameIdentifier idenName && idenName.Iden.StartsWith("__"))
						{
							specialFunctions.Add(idenName);
							methods.Add(method);
						}
						else
						{
							methods.Add(method);
						}

					}
				}
			}

			// Add unoverloaded specialized tables
			if (validTables.PartSpecTables.Count > 0)
			{
				foreach (VTable table in validTables.PartSpecTables)
				{
					foreach (KeyValuePair<string, List<VTableMethod>> pair in table.Methods)
					{
						foreach (VTableMethod method in pair.Value)
						{
							if (method.Iden is NameIdentifier idenName && idenName.Iden.StartsWith("__"))
							{
								if (!specialFunctions.Contains(idenName))
								{
									specialFunctions.Add(idenName);
									methods.Add(method);
								}
							}
							else
							{
								bool alreadyExists = false;
								foreach (VTableMethod addedMethod in methods)
								{
									if (addedMethod.Iden == method.Iden)
									{
										SymbolType baseAddedType = GetTemplateBaseType(addedMethod.Type, baseSymbol);
										SymbolType baseType = GetTemplateBaseType(method.Type, baseSymbol);

										if (baseAddedType == baseType)
											alreadyExists = true;
									}
								}

								if (!alreadyExists)
									methods.Add(method);
							}
						}
					}
				}
			}

			// Handle base table
			foreach (KeyValuePair<string, List<VTableMethod>> pair in validTables.BaseTable.Methods)
			{
				foreach (VTableMethod method in pair.Value)
				{
					if (method.Iden is NameIdentifier idenName && idenName.Iden.StartsWith("__"))
					{
						if (!specialFunctions.Contains(idenName))
						{
							specialFunctions.Add(idenName);
							methods.Add(method);
						}
					}
					else
					{
						bool alreadyExists = false;
						foreach (VTableMethod addedMethod in methods)
						{
							if (addedMethod.Iden == method.Iden)
							{
								SymbolType baseAddedType = GetTemplateBaseType(addedMethod.Type, baseSymbol);
								SymbolType baseType = GetTemplateBaseType(method.Type, baseSymbol);

								if (baseAddedType == baseType)
									alreadyExists = true;
							}
						}

						if (!alreadyExists)
							methods.Add(method);
					}
				}
			}

			return methods;
		}

		public static TemplateValueParamCategory CategorizeType(SymbolType type)
		{
			switch (type)
			{
			case BuiltinSymbolType builtin:
			{
				switch (builtin.Builtin)
				{
				case BuiltinTypes.Bool:
					return TemplateValueParamCategory.Bool;
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
					return TemplateValueParamCategory.Integer;
				case BuiltinTypes.F32:
				case BuiltinTypes.F64:
					return TemplateValueParamCategory.FloatingPoint;
				case BuiltinTypes.Char:
				case BuiltinTypes.WChar:
				case BuiltinTypes.Rune:
					return TemplateValueParamCategory.Char;
				case BuiltinTypes.String:
				case BuiltinTypes.StringLiteral:
					return TemplateValueParamCategory.String;
				case BuiltinTypes.Null:
				case BuiltinTypes.Any:
				default:
					return TemplateValueParamCategory.Unknown;
				}
			}
			default:
				return TemplateValueParamCategory.Unknown;
			}
		}

		public static bool IsValidTemplate(TemplateDefinitionIdentifier defIden, List<SymbolType> funcParamTypes = null)
		{
			if (funcParamTypes == null)
			{
				// When no function parameters are passed to the template, if any template parameter has a default value, they need to be at the end of the template parameter list

				List<SymbolType> inferredTypes = new List<SymbolType>();
				List<string> inferredValue = new List<string>();

				bool encounteredDefault = false;
				foreach (TemplateParameter param in defIden.Parameters)
				{
					// Get all types that can be inferred from the current
					if (param.ValueName == null)
					{
						if (param.TypeSpecialization != null)
						{
							List<SymbolType> tmpInferredSpec = new List<SymbolType>();
							List<string> tmpInferredVal = new List<string>();
							GetInferableTypes(param.TypeSpecialization, ref tmpInferredSpec, ref tmpInferredVal);

							if (tmpInferredSpec.Count > 0)
							{
								param.InferredParams = new List<int>();
								foreach (SymbolType inferred in tmpInferredSpec)
								{
									TemplateParameter tmpParam = defIden.Parameters.Find(p =>
									{
										if (p.Type == inferred)
											return true;
										return false;
									});

									if (tmpParam != null)
									{
										param.InferredParams.Add(tmpParam.Index);
									}
								}
							}

							if (tmpInferredVal.Count > 0)
							{
								if (param.InferredParams == null)
									param.InferredParams = new List<int>();

								foreach (string inferred in tmpInferredVal)
								{
									TemplateParameter tmpParam = defIden.Parameters.Find(p =>
									{
										if (p.ValueName == inferred)
											return true;
										return false;
									});

									if (tmpParam != null)
									{
										param.InferredParams.Add(tmpParam.Index);
									}
								}
							}

							inferredTypes.AddRange(tmpInferredSpec);
							inferredValue.AddRange(tmpInferredVal);
						}
					}

					// If the parameter has a default type or value, make sure we know it next iteration
					if (param.HasDefaultValue())
					{
						encounteredDefault = true;
						continue;
					}

					// If a previous param had a default value, make sure that the current param can be derived from it
					if (encounteredDefault)
					{
						if (param.ValueName != null)
						{
							string found = inferredValue.Find(s =>
							{
								if (s == param.ValueName)
									return true;
								return false;
							});

							if (found == null)
								return false;
						}
						else
						{
							if (!inferredTypes.Contains(param.Type))
								return false;
						}
					}
				}
			}
			else
			{
				// When function parameters are passed the the template, for any template parameter with no default value, encountered after the first template parameters with a default value, the template parameter needs to be able to be inferred from the function parameters

				bool encounteredDefault = false;
				foreach (TemplateParameter param in defIden.Parameters)
				{
					// TODO

					if (param.HasDefaultValue())
					{
						encounteredDefault = true;
						continue;
					}

					if (encounteredDefault)
					{
						if (param.ValueName != null)
						{
							// TODO
						}
						else
						{
							bool found = false;
							foreach (SymbolType paramType in funcParamTypes)
							{
								List<TemplateParamSymbolType> subTypes = paramType.GetSubTypesOf<TemplateParamSymbolType>();
								foreach (TemplateParamSymbolType subType in subTypes)
								{
									if (subType == param.Type)
									{
										found = true;
										break;
									}
								}

								if (found)
									break;
							}

							if (!found)
								return false;
						}
					}
				}
			}

			return true;
		}

		static void GetInferableTypes(SymbolType type, ref List<SymbolType> inferredTypes, ref List<string> inferredValues)
		{
			switch (type)
			{
			case PointerSymbolType ptrType:
			{
				GetInferableTypes(ptrType.BaseType, ref inferredTypes, ref inferredValues);
				break;
			}
			case ReferenceSymbolType refType:
			{
				GetInferableTypes(refType.BaseType, ref inferredTypes, ref inferredValues);
				break;
			}
			case ArraySymbolType arrType:
			{
				if (arrType.SizeExpr is IdentifierNameSyntax nameSyntax)
				{
					inferredValues.Add(nameSyntax.Context.Identifier.GetSimpleName());
				}

				GetInferableTypes(arrType.BaseType, ref inferredTypes, ref inferredValues);
				break;
			}
			case NullableSymbolType nullType:
			{
				GetInferableTypes(nullType.BaseType, ref inferredTypes, ref inferredValues);
				break;
			}
			case TemplateParamSymbolType _:
			{
				inferredTypes.Add(type);
				break;
			}
			case DelegateSymbolType delType:
			{
				if (delType.ReturnType != null)
					GetInferableTypes(delType.ReturnType, ref inferredTypes, ref inferredValues);

				if (delType.ParamTypes != null)
				{
					foreach (SymbolType paramType in delType.ParamTypes)
					{
						GetInferableTypes(paramType, ref inferredTypes, ref inferredValues);
					}
				}

				break;
			}
			case TupleSymbolType tupleType:
			{
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					GetInferableTypes(subType, ref inferredTypes, ref inferredValues);
				}

				break;
			}
			}
		}

		// TODO: Let work with function parameters
		public static bool IsCompatible(TemplateDefinitionIdentifier def, TemplateInstanceIdentifier inst)
		{
			for (int i = 0; i < def.Parameters.Count; i++)
			{
				TemplateParameter param = def.Parameters[i];
				TemplateArgument arg;
				inst.Arguments.TryGetValue(i, out arg);

				if (arg == null)
				{
					// No argument is provided to the instance
					if (!param.HasDefaultValue())
					{
						// If no default value has been assigned, check whether the parameter can be inferred from the previous parameters
						bool isInferred = false;
						for (int j = 0; j < i; j++)
						{
							TemplateParameter tmp = def.Parameters[j];
							List<int> inferred = tmp.InferredParams;
							if (inferred != null && inferred.Contains(param.Index))
							{
								isInferred = true;
								break;
							}
						}

						if (!isInferred)
							return false;
					}
				}
				else
				{
					if (param.ValueName != null)
					{
						TemplateValueParamCategory paramCat = CategorizeType(param.Type);
						TemplateValueParamCategory argCat = CategorizeType(arg.Type);

						if (paramCat != argCat)
							return false;
					}
					else
					{
						// Both are types, check whether any specialization exists
						if (param.TypeSpecialization != null && !IsTypeCompatible(param.TypeSpecialization, arg.Type))
							return false;
					}
				}
			}

			return true;
		}

		static bool IsTypeCompatible(SymbolType specialization, SymbolType type)
		{
			switch (specialization)
			{
			case TemplateParamSymbolType _:
				return true;
			case PointerSymbolType sPtrType when type is PointerSymbolType ptrType:
				return IsTypeCompatible(sPtrType.BaseType, ptrType.BaseType);
			case ReferenceSymbolType sRefType when type is ReferenceSymbolType refType:
				return IsTypeCompatible(sRefType.BaseType, refType.BaseType);
			case ArraySymbolType sArrType when type is ArraySymbolType arrType:
				return IsTypeCompatible(sArrType.BaseType, arrType.BaseType);
			case NullableSymbolType sNullType when type is NullableSymbolType nullType:
				return IsTypeCompatible(sNullType.BaseType, nullType.BaseType);
			case DelegateSymbolType sDelType when type is DelegateSymbolType delType:
			{
				if (sDelType.ReturnType != null || delType.ReturnType != null)
				{
					if (!(sDelType.ReturnType != null && delType.ReturnType != null) ||
					    !IsTypeCompatible(sDelType.ReturnType, delType.ReturnType))
						return false;
				}

				if (sDelType.ParamTypes != null || delType.ReturnType != null)
				{
					if (sDelType.ParamTypes == null || delType.ReturnType == null ||
					    sDelType.ParamTypes.Count != delType.ParamTypes.Count)
						return false;

					for (int i = 0; i < sDelType.ParamTypes.Count; i++)
					{
						SymbolType sParamType = sDelType.ParamTypes[i];
						SymbolType paramType = delType.ParamTypes[i];

						if (!IsTypeCompatible(sParamType, paramType))
							return false;
					}
				}

				return true;
			}
			case TupleSymbolType sTupType when type is TupleSymbolType tupType:
			{
				for (var i = 0; i < sTupType.SubTypes.Count; i++)
				{
					SymbolType sSubType = sTupType.SubTypes[i];
					SymbolType subType = tupType.SubTypes[i];
					if (!IsTypeCompatible(sSubType, subType))
						return false;
				}

				return true;
			}
			default:
				return specialization == type;
			}
		}

		public static void UpdateAndInferDefaults(TemplateInstanceIdentifier instIden, TemplateDefinitionIdentifier defIden)
		{
			// If the same amount of arguments are given than parameters, nothing needs to be done
			if (instIden.Arguments.Count == defIden.Parameters.Count)
				return;

			// Get all the inferred types
			Dictionary<SymbolType, SymbolType> inferredType = new Dictionary<SymbolType, SymbolType>();
			Dictionary<string, string> inferredValues = new Dictionary<string, string>();
			for (var i = 0; i < instIden.Arguments.Count; i++)
			{
				TemplateParameter param = defIden.Parameters[i];
				TemplateArgument argument = instIden.Arguments[i];

				if (param.TypeSpecialization != null)
				{
					Dictionary<SymbolType, SymbolType> tmpMapping = new Dictionary<SymbolType, SymbolType>();
					InferTypesAndValues(param.TypeSpecialization, argument.Type, ref tmpMapping, ref inferredValues);

					foreach (KeyValuePair<SymbolType, SymbolType> pair in tmpMapping)
					{
						if (!inferredType.ContainsKey(pair.Key))
							inferredType.Add(pair.Key, pair.Value);
					}
				}
			}

			for (int i = instIden.Arguments.Count; i < defIden.Parameters.Count; i++)
			{
				TemplateParameter param = defIden.Parameters[i];
				TemplateArgument argument;

				if (param.ValueName != null)
				{
					if (param.ValueDefault != null)
					{
						argument = new TemplateArgument(i, param.Type, param.ValueDefault);
					}
					else
					{
						string val = inferredValues[param.ValueName];
						argument = new TemplateArgument(i, param.Type, val);
					}
				}
				else
				{
					if (param.TypeDefault != null)
					{
						argument = new TemplateArgument(i, param.TypeDefault);
					}
					else
					{
						SymbolType tmp = inferredType[param.Type];
						argument = new TemplateArgument(i, tmp);
					}
				}

				instIden.Arguments.Add(argument);
			}
		}

		public static void InferTypesAndValues(SymbolType specialization, SymbolType type, ref Dictionary<SymbolType, SymbolType> inferredTypes, ref Dictionary<string, string> inferredValues)
		{
			switch (specialization)
			{
			case TemplateParamSymbolType _:
			{
				inferredTypes.Add(specialization, type);
				break;
			}
			case PointerSymbolType sPtrType when type is PointerSymbolType ptrType:
			{
				InferTypesAndValues(sPtrType.BaseType, ptrType.BaseType, ref inferredTypes, ref inferredValues);
				break;
			}
			case ReferenceSymbolType sRefType when type is ReferenceSymbolType refType:
			{
				InferTypesAndValues(sRefType.BaseType, refType.BaseType, ref inferredTypes, ref inferredValues);
				break;
			}
			case ArraySymbolType sArrType when type is ArraySymbolType arrType:
			{
				if (sArrType.SizeExpr is IdentifierNameSyntax idenName)
				{
					inferredValues.Add(idenName.Identifier.Text, arrType.ArraySize.ToString());
				}

				InferTypesAndValues(sArrType.BaseType, arrType.BaseType, ref inferredTypes, ref inferredValues);
				break;
			}
			case NullableSymbolType sNullType when type is NullableSymbolType nullType:
			{
				InferTypesAndValues(sNullType.BaseType, nullType.BaseType, ref inferredTypes, ref inferredValues);
				break;
			}
			case DelegateSymbolType sDelType when type is DelegateSymbolType delType:
			{
				if (sDelType.ReturnType != null || delType.ReturnType != null)
				{
					if (!(sDelType.ReturnType != null && delType.ReturnType != null))
						break;

					InferTypesAndValues(sDelType.ReturnType, delType.ReturnType, ref inferredTypes, ref inferredValues);
				}

				if (sDelType.ParamTypes != null || delType.ReturnType != null)
				{
					if (sDelType.ParamTypes == null || delType.ReturnType == null ||
					    sDelType.ParamTypes.Count != delType.ParamTypes.Count)
						break;

					for (int i = 0; i < sDelType.ParamTypes.Count; i++)
					{
						SymbolType sParamType = sDelType.ParamTypes[i];
						SymbolType paramType = delType.ParamTypes[i];

						InferTypesAndValues(sParamType, paramType, ref inferredTypes, ref inferredValues);
					}
				}

				break;
			}
			case TupleSymbolType sTupType when type is TupleSymbolType tupType:
			{
				for (var i = 0; i < sTupType.SubTypes.Count; i++)
				{
					SymbolType sSubType = sTupType.SubTypes[i];
					SymbolType subType = tupType.SubTypes[i];
					InferTypesAndValues(sSubType, subType, ref inferredTypes, ref inferredValues);
				}

				break;
			}
			}
		}

	}
}
