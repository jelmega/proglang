﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.ILBackend;

namespace MJC.General
{
	public class CompilerContext
	{
		public SymbolTable Symbols;
		public ImportTable Imports;
		public FileImports FileImport;
		public FileImportDirective ModuleImportDirective; // Used by imported modules
		public Dictionary<ScopeVariable, List<VTable>> VTables = new Dictionary<ScopeVariable, List<VTable>>();
		public Dictionary<string, TypeInfo> TypeInfos = new Dictionary<string, TypeInfo>();

		public CompilerContext()
		{
			Symbols = new SymbolTable(this);
			Imports = new ImportTable(this);
		}

		public void SetCurFileImportTable(FileImports importTable)
		{
			FileImport = importTable;
		}

		public Symbol FindDefinition(Scope scope, Identifier name, int location, Scope qualifiedScope)
		{
			Symbol symbol = Symbols.FindDefinition(scope, name, location, qualifiedScope);
			if (symbol != null)
				return symbol;

			// Not defined in module, search in imports
			return FileImport != null ? Imports.FindDefinition(scope, name, location, qualifiedScope, FileImport, ModuleImportDirective) : null;
		}

		public Symbol FindDefinition(Scope scope, ScopeVariable scopeVar, int location)
		{
			return FindDefinition(scope, scopeVar.Name, location, scopeVar.Scope);
		}

		public Symbol FindFunction(Scope scope, Identifier funcName, SymbolType recType, List<SymbolType> argTypes, Scope qualifiedScope, int location)
		{
			Symbol symbol = Symbols.FindFunctionWithParameters(scope, funcName, recType, argTypes, qualifiedScope);
			if (symbol != null)
				return symbol;

			// Not defined in module, search in imports
			return FileImport != null ? Imports.FindFunction(scope, funcName, recType, argTypes, qualifiedScope, location, FileImport, ModuleImportDirective) : null;
		}

		public Symbol FindFunction(Scope scope, ScopeVariable scopeVar, SymbolType recType, List<SymbolType> argTypes, int location)
		{
			return FindFunction(scope, scopeVar.Name, recType, argTypes, scopeVar.Scope, location);
		}

		public Symbol FindType(Scope scope, Identifier name, Scope qualifiedScope = null)
		{
			Symbol symbol = Symbols.FindType(scope, name, qualifiedScope);
			if (symbol != null)
				return symbol;

			// Not defined in module, search in imports
			return FileImport != null ? Imports.FindType(scope, name, qualifiedScope, FileImport, ModuleImportDirective) : null;
		}

		public Symbol FindType(Scope scope, ScopeVariable scopeVariable)
		{
			return FindType(scope, scopeVariable.Name, scopeVariable.Scope);
		}

		public Symbol FindTemplate(Scope scope, TemplateInstanceIdentifier iden, Scope qualifiedScope)
		{
			Symbol symbol = Symbols.FindTemplate(scope, iden, qualifiedScope);
			if (symbol != null)
				return symbol;

			return FileImport != null ? Imports.FindTemplate(scope, iden, qualifiedScope, FileImport, ModuleImportDirective) : null;
		}

		public Symbol FindTemplate(Scope scope, ScopeVariable scopeVar)
		{
			if (scopeVar.Name is TemplateInstanceIdentifier instName)
				return FindTemplate(scope, instName, scopeVar.Scope);
			return null;
		}

		public List<Symbol> FindCompatibleTemplates(Scope scope, TemplateInstanceIdentifier iden, Scope qualifiedScope)
		{
			List<Symbol> symbols = Symbols.FindCompatibleTemplates(scope, iden, qualifiedScope);
			if (symbols != null)
				return symbols;

			return FileImport != null ? Imports.FindCompatibleTemplates(scope, iden, qualifiedScope, FileImport, ModuleImportDirective) : null;
		}

		public List<Symbol> FindCompatibleTemplates(Scope scope, ScopeVariable scopeVar)
		{
			if (scopeVar.Name is TemplateInstanceIdentifier instName)
				return FindCompatibleTemplates(scope, instName, scopeVar.Scope);
			return null;
		}

		public void AddVTable(VTable table)
		{
			ScopeVariable baseScopeVar = table.Iden.GetBaseTemplate();
			if (!VTables.ContainsKey(baseScopeVar))
				VTables.Add(baseScopeVar, new List<VTable>());

			VTables[baseScopeVar].Add(table);
		}

		public VTable GetVTable(ScopeVariable scopeVar)
		{
			ScopeVariable baseScope = scopeVar.GetBaseTemplate();
			bool res = VTables.TryGetValue(baseScope, out List<VTable> tables);
			if (!res)
				return null;

			foreach (VTable table in tables)
			{
				if (table.Iden == scopeVar)
					return table;
			}

			return null;
		}

		public List<VTable> GetTemplateVTables(ScopeVariable scopeVar)
		{
			VTables.TryGetValue(scopeVar, out List<VTable> tables);
			return tables;
		}

		public void AddTypeInfo(string mangled, TypeInfo typeInfo)
		{
			TypeInfos.Add(mangled, typeInfo);
		}

		public TypeInfo GetTypeInfo(string mangled)
		{
			TypeInfos.TryGetValue(mangled, out TypeInfo info);
			return info;
		}

		public TypeInfo GetTypeInfo(SymbolType type)
		{
			switch (type)
			{
			case PointerSymbolType _:
				return TypeInfos["P"];
			case ReferenceSymbolType _:
				return TypeInfos["R"];
			case BuiltinSymbolType builtin:
			{
				string mangle = NameMangling.MangleType(builtin);
				return TypeInfos[mangle];
			}
			case AggregateSymbolType aggrType:
				return TypeInfos[aggrType.MangledIdentifier];
			case EnumSymbolType enumType:
				return TypeInfos[enumType.MangledIdentifier];
			default:
				return null;
			}
		}

		public void PrepareILStage(CompilerContext context)
		{
			// Generate default typeinfo
			AddTypeInfo("g", new TypeInfo(1, 1)); // i8
			AddTypeInfo("h", new TypeInfo(1, 1)); // u8
			AddTypeInfo("s", new TypeInfo(2, 2)); // i16
			AddTypeInfo("t", new TypeInfo(2, 2)); // u16
			AddTypeInfo("i", new TypeInfo(4, 4)); // i32
			AddTypeInfo("j", new TypeInfo(4, 4)); // u32
			AddTypeInfo("f", new TypeInfo(4, 4)); // f32
			AddTypeInfo("l", new TypeInfo(8, 8)); // i64
			AddTypeInfo("m", new TypeInfo(8, 8)); // u64
			AddTypeInfo("d", new TypeInfo(8, 8)); // f64

			AddTypeInfo("b", new TypeInfo(1, 1)); // bool
			AddTypeInfo("v", new TypeInfo(0, 0)); // void

			AddTypeInfo("c", new TypeInfo(1, 1)); // char
			AddTypeInfo("w", new TypeInfo(1, 1)); // wchar
			AddTypeInfo("r", new TypeInfo(1, 1)); // rune

			byte ptrSize = (byte)(CmdLine.IsX64 ? 8 : 4);
			AddTypeInfo("P", new TypeInfo(ptrSize, ptrSize)); // pointer
			AddTypeInfo("R", new TypeInfo(ptrSize, ptrSize)); // reference
			AddTypeInfo("A", new TypeInfo((byte)(ptrSize * 2), (ulong)(ptrSize * 2))); // array (fat pointer)

			Imports.GenerateImportedILCompUnits(context);
		}

		public void LoadImportModules()
		{
			Imports.LoadModules();
		}

		public string PrepareForLinking()
		{
			return Imports.PrepareForLinking();
		}

		public void CleanupAfterLinking()
		{
			Imports.CleanupAfterLinking();
		}

		public Dictionary<ScopeVariable, List<VTable>> GetAllVTables()
		{
			Dictionary<ScopeVariable, List<VTable>> vtables = new Dictionary<ScopeVariable, List<VTable>>();

			foreach (KeyValuePair<ScopeVariable, List<VTable>> pair in VTables)
			{
				vtables.Add(pair.Key, pair.Value);
			}

			foreach (KeyValuePair<string, Module.Module> modulePair in Imports.Modules)
			{
				ILCompUnit compUnit = modulePair.Value.ILCompUnit;
				foreach (KeyValuePair<ScopeVariable, List<VTable>> pair in compUnit.VTablesVariants)
				{
					if (vtables.TryGetValue(pair.Key, out List<VTable> tmp))
						tmp.AddRange(pair.Value);
					else
						vtables.Add(pair.Key, pair.Value);
				}
			}

			return vtables;
		}
	}
}
