﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.General
{
	public class TypeInfo
	{
		public byte Alignment;
		public ulong Size;

		public TypeInfo(byte align, ulong size)
		{
			Alignment = align;
			Size = size;
		}


		public override string ToString()
		{
			return $"Align={Alignment}, Size={Size}";
		}
	}
}
