﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MJC.General
{
	public static class Extensions
	{
		public static void Resize<T>(this List<T> list, int size, T val)
		{
			int cur = list.Count;
			if (size < cur)
			{
				list.RemoveRange(size, cur - size);
			}
			else
			{
				if (size > list.Capacity)
					list.Capacity = size;
				list.AddRange(Enumerable.Repeat<T>(val, size - cur));
			}
		}

		public static void Resize<T>(this List<T> list, int size) where T : new()
		{
			int cur = list.Count;
			if (size < cur)
			{
				list.RemoveRange(size, cur - size);
			}
			else
			{
				if (size > list.Capacity)
					list.Capacity = size;
				for (int i = cur; i < size; i++)
				{
					list.Add(new T());
				}
			}
		}

		public static void RemoveLast<T>(this List<T> list)
		{
			if (list.Count > 0)
				list.RemoveAt(list.Count - 1);
		}

		public static string ToListString<T>(this List<T> list, string separator)
		{
			string str = "";
			for (var i = 0; i < list.Count; i++)
			{
				if (i != 0)
					str += separator;

				T val = list[i];
				str += $"{val?.ToString()}";
			}

			return str;
		}

		public static bool TryGetValue<T>(this List<T> list, int index, out T elem)
		{
			elem = default(T);
			if (index >= list.Count)
				return false;
			elem = list[index];
			return true;
		}

		public static void ReplaceRange<T>(this List<T> list, int loc, IEnumerable<T> range)
		{
			int i = loc;
			foreach (T val in range)
			{
				list[i] = val;
				++i;
			}
		}

		public static int NumBitsSet(this BitArray ba)
		{
			int count = 0;
			foreach (bool b in ba)
			{
				if (b)
					++count;
			}
			return count;
		}

		public static bool ExistsAtLoc(this string str, int pos, string text)
		{
			return pos + text.Length <= str.Length &&
			       str.IndexOf(text, pos, text.Length, StringComparison.Ordinal) != -1;
		}
	}
}
