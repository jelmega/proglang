﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.SyntaxTree;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC.General
{
	public class CompileAttribute
	{
		public static string Id => "";
		public virtual string CurId => Id;
	}

	public class AlignCompileAttribute : CompileAttribute
	{
		public new static string Id => "align";
		public override string CurId => Id;
		public int Alignment;

		public AlignCompileAttribute(int alignment)
		{
			Alignment = alignment;
		}
	}

	public class DynamicallyLoadedCompileAttribute : CompileAttribute
	{
		public new static string Id => "dynamic_load";
		public override string CurId => Id;
	}

	public class ForeignCompileAttribute : CompileAttribute
	{
		public new static string Id => "foreign";
		public override string CurId => Id;
		public string Library;

		public ForeignCompileAttribute(string library)
		{
			Library = library;
		}
	}

	public class LinkNameCompileAttribute : CompileAttribute
	{
		public new static string Id => "link_name";
		public override string CurId => Id;
		public string LinkName;

		public LinkNameCompileAttribute(string linkName)
		{
			LinkName = linkName;
		}
	}

	public class CallingConventionAttribute : CompileAttribute
	{
		public new static string Id => "call_conv";
		public override string CurId => Id;
		public string CallConv;

		public CallingConventionAttribute(string callConv)
		{
			CallConv = callConv;
		}
	}

	public class DefaultVisibilityAttribute : CompileAttribute
	{
		public new static string Id => "default_visibility";
		public override string CurId => Id;
		public SemanticVisibility DefaultVisibility;

		public DefaultVisibilityAttribute(SemanticVisibility defaultVisibility)
		{
			DefaultVisibility = defaultVisibility;
		}
	}

	public class FuncPtrAttribute : CompileAttribute
	{
		public new static string Id => "func_ptr";
		public override string CurId => Id;
	}

	public class ArrPtrAttribute : CompileAttribute
	{
		public new static string Id => "arr_ptr";
		public override string CurId => Id;
	}

	public class AllowExtensionAttribute : CompileAttribute
	{
		public new static string Id => "allow_extension";
		public override string CurId => Id;
	}

	public class ExtendBuiltinAttribute : CompileAttribute
	{
		public new static string Id => "builtin_extension";
		public override string CurId => Id;
	}

	public class CoreStackArrayAttribute : CompileAttribute
	{
		public new static string Id => "core_stack_array";
		public override string CurId => Id;
	}

	public class InterpFuncAttribute : CompileAttribute
	{
		public new static string Id => "interp_func";
		public override string CurId => Id;

		public string FuncName;

		public InterpFuncAttribute(string funcName)
		{
			FuncName = funcName;
		}
	}

}
