﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MJC.General
{
	public class Nothing<T> where T : new()
	{
		public static readonly T Value = new T();
	}

	public static class Helpers
	{
		public static string RemoveRelativeParentPath(string path)
		{
			while (path.StartsWith("../") || path.StartsWith("..\\"))
			{
				path = path.Substring(3);
			}

			return path;
		}

		public static void EnsureDirectoryExists(string dir)
		{
			if (!Directory.Exists(dir))
			{
				Directory.CreateDirectory(dir);
			}
		}

		public static string GetModuleFilePath(string fullModule)
		{
			string tmp = fullModule.Replace('.', '/');
			if (!tmp.EndsWith('/'))
				tmp += '/';
			return tmp;
		}
	}
}
