﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.General
{
	class TypeinfoHelpers
	{
		public static TypeInfo GetMinimalTypeInfo(SymbolType type, CompilerContext context)
		{
			switch (type)
			{
			case PointerSymbolType _:
				return context.GetTypeInfo("P");
			case ReferenceSymbolType _:
				return context.GetTypeInfo("P");
			case BuiltinSymbolType builtin:
			{
				string iden = NameMangling.MangleType(builtin);
				return context.GetTypeInfo(iden);
			}
			case AggregateSymbolType aggrType:
			{
				return context.GetTypeInfo(aggrType.MangledIdentifier);
			}
			case EnumSymbolType enumType:
			{
				return context.GetTypeInfo(enumType.MangledIdentifier);
			}
			case ArraySymbolType arrType:
			{
				if (arrType.ArraySize != ulong.MaxValue)
				{
					TypeInfo tmp = GetMinimalTypeInfo(arrType.BaseType, context);
					return new TypeInfo(tmp.Alignment, tmp.Size * arrType.ArraySize);
				}

				return context.GetTypeInfo("A");
			}
			case TupleSymbolType tupleType:
			{
				byte align = 0;
				ulong size = 0;
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					TypeInfo tmp = GetMinimalTypeInfo(subType, context);

					if (align < tmp.Alignment)
						align = tmp.Alignment;

					ulong diff = size % align;
					if (diff != 0)
						size += (align - diff);

					size += tmp.Size;
				}

				return new TypeInfo(align, size);
			}
			}


			return null;
		}
	}
}
