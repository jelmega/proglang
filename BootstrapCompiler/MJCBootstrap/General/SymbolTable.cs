﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.SyntaxTree;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC.General
{	
	public class SymbolTable
	{
		public Scope Scope;
		public CompilerContext CompilerContext;
		public SymbolTable Parent;
		public Dictionary<Identifier, SymbolTable> SubTables = new Dictionary<Identifier, SymbolTable>();
		public Dictionary<Identifier, List<Symbol>> Symbols = new Dictionary<Identifier, List<Symbol>>();

		public SymbolTable(CompilerContext compilerContext)
		{
			CompilerContext = compilerContext;
		}

		public SymbolTable(CompilerContext compilerContext, SymbolTable parent, Identifier subScope)
		{
			CompilerContext = compilerContext;
			Parent = parent;
			Scope = new Scope();

			if (parent.Scope != null)
				Scope.Names.AddRange(parent.Scope.Names);

			Scope.Names.Add(subScope);
		}

		public bool AddSymbol(Symbol symbol)
		{
			if (symbol.Scope.GetBaseTemplateScope() == Scope || (Scope == null && symbol.Scope.IsEmpty()))
			{
				Identifier iden = symbol.Identifier.GetBaseIdentifier();
				bool exist = Symbols.ContainsKey(iden);
				if (!exist)
					Symbols.Add(iden, new List<Symbol>());

				Symbols[iden].Add(symbol);

				// Set the symbol's table
				symbol.CompilerContext = CompilerContext;
				return exist;
			}

			int idx = Scope?.Names.Count ?? 0;
			Identifier tmp = symbol.Scope.Names[idx].GetSymTableScopeIden();

			if (!SubTables.ContainsKey(tmp))
			{
				SubTables.Add(tmp, new SymbolTable(CompilerContext, this, tmp));
			}
			return SubTables[tmp].AddSymbol(symbol);
		}

		public Symbol FindDefinition(Scope curScope, ScopeVariable scopeVar, int location)
		{
			return FindDefinition(curScope, scopeVar.Name, location, scopeVar.Scope);
		}

		public Symbol FindDefinition(Scope curScope, Identifier name, int location, Scope qualifiedScope = null)
		{
			SymbolTable table = FindClosestTable(curScope);

			if (table == null)
				return null;

			Symbol symbol = table.FindDefinitionDescend(name, location, qualifiedScope);
			return symbol;
		}

		public Symbol FindFunctionWithParameters(Scope curScope, Identifier name, SymbolType receiverType, List<SymbolType> argTypes, Scope qualifiedScope)
		{
			SymbolTable table = FindClosestTable(curScope);
			if (table != null)
			{
				Symbol symbol = table.FindFunctionWithParametersDescend(name, receiverType, argTypes, qualifiedScope);
				return symbol;
			}

			return null;
		}

		public Symbol FindType(Scope curScope, ScopeVariable scopeVar)
		{
			return FindType(curScope, scopeVar.Name, scopeVar.Scope);
		}

		public Symbol FindType(Scope curScope, Identifier name, Scope qualifiedScope = null)
		{
			SymbolTable table = FindClosestTable(curScope);

			if (table == null)
				return null;

			Identifier baseIden = name.GetBaseIdentifier();
			Symbol symbol = table.FindTypeDescend(baseIden, qualifiedScope);
			return symbol;
		}

		public Symbol FindTemplate(Scope curScope, ScopeVariable scopeVar)
		{
			if (scopeVar.Name is TemplateInstanceIdentifier instName)
				return FindTemplate(curScope, instName, scopeVar.Scope);
			return null;
		}

		public Symbol FindTemplate(Scope curScope, TemplateInstanceIdentifier iden, Scope qualifiedScope = null)
		{
			SymbolTable table = FindClosestTable(curScope);
			if (table == null)
				return null;

			Symbol symbol = table.FindTemplateDescend(iden, qualifiedScope);
			return symbol;
		}

		public List<Symbol> FindCompatibleTemplates(Scope curScope, ScopeVariable scopeVar)
		{
			if (scopeVar.Name is TemplateInstanceIdentifier instName)
				return FindCompatibleTemplates(curScope, instName, scopeVar.Scope);
			return null;
		}

		public List<Symbol> FindCompatibleTemplates(Scope curScope, TemplateInstanceIdentifier iden, Scope qualifiedScope = null)
		{
			SymbolTable table = FindClosestTable(curScope);
			if (table == null)
				return null;

			List<Symbol> symbols = table.FindCompatibleTemplatesDescend(iden, qualifiedScope);
			return symbols;
		}

		public SymbolTable FindClosestTable(Scope scope)
		{
			SymbolTable curTable = this;
			for (int i = 0; i < scope.Names.Count; i++)
			{
				Identifier tmp = scope.Names[i].GetSymTableScopeIden();
				if (!curTable.SubTables.ContainsKey(tmp))
					return curTable;
				curTable = curTable.SubTables[tmp];
			}
			return curTable;
		}

		public SymbolTable FindTable(Scope scope)
		{
			SymbolTable curTable = this;
			for (int i = 0; i < scope.Names.Count; i++)
			{
				Identifier tmp = scope.Names[i].GetSymTableScopeIden();
				if (!curTable.SubTables.ContainsKey(tmp))
					return null;
				curTable = curTable.SubTables[tmp];
			}
			return curTable;
		}

		private Symbol FindDefinitionDescend(Identifier name, int location, Scope qualifiedScope)
		{
			Symbol symbol = FindDefinitionNoDescend(name, location, qualifiedScope);
			if (symbol != null)
				return symbol;

			if (Parent != null)
				return Parent.FindDefinitionDescend(name, location, qualifiedScope);
			return null;
		}

		public Symbol FindDefinitionNoDescend(Identifier iden, int location, Scope qualifiedScope)
		{
			Dictionary<Identifier, List<Symbol>> symbols = Symbols;
			if (qualifiedScope != null && !qualifiedScope.IsEmpty())
			{
				SymbolTable qualTable = FindTable(qualifiedScope);
				symbols = qualTable?.Symbols;
			}

			Identifier baseIden = iden.GetBaseIdentifier();
			if (symbols != null && symbols.ContainsKey(baseIden))
			{
				List<Symbol> subSyms = symbols[baseIden];
				
				foreach (Symbol sym in subSyms)
				{
					if ((sym.Kind != SymbolKind.LocalVar || location > sym.Location) && sym.Identifier == iden)
						return sym;
				}
			}
			return null;
		}

		private Symbol FindFunctionWithParametersDescend(Identifier iden, SymbolType receiverType, List<SymbolType> argTypes, Scope qualifiedScope)
		{
			Dictionary<Identifier, List<Symbol>> symbols = Symbols;
			if (qualifiedScope != null && !qualifiedScope.IsEmpty())
			{
				SymbolTable qualTable = FindTable(qualifiedScope);
				symbols = qualTable?.Symbols;
			}
			
			if (symbols != null && symbols.ContainsKey(iden))
			{
				List<Symbol> subSyms = symbols[iden];

				Symbol bestSymbol = null;
				int minImplicitCasts = 0;
				foreach (Symbol symbol in subSyms)
				{
					if (symbol.Kind != SymbolKind.Function && symbol.Kind != SymbolKind.Method &&
					    symbol.Kind != SymbolKind.TemplateInstance)
						continue;

					FunctionSymbolType funcType = null;
					if (symbol.Type is FunctionSymbolType funcTmp)
					{
						funcType = funcTmp;
					}
					else if(symbol.Type is TemplateInstanceSymbolType instTmp &&
					        instTmp.TemplateSymbol.Type is FunctionSymbolType funcBase)
					{
						funcType = funcBase;
					}
					
					if (funcType == null)
						continue;

					bool receiverAsArg = false;
					if (funcType.ReceiverType != null)
					{
						if (receiverType != null)
						{
							SymbolType funcRecieverType = funcType.ReceiverType;
							if (funcRecieverType is ReferenceSymbolType refType)
								funcRecieverType = refType.BaseType;

							SymbolType recType = receiverType;
							if (recType is TemplateInstanceSymbolType instType)
							{
								recType = instType.TemplateSymbol.Type;
							}

							if (funcRecieverType != recType)
								continue;
						}
						else
						{
							receiverAsArg = true;
						}
					}
					else if (receiverType != null)
					{
						continue;
					}

					// to many arguments for function
					int numArgs = argTypes?.Count ?? 0;
					int numSymbolArgs = funcType.ParamTypes?.Count ?? 0;
					if (receiverAsArg)
						++numSymbolArgs;

					if (numArgs > numSymbolArgs)
						continue;
					// Not enough arguments for function and additional params don't have default value(s)
					if (numArgs < numSymbolArgs && !funcType.HasDefaultValues[numArgs + 1])
						continue;

					bool found = true;
					int numImplicitCasts = 0;
					if (numArgs > 0)
					{
						if (receiverAsArg)
						{
							for (int i = 0; i < numArgs + 1; ++i)
							{
								SymbolType fromType = argTypes[i];
								SymbolType toType;
								if (i == 0)
									toType = funcType.ReceiverType;
								else
									toType = funcType.ParamTypes[i - 1];

								if (fromType != toType)
								{
									++numImplicitCasts;
									if (!CanImplicitlyConvert(Scope, fromType, toType))
									{
										found = false;
										break;
									}
								}
							}
						}
						else
						{
							for (int i = 0; i < numArgs; ++i)
							{
								SymbolType fromType = argTypes[i];
								SymbolType toType = funcType.ParamTypes[i];

								if (fromType != toType)
								{
									++numImplicitCasts;
									if (!CanImplicitlyConvert(Scope, fromType, toType))
									{
										found = false;
										break;
									}
								}
							}
						}
					}
					
					// If no implicit casts happened, we are sure we got the best function
					if (found && numImplicitCasts == 0)
						return symbol;

					// If implicit casts happened, does it better match the provided types
					if (bestSymbol == null || numImplicitCasts < minImplicitCasts)
					{
						bestSymbol = symbol;
						minImplicitCasts = numImplicitCasts;
					}
				}
				
				// Return best fitting symbol
				// TODO: Prefer least implicit casts over closest function ???
				if (bestSymbol != null)
					return bestSymbol;
			}

			if (Parent != null)
				return Parent.FindFunctionWithParametersDescend(iden, receiverType, argTypes, qualifiedScope);
			return null;
		}

		private Symbol FindTypeDescend(Identifier iden, Scope qualifiedScope)
		{
			Dictionary<Identifier, List<Symbol>> symbols = Symbols;
			if (qualifiedScope != null && !qualifiedScope.IsEmpty())
			{
				SymbolTable qualTable = FindTable(qualifiedScope);
				symbols = qualTable?.Symbols;
			}
			
			if (symbols != null && symbols.ContainsKey(iden))
			{
				List<Symbol> subSyms = symbols[iden];

				foreach (Symbol symbol in subSyms)
				{
					if (symbol.IsType() && symbol.Identifier == iden)
						return symbol;
				}
			}

			if (Parent != null)
				return Parent.FindTypeDescend(iden, qualifiedScope);
			return null;
		}

		private Symbol FindTemplateDescend(TemplateInstanceIdentifier iden, Scope qualifiedScope)
		{
			Dictionary<Identifier, List<Symbol>> symbols = Symbols;
			if (qualifiedScope != null && !qualifiedScope.IsEmpty())
			{
				SymbolTable qualTable = FindTable(qualifiedScope);
				symbols = qualTable?.Symbols;
			}

			if (symbols != null)
			{
				foreach (KeyValuePair<Identifier, List<Symbol>> pair in symbols)
				{
					if (pair.Key is TemplateDefinitionIdentifier defName &&
					    defName.Iden == iden.Iden &&
					    defName.Parameters.Count >= iden.Arguments.Count) // symbol cannot have less params than the template to find
					{
						foreach (Symbol symbol in pair.Value)
						{
							if (symbol.Identifier is TemplateDefinitionIdentifier symDefName &&
								TemplateHelpers.IsCompatible(symDefName, iden))
							{
								// Find the non-specialized symbol
								foreach (Symbol sym in pair.Value)
								{
									TemplateDefinitionIdentifier tmp = sym.Identifier as TemplateDefinitionIdentifier;
									bool isSpecialized = false;
									foreach (TemplateParameter param in tmp.Parameters)
									{
										if (param.HasSpecialization())
										{
											isSpecialized = true;
											break;
										}
									}

									if (!isSpecialized)
										return sym;
								}
							}
						}
					}
				}
			}


			if (Parent != null)
				return Parent.FindTemplateDescend(iden, qualifiedScope);
			return null;
		}

		private List<Symbol> FindCompatibleTemplatesDescend(TemplateInstanceIdentifier iden, Scope qualifiedScope)
		{
			Dictionary<Identifier, List<Symbol>> symbols = Symbols;
			if (qualifiedScope != null && !qualifiedScope.IsEmpty())
			{
				SymbolTable qualTable = FindTable(qualifiedScope);
				symbols = qualTable?.Symbols;
			}

			List<Symbol> compatSyms = new List<Symbol>();
			if (symbols != null)
			{
				foreach (KeyValuePair<Identifier, List<Symbol>> pair in symbols)
				{
					if (pair.Key is TemplateDefinitionIdentifier defName &&
					    defName.Iden == iden.Iden &&
					    defName.Parameters.Count >= iden.Arguments.Count) // symbol cannot have less params than the template to find
					{
						foreach (Symbol symbol in pair.Value)
						{
							if (symbol.Identifier is TemplateDefinitionIdentifier symDefName &&
							    TemplateHelpers.IsCompatible(symDefName, iden))
							{
								compatSyms.Add(symbol);
							}
						}
					}
				}
			}

			if (compatSyms.Count > 0)
				return compatSyms;


			if (Parent != null)
				return Parent.FindCompatibleTemplatesDescend(iden, qualifiedScope);
			return null;
		}


		public void Merge(SymbolTable otherTable)
		{
			Stack<SymbolTable> subTables = new Stack<SymbolTable>();
			subTables.Push(otherTable);

			while (subTables.Count > 0)
			{
				SymbolTable subTable = subTables.Pop();

				foreach (KeyValuePair<Identifier, List<Symbol>> pair in subTable.Symbols)
				{
					foreach (Symbol symbol in pair.Value)
					{
						symbol.CompilerContext = CompilerContext;
						AddSymbol(symbol);
					}
				}

				foreach (KeyValuePair<Identifier, SymbolTable> pair in subTable.SubTables)
				{
					subTables.Push(pair.Value);
				}
			}
		}

		public void ReplaceAliases()
		{
			Stack<SymbolTable> subTables = new Stack<SymbolTable>();
			subTables.Push(this);

			while (subTables.Count > 0)
			{
				SymbolTable subTable = subTables.Pop();

				foreach (KeyValuePair<Identifier, List<Symbol>> pair in subTable.Symbols)
				{
					foreach (Symbol symbol in pair.Value)
					{
						SymbolType aliasType = GetReplacedAliasType(symbol.Scope, symbol.Type);

						if (aliasType != null)
							symbol.Type = aliasType;
					}
				}

				foreach (KeyValuePair<Identifier, SymbolTable> pair in subTable.SubTables)
				{
					subTables.Push(pair.Value);
				}
			}
		}

		private SymbolType GetReplacedAliasType(Scope curScope, SymbolType type)
		{
			if (type == null)
				return null;

			Stack<SymbolType> typeStack = new Stack<SymbolType>();

			SymbolType tmpType = type;
			while (tmpType != null)
			{
				typeStack.Push(tmpType);

				tmpType = tmpType.GetBaseType();
			}

			tmpType = typeStack.Pop();

			switch (tmpType)
			{
			case TupleSymbolType tupleType:
			{
				List<SymbolType> tmpList = new List<SymbolType>();
				bool hadAlias = false;
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					SymbolType tmp = GetReplacedAliasType(curScope, subType);

					if (tmp != null)
					{
						tmpList.Add(tmp);
						hadAlias = true;
					}
					else
					{
						tmpList.Add(subType);
					}
				}

				if (hadAlias)
					tmpType = SymbolType.TupleType(tmpList);
				break;
			}
			case FunctionSymbolType funcType:
			{
				if (funcType.ParamTypes != null)
				{
					for (var i = 0; i < funcType.ParamTypes.Count; i++)
					{
						SymbolType parameterType = funcType.ParamTypes[i];
						SymbolType tmp = GetReplacedAliasType(curScope, parameterType);

						if (tmp != null)
						{
							funcType.ParamTypes[i] = tmp;
						}
					}
				}

				SymbolType recType = GetReplacedAliasType(curScope, funcType.ReceiverType);
				if (recType != null)
					funcType.ReceiverType = recType;
				SymbolType retTmp = GetReplacedAliasType(curScope, funcType.ReturnType);
				if (retTmp != null)
					funcType.ReturnType = retTmp;

				break;
			}
			case DelegateSymbolType delType:
			{
				if (delType.ParamTypes != null)
				{
					for (var i = 0; i < delType.ParamTypes.Count; i++)
					{
						SymbolType parameterType = delType.ParamTypes[i];
						SymbolType tmp = GetReplacedAliasType(curScope, parameterType);

						if (tmp != null)
						{
							delType.ParamTypes[i] = tmp;
						}
					}
				}
				
				SymbolType retTmp = GetReplacedAliasType(curScope, delType.ReturnType);
				if (retTmp != null)
					delType.ReturnType = retTmp;
				break;
			}
			case UnknownSymbolType unknownType:
			{
				Symbol sym = FindType(curScope, unknownType.Identifier);

				if (sym != null && sym.Kind == SymbolKind.TypeAlias)
				{
					if (unknownType.Identifier.Name == sym.Type.GetInnerType().GetIdentifier())
					{
						ErrorSystem.Error($"`typealias` reference itself: '{unknownType.Identifier}'");
						return null;
					}
					
					SymbolType tmp = GetReplacedAliasType(curScope, sym.Type);

					if (tmp != null)
						tmpType = tmp;
					else
						tmpType = sym.Type;
				}

				break;
			}
			}

			if (tmpType == null)
				return null;

			if (typeStack.Count == 0)
				return tmpType;

			SymbolType prevType = typeStack.Pop();

			switch (prevType)
			{
			case PointerSymbolType ptrType:
				ptrType.BaseType = tmpType;
				break;
			case ReferenceSymbolType refType:
				refType.BaseType = tmpType;
				break;
			case NullableSymbolType nullableType:
				nullableType.BaseType = tmpType;
				break;
			case ArraySymbolType arrType:
				arrType.BaseType = tmpType;
				break;
			}

			if (typeStack.Count == 0)
				return prevType;

			return typeStack.Pop();
		}

		public void UpdateTypes()
		{
			Stack<SymbolTable> subTables = new Stack<SymbolTable>();
			subTables.Push(this);

			while (subTables.Count > 0)
			{
				SymbolTable subTable = subTables.Pop();

				foreach (KeyValuePair<Identifier, List<Symbol>> pair in subTable.Symbols)
				{
					foreach (Symbol symbol in pair.Value)
					{
						switch (symbol.Kind)
						{
						case SymbolKind.Variable:
						case SymbolKind.LocalVar:
						case SymbolKind.EnumMember:
						case SymbolKind.Function:
						case SymbolKind.Method:
						case SymbolKind.Delegate:
						case SymbolKind.TypeAlias:
							symbol.UpdateType();
							break;
						case SymbolKind.Enum:
						{
							EnumSymbolType enumType = symbol.Type as EnumSymbolType;
							enumType.UpdateSimpleEnum();
							break;
						}
						}
					}
				}

				foreach (KeyValuePair<Identifier, SymbolTable> pair in subTable.SubTables)
				{
					subTables.Push(pair.Value);
				}
			}
		}

		public void GenerateMangledNames()
		{
			Stack<SymbolTable> subTables = new Stack<SymbolTable>();
			HashSet<string> aggregateScopes = new HashSet<string>();
			subTables.Push(this);

			while (subTables.Count > 0)
			{
				SymbolTable subTable = subTables.Pop();

				foreach (KeyValuePair<Identifier, List<Symbol>> pair in subTable.Symbols)
				{
					foreach (Symbol symbol in pair.Value)
					{
						// Don't mangle template instances, names are generated during instantiation in 
						if (symbol.Type is TemplateInstanceSymbolType &&
						    symbol.Kind != SymbolKind.Typedef)
							continue;

						if (symbol.Kind == SymbolKind.Struct ||
						    symbol.Kind == SymbolKind.Interface)
							aggregateScopes.Add(symbol.ScopeVar.ToString());

						if (symbol.Kind != SymbolKind.LocalVar)
						{
							if (symbol.CompileAttribs.ContainsKey(LinkNameCompileAttribute.Id))
							{
								LinkNameCompileAttribute attrib = symbol.CompileAttribs[LinkNameCompileAttribute.Id] as LinkNameCompileAttribute;
								symbol.MangledName = attrib?.LinkName ?? "__unknown_link_name";
							}
							else
							{
								symbol.MangledName = NameMangling.Mangle(symbol);
							}

						}
					}
				}

				foreach (KeyValuePair<Identifier, SymbolTable> pair in subTable.SubTables)
				{
					subTables.Push(pair.Value);
				}
			}
		}

		private bool CanImplicitlyConvert(Scope curScope, SymbolType fromType, SymbolType toType)
		{
			if (fromType == toType)
				return true;

			// Special case for null
			if (fromType is BuiltinSymbolType fromNull && fromNull.Builtin == BuiltinTypes.Null &&
			    toType is PointerSymbolType)
			{
				return true;
			}

			// Special case for string literals
			{
				if (fromType is BuiltinSymbolType fromStrLit && fromStrLit.Builtin == BuiltinTypes.StringLiteral &&
				    toType is PointerSymbolType ptrType &&
				    ptrType.BaseType is BuiltinSymbolType ptrBuiltin &&
				    (ptrBuiltin.Builtin == BuiltinTypes.Char ||
				     ptrBuiltin.Builtin == BuiltinTypes.WChar ||
				     ptrBuiltin.Builtin == BuiltinTypes.Rune ||
				     ptrBuiltin.Builtin == BuiltinTypes.Void))
				{
					return true;
				}
			}

			// Special case to void pointer
			{
				if (fromType is PointerSymbolType &&
				    toType is PointerSymbolType ptrType &&
					ptrType.BaseType is BuiltinSymbolType ptrBuiltin &&
					ptrBuiltin.Builtin == BuiltinTypes.Void)
				{
					return true;
				}
			}

			// Special case for arrays
			{
				if (fromType is ArraySymbolType arrType &&
				    toType is PointerSymbolType ptrType &&
				    (arrType.BaseType == ptrType.BaseType ||
					 // Also allow implicit conversion from arrays to void pointers (double implicit cast)
				     ptrType.BaseType is BuiltinSymbolType builtin && builtin.Builtin == BuiltinTypes.Void))
				{
					return true;
				}
			}

			return fromType.HasBuiltinConvertion(toType);
		}

		public void PrintTable(int indent = 0)
		{
			string indentStr = "".PadLeft(indent * 4, ' ');

			if (indent == 0)
				Console.WriteLine("Symbol table:");
			else
				Console.WriteLine($"{indentStr}{Scope.Names.Last()}:");

			foreach (KeyValuePair<Identifier, List<Symbol>> pair in Symbols)
			{
				foreach (Symbol symbol in pair.Value)
				{
					Console.WriteLine($"{indentStr}    {symbol.Location,-4}| {symbol.Kind,-10}| {symbol.Identifier} | {symbol.Type} | {symbol.MangledName}");
				}
			}

			++indent;
			foreach (KeyValuePair<Identifier, SymbolTable> pair in SubTables)
			{
				pair.Value.PrintTable(indent);
			}
		}

		public override string ToString()
		{
			return $"mod sym table: {Scope}";
		}
	}
}
