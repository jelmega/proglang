﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MJC.Module;
using MJC.SyntaxTree.SemanticAnalysis;

// TODO: import aliases
namespace MJC.General
{
	public class FileImportDirective
	{
		public bool IsStatic; // requires full scope
		public bool IsPublic; // exposed to module
		public bool AllSymbols;
		public List<string> RequestedSymbols;
		public string ModuleName;
		public string PackageName;
		public int FileLocation;
		public Scope ContainingScope;
		public Scope DirectiveScope;

		public FileImportDirective(string packageName, string moduleName)
		{
			PackageName = packageName;
			ModuleName = moduleName;

			DirectiveScope = new Scope();
			if (!string.IsNullOrEmpty(PackageName))
			{
				string[] parts = PackageName.Split('.');
				foreach (string part in parts)
				{
					DirectiveScope.Names.Add(new NameIdentifier(part));
				}
			}
			DirectiveScope.Names.Add(new NameIdentifier(ModuleName));
		}
	}

	public class FileImports
	{
		public List<FileImportDirective> Directives = new List<FileImportDirective>();
		public HashSet<string> UsedModules = new HashSet<string>();

		public void AddImport(Scope scope, int location, string module, List<string> symbols = null)
		{
			FileImportDirective directive;
			int lastDot = module.LastIndexOf('.');
			if (lastDot == -1)
			{
				directive = new FileImportDirective(null, module);
			}
			else
			{
				string packageName = module.Substring(0, lastDot);
				string moduleName = module.Substring(lastDot + 1);

				directive = new FileImportDirective(packageName, moduleName);
			}

			directive.FileLocation = location;
			directive.ContainingScope = scope;

			if (symbols == null)
			{
				directive.AllSymbols = true;
			}
			else
			{
				directive.RequestedSymbols.AddRange(symbols);
			}
			Directives.Add(directive);
		}

		public void AddUsedModule(FileImportDirective directive)
		{
			string fullIden = "";
			if (!string.IsNullOrEmpty(directive.PackageName))
				fullIden += directive.PackageName + '.';
			fullIden += directive.ModuleName;

			if (!UsedModules.Contains(fullIden))
				UsedModules.Add(fullIden);
		}
	}

	public class ImportTable
	{
		private List<FileImports> _fileImports = new List<FileImports>();
		private Dictionary<string, string[]> _moduleIdens = new Dictionary<string, string[]>();
		private SymbolTable _symbolTable;
		private List<string> _tempObjFiles = new List<string>();

		public Dictionary<string, Module.Module> Modules = new Dictionary<string, Module.Module>();

		public ImportTable(CompilerContext globalTable)
		{
			_symbolTable = new SymbolTable(globalTable);

			// TODO: Move this
			// Add core modules
			//_moduleIdens.Add("core.builtin", new []{ "core", "builtin" });
		}
		
		public void AddFileImports(FileImports imports)
		{
			_fileImports.Add(imports);

			foreach (FileImportDirective directive in imports.Directives)
			{
				string fullIden = "";
				if (!string.IsNullOrEmpty(directive.PackageName))
					fullIden += directive.PackageName + '.';
				fullIden += directive.ModuleName;

				if (!_moduleIdens.ContainsKey(fullIden))
					_moduleIdens.Add(fullIden, new []{ directive.PackageName, directive.ModuleName });
			}
		}

		public Symbol FindDefinition(Scope scope, Identifier iden, int location, Scope baseScope, FileImports fileImports, FileImportDirective moduleImportDirective)
		{
			if (moduleImportDirective != null)
			{
				Symbol symbol = _symbolTable.FindDefinition(moduleImportDirective.DirectiveScope, iden, -1, baseScope);
				if (symbol != null)
				{
					return symbol;
				}
			}
			else
			{
				// Get all available directives from the current scope
				List<FileImportDirective>[] availableDirectives = GetAvailableDirectives(scope, location, fileImports);
				// Check if symbol is available from current scope
				for (int i = availableDirectives.Length - 1; i >= 0; i--)
				{
					List<FileImportDirective> directives = availableDirectives[i];
					if (directives == null)
						continue;

					foreach (FileImportDirective directive in directives)
					{
						Symbol symbol = _symbolTable.FindDefinition(directive.DirectiveScope, iden, -1, baseScope);
						if (symbol != null)
						{
							fileImports.AddUsedModule(directive);
							return symbol;
						}
					}
				}
			}

			return null;
		}

		public Symbol FindFunction(Scope scope, Identifier funcName, SymbolType recType, List<SymbolType> argTypes, Scope qualifiedScope, int location, FileImports fileImports, FileImportDirective moduleImportDirective)
		{
			if (moduleImportDirective != null)
			{
				Symbol symbol = _symbolTable.FindFunctionWithParameters(moduleImportDirective.DirectiveScope, funcName, recType, argTypes, qualifiedScope);
				if (symbol != null)
				{
					return symbol;
				}
			}
			else
			{
				// Get all available directives from the current scope
				List<FileImportDirective>[] availableDirectives = GetAvailableDirectives(scope, location, fileImports);
				// Check if symbol is available from current scope
				for (int i = availableDirectives.Length - 1; i >= 0; i--)
				{
					List<FileImportDirective> directives = availableDirectives[i];
					if (directives == null)
						continue;

					foreach (FileImportDirective directive in directives)
					{
						Symbol symbol = _symbolTable.FindFunctionWithParameters(directive.DirectiveScope, funcName, recType, argTypes, qualifiedScope);
						if (symbol != null)
						{
							fileImports.AddUsedModule(directive);
							return symbol;
						}
					}
				}
			}

			return null;
		}

		public Symbol FindType(Scope scope, Identifier iden, Scope baseScope, FileImports fileImports, FileImportDirective moduleImportDirective)
		{
			if (moduleImportDirective != null)
			{
				Symbol symbol = _symbolTable.FindType(moduleImportDirective.DirectiveScope, iden, baseScope);
				if (symbol != null)
				{
					return symbol;
				}
			}
			else
			{
				// Get all available directives from the current scope
				List<FileImportDirective>[] availableDirectives = GetAvailableDirectives(scope, -1, fileImports);
				// Check if symbol is available from current scope
				for (int i = availableDirectives.Length - 1; i >= 0; i--)
				{
					List<FileImportDirective> directives = availableDirectives[i];
					if (directives == null)
						continue;

					foreach (FileImportDirective directive in directives)
					{
						Symbol symbol = _symbolTable.FindType(directive.DirectiveScope, iden, baseScope);
						if (symbol != null)
						{
							fileImports.AddUsedModule(directive);
							return symbol;
						}
					}
				}
			}

			return null;
		}

		public Symbol FindTemplate(Scope scope, TemplateInstanceIdentifier iden, Scope baseScope, FileImports fileImports, FileImportDirective moduleImportDirective)
		{
			if (moduleImportDirective != null)
			{
				Symbol symbol = _symbolTable.FindTemplate(moduleImportDirective.DirectiveScope, iden, baseScope);
				if (symbol != null)
				{
					return symbol;
				}
			}
			else
			{
				// Get all available directives from the current scope
				List<FileImportDirective>[] availableDirectives = GetAvailableDirectives(scope, -1, fileImports);
				// Check if symbol is available from current scope
				for (int i = availableDirectives.Length - 1; i >= 0; i--)
				{
					List<FileImportDirective> directives = availableDirectives[i];
					if (directives == null)
						continue;

					foreach (FileImportDirective directive in directives)
					{
						Symbol symbol = _symbolTable.FindTemplate(directive.DirectiveScope, iden, baseScope);
						if (symbol != null)
						{
							fileImports.AddUsedModule(directive);
							return symbol;
						}
					}
				}
			}

			return null;
		}

		public List<Symbol> FindCompatibleTemplates(Scope scope, TemplateInstanceIdentifier iden, Scope baseScope, FileImports fileImports, FileImportDirective importDirective)
		{
			if (importDirective != null)
			{
				List<Symbol> symbols = _symbolTable.FindCompatibleTemplates(importDirective.DirectiveScope, iden, baseScope);
				if (symbols != null)
				{
					return symbols;
				}
			}
			else
			{
				// Get all available directives from the current scope
				List<FileImportDirective>[] availableDirectives = GetAvailableDirectives(scope, -1, fileImports);
				// Check if symbol is available from current scope
				for (int i = availableDirectives.Length - 1; i >= 0; i--)
				{
					List<FileImportDirective> directives = availableDirectives[i];
					if (directives == null)
						continue;

					foreach (FileImportDirective directive in directives)
					{
						List<Symbol> symbols = _symbolTable.FindCompatibleTemplates(directive.DirectiveScope, iden, baseScope);
						if (symbols != null)
						{
							fileImports.AddUsedModule(directive);
							return symbols;
						}
					}
				}
			}

			return null;
		}

		List<FileImportDirective>[] GetAvailableDirectives(Scope scope, int location, FileImports fileImports)
		{
			List<FileImportDirective>[] availableDirectives = new List<FileImportDirective>[scope.Names.Count];
			foreach (FileImportDirective directive in fileImports.Directives)
			{
				if (scope.IsEmpty() || scope.ContainsScope(directive.ContainingScope))
				{
					if (directive.FileLocation > location)
						continue;

					int depth = directive.ContainingScope.Names.Count - 1;

					if (availableDirectives[depth] == null)
						availableDirectives[depth] = new List<FileImportDirective>();

					availableDirectives[depth].Add(directive);
				}
			}
			return availableDirectives;
		}

		public void LoadModules()
		{
			// Extract expected modules
			Dictionary<string, string[]> expectedModules = new Dictionary<string, string[]>();
			foreach (KeyValuePair<string, string[]> moduleIden in _moduleIdens)
			{
				expectedModules.Add(moduleIden.Key, moduleIden.Value);
			}

			// Get available modules
			List<string> availableModules = new List<string>();
			foreach (string file in Directory.EnumerateFiles(Directory.GetCurrentDirectory()))
			{
				if (file.EndsWith(".mjm"))
				{
					availableModules.Add(file);
				}
			}

			foreach (string dir in CmdLine.IncludeDirs)
			{
				foreach (string file in Directory.EnumerateFiles(dir))
				{
					if (file.EndsWith(".mjm"))
					{
						availableModules.Add(file);
					}
				}
			}

			// load modules
			foreach (string moduleFile in availableModules)
			{
				byte[] data = File.ReadAllBytes(moduleFile);

				// Load header and check for module or package name
				ByteBuffer buffer = new ByteBuffer(data);
				ModuleHeader header = new ModuleHeader(buffer);

				string fullIden = "";
				if (!string.IsNullOrEmpty(header.Package))
					fullIden += header.Package + '.';
				fullIden += header.Module;

				if (expectedModules.ContainsKey(fullIden))
				{
					Module.Module module = ModuleImporter.Import(data);
					Modules.Add(fullIden, module);
					expectedModules.Remove(fullIden);

					if (expectedModules.Count == 0)
						break;
				}
			}

			if (expectedModules.Count > 0)
			{
				// TODO: Error
			}

			// extract symbols from module
			foreach (KeyValuePair<string, Module.Module> pair in Modules)
			{
				Module.Module module = pair.Value;
				List<Identifier> scopeNames = new List<Identifier>();
				if (!string.IsNullOrEmpty(module.Header.Package))
				{
					string[] subNames = module.Header.Package.Split('.');
					foreach (string subName in subNames)
						scopeNames.Add(new NameIdentifier(subName));
				}
				scopeNames.Add(new NameIdentifier(module.Header.Module));
				Scope moduleScope = new Scope(scopeNames);

				// functions

				// TODO
				if (module.Header.Package == "__TODO__")
				{
					if (module.TryGetTable("pak", out Table pakTable))
					{
						foreach (TableEntry baseTableEntry in pakTable.Entries)
						{
							SymbolEntry entry = baseTableEntry as SymbolEntry;
							NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);
							_symbolTable.AddSymbol(symbol);
						}
					}
				}

				if (module.TryGetTable("pub", out Table pubTable))
				{
					foreach (TableEntry baseTableEntry in pubTable.Entries)
					{
						SymbolEntry entry = baseTableEntry as SymbolEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);

						if (symbol == null)
						{
							int start = 0;
							(SymbolType type, SymbolKind kind) = NameMangling.DemangleType(entry.MangledType, ref start);

							symbol = new Symbol(moduleScope, null, new NameIdentifier(entry.MangledName), kind, SemanticVisibility.Public, SemanticAttributes.None);
							symbol.Type = type;
						}

						symbol.MangledName = entry.MangledName;
						symbol.Visibility = SemanticVisibility.Public;
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);
					}
				}

				if (module.TryGetTable("exp", out Table expTable))
				{
					foreach (TableEntry baseTableEntry in expTable.Entries)
					{
						SymbolEntry entry = baseTableEntry as SymbolEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);

						if (symbol == null)
						{
							int start = 0;
							(SymbolType type, SymbolKind kind) = NameMangling.DemangleType(entry.MangledType, ref start);

							symbol = new Symbol(moduleScope, null, new NameIdentifier(entry.MangledName), kind, SemanticVisibility.Export, SemanticAttributes.None);
							symbol.Type = type;
						}

						symbol.MangledName = entry.MangledName;
						symbol.Visibility = SemanticVisibility.Import;
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);
					}
				}

				// globals
				if (module.TryGetTable("glo", out Table gloTable))
				{
					foreach (TableEntry baseTableEntry in gloTable.Entries)
					{
						SymbolEntry entry = baseTableEntry as SymbolEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);
						symbol.MangledName = entry.MangledName;
						symbol.Visibility = SemanticVisibility.Import;
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);
					}
				}

				// structures
				if (module.TryGetTable("str", out Table strTable))
				{
					foreach (TableEntry baseTableEntry in strTable.Entries)
					{
						TypeEntry entry = baseTableEntry as TypeEntry;
						NameMangling.Demangle(entry.Identifier, out _, out Symbol symbol);
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);

						foreach (TypeChild child in entry.Children)
						{
							NameMangling.Demangle(child.Identifier, out _, out Symbol childSymbol);
							bool exists = _symbolTable.AddSymbol(childSymbol);
							if (!exists)
								module.Symbols.Add(childSymbol);
						}
					}
				}

				// unions
				if (module.TryGetTable("uni", out Table uniTable))
				{
					foreach (TableEntry baseTableEntry in uniTable.Entries)
					{
						TypeEntry entry = baseTableEntry as TypeEntry;
						NameMangling.Demangle(entry.Identifier, out _, out Symbol symbol);
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);

						foreach (TypeChild child in entry.Children)
						{
							NameMangling.Demangle(child.Identifier, out _, out Symbol childSymbol);
							bool exists = _symbolTable.AddSymbol(childSymbol);
							if (!exists)
								module.Symbols.Add(childSymbol);
						}
					}
				}

				// simple enum
				if (module.TryGetTable("enu", out Table enuTable))
				{
					foreach (TableEntry baseTableEntry in enuTable.Entries)
					{
						TypeEntry entry = baseTableEntry as TypeEntry;
						NameMangling.Demangle(entry.Identifier, out _, out Symbol symbol);
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);

						foreach (TypeChild child in entry.Children)
						{
							NameMangling.Demangle(child.Identifier, out _, out Symbol childSymbol);
							bool exists = _symbolTable.AddSymbol(childSymbol);
							if (!exists)
								module.Symbols.Add(childSymbol);
						}
					}
				}

				// adt enum
				if (module.TryGetTable("adt", out Table adtTable))
				{
					foreach (TableEntry baseTableEntry in adtTable.Entries)
					{
						TypeEntry entry = baseTableEntry as TypeEntry;
						NameMangling.Demangle(entry.Identifier, out _, out Symbol symbol);
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);

						foreach (TypeChild child in entry.Children)
						{
							NameMangling.Demangle(child.Identifier, out _, out Symbol childSymbol);
							bool exists = _symbolTable.AddSymbol(childSymbol);
							if (!exists)
								module.Symbols.Add(childSymbol);
						}
					}
				}

				// delegate
				if (module.TryGetTable("del", out Table delTable))
				{
					foreach (TableEntry baseTableEntry in delTable.Entries)
					{
						SymbolEntry entry = baseTableEntry as SymbolEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);

						AttributeTableEntry atrEntry = GetAttributeEntry(module, entry.Info.AttributeIndex);

						if ((atrEntry.Flags & AttributeFlags.DelegateFuncPtr) != 0)
							symbol.CompileAttribs.Add(FuncPtrAttribute.Id, new FuncPtrAttribute());


						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);
					}
				}

				// aliases
				if (module.TryGetTable("ali", out Table aliTable))
				{
					foreach (TableEntry baseTableEntry in aliTable.Entries)
					{
						SymbolEntry entry = baseTableEntry as SymbolEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);
					}
				}

				module.UpdateTypes(_symbolTable);
				module.UpdateChildren();
			}

		}

		AttributeTableEntry GetAttributeEntry(Module.Module module, uint index)
		{
			if (module.TryGetTable("atr", out Table table) && table.Entries.Count > index)
			{
				return table.Entries[(int)index] as AttributeTableEntry;
			}
			return null;
		}

		public void GenerateImportedILCompUnits(CompilerContext context)
		{
			foreach (KeyValuePair<string, Module.Module> pair in Modules)
			{
				pair.Value.GenerateILCompUnit(context);
			}
		}

		public string PrepareForLinking()
		{
			string interDir = CmdLine.IntermediateDirectory ?? "";
			string cmdLine = "";
			foreach (KeyValuePair<string, Module.Module> pair in Modules)
			{
				Module.Module module = pair.Value;

				foreach (ObjectFile file in module.ObjectFiles)
				{
					string path = interDir + file.FileName;
					using (FileStream stream = new FileStream(path, FileMode.Create))
					using (BinaryWriter writer = new BinaryWriter(stream))
					{
						writer.Write(file.Data);
					}
					_tempObjFiles.Add(path);

					cmdLine += ' ' + path;
				}
			}

			return cmdLine;
		}

		public void CleanupAfterLinking()
		{
			foreach (string objFile in _tempObjFiles)
			{
				File.Delete(objFile);
			}
			_tempObjFiles.Clear();
		}
	}

}
