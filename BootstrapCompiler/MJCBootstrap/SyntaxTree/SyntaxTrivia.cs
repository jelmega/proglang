﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.SyntaxTree
{
	public class SyntaxTrivia
	{
		public TextSpan Span;
		public virtual string Value { get; }

		public override string ToString()
		{
			return Value;
		}

		public virtual string ToDebugString()
		{
			return Value;
		}
	}

	public class SingleLineCommentTrivia : SyntaxTrivia
	{
		public string Comment { get; }
		public override string Value { get { return Comment; } }

		public SingleLineCommentTrivia(TextSpan span, string comment)
		{
			Span = span;
			Comment = comment;
		}

		public override string ToDebugString()
		{
			return $"SingleLineCommentTrivia: '{Comment}'";
		}
	}

	public class BlockCommentTrivia : SyntaxTrivia
	{
		public string Comment { get; }
		public override string Value { get { return Comment; } }

		public BlockCommentTrivia(TextSpan span, string comment)
		{
			Span = span;
			Comment = comment;
		}

		public override string ToDebugString()
		{
			return $"SingleLineCommentTrivia: '{Comment.Replace("\n", "\\n").Replace("\r", "\\r")}'";
		}
	}

	public class EndOfLineTrivia : SyntaxTrivia
	{
		public string EndOfLine;
		public override string Value { get { return EndOfLine; } }

		public EndOfLineTrivia(TextSpan span, string endOfLine)
		{
			Span = span;
			EndOfLine = endOfLine;
		}

		public override string ToDebugString()
		{
			return "EndOfLineTrivia";
		}
	}

	public class WhiteSpaceTrivia : SyntaxTrivia
	{
		public string WhiteSpace;
		public override string Value { get { return WhiteSpace; } }

		public WhiteSpaceTrivia(TextSpan span, string whiteSpace)
		{
			Span = span;
			WhiteSpace = whiteSpace;
		}

		public override string ToDebugString()
		{
			return "WhiteSpaceTrivia";
		}
	}
}
