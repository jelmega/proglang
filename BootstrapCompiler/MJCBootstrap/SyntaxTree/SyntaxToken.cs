﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using MJC.General;

// ReSharper disable InconsistentNaming

namespace MJC.SyntaxTree
{
	public enum TokenType
	{
		// Special types
		Unknown,
		Missing,
		EndOfFile,
		// General
		Identifier,
		// Literals
		I8Literal,
		I16Literal,
		I32Literal,
		I64Literal,
		U8Literal,
		U16Literal,
		U32Literal,
		U64Literal,
		F32Literal,
		F64Literal,
		CharLiteral,
		StringLiteral,
		// Operators
		Plus,
		PlusPlus,
		PlusEquals,
		Minus,
		MinusMinus,
		MinusEquals,
		Asterisk,
		AsteriskEquals,
		Slash,
		SlashEquals,
		Or,
		OrEquals,
		OrOr,
		And,
		AndEquals,
		AndAnd,
		Caret,
		CaretEquals,
		CaretCaret,
		CaretCaretEquals,
		Exclaim,
		ExclaimEquals,
		ExclaimLess,
		Equals,
		EqualsEquals,
		Less,
		LessEquals,
		LessLess,
		LessLessEquals,
		Greater,
		GreaterEquals,
		GreaterGreater,
		GreaterGreaterEquals,
		GreaterGreaterGreater,
		GreaterGreaterGreaterEquals,
		LParen,
		RParen,
		LBrace,
		RBrace,
		LBracket,
		RBracket,
		Comma,
		Dot,
		DotDot,
		DotDotDot,
		Modulo,
		ModuloEquals,
		Tilde,
		TildeEquals,
		Arrow,
		DblArrow,
		Colon,
		ColonEquals,
		Semicolon,
		Question,
		QuestionQuestion,
		QuestionDot,
		At,
		AtColon,
		ColonColon,
		Hash,
		HashBrace,
		// Keywords
		AlignOf,
		As,
		Asm,
		Assert,
		Bool,
		Break,
		Byte,
		Case,
		CConst,
		Char,
		Const,
		Continue,
		Debug,
		Default,
		Defer,
		Delegate,
		Delete,
		Do,
		Double,
		Else,
		Elif,
		Enum,
		Export,
		Fallthrough,
		False,
		Float,
		For,
		Foreach,
		Func,
		Goto,
		If,
		Interface,
		Immutable,
		Import,
		Int,
		Internal,
		Is,
		ISize,
		In,
		Inout,
		Impl,
		Lazy,
		Long,
		Mixin,
		Module,
		Mutable,
		Namespace,
		New,
		Null,
		Out,
		Package,
		Private,
		Public,
		Return,
		Rune,
		Self,
		SelfType,
		Shared,
		Short,
		Sizeof,
		Static,
		String,
		Struct,
		Switch,
		Synchronized,
		Transmute,
		True,
		TypeAlias,
		Typedef,
		Typeid,
		Typeof,
		UByte,
		UInt,
		ULong,
		Union,
		UnitTest,
		UShort,
		USize,
		Version,
		Void,
		WChar,
		While,
		UUCCast,
		UUGlobal,
		// Special keywords
		SKFile,
		SKFileFullPath,
		SKPackage,
		SKModule,
		SKNamespace,
		SKLine,
		SKFunc,
		SKPrettyFunc
	}

	public class SyntaxToken //: SimpleExpressionSyntax
	{
		public TextSpan Span { get; }
		public TextSpan FullSpan { get; }

		public List<SyntaxTrivia> LeadTrivia = new List<SyntaxTrivia>();
		public List<SyntaxTrivia> TrailTrivia = new List<SyntaxTrivia>();

		public TokenType Type;

		public string Text;
		
		// TODO: Better value system
		public struct TokenValues
		{
			public long Integer;
			public double Double;
			public char Character;
			public string String;
		}
		public TokenValues Values;

		public SyntaxToken(TextSpan span, TokenType type, string text)
		{
			Span = span;
			Type = type;
			Text = text;

			int start = LeadTrivia.Count > 0 ? LeadTrivia[0].Span.Start : Span.Start;
			int end = TrailTrivia.Count > 0 ? TrailTrivia[TrailTrivia.Count - 1].Span.End : Span.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public SyntaxToken(TextSpan span, TokenType type, string text, long integer)
		{
			Span = span;
			Type = type;
			Text = text;
			Values.Integer = integer;

			int start = LeadTrivia.Count > 0 ? LeadTrivia[0].Span.Start : Span.Start;
			int end = TrailTrivia.Count > 0 ? TrailTrivia[TrailTrivia.Count - 1].Span.End : Span.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public SyntaxToken(TextSpan span, TokenType type, string text, ulong unsigned)
		{
			Span = span;
			Type = type;
			Text = text;
			Values.Integer = (long)unsigned;

			int start = LeadTrivia.Count > 0 ? LeadTrivia[0].Span.Start : Span.Start;
			int end = TrailTrivia.Count > 0 ? TrailTrivia[TrailTrivia.Count - 1].Span.End : Span.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public SyntaxToken(TextSpan span, TokenType type, string text, double floatingPoint)
		{
			Span = span;
			Type = type;
			Text = text;
			Values.Double = floatingPoint;

			int start = LeadTrivia.Count > 0 ? LeadTrivia[0].Span.Start : Span.Start;
			int end = TrailTrivia.Count > 0 ? TrailTrivia[TrailTrivia.Count - 1].Span.End : Span.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public SyntaxToken(TextSpan span, TokenType type, string text, string actString)
		{
			Span = span;
			Type = type;
			Text = text;
			Values.String = actString;

			int start = LeadTrivia.Count > 0 ? LeadTrivia[0].Span.Start : Span.Start;
			int end = TrailTrivia.Count > 0 ? TrailTrivia[TrailTrivia.Count - 1].Span.End : Span.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public SyntaxToken(TextSpan span, TokenType type, string text, char ch)
		{
			Span = span;
			Type = type;
			Text = text;
			Values.Character = ch;

			int start = LeadTrivia.Count > 0 ? LeadTrivia[0].Span.Start : Span.Start;
			int end = TrailTrivia.Count > 0 ? TrailTrivia[TrailTrivia.Count - 1].Span.End : Span.End;
			FullSpan = new TextSpan(start, end - start);
		}


		public string ToDebugString()
		{
			return $"{Type}Token";
		}

		public override string ToString()
		{
			return Text;
		}

		public string ToFullString()
		{
			string str = "";

			foreach (SyntaxTrivia trivia in LeadTrivia)
			{
				str += trivia.Value;
			}
			
			str += Text;

			foreach (SyntaxTrivia trivia in TrailTrivia)
			{
				str += trivia.Value;
			}

			return str;
		}
	}
}
