﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis
{
	[Flags]
	public enum SemanticVisibility : byte
	{
		Default		= 0b0000_0000,
		Private		= 0b0000_0001,
		Internal	= 0b0000_0010,
		Package     = 0b0000_0100,
		Public		= 0b0000_1000,
		Export		= 0b0001_0000,

		Import		= 0b1000_0000,
	}

	[Flags]
	public enum SemanticAttributes : uint
	{
		None					= 0b0000_0000_0000_0000_0000,
		Const					= 0b0000_0000_0000_0000_0001,
		CConst					= 0b0000_0000_0000_0000_0010,
		Immutable				= 0b0000_0000_0000_0000_0100,
		Mutable					= 0b0000_0000_0000_0000_1000,
		Static					= 0b0000_0000_0000_0001_0000,
		In						= 0b0000_0000_0000_0010_0000,
		Out						= 0b0000_0000_0000_0100_0000,
		Inout					= 0b0000_0000_0000_1000_0000,
		Lazy					= 0b0000_0000_0001_0000_0000,
		Synchronized			= 0b0000_0000_0010_0000_0000,
		Shared					= 0b0000_0000_0100_0000_0000,
		Global					= 0b0000_0000_1000_0000_0000,

		Func					= Const | CConst | Static | Synchronized,
		StructInterface	        = Static,
		UnionEnum				= None,
		Type					= Const | Immutable | Mutable,
		Argument				= Out,
		Parameter				= Out | In | Inout,
		Declaration				= Const | CConst | Immutable | Mutable | Static | Shared | Global

	}

	public class SemanticContext
	{
		public SemanticVisibility Visibility;
		public SemanticAttributes Attribs;

		public Dictionary<string, CompileAttribute> CompileAttribs = new Dictionary<string, CompileAttribute>();

		public bool AllowVisitAll = true;
		public bool AllowCompilation = true;

		public Scope Scope;
		public Identifier Identifier
		{
			get { return IdenScopeVar.Name; }
		}

		public ScopeVariable IdenScopeVar;
		public List<ScopeVariable> IdentifierList;

		public Symbol Symbol;
		public List<Symbol> SymbolList;

		public string AnonymousName;
		public string InitFunc;

		private SymbolType _type;
		public SymbolType _funcType;

		public SymbolType FuncType {
			get
			{
				if (Symbol?.Type != null)
				{
					if (Symbol.Type is FunctionSymbolType ft)
						return ft;
				}

				return _funcType;
			}
			set { _funcType = value; }
		}

		public SymbolType Type
		{
			get
			{
				if (Symbol?.Type != null)
				{
					if (Symbol.Type is FunctionSymbolType funcType)
						return funcType.ReturnType;
					return Symbol.Type;
				}
				return _type;
			}
			set { _type = value; }
		}
	}
}
