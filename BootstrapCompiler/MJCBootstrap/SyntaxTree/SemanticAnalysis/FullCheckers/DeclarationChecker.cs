﻿using System.Collections.Generic;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.FullCheckers
{
	class DeclarationChecker : ISemanticPass
	{
		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			if (syntax.Context.SymbolList != null)
			{
				foreach (Symbol symbol in syntax.Context.SymbolList)
				{
					
				}
			}
			else
			{
				Symbol symbol = syntax.Context.Symbol;
				if (symbol.Kind == SymbolKind.Variable)
					return;

				TextSpan symbolSpan = syntax.Names.FullSpan;
				SimpleExpressionSyntax initializer = syntax.Initializer;

				CheckInitializerForSymbol(symbol, symbolSpan, initializer);
			}
		}

		void CheckInitializerForSymbol(Symbol symbol, TextSpan symbolSpan, SimpleExpressionSyntax init)
		{
			if (!(symbol.Type is AggregateSymbolType ||
			      symbol.Type is InterfaceSymbolType ||
			      (symbol.Type is EnumSymbolType enumType &&
			       !enumType.IsSimpleEnum)))
				return;

			if (init == null)
			{
				ErrorSystem.AstError(symbolSpan, "uninitialized variable");
				return;
			}

			if (symbol.Type != init.Context.Type && 
			    !(init.Context.Type is BuiltinSymbolType builtin && builtin.Builtin == BuiltinTypes.Void))
			{
				string initType = init.Context.Type?.ToString() ?? "undefined";
				ErrorSystem.AstError(symbolSpan, $"mismatched type during initialization: was '{initType}', expected '{symbol.Type}'");
				return;
			}

			if (init is AggregateInitializerSyntax structInit)
			{
				CompilerContext baseTable = symbol.CompilerContext;

				Symbol typeSym = baseTable.FindDefinition(symbol.Scope, structInit.Context.IdenScopeVar, init.FullSpan.Start);

				List<Symbol> varChildren = new List<Symbol>();
				foreach (Symbol child in typeSym.Children)
				{
					if (child.Kind == SymbolKind.Variable)
						varChildren.Add(child);
				}

				if (typeSym.Kind == SymbolKind.Struct && varChildren.Count > 0)
				{
					if (structInit.Elements == null)
					{
						string errStr = "initializer doesn't initialize the following variables:";

						for (var i = 0; i < typeSym.Children.Count; i++)
						{
							if (i != 0)
								errStr += ", ";

							Symbol elemSymbol = typeSym.Children[i];
							errStr += elemSymbol.Identifier;
						}

						ErrorSystem.AstError(structInit.FullSpan, errStr);
						return;
					}

					string missingParams = null;
					bool idenEncountered = false;
					HashSet<Identifier> encounteredIdens = new HashSet<Identifier>();
					for (var childIdx = 0; childIdx < typeSym.Children.Count; childIdx++)
					{
						Symbol child = typeSym.Children[childIdx];
						bool found = false;
						for (var i = 0; i < structInit.Elements.Count; i++)
						{
							AggregateInitializerElemSyntax elem = structInit.Elements[i];
							if (elem.Identifier == null)
							{
								if (childIdx == i)
								{
									if (idenEncountered)
									{
										ErrorSystem.AstError(elem.FullSpan, "a value with explicit identifier was encountered before this expression. Values after explicit identifiers aren't allowed.");
									}

									found = true;
									break;
								}
							}
							else if (child.Identifier == elem.Identifier.Context.Identifier)
							{
								if (encounteredIdens.Contains(child.Identifier))
								{
									ErrorSystem.AstError(elem.FullSpan, $"variable '{elem.Identifier}' has already been assigned");
								}

								encounteredIdens.Add(child.Identifier);
								idenEncountered = true;
								found = true;
							}
						}

						if (!found)
						{
							if (missingParams == null)
								missingParams += child.Identifier;
							else
								missingParams += ", " + child.Identifier;
						}
					}

					if (missingParams != null)
					{
						string errStr = "initializer doesn't initialize the following variables: ";
						ErrorSystem.AstError(structInit.FullSpan, errStr + missingParams);
					}
				}
			}
		}

	}
}
