﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.SyntaxTree.SemanticAnalysis.FullCheckers
{
	class TemplateMethodChecker : ISemanticPass
	{
		private SyntaxTree _tree;
		private CompilerContext _compContext;

		public override void Setup(CompilerContext compilerContext)
		{
			_compContext = compilerContext;
		}

		public override void Visit(SyntaxTree tree)
		{
			_tree = tree;
			base.Visit(tree);
			_tree = null;
		}

		protected override void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			base.VisitFunctionCall(syntax);

			if (syntax.QualifiedName.SeperationToken?.Type == TokenType.Dot)
			{
				Scope tmpScope = new Scope(syntax.QualifiedName.Left);
				Symbol callerObj = _compContext.FindDefinition(syntax.Context.Scope, tmpScope.LastSubScope, syntax.FullSpan.Start, null);

				if (callerObj.Type is TemplateInstanceSymbolType instType)
				{
					List<VTableMethod> availableMethods = TemplateHelpers.GetInstanceMethods(instType.Iden, instType.TemplateSymbol, _compContext.VTables);

					Identifier methodIden = syntax.Context.Identifier;
					VTableMethod method = availableMethods.Find(m =>
					{
						if (m.Iden == methodIden)
						{
							// TODO: Type
							return true;
						}
						return false;
					});

					if (method == null)
					{
						string paramStr = "";
						Symbol funcSym = syntax.Context.Symbol;
						if (funcSym.Type is FunctionSymbolType funcType)
						{
							if (funcType.ParamTypes != null)
							{
								for (var i = 0; i < funcType.ParamTypes.Count; i++)
								{
									if (i != 0)
										paramStr += ", ";
									paramStr += funcType.ParamTypes[i].ToString();
								}
							}
						}


						ErrorSystem.AstError(syntax.FullSpan, $"function '{methodIden}' with parameters '({paramStr})' is not found for the template instance '{instType}'");
					}
				}
				
			}
		}
	}
}
