﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis
{
	public static class SemanticHelpers
	{
		public static string[] ExtractNames(QualifiedNameSyntax qualifiedName)
		{
			Stack<string> stack = new Stack<string>();
			while (qualifiedName.Left is QualifiedNameSyntax)
			{
				if (qualifiedName.Right is IdentifierNameSyntax identifier)
					stack.Push(identifier.Identifier.ToString());

				qualifiedName = qualifiedName.Left as QualifiedNameSyntax;
			}

			{
				if (qualifiedName.Right is IdentifierNameSyntax rIdentifier)
					stack.Push(rIdentifier.Identifier.ToString());
				if (qualifiedName.Left is IdentifierNameSyntax lIdentifier)
					stack.Push(lIdentifier.Identifier.ToString());
			}

			return stack.ToArray();
		}

	}
}
