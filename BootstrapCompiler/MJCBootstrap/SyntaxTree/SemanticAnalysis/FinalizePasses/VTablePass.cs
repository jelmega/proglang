﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.FinalizePasses
{
	class VTablePass : ISemanticPass
	{
		private bool _inImplOrInterface = false;
		private ScopeVariable _vtableScopeVar;
		private List<Symbol> _interfaces = new List<Symbol>();
		private Dictionary<ScopeVariable, VTable> _vTables;
		private CompilerContext _compContext;

		public override void Setup(CompilerContext compilerContext)
		{
			_compContext = compilerContext;
		}

		public override void Visit(SyntaxTree tree)
		{
			_vTables = new Dictionary<ScopeVariable, VTable>();
			base.Visit(tree);
			tree.VTables = _vTables;
			_vTables = null;
		}

		public override void Finalize(CompilerContext compilerContext)
		{
			// Update vtable method order
			foreach (KeyValuePair<ScopeVariable, List<VTable>> pair in compilerContext.VTables)
			{
				foreach (VTable table in pair.Value)
				{
					table.UpdateMethodOrder(compilerContext);
				}
			}
		}

		protected override void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
			//base.VisitPackageDirective(syntax);
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			base.VisitModuleDirective(syntax);
		}

		protected override void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
			base.VisitQualifiedName(syntax);
		}

		protected override void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
			base.VisitIdentifierName(syntax);
		}

		protected override void VisitTemplateName(TemplateNameSyntax syntax)
		{
			base.VisitTemplateName(syntax);
		}

		protected override void VisitNameList(NameListSyntax syntax)
		{
			base.VisitNameList(syntax);
		}

		protected override void VisitAttributes(AttributeSyntax syntax)
		{
			base.VisitAttributes(syntax);
		}

		protected override void VisitAtAttribute(AtAttributeSyntax syntax)
		{
			base.VisitAtAttribute(syntax);
		}

		protected override void VisitCompilerAtAttribute(CompilerAtAttributeSyntax syntax)
		{
			base.VisitCompilerAtAttribute(syntax);
		}

		protected override void VisitSingleAttribute(SingleAttributeSyntax syntax)
		{
			base.VisitSingleAttribute(syntax);
		}

		protected override void VisitArgument(ArgumentSyntax syntax)
		{
			base.VisitArgument(syntax);
		}

		protected override void VisitParameter(ParameterSyntax syntax)
		{
			base.VisitParameter(syntax);
		}

		protected override void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			base.VisitTemplateParameter(syntax);
		}

		protected override void VisitType(TypeSyntax syntax)
		{
			base.VisitType(syntax);
		}

		protected override void VisitBuiltinType(BuiltinTypeSyntax syntax)
		{
			base.VisitBuiltinType(syntax);
		}

		protected override void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
			base.VisitIdentifierType(syntax);
		}

		protected override void VisitPointerType(PointerTypeSyntax syntax)
		{
			base.VisitPointerType(syntax);
		}

		protected override void VisitArrayType(ArrayTypeSyntax syntax)
		{
			base.VisitArrayType(syntax);
		}

		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			base.VisitAttributedType(syntax);
		}

		protected override void VisitTupleType(TupleTypeSyntax syntax)
		{
			base.VisitTupleType(syntax);
		}

		protected override void VisitNullableType(NullableTypeSyntax syntax)
		{
			base.VisitNullableType(syntax);
		}

		protected override void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			base.VisitVariadicType(syntax);
		}

		protected override void VisitTypeof(TypeofSyntax syntax)
		{
			base.VisitTypeof(syntax);
		}

		protected override void VisitInlineStruct(InlineStructTypeSyntax syntax)
		{
			base.VisitInlineStruct(syntax);
		}

		protected override void VisitInlineUnion(InlineUnionTypeSyntax syntax)
		{
			base.VisitInlineUnion(syntax);
		}

		protected override void VisitDelegate(DelegateSyntax syntax)
		{
			base.VisitDelegate(syntax);
		}

		protected override void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			base.VisitBlockStatement(syntax);
		}

		protected override void VisitNamespace(NamespaceSyntax syntax)
		{
			base.VisitNamespace(syntax);
		}

		protected override void VisitImport(ImportSyntax syntax)
		{
			base.VisitImport(syntax);
		}

		protected override void VisitReceiver(ReceiverSyntax syntax)
		{
			base.VisitReceiver(syntax);
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			base.VisitFunction(syntax);

			if (_inImplOrInterface)
			{
				VTable table = _vTables[_vtableScopeVar];

				VTableMethod method = new VTableMethod(syntax.Context.Symbol);
				table.AddMethod(method, _interfaces);
			}
		}

		protected override void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			base.VisitTemplateConstraint(syntax);
		}

		protected override void VisitTypeAlias(TypeAliasSyntax syntax)
		{
			base.VisitTypeAlias(syntax);
		}

		protected override void VisitTypedef(TypedefSyntax syntax)
		{
			base.VisitTypedef(syntax);
		}

		protected override void VisitIfElse(IfElseSyntax syntax)
		{
			base.VisitIfElse(syntax);
		}

		protected override void VisitIf(IfSyntax syntax)
		{
			base.VisitIf(syntax);
		}

		protected override void VisitElse(ElseSyntax syntax)
		{
			base.VisitElse(syntax);
		}

		protected override void VisitWhile(WhileSyntax syntax)
		{
			base.VisitWhile(syntax);
		}

		protected override void VisitDoWhile(DoWhileSyntax syntax)
		{
			base.VisitDoWhile(syntax);
		}

		protected override void VisitFor(ForSyntax syntax)
		{
			base.VisitFor(syntax);
		}

		protected override void VisitForeach(ForeachSyntax syntax)
		{
			base.VisitForeach(syntax);
		}

		protected override void VisitSwitch(SwitchSyntax syntax)
		{
			base.VisitSwitch(syntax);
		}

		protected override void VisitSwitchCase(SwitchCaseSyntax syntax)
		{
			base.VisitSwitchCase(syntax);
		}

		protected override void VisitFallthrough(FallthroughSyntax syntax)
		{
			base.VisitFallthrough(syntax);
		}

		protected override void VisitContinue(ContinueSyntax syntax)
		{
			base.VisitContinue(syntax);
		}

		protected override void VisitBreak(BreakSyntax syntax)
		{
			base.VisitBreak(syntax);
		}

		protected override void VisitReturn(ReturnSyntax syntax)
		{
			base.VisitReturn(syntax);
		}

		protected override void VisitDefer(DeferSyntax syntax)
		{
			base.VisitDefer(syntax);
		}

		protected override void VisitGoto(GotoSyntax syntax)
		{
			base.VisitGoto(syntax);
		}

		protected override void VisitLabel(LabelSyntax syntax)
		{
			base.VisitLabel(syntax);
		}

		protected override void VisitImplInterfaceFieldDef(ImplInterfaceFieldDefSyntax syntax)
		{
			base.VisitImplInterfaceFieldDef(syntax);
		}

		protected override void VisitImplInterface(ImplInterfaceSyntax syntax)
		{
			base.VisitImplInterface(syntax);
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			_vtableScopeVar = new ScopeVariable(syntax.Type.Context.Scope, syntax.Type.Context.Identifier);
			_vtableScopeVar = _vtableScopeVar.GetBaseTemplate();

			if (syntax.BaseList != null)
			{
				foreach (ImplInterfaceSyntax implInterface in syntax.BaseList)
				{
					_interfaces.Add(implInterface.Context.Symbol);
				}
			}

			if (!_vTables.ContainsKey(_vtableScopeVar))
			{
				// TODO: Correct mangled extensions
				string mangled = "_M" + NameMangling.MangleScopeVar(_vtableScopeVar) + "TS";
				VTable vTable = new VTable(_vtableScopeVar, mangled);
				_vTables.Add(_vtableScopeVar, vTable);
				_compContext.AddVTable(vTable);
			}

			_inImplOrInterface = true;
			base.VisitImpl(syntax);
			_inImplOrInterface = false;
			_interfaces.Clear();
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			base.VisitStruct(syntax);

			ScopeVariable scopeVar = syntax.Context.Symbol.ScopeVar;
			if (!_vTables.ContainsKey(scopeVar))
			{
				Symbol symbol = syntax.Context.Symbol;
				string mangled = "_M" + NameMangling.MangleScopeVar(symbol.ScopeVar) + "TS";
				VTable vTable = new VTable(scopeVar, mangled);
				_vTables.Add(scopeVar, vTable);
				_compContext.AddVTable(vTable);

				// Generate init methods
				List<SymbolType> initParamTypes = new List<SymbolType>();
				foreach (Symbol child in symbol.Children)
				{
					if (child.Kind == SymbolKind.Variable)
					{
						initParamTypes.Add(child.Type);
					}
				}

				if (initParamTypes.Count == 0)
					initParamTypes = null;

				SymbolType instType;
				if (syntax.Context.Identifier is TemplateDefinitionIdentifier defName)
				{
					Scope structScope = new Scope(symbol.ScopeVar);
					List<TemplateArgument> args = new List<TemplateArgument>();

					for (var i = 0; i < defName.Parameters.Count; i++)
					{
						TemplateParameter param = defName.Parameters[i];

						SymbolType argType;
						if (param.ValueName != null)
						{
							argType = param.Type;
						}
						else
						{
							Identifier argTypeIden = param.Type.GetIdentifier();
							argType = SymbolType.TemplateParamType(structScope, argTypeIden, i);
						}

						TemplateArgument arg = new TemplateArgument(i, argType, param.ValueName);
						args.Add(arg);
					}

					TemplateInstanceIdentifier instIden = new TemplateInstanceIdentifier(symbol.Identifier.GetSimpleName(), args.Count);
					instIden.Arguments = args;
					instType = SymbolType.TemplateInstanceType(symbol.Scope, instIden, symbol);
				}
				else
				{
					instType = SymbolType.AggregateType(symbol.ScopeVar, AggregateTypes.Struct);
				}

				FunctionSymbolType initType = SymbolType.FunctionType(null, initParamTypes, instType);
				string initMangledName = "_M" + NameMangling.MangleScopeVar(symbol.ScopeVar) + "6__init" + NameMangling.MangleType(initType);

				VTableMethod method = new VTableMethod(new NameIdentifier("__init"), initType, initMangledName);
				vTable.AddMethod(method, null);
			}
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			base.VisitInterface(syntax);

			_vtableScopeVar = syntax.Context.Symbol.ScopeVar.GetBaseTemplate();
			if (!_vTables.ContainsKey(_vtableScopeVar))
			{
				string mangled = "_M" + NameMangling.MangleScopeVar(syntax.Context.Symbol.ScopeVar) + "TI";
				VTable vTable = new VTable(_vtableScopeVar, mangled);
				_vTables.Add(_vtableScopeVar, vTable);
				_compContext.AddVTable(vTable);
			}

			_inImplOrInterface = true;
			base.VisitInterface(syntax);
			_inImplOrInterface = false;
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			base.VisitUnion(syntax);

			ScopeVariable scopeVar = syntax.Context.Symbol.ScopeVar;
			if (!_vTables.ContainsKey(scopeVar))
			{
				Symbol symbol = syntax.Context.Symbol;
				string mangled = "_M" + NameMangling.MangleScopeVar(symbol.ScopeVar) + "TU";
				VTable vTable = new VTable(scopeVar, mangled);
				_vTables.Add(scopeVar, vTable);
				_compContext.AddVTable(vTable);

				SymbolType instType;
				if (syntax.Context.Identifier is TemplateDefinitionIdentifier defName)
				{
					Scope unionScope = new Scope(symbol.ScopeVar);
					List<TemplateArgument> args = new List<TemplateArgument>();

					for (var i = 0; i < defName.Parameters.Count; i++)
					{
						TemplateParameter param = defName.Parameters[i];

						Identifier argTypeIden = param.Type.GetIdentifier();
						TemplateParamSymbolType argType = SymbolType.TemplateParamType(unionScope, argTypeIden, i);

						TemplateArgument arg = new TemplateArgument(i, argType, param.ValueName);
						args.Add(arg);
					}

					TemplateInstanceIdentifier instIden = new TemplateInstanceIdentifier(symbol.Identifier.GetSimpleName(), args.Count);
					instIden.Arguments = args;
					instType = SymbolType.TemplateInstanceType(symbol.Scope, instIden, symbol);
				}
				else
				{
					instType = SymbolType.AggregateType(symbol.ScopeVar, AggregateTypes.Struct);
				}

				int idx = 0;
				foreach (Symbol child in symbol.Children)
				{
					if (child.Kind == SymbolKind.Variable)
					{
						List<SymbolType> initParamTypes = new List<SymbolType> { child.Type };

						string funcName = $"__init_{idx}";
						string initName = $"{funcName.Length}{funcName}";

						FunctionSymbolType initType = SymbolType.FunctionType(null, initParamTypes, instType);
						string initMangledName = "_M" + NameMangling.MangleScopeVar(symbol.ScopeVar) + initName + NameMangling.MangleType(initType);
						
						VTableMethod method = new VTableMethod(new NameIdentifier(funcName), initType, initMangledName);
						vTable.AddMethod(method, null);

						++idx;
					}
				}
			}
		}

		protected override void VisitEnum(EnumSyntax syntax)
		{
			base.VisitEnum(syntax);

			ScopeVariable scopeVar = syntax.Context.Symbol.ScopeVar;
			if (!_vTables.ContainsKey(scopeVar))
			{
				Symbol symbol = syntax.Context.Symbol;
				string mangled = "_M" + NameMangling.MangleScopeVar(symbol.ScopeVar) + "TE";
				VTable vTable = new VTable(scopeVar, mangled);
				_vTables.Add(scopeVar, vTable);
				_compContext.AddVTable(vTable);

				foreach (EnumMemberSyntax member in syntax.Members)
				{
					// Generate initializer for enum member 'body'
					if (member.AggrBody != null)
					{
						// Create struct
						Symbol structSymbol = (member.Context.Type as AggregateSymbolType).Symbol;

						vTable = new VTable(structSymbol.ScopeVar, structSymbol.MangledName);
						_vTables.Add(structSymbol.ScopeVar, vTable);
						_compContext.AddVTable(vTable);

						// Create init func
						List<SymbolType> initParamTypes = new List<SymbolType>();
						foreach (Symbol child in structSymbol.Children)
						{
							if (child.Kind == SymbolKind.Variable)
								initParamTypes.Add(child.Type);
						}

						SymbolType structType = structSymbol.Type;
						SymbolType instType = structType;

						FunctionSymbolType initType = SymbolType.FunctionType(null, initParamTypes, instType);
						string initMangledName = "_M" + NameMangling.MangleScopeVar(structSymbol.ScopeVar) + "6__init" + NameMangling.MangleType(initType);

						VTableMethod method = new VTableMethod(new NameIdentifier("__init"), initType, initMangledName);
						vTable.AddMethod(method, null);
					}
				}
			}
		}

		protected override void VisitUnitTest(UnitTestSyntax syntax)
		{
			base.VisitUnitTest(syntax);
		}

		protected override void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			base.VisitCompileCondition(syntax);
		}

		protected override void VisitAssert(AssertSyntax syntax)
		{
			base.VisitAssert(syntax);
		}

		protected override void VisitSimpleExpression(SimpleExpressionSyntax syntax)
		{
			base.VisitSimpleExpression(syntax);
		}

		protected override void VisitExpression(ExpressionSyntax syntax)
		{
			base.VisitExpression(syntax);
		}

		protected override void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			base.VisitCommaExpression(syntax);
		}

		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			base.VisitDeclaration(syntax);
		}

		protected override void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			base.VisitTernaryExpression(syntax);
		}

		protected override void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			base.VisitBinaryExpression(syntax);
		}

		protected override void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			base.VisitAssignExpression(syntax);
		}

		protected override void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			base.VisitPrefixExpression(syntax);
		}

		protected override void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			base.VisitPostfixExpression(syntax);
		}

		protected override void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			base.VisitBracketedExpression(syntax);
		}

		protected override void VisitCastExpression(CastExpressionSyntax syntax)
		{
			base.VisitCastExpression(syntax);
		}

		protected override void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			base.VisitTransmuteExpression(syntax);
		}

		protected override void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			base.VisitCCastExpression(syntax);
		}

		protected override void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			base.VisitIndexSliceExpression(syntax);
		}

		protected override void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			base.VisitFunctionCall(syntax);
		}

		protected override void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			base.VisitMixinExpression(syntax);
		}

		protected override void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			base.VisitIntrinsicExpression(syntax);
		}

		protected override void VisitIsExpression(IsExpressionSyntax syntax)
		{
			base.VisitIsExpression(syntax);
		}

		protected override void VisitNewExpression(NewExpressionSyntax syntax)
		{
			base.VisitNewExpression(syntax);
		}

		protected override void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			base.VisitDeleteExpression(syntax);
		}

		protected override void VisitLambda(ClosureSyntax syntax)
		{
			base.VisitLambda(syntax);
		}

		protected override void VisitLiteral(LiteralExpressionSyntax syntax)
		{
			base.VisitLiteral(syntax);
		}

		protected override void VisitArrayLiteral(ArrayLiteralExpressionSyntax syntax)
		{
			base.VisitArrayLiteral(syntax);
		}

		protected override void VisitAssocArrayLiteral(AssocArrayLiteralExpressionSyntax syntax)
		{
			base.VisitAssocArrayLiteral(syntax);
		}

		protected override void VisitAggregateInitializer(AggregateInitializerSyntax syntax)
		{
			base.VisitAggregateInitializer(syntax);
		}

		protected override void VisitSpecialKeyword(SpecialKeywordExpressionSyntax syntax)
		{
			base.VisitSpecialKeyword(syntax);
		}

		protected override void VisitBitField(BitFieldTypeSyntax syntax)
		{
			base.VisitBitField(syntax);
		}

		protected override void VisitTypeExpression(TypeExpressionSyntax syntax)
		{
			base.VisitTypeExpression(syntax);
		}
	}
}
