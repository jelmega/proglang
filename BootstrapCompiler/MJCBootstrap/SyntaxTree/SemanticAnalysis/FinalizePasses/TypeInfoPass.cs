﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.FinalizePasses
{
	class TypeInfoPass : ISemanticPass
	{
		private CompilerContext _context;

		public override void Setup(CompilerContext compilerContext)
		{
			_context = compilerContext;

			// Gen typedef for builtin types

			_context.AddTypeInfo("g", new TypeInfo(1, 1)); // i8
			_context.AddTypeInfo("h", new TypeInfo(1, 1)); // u8
			_context.AddTypeInfo("s", new TypeInfo(2, 2)); // i16
			_context.AddTypeInfo("t", new TypeInfo(2, 2)); // u16
			_context.AddTypeInfo("i", new TypeInfo(4, 4)); // i32
			_context.AddTypeInfo("j", new TypeInfo(4, 4)); // u32
			_context.AddTypeInfo("f", new TypeInfo(4, 4)); // f32
			_context.AddTypeInfo("l", new TypeInfo(8, 8)); // i64
			_context.AddTypeInfo("m", new TypeInfo(8, 8)); // u64
			_context.AddTypeInfo("d", new TypeInfo(8, 8)); // f64

			_context.AddTypeInfo("b", new TypeInfo(1, 1)); // bool

			_context.AddTypeInfo("c", new TypeInfo(1, 1)); // char
			_context.AddTypeInfo("w", new TypeInfo(1, 1)); // wchar
			_context.AddTypeInfo("r", new TypeInfo(1, 1)); // rune
		}

		public override void Visit(SyntaxTree tree)
		{
			base.Visit(tree);
		}

		protected override void VisitCompilationUnit(CompilationUnitSyntax syntax)
		{
			base.VisitCompilationUnit(syntax);
		}

		protected override void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
			base.VisitPackageDirective(syntax);
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			base.VisitModuleDirective(syntax);
		}

		protected override void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
			base.VisitQualifiedName(syntax);
		}

		protected override void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
			base.VisitIdentifierName(syntax);
		}

		protected override void VisitTemplateName(TemplateNameSyntax syntax)
		{
			base.VisitTemplateName(syntax);
		}

		protected override void VisitNameList(NameListSyntax syntax)
		{
			base.VisitNameList(syntax);
		}

		protected override void VisitAttributes(AttributeSyntax syntax)
		{
			base.VisitAttributes(syntax);
		}

		protected override void VisitAtAttribute(AtAttributeSyntax syntax)
		{
			base.VisitAtAttribute(syntax);
		}

		protected override void VisitCompilerAtAttribute(CompilerAtAttributeSyntax syntax)
		{
			base.VisitCompilerAtAttribute(syntax);
		}

		protected override void VisitSingleAttribute(SingleAttributeSyntax syntax)
		{
			base.VisitSingleAttribute(syntax);
		}

		protected override void VisitArgument(ArgumentSyntax syntax)
		{
			base.VisitArgument(syntax);
		}

		protected override void VisitParameter(ParameterSyntax syntax)
		{
			base.VisitParameter(syntax);
		}

		protected override void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			base.VisitTemplateParameter(syntax);
		}

		protected override void VisitType(TypeSyntax syntax)
		{
			base.VisitType(syntax);
		}

		protected override void VisitBuiltinType(BuiltinTypeSyntax syntax)
		{
			base.VisitBuiltinType(syntax);
		}

		protected override void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
			base.VisitIdentifierType(syntax);
		}

		protected override void VisitPointerType(PointerTypeSyntax syntax)
		{
			base.VisitPointerType(syntax);
		}

		protected override void VisitArrayType(ArrayTypeSyntax syntax)
		{
			base.VisitArrayType(syntax);
		}

		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			base.VisitAttributedType(syntax);
		}

		protected override void VisitTupleType(TupleTypeSyntax syntax)
		{
			base.VisitTupleType(syntax);
		}

		protected override void VisitNullableType(NullableTypeSyntax syntax)
		{
			base.VisitNullableType(syntax);
		}

		protected override void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			base.VisitVariadicType(syntax);
		}

		protected override void VisitTypeof(TypeofSyntax syntax)
		{
			base.VisitTypeof(syntax);
		}

		protected override void VisitInlineStruct(InlineStructTypeSyntax syntax)
		{
			base.VisitInlineStruct(syntax);
		}

		protected override void VisitInlineUnion(InlineUnionTypeSyntax syntax)
		{
			base.VisitInlineUnion(syntax);
		}

		protected override void VisitDelegate(DelegateSyntax syntax)
		{
			base.VisitDelegate(syntax);
		}

		protected override void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			base.VisitBlockStatement(syntax);
		}

		protected override void VisitNamespace(NamespaceSyntax syntax)
		{
			base.VisitNamespace(syntax);
		}

		protected override void VisitImport(ImportSyntax syntax)
		{
			base.VisitImport(syntax);
		}

		protected override void VisitReceiver(ReceiverSyntax syntax)
		{
			base.VisitReceiver(syntax);
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			base.VisitFunction(syntax);
		}

		protected override void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			base.VisitTemplateConstraint(syntax);
		}

		protected override void VisitTypeAlias(TypeAliasSyntax syntax)
		{
			base.VisitTypeAlias(syntax);
		}

		protected override void VisitTypedef(TypedefSyntax syntax)
		{
			base.VisitTypedef(syntax);
		}

		protected override void VisitIfElse(IfElseSyntax syntax)
		{
			base.VisitIfElse(syntax);
		}

		protected override void VisitIf(IfSyntax syntax)
		{
			base.VisitIf(syntax);
		}

		protected override void VisitElse(ElseSyntax syntax)
		{
			base.VisitElse(syntax);
		}

		protected override void VisitWhile(WhileSyntax syntax)
		{
			base.VisitWhile(syntax);
		}

		protected override void VisitDoWhile(DoWhileSyntax syntax)
		{
			base.VisitDoWhile(syntax);
		}

		protected override void VisitFor(ForSyntax syntax)
		{
			base.VisitFor(syntax);
		}

		protected override void VisitForeach(ForeachSyntax syntax)
		{
			base.VisitForeach(syntax);
		}

		protected override void VisitSwitch(SwitchSyntax syntax)
		{
			base.VisitSwitch(syntax);
		}

		protected override void VisitSwitchCase(SwitchCaseSyntax syntax)
		{
			base.VisitSwitchCase(syntax);
		}

		protected override void VisitFallthrough(FallthroughSyntax syntax)
		{
			base.VisitFallthrough(syntax);
		}

		protected override void VisitContinue(ContinueSyntax syntax)
		{
			base.VisitContinue(syntax);
		}

		protected override void VisitBreak(BreakSyntax syntax)
		{
			base.VisitBreak(syntax);
		}

		protected override void VisitReturn(ReturnSyntax syntax)
		{
			base.VisitReturn(syntax);
		}

		protected override void VisitDefer(DeferSyntax syntax)
		{
			base.VisitDefer(syntax);
		}

		protected override void VisitGoto(GotoSyntax syntax)
		{
			base.VisitGoto(syntax);
		}

		protected override void VisitLabel(LabelSyntax syntax)
		{
			base.VisitLabel(syntax);
		}

		protected override void VisitImplInterfaceFieldDef(ImplInterfaceFieldDefSyntax syntax)
		{
			base.VisitImplInterfaceFieldDef(syntax);
		}

		protected override void VisitImplInterface(ImplInterfaceSyntax syntax)
		{
			base.VisitImplInterface(syntax);
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			base.VisitImpl(syntax);
		}

		protected override void VisitAggregateBody(AggregateBodySyntax syntax)
		{
			base.VisitAggregateBody(syntax);
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			base.VisitStruct(syntax);
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			base.VisitInterface(syntax);
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			base.VisitUnion(syntax);
		}

		protected override void VisitEnum(EnumSyntax syntax)
		{
			base.VisitEnum(syntax);
		}

		protected override void VisitUnitTest(UnitTestSyntax syntax)
		{
			base.VisitUnitTest(syntax);
		}

		protected override void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			base.VisitCompileCondition(syntax);
		}

		protected override void VisitAssert(AssertSyntax syntax)
		{
			base.VisitAssert(syntax);
		}

		protected override void VisitSimpleExpression(SimpleExpressionSyntax syntax)
		{
			base.VisitSimpleExpression(syntax);
		}

		protected override void VisitExpression(ExpressionSyntax syntax)
		{
			base.VisitExpression(syntax);
		}

		protected override void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			base.VisitCommaExpression(syntax);
		}

		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			base.VisitDeclaration(syntax);
		}

		protected override void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			base.VisitTernaryExpression(syntax);
		}

		protected override void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			base.VisitBinaryExpression(syntax);
		}

		protected override void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			base.VisitAssignExpression(syntax);
		}

		protected override void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			base.VisitPrefixExpression(syntax);
		}

		protected override void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			base.VisitPostfixExpression(syntax);
		}

		protected override void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			base.VisitBracketedExpression(syntax);
		}

		protected override void VisitCastExpression(CastExpressionSyntax syntax)
		{
			base.VisitCastExpression(syntax);
		}

		protected override void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			base.VisitTransmuteExpression(syntax);
		}

		protected override void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			base.VisitCCastExpression(syntax);
		}

		protected override void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			base.VisitIndexSliceExpression(syntax);
		}

		protected override void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			base.VisitFunctionCall(syntax);
		}

		protected override void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			base.VisitMixinExpression(syntax);
		}

		protected override void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			base.VisitIntrinsicExpression(syntax);
		}

		protected override void VisitIsExpression(IsExpressionSyntax syntax)
		{
			base.VisitIsExpression(syntax);
		}

		protected override void VisitNewExpression(NewExpressionSyntax syntax)
		{
			base.VisitNewExpression(syntax);
		}

		protected override void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			base.VisitDeleteExpression(syntax);
		}

		protected override void VisitLambda(ClosureSyntax syntax)
		{
			base.VisitLambda(syntax);
		}

		protected override void VisitLiteral(LiteralExpressionSyntax syntax)
		{
			base.VisitLiteral(syntax);
		}

		protected override void VisitArrayLiteral(ArrayLiteralExpressionSyntax syntax)
		{
			base.VisitArrayLiteral(syntax);
		}

		protected override void VisitAssocArrayLiteral(AssocArrayLiteralExpressionSyntax syntax)
		{
			base.VisitAssocArrayLiteral(syntax);
		}

		protected override void VisitAggregateInitializer(AggregateInitializerSyntax syntax)
		{
			base.VisitAggregateInitializer(syntax);
		}

		protected override void VisitSpecialKeyword(SpecialKeywordExpressionSyntax syntax)
		{
			base.VisitSpecialKeyword(syntax);
		}

		protected override void VisitBitField(BitFieldTypeSyntax syntax)
		{
			base.VisitBitField(syntax);
		}

		protected override void VisitTypeExpression(TypeExpressionSyntax syntax)
		{
			base.VisitTypeExpression(syntax);
		}
	}
}
