﻿using System.Collections.Generic;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.PartialCheckers
{
	class AttributeChecker : ISemanticPass
	{
		private enum CurrScope
		{
			None,
			Parameter,
			Argument,
			Declaration
		}

		private CurrScope _currScope;

		private HashSet<string> _builtinTypeNames = new HashSet<string>
		{
			"bool",
			"i8",
			"i16",
			"i32",
			"i64",
			"u8",
			"u16",
			"u32",
			"u64",
			"f32",
			"f64",
		};

		protected override void VisitArgument(ArgumentSyntax syntax)
		{
			CurrScope prev = _currScope;
			_currScope = CurrScope.Argument;
			base.VisitArgument(syntax);
			_currScope = prev;
		}

		protected override void VisitAssert(AssertSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				if (syntax.Attributes.AtAttribs != null)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "An assert is not allowed to have any @ attributes");

				if (GetVisibility(syntax.Attributes.SingleAttribs) != SemanticVisibility.Default)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "An assert is not allowed to have a visibility attribute");
				syntax.Context.Attribs = GetAttributes(syntax.Attributes.SingleAttribs);

				SemanticAttributes invalid = syntax.Context.Attribs & ~SemanticAttributes.CConst;
				if (invalid != SemanticAttributes.None)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "An assert may only have the 'cconst' attribute");
			}

			base.VisitAssert(syntax);
		}
		
		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			if (syntax.Attributes.AtAttribs != null)
				ErrorSystem.AstError(syntax.Attributes.FullSpan, "A type is not allowed to have any @ attributes");

			syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
			syntax.Context.Attribs = GetAttributes(syntax.Attributes.SingleAttribs);

			SemanticAttributes invalid = syntax.Context.Attribs & ~SemanticAttributes.Type;
			if (invalid != SemanticAttributes.None)
			{
				string invalidStr = AttribsToString(invalid);
				ErrorSystem.AstError(syntax.Attributes.FullSpan, $"A type may not have the following attributes: {invalidStr}");
			}

			base.VisitAttributedType(syntax);
		}

		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			CurrScope prev = _currScope;
			_currScope = CurrScope.Declaration;
			base.VisitDeclaration(syntax);
			_currScope = prev;
		}
		
		protected override void VisitEnum(EnumSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
				syntax.Context.Attribs = GetAttributes(syntax.Attributes.SingleAttribs);

				SemanticAttributes invalid = syntax.Context.Attribs & ~SemanticAttributes.UnionEnum;
				if (invalid != SemanticAttributes.None)
				{
					string invalidStr = AttribsToString(invalid);
					ErrorSystem.AstError(syntax.Attributes.FullSpan, $"An enum may not have the following attributes: {invalidStr}");
				}
			}

			base.VisitEnum(syntax);
		}

		protected override void VisitFor(ForSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				if (syntax.Attributes.AtAttribs != null)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "A for-loop is not allowed to have any @ attributes");

				if (GetVisibility(syntax.Attributes.SingleAttribs) != SemanticVisibility.Default)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "A for-loop is not allowed to have a visibility attribute");
				syntax.Context.Attribs = GetAttributes(syntax.Attributes.SingleAttribs);

				SemanticAttributes invalid = syntax.Context.Attribs & ~SemanticAttributes.CConst;
				if (invalid != SemanticAttributes.None)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "A for-loop may only have the 'cconst' attribute");
			}

			base.VisitFor(syntax);
		}

		protected override void VisitForeach(ForeachSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				if (syntax.Attributes.AtAttribs != null)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "A foreach-loop is not allowed to have any @ attributes");

				if (GetVisibility(syntax.Attributes.SingleAttribs) != SemanticVisibility.Default)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "A foreach-loop is not allowed to have a visibility attribute");
				syntax.Context.Attribs = GetAttributes(syntax.Attributes.SingleAttribs);

				SemanticAttributes invalid = syntax.Context.Attribs & ~SemanticAttributes.CConst;
				if (invalid != SemanticAttributes.None)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "A foreach-loop may only have the 'cconst' attribute");
			}

			base.VisitForeach(syntax);
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				if (syntax.Attributes.AtAttribs != null)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "A function is not allowed to have any @ attributes");
				
				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
				syntax.Context.Attribs = GetAttributes(syntax.Attributes.SingleAttribs);

				SemanticAttributes invalid = syntax.Context.Attribs & ~SemanticAttributes.Func;
				if (invalid != SemanticAttributes.None)
				{
					string invalidStr = AttribsToString(invalid);
					ErrorSystem.AstError(syntax.Attributes.FullSpan, $"An enum may not have the following attributes: {invalidStr}");
				}
			}

			base.VisitFunction(syntax);
		}

		protected override void VisitIf(IfSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				if (syntax.Attributes.AtAttribs != null)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "An if-statement is not allowed to have any @ attributes");

				if (GetVisibility(syntax.Attributes.SingleAttribs) != SemanticVisibility.Default)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "An if-statement is not allowed to have a visibility attribute");
				syntax.Context.Attribs = GetAttributes(syntax.Attributes.SingleAttribs);

				SemanticAttributes invalid = syntax.Context.Attribs & ~SemanticAttributes.CConst;
				if (invalid != SemanticAttributes.None)
					ErrorSystem.AstError(syntax.Attributes.FullSpan, "A for-loop may only have the 'cconst' attribute");
			}

			base.VisitIf(syntax);
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			if (syntax.Context.CompileAttribs.ContainsKey(ExtendBuiltinAttribute.Id))
			{
				string name = syntax.Type.ToString();
				if (!_builtinTypeNames.Contains(name))
				{
					ErrorSystem.AstError(syntax.Type.FullSpan, $"{name} is not an extensible builtin type");
				}

				if (syntax.OpenTemplateToken != null)
				{
					int start = syntax.OpenTemplateToken.FullSpan.Start;
					int end = syntax.CloseTemplateToken.FullSpan.End;
					TextSpan span = new TextSpan(start, end - start);
					ErrorSystem.AstError(span, "extending a builtin type may not be implemented with template parameters");
				}
			}

			base.VisitImpl(syntax);
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
				syntax.Context.Attribs = GetAttributes(syntax.Attributes.SingleAttribs);

				SemanticAttributes invalid = syntax.Context.Attribs & ~SemanticAttributes.StructInterface;
				if (invalid != SemanticAttributes.None)
				{
					string invalidStr = AttribsToString(invalid);
					ErrorSystem.AstError(syntax.Attributes.FullSpan, $"An interface may not have the following attributes: {invalidStr}");
				}
			}

			base.VisitInterface(syntax);
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				if (syntax.Context.Attribs != SemanticAttributes.None)
				{
					string invalidStr = AttribsToString(syntax.Context.Attribs);
					ErrorSystem.AstError(syntax.Attributes.FullSpan, $"A module may not have the following attributes: {invalidStr}");
				}
			}

			base.VisitModuleDirective(syntax);
		}

		protected override void VisitParameter(ParameterSyntax syntax)
		{
			CurrScope prev = _currScope;
			_currScope = CurrScope.Parameter;
			base.VisitParameter(syntax);
			_currScope = prev;
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
				syntax.Context.Attribs = GetAttributes(syntax.Attributes.SingleAttribs);

				SemanticAttributes invalid = syntax.Context.Attribs & ~SemanticAttributes.StructInterface;
				if (invalid != SemanticAttributes.None)
				{
					string invalidStr = AttribsToString(invalid);
					ErrorSystem.AstError(syntax.Attributes.FullSpan, $"A struct may not have the following attributes: {invalidStr}");
				}
			}

			base.VisitStruct(syntax);
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
				syntax.Context.Attribs = GetAttributes(syntax.Attributes.SingleAttribs);

				SemanticAttributes invalid = syntax.Context.Attribs & ~SemanticAttributes.UnionEnum;
				if (invalid != SemanticAttributes.None)
				{
					string invalidStr = AttribsToString(invalid);
					ErrorSystem.AstError(syntax.Attributes.FullSpan, $"An union may not have the following attributes: {invalidStr}");
				}
			}

			base.VisitUnion(syntax);
		}

		private SemanticVisibility GetVisibility(List<SingleAttributeSyntax> attribs)
		{
			if (attribs == null)
				return SemanticVisibility.Default;

			SemanticVisibility visibility = SemanticVisibility.Default;
			foreach (SingleAttributeSyntax attrib in attribs)
			{
				SemanticVisibility tmp = SemanticVisibility.Default;
				switch (attrib.AttributeToken.Type)
				{
					case TokenType.Private:
						tmp = SemanticVisibility.Private;
						break;
				case TokenType.Internal:
						tmp = SemanticVisibility.Internal;
						break;
				case TokenType.Public:
						tmp = SemanticVisibility.Public;
						break;
				case TokenType.Export:
						tmp = SemanticVisibility.Export;
						break;
				}
				if (tmp == SemanticVisibility.Default)
					continue;

				if ((visibility & tmp) != SemanticVisibility.Default)
				{
					ErrorSystem.AstError(attrib.FullSpan, $"Duplicate visibility attribute: {tmp.ToString().ToLower()}");
				}
				visibility |= tmp;
			}

			return visibility;
		}

		private SemanticAttributes GetAttributes(List<SingleAttributeSyntax> attribs)
		{
			if (attribs == null)
				return SemanticAttributes.None;

			SemanticAttributes attributes = SemanticAttributes.None;
			foreach (SingleAttributeSyntax attrib in attribs)
			{
				SemanticAttributes tmp = SemanticAttributes.None;
				switch (attrib.AttributeToken.Type)
				{
				case TokenType.Const:
					tmp = SemanticAttributes.Const;
					break;
				case TokenType.CConst:
					tmp = SemanticAttributes.CConst;
					break;
				case TokenType.Immutable:
					tmp = SemanticAttributes.Immutable;
					break;
				case TokenType.Mutable:
					tmp = SemanticAttributes.Mutable;
					break;
				case TokenType.Static:
					tmp = SemanticAttributes.Static;
					break;
				case TokenType.In:
					tmp = SemanticAttributes.In;
					break;
				case TokenType.Out:
					tmp = SemanticAttributes.Out;
					break;
				case TokenType.Inout:
					tmp = SemanticAttributes.Inout;
					break;
				case TokenType.Lazy:
					tmp = SemanticAttributes.Lazy;
					break;
				case TokenType.Synchronized:
					tmp = SemanticAttributes.Synchronized;
					break;
				case TokenType.Shared:
					tmp = SemanticAttributes.Shared;
					break;
				case TokenType.UUGlobal:
					tmp = SemanticAttributes.Global;
					break;
				}
				if (tmp == SemanticAttributes.None)
					continue;

				if ((attributes & tmp) != SemanticAttributes.None)
				{
					ErrorSystem.AstError(attrib.FullSpan, $"Duplicate visibility attribute: {tmp.ToString().ToLower()}");
				}
				attributes |= tmp;
			}

			return attributes;
		}

		private string AttribsToString(SemanticAttributes attribs)
		{
			string str = null;
			for (int i = 0; i < 10; i++)
			{
				SemanticAttributes mask = (SemanticAttributes) (1 << i);
				SemanticAttributes tmp = attribs & mask;
				if (tmp != SemanticAttributes.None)
				{
					if (str == null)
						str = $"{tmp.ToString().ToLower()}";
					else
						str += $", {tmp.ToString().ToLower()}";
				}
			}
			return str;
		}


	}
}
