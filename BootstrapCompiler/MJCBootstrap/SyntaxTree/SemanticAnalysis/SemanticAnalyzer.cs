﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.SyntaxTree.SemanticAnalysis;
using MJC.SyntaxTree.SemanticAnalysis.FinalizePasses;
using MJC.SyntaxTree.SemanticAnalysis.FullCheckers;
using MJC.SyntaxTree.SemanticAnalysis.FullPasses;
using MJC.SyntaxTree.SemanticAnalysis.PartialCheckers;
using MJC.SyntaxTree.SemanticAnalysis.PartialPasses;

namespace MJC.SyntaxTree.SemanticAnalysis
{
	public class SemanticAnalyzer
	{
		public List<string> VersionConditionalVariables;
		public List<string> DebugConditionalVariables;

		private List<ISemanticPass> _partialPasses = new List<ISemanticPass>();
		private List<ISemanticPass> _fullPasses = new List<ISemanticPass>();
		private List<ISemanticPass> _finalizePasses = new List<ISemanticPass>();

		public SemanticAnalyzer(CompilerContext globalTable)
		{
			// Partial passes
			_partialPasses.Add(new CompileConditionalPass());
			_partialPasses.Add(new AnonNameGenPass());
			_partialPasses.Add(new IdentifierGenPass());
			_partialPasses.Add(new ScopeGenPass());
			_partialPasses.Add(new AttributePass());
			_partialPasses.Add(new TableGenPass(globalTable));

			// Partial checkers
			_partialPasses.Add(new AttributeChecker());

			// Full passes
			_fullPasses.Add(new TypePropagotionPass());
			_fullPasses.Add(new InterfaceImplPass());

			// Full checkers
			_fullPasses.Add(new DeclarationChecker());
			_fullPasses.Add(new TemplateMethodChecker());

			// Finalize passes (after name mangling)
			_finalizePasses.Add(new VTablePass());
		}

		/// <summary>
		/// Analyze the all possible steps with the data from the partial module
		/// </summary>
		public void AnalyzePartial(SyntaxTree tree)
		{
			foreach (ISemanticPass pass in _partialPasses)
			{
				pass.Setup(VersionConditionalVariables, DebugConditionalVariables);
				pass.Setup(tree);

				pass.Visit(tree);
			}
		}

		/// <summary>
		/// Finalize analysis with known data from full module and imports
		/// </summary>
		public void AnalyzeFull(SyntaxTree tree, CompilerContext compilerContext)
		{
			foreach (ISemanticPass pass in _fullPasses)
			{
				pass.Setup(VersionConditionalVariables, DebugConditionalVariables);
				pass.Setup(compilerContext);
				pass.Setup(tree);

				pass.Visit(tree);

				pass.Finalize(compilerContext);
			}
		}

		public void Finalize(SyntaxTree tree, CompilerContext compilerContext)
		{
			foreach (ISemanticPass pass in _finalizePasses)
			{
				pass.Setup(VersionConditionalVariables, DebugConditionalVariables);
				pass.Setup(compilerContext);
				pass.Setup(tree);

				pass.Visit(tree);

				pass.Finalize(compilerContext);
			}
		}
	}
}
