﻿namespace MJC.SyntaxTree.SemanticAnalysis.PartialPasses
{
	// TODO: Better name for anonymous structures than prefix followed by text span start
	class AnonNameGenPass : ISemanticPass
	{
		public override void Visit(SyntaxTree tree)
		{
			base.Visit(tree);
		}

		protected override void VisitCompilationUnit(CompilationUnitSyntax syntax)
		{
			base.VisitCompilationUnit(syntax);
		}

		protected override void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
		}

		protected override void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
		}

		protected override void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
		}

		protected override void VisitTemplateName(TemplateNameSyntax syntax)
		{
		}

		protected override void VisitNameList(NameListSyntax syntax)
		{
		}

		protected override void VisitAttributes(AttributeSyntax syntax)
		{
		}

		protected override void VisitArgument(ArgumentSyntax syntax)
		{
		}

		protected override void VisitParameter(ParameterSyntax syntax)
		{
			base.VisitParameter(syntax);
		}

		protected override void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			base.VisitTemplateParameter(syntax);
		}

		protected override void VisitType(TypeSyntax syntax)
		{
			base.VisitType(syntax);
		}

		protected override void VisitBuiltinType(BuiltinTypeSyntax syntax)
		{
		}

		protected override void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
		}

		protected override void VisitPointerType(PointerTypeSyntax syntax)
		{
			base.VisitPointerType(syntax);
		}

		protected override void VisitArrayType(ArrayTypeSyntax syntax)
		{
			base.VisitArrayType(syntax);
		}

		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			base.VisitAttributedType(syntax);
		}

		protected override void VisitTupleType(TupleTypeSyntax syntax)
		{
			base.VisitTupleType(syntax);
		}

		protected override void VisitNullableType(NullableTypeSyntax syntax)
		{
			base.VisitNullableType(syntax);
		}

		protected override void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			base.VisitVariadicType(syntax);
		}

		protected override void VisitTypeof(TypeofSyntax syntax)
		{
			base.VisitTypeof(syntax);
		}

		protected override void VisitInlineStruct(InlineStructTypeSyntax syntax)
		{
			string anonName = $"__anon_s_{syntax.FullSpan.Start}";
			syntax.Context.AnonymousName = anonName;
			syntax.StructSyntax.Context.AnonymousName = anonName;
			base.VisitStruct(syntax.StructSyntax);
		}

		protected override void VisitInlineUnion(InlineUnionTypeSyntax syntax)
		{
			string anonName = $"__anon_u_{syntax.FullSpan.Start}";
			syntax.Context.AnonymousName = anonName;
			syntax.UnionSyntax.Context.AnonymousName = anonName;
			base.VisitUnion(syntax.UnionSyntax);
		}

		protected override void VisitDelegate(DelegateSyntax syntax)
		{
			if (syntax.Identifier == null && syntax.Context.AnonymousName == null)
			{
				string anonName = $"__anon_d_{syntax.FullSpan.Start}";
				syntax.Context.AnonymousName = anonName;
			}
			base.VisitDelegate(syntax);
		}

		protected override void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			base.VisitBlockStatement(syntax);
		}

		protected override void VisitNamespace(NamespaceSyntax syntax)
		{
			base.VisitNamespace(syntax);
		}

		protected override void VisitImport(ImportSyntax syntax)
		{
		}

		protected override void VisitReceiver(ReceiverSyntax syntax)
		{
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			base.VisitFunction(syntax);
		}

		protected override void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			base.VisitTemplateConstraint(syntax);
		}

		protected override void VisitTypeAlias(TypeAliasSyntax syntax)
		{
			base.VisitTypeAlias(syntax);
		}

		protected override void VisitTypedef(TypedefSyntax syntax)
		{
			base.VisitTypedef(syntax);
		}

		protected override void VisitIfElse(IfElseSyntax syntax)
		{
			base.VisitIfElse(syntax);
		}

		protected override void VisitIf(IfSyntax syntax)
		{
			base.VisitIf(syntax);
		}

		protected override void VisitElse(ElseSyntax syntax)
		{
			base.VisitElse(syntax);
		}

		protected override void VisitWhile(WhileSyntax syntax)
		{
			base.VisitWhile(syntax);
		}

		protected override void VisitDoWhile(DoWhileSyntax syntax)
		{
			base.VisitDoWhile(syntax);
		}

		protected override void VisitFor(ForSyntax syntax)
		{
			base.VisitFor(syntax);
		}

		protected override void VisitForeach(ForeachSyntax syntax)
		{
			base.VisitForeach(syntax);
		}

		protected override void VisitSwitch(SwitchSyntax syntax)
		{
			base.VisitSwitch(syntax);
		}

		protected override void VisitSwitchCase(SwitchCaseSyntax syntax)
		{
			base.VisitSwitchCase(syntax);
		}

		protected override void VisitFallthrough(FallthroughSyntax syntax)
		{
			base.VisitFallthrough(syntax);
		}

		protected override void VisitContinue(ContinueSyntax syntax)
		{
			base.VisitContinue(syntax);
		}

		protected override void VisitBreak(BreakSyntax syntax)
		{
			base.VisitBreak(syntax);
		}

		protected override void VisitReturn(ReturnSyntax syntax)
		{
			base.VisitReturn(syntax);
		}

		protected override void VisitDefer(DeferSyntax syntax)
		{
			base.VisitDefer(syntax);
		}

		protected override void VisitGoto(GotoSyntax syntax)
		{
			base.VisitGoto(syntax);
		}

		protected override void VisitLabel(LabelSyntax syntax)
		{
			base.VisitLabel(syntax);
		}

		protected override void VisitImplInterfaceFieldDef(ImplInterfaceFieldDefSyntax syntax)
		{
			base.VisitImplInterfaceFieldDef(syntax);
		}

		protected override void VisitImplInterface(ImplInterfaceSyntax syntax)
		{
			base.VisitImplInterface(syntax);
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			base.VisitImpl(syntax);
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			if (syntax.Name == null && syntax.Context.AnonymousName == null)
			{
				string anonName = $"__anon_s_{syntax.FullSpan.Start}";
				syntax.Context.AnonymousName = anonName;
			}

			int paddingId = 0;
			if (syntax.Body.Declarations != null)
			{
				foreach (DeclarationSyntax decl in syntax.Body.Declarations)
				{
					if (decl.Names is IdentifierNameSyntax iden && iden.Identifier.Text == "_")
					{
						iden.Identifier.Text = $"__padding{paddingId}";
						++paddingId;
					}
					else if (decl.Names is QualifiedNameSyntax qualIden && qualIden.Left == null &&
					         qualIden.Right is IdentifierNameSyntax qualR && qualR.Identifier.Text == "_")
					{
						qualR.Identifier.Text = $"__padding{paddingId}";
						++paddingId;
					}
				}
			}

			base.VisitStruct(syntax);
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			base.VisitInterface(syntax);
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			if (syntax.Name == null && syntax.Context.AnonymousName == null)
			{
				string anonName = $"__anon_u_{syntax.FullSpan.Start}";
				syntax.Context.AnonymousName = anonName;
			}
			base.VisitUnion(syntax);
		}

		protected override void VisitEnum(EnumSyntax syntax)
		{
		}

		protected override void VisitUnitTest(UnitTestSyntax syntax)
		{
			base.VisitUnitTest(syntax);
		}

		protected override void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			base.VisitCompileCondition(syntax);
		}

		protected override void VisitAssert(AssertSyntax syntax)
		{
		}

		protected override void VisitSimpleExpression(SimpleExpressionSyntax syntax)
		{
			base.VisitSimpleExpression(syntax);
		}

		protected override void VisitExpression(ExpressionSyntax syntax)
		{
			base.VisitExpression(syntax);
		}

		protected override void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			base.VisitCommaExpression(syntax);
		}

		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			base.VisitDeclaration(syntax);
		}

		protected override void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			base.VisitTernaryExpression(syntax);
		}

		protected override void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			base.VisitBinaryExpression(syntax);
		}

		protected override void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			base.VisitAssignExpression(syntax);
		}

		protected override void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			base.VisitPrefixExpression(syntax);
		}

		protected override void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			base.VisitPostfixExpression(syntax);
		}

		protected override void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			base.VisitBracketedExpression(syntax);
		}

		protected override void VisitCastExpression(CastExpressionSyntax syntax)
		{
			base.VisitCastExpression(syntax);
		}

		protected override void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			base.VisitTransmuteExpression(syntax);
		}

		protected override void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			base.VisitCCastExpression(syntax);
		}

		protected override void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			base.VisitIndexSliceExpression(syntax);
		}

		protected override void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			base.VisitFunctionCall(syntax);
		}

		protected override void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			base.VisitMixinExpression(syntax);
		}

		protected override void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			base.VisitIntrinsicExpression(syntax);
		}

		protected override void VisitIsExpression(IsExpressionSyntax syntax)
		{
			base.VisitIsExpression(syntax);
		}

		protected override void VisitNewExpression(NewExpressionSyntax syntax)
		{
			base.VisitNewExpression(syntax);
		}

		protected override void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			base.VisitDeleteExpression(syntax);
		}

		protected override void VisitLambda(ClosureSyntax syntax)
		{
			base.VisitLambda(syntax);
		}

		protected override void VisitLiteral(LiteralExpressionSyntax syntax)
		{
			base.VisitLiteral(syntax);
		}

		protected override void VisitArrayLiteral(ArrayLiteralExpressionSyntax syntax)
		{
			base.VisitArrayLiteral(syntax);
		}

		protected override void VisitAssocArrayLiteral(AssocArrayLiteralExpressionSyntax syntax)
		{
			base.VisitAssocArrayLiteral(syntax);
		}

		protected override void VisitAggregateInitializer(AggregateInitializerSyntax syntax)
		{
			base.VisitAggregateInitializer(syntax);
		}
	}
}
