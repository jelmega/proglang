﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.PartialPasses
{
	class IdentifierGenPass : ISemanticPass
	{
		public override void Visit(SyntaxTree tree)
		{
			base.Visit(tree);
		}

		protected override void VisitCompilationUnit(CompilationUnitSyntax syntax)
		{
			base.VisitCompilationUnit(syntax);
		}

		protected override void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
			base.VisitPackageDirective(syntax);
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			base.VisitModuleDirective(syntax);
		}

		protected override void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
			base.VisitQualifiedName(syntax);
			Scope scope = syntax.Left == null ? Scope.Empty : new Scope(syntax.Left.Context.IdenScopeVar);
			syntax.Context.IdenScopeVar = new ScopeVariable(scope, syntax.Right.Context.Identifier);
		}

		protected override void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
			syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new NameIdentifier(syntax));
		}

		protected override void VisitTemplateName(TemplateNameSyntax syntax)
		{
			syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new TemplateInstanceIdentifier(syntax));
			base.VisitTemplateName(syntax);
		}
		
		protected override void VisitNameList(NameListSyntax syntax)
		{
			base.VisitNameList(syntax);
		}

		protected override void VisitAttributes(AttributeSyntax syntax)
		{
			base.VisitAttributes(syntax);
		}

		protected override void VisitAtAttribute(AtAttributeSyntax syntax)
		{
			base.VisitAtAttribute(syntax);
		}

		protected override void VisitCompilerAtAttribute(CompilerAtAttributeSyntax syntax)
		{
			base.VisitCompilerAtAttribute(syntax);
		}

		protected override void VisitSingleAttribute(SingleAttributeSyntax syntax)
		{
			base.VisitSingleAttribute(syntax);
		}

		protected override void VisitArgument(ArgumentSyntax syntax)
		{
			base.VisitArgument(syntax);
		}

		protected override void VisitParameter(ParameterSyntax syntax)
		{
			base.VisitParameter(syntax);
			syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new NameIdentifier(syntax.Identifier.Identifier.Text));
		}

		protected override void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			base.VisitTemplateParameter(syntax);
			syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new NameIdentifier(syntax.Identifier));
		}

		protected override void VisitType(TypeSyntax syntax)
		{
			base.VisitType(syntax);
		}

		protected override void VisitBuiltinType(BuiltinTypeSyntax syntax)
		{
			syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new NameIdentifier(syntax.TypeToken.Text));
		}

		protected override void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
			base.VisitIdentifierType(syntax);
			syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, syntax.Name.Context.Identifier);
		}

		protected override void VisitPointerType(PointerTypeSyntax syntax)
		{
			base.VisitPointerType(syntax);
		}

		protected override void VisitArrayType(ArrayTypeSyntax syntax)
		{
			base.VisitArrayType(syntax);
		}

		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			base.VisitAttributedType(syntax);
		}

		protected override void VisitTupleType(TupleTypeSyntax syntax)
		{
			base.VisitTupleType(syntax);
		}

		protected override void VisitNullableType(NullableTypeSyntax syntax)
		{
			base.VisitNullableType(syntax);
		}

		protected override void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			base.VisitVariadicType(syntax);
		}

		protected override void VisitTypeof(TypeofSyntax syntax)
		{
			base.VisitTypeof(syntax);
		}

		protected override void VisitInlineStruct(InlineStructTypeSyntax syntax)
		{
			base.VisitInlineStruct(syntax);
		}

		protected override void VisitInlineUnion(InlineUnionTypeSyntax syntax)
		{
			base.VisitInlineUnion(syntax);
		}

		protected override void VisitDelegate(DelegateSyntax syntax)
		{
			base.VisitDelegate(syntax);
		}

		protected override void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			base.VisitBlockStatement(syntax);
		}

		protected override void VisitNamespace(NamespaceSyntax syntax)
		{
			base.VisitNamespace(syntax);
		}

		protected override void VisitImport(ImportSyntax syntax)
		{
			base.VisitImport(syntax);
		}

		protected override void VisitReceiver(ReceiverSyntax syntax)
		{
			base.VisitReceiver(syntax);
			syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new NameIdentifier("self"));
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			base.VisitFunction(syntax);
			Identifier iden = syntax.Name?.Context.Identifier ?? new NameIdentifier(syntax.Context.AnonymousName);
			syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, iden);
		}

		protected override void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			base.VisitTemplateConstraint(syntax);
		}

		protected override void VisitTypeAlias(TypeAliasSyntax syntax)
		{
			base.VisitTypeAlias(syntax);
			Identifier iden = syntax.Name?.Context.Identifier ?? new NameIdentifier(syntax.Context.AnonymousName);
			syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, iden);
		}

		protected override void VisitTypedef(TypedefSyntax syntax)
		{
			base.VisitTypedef(syntax);
			Identifier iden = syntax.Name?.Context.Identifier ?? new NameIdentifier(syntax.Context.AnonymousName);
			syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, iden);
		}

		protected override void VisitIfElse(IfElseSyntax syntax)
		{
			base.VisitIfElse(syntax);
		}

		protected override void VisitIf(IfSyntax syntax)
		{
			base.VisitIf(syntax);
		}

		protected override void VisitElse(ElseSyntax syntax)
		{
			base.VisitElse(syntax);
		}

		protected override void VisitWhile(WhileSyntax syntax)
		{
			base.VisitWhile(syntax);
		}

		protected override void VisitDoWhile(DoWhileSyntax syntax)
		{
			base.VisitDoWhile(syntax);
		}

		protected override void VisitFor(ForSyntax syntax)
		{
			base.VisitFor(syntax);
		}

		protected override void VisitForeach(ForeachSyntax syntax)
		{
			base.VisitForeach(syntax);
		}

		protected override void VisitSwitch(SwitchSyntax syntax)
		{
			base.VisitSwitch(syntax);
		}

		protected override void VisitSwitchCase(SwitchCaseSyntax syntax)
		{
			base.VisitSwitchCase(syntax);
		}

		protected override void VisitFallthrough(FallthroughSyntax syntax)
		{
			base.VisitFallthrough(syntax);
		}

		protected override void VisitContinue(ContinueSyntax syntax)
		{
			base.VisitContinue(syntax);
		}

		protected override void VisitBreak(BreakSyntax syntax)
		{
			base.VisitBreak(syntax);
		}

		protected override void VisitReturn(ReturnSyntax syntax)
		{
			base.VisitReturn(syntax);
		}

		protected override void VisitDefer(DeferSyntax syntax)
		{
			base.VisitDefer(syntax);
		}

		protected override void VisitGoto(GotoSyntax syntax)
		{
			base.VisitGoto(syntax);
		}

		protected override void VisitLabel(LabelSyntax syntax)
		{
			base.VisitLabel(syntax);
		}

		protected override void VisitImplInterfaceFieldDef(ImplInterfaceFieldDefSyntax syntax)
		{
			base.VisitImplInterfaceFieldDef(syntax);
		}

		protected override void VisitImplInterface(ImplInterfaceSyntax syntax)
		{
			base.VisitImplInterface(syntax);
			syntax.Context.IdenScopeVar = syntax.Name.Context.IdenScopeVar;
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			base.VisitImpl(syntax);
			syntax.Context.IdenScopeVar = syntax.Type.Context.IdenScopeVar;
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			base.VisitStruct(syntax);

			string name = syntax.Name?.Identifier.Text ?? syntax.Context.AnonymousName;
			if (syntax.OpenTemplateToken == null)
			{
				syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new NameIdentifier(name));
			}
			else
			{
				List<TemplateParameter> parameters = new List<TemplateParameter>();
				for (var i = 0; i < syntax.TemplateParameters.Count; i++)
				{
					TemplateParameterSyntax paramSyntax = syntax.TemplateParameters[i];
					TemplateParameter param;
					if (paramSyntax.Type != null)
					{
						string valName = paramSyntax.Identifier.Identifier.Text;
						param = new TemplateParameter(valName, i);
					}
					else
					{
						
						param = new TemplateParameter(i);
					}

					parameters.Add(param);
				}
				syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new TemplateDefinitionIdentifier(name, parameters));
			}
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			base.VisitInterface(syntax);

			string name = syntax.Name?.Identifier.Text ?? syntax.Context.AnonymousName;
			if (syntax.OpenTemplateToken == null)
			{
				syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new NameIdentifier(name));
			}
			else
			{
				List<TemplateParameter> parameters = new List<TemplateParameter>();
				for (var i = 0; i < syntax.TemplateParameters.Count; i++)
				{
					TemplateParameterSyntax paramSyntax = syntax.TemplateParameters[i];
					TemplateParameter param;
					if (paramSyntax.Type != null)
					{
						string valName = paramSyntax.Identifier.Identifier.Text;
						param = new TemplateParameter(valName, i);
					}
					else
					{

						param = new TemplateParameter(i);
					}

					parameters.Add(param);
				}
				syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new TemplateDefinitionIdentifier(name, parameters));
			}
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			base.VisitUnion(syntax);

			string name = syntax.Name?.Identifier.Text ?? syntax.Context.AnonymousName;
			if (syntax.OpenTemplateToken == null)
			{
				syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new NameIdentifier(name));
			}
			else
			{
				List<TemplateParameter> parameters = new List<TemplateParameter>();
				for (var i = 0; i < syntax.TemplateParameters.Count; i++)
				{
					TemplateParameterSyntax paramSyntax = syntax.TemplateParameters[i];
					TemplateParameter param;
					if (paramSyntax.Type != null)
					{
						string valName = paramSyntax.Identifier.Identifier.Text;
						param = new TemplateParameter(valName, i);
					}
					else
					{

						param = new TemplateParameter(i);
					}

					parameters.Add(param);
				}
				syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new TemplateDefinitionIdentifier(name, parameters));
			}
		}

		protected override void VisitEnum(EnumSyntax syntax)
		{
			base.VisitEnum(syntax);

			string name = syntax.Name?.Identifier.Text ?? syntax.Context.AnonymousName;
			if (syntax.OpenTemplateToken == null)
			{
				syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new NameIdentifier(name));
			}
			else
			{
				List<TemplateParameter> parameters = new List<TemplateParameter>();
				for (var i = 0; i < syntax.TemplateParameters.Count; i++)
				{
					TemplateParameterSyntax paramSyntax = syntax.TemplateParameters[i];
					TemplateParameter param;
					if (paramSyntax.Type != null)
					{
						string valName = paramSyntax.Identifier.Identifier.Text;
						param = new TemplateParameter(valName, i);
					}
					else
					{

						param = new TemplateParameter(i);
					}

					parameters.Add(param);
				}
				syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, new TemplateDefinitionIdentifier(name, parameters));
			}
		}

		protected override void VisitUnitTest(UnitTestSyntax syntax)
		{
			base.VisitUnitTest(syntax);
		}

		protected override void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			base.VisitCompileCondition(syntax);
		}

		protected override void VisitAssert(AssertSyntax syntax)
		{
			base.VisitAssert(syntax);
		}

		protected override void VisitSimpleExpression(SimpleExpressionSyntax syntax)
		{
			base.VisitSimpleExpression(syntax);
		}

		protected override void VisitExpression(ExpressionSyntax syntax)
		{
			base.VisitExpression(syntax);
		}

		protected override void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			base.VisitCommaExpression(syntax);
		}

		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			base.VisitDeclaration(syntax);

			if (syntax.Names is NameListSyntax nameList)
			{
				syntax.Context.IdentifierList = new List<ScopeVariable>();
				foreach (NameListElemSyntax elem in nameList.Names)
				{
					if (elem.Name is SimpleNameSyntax simpleName)
					{
						syntax.Context.IdentifierList.Add(new ScopeVariable(Scope.Empty, Identifier.GetIdentifier(simpleName)));
					}
				}
			}
			else if (syntax.Names is SimpleNameSyntax simpleName)
			{
				syntax.Context.IdenScopeVar = new ScopeVariable(Scope.Empty, Identifier.GetIdentifier(simpleName));
			}
		}

		protected override void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			base.VisitTernaryExpression(syntax);
		}

		protected override void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			base.VisitBinaryExpression(syntax);
		}

		protected override void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			base.VisitAssignExpression(syntax);
		}

		protected override void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			base.VisitPrefixExpression(syntax);
		}

		protected override void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			base.VisitPostfixExpression(syntax);
		}

		protected override void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			base.VisitBracketedExpression(syntax);
		}

		protected override void VisitCastExpression(CastExpressionSyntax syntax)
		{
			base.VisitCastExpression(syntax);
		}

		protected override void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			base.VisitTransmuteExpression(syntax);
		}

		protected override void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			base.VisitCCastExpression(syntax);
		}

		protected override void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			base.VisitIndexSliceExpression(syntax);
		}

		protected override void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			base.VisitFunctionCall(syntax);
			syntax.Context.IdenScopeVar = syntax.QualifiedName.Context.IdenScopeVar;
		}

		protected override void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			base.VisitMixinExpression(syntax);
		}

		protected override void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			base.VisitIntrinsicExpression(syntax);
		}

		protected override void VisitIsExpression(IsExpressionSyntax syntax)
		{
			base.VisitIsExpression(syntax);
		}

		protected override void VisitNewExpression(NewExpressionSyntax syntax)
		{
			base.VisitNewExpression(syntax);
		}

		protected override void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			base.VisitDeleteExpression(syntax);
		}

		protected override void VisitLambda(ClosureSyntax syntax)
		{
			base.VisitLambda(syntax);
		}

		protected override void VisitLiteral(LiteralExpressionSyntax syntax)
		{
			base.VisitLiteral(syntax);
		}

		protected override void VisitArrayLiteral(ArrayLiteralExpressionSyntax syntax)
		{
			base.VisitArrayLiteral(syntax);
		}

		protected override void VisitAssocArrayLiteral(AssocArrayLiteralExpressionSyntax syntax)
		{
			base.VisitAssocArrayLiteral(syntax);
		}

		protected override void VisitAggregateInitializer(AggregateInitializerSyntax syntax)
		{
			base.VisitAggregateInitializer(syntax);
			syntax.Context.IdenScopeVar = syntax.TypeIden.Context.IdenScopeVar;
		}

		protected override void VisitSpecialKeyword(SpecialKeywordExpressionSyntax syntax)
		{
			base.VisitSpecialKeyword(syntax);
		}

		protected override void VisitBitField(BitFieldTypeSyntax syntax)
		{
			base.VisitBitField(syntax);
		}

		protected override void VisitTypeExpression(TypeExpressionSyntax syntax)
		{
			base.VisitTypeExpression(syntax);
		}
	}
}
