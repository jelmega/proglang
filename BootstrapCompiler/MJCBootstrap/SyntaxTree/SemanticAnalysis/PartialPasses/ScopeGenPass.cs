﻿using System;
using System.Collections.Generic;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.PartialPasses
{
	internal enum ScopeIdType
	{
		Block,
		If,
		While,
		DoWhile,
		For,
		Foreach,
		Switch,
		Case,
		Func,
		Impl,
		
		UnitTest
	}

	internal struct ScopeIds
	{
		internal int Block;
		internal int If;
		internal int While;
		internal int DoWhile;
		internal int For;
		internal int Foreach;
		internal int Switch;
		internal int Case;
		internal int Func;
		internal int Impl;

		internal int UnitTest;
	}

	class ScopeGenPass : ISemanticPass
	{
		private Scope _currentScope = new Scope();
		private Stack<ScopeIds> _blockIndices = new Stack<ScopeIds>();
		private bool _createBlockScope = true;
		private bool _compUnitBlock = true;

		private Scope _implScope;
		private bool _setImplScope = true;

		protected override void VisitTypeAlias(TypeAliasSyntax syntax)
		{
			Action<TypeAliasSyntax> dummy = s =>
			{
				if (s.TemplateParameters != null)
				{
					foreach (TemplateParameterSyntax param in s.TemplateParameters)
					{
						VisitTemplateParameter(param);
					}
				}
			};

			HandleNewScopeBlock(syntax, dummy, innerblock: false, blockName: syntax.Context.Identifier);

			syntax.Context.Scope = new Scope(_currentScope);
			VisitSimpleExpression(syntax.Type);
			VisitIdentifierName(syntax.Name);
		}

		protected override void VisitArgument(ArgumentSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitArgument(syntax);
		}

		protected override void VisitAssert(AssertSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitAssert(syntax);
		}

		protected override void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitAssignExpression(syntax);
		}

		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitAttributedType(syntax);
		}

		protected override void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitBinaryExpression(syntax);
		}

		protected override void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			if (_createBlockScope && !_compUnitBlock)
			{
				_compUnitBlock = false;
				HandleNewScopeBlock(syntax, base.VisitBlockStatement);
			}
			else if (_setImplScope && _implScope != null)
			{
				HandleNewScopeBlock(syntax, base.VisitBlockStatement, scope:_implScope);
			}
			else
			{
				syntax.Context.Scope = new Scope(_currentScope);
				base.VisitBlockStatement(syntax);
			}
		}

		protected override void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitBracketedExpression(syntax);
		}

		protected override void VisitBreak(BreakSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitBreak(syntax);
		}

		protected override void VisitSwitchCase(SwitchCaseSyntax syntax)
		{
			Identifier blockName = syntax.DefaultToken == null ? null : new NameIdentifier("__default");
			HandleNewScopeBlock(syntax, base.VisitSwitchCase, ScopeIdType.Case, blockName == null, blockName);
		}

		protected override void VisitCastExpression(CastExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitCastExpression(syntax);
		}

		protected override void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitCCastExpression(syntax);
		}

		protected override void VisitImplInterface(ImplInterfaceSyntax syntax)
		{
			base.VisitImplInterface(syntax);
			syntax.Context.Scope = new Scope(_currentScope);
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			_implScope = new Scope(_currentScope);

			Identifier iden = syntax.Type.Context.Identifier;
			if (iden is TemplateInstanceIdentifier instName)
				iden = instName.BaseIden;
			_implScope.Names.Add(iden);
			
			Action<ImplSyntax> dummy = s =>
			{
				VisitType(s.Type);
				if (s.OpenTemplateToken != null)
				{
					s.TemplateParameters.ForEach(VisitTemplateParameter);
				}
				if (s.ColonToken != null)
				{
					s.BaseList.ForEach(VisitImplInterface);
				}
				if (s.Constraint != null)
					VisitTemplateConstraint(s.Constraint);
				VisitBlockStatement(s.Body);
			};

			HandleNewScopeBlock(syntax, dummy, ScopeIdType.Impl, false);
			_implScope = null;
		}

		protected override void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitCommaExpression(syntax);
		}

		protected override void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitCompileCondition(syntax);
		}

		protected override void VisitCompilationUnit(CompilationUnitSyntax syntax)
		{
			_createBlockScope = false;
			_currentScope = new Scope();
			base.VisitCompilationUnit(syntax);
		}
		
		protected override void VisitContinue(ContinueSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
		}

		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitDeclaration(syntax);
		}

		protected override void VisitDefer(DeferSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitDefer(syntax);
		}

		protected override void VisitDelegate(DelegateSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitDelegate(syntax);
		}

		protected override void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitDeleteExpression(syntax);
		}
		
		protected override void VisitDoWhile(DoWhileSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitDoWhile, ScopeIdType.DoWhile, false);
		}

		protected override void VisitEnum(EnumSyntax syntax)
		{
			Action<EnumSyntax> dummy = s =>
			{
				VisitIdentifierName(syntax.Name);
				if (syntax.TemplateParameters != null)
				{
					foreach (TemplateParameterSyntax param in syntax.TemplateParameters)
					{
						VisitTemplateParameter(param);
					}
				}
				if (syntax.Type != null)
				{
					VisitType(syntax.Type);
				}

				foreach (EnumMemberSyntax member in syntax.Members)
				{
					member.Context.Scope = new Scope(syntax.Context.Scope);

					if (member.AggrBody != null)
					{
						VisitIdentifierName(member.Name);
						if (member.TupleType != null)
						{
							VisitType(member.TupleType);
						}
						if (member.AggrBody != null)
						{
							Identifier iden = new NameIdentifier($"__es_{member.Context.Identifier}");
							HandleNewScopeBlock(member.AggrBody, base.VisitAggregateBody, innerblock: false, blockName: iden);
						}
						if (member.EqualsToken != null)
						{
							VisitSimpleExpression(member.DefaultValue);
						}


						Scope scope = new Scope(syntax.Context.Scope);
						member.AggrBody.Context.Scope = scope;
					}
				}
			};

			HandleNewScopeBlock(syntax, dummy, innerblock: false, blockName: syntax.Context.Identifier);
		}

		protected override void VisitExpression(ExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitExpression(syntax);
		}
		
		protected override void VisitFallthrough(FallthroughSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitFallthrough(syntax);
		}

		protected override void VisitFor(ForSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitFor, ScopeIdType.For, false);
		}

		protected override void VisitForeach(ForeachSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitForeach, ScopeIdType.Foreach, false);
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitFunction, ScopeIdType.Func, false, syntax.Context.Identifier);
		}

		protected override void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitFunctionCall(syntax);
		}

		protected override void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitTemplateConstraint(syntax);
		}

		protected override void VisitGoto(GotoSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitGoto(syntax);
		}

		protected override void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitIdentifierName(syntax);
		}

		protected override void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitIdentifierType(syntax);
		}

		protected override void VisitIf(IfSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitIf, ScopeIdType.If, false);
		}

		protected override void VisitIfElse(IfElseSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitIfElse(syntax);
		}

		protected override void VisitElse(ElseSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitElse, innerblock: false);
		}

		protected override void VisitImport(ImportSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitImport(syntax);
		}

		protected override void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitIndexSliceExpression(syntax);
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitInterface, innerblock: false, blockName: syntax.Context.Identifier);
		}

		protected override void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitIntrinsicExpression(syntax);
		}

		protected override void VisitIsExpression(IsExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitIsExpression(syntax);
		}

		protected override void VisitLabel(LabelSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitLabel(syntax);
		}

		protected override void VisitLambda(ClosureSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitLambda(syntax);
		}

		protected override void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitMixinExpression(syntax);
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			_currentScope.Names.Add(new NameIdentifier(syntax.Name.Identifier.Text));
			base.VisitModuleDirective(syntax);
		}

		protected override void VisitNameList(NameListSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitNameList(syntax);
		}

		protected override void VisitNamespace(NamespaceSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitNamespace, syntax.Name, false);
		}

		protected override void VisitNewExpression(NewExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitNewExpression(syntax);
		}

		protected override void VisitNullableType(NullableTypeSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitNullableType(syntax);
		}

		protected override void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
			if (syntax.Name is QualifiedNameSyntax qualName)
			{
				string[] names = SemanticHelpers.ExtractNames(qualName);
				foreach (string name in names)
				{
					_currentScope.Names.Add(new NameIdentifier(name));
				}
			}
			else if (syntax.Name is IdentifierNameSyntax idenName)
			{
				_currentScope.Names.Add(new NameIdentifier(idenName.Identifier.Text));
			}
			

			base.VisitPackageDirective(syntax);
		}

		protected override void VisitParameter(ParameterSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitParameter(syntax);
		}

		protected override void VisitPointerType(PointerTypeSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitPointerType(syntax);
		}

		protected override void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitPostfixExpression(syntax);
		}
		
		protected override void VisitBuiltinType(BuiltinTypeSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitBuiltinType(syntax);
		}

		protected override void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitPrefixExpression(syntax);
		}

		protected override void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitQualifiedName(syntax);
		}

		protected override void VisitReturn(ReturnSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitReturn(syntax);
		}

		protected override void VisitReceiver(ReceiverSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitReceiver(syntax);
		}

		protected override void VisitSpecialKeyword(SpecialKeywordExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitSpecialKeyword(syntax);
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitStruct, innerblock: false, blockName: syntax.Context.Identifier);
		}

		protected override void VisitAggregateInitializer(AggregateInitializerSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitAggregateInitializer(syntax);
		}

		protected override void VisitSwitch(SwitchSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitSwitch, ScopeIdType.Switch, false);
		}

		protected override void VisitTemplateName(TemplateNameSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitTemplateName(syntax);
		}

		protected override void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			bool prev = _setImplScope;
			_setImplScope = false;
			base.VisitTemplateParameter(syntax);
			_setImplScope = prev;
		}

		protected override void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitTernaryExpression(syntax);
		}

		protected override void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitTransmuteExpression(syntax);
		}

		protected override void VisitTupleType(TupleTypeSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitTupleType(syntax);
		}

		protected override void VisitTypedef(TypedefSyntax syntax)
		{
			Action<TypedefSyntax> dummy = s =>
			{
				if (s.TemplateParameters != null)
				{
					foreach (TemplateParameterSyntax param in s.TemplateParameters)
					{
						VisitTemplateParameter(param);
					}
				}
			};

			HandleNewScopeBlock(syntax, dummy, innerblock: false, blockName: syntax.Context.Identifier);

			syntax.Context.Scope = new Scope(_currentScope);
			VisitSimpleExpression(syntax.Type);
			VisitIdentifierName(syntax.Name);
		}

		protected override void VisitTypeof(TypeofSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitTypeof(syntax);
		}

		protected override void VisitTypeExpression(TypeExpressionSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitTypeExpression(syntax);
		}

		protected override void VisitType(TypeSyntax syntax)
		{
			bool prev = _setImplScope;
			_setImplScope = false;
			base.VisitType(syntax);
			_setImplScope = prev;

			if (_setImplScope && _implScope != null)
			{
				syntax.Context.Scope = _implScope.GetBaseScope();
			}
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitUnion, innerblock: false, blockName: syntax.Context.Identifier);
		}

		protected override void VisitUnitTest(UnitTestSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitUnitTest, ScopeIdType.UnitTest, false, syntax.Context.Identifier);
		}

		protected override void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			syntax.Context.Scope = new Scope(_currentScope);
			base.VisitVariadicType(syntax);
		}

		protected override void VisitWhile(WhileSyntax syntax)
		{
			HandleNewScopeBlock(syntax, base.VisitWhile, ScopeIdType.While, false);
		}

		private void HandleNewScopeBlock<T>(T syntax, Action<T> visit, ScopeIdType idType = ScopeIdType.Block, bool innerblock = true, Identifier blockName = null, Scope scope = null) where T : SyntaxNode
		{
			ScopeIds scopeIds;
			if (scope == null && 
			    (blockName == null || 
			     idType == ScopeIdType.Func || 
			     idType == ScopeIdType.UnitTest))
			{
				if (!_blockIndices.TryPop(out scopeIds))
					scopeIds = new ScopeIds();

				string scopeName;
				switch (idType)
				{
				case ScopeIdType.If:
					scopeName = $"_if{scopeIds.If}";
					++scopeIds.If;
					break;
				case ScopeIdType.While:
					scopeName = $"_while{scopeIds.While}";
					++scopeIds.While;
					break;
				case ScopeIdType.DoWhile:
					scopeName = $"__dowhile{scopeIds.DoWhile}";
					++scopeIds.DoWhile;
					break;
				case ScopeIdType.For:
					scopeName = $"_for{scopeIds.For}";
					++scopeIds.For;
					break;
				case ScopeIdType.Foreach:
					scopeName = $"_foreach{scopeIds.Foreach}";
					++scopeIds.Foreach;
					break;
				case ScopeIdType.Switch:
					scopeName = $"_switch{scopeIds.Switch}";
					++scopeIds.Switch;
					break;
				case ScopeIdType.Case:
					scopeName = $"_case{scopeIds.Case}";
					++scopeIds.Case;
					break;
				case ScopeIdType.Func:
					scopeName = $"__{blockName}-{scopeIds.Func}";
					++scopeIds.Func;
					break;
				case ScopeIdType.UnitTest:
					scopeName = $"__ut{(blockName != null ? $"{blockName}__" : "")}{scopeIds.UnitTest}";
					++scopeIds.UnitTest;
					break;
				case ScopeIdType.Impl:
					scopeName = $"impl{scopeIds.Impl}";
					++scopeIds.Impl;
					break;
				case ScopeIdType.Block:
				default:
					scopeName = $"__{scopeIds.Block}";
					++scopeIds.Block;
					break;
				}

				_currentScope.Names.Add(new NameIdentifier(scopeName));
			}
			else if (blockName != null)
			{
				scopeIds = new ScopeIds();
				_currentScope.Names.Add(blockName);
			}
			else
			{
				scopeIds = new ScopeIds();
			}

			Scope tmpScope = _currentScope;
			if (scope != null)
			{
				_currentScope = scope;
			}
			

			syntax.Context.Scope = new Scope(_currentScope);

			bool tmp = _createBlockScope;
			_createBlockScope = innerblock;
			_blockIndices.Push(scopeIds);
			visit(syntax);
			_blockIndices.Pop();
			_createBlockScope = tmp;


			if (scope != null)
			{
				_currentScope = tmpScope;
			}
			else
			{
				_currentScope.Names.RemoveLast();
			}

			_blockIndices.Push(scopeIds);
			
		}

		private void HandleNewScopeBlock<T>(T syntax, Action<T> visit, NameSyntax name, bool innerblock = true) where T : SyntaxNode
		{
			if (name is QualifiedNameSyntax qualName)
			{
				string[] names = SemanticHelpers.ExtractNames(qualName);
				foreach (string val in names)
				{
					_currentScope.Names.Add(new NameIdentifier(val));
				}
			}
			else if (name is SimpleNameSyntax simpleName)
			{
				_currentScope.Names.Add(Identifier.GetIdentifier(simpleName));
			}

			syntax.Context.Scope = new Scope(_currentScope);

			bool tmp = _createBlockScope;
			_createBlockScope = innerblock;
			visit(syntax);
			_createBlockScope = tmp;

			_currentScope.Names.RemoveAt(_currentScope.Names.Count - 1);
		}
	}
}
