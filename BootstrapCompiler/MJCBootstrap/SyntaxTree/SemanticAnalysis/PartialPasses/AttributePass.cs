﻿using System.Collections.Generic;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.PartialPasses
{
	// TODO: Handle scoped attributes
	public class AttributePass : ISemanticPass
	{
		private List<Dictionary<string, CompileAttribute>> _scopeCompileAttribs = new List<Dictionary<string, CompileAttribute>>();
		private SyntaxTree _tree;

		public override void Visit(SyntaxTree tree)
		{
			_tree = tree;
			base.Visit(tree);
			_tree = null;
		}

		protected override void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			if (syntax.Attributes?.CompilerAtAttribs != null)
			{
				_scopeCompileAttribs.Add(ProcessCompileAttribs(syntax.Attributes.CompilerAtAttribs));
			}

			base.VisitBlockStatement(syntax);
		}

		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			base.VisitDeclaration(syntax);

			if (syntax.Attributes != null)
			{
				ProcessCompileAttribs(syntax.Context, syntax.Attributes.CompilerAtAttribs);

				ProcessAttributes(syntax.Context, syntax.Attributes);
			}
		}

		protected override void VisitName(NameSyntax syntax)
		{
			base.VisitName(syntax);

			if (syntax.Attribs != null)
			{
				ProcessAttributes(syntax.Context, syntax.Attribs);
			}
		}

		protected override void VisitDelegate(DelegateSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				ProcessCompileAttribs(syntax.Context, syntax.Attributes.CompilerAtAttribs);

				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
			}

			base.VisitDelegate(syntax);
		}

		protected override void VisitEnum(EnumSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				ProcessCompileAttribs(syntax.Context, syntax.Attributes.CompilerAtAttribs);

				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
			}

			base.VisitEnum(syntax);
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				if (syntax.Attributes.CompilerAtAttribs != null)
					ProcessCompileAttribs(syntax.Context, syntax.Attributes.CompilerAtAttribs);

				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
			}

			base.VisitFunction(syntax);
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				if (syntax.Attributes.CompilerAtAttribs != null)
					ProcessCompileAttribs(syntax.Context, syntax.Attributes.CompilerAtAttribs);
			}

			base.VisitImpl(syntax);
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				ProcessCompileAttribs(syntax.Context, syntax.Attributes.CompilerAtAttribs);

				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
			}

			base.VisitInterface(syntax);
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			//base.VisitModuleDirective(syntax);

			if (syntax.Attributes != null)
			{
				ProcessCompileAttribs(syntax.Context, syntax.Attributes.CompilerAtAttribs);
				_scopeCompileAttribs.Add(syntax.Context.CompileAttribs);
			}
		}

		protected override void VisitNamespace(NamespaceSyntax syntax)
		{
			if (syntax.Attributes != null)
			{
				_scopeCompileAttribs.Add(ProcessCompileAttribs(syntax.Attributes.CompilerAtAttribs));
			}

			base.VisitNamespace(syntax);
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			base.VisitStruct(syntax);

			if (syntax.Attributes != null)
			{
				ProcessCompileAttribs(syntax.Context, syntax.Attributes.CompilerAtAttribs);

				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
			}
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			base.VisitUnion(syntax);

			if (syntax.Attributes != null)
			{
				ProcessCompileAttribs(syntax.Context, syntax.Attributes.CompilerAtAttribs);

				syntax.Context.Visibility = GetVisibility(syntax.Attributes.SingleAttribs);
			}
		}

		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			base.VisitAttributedType(syntax);
			ProcessAttributes(syntax.Context, syntax.Attributes);
		}

		void ProcessCompileAttribs(SemanticContext context, List<CompilerAtAttributeSyntax> attribs)
		{
			if (attribs == null)
				return;

			foreach (CompilerAtAttributeSyntax atAttrib in attribs)
			{
				CompileAttribute attribute = GetCompileAttrib(atAttrib);
				if (attribute != null)
					AddCompileAttrib(context, attribute);
			}

			// Update for scoped attribs
			for (var index = _scopeCompileAttribs.Count - 1; index >= 0; index--)
			{
				Dictionary<string, CompileAttribute> attrib = _scopeCompileAttribs[index];

				foreach (KeyValuePair<string, CompileAttribute> pair in attrib)
				{
					if (context.Symbol != null)
					{
						if (!context.Symbol.CompileAttribs.ContainsKey(pair.Key))
							context.Symbol.CompileAttribs.Add(pair.Key, pair.Value);
					}
					else if (context.SymbolList != null)
					{
						foreach (Symbol symbol in context.SymbolList)
						{
							if (!symbol.CompileAttribs.ContainsKey(pair.Key))
								symbol.CompileAttribs.Add(pair.Key, pair.Value);
						}
					}
				}
			}
		}

		Dictionary<string, CompileAttribute> ProcessCompileAttribs(List<CompilerAtAttributeSyntax> attribs)
		{
			Dictionary<string, CompileAttribute> compileAttribs = new Dictionary<string, CompileAttribute>();
			foreach (CompilerAtAttributeSyntax atAttrib in attribs)
			{
				CompileAttribute attribute = GetCompileAttrib(atAttrib);
				if (attribute != null)
					compileAttribs.Add(attribute.CurId, attribute);
			}
			return compileAttribs;
		}

		public CompileAttribute GetCompileAttrib(CompilerAtAttributeSyntax attrib)
		{
			string qualifiedName = attrib.Name.ToString();
			List<ArgumentSyntax> arguments = attrib.Arguments;

			switch (qualifiedName)
			{
			case "align":
			{
				var litExpr = arguments[0].Expression as LiteralExpressionSyntax;
				if (litExpr == null)
					return null;

				int alignment = (int)litExpr.LiteralToken.Values.Integer;
				return new AlignCompileAttribute(alignment);
			}
			case "dynamic_load":
			{
				return new DynamicallyLoadedCompileAttribute();
			}
			case "foreign":
			{
				var litExpr = arguments?[0]?.Expression as LiteralExpressionSyntax;
				var lib = litExpr?.LiteralToken.Text.Trim('"');

				if (!_tree.ForeignLibs.Contains(lib))
					_tree.ForeignLibs.Add(lib);

				return new ForeignCompileAttribute(lib);
			}
			case "link_name":
			{
				var litExpr = arguments?[0]?.Expression as LiteralExpressionSyntax;
				string name;
				if (litExpr == null)
					name = "__unknown_link_name";
				else
					name = litExpr.LiteralToken.Text.Trim('"');

				return new LinkNameCompileAttribute(name);
			}
			case "call_conv":
			{
				var litExpr = arguments?[0]?.Expression as LiteralExpressionSyntax;
				string callConv;
				if (litExpr == null)
					callConv = "mjay";
				else
					callConv = litExpr.LiteralToken.Text.Trim('"').ToLower();

				return new CallingConventionAttribute(callConv);
			}
			case "default_visibility":
			{
				var litExpr = arguments?[0]?.Expression as LiteralExpressionSyntax;
				string visibilityName;
				if (litExpr == null)
					visibilityName = "private";
				else
					visibilityName = litExpr.LiteralToken.Text.Trim('"').ToLower();

				SemanticVisibility visibility;
				switch (visibilityName)
				{
				case "internal":
					visibility = SemanticVisibility.Internal;
					break;
				case "package":
					visibility = SemanticVisibility.Package;
					break;
				case "public":
					visibility = SemanticVisibility.Public;
					break;
				case "export":
					visibility = SemanticVisibility.Export;
					break;
				case "private":
				default:
					visibility = SemanticVisibility.Private;
					break;
				}

				return new DefaultVisibilityAttribute(visibility);
			}
			case "func_ptr":
			{
				return new FuncPtrAttribute();
			}
			case "arr_ptr":
			{
				return new ArrPtrAttribute();
			}
			case "allow_extension":
			{
				return new AllowExtensionAttribute();
			}
			case "extend_builtin":
			{
				return new ExtendBuiltinAttribute();
			}
			case "core_stack_array":
			{
				return new CoreStackArrayAttribute();
			}

			case "interp_func":
			{
				var litExpr = arguments?[0]?.Expression as LiteralExpressionSyntax;
				string name;
				if (litExpr == null)
					name = "_";
				else
					name = litExpr.LiteralToken.Text.Trim('"');

				return new InterpFuncAttribute(name);
			}
			default:
				return null;
			}
		}


		void AddCompileAttrib(SemanticContext context, CompileAttribute attrib)
		{
			if (context.Symbol != null)
			{
				context.Symbol.CompileAttribs.Add(attrib.CurId, attrib);
			}
			else if (context.SymbolList != null)
			{
				foreach (Symbol symbol in context.SymbolList)
				{
					symbol.CompileAttribs.Add(attrib.CurId, attrib);
				}
			}
			else
			{
				context.CompileAttribs.Add(attrib.CurId, attrib);
			}
		}

		void ProcessAttributes(SemanticContext context, AttributeSyntax attribSyntax)
		{
			if (attribSyntax.SingleAttribs != null)
			{
				SemanticAttributes attribs = SemanticAttributes.None;
				foreach (SingleAttributeSyntax singleAttrib in attribSyntax.SingleAttribs)
				{
					switch (singleAttrib.AttributeToken.Type)
					{
					case TokenType.CConst:
						attribs |= SemanticAttributes.CConst;
						break;
					case TokenType.Const:
						attribs |= SemanticAttributes.Const;
						break;
					case TokenType.Immutable:
						attribs |= SemanticAttributes.Immutable;
						break;
					case TokenType.Static:
						attribs |= SemanticAttributes.Static;
						break;
					case TokenType.Synchronized:
						attribs |= SemanticAttributes.Synchronized;
						break;
					case TokenType.Shared:
						attribs |= SemanticAttributes.Shared;
						break;
					case TokenType.UUGlobal:
						attribs |= SemanticAttributes.Global;
						break;
					}
				}

				context.Attribs = attribs;
			}
		}

		private SemanticVisibility GetVisibility(List<SingleAttributeSyntax> attribs)
		{
			SemanticVisibility defaultVisibility = SemanticVisibility.Private;
			for (var i = _scopeCompileAttribs.Count - 1; i >= 0; i--)
			{
				Dictionary<string, CompileAttribute> compileAttribs = _scopeCompileAttribs[i];
				if (compileAttribs.ContainsKey(DefaultVisibilityAttribute.Id))
				{
					defaultVisibility = (compileAttribs[DefaultVisibilityAttribute.Id] as DefaultVisibilityAttribute).DefaultVisibility;
					break;
				}
			}

			if (attribs == null)
				return defaultVisibility;

			SemanticVisibility visibility = SemanticVisibility.Default;
			foreach (SingleAttributeSyntax attrib in attribs)
			{
				SemanticVisibility tmp = SemanticVisibility.Default;
				switch (attrib.AttributeToken.Type)
				{
				case TokenType.Private:
					tmp = SemanticVisibility.Private;
					break;
				case TokenType.Internal:
					tmp = SemanticVisibility.Internal;
					break;
				case TokenType.Public:
					tmp = SemanticVisibility.Public;
					break;
				case TokenType.Export:
					tmp = SemanticVisibility.Export;
					break;
				}
				if (tmp == SemanticVisibility.Default)
					continue;

				if (visibility != SemanticVisibility.Default)
				{
					ErrorSystem.AstError(attrib.FullSpan, $"multiple visibility attributes: {tmp.ToString().ToLower()}");
				}
				visibility = tmp;
			}

			if (visibility == SemanticVisibility.Default)
				visibility = defaultVisibility;

			return visibility;
		}

	}
}
