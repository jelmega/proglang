﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC.SyntaxTree
{
	public class SyntaxNode
	{
		public TextSpan FullSpan { get; protected set; }

		public SemanticContext Context = new SemanticContext();

		public virtual string ToFullString() { return null; }
	}

	#region General
	public class CompilationUnitSyntax : SyntaxNode
	{
		public PackageDirectiveSyntax Package { get; }
		public ModuleDirectiveSyntax Module { get; }
		public BlockStatementSyntax Block { get; }
		public SyntaxToken EndOfFileToken { get; }

		public CompilationUnitSyntax(PackageDirectiveSyntax package, ModuleDirectiveSyntax module, BlockStatementSyntax block, SyntaxToken endOfFileToken)
		{
			Package = package;
			Module = module;
			Block = block;
			EndOfFileToken = endOfFileToken;

			int start = Package?.FullSpan.Start ?? Module?.FullSpan.Start ?? Block?.FullSpan.Start ?? EndOfFileToken.FullSpan.Start;
			FullSpan = new TextSpan(start, EndOfFileToken.FullSpan.End);
		}

		public override string ToString()
		{
			string str = "";
			if (Package != null)
				str += Package.ToString();
			if (Module != null)
				str += Module.ToString();
			if (Block != null)
				str += Block.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Package != null)
				str += Package.ToFullString();
			if (Module != null)
				str += Module.ToFullString();
			if (Block != null)
				str += Block.ToFullString();
			return str;
		}
	}

	public class PackageDirectiveSyntax : SyntaxNode
	{
		public SyntaxToken PackageToken;
		public NameSyntax Name;
		public SyntaxToken SemicolonToken;

		public PackageDirectiveSyntax(SyntaxToken package, NameSyntax name, SyntaxToken semicolon)
		{
			PackageToken = package;
			Name = name;
			SemicolonToken = semicolon;

			int start = PackageToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return PackageToken.ToString() + ' ' + Name.ToString() + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			return PackageToken.ToFullString() + Name.ToFullString() + SemicolonToken.ToFullString();
		}
	}

	public class ModuleDirectiveSyntax : SyntaxNode
	{
		public AttributeSyntax Attributes;
		public SyntaxToken ModuleToken;
		public IdentifierNameSyntax Name;
		public SyntaxToken SemicolonToken;

		public ModuleDirectiveSyntax(AttributeSyntax attribs, SyntaxToken module, IdentifierNameSyntax name, SyntaxToken semicolon)
		{
			Attributes = attribs;
			ModuleToken = module;
			Name = name;
			SemicolonToken = semicolon;

			int start = Attributes?.FullSpan.Start ?? ModuleToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return ModuleToken.ToString() + ' ' + Name.ToString() + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			return ModuleToken.ToFullString() + Name.ToFullString() + SemicolonToken.ToFullString();
		}
	}

	public class NameSyntax : SimpleExpressionSyntax
	{
		public AttributeSyntax Attribs;
	}

	public class SimpleNameSyntax : NameSyntax
	{

	}

	public class IdentifierNameSyntax : SimpleNameSyntax
	{
		public SyntaxToken Identifier;

		public IdentifierNameSyntax(AttributeSyntax attribs, SyntaxToken identifier)
		{
			Attribs = attribs;
			Identifier = identifier;

			int start = Attribs?.FullSpan.Start ?? Identifier.FullSpan.Start;
			int end = Identifier.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = Attribs != null ? $"{Attribs} " : "";
			return str + Identifier.ToString();
		}

		public override string ToFullString()
		{
			string str = Attribs?.ToFullString() ?? "";
			return str + Identifier.ToFullString();
		}
	}

	public class TemplateNameSyntax : SimpleNameSyntax
	{
		public SyntaxToken Identifier;
		public SyntaxToken OpenTemplateToken;
		public List<ArgumentSyntax> Arguments;
		public SyntaxToken CloseTemplateToken;

		public TemplateNameSyntax(AttributeSyntax attribs, SyntaxToken identifier, SyntaxToken openTemplate, List<ArgumentSyntax> arguments, SyntaxToken closeTemplate)
		{
			Attribs = attribs;
			Identifier = identifier;
			OpenTemplateToken = openTemplate;
			Arguments = arguments;
			CloseTemplateToken = closeTemplate;

			int start = Attribs?.FullSpan.Start ?? Identifier.FullSpan.Start;
			int end = CloseTemplateToken?.FullSpan.End ?? Arguments.Last().FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attribs != null)
				str += $"{Attribs} ";
			str += Identifier.ToString() + OpenTemplateToken.ToString();
			if (Arguments != null)
			{
				foreach (ArgumentSyntax arg in Arguments)
				{
					str += arg.ToString();
				}
			}
			if (CloseTemplateToken != null)
				str += CloseTemplateToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attribs != null)
				str += Attribs.ToFullString();
			str += Identifier.ToFullString() + OpenTemplateToken.ToFullString();
			if (Arguments != null)
			{
				foreach (ArgumentSyntax arg in Arguments)
				{
					str += arg.ToFullString();
				}
			}
			if (CloseTemplateToken != null)
				str += CloseTemplateToken.ToFullString();
			return str;
		}
	}

	public class QualifiedNameSyntax : NameSyntax
	{
		public NameSyntax Left;
		public SyntaxToken SeperationToken;
		public SimpleNameSyntax Right;

		public QualifiedNameSyntax(AttributeSyntax attribs, NameSyntax left, SyntaxToken seperation, SimpleNameSyntax right)
		{
			Attribs = attribs;
			Left = left;
			SeperationToken = seperation;
			Right = right;

			int start = Attribs?.FullSpan.Start ?? Left?.FullSpan.Start ?? SeperationToken?.FullSpan.Start ?? Right.FullSpan.Start;
			int end = Right.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attribs != null)
				str += $"{Attribs} ";
			if (Left != null)
				str += Left.ToString();
			if (SeperationToken != null)
				str += SeperationToken.ToString();
			return str + Right.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attribs != null)
				str += Attribs.ToFullString();
			if (Left != null)
				str += Left.ToFullString();
			if (SeperationToken != null)
				str += SeperationToken.ToFullString();
			return str + Right.ToFullString();
		}
	}

	public class NameListElemSyntax : SyntaxNode
	{
		public NameSyntax Name;
		public SyntaxToken CommaToken;

		public NameListElemSyntax(NameSyntax name, SyntaxToken comma)
		{
			Name = name;
			CommaToken = comma;

			int start = Name.FullSpan.Start;
			int end = CommaToken?.FullSpan.End ?? Name.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = Name.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = Name.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class NameListSyntax : NameSyntax
	{
		public List<NameListElemSyntax> Names;

		public NameListSyntax(List<NameListElemSyntax> names)
		{
			Names = names;

			int start = Names[0].FullSpan.Start;
			int end = Names.Last().FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			foreach (NameListElemSyntax name in Names)
			{
				str += name.ToString();
			}
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			foreach (NameListElemSyntax name in Names)
			{
				str += name.ToFullString();
			}
			return str;
		}
	}

	public class AttributeSyntax : SyntaxNode
	{
		public List<SingleAttributeSyntax> SingleAttribs;
		public List<AtAttributeSyntax> AtAttribs;
		public List<CompilerAtAttributeSyntax> CompilerAtAttribs;

		public AttributeSyntax(List<SingleAttributeSyntax> singleAttribs, List<AtAttributeSyntax> atAttribs, List<CompilerAtAttributeSyntax> compilerAtAttribs)
		{
			SingleAttribs = singleAttribs;
			AtAttribs = atAttribs;
			CompilerAtAttribs = compilerAtAttribs;

			int start = -1, end = -1;
			if (SingleAttribs!= null)
			{
				start = SingleAttribs[0].FullSpan.Start;
				end = SingleAttribs.Last().FullSpan.End;
			}

			if (AtAttribs != null)
			{
				if (start == -1)
					start = AtAttribs[0].FullSpan.Start;
				else
					start = Math.Min(start, AtAttribs[0].FullSpan.Start);

				if (end == -1)
					end = AtAttribs.Last().FullSpan.End;
				else
					end = Math.Max(end, AtAttribs.Last().FullSpan.End);
			}

			if (CompilerAtAttribs != null)
			{
				if (start == -1)
					start = CompilerAtAttribs[0].FullSpan.Start;
				else
					start = Math.Min(start, CompilerAtAttribs[0].FullSpan.Start);

				if (end == -1)
					end = CompilerAtAttribs.Last().FullSpan.End;
				else
					end = Math.Max(end, CompilerAtAttribs.Last().FullSpan.End);
			}

			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			int singleIdx = 0;
			int atIdx = 0;
			int compilerAtIdx = 0;
			while (true)
			{
				int singleStart = (SingleAttribs != null && SingleAttribs.Count > singleIdx) ? SingleAttribs[singleIdx].FullSpan.Start : Int32.MaxValue;
				int atStart = (AtAttribs != null && AtAttribs.Count > atIdx) ? AtAttribs[atIdx].FullSpan.Start : Int32.MaxValue;
				int compilerAtStart = (CompilerAtAttribs != null && CompilerAtAttribs.Count > compilerAtIdx) ? CompilerAtAttribs[compilerAtIdx].FullSpan.Start : Int32.MaxValue;

				int min = Math.Min(Math.Min(singleStart, atStart), compilerAtStart);

				if (min == Int32.MaxValue)
					break;

				if (min == singleStart)
				{
					str += $"{SingleAttribs?[singleIdx]} ";
					++singleIdx;
				}
				else if (min == atStart)
				{
					str += $"{AtAttribs?[atIdx]} ";
					++atIdx;
				}
				else
				{
					str += $"{CompilerAtAttribs?[compilerAtIdx]} ";
					++compilerAtIdx;
				}
			}
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			int singleIdx = 0;
			int atIdx = 0;
			int compilerAtIdx = 0;
			while (true)
			{
				int singleStart = (SingleAttribs != null && SingleAttribs.Count > singleIdx) ? SingleAttribs[singleIdx].FullSpan.Start : Int32.MaxValue;
				int atStart = (AtAttribs != null && AtAttribs.Count > atIdx) ? AtAttribs[atIdx].FullSpan.Start : Int32.MaxValue;
				int compilerAtStart = (CompilerAtAttribs != null && CompilerAtAttribs.Count > compilerAtIdx) ? CompilerAtAttribs[compilerAtIdx].FullSpan.Start : Int32.MaxValue;

				int min = Math.Min(Math.Min(singleStart, atStart), compilerAtStart);

				if (min == Int32.MaxValue)
					break;

				if (min == singleStart)
				{
					str += $"{SingleAttribs?[singleIdx].ToFullString()}";
					++singleIdx;
				}
				else if (min == atStart)
				{
					str += $"{AtAttribs?[atIdx].ToFullString()}";
					++atIdx;
				}
				else
				{
					str += $"{CompilerAtAttribs?[compilerAtIdx].ToFullString()}";
					++compilerAtIdx;
				}
			}
			return str;
		}
	}

	public class AtAttributeSyntax : SyntaxNode
	{
		public SyntaxToken AtToken;
		public NameSyntax Name;
		public SyntaxToken OpenParenToken;
		public List<ArgumentSyntax> Arguments;
		public SyntaxToken CloseParenToken;

		public AtAttributeSyntax(SyntaxToken at, NameSyntax name, SyntaxToken openParen, List<ArgumentSyntax> args, SyntaxToken closeParen)
		{
			AtToken = at;
			Name = name;
			OpenParenToken = openParen;
			Arguments = args;
			CloseParenToken = closeParen;

			int start = AtToken.FullSpan.Start;
			int end = CloseParenToken?.FullSpan.End ?? Name.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = AtToken.ToString() + Name.ToString();
			if (OpenParenToken != null)
			{
				str += OpenParenToken.ToString();
				if (Arguments != null)
				{
					foreach (ArgumentSyntax arg in Arguments)
					{
						str += arg.ToString();
					}
				}
				str += CloseParenToken.ToString();
			}
			return str;
		}

		public override string ToFullString()
		{
			string str = AtToken.ToFullString() + Name.ToFullString();
			if (OpenParenToken != null)
			{
				str += OpenParenToken.ToFullString();
				if (Arguments != null)
				{
					foreach (ArgumentSyntax arg in Arguments)
					{
						str += arg.ToFullString();
					}
				}
				str += CloseParenToken.ToFullString();
			}
			return str;
		}
	}

	public class CompilerAtAttributeSyntax : SyntaxNode
	{
		public SyntaxToken AtColonToken;
		public NameSyntax Name;
		public SyntaxToken OpenParenToken;
		public List<ArgumentSyntax> Arguments;
		public SyntaxToken CloseParenToken;

		public CompilerAtAttributeSyntax(SyntaxToken atColon, NameSyntax name, SyntaxToken openParen, List<ArgumentSyntax> args, SyntaxToken closeParen)
		{
			AtColonToken = atColon;
			Name = name;
			OpenParenToken = openParen;
			Arguments = args;
			CloseParenToken = closeParen;

			int start = AtColonToken.FullSpan.Start;
			int end = CloseParenToken?.FullSpan.End ?? Name.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = AtColonToken.ToString() + Name.ToString();
			if (OpenParenToken != null)
			{
				str += OpenParenToken.ToString();
				if (Arguments != null)
				{
					foreach (ArgumentSyntax arg in Arguments)
					{
						str += arg.ToString();
					}
				}
				str += CloseParenToken.ToString();
			}
			return str;
		}

		public override string ToFullString()
		{
			string str = AtColonToken.ToFullString() + Name.ToFullString();
			if (OpenParenToken != null)
			{
				str += OpenParenToken.ToFullString();
				if (Arguments != null)
				{
					foreach (ArgumentSyntax arg in Arguments)
					{
						str += arg.ToFullString();
					}
				}
				str += CloseParenToken.ToFullString();
			}
			return str;
		}
	}

	public class SingleAttributeSyntax : SyntaxNode
	{
		public SyntaxToken AttributeToken;

		public SingleAttributeSyntax(SyntaxToken attrib)
		{
			AttributeToken = attrib;
			FullSpan = new TextSpan(AttributeToken.FullSpan.Start, AttributeToken.FullSpan.Length);
		}

		public override string ToString()
		{
			return AttributeToken.ToString();
		}

		public override string ToFullString()
		{
			return AttributeToken.ToFullString();
		}
	}
	
	public class ArgumentSyntax : SyntaxNode
	{
		public IdentifierNameSyntax Identifier;
		public SyntaxToken ColonToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CommaToken;

		public ArgumentSyntax(IdentifierNameSyntax identifier, SyntaxToken colon, SimpleExpressionSyntax expression, SyntaxToken comma)
		{
			Identifier = identifier;
			ColonToken = colon;
			Expression = expression;
			CommaToken = comma;

			int start = Identifier?.FullSpan.Start ?? Expression.FullSpan.Start;
			int end = CommaToken?.FullSpan.End ?? Expression.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Identifier != null)
				str += Identifier.ToString() + ColonToken.ToString();
			str += Expression.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Identifier != null)
				str += Identifier.ToFullString() + ColonToken.ToFullString();
			str += Expression.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class ParameterSyntax : SyntaxNode
	{
		public AttributeSyntax Attribs;
		public IdentifierNameSyntax Identifier;
		public SyntaxToken ColonToken;
		public TypeSyntax Type;
		public SyntaxToken VariadicToken;
		public SyntaxToken EqualsToken;
		public SimpleExpressionSyntax DefaultValue;
		public SyntaxToken CommaToken;

		public ParameterSyntax(AttributeSyntax attribs, IdentifierNameSyntax identifier, SyntaxToken colon, TypeSyntax type, SyntaxToken variadic, SyntaxToken equalsToken, SimpleExpressionSyntax defaultValue, SyntaxToken comma)
		{
			Attribs = attribs;
			Identifier = identifier;
			ColonToken = colon;
			Type = type;
			EqualsToken = equalsToken;
			DefaultValue = defaultValue;
			VariadicToken = variadic;
			CommaToken = comma;

			int start = Attribs?.FullSpan.Start ?? Identifier.FullSpan.Start;
			int end = Type?.FullSpan.End ?? Identifier.FullSpan.End;

			if (CommaToken != null)
				end = CommaToken.FullSpan.End;
			else if (DefaultValue != null)
				end = DefaultValue.FullSpan.End;

			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attribs != null)
				str += $"{Attribs} ";
			if (Identifier != null)
				str += Identifier.ToString();
			if (ColonToken != null)
				str += ColonToken.ToString();
			if (Type != null)
				str += Type.ToString();
			if (VariadicToken != null)
				str += VariadicToken.ToString();
			if (EqualsToken != null)
				str += EqualsToken.ToString() + DefaultValue.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attribs != null)
				str += Attribs.ToFullString();
			if (Identifier != null)
				str += Identifier.ToFullString();
			if (ColonToken != null)
				str += ColonToken.ToFullString();
			if (Type != null)
				str += Type.ToFullString();
			if (VariadicToken != null)
				str += VariadicToken.ToString();
			if (EqualsToken != null)
				str += EqualsToken.ToFullString() + DefaultValue.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class TemplateParameterSyntax : SyntaxNode
	{
		public IdentifierNameSyntax Identifier;
		public SyntaxToken ColonToken;
		public TypeSyntax Type;
		public SyntaxToken ColonEqualsToken;
		public SimpleExpressionSyntax SpecializedValue;
		public SyntaxToken VariadicToken;
		public SyntaxToken EqualsToken;
		public SimpleExpressionSyntax DefaultValue;
		public SyntaxToken CommaToken;

		public TemplateParameterSyntax(IdentifierNameSyntax identifier, SyntaxToken colon, TypeSyntax type, SyntaxToken colonEquals, SimpleExpressionSyntax specializedValue, SyntaxToken variadic, SyntaxToken equals, SimpleExpressionSyntax defaultValue, SyntaxToken comma)
		{
			Identifier = identifier;
			ColonToken = colon;
			Type = type;
			ColonEqualsToken = colonEquals;
			SpecializedValue = specializedValue;
			VariadicToken = variadic;
			EqualsToken = equals;
			DefaultValue = defaultValue;
			CommaToken = comma;

			int start = Identifier.FullSpan.Start;
			int end = CommaToken?.FullSpan.End ?? DefaultValue?.FullSpan.End ?? VariadicToken?.FullSpan.End ?? specializedValue?.FullSpan.End ?? type?.FullSpan.End ?? Identifier.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = Identifier.ToString();
			if (ColonToken != null)
				str += ColonToken.ToString() + Type.ToString();
			if (ColonEqualsToken != null)
				str += ColonEqualsToken.ToString() + SpecializedValue.ToString();
			if (VariadicToken != null)
				str += VariadicToken.ToString();
			if (EqualsToken != null)
				str += EqualsToken.ToString() + DefaultValue.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = Identifier.ToFullString();
			if (ColonToken != null)
				str += ColonToken.ToFullString() + Type.ToFullString();
			if (ColonEqualsToken != null)
				str += ColonEqualsToken.ToFullString() + SpecializedValue.ToFullString();
			if (VariadicToken != null)
				str += VariadicToken.ToString();
			if (EqualsToken != null)
				str += EqualsToken.ToFullString() + DefaultValue.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class TypeSyntax : SimpleExpressionSyntax
	{

	}

	public class BuiltinTypeSyntax : TypeSyntax
	{
		public SyntaxToken TypeToken;

		public BuiltinTypeSyntax(SyntaxToken type)
		{
			TypeToken = type;

			FullSpan = new TextSpan(TypeToken.FullSpan.Start, TypeToken.FullSpan.Length);
		}

		public override string ToString()
		{
			return TypeToken.ToString();
		}

		public override string ToFullString()
		{
			return TypeToken.ToFullString();
		}
	}

	public class IdentifierTypeSyntax : TypeSyntax
	{
		public NameSyntax Name;

		public IdentifierTypeSyntax(NameSyntax name)
		{
			Name = name;

			FullSpan = new TextSpan(Name.FullSpan.Start, Name.FullSpan.Length);
		}

		public override string ToString()
		{
			return Name.ToString();
		}

		public override string ToFullString()
		{
			return Name.ToFullString();
		}
	}

	public class PointerTypeSyntax : TypeSyntax
	{
		public SyntaxToken PointerToken;
		public TypeSyntax BaseType;

		public PointerTypeSyntax(SyntaxToken pointer, TypeSyntax baseType)
		{
			PointerToken = pointer;
			BaseType = baseType;

			int start = PointerToken.FullSpan.Start;
			int end = BaseType.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return PointerToken.ToString() + BaseType.ToString();
		}

		public override string ToFullString()
		{
			return PointerToken.ToFullString() + BaseType.ToFullString();
		}
	}

	public class ReferenceTypeSyntax : TypeSyntax
	{
		public SyntaxToken ReferenceToken;
		public TypeSyntax BaseType;

		public ReferenceTypeSyntax(SyntaxToken reference, TypeSyntax baseType)
		{
			ReferenceToken = reference;
			BaseType = baseType;

			int start = ReferenceToken.FullSpan.Start;
			int end = BaseType.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return ReferenceToken.ToString() + BaseType.ToString();
		}

		public override string ToFullString()
		{
			return ReferenceToken.ToFullString() + BaseType.ToFullString();
		}
	}

	public class ArrayTypeSyntax : TypeSyntax
	{
		public SyntaxToken OpenBracketToken;
		public SimpleExpressionSyntax SizeExpression;
		public SyntaxToken CloseBracketToken;
		public TypeSyntax BaseType;

		public ArrayTypeSyntax(SyntaxToken openBracket, SimpleExpressionSyntax sizeExpression, SyntaxToken closeBracket, TypeSyntax baseType)
		{
			OpenBracketToken = openBracket;
			SizeExpression = sizeExpression;
			CloseBracketToken = closeBracket;
			BaseType = baseType;

			int start = OpenBracketToken.FullSpan.Start;
			int end = BaseType.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = OpenBracketToken.ToString();
			if (SizeExpression != null)
				str += SizeExpression.ToString();
			str += CloseBracketToken.ToString();
			return str + BaseType.ToString();
		}

		public override string ToFullString()
		{
			string str = OpenBracketToken.ToFullString();
			if (SizeExpression != null)
				str += SizeExpression.ToFullString();
			str += CloseBracketToken.ToFullString();
			return str + BaseType.ToFullString();
		}
	}

	public class AttributedTypeSyntax : TypeSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken OpenParenToken;
		public TypeSyntax BaseType;
		public SyntaxToken CloseParenToken;

		public AttributedTypeSyntax(AttributeSyntax attribs, SyntaxToken openParen, TypeSyntax baseType, SyntaxToken closeParen)
		{
			Attributes = attribs;
			OpenParenToken = openParen;
			BaseType = baseType;
			CloseParenToken = closeParen;

			int start = Attributes.FullSpan.Start;
			int end = CloseParenToken?.FullSpan.End ?? BaseType.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = Attributes.ToString();
			if (OpenParenToken != null)
				str += OpenParenToken.ToString();
			str += BaseType.ToString();
			if (CloseParenToken != null)
				str += CloseParenToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = Attributes.ToFullString();
			if (OpenParenToken != null)
				str += OpenParenToken.ToFullString();
			str += BaseType.ToFullString();
			if (CloseParenToken != null)
				str += CloseParenToken.ToFullString();
			return str;
		}
	}

	public class TupleSubTypeSyntax : TypeSyntax
	{
		public TypeSyntax BaseType;
		public SyntaxToken CommaToken;

		public TupleSubTypeSyntax(TypeSyntax baseType, SyntaxToken comma)
		{
			BaseType = baseType;
			CommaToken = comma;

			int start = BaseType.FullSpan.Start;
			int end = CommaToken?.FullSpan.End ?? BaseType.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = BaseType.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = BaseType.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class TupleTypeSyntax : TypeSyntax
	{
		public SyntaxToken OpenParenToken;
		public List<TupleSubTypeSyntax> SubTypes;
		public SyntaxToken CloseParenToken;

		public TupleTypeSyntax(SyntaxToken openParen, List<TupleSubTypeSyntax> subTypes, SyntaxToken closeParen)
		{
			OpenParenToken = openParen;
			SubTypes = subTypes;
			CloseParenToken = closeParen;

			int start = OpenParenToken.FullSpan.Start;
			int end = CloseParenToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = OpenParenToken.ToString();
			foreach (TupleSubTypeSyntax subType in SubTypes)
			{
				str += subType.ToString();
			}
			return str + CloseParenToken.ToString();
		}

		public override string ToFullString()
		{
			string str = OpenParenToken.ToFullString();
			foreach (TupleSubTypeSyntax subType in SubTypes)
			{
				str += subType.ToFullString();
			}
			return str + CloseParenToken.ToFullString();
		}
	}

	public class NullableTypeSyntax : TypeSyntax
	{
		public SyntaxToken QuestionToken;
		public TypeSyntax BaseType;

		public NullableTypeSyntax(TypeSyntax baseType, SyntaxToken questionToken)
		{
			QuestionToken = questionToken;
			BaseType = baseType;

			int start = QuestionToken.FullSpan.Start;
			int end = BaseType.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return QuestionToken.ToString() + BaseType.ToString();
		}  

		public override string ToFullString()
		{
			return QuestionToken.ToFullString() + BaseType.ToFullString();
		}
	}

	public class VariadicTypeSyntax : TypeSyntax
	{
		public TypeSyntax BaseType;
		public SyntaxToken VariadicToken;

		public VariadicTypeSyntax(TypeSyntax baseType, SyntaxToken variadic)
		{
			BaseType = baseType;
			VariadicToken = variadic;

			int start = BaseType.FullSpan.Start;
			int end = VariadicToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return BaseType.ToString() + VariadicToken.ToString();
		}

		public override string ToFullString()
		{
			return BaseType.ToFullString() + VariadicToken.ToFullString();
		}
	}

	public class TypeofSyntax : TypeSyntax
	{
		public SyntaxToken TypeofToken;
		public SyntaxToken OpenParenToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CloseParenToken;

		public TypeofSyntax(SyntaxToken typeofToken, SyntaxToken openParen, SimpleExpressionSyntax expression, SyntaxToken closeParen)
		{
			TypeofToken = typeofToken;
			OpenParenToken = openParen;
			Expression = expression;
			CloseParenToken = closeParen;

			int start = TypeofToken.FullSpan.Start;
			int end = CloseParenToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return TypeofToken.ToString() + OpenParenToken.ToString() + Expression.ToString() + CloseParenToken.ToString();
		}

		public override string ToFullString()
		{
			return TypeofToken.ToFullString() + OpenParenToken.ToFullString() + Expression.ToFullString() + CloseParenToken.ToFullString();
		}
	}

	public class DelegateSyntax : TypeSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken DelegateToken;
		public IdentifierNameSyntax Identifier;
		public SyntaxToken OpenParenToken;
		public List<ParameterSyntax> Parameters;
		public SyntaxToken CloseParenToken;
		public SyntaxToken ArrowToken;
		public TypeSyntax ReturnType;
		public SyntaxToken SemicolonToken;

		public DelegateSyntax(AttributeSyntax attribs, SyntaxToken delegateToken, IdentifierNameSyntax identifier, SyntaxToken openParen, List<ParameterSyntax> parameters, SyntaxToken closeParen, SyntaxToken arrow, TypeSyntax returnType, SyntaxToken semicolon)
		{
			Attributes = attribs;
			DelegateToken = delegateToken;
			Identifier = identifier;
			OpenParenToken = openParen;
			Parameters = parameters;
			CloseParenToken = closeParen;
			ArrowToken = arrow;
			ReturnType = returnType;
			SemicolonToken = semicolon;

			int start = Attributes?.FullSpan.Start ?? DelegateToken.FullSpan.Start;
			int end = SemicolonToken?.FullSpan.End ?? ReturnType?.FullSpan.End ?? CloseParenToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString() + ' ';
			str += DelegateToken.ToString();
			if (Identifier != null)
				str += ' ' + Identifier.ToString();
			str += OpenParenToken.ToString();
			if (Parameters != null)
			{
				foreach (ParameterSyntax parameter in Parameters)
				{
					str += parameter.ToString();
				}
			}
			str += CloseParenToken.ToString();
			if (ArrowToken != null)
				str += ArrowToken.ToString() + ReturnType.ToString();
			if (SemicolonToken != null)
				str += SemicolonToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString() + ' ';
			str += DelegateToken.ToFullString();
			if (Identifier != null)
				str += Identifier.ToFullString();
			str += OpenParenToken.ToFullString();
			if (Parameters != null)
			{
				foreach (ParameterSyntax parameter in Parameters)
				{
					str += parameter.ToFullString();
				}
			}
			str += CloseParenToken.ToFullString();
			if (ArrowToken != null)
				str += ArrowToken.ToFullString() + ReturnType.ToFullString();
			if (SemicolonToken != null)
				str += SemicolonToken.ToFullString();
			return str;
		}
	}

	public class InlineStructTypeSyntax : TypeSyntax
	{
		public StructSyntax StructSyntax;

		public InlineStructTypeSyntax(StructSyntax structSyntax)
		{
			StructSyntax = structSyntax;

			FullSpan = new TextSpan(structSyntax.FullSpan.Start, structSyntax.FullSpan.Length);
		}

		public override string ToString()
		{
			return StructSyntax.ToString();
		}

		public override string ToFullString()
		{
			return StructSyntax.ToFullString();
		}
	}

	public class InlineUnionTypeSyntax : TypeSyntax
	{
		public UnionSyntax UnionSyntax;

		public InlineUnionTypeSyntax(UnionSyntax unionSyntax)
		{
			UnionSyntax = unionSyntax;

			FullSpan = new TextSpan(unionSyntax.FullSpan.Start, unionSyntax.FullSpan.Length);
		}

		public override string ToString()
		{
			return UnionSyntax.ToString();
		}

		public override string ToFullString()
		{
			return UnionSyntax.ToFullString();
		}
	}

	public class BitFieldTypeSyntax : TypeSyntax
	{
		public TypeSyntax BaseType;
		public SyntaxToken DotToken;
		public SimpleExpressionSyntax ExprSyntax;

		public BitFieldTypeSyntax(TypeSyntax baseType, SyntaxToken dot, SimpleExpressionSyntax expr)
		{
			BaseType = baseType;
			DotToken = dot;
			ExprSyntax = expr;

			int start = baseType.FullSpan.Start;
			int end = baseType.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return BaseType.ToString() + DotToken.ToString() + ExprSyntax.ToString();
		}

		public override string ToFullString()
		{
			return BaseType.ToFullString() + DotToken.ToFullString() + ExprSyntax.ToFullString();
		}
	}

	#endregion

	#region Statement

	public class StatementSyntax : SyntaxNode
	{

	}

	public class BlockStatementSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken OpenBraceToken;
		public List<StatementSyntax> Statements;
		public SyntaxToken CloseBraceToken;

		public BlockStatementSyntax(AttributeSyntax attribs, SyntaxToken openBrace, List<StatementSyntax> statements, SyntaxToken closeBrace)
		{
			Attributes = attribs;
			OpenBraceToken = openBrace;
			Statements = statements;
			CloseBraceToken = closeBrace;

			int start = Attributes?.FullSpan.Start ?? OpenBraceToken?.FullSpan.Start ?? -1;
			int end = CloseBraceToken?.FullSpan.End ?? -1;
			if (Statements != null && Statements.Count > 0)
			{
				if (start == -1)
					start = Statements[0].FullSpan.Start;
				if (end == -1)
					end = Statements[Statements.Count - 1].FullSpan.End;
			}
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			if (OpenBraceToken != null)
				str += OpenBraceToken.ToString();
			if (Statements != null)
			{
				foreach (StatementSyntax statement in Statements)
				{
					str += statement.ToString();
				}
			}
			if (CloseBraceToken != null)
				str += CloseBraceToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();
			if (OpenBraceToken != null)
				str += OpenBraceToken.ToFullString();
			if (Statements != null)
			{
				foreach (StatementSyntax statement in Statements)
				{
					str += statement.ToFullString();
				}
			}
			if (CloseBraceToken != null)
				str += CloseBraceToken.ToFullString();
			return str;
		}
	}

	public class NamespaceSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken NamespaceToken;
		public NameSyntax Name;
		public BlockStatementSyntax Body;

		public NamespaceSyntax(AttributeSyntax attribs, SyntaxToken namespaceToken, NameSyntax name, BlockStatementSyntax body)
		{
			Attributes = attribs;
			NamespaceToken = namespaceToken;
			Name = name;
			Body = body;

			int start = Attributes?.FullSpan.Start ?? NamespaceToken.FullSpan.Start;
			int end = Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string prefix = Attributes?.ToString() ?? "";
			return prefix + NamespaceToken.ToString() + ' ' + Name.ToString() + Body.ToString();
		}

		public override string ToFullString()
		{
			string prefix = Attributes?.ToFullString() ?? "";
			return prefix + NamespaceToken.ToFullString() + ' ' + Name.ToFullString() + Body.ToFullString();
		}
	}

	public class ImportSymbolSyntax : SyntaxNode
	{
		public NameSyntax Symbol;
		public SyntaxToken AsToken;
		public IdentifierNameSyntax Alias;
		public SyntaxToken CommaToken;

		public ImportSymbolSyntax(NameSyntax symbol, SyntaxToken asToken, IdentifierNameSyntax alias, SyntaxToken comma)
		{
			Symbol = symbol;
			AsToken = asToken;
			Alias = alias;
			CommaToken = comma;
		}

		public override string ToString()
		{
			string str = Symbol.ToString();
			if (AsToken != null)
				str += ' ' + AsToken.ToString() + ' ' + Alias.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = Symbol.ToFullString();
			if (AsToken != null)
				str += AsToken.ToFullString() + Alias.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class ImportSyntax : StatementSyntax
	{
		public AttributeSyntax Attribs;
		public SyntaxToken ImportToken;
		public NameSyntax ImportedModule;
		public SyntaxToken AsToken;
		public IdentifierNameSyntax ModuleAlias;
		public SyntaxToken ColonToken;
		public List<ImportSymbolSyntax> Symbols;
		public SyntaxToken SemicolonToken;

		public ImportSyntax(AttributeSyntax attribs, SyntaxToken import, NameSyntax importedModule, SyntaxToken asToken, IdentifierNameSyntax moduleAlias, SyntaxToken colon, List<ImportSymbolSyntax> symbols, SyntaxToken semicolon)
		{
			Attribs = attribs;
			ImportToken = import;
			ImportedModule = importedModule;
			AsToken = asToken;
			ModuleAlias = moduleAlias;
			ColonToken = colon;
			Symbols = symbols;
			SemicolonToken = semicolon;

			int start = ImportToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attribs != null)
				str += Attribs.ToString();
			str += ImportToken.ToString() + ' ' + ImportedModule.ToString();
			if (AsToken != null)
				str += ' ' + AsToken.ToString() + ' ' + ModuleAlias.ToString();
			if (ColonToken != null)
			{
				str += ColonToken.ToString();

				foreach (ImportSymbolSyntax symbol in Symbols)
				{
					str += symbol.ToString();
				}
			}
			return str + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attribs != null)
				str += Attribs.ToFullString();
			str += ImportToken.ToFullString() + ' ' + ImportedModule.ToFullString();
			if (AsToken != null)
				str += AsToken.ToFullString() + ModuleAlias.ToFullString();
			if (ColonToken != null)
			{
				str += ColonToken.ToFullString();

				foreach (ImportSymbolSyntax symbol in Symbols)
				{
					str += symbol.ToFullString();
				}
			}
			return str + SemicolonToken.ToFullString();
		}
	}

	public class ReceiverSyntax : StatementSyntax
	{
		public SyntaxToken OpenParenToken;
		public SyntaxToken RefToken;
		public SyntaxToken ConstToken;
		public SyntaxToken SelfToken;
		public SyntaxToken CloseParenToken;

		public ReceiverSyntax(SyntaxToken openParen, SyntaxToken refToken, SyntaxToken constTok, SyntaxToken self, SyntaxToken closeParen)
		{
			OpenParenToken = openParen;
			RefToken = refToken;
			ConstToken = constTok;
			SelfToken = self;
			CloseParenToken = closeParen;

			int start = OpenParenToken.FullSpan.Start;
			int end = OpenParenToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = OpenParenToken.ToString();
			if (RefToken != null)
				str += RefToken.ToString();
			if (ConstToken != null)
				str += ConstToken.ToString() + ' ';
			return str + SelfToken.ToString() + CloseParenToken.ToString();
		}

		public override string ToFullString()
		{
			string str = OpenParenToken.ToFullString();
			if (RefToken != null)
				str += RefToken.ToFullString();
			if (ConstToken != null)
				str += ConstToken.ToFullString();
			return str + SelfToken.ToFullString() + CloseParenToken.ToFullString();
		}
	}

	public class FunctionSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public ReceiverSyntax Receiver;
		public SyntaxToken FuncToken;
		public IdentifierNameSyntax Name;
		public SyntaxToken OpenTemplateToken;
		public List<TemplateParameterSyntax> TemplateParameters;
		public SyntaxToken CloseTemplateToken;
		public SyntaxToken OpenParenToken;
		public List<ParameterSyntax> Parameters;
		public SyntaxToken CloseParenToken;
		public SyntaxToken ArrowToken;
		public TypeSyntax ReturnType;
		public TemplateConstraintSyntax Constraint;
		public BlockStatementSyntax Body;
		public SyntaxToken SemicolonToken;

		public FunctionSyntax(AttributeSyntax attribs, SyntaxToken func, ReceiverSyntax receiver, IdentifierNameSyntax name, SyntaxToken openTemplate, List<TemplateParameterSyntax> templateParameters, SyntaxToken closeTemplate, SyntaxToken openParen, List<ParameterSyntax> parameters, SyntaxToken closeParen, SyntaxToken arrow, TypeSyntax returnType, TemplateConstraintSyntax constraint, BlockStatementSyntax body, SyntaxToken semicolon)
		{
			Attributes = attribs;
			FuncToken = func;
			Receiver = receiver;
			Name = name;
			OpenTemplateToken = openTemplate;
			TemplateParameters = templateParameters;
			CloseTemplateToken = closeTemplate;
			OpenParenToken = openParen;
			Parameters = parameters;
			CloseParenToken = closeParen;
			ArrowToken = arrow;
			ReturnType = returnType;
			Constraint = constraint;
			Body = body;
			SemicolonToken = semicolon;

			int start = Attributes?.FullSpan.Start ?? FuncToken.FullSpan.Start;
			int end = Body?.FullSpan.End ?? SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();

			str += FuncToken.ToString();
			if (Receiver != null)
			{
				str += Receiver.ToString();
			}
			else
			{
				str += ' ';
			}
			str += Name.ToString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToString();
				foreach (TemplateParameterSyntax param in TemplateParameters)
				{
					str += param.ToString();
				}
				str += CloseTemplateToken.ToString();
			}
			str += OpenParenToken.ToString();
			if (Parameters != null)
			{
				foreach (ParameterSyntax param in Parameters)
				{
					str += param.ToString();
				}
			}
			str += CloseParenToken.ToString();

			if (ArrowToken != null)
				str += ArrowToken.ToString() + ReturnType.ToString();

			if (Constraint != null)
				str += Constraint.ToString();
			if (Body != null)
				str += Body.ToString();
			if (SemicolonToken != null)
				str += SemicolonToken.ToString();

			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();

			str += FuncToken.ToFullString();
			if (Receiver != null)
				str += Receiver.ToFullString();
			str += Name.ToFullString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToString();
				foreach (TemplateParameterSyntax param in TemplateParameters)
				{
					str += param.ToString();
				}
				str += CloseTemplateToken.ToString();
			}
			str += OpenParenToken.ToFullString();
			if (Parameters != null)
			{
				foreach (ParameterSyntax param in Parameters)
				{
					str += param.ToFullString();
				}
			}
			str += CloseParenToken.ToFullString();

			if (ArrowToken != null)
				str += ArrowToken.ToFullString() + ReturnType.ToFullString();

			if (Constraint != null)
				str += Constraint.ToFullString();

			if (Body != null)
				str += Body.ToFullString();
			if (SemicolonToken != null)
				str += SemicolonToken.ToFullString();

			return str;
		}
	}

	public class TemplateConstraintSyntax : StatementSyntax
	{
		public SyntaxToken IfToken;
		public SyntaxToken OpenParanToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CloseParanToken;

		public TemplateConstraintSyntax(SyntaxToken ifToken, SyntaxToken openParan, SimpleExpressionSyntax expression, SyntaxToken closeParan)
		{
			IfToken = ifToken;
			OpenParanToken = openParan;
			Expression = expression;
			CloseParanToken = closeParan;

			int start = IfToken.FullSpan.Start;
			int end = CloseParanToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return IfToken.ToString() + OpenParanToken.ToString() + Expression.ToString() + CloseParanToken.ToString();
		}

		public override string ToFullString()
		{
			return IfToken.ToFullString() + OpenParanToken.ToFullString() + Expression.ToFullString() + CloseParanToken.ToFullString();
		}
	}
	
	public class TypeAliasSyntax : StatementSyntax
	{
		public SyntaxToken AliasToken;
		public SyntaxToken OpenTemplateToken;
		public List<TemplateParameterSyntax> TemplateParameters;
		public SyntaxToken CloseTemplateToken;
		public IdentifierNameSyntax Name;
		public SyntaxToken EqualsToken;
		public TypeSyntax Type;
		public SyntaxToken SemicolonToken;

		public TypeAliasSyntax(SyntaxToken aliasToken, SyntaxToken openTemplate, List<TemplateParameterSyntax> templateParameters, SyntaxToken closeTemplate, IdentifierNameSyntax name, SyntaxToken equalsToken, TypeSyntax type, SyntaxToken semicolon)
		{
			AliasToken = aliasToken;
			OpenTemplateToken = openTemplate;
			TemplateParameters = templateParameters;
			CloseTemplateToken = closeTemplate;
			Name = name;
			EqualsToken = equalsToken;
			Type = type;
			SemicolonToken = semicolon;

			int start = AliasToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = AliasToken.ToString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToString();
				foreach (TemplateParameterSyntax param in TemplateParameters)
				{
					str += param.ToString();
				}
				str += CloseTemplateToken.ToString();
			}

			str += ' ' + Name.ToString();
			if (Type != null)
				str += ' ' + EqualsToken.ToString() + ' ' + Type.ToString();
			return str + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			string str = AliasToken.ToFullString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToFullString();
				foreach (TemplateParameterSyntax param in TemplateParameters)
				{
					str += param.ToFullString();
				}
				str += CloseTemplateToken.ToFullString();
			}
			str += Name.ToFullString();
			if (Type != null)
				str += EqualsToken.ToFullString() + Type.ToFullString();
			return str + SemicolonToken.ToFullString();
		}
	}
	
	public class TypedefSyntax : StatementSyntax
	{
		public SyntaxToken TypedefToken;
		public SyntaxToken OpenTemplateToken;
		public List<TemplateParameterSyntax> TemplateParameters;
		public SyntaxToken CloseTemplateToken;
		public IdentifierNameSyntax Name;
		public SyntaxToken EqualsToken;
		public TypeSyntax Type;
		public SyntaxToken SemicolonToken;

		public TypedefSyntax(SyntaxToken typedef, SyntaxToken openTemplate, List<TemplateParameterSyntax> templateParameters, SyntaxToken closeTemplate, IdentifierNameSyntax name, SyntaxToken equalsToken, TypeSyntax type, SyntaxToken semicolon)
		{
			TypedefToken = typedef;
			OpenTemplateToken = openTemplate;
			TemplateParameters = templateParameters;
			CloseTemplateToken = closeTemplate;
			Type = type;
			EqualsToken = equalsToken;
			Name = name;
			SemicolonToken = semicolon;

			int start = TypedefToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = TypedefToken.ToString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToString();
				foreach (TemplateParameterSyntax param in TemplateParameters)
				{
					str += param.ToString();
				}
				str += CloseTemplateToken.ToString();
			}

			str += ' ' + Name.ToString();
			if (Type != null)
				str += ' ' + EqualsToken.ToString() + ' ' + Type.ToString();
			return str + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			string str = TypedefToken.ToFullString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToFullString();
				foreach (TemplateParameterSyntax param in TemplateParameters)
				{
					str += param.ToFullString();
				}
				str += CloseTemplateToken.ToFullString();
			}

			str += Name.ToString();
			if (Type != null)
				str += EqualsToken.ToFullString() + Type.ToFullString();
			return str + SemicolonToken.ToFullString();
		}
	}

	public class SimpleIfSubStatementSyntax : StatementSyntax
	{

	}

	public class IfElseSyntax : SimpleIfSubStatementSyntax
	{
		public IfSyntax If;
		public SimpleIfSubStatementSyntax Else;

		public IfElseSyntax(IfSyntax ifSyntax, SimpleIfSubStatementSyntax elseStatement)
		{
			If = ifSyntax;
			Else = elseStatement;

			int start = ifSyntax?.FullSpan.Start ?? elseStatement.FullSpan.Start;
			int end = Else?.FullSpan.End ?? ifSyntax.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = If.ToString();
			if (Else != null)
				str += Else.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = If.ToFullString();
			if (Else != null)
				str += Else.ToFullString();
			return str;
		}
	}

	public class ElseSyntax : SimpleIfSubStatementSyntax
	{
		public SyntaxToken ElseToken;
		public StatementSyntax Statement;

		public ElseSyntax(SyntaxToken elseToken, StatementSyntax statement)
		{
			ElseToken = elseToken;
			Statement = statement;

			int start = ElseToken.FullSpan.Start;
			int end = Statement.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return ElseToken.ToString() + ' '  + Statement.ToString();
		}

		public override string ToFullString()
		{
			return ElseToken.ToFullString() + Statement.ToFullString();
		}
	}

	public class IfSyntax : SimpleIfSubStatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken LeadingElse;
		public SyntaxToken ElifIfToken;
		public SyntaxToken OpenParenToken;
		public SimpleExpressionSyntax Condition;
		public SyntaxToken CloseParenToken;
		public StatementSyntax Then;

		public IfSyntax(AttributeSyntax attributes, SyntaxToken leadingElse, SyntaxToken elifIfToken, SyntaxToken openParen, SimpleExpressionSyntax condition, SyntaxToken closeParen, StatementSyntax then)
		{
			Attributes = attributes;
			LeadingElse = leadingElse;
			ElifIfToken = elifIfToken;
			OpenParenToken = openParen;
			Condition = condition;
			CloseParenToken = closeParen;
			Then = then;

			int start = Attributes?.FullSpan.Start ?? LeadingElse?.FullSpan.Start ?? ElifIfToken.FullSpan.Start;
			int end = Then.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			if (LeadingElse != null)
				str += LeadingElse.ToString();
			str += ElifIfToken.ToString() + OpenParenToken.ToString() + Condition.ToString() + CloseParenToken.ToString() + Then.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();
			if (LeadingElse != null)
				str += LeadingElse.ToFullString();
			str += ElifIfToken.ToFullString() + OpenParenToken.ToFullString() + Condition.ToFullString() + CloseParenToken.ToFullString() + Then.ToFullString();
			return str;
		}
	}

	public class WhileSyntax : StatementSyntax
	{
		public SyntaxToken WhileToken;
		public SyntaxToken OpenParenToken;
		public SimpleExpressionSyntax Condition;
		public SyntaxToken CloseParenToken;
		public StatementSyntax Body;

		public WhileSyntax(SyntaxToken whileToken, SyntaxToken openParen, SimpleExpressionSyntax condition, SyntaxToken closeParen, StatementSyntax body)
		{
			WhileToken = whileToken;
			OpenParenToken = openParen;
			Condition = condition;
			CloseParenToken = closeParen;
			Body = body;

			int start = WhileToken.FullSpan.Start;
			int end = Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return WhileToken.ToString() + OpenParenToken.ToString() + Condition.ToString() + CloseParenToken.ToString() + Body.ToString();
		}

		public override string ToFullString()
		{
			return WhileToken.ToFullString() + OpenParenToken.ToFullString() + Condition.ToFullString() + CloseParenToken.ToFullString() + Body.ToFullString();
		}
	}

	public class DoWhileSyntax : StatementSyntax
	{
		public SyntaxToken DoToken;
		public StatementSyntax Body;
		public SyntaxToken WhileToken;
		public SyntaxToken OpenParenToken;
		public SimpleExpressionSyntax Condition;
		public SyntaxToken CloseParenToken;
		public SyntaxToken SemicolonToken;

		public DoWhileSyntax(SyntaxToken doToken, StatementSyntax body, SyntaxToken whileToken, SyntaxToken openParen, SimpleExpressionSyntax condition, SyntaxToken closeParen, SyntaxToken semicolon)
		{
			DoToken = doToken;
			Body = body;
			WhileToken = whileToken;
			OpenParenToken = openParen;
			Condition = condition;
			CloseParenToken = closeParen;
			SemicolonToken = semicolon;

			int start = DoToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return DoToken.ToString() + Body.ToString() + WhileToken.ToString() + OpenParenToken.ToString() + Condition.ToString() + CloseParenToken.ToString() + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			return DoToken.ToFullString() + Body.ToFullString() + WhileToken.ToFullString() + OpenParenToken.ToFullString() + Condition.ToFullString() + CloseParenToken.ToFullString() + SemicolonToken.ToFullString();
		}
	}

	public class ForSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken ForToken;
		public SyntaxToken OpenParenToken;
		public SimpleExpressionSyntax Initializer;
		public SyntaxToken SemicolonToken0;
		public SimpleExpressionSyntax Condition;
		public SyntaxToken SemicolonToken1;
		public SimpleExpressionSyntax Increment;
		public SyntaxToken CloseParenToken;
		public StatementSyntax Body;

		public ForSyntax(AttributeSyntax attributes, SyntaxToken forToken, SyntaxToken openParen, SimpleExpressionSyntax initializer, SyntaxToken semicolon0, SimpleExpressionSyntax condition, SyntaxToken semicolon1, SimpleExpressionSyntax increment, SyntaxToken closeParen, StatementSyntax body)
		{
			Attributes = attributes;
			ForToken = forToken;
			OpenParenToken = openParen;
			Initializer = initializer;
			SemicolonToken0 = semicolon0;
			Condition = condition;
			SemicolonToken1 = semicolon1;
			Increment = increment;
			CloseParenToken = closeParen;
			Body = body;

			int start = Attributes?.FullSpan.Start ?? ForToken.FullSpan.Start;
			int end = Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			str += ForToken.ToString() + OpenParenToken.ToString() + Initializer.ToString();
			if (SemicolonToken0 != null)
				str += SemicolonToken0.ToString();
			return str + Condition.ToString() + SemicolonToken1.ToString() + Increment.ToString() + CloseParenToken.ToString() + Body.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();
			str += ForToken.ToFullString() + OpenParenToken.ToFullString() + Initializer.ToFullString();
			if (SemicolonToken0 != null)
				str += SemicolonToken0.ToFullString();
			return str + Condition.ToFullString() + SemicolonToken1.ToFullString() + Increment.ToFullString() + CloseParenToken.ToFullString() + Body.ToFullString();
		}
	}

	public class ForeachSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken ForeachToken;
		public SyntaxToken OpenParenToken;
		public NameSyntax Values;
		public SyntaxToken InToken;
		public SimpleExpressionSyntax Source;
		public SyntaxToken CloseParenToken;
		public StatementSyntax Body;

		public ForeachSyntax(AttributeSyntax attributes, SyntaxToken foreachToken, SyntaxToken openParen, NameSyntax values, SyntaxToken inToken, SimpleExpressionSyntax source, SyntaxToken closeParen, StatementSyntax body)
		{
			Attributes = attributes;
			ForeachToken = foreachToken;
			OpenParenToken = openParen;
			Values = values;
			InToken = inToken;
			Source = source;
			CloseParenToken = closeParen;
			Body = body;

			int start = Attributes?.FullSpan.Start ?? ForeachToken.FullSpan.Start;
			int end = Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			return str + ForeachToken.ToString() + OpenParenToken.ToString() + Values.ToString() + ' ' + InToken.ToString() + ' ' + Source.ToString() + CloseParenToken.ToString() + Body.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			return str + ForeachToken.ToFullString() + OpenParenToken.ToFullString() + Values.ToFullString() + InToken.ToFullString() + Source.ToFullString() + CloseParenToken.ToFullString() + Body.ToFullString();
		}
	}

	public class SwitchCaseSyntax : StatementSyntax
	{
		public SyntaxToken CaseToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken DefaultToken;
		public SyntaxToken ColonToken;
		public List<StatementSyntax> Statements;

		public SwitchCaseSyntax(SyntaxToken caseToken, SimpleExpressionSyntax expression, SyntaxToken defaultToken, SyntaxToken colon, List<StatementSyntax> statements)
		{
			CaseToken = caseToken;
			Expression = expression;
			DefaultToken = defaultToken;
			ColonToken = colon;
			Statements = statements;

			int start = CaseToken?.FullSpan.Start ?? DefaultToken.FullSpan.End;
			int end = Statements?.Last().FullSpan.End ?? ColonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (CaseToken != null)
				str += CaseToken.ToString() + Expression.ToString();
			else
				str += DefaultToken.ToString();
			str += ColonToken.ToString();
			if (Statements != null)
			{
				foreach (StatementSyntax statement in Statements)
				{
					str += statement.ToString();
				}
			}
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (CaseToken != null)
				str += CaseToken.ToFullString() + Expression.ToFullString();
			else
				str += DefaultToken.ToFullString();
			str += ColonToken.ToFullString();
			if (Statements != null)
			{
				foreach (StatementSyntax statement in Statements)
				{
					str += statement.ToFullString();
				}
			}
			return str;
		}
	}

	public class SwitchSyntax : StatementSyntax
	{
		public SyntaxToken SwitchToken;
		public SyntaxToken OpenParenToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CloseParenToken;
		public SyntaxToken OpenBraceToken;
		public List<SwitchCaseSyntax> Cases;
		public SyntaxToken CloseBraceToken;

		public SwitchSyntax(SyntaxToken switchToken, SyntaxToken openParen, SimpleExpressionSyntax expression, SyntaxToken closeParen, SyntaxToken openBrace, List<SwitchCaseSyntax> cases, SyntaxToken closeBrace)
		{
			SwitchToken = switchToken;
			OpenParenToken = openParen;
			Expression = expression;
			CloseParenToken = closeParen;
			OpenBraceToken = openBrace;
			Cases = cases;
			CloseBraceToken = closeBrace;

			int start = SwitchToken.FullSpan.Start;
			int end = CloseBraceToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = SwitchToken.ToString() + OpenParenToken.ToString() + Expression.ToString() + CloseParenToken.ToString() + OpenBraceToken.ToString();
			if (Cases != null)
			{
				foreach (SwitchCaseSyntax switchCase in Cases)
				{
					str += switchCase.ToString();
				}
			}
			return str + CloseBraceToken.ToString();
		}

		public override string ToFullString()
		{
			string str = SwitchToken.ToFullString() + OpenParenToken.ToFullString() + Expression.ToFullString() + CloseParenToken.ToFullString() + OpenBraceToken.ToFullString();
			if (Cases != null)
			{
				foreach (SwitchCaseSyntax switchCase in Cases)
				{
					str += switchCase.ToFullString();
				}
			}
			return str + CloseBraceToken.ToFullString();
		}
	}

	public class FallthroughSyntax : StatementSyntax
	{
		public SyntaxToken FallthroughToken;
		public SyntaxToken SemicolonToken;

		public FallthroughSyntax(SyntaxToken fallthrough, SyntaxToken semicolon)
		{
			FallthroughToken = fallthrough;
			SemicolonToken = semicolon;

			int start = FallthroughToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return FallthroughToken.ToString() + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			return FallthroughToken.ToFullString() + SemicolonToken.ToFullString();
		}
	}

	public class ContinueSyntax : StatementSyntax
	{
		public SyntaxToken ContinueToken;
		public SyntaxToken SemicolonToken;

		public ContinueSyntax(SyntaxToken continueToken, SyntaxToken semicolon)
		{
			ContinueToken = continueToken;
			SemicolonToken = semicolon;

			int start = ContinueToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return ContinueToken.ToString() + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			return ContinueToken.ToFullString() + SemicolonToken.ToFullString();
		}
	}

	public class BreakSyntax : StatementSyntax
	{
		public SyntaxToken BreakToken;
		public SyntaxToken SemicolonToken;

		public BreakSyntax(SyntaxToken breakToken, SyntaxToken semicolon)
		{
			BreakToken = breakToken;
			SemicolonToken = semicolon;

			int start = BreakToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return BreakToken.ToString() + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			return BreakToken.ToFullString() + SemicolonToken.ToFullString();
		}
	}

	public class ReturnSyntax : StatementSyntax
	{
		public SyntaxToken ReturnToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken SemicolonToken;

		public ReturnSyntax(SyntaxToken returnToken, SimpleExpressionSyntax expression, SyntaxToken semicolon)
		{
			ReturnToken = returnToken;
			Expression = expression;
			SemicolonToken = semicolon;

			int start = ReturnToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = ReturnToken.ToString();
			if (!(Expression is EmptyExpressionSyntax))
				str += ' ' + Expression.ToString();
			return str + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			string str = ReturnToken.ToFullString();
			if (!(Expression is EmptyExpressionSyntax))
				str += ' ' + Expression.ToFullString();
			return str + SemicolonToken.ToFullString();
		}
	}

	public class DeferSyntax : StatementSyntax
	{
		public SyntaxToken DeferToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken SemicolonToken;

		public DeferSyntax(SyntaxToken deferToken, SimpleExpressionSyntax expression, SyntaxToken semicolon)
		{
			DeferToken = deferToken;
			Expression = expression;
			SemicolonToken = semicolon;

			int start = DeferToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return DeferToken.ToString() + ' ' + Expression?.ToString() + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			return DeferToken.ToFullString() + Expression?.ToFullString() + SemicolonToken.ToFullString();
		}
	}

	public class GotoSyntax : StatementSyntax
	{
		public SyntaxToken GotoToken;
		public SyntaxToken DefaultToken;
		public SyntaxToken CaseToken;
		public IdentifierNameSyntax Identifier;
		public SyntaxToken SemicolonToken;

		public GotoSyntax(SyntaxToken gotoToken, SyntaxToken defaultToken, SyntaxToken caseToken, IdentifierNameSyntax identifier, SyntaxToken semicolon)
		{
			GotoToken = gotoToken;
			DefaultToken = defaultToken;
			CaseToken = caseToken;
			Identifier = identifier;
			SemicolonToken = semicolon;

			int start = GotoToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = GotoToken.ToString() + ' ';
			if (DefaultToken != null)
				str += DefaultToken.ToString();
			if (CaseToken != null)
				str += CaseToken.ToString() + ' ';
			if (Identifier != null)
				str += Identifier.ToString();
			return str + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			string str = GotoToken.ToFullString();
			if (DefaultToken != null)
				str += DefaultToken.ToFullString();
			if (CaseToken != null)
				str += CaseToken.ToFullString();
			if (Identifier != null)
				str += Identifier.ToFullString();
			return str + SemicolonToken.ToFullString();
		}
	}

	public class LabelSyntax : StatementSyntax
	{
		public SyntaxToken LeftColonToken;
		public IdentifierNameSyntax Name;
		public SyntaxToken RightColonToken;

		public LabelSyntax(SyntaxToken leftColon, IdentifierNameSyntax name, SyntaxToken rightColon)
		{
			LeftColonToken = leftColon;
			Name = name;
			RightColonToken = rightColon;

			int start = LeftColonToken.FullSpan.Start;
			int end = RightColonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return LeftColonToken.ToString() + Name.ToString() + RightColonToken.ToString();
		}

		public override string ToFullString()
		{
			return LeftColonToken.ToFullString() + Name.ToFullString() + RightColonToken.ToFullString();
		}
	}

	public class ImplInterfaceFieldDefSyntax : StatementSyntax
	{
		public IdentifierNameSyntax InterfaceField;
		public SyntaxToken Colon;
		public NameSyntax AggregateField;
		public SyntaxToken Comma;

		public ImplInterfaceFieldDefSyntax(IdentifierNameSyntax interfaceField, SyntaxToken colon, NameSyntax aggregateField, SyntaxToken comma)
		{
			InterfaceField = interfaceField;
			Colon = colon;
			AggregateField = aggregateField;
			Comma = comma;

			int start = InterfaceField.FullSpan.Start;
			int end = Comma?.FullSpan.End ?? AggregateField.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = InterfaceField.ToString() + Colon.ToString() + AggregateField.ToString();
			if (Comma != null)
				str += Comma.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = InterfaceField.ToFullString() + Colon.ToFullString() + AggregateField.ToFullString();
			if (Comma != null)
				str += Comma.ToFullString();
			return str;
		}
	}

	public class ImplInterfaceSyntax : StatementSyntax
	{
		public NameSyntax Name;
		public SyntaxToken OpenParen;
		public List<ImplInterfaceFieldDefSyntax> FieldDefs;
		public SyntaxToken CloseParen;
		public SyntaxToken Comma;

		public ImplInterfaceSyntax(NameSyntax name, SyntaxToken openParen, List<ImplInterfaceFieldDefSyntax> fieldDefs, SyntaxToken closeParen, SyntaxToken comma)
		{
			Name = name;
			OpenParen = openParen;
			FieldDefs = fieldDefs;
			CloseParen = closeParen;
			Comma = comma;

			int start = Name.FullSpan.Start;
			int end = Comma?.FullSpan.End ?? CloseParen?.FullSpan.End ?? Name.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = Name.ToString();
			if (OpenParen != null)
			{
				str += OpenParen.ToString();
				foreach (ImplInterfaceFieldDefSyntax fieldDef in FieldDefs)
				{
					str += fieldDef.ToString();
				}
				str += CloseParen.ToString();
			}
			if (Comma != null)
				str += Comma.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = Name.ToFullString();
			if (OpenParen != null)
			{
				str += OpenParen.ToFullString();
				foreach (ImplInterfaceFieldDefSyntax fieldDef in FieldDefs)
				{
					str += fieldDef.ToFullString();
				}
				str += CloseParen.ToFullString();
			}
			if (Comma != null)
				str += Comma.ToFullString();
			return str;
		}
	}

	public class ImplSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken ImplToken;
		public SyntaxToken OpenTemplateToken;
		public List<TemplateParameterSyntax> TemplateParameters;
		public SyntaxToken CloseTemplateToken;
		public TypeSyntax Type;
		public SyntaxToken ColonToken;
		public List<ImplInterfaceSyntax> BaseList;
		public TemplateConstraintSyntax Constraint;
		public BlockStatementSyntax Body;

		public ImplSyntax(AttributeSyntax attribs, SyntaxToken implToken, SyntaxToken openTemplate, List<TemplateParameterSyntax> templateParameters, SyntaxToken closeTemplate, TypeSyntax type, SyntaxToken colon, List<ImplInterfaceSyntax> baseList, TemplateConstraintSyntax contraint, BlockStatementSyntax body)
		{
			Attributes = attribs;
			ImplToken = implToken;
			OpenTemplateToken = openTemplate;
			TemplateParameters = templateParameters;
			CloseTemplateToken = closeTemplate;
			Type = type;
			ColonToken = colon;
			BaseList = baseList;
			Constraint = contraint;
			Body = body;

			int start = Attributes?.FullSpan.Start ?? ImplToken.FullSpan.Start;
			int end = Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			str += ImplToken.ToString();
			if (OpenTemplateToken != null)
			{ 
				str += OpenTemplateToken.ToString();
				foreach (TemplateParameterSyntax parameter in TemplateParameters)
				{
					str += parameter.ToString();
				}
				str += CloseTemplateToken.ToString();
			}
			str += ' ' + Type.ToString();
			if (ColonToken != null)
			{
				str += ColonToken.ToString();
				foreach (ImplInterfaceSyntax implInterface in BaseList)
				{
					str += implInterface.ToString();
				}
			}
			if (Constraint != null)
				str += Constraint.ToString();
			return str + Body.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();
			str += ImplToken.ToFullString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToFullString();
				foreach (TemplateParameterSyntax parameter in TemplateParameters)
				{
					str += parameter.ToFullString();
				}
				str += CloseTemplateToken.ToFullString();
			}
			str += Type.ToFullString();
			if (ColonToken != null)
			{
				str += ColonToken.ToFullString();
				foreach (ImplInterfaceSyntax implInterface in BaseList)
				{
					str += implInterface.ToFullString();
				}
			}
			if (Constraint != null)
				str += Constraint.ToFullString();
			return str + Body.ToFullString();
		}
	}

	public class AggregateBodySyntax : StatementSyntax
	{
		public SyntaxToken OpenBodyToken;
		public List<DeclarationSyntax> Declarations;
		public SyntaxToken CloseBodyToken;

		public AggregateBodySyntax(SyntaxToken openBodyToken, List<DeclarationSyntax> declarations, SyntaxToken closeBodyToken)
		{
			OpenBodyToken = openBodyToken;
			Declarations = declarations;
			CloseBodyToken = closeBodyToken;

			int start = OpenBodyToken.FullSpan.Start;
			int end = CloseBodyToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = OpenBodyToken.ToString();
			if (Declarations != null)
			{
				foreach (DeclarationSyntax decl in Declarations)
				{
					str += decl.ToString();
				}
			}
			return str + CloseBodyToken.ToString();
		}

		public override string ToFullString()
		{
			string str = OpenBodyToken.ToFullString();
			if (Declarations != null)
			{
				foreach (DeclarationSyntax decl in Declarations)
				{
					str += decl.ToFullString();
				}
			}
			return str + CloseBodyToken.ToFullString();
		}
	}

	public class StructSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken StructToken;
		public IdentifierNameSyntax Name;
		public SyntaxToken OpenTemplateToken;
		public List<TemplateParameterSyntax> TemplateParameters;
		public SyntaxToken CloseTemplateToken;
		public SyntaxToken ColonToken;
		public NameSyntax BaseList;
		public AggregateBodySyntax Body;

		public StructSyntax(AttributeSyntax attribs, SyntaxToken structToken, IdentifierNameSyntax name, SyntaxToken openTemplate, List<TemplateParameterSyntax> templateParameters, SyntaxToken closeTemplate, SyntaxToken colon, NameSyntax baseList, AggregateBodySyntax body)
		{
			Attributes = attribs;
			StructToken = structToken;
			Name = name;
			OpenTemplateToken = openTemplate;
			TemplateParameters = templateParameters;
			CloseTemplateToken = closeTemplate;
			ColonToken = colon;
			BaseList = baseList;
			Body = body;

			int start = Attributes?.FullSpan.Start ?? StructToken.FullSpan.Start;
			int end = Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			str += StructToken.ToString();
			if (Name != null)
				str += ' ' + Name.ToString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToString();
				foreach (TemplateParameterSyntax parameter in TemplateParameters)
				{
					str += parameter.ToString();
				}
				str += CloseTemplateToken.ToString();
			}
			if (ColonToken != null)
				str += ColonToken.ToString() + BaseList.ToString();
			return str + Body.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();
			str += StructToken.ToFullString();
			if (Name != null)
				str += Name.ToFullString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToFullString();
				foreach (TemplateParameterSyntax parameter in TemplateParameters)
				{
					str += parameter.ToFullString();
				}
				str += CloseTemplateToken.ToFullString();
			}
			if (ColonToken != null)
				str += ColonToken.ToFullString() + BaseList.ToFullString();
			return str + Body.ToFullString();
		}
	}

	public class InterfaceSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken InterfaceToken;
		public IdentifierNameSyntax Name;
		public SyntaxToken OpenTemplateToken;
		public List<TemplateParameterSyntax> TemplateParameters;
		public SyntaxToken CloseTemplateToken;
		public SyntaxToken ColonToken;
		public NameSyntax BaseList;
		public BlockStatementSyntax Body;

		public InterfaceSyntax(AttributeSyntax attribs, SyntaxToken interfaceToken, IdentifierNameSyntax name, SyntaxToken openTemplate, List<TemplateParameterSyntax> templateParameters, SyntaxToken closeTemplate, SyntaxToken colon, NameSyntax baseList, BlockStatementSyntax body)
		{
			Attributes = attribs;
			InterfaceToken = interfaceToken;
			Name = name;
			OpenTemplateToken = openTemplate;
			TemplateParameters = templateParameters;
			CloseTemplateToken = closeTemplate;
			ColonToken = colon;
			BaseList = baseList;
			Body = body;

			int start = Attributes?.FullSpan.Start ?? InterfaceToken.FullSpan.Start;
			int end = Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			str += InterfaceToken.ToString() + ' ' + Name.ToString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToString();
				foreach (TemplateParameterSyntax parameter in TemplateParameters)
				{
					str += parameter.ToString();
				}
				str += CloseTemplateToken.ToString();
			}
			if (ColonToken != null)
				str += ColonToken.ToString() + BaseList.ToString();
			return str + Body.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();
			str += InterfaceToken.ToFullString() + Name.ToFullString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToFullString();
				foreach (TemplateParameterSyntax parameter in TemplateParameters)
				{
					str += parameter.ToFullString();
				}
				str += CloseTemplateToken.ToFullString();
			}  
			if (ColonToken != null)
				str += ColonToken.ToFullString() + BaseList.ToFullString();
			return str + Body.ToFullString();
		}
	}

	public class UnionSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken UnionToken;
		public IdentifierNameSyntax Name;
		public SyntaxToken OpenTemplateToken;
		public List<TemplateParameterSyntax> TemplateParameters;
		public SyntaxToken CloseTemplateToken;
		public AggregateBodySyntax Body;

		public UnionSyntax(AttributeSyntax attribs, SyntaxToken unionToken, IdentifierNameSyntax name, SyntaxToken openTemplate, List<TemplateParameterSyntax> templateParameters, SyntaxToken closeTemplate, AggregateBodySyntax body)
		{
			Attributes = attribs;
			UnionToken = unionToken;
			Name = name;
			OpenTemplateToken = openTemplate;
			TemplateParameters = templateParameters;
			CloseTemplateToken = closeTemplate;
			Body = body;

			int start = Attributes?.FullSpan.Start ?? UnionToken.FullSpan.Start;
			int end = Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			str += UnionToken.ToString();
			if (Name != null)
				str += ' ' + Name.ToString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToString();
				foreach (TemplateParameterSyntax parameter in TemplateParameters)
				{
					str += parameter.ToString();
				}
				str += CloseTemplateToken.ToString();
			}
			return str + Body.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();
			str += UnionToken.ToFullString();
			if (Name != null)
				str += Name.ToFullString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToFullString();
				foreach (TemplateParameterSyntax parameter in TemplateParameters)
				{
					str += parameter.ToFullString();
				}
				str += CloseTemplateToken.ToFullString();
			}
			return str + Body.ToFullString();
		}
	}

	public class EnumMemberSyntax : StatementSyntax
	{
		public IdentifierNameSyntax Name;
		public TupleTypeSyntax TupleType;
		public AggregateBodySyntax AggrBody;
		public SyntaxToken EqualsToken;
		public SimpleExpressionSyntax DefaultValue;
		public SyntaxToken CommaToken;

		public EnumMemberSyntax(IdentifierNameSyntax name, AggregateBodySyntax aggrBody, TupleTypeSyntax tupleType, SyntaxToken equals, SimpleExpressionSyntax defaultValue, SyntaxToken comma)
		{
			Name = name;
			AggrBody = aggrBody;
			TupleType = tupleType;
			EqualsToken = equals;
			DefaultValue = defaultValue;
			CommaToken = comma;

			int start = Name.FullSpan.Start;
			int end = CommaToken?.FullSpan.End ?? DefaultValue?.FullSpan.End ?? TupleType?.FullSpan.End ?? AggrBody?.FullSpan.Start ?? Name.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = Name.ToString();
			if (TupleType != null)
				str += TupleType.ToString();
			if (EqualsToken != null)
				str += EqualsToken.ToString() + DefaultValue.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = Name.ToFullString();
			if (TupleType != null)
				str += TupleType.ToFullString();
			if (EqualsToken != null)
				str += EqualsToken.ToFullString() + DefaultValue.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class EnumSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken EnumToken;
		public IdentifierNameSyntax Name;
		public SyntaxToken OpenTemplateToken;
		public List<TemplateParameterSyntax> TemplateParameters;
		public SyntaxToken CloseTemplateToken;
		public SyntaxToken ColonToken;
		public TypeSyntax Type;
		public SyntaxToken OpenBraceToken;
		public List<EnumMemberSyntax> Members;
		public SyntaxToken CloseBraceToken;

		public EnumSyntax(AttributeSyntax attibs, SyntaxToken enumToken, IdentifierNameSyntax name, SyntaxToken openTemplate, List<TemplateParameterSyntax> templateParameters, SyntaxToken closeTemplate, SyntaxToken colon, TypeSyntax type, SyntaxToken openBrace, List<EnumMemberSyntax> members, SyntaxToken closeBrace)
		{
			Attributes = attibs;
			EnumToken = enumToken;
			Name = name;
			OpenTemplateToken = openTemplate;
			TemplateParameters = templateParameters;
			CloseTemplateToken = closeTemplate;
			ColonToken = colon;
			Type = type;
			OpenBraceToken = openBrace;
			Members = members;
			CloseBraceToken = closeBrace;

			int start = Attributes?.FullSpan.Start ?? EnumToken.FullSpan.Start;
			int end = CloseBraceToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += ToString();
			str += EnumToken.ToString();
			if (Name != null)
				str += Name.ToString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToString();
				foreach (TemplateParameterSyntax parameter in TemplateParameters)
				{
					str += parameter.ToString();
				}
				str += CloseTemplateToken.ToString();
			}
			if (ColonToken != null)
				str += ColonToken.ToString() + Type.ToString();
			str += OpenBraceToken.ToString();
			if (Members != null)
			{
				foreach (EnumMemberSyntax member in Members)
				{
					str += member.ToString();
				}
			}
			return str + CloseBraceToken.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += ToFullString();
			str += EnumToken.ToFullString();
			if (Name != null)
				str += Name.ToFullString();
			if (OpenTemplateToken != null)
			{
				str += OpenTemplateToken.ToFullString();
				foreach (TemplateParameterSyntax parameter in TemplateParameters)
				{
					str += parameter.ToFullString();
				}
				str += CloseTemplateToken.ToFullString();
			}
			if (ColonToken != null)
				str += ColonToken.ToFullString() + Type.ToFullString();
			str += OpenBraceToken.ToFullString();
			if (Members != null)
			{
				foreach (EnumMemberSyntax member in Members)
				{
					str += member.ToFullString();
				}
			}
			return str + CloseBraceToken.ToFullString();
		}
	}

	public class UnitTestSyntax : StatementSyntax
	{
		public SyntaxToken UnitTestToken;
		public IdentifierNameSyntax Name;
		public BlockStatementSyntax Body;

		public UnitTestSyntax(SyntaxToken unitTestToken, IdentifierNameSyntax name, BlockStatementSyntax body)
		{
			UnitTestToken = unitTestToken;
			Name = name;
			Body = body;

			int start = UnitTestToken.FullSpan.Start;
			int end = Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			str += UnitTestToken.ToString();
			if (Name != null)
				str += ' ' + Name.ToString();
			return str + Body.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			str += UnitTestToken.ToFullString();
			if (Name != null)
				str += ' ' + Name.ToFullString();
			return str + Body.ToFullString();
		}
	}

	public class CompileConditionalSyntax : StatementSyntax
	{
		public SyntaxToken ConditionalToken;
		public SyntaxToken OpenParenToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CloseParenToken;
		public BlockStatementSyntax Body;
		public SyntaxToken ElseToken;
		public StatementSyntax ElseBody;

		public CompileConditionalSyntax(SyntaxToken conditional, SyntaxToken openParen, SimpleExpressionSyntax expression, SyntaxToken closeParen, BlockStatementSyntax body, SyntaxToken elseToken, StatementSyntax elseBody)
		{
			ConditionalToken = conditional;
			OpenParenToken = openParen;
			Expression = expression;
			CloseParenToken = closeParen;
			Body = body;
			ElseToken = elseToken;
			ElseBody = elseBody;

			int start = ConditionalToken.FullSpan.Start;
			int end = ElseBody?.FullSpan.End ?? Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			if (!Context.AllowCompilation)
				return "";
			string str = ConditionalToken.ToString() + OpenParenToken.ToString() + Expression.ToString() + CloseParenToken.ToString() + Body.ToString();
			if (ElseToken != null)
				str += ElseToken.ToString() + ' ' + ElseBody.ToString();
			return str;
		}

		public override string ToFullString()
		{
			if (!Context.AllowCompilation)
				return "";
			string str = ConditionalToken.ToFullString() + OpenParenToken.ToFullString() + Expression.ToFullString() + CloseParenToken.ToFullString() + Body.ToFullString();
			if (ElseToken != null)
				str += ElseToken.ToFullString() + ElseBody.ToFullString();
			return str;
		}
	}

	public class AssertSyntax : StatementSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken AssertToken;
		public SyntaxToken OpenParenToken;
		public List<ArgumentSyntax> Arguments;
		public SyntaxToken CloseParenToken;
		public SyntaxToken SemicolonToken;

		public AssertSyntax(AttributeSyntax attribs, SyntaxToken assert, SyntaxToken openParen, List<ArgumentSyntax> arguments, SyntaxToken closeParen, SyntaxToken semicolon)
		{
			Attributes = attribs;
			AssertToken = assert;
			OpenParenToken = openParen;
			Arguments = arguments;
			CloseParenToken = closeParen;
			SemicolonToken = semicolon;

			int start = Attributes?.FullSpan.Start ?? AssertToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			str += AssertToken.ToString() + OpenParenToken.ToString();
			if (Arguments != null)
			{
				foreach (ArgumentSyntax argument in Arguments)
				{
					str += argument.ToString();
				}
			}
			return str + CloseParenToken.ToString() + SemicolonToken.ToString();
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();
			str += AssertToken.ToFullString() + OpenParenToken.ToFullString();
			if (Arguments != null)
			{
				foreach (ArgumentSyntax argument in Arguments)
				{
					str += argument.ToFullString();
				}
			}
			return str + CloseParenToken.ToFullString() + SemicolonToken.ToFullString();
		}
	}

	#endregion

	#region Expression

	public enum ExpressionPrecedence
	{
		None,
		Assign,
		Conditional,
		LogicOr,
		LogicAnd,
		BinOr,
		BinXor,
		BinAnd,
		Compare,
		Shift,
		AddSub,
		MulDiv,
		Prefix,
		Postfix,
		Primary
	}

	public class SimpleExpressionSyntax : StatementSyntax
	{
		public ExpressionPrecedence Precedence = ExpressionPrecedence.None;
	}

	public class ExpressionSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Expression;
		public SyntaxToken SemicolonToken;

		public ExpressionSyntax(SimpleExpressionSyntax expression, SyntaxToken semicolon)
		{
			Expression = expression;
			SemicolonToken = semicolon;

			int start = Expression?.FullSpan.Start ?? SemicolonToken.FullSpan.Start;
			int end = SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Expression != null)
				str += Expression.ToString();
			str += SemicolonToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Expression != null)
				str += Expression.ToFullString();
			str += SemicolonToken.ToFullString();
			return str;
		}
	}

	public class CommaExpressionElemSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CommaToken;

		public CommaExpressionElemSyntax(SimpleExpressionSyntax expression, SyntaxToken comma)
		{
			Expression = expression;
			CommaToken = comma;

			int start = Expression.FullSpan.Start;
			int end = CommaToken?.FullSpan.End ?? Expression.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = Expression.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = Expression.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class CommaExpressionSyntax : SimpleExpressionSyntax
	{
		public List<CommaExpressionElemSyntax> Expressions;

		public CommaExpressionSyntax(List<CommaExpressionElemSyntax> expressions)
		{
			Expressions = expressions;

			int start = Expressions[0].FullSpan.Start;
			int end = Expressions.Last().FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			foreach (var expr in Expressions)
			{
				str += expr.ToString();
			}
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			foreach (var expr in Expressions)
			{
				str += expr.ToFullString();
			}
			return str;
		}
	}

	public class PropertyGetSetSyntax : SimpleExpressionSyntax
	{
		public AttributeSyntax Attributes;
		public SyntaxToken Identifier;
		public BlockStatementSyntax Body;
		public SyntaxToken SemicolonToken;

		public PropertyGetSetSyntax(AttributeSyntax attribs, SyntaxToken identifier, BlockStatementSyntax body, SyntaxToken semicolon)
		{
			Attributes = attribs;
			Identifier = identifier;
			Body = body;
			SemicolonToken = semicolon;

			int start = Attributes?.FullSpan.Start ?? Identifier.FullSpan.Start;
			int end = Body?.FullSpan.End ?? SemicolonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			str += Identifier.ToString();
			if (Body != null)
				str += Body.ToString();
			if (SemicolonToken != null)
				str += SemicolonToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();
			str += Identifier.ToFullString();
			if (Body != null)
				str += Body.ToFullString();
			if (SemicolonToken != null)
				str += SemicolonToken.ToFullString();
			return str;
		}
	}

	public class DeclarationSyntax : SimpleExpressionSyntax
	{
		public AttributeSyntax Attributes;
		public NameSyntax Names;
		public SyntaxToken TypeSpecificationToken;
		public TypeSyntax Type;
		public SyntaxToken OpenBraceToken;
		public List<PropertyGetSetSyntax> GetSet;
		public SyntaxToken CloseBraceToken;
		public SyntaxToken AssignmentToken;
		public SimpleExpressionSyntax Initializer;
		public SyntaxToken SemicolonToken;
		public SyntaxToken CommaToken;

		public DeclarationSyntax(AttributeSyntax attribs, NameSyntax names, SyntaxToken typeSpecification, TypeSyntax type, SyntaxToken openBrace, List<PropertyGetSetSyntax> getset, SyntaxToken closeBrace, SyntaxToken assignment, SimpleExpressionSyntax initializer, SyntaxToken semicolon, SyntaxToken comma)
		{
			Attributes = attribs;
			Names = names;
			TypeSpecificationToken = typeSpecification;
			Type = type;
			OpenBraceToken = openBrace;
			GetSet = getset;
			CloseBraceToken = closeBrace;
			AssignmentToken = assignment;
			Initializer = initializer;
			SemicolonToken = semicolon;
			CommaToken = comma;

			int start = Attributes?.FullSpan.Start ?? Names.FullSpan.Start;
			int end = SemicolonToken?.FullSpan.End ?? CommaToken?.FullSpan.End ?? CloseBraceToken?.FullSpan.End ?? Type.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToString();
			str += Names.ToString();
			if (TypeSpecificationToken != null)
			{
				str += TypeSpecificationToken.ToString() + Type.ToString();
			}
			if (OpenBraceToken != null)
			{
				str += OpenBraceToken.ToString();
				foreach (PropertyGetSetSyntax getset in GetSet)
				{
					str += getset.ToString();
				}
				str += CloseBraceToken.ToString();
			}
			if (AssignmentToken != null)
				str += AssignmentToken.ToString() + Initializer.ToString();
			if (SemicolonToken != null)
				str += SemicolonToken.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Attributes != null)
				str += Attributes.ToFullString();
			str += Names.ToFullString();
			if (TypeSpecificationToken != null)
			{
				str += TypeSpecificationToken.ToFullString() + Type.ToFullString();
			}
			if (OpenBraceToken != null)
			{
				str += OpenBraceToken.ToFullString();
				foreach (PropertyGetSetSyntax getset in GetSet)
				{
					str += getset.ToFullString();
				}
				str += CloseBraceToken.ToFullString();
			}
			if (AssignmentToken != null)
				str += AssignmentToken.ToFullString() + Initializer.ToFullString();
			if (SemicolonToken != null)
				str += SemicolonToken.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class TernaryExpressionSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Condition;
		public SyntaxToken QuestionToken;
		public SimpleExpressionSyntax Then;
		public SyntaxToken ColonToken;
		public SimpleExpressionSyntax Else;

		public TernaryExpressionSyntax(SimpleExpressionSyntax condition, SyntaxToken question, SimpleExpressionSyntax then, SyntaxToken colon, SimpleExpressionSyntax elseExpr)
		{
			Condition = condition;
			QuestionToken = question;
			Then = then;
			ColonToken = colon;
			Else = elseExpr;
			Precedence = ExpressionPrecedence.Conditional;

			int start = ColonToken.FullSpan.Start;
			int end = ColonToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return Condition.ToString() + QuestionToken.ToString() + Then.ToString() + ColonToken.ToString() + Else.ToString();
		}

		public override string ToFullString()
		{
			return Condition.ToFullString() + QuestionToken.ToFullString() + Then.ToFullString() + ColonToken.ToFullString() + Else.ToFullString();
		}
	}

	public class BinaryExpressionSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Left;
		public SyntaxToken OperatorToken;
		public SimpleExpressionSyntax Right;

		public BinaryExpressionSyntax(SimpleExpressionSyntax left, SyntaxToken operatorToken, SimpleExpressionSyntax right)
		{
			Left = left;
			OperatorToken = operatorToken;
			Right = right;
			Precedence = SyntaxNodeHelpers.GetBinaryPrecedence(OperatorToken);

			int start = Left.FullSpan.Start;
			int end = Right.FullSpan.Start;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return Left.ToString() + OperatorToken.ToString() + Right.ToString();
		}

		public override string ToFullString()
		{
			return Left.ToFullString() + OperatorToken.ToFullString() + Right.ToFullString();
		}
	}

	public class AssignExpressionSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Left;
		public SyntaxToken AssignToken;
		public SimpleExpressionSyntax Right;

		public AssignExpressionSyntax(SimpleExpressionSyntax left, SyntaxToken assignToken, SimpleExpressionSyntax right)
		{
			Left = left;
			AssignToken = assignToken;
			Right = right;
			Precedence = ExpressionPrecedence.Assign;

			int start = Left.FullSpan.Start;
			int end = Right.FullSpan.Start;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return Left.ToString() + AssignToken.ToString() + Right.ToString();
		}

		public override string ToFullString()
		{
			return Left.ToFullString() + AssignToken.ToFullString() + Right.ToFullString();
		}
	}

	public class PrefixExpressionSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken OperatorToken;
		public SimpleExpressionSyntax Right;

		public PrefixExpressionSyntax(SyntaxToken operatorToken, SimpleExpressionSyntax right)
		{
			OperatorToken = operatorToken;
			Right = right;
			Precedence = ExpressionPrecedence.Prefix;

			int start = OperatorToken.FullSpan.Start;
			int end = Right.FullSpan.Start;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return OperatorToken.ToString() + Right.ToString();
		}

		public override string ToFullString()
		{
			return OperatorToken.ToFullString() + Right.ToFullString();
		}
	}

	public class PostfixExpressionSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Left;
		public SyntaxToken OperatorToken;

		public PostfixExpressionSyntax(SimpleExpressionSyntax left, SyntaxToken operatorToken)
		{
			Left = left;
			OperatorToken = operatorToken;
			Precedence = ExpressionPrecedence.Postfix;

			int start = Left.FullSpan.Start;
			int end = operatorToken.FullSpan.Start;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return Left.ToString() + OperatorToken.ToString();
		}

		public override string ToFullString()
		{
			return Left.ToFullString() + OperatorToken.ToFullString();
		}
	}

	public class BracketedExpressionSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken OpenParen;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CloseParen;

		public BracketedExpressionSyntax(SyntaxToken openParen, SimpleExpressionSyntax expression, SyntaxToken closeParen)
		{
			OpenParen = openParen;
			Expression = expression;
			CloseParen = closeParen;

			int start = OpenParen.FullSpan.Start;
			int end = OpenParen.FullSpan.Start;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return OpenParen.ToString() + Expression.ToString() + CloseParen.ToString();
		}

		public override string ToFullString()
		{
			return OpenParen.ToFullString() + Expression.ToFullString() + CloseParen.ToFullString();
		}
	}

	public class CastExpressionSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Expression;
		public SyntaxToken AsToken;
		public TypeSyntax Type;

		public CastExpressionSyntax(SimpleExpressionSyntax expression, SyntaxToken asToken, TypeSyntax type)
		{
			Expression = expression;
			AsToken = asToken;
			Type = type;

			int start = Expression.FullSpan.Start;
			int end = Type.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return Expression.ToString() + ' ' + AsToken.ToString() + ' ' + Type.ToString();
		}

		public override string ToFullString()
		{
			return Expression.ToFullString() + AsToken.ToFullString() + Type.ToFullString();
		}
	}

	public class TransmuteExpressionSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Expression;
		public SyntaxToken TransmuteToken;
		public TypeSyntax Type;

		public TransmuteExpressionSyntax(SimpleExpressionSyntax expression, SyntaxToken transmute, TypeSyntax type)
		{
			Expression = expression;
			TransmuteToken = transmute;
			Type = type;

			int start = Expression.FullSpan.Start;
			int end = Type.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return Expression.ToString() + ' ' + TransmuteToken.ToString() + ' ' + Type.ToString();
		}

		public override string ToFullString()
		{
			return Expression.ToFullString() + TransmuteToken.ToFullString() + Type.ToFullString();
		}
	}

	public class CCastExpressionSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CCastToken;
		public TypeSyntax Type;

		public CCastExpressionSyntax(SimpleExpressionSyntax expression, SyntaxToken cCastToken, TypeSyntax type)
		{
			Expression = expression;
			CCastToken = cCastToken;
			Type = type;

			int start = Expression.FullSpan.Start;
			int end = Type.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return Expression.ToString() + ' ' + CCastToken.ToString() + ' ' + Type.ToString();
		}

		public override string ToFullString()
		{
			return Expression.ToFullString() + CCastToken.ToFullString() + Type.ToFullString();
		}
	}

	public class IndexSliceExpressionSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Expression;
		public SyntaxToken OpenBracketToken;
		public SimpleExpressionSyntax IndexSliceExpression;
		public SyntaxToken CloseBracketToken;

		public IndexSliceExpressionSyntax(SimpleExpressionSyntax expression, SyntaxToken openBracket, SimpleExpressionSyntax indexSliceExpression, SyntaxToken closeBracket)
		{
			Expression = expression;
			OpenBracketToken = openBracket;
			IndexSliceExpression = indexSliceExpression;
			CloseBracketToken = closeBracket;

			int start = Expression.FullSpan.Start;
			int end = CloseBracketToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return Expression.ToString() + OpenBracketToken.ToString() + IndexSliceExpression.ToString() + CloseBracketToken.ToString();
		}

		public override string ToFullString()
		{
			return Expression.ToFullString() + OpenBracketToken.ToFullString() + IndexSliceExpression.ToFullString() + CloseBracketToken.ToFullString();
		}
	}

	public class FunctionCallExpressionSyntax : SimpleExpressionSyntax
	{
		public QualifiedNameSyntax QualifiedName;
		public SyntaxToken OpenParenToken;
		public List<ArgumentSyntax> Arguments;
		public SyntaxToken CloseParenToken;

		public FunctionCallExpressionSyntax(QualifiedNameSyntax name, SyntaxToken openParen, List<ArgumentSyntax> arguments, SyntaxToken closeParen)
		{
			QualifiedName = name;
			OpenParenToken = openParen;
			Arguments = arguments;
			CloseParenToken = closeParen;

			int start = QualifiedName.FullSpan.Start;
			int end = CloseParenToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = QualifiedName.ToString() + OpenParenToken.ToString();
			if (Arguments != null)
			{
				foreach (ArgumentSyntax argument in Arguments)
				{
					str += argument.ToString();
				}
			}
			return str + CloseParenToken.ToString();
		}

		public override string ToFullString()
		{
			string str = QualifiedName.ToFullString() + OpenParenToken.ToFullString();
			if (Arguments != null)
			{
				foreach (ArgumentSyntax argument in Arguments)
				{
					str += argument.ToFullString();
				}
			}
			return str + CloseParenToken.ToFullString();
		}
	}

	public class MixinExpressionSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken MixinToken;
		public SyntaxToken OpenParenToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CloseParenToken;

		public MixinExpressionSyntax(SyntaxToken mixin, SyntaxToken openParen, SimpleExpressionSyntax expression, SyntaxToken closeParen)
		{
			MixinToken = mixin;
			OpenParenToken = openParen;
			Expression = expression;
			CloseParenToken = closeParen;

			int start = MixinToken.FullSpan.Start;
			int end = CloseParenToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return MixinToken.ToString() + OpenParenToken.ToString() + Expression.ToString() + CloseParenToken.ToString();
		}

		public override string ToFullString()
		{
			return MixinToken.ToFullString() + OpenParenToken.ToFullString() + Expression.ToFullString() + CloseParenToken.ToFullString();
		}
	}
	
	public class IsExpressionSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken IsToken;
		public SyntaxToken OpenParen;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken EqualsEqualsToken;
		public SyntaxToken ColonToken;
		public TypeSyntax Type;
		public SyntaxToken CloseParen;

		public IsExpressionSyntax(SyntaxToken typeid, SyntaxToken openParen, SimpleExpressionSyntax expression, SyntaxToken equalsEquals, SyntaxToken colon, TypeSyntax type, SyntaxToken closeParen)
		{
			IsToken = typeid;
			OpenParen = openParen;
			Expression = expression;
			EqualsEqualsToken = equalsEquals;
			ColonToken = colon;
			Type = type;
			CloseParen = closeParen;

			int start = IsToken.FullSpan.Start;
			int end = CloseParen.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = IsToken.ToString() + OpenParen.ToString() + Expression.ToString();
			if (EqualsEqualsToken != null || ColonToken != null)
			{
				str += EqualsEqualsToken?.ToString() ?? ColonToken.ToString();
				str += Type.ToString();
			}
			return str + CloseParen.ToString();
		}

		public override string ToFullString()
		{
			string str = IsToken.ToFullString() + OpenParen.ToFullString() + Expression.ToString();
			if (EqualsEqualsToken != null || ColonToken != null)
			{
				str += EqualsEqualsToken?.ToFullString() ?? ColonToken.ToFullString();
				str += Type.ToFullString();
			}
			return str + CloseParen.ToFullString();
		}
	}

	public class NewExpressionSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken NewToken;
		public SyntaxToken OpenNewParen;
		public List<ArgumentSyntax> NewArguments;
		public SyntaxToken CloseNewParen;
		public TypeSyntax Type;
		public SyntaxToken OpenTypeParen;
		public List<ArgumentSyntax> TypeArguments;
		public SyntaxToken CloseTypeParen;
		public SyntaxToken OpenArrayBracket;
		public SimpleExpressionSyntax ArrayExpression;
		public SyntaxToken CloseArrayBracket;

		public NewExpressionSyntax(SyntaxToken newToken, SyntaxToken openNewParen, List<ArgumentSyntax> newArguments, SyntaxToken closeNewParen, TypeSyntax type, SyntaxToken openTypeParen, List<ArgumentSyntax> typeArguments, SyntaxToken closeTypeParen, SyntaxToken openArrayBracket, SimpleExpressionSyntax arrayExpression, SyntaxToken closeArrayBracket)
		{
			NewToken = newToken;
			OpenNewParen = openNewParen;
			NewArguments = newArguments;
			CloseNewParen = closeNewParen;
			Type = type;
			OpenTypeParen = openTypeParen;
			TypeArguments = typeArguments;
			CloseTypeParen = closeTypeParen;
			OpenArrayBracket = openArrayBracket;
			ArrayExpression = arrayExpression;
			CloseArrayBracket = closeArrayBracket;

			int start = NewToken.FullSpan.Start;
			int end = closeArrayBracket?.FullSpan.End ?? CloseTypeParen?.FullSpan.End ?? Type.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = NewToken.ToString();
			if (OpenNewParen != null)
			{
				str += OpenNewParen.ToString();
				foreach (ArgumentSyntax argument in NewArguments)
				{
					str += argument.ToString();
				}
				str += CloseNewParen.ToString();
			}
			else 
				str += ' ';
			str += Type.ToString();
			if (OpenTypeParen != null)
			{
				str += OpenTypeParen.ToString();
				foreach (ArgumentSyntax argument in TypeArguments)
				{
					str += argument.ToString();
				}
				str += CloseTypeParen.ToString();
			}
			if (OpenArrayBracket != null)
				str += OpenArrayBracket.ToString() + ArrayExpression.ToString() +  CloseArrayBracket.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = NewToken.ToFullString();
			if (OpenNewParen != null)
			{
				str += OpenNewParen.ToFullString();
				foreach (ArgumentSyntax argument in NewArguments)
				{
					str += argument.ToFullString();
				}
				str += CloseNewParen.ToFullString();
			}
			else
				str += ' ';
			str += Type.ToFullString();
			if (OpenTypeParen != null)
			{
				str += OpenTypeParen.ToFullString();
				foreach (ArgumentSyntax argument in TypeArguments)
				{
					str += argument.ToFullString();
				}
				str += CloseTypeParen.ToFullString();
			}
			if (OpenArrayBracket != null)
				str += OpenArrayBracket.ToFullString() + ArrayExpression.ToFullString() + CloseArrayBracket.ToFullString();
			return str;
		}
	}

	public class DeleteExpressionSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken DeleteToken;
		public SimpleExpressionSyntax Expression;

		public DeleteExpressionSyntax(SyntaxToken delete, SimpleExpressionSyntax expression)
		{
			DeleteToken = delete;
			Expression = expression;

			int start = DeleteToken.FullSpan.Start;
			int end = Expression.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return DeleteToken.ToString() + ' ' + Expression.ToString();
		}

		public override string ToFullString()
		{
			return DeleteToken.ToFullString() + Expression.ToFullString();
		}
	}

	public class ClosureCaptureSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken EqualsToken;
		public SyntaxToken AndToken;
		public NameSyntax Name;
		public SyntaxToken CommaToken;

		public ClosureCaptureSyntax(SyntaxToken equals, SyntaxToken and, NameSyntax name, SyntaxToken comma)
		{
			EqualsToken = equals;
			AndToken = and;
			Name = name;
			CommaToken = comma;

			int start = EqualsToken?.FullSpan.Start ?? AndToken?.FullSpan.Start ?? Name.FullSpan.Start;
			int end = CommaToken?.FullSpan.End ?? Name?.FullSpan.Start ?? AndToken?.FullSpan.Start ?? EqualsToken.FullSpan.Start;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (EqualsToken != null)
				str += EqualsToken.ToString();
			if (AndToken != null)
				str += AndToken.ToString();
			if (Name != null)
				str += Name.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (EqualsToken != null)
				str += EqualsToken.ToFullString();
			if (AndToken != null)
				str += AndToken.ToFullString();
			if (Name != null)
				str += Name.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}
	}

	public class ClosureSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Identifiers;
		public SyntaxToken DoubleArrowToken;
		public SyntaxToken OpenBracketToken;
		public List<ClosureCaptureSyntax> Captures;
		public SyntaxToken CloseBracketToken;
		public StatementSyntax Body;

		public ClosureSyntax(SimpleExpressionSyntax identifiers, SyntaxToken doubleArrow, SyntaxToken openBracket, List<ClosureCaptureSyntax> captures, SyntaxToken closeBracket, StatementSyntax body)
		{
			Identifiers = identifiers;
			DoubleArrowToken = doubleArrow;
			OpenBracketToken = openBracket;
			Captures = captures;
			CloseBracketToken = closeBracket;
			Body = body;
			Precedence = ExpressionPrecedence.Primary;

			int start = Identifiers.FullSpan.Start;
			int end = Body.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = Identifiers.ToString() + DoubleArrowToken.ToString();
			if (OpenBracketToken != null)
			{
				str += OpenBracketToken.ToString();
				foreach (ClosureCaptureSyntax capture in Captures)
				{
					str += capture.ToString();
				}
				str += CloseBracketToken.ToString();
			}
			return str += Body.ToString();
		}

		public override string ToFullString()
		{
			string str = Identifiers.ToFullString() + DoubleArrowToken.ToFullString();
			if (OpenBracketToken != null)
			{
				str += OpenBracketToken.ToFullString();
				foreach (ClosureCaptureSyntax capture in Captures)
				{
					str += capture.ToFullString();
				}
				str += CloseBracketToken.ToFullString();
			}
			return str += Body.ToFullString();
		}
	}

	public class LiteralExpressionSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken LiteralToken;

		public LiteralExpressionSyntax(SyntaxToken literal)
		{
			LiteralToken = literal;
			Precedence = ExpressionPrecedence.Primary;
			FullSpan = new TextSpan(LiteralToken.FullSpan.Start, LiteralToken.FullSpan.Length);
		}

		public override string ToString()
		{
			return LiteralToken.ToString();
		}

		public override string ToFullString()
		{
			return LiteralToken.ToFullString();
		}
	}

	public class SimpleArrayLiteralExpressionSyntax : SimpleExpressionSyntax
	{
	}

	public class ArrayLiteralElemSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CommaToken;

		public ArrayLiteralElemSyntax(SimpleExpressionSyntax expression, SyntaxToken comma)
		{
			Expression = expression;
			CommaToken = comma;

			int start = Expression.FullSpan.Start;
			int end = CommaToken?.FullSpan.End ?? Expression.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = Expression.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = Expression.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class ArrayLiteralExpressionSyntax : SimpleArrayLiteralExpressionSyntax
	{
		public SyntaxToken OpenBracketToken;
		public List<ArrayLiteralElemSyntax> Elements;
		public SyntaxToken CloseBracketToken;

		public ArrayLiteralExpressionSyntax(SyntaxToken openBracket, List<ArrayLiteralElemSyntax> elements, SyntaxToken closeBracket)
		{
			OpenBracketToken = openBracket;
			Elements = elements;
			CloseBracketToken = closeBracket;
			Precedence = ExpressionPrecedence.Primary;

			int start = OpenBracketToken.FullSpan.Start;
			int end = CloseBracketToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = OpenBracketToken.ToString();
			if (Elements != null)
			{
				foreach (var element in Elements)
				{
					str += element.ToString();
				}
			}
			return str + CloseBracketToken.ToString();
		}

		public override string ToFullString()
		{
			string str = OpenBracketToken.ToFullString();
			if (Elements != null)
			{
				foreach (var element in Elements)
				{
					str += element.ToFullString();
				}
			}
			return str + CloseBracketToken.ToFullString();
		}
	}

	public class AssocArrayLiteralElemSyntax : SimpleExpressionSyntax
	{
		public SimpleExpressionSyntax Left;
		public SyntaxToken ColonToken;
		public SimpleExpressionSyntax Right;
		public SyntaxToken CommaToken;

		public AssocArrayLiteralElemSyntax(SimpleExpressionSyntax left, SyntaxToken colon, SimpleExpressionSyntax right, SyntaxToken comma)
		{
			Left = left;
			ColonToken = colon;
			Right = right;
			CommaToken = comma;

			int start = Left.FullSpan.Start;
			int end = CommaToken?.FullSpan.End ?? Right.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = Left.ToString() + ColonToken.ToString() + Right.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = Left.ToFullString() + ColonToken.ToFullString() + Right.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class AssocArrayLiteralExpressionSyntax : SimpleArrayLiteralExpressionSyntax
	{
		public SyntaxToken OpenBracketToken;
		public List<AssocArrayLiteralElemSyntax> Elements;
		public SyntaxToken CloseBracketToken;

		public AssocArrayLiteralExpressionSyntax(SyntaxToken openBracket, List<AssocArrayLiteralElemSyntax> elements, SyntaxToken closeBracket)
		{
			OpenBracketToken = openBracket;
			Elements = elements;
			CloseBracketToken = closeBracket;
			Precedence = ExpressionPrecedence.Primary;

			int start = OpenBracketToken.FullSpan.Start;
			int end = CloseBracketToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = OpenBracketToken.ToString();
			if (Elements != null)
			{
				foreach (var element in Elements)
				{
					str += element.ToString();
				}
			}
			return str + CloseBracketToken.ToString();
		}

		public override string ToFullString()
		{
			string str = OpenBracketToken.ToFullString();
			if (Elements != null)
			{
				foreach (var element in Elements)
				{
					str += element.ToFullString();
				}
			}
			return str + CloseBracketToken.ToFullString();
		}
	}

	public class AggregateInitializerElemSyntax : SimpleExpressionSyntax
	{
		public IdentifierNameSyntax Identifier;
		public SyntaxToken ColonToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CommaToken;

		public AggregateInitializerElemSyntax(IdentifierNameSyntax identifier, SyntaxToken equals, SimpleExpressionSyntax expression, SyntaxToken comma)
		{
			Identifier = identifier;
			ColonToken = equals;
			Expression = expression;
			CommaToken = comma;

			int start = Identifier?.FullSpan.Start ?? Expression.FullSpan.Start;
			int end = CommaToken?.FullSpan.End ?? Expression.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = "";
			if (Identifier != null)
				str += Identifier.ToString() + ColonToken.ToString();
			str += Expression.ToString();
			if (CommaToken != null)
				str += CommaToken.ToString();
			return str;
		}

		public override string ToFullString()
		{
			string str = "";
			if (Identifier != null)
				str += Identifier.ToFullString() + ColonToken.ToFullString();
			str += Expression.ToFullString();
			if (CommaToken != null)
				str += CommaToken.ToFullString();
			return str;
		}
	}

	public class AggregateInitializerSyntax : SimpleExpressionSyntax
	{
		public NameSyntax TypeIden;
		public SyntaxToken OpenBraceToken;
		public List<AggregateInitializerElemSyntax> Elements;
		public SyntaxToken CloseBraceToken;

		public AggregateInitializerSyntax(NameSyntax typeIden, SyntaxToken openBrace, List<AggregateInitializerElemSyntax> elements, SyntaxToken closeBrace)
		{
			TypeIden = typeIden;
			OpenBraceToken = openBrace;
			Elements = elements;
			CloseBraceToken = closeBrace;
			Precedence = ExpressionPrecedence.Primary;

			int start = TypeIden.FullSpan.Start;
			int end = CloseBraceToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			string str = TypeIden.ToString() + OpenBraceToken.ToString();
			if (Elements != null)
			{
				foreach (AggregateInitializerElemSyntax element in Elements)
				{
					str += element.ToString();
				}
			}
			return str + CloseBraceToken.ToString();
		}

		public override string ToFullString()
		{
			string str = TypeIden.ToFullString() + OpenBraceToken.ToFullString();
			if (Elements != null)
			{
				foreach (AggregateInitializerElemSyntax element in Elements)
				{
					str += element.ToFullString();
				}
			}
			return str + CloseBraceToken.ToFullString();
		}
	}

	public class SpecialKeywordExpressionSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken SpecialKeywordToken;

		public SpecialKeywordExpressionSyntax(SyntaxToken specialKeyword)
		{
			SpecialKeywordToken = specialKeyword;
			Precedence = ExpressionPrecedence.Primary;
			FullSpan = new TextSpan(SpecialKeywordToken.FullSpan.Start, SpecialKeywordToken.FullSpan.Length);
		}

		public override string ToString()
		{
			return SpecialKeywordToken.ToString();
		}

		public override string ToFullString()
		{
			return SpecialKeywordToken.ToFullString();
		}
	}

	public class IntrinsicSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken IntrinsicToken;
		public SyntaxToken OpenParenToken;
		public SimpleExpressionSyntax Expression;
		public SyntaxToken CloseParenToken;

		public IntrinsicSyntax(SyntaxToken intrinsic, SyntaxToken openParen, SimpleExpressionSyntax expression, SyntaxToken closeParen)
		{
			IntrinsicToken = intrinsic;
			OpenParenToken = openParen;
			Expression = expression;
			CloseParenToken = closeParen;

			int start = IntrinsicToken.FullSpan.Start;
			int end = CloseParenToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return IntrinsicToken.ToString() + OpenParenToken.ToString() + Expression.ToString()+ CloseParenToken.ToString();
		}

		public override string ToFullString()
		{
			return IntrinsicToken.ToFullString() + OpenParenToken.ToFullString() + Expression.ToFullString() + CloseParenToken.ToFullString();
		}
	}

	public class TypeExpressionSyntax : SimpleExpressionSyntax
	{
		public SyntaxToken OpenExprToken;
		public TypeSyntax Type;
		public SyntaxToken CloseExprToken;

		public TypeExpressionSyntax(SyntaxToken openExpr, TypeSyntax type, SyntaxToken closeExpr)
		{
			OpenExprToken = openExpr;
			Type = type;
			CloseExprToken = closeExpr;

			int start = OpenExprToken.FullSpan.Start;
			int end = CloseExprToken.FullSpan.End;
			FullSpan = new TextSpan(start, end - start);
		}

		public override string ToString()
		{
			return OpenExprToken.ToString() + Type.ToString() + CloseExprToken.ToString();
		}

		public override string ToFullString()
		{
			return OpenExprToken.ToFullString() + Type.ToFullString() + CloseExprToken.ToFullString();
		}
	}

	public class CompileExecSyntax : SimpleExpressionSyntax
	{
		// TODO
	}

	public class EmptyExpressionSyntax : SimpleExpressionSyntax
	{
		public EmptyExpressionSyntax(int position)
		{
			Precedence = ExpressionPrecedence.Primary;
			FullSpan = new TextSpan(position, 0);
		}

		public override string ToString()
		{
			return "";
		}
	}


	struct SyntaxNodeHelpers
	{
		public static ExpressionPrecedence GetBinaryPrecedence(SyntaxToken token)
		{
			switch (token.Type)
			{
			case TokenType.QuestionQuestion:
				return ExpressionPrecedence.Conditional;
			case TokenType.OrOr:
				return ExpressionPrecedence.LogicOr;
			case TokenType.AndAnd:
				return ExpressionPrecedence.LogicAnd;
			case TokenType.Or:
				return ExpressionPrecedence.BinOr;
			case TokenType.Caret:
				return ExpressionPrecedence.BinXor;
			case TokenType.And:
				return ExpressionPrecedence.BinAnd;
			case TokenType.EqualsEquals:
			case TokenType.ExclaimEquals:
			case TokenType.Less:
			case TokenType.LessEquals:
			case TokenType.Greater:
			case TokenType.GreaterEquals:
				return ExpressionPrecedence.Compare;
			case TokenType.LessLess:
			case TokenType.GreaterGreater:
			case TokenType.GreaterGreaterGreater:
				return ExpressionPrecedence.Shift;
			case TokenType.Plus:
			case TokenType.Minus:
				return ExpressionPrecedence.AddSub;
			case TokenType.Asterisk:
			case TokenType.Slash:
			case TokenType.Module:
			case TokenType.Tilde:
			case TokenType.CaretCaret:
			case TokenType.DotDot:
				return ExpressionPrecedence.MulDiv;
			default:
				return ExpressionPrecedence.None;
			}
		}

		// TODO: Does not handle all cases yet
		public static SimpleExpressionSyntax FixPrecedenceOrder(SimpleExpressionSyntax node)
		{
			// TODO: Spans
			if (node is BinaryExpressionSyntax binExpr)
			{
				if (binExpr.Precedence > binExpr.Left.Precedence)
				{
					if (binExpr.Right is BinaryExpressionSyntax binRight)
					{
						SimpleExpressionSyntax tmp = binRight.Left;
						binRight.Left = binExpr;
						binExpr.Right = tmp;

						node = FixPrecedenceOrder(binRight);
					}
				}
			}
			else if (node is TernaryExpressionSyntax terExpr)
			{
				if (terExpr.Precedence > terExpr.Condition.Precedence
				    && terExpr.Condition is BinaryExpressionSyntax cond)
				{
					SimpleExpressionSyntax tmp = cond.Left;
					cond.Left = terExpr;
					terExpr.Condition = tmp;
				}
			}
			else if (node is PrefixExpressionSyntax prefix)
			{
				if (prefix.Precedence > prefix.Right.Precedence)
				{
					if (prefix.Right is BinaryExpressionSyntax binRight)
					{
						SimpleExpressionSyntax tmp = binRight.Left;
						binRight.Left = prefix;
						prefix.Right = tmp;

						node = FixPrecedenceOrder(binRight);
					}
				}
			}

			return node;
		}
	}

	#endregion
}
