﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;

namespace MJC.SyntaxTree
{
	// TODO: How to parse types for expressions that can be a type or an expression
	public class SyntaxParser
	{
		private List<SyntaxToken> _tokens;
		private int _index = 0;

		public SyntaxTree Parse(List<SyntaxToken> tokens)
		{
			_tokens = tokens;
			_index = 0;

			PackageDirectiveSyntax package = null;
			ModuleDirectiveSyntax module = null;
			BlockStatementSyntax block = null;
			SyntaxToken token = PeekToken();

			while (token.Type != TokenType.EndOfFile)
			{
				if (token.Type == TokenType.Package)
				{
					package = ParsePackage();
				}
				else
				{
					AttributeSyntax attribs = ParseAttributes();

					if (PeekToken().Type == TokenType.Module)
					{
						module = ParseModule(attribs);
					}
					else
					{
						block = ParseBlock(true, attribs, true);
					}
				}

				token = PeekToken();
			}


			CompilationUnitSyntax unit = new CompilationUnitSyntax(package, module, block, token);
			SyntaxTree tree = new SyntaxTree(unit);

			tree.PackageName = unit.Package?.Name.ToString() ?? "";
			tree.ModuleName = unit.Module?.Name.ToString() ?? CmdLine.DefaultModule;

			return tree;
		}

		#region General Parsing

		PackageDirectiveSyntax ParsePackage()
		{
			SyntaxToken package = EatToken(TokenType.Package);
			NameSyntax qualifiedName = ParseName(null);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new PackageDirectiveSyntax(package, qualifiedName, semicolon);
		}

		ModuleDirectiveSyntax ParseModule(AttributeSyntax attribs)
		{
			SyntaxToken module = EatToken(TokenType.Module);
			IdentifierNameSyntax identifier = ParseIdentifier(null);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new ModuleDirectiveSyntax(attribs, module, identifier, semicolon);
		}

		SimpleNameSyntax ParseSimpleName(AttributeSyntax attibs)
		{
			if (PeekToken(1).Type == TokenType.ExclaimLess || PeekToken(1).Type == TokenType.Exclaim)
			{
				return ParseTemplateName(attibs);
			}
			else
			{
				return ParseAsIdentifier(attibs);
			}
		}

		IdentifierNameSyntax ParseIdentifier(AttributeSyntax attribs)
		{
			SyntaxToken token = EatToken(TokenType.Identifier, TokenType.Self);
			return new IdentifierNameSyntax(attribs, token);
		}

		IdentifierNameSyntax ParseAsIdentifier(AttributeSyntax attribs)
		{
			SyntaxToken token = PeekToken();
			NextToken();
			return new IdentifierNameSyntax(attribs, token);
		}

		TemplateNameSyntax ParseTemplateName(AttributeSyntax attribs, SyntaxToken identifier = null)
		{
			if (identifier == null)
				identifier = EatToken(TokenType.Identifier);
			SyntaxToken openTemplate = EatTokenOrNull(TokenType.ExclaimLess);

			TokenType[] stopTypes = { TokenType.Greater };
			List<ArgumentSyntax> arguments = openTemplate == null ? null : ParseArguments(stopTypes);
			SyntaxToken closeTemplate = openTemplate == null ? null : EatToken(TokenType.Greater);

			if (openTemplate == null)
			{
				openTemplate = EatToken(TokenType.Exclaim);
				arguments = new List<ArgumentSyntax>();
				arguments.Add(ParseArgument(null));
			}

			return new TemplateNameSyntax(attribs, identifier, openTemplate, arguments, closeTemplate);
		}

		NameSyntax ParseName(AttributeSyntax attribs, NameSyntax prevName = null)
		{
			NameSyntax name;
			if (PeekToken(1).Type == TokenType.Dot)
			{
				if (PeekToken(2).Type == TokenType.Identifier)
				{
					NameSyntax left = ParseIdentifier(null);
					SyntaxToken dot = EatToken(TokenType.Dot);
					SimpleNameSyntax right = ParseSimpleName(null);
					name = new QualifiedNameSyntax(attribs, left, dot, right);
				}
				else
				{
					SyntaxToken dot = EatToken(TokenType.Dot);
					SimpleNameSyntax iden = ParseIdentifier(null);
					return new QualifiedNameSyntax(attribs, null, dot, iden);
				}
			}
			else if (PeekToken(1).Type == TokenType.ColonColon)
			{
				NameSyntax left = ParseAsIdentifier(null);
				SyntaxToken colonColon = EatToken(TokenType.ColonColon);
				SimpleNameSyntax right = ParseSimpleName(null);
				name = new QualifiedNameSyntax(attribs, left, colonColon, right);
			}
			else if (prevName != null)
			{
				SyntaxToken separation = EatToken(TokenType.Dot, TokenType.ColonColon);
				SimpleNameSyntax right = ParseSimpleName(null);
				name = new QualifiedNameSyntax(attribs, prevName, separation, right);
			}
			else
			{
				name = ParseSimpleName(attribs);
			}

			if ((PeekToken().Type == TokenType.Dot ||
			     PeekToken().Type == TokenType.ColonColon) &&
			    PeekToken(1).Type == TokenType.Identifier)
			{
				name.Attribs = null;
				return ParseName(attribs, name);
			}
			return name;
		}

		NameSyntax ParseNameList(AttributeSyntax attribs, bool allowMultiple)
		{
			List<NameListElemSyntax> elements = new List<NameListElemSyntax>();
			NameListElemSyntax elem;
			do
			{
				NameSyntax name = ParseName(null);
				SyntaxToken comma = allowMultiple ? EatTokenOrNull(TokenType.Comma) : null;
				elem = new NameListElemSyntax(name, comma);
				elements.Add(elem);

			} while (elem.CommaToken != null && allowMultiple);

			if (elements.Count > 1)
				return new NameListSyntax(elements);
			return elements[0].Name;
		}

		TypeSyntax ParseType(AttributeSyntax attribs = null)
		{
			if (attribs == null)
				attribs = ParseAttributes();

			TypeSyntax curType = null;
			
			if (PeekToken().Type == TokenType.LParen) // Either tuple or surrounded with attribute
			{
				SyntaxToken openParen = EatToken(TokenType.LParen);

				TypeSyntax type = ParseType();
				if (PeekToken().Type == TokenType.Comma) // Tuple
				{
					SyntaxToken comma = EatToken(TokenType.Comma);
					TupleSubTypeSyntax subType = new TupleSubTypeSyntax(type, comma);

					List<TupleSubTypeSyntax> subTypes = new List<TupleSubTypeSyntax>{ subType };
					do
					{
						type = ParseType();
						comma = EatTokenOrNull(TokenType.Comma);
						subType = new TupleSubTypeSyntax(type, comma);
						subTypes.Add(subType);

					} while (comma != null);

					SyntaxToken closeParen = EatToken(TokenType.RParen);

					curType = new TupleTypeSyntax(openParen, subTypes, closeParen);
				}
				else // Single type enum, special case (ADT enum)
				{
					TupleSubTypeSyntax subType = new TupleSubTypeSyntax(type, null);

					List<TupleSubTypeSyntax> subTypes = new List<TupleSubTypeSyntax> { subType };

					SyntaxToken closeParen = EatToken(TokenType.RParen);

					curType = new TupleTypeSyntax(openParen, subTypes, closeParen);
				}
			}
			else if (PeekToken().Type == TokenType.Identifier)
			{
				NameSyntax qualifiedName = ParseName(attribs);
				curType = new IdentifierTypeSyntax(qualifiedName);
			}
			else if (PeekToken().Type == TokenType.Typeof)
			{
				SyntaxToken typeofToken = EatToken(TokenType.Typeof);
				SyntaxToken openParen = EatToken(TokenType.LParen);
				SimpleExpressionSyntax expr = ParseExpression(true);
				SyntaxToken closeParen = EatToken(TokenType.RParen);

				curType = new TypeofSyntax(typeofToken, openParen, expr, closeParen);
			}
			else if (PeekToken().Type == TokenType.Delegate)
			{
				curType = ParseDelegate(attribs);
				attribs = null;
			}
			else if (PeekToken().Type == TokenType.Asterisk)
			{
				SyntaxToken asterisk = EatToken(TokenType.Asterisk);
				curType = ParseType();
				curType = new PointerTypeSyntax(asterisk, curType);

			}
			else if (PeekToken().Type == TokenType.And)
			{
				SyntaxToken reference = EatToken(TokenType.And);
				curType = ParseType();
				curType = new ReferenceTypeSyntax(reference, curType);

			}
			else if (PeekToken().Type == TokenType.LBracket)
			{
				SyntaxToken openBrace = EatToken(TokenType.LBracket);
				SimpleExpressionSyntax expr = null;
				if (PeekToken().Type != TokenType.RBracket)
					expr = ParseExpression(true);
				SyntaxToken closeBrace = EatToken(TokenType.RBracket);

				curType = ParseType();
				curType = new ArrayTypeSyntax(openBrace, expr, closeBrace, curType);

			}
			else if (PeekToken().Type == TokenType.Question)
			{
				SyntaxToken questionToken = EatToken(TokenType.Question);

				curType = ParseType();
				curType = new NullableTypeSyntax(curType, questionToken);

			}
			else if (PeekToken().Type == TokenType.Struct)
			{
				StructSyntax structSyntax = ParseStruct(attribs);
				curType = new InlineStructTypeSyntax(structSyntax);
				attribs = null;
			}
			else if (PeekToken().Type == TokenType.Union)
			{
				UnionSyntax unionSyntax = ParseUnion(attribs);
				curType = new InlineUnionTypeSyntax(unionSyntax);
				attribs = null;
			}
			else
			{
				curType = ParsePredefinedType();
			}

			if (PeekToken().Type == TokenType.Dot)
			{
				SyntaxToken dot = EatToken(TokenType.Dot);
				SimpleExpressionSyntax lit = ParseExpression(true);
				curType = new BitFieldTypeSyntax(curType, dot, lit);
			}
			else if (PeekToken().Type == TokenType.DotDotDot)
			{
				SyntaxToken variadic = EatToken(TokenType.DotDotDot);
				curType = new VariadicTypeSyntax(curType, variadic);
			}

			if (attribs != null)
				return new AttributedTypeSyntax(attribs, null, curType, null);

			return curType;
		}

		BuiltinTypeSyntax ParsePredefinedType()
		{
			SyntaxToken token = PeekToken();

			switch (token.Type)
			{
			case TokenType.Bool:
			case TokenType.Byte:
			case TokenType.UByte:
			case TokenType.Short:
			case TokenType.UShort:
			case TokenType.Int:
			case TokenType.UInt:
			case TokenType.Long:
			case TokenType.ULong:
			case TokenType.ISize:
			case TokenType.USize:
			case TokenType.Float:
			case TokenType.Double:
			case TokenType.Char:
			case TokenType.WChar:
			case TokenType.Rune:
			case TokenType.Void:
			case TokenType.String:
			case TokenType.SelfType:
			{
				BuiltinTypeSyntax type = new BuiltinTypeSyntax(token);
				NextToken();
				return type;
			}
			default:
			{
				token = new SyntaxToken(new TextSpan(token.FullSpan.Start, 0), TokenType.Missing, "");
				return new BuiltinTypeSyntax(token);
			}
			}
		}

		DelegateSyntax ParseDelegate(AttributeSyntax attribs, bool withSemicolon = false)
		{
			if (attribs == null)
				attribs = ParseAttributes();

			SyntaxToken delegateToken = EatToken(TokenType.Delegate);
			IdentifierNameSyntax name = PeekToken().Type == TokenType.LParen ? null : ParseIdentifier(null);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			List<ParameterSyntax> parameters = ParseParameters();
			SyntaxToken closeParen = EatToken(TokenType.RParen);
			SyntaxToken arrow = EatTokenOrNull(TokenType.Arrow);
			TypeSyntax returnType = arrow == null ? null : ParseType();

			SyntaxToken semicolon = withSemicolon ? EatToken(TokenType.Semicolon) : null;

			return new DelegateSyntax(attribs, delegateToken, name, openParen, parameters, closeParen, arrow, returnType, semicolon);
		}

		AttributeSyntax ParseAttributes()
		{
			List<SingleAttributeSyntax> singleAttribs = null;
			List<AtAttributeSyntax> atAttribs = null;
			List<CompilerAtAttributeSyntax> compilerAtAttribs = null;

			bool run = true;
			while (run)
			{
				switch (PeekToken().Type)
				{
				case TokenType.Const:
				case TokenType.CConst:
				case TokenType.Immutable:
				case TokenType.Mutable:
				case TokenType.Static:
				case TokenType.Private:
				case TokenType.Internal:
				case TokenType.Public:
				case TokenType.Export:
				case TokenType.In:
				case TokenType.Out:
				case TokenType.Inout:
				case TokenType.Lazy:
				case TokenType.Synchronized:
				case TokenType.Shared:
				case TokenType.UUGlobal:
				{
					if (singleAttribs == null)
						singleAttribs = new List<SingleAttributeSyntax>();

					SingleAttributeSyntax attrib = new SingleAttributeSyntax(PeekToken());
					singleAttribs.Add(attrib);
					NextToken();
					break;
				}
				case TokenType.At:
				{
					if (atAttribs == null)
						atAttribs = new List<AtAttributeSyntax>();

					AtAttributeSyntax attrib = ParseAtAttribute();
					atAttribs.Add(attrib);
					break;
				}
				case TokenType.AtColon:
				{
					if (compilerAtAttribs == null)
						compilerAtAttribs = new List<CompilerAtAttributeSyntax>();

					CompilerAtAttributeSyntax attrib = ParseCompilerAtAttribute();
					compilerAtAttribs.Add(attrib);
					break;
				}
				default:
					run = false;
					break;
				}
			}

			if (singleAttribs == null && atAttribs == null && compilerAtAttribs == null)
				return null;

			return new AttributeSyntax(singleAttribs, atAttribs, compilerAtAttribs);
		}

		AtAttributeSyntax ParseAtAttribute()
		{
			SyntaxToken at = EatToken(TokenType.At);
			NameSyntax name = ParseName(null);
			SyntaxToken openParen = EatTokenOrNull(TokenType.LParen);

			List<ArgumentSyntax> arguments = null;
			SyntaxToken closeParen = null;
			if (openParen != null)
			{
				if (PeekToken().Type != TokenType.RParen)
					arguments = ParseArguments();

				closeParen = EatToken(TokenType.RParen);
			}

			return new AtAttributeSyntax(at, name, openParen, arguments, closeParen);
		}

		CompilerAtAttributeSyntax ParseCompilerAtAttribute()
		{
			SyntaxToken at = EatToken(TokenType.AtColon);
			NameSyntax name = ParseName(null);
			SyntaxToken openParen = EatTokenOrNull(TokenType.LParen);

			List<ArgumentSyntax> arguments = null;
			SyntaxToken closeParen = null;
			if (openParen != null)
			{
				if (PeekToken().Type != TokenType.RParen)
					arguments = ParseArguments();

				closeParen = EatToken(TokenType.RParen);
			}

			return new CompilerAtAttributeSyntax(at, name, openParen, arguments, closeParen);
		}
		
		List<ArgumentSyntax> ParseArguments(TokenType[] stopTypes = null)
		{
			if (PeekToken().Type == TokenType.RParen)
				return null;

			List<ArgumentSyntax> arguments = new List<ArgumentSyntax>();
			ArgumentSyntax arg;
			
			do
			{
				arg = ParseArgument(stopTypes);
				arguments.Add(arg);

			} while (arg.CommaToken != null);
			
			return arguments;
		}

		ArgumentSyntax ParseArgument(TokenType[] stopTypes)
		{
			IdentifierNameSyntax identifier = null;
			SyntaxToken colon = null;
			if (PeekToken(1).Type == TokenType.Colon)
			{
				identifier = ParseAsIdentifier(null);
				colon = EatToken(TokenType.Colon);
			}

			SimpleExpressionSyntax expr = ParseExpression(true, stopTokenType: stopTypes);
			SyntaxToken comma = EatTokenOrNull(TokenType.Comma);

			return new ArgumentSyntax(identifier, colon, expr, comma);
		}

		List<ParameterSyntax> ParseParameters()
		{
			if (PeekToken().Type == TokenType.RParen)
				return null;

			List<ParameterSyntax> parameters = new List<ParameterSyntax>();
			ParameterSyntax param;
			do
			{
				param = ParseParameter();
				parameters.Add(param);

			} while (param.CommaToken != null);

			return parameters;
		}

		ParameterSyntax ParseParameter()
		{
			AttributeSyntax attribs = ParseAttributes();
			IdentifierNameSyntax identifier = ParseIdentifier(null);

			SyntaxToken colon = EatTokenOrNull(TokenType.Colon);
			TypeSyntax type = null;
			SyntaxToken variadic = null;

			if (PeekToken().Type == TokenType.DotDotDot)
				variadic = EatToken(TokenType.DotDotDot);
			else
				type = ParseType();

			SyntaxToken equals = EatTokenOrNull(TokenType.Equals);
			SimpleExpressionSyntax defaultValue = equals == null ? null : ParseExpression(true);

			SyntaxToken comma = EatTokenOrNull(TokenType.Comma);

			return new ParameterSyntax(attribs, identifier, colon, type, variadic, equals, defaultValue, comma);
		}

		List<TemplateParameterSyntax> ParseTemplateParameters()
		{
			List<TemplateParameterSyntax> parameters = new List<TemplateParameterSyntax>();
			TemplateParameterSyntax param;
			do
			{
				param = ParseTemplateParameter();
				parameters.Add(param);

			} while (param.CommaToken != null);

			return parameters;
		}

		TemplateParameterSyntax ParseTemplateParameter()
		{
			IdentifierNameSyntax identifier = ParseIdentifier(null);

			SyntaxToken colon = EatTokenOrNull(TokenType.Colon);
			TypeSyntax type = colon == null ? null : ParseType();

			TokenType[] stopTokens = { TokenType.Greater, TokenType.Equals };

			SyntaxToken exclaimColon = EatTokenOrNull(TokenType.ColonEquals);
			SimpleExpressionSyntax specializationValue = exclaimColon == null ? null : ParseExpression(true, stopTokenType: stopTokens);

			SyntaxToken variadic = EatTokenOrNull(TokenType.DotDotDot);

			SyntaxToken equals = EatTokenOrNull(TokenType.Equals);
			SimpleExpressionSyntax defaultValue = equals == null ? null : ParseExpression(true, stopTokenType: stopTokens);

			SyntaxToken comma = EatTokenOrNull(TokenType.Comma);

			return new TemplateParameterSyntax(identifier, colon, type, exclaimColon, specializationValue, variadic, equals, defaultValue, comma);
		}

		#endregion

		#region Statement parsing

		BlockStatementSyntax ParseBlock(bool parseUntilEoF = false, AttributeSyntax attribs = null, bool globalBlock = false)
		{
			if (attribs == null)
				attribs = ParseAttributes();

			SyntaxToken openBrace = null;
			if (PeekToken().Type == TokenType.LBrace)
				openBrace = EatToken(TokenType.LBrace);

			List<StatementSyntax> statements = new List<StatementSyntax>();
			while ((parseUntilEoF && PeekToken().Type != TokenType.EndOfFile) || (!parseUntilEoF && PeekToken().Type != TokenType.RBrace))
			{
				StatementSyntax statement = ParseStatement(globalBlock ? attribs : null);
				statements.Add(statement);

				if (globalBlock)
					attribs = null;
			}

			SyntaxToken closeBrace = null;
			if (openBrace != null)
				closeBrace = EatToken(TokenType.RBrace);

			if (statements.Count == 0)
				statements = null;

			if (statements == null && openBrace == null)
				return null;

			return new BlockStatementSyntax(attribs, openBrace, statements, closeBrace);
		}

		StatementSyntax ParseStatement(AttributeSyntax attribs = null)
		{
			if (attribs == null)
				attribs = ParseAttributes();

			NameSyntax nameList = null;

			while (true)
			{
				switch (PeekToken().Type)
				{
				case TokenType.LBrace:
					return ParseBlock(attribs: attribs);
				case TokenType.Import:
					return ParseImport();
				case TokenType.Func:
					return ParseFunction(attribs);
				case TokenType.Identifier:
					nameList = ParseNameList(attribs, true);
					break;
				case TokenType.Colon:
				{
					if (nameList != null)
						return ParseDeclaration(attribs, nameList);
					return ParseLabel();
				}
				case TokenType.ColonEquals:
					return ParseDeclaration(attribs, nameList);
				case TokenType.Namespace:
					return ParseNamespace();
				case TokenType.TypeAlias:
					return ParseTypeAlias();
				case TokenType.Typedef:
					return ParseTypedef();
				case TokenType.If:
					return ParseIfElse(attribs);
				case TokenType.While:
					return ParseWhile();
				case TokenType.Do:
					return ParseDoWhile();
				case TokenType.For:
					return ParseFor(attribs);
				case TokenType.Foreach:
					return ParseForeach(attribs);
				case TokenType.Switch:
					return ParseSwitch();
				case TokenType.Fallthrough:
					return ParseFallthrough();
				case TokenType.Continue:
					return ParseContinue();
				case TokenType.Break:
					return ParseBreak();
				case TokenType.Return:
					return ParseReturn();
				case TokenType.Defer:
					return ParseDefer();
				case TokenType.Goto:
					return ParseGoto();
				case TokenType.Delegate:
					return ParseDelegate(attribs, true);
				case TokenType.Impl:
					return ParseImpl(attribs);
				case TokenType.Struct:
					return ParseStruct(attribs);
				case TokenType.Union:
					return ParseUnion(attribs);
				case TokenType.Interface:
					return ParseInterface(attribs);
				case TokenType.Enum:
					return ParseEnum(attribs);
				case TokenType.UnitTest:
					return ParseUnitTest();
				case TokenType.Version:
				case TokenType.Debug:
				{
					if (PeekToken(1).Type == TokenType.LParen)
						return ParseCompileConditional();
					nameList = ParseNameList(null, false);
					break;
				}
				case TokenType.Assert:
					return ParseAssert(attribs);
				default:
					return ParseExpression(false, left: nameList, allowCommaExpression: true);
				}
			}
		}

		NamespaceSyntax ParseNamespace()
		{
			AttributeSyntax attribs = ParseAttributes();
			SyntaxToken namespaceToken = EatToken(TokenType.Namespace);
			NameSyntax name = ParseName(null);
			BlockStatementSyntax body = ParseBlock();

			return new NamespaceSyntax(attribs, namespaceToken, name, body);
		}

		ImportSyntax ParseImport(AttributeSyntax attribs = null)
		{
			SyntaxToken import = EatToken(TokenType.Import);
			NameSyntax moduleName = ParseName(null);

			SyntaxToken asToken = null;
			IdentifierNameSyntax moduleAlias = null;
			if (PeekToken().Type == TokenType.As)
			{
				asToken = EatToken(TokenType.As);
				moduleAlias = ParseIdentifier(null);
			}

			SyntaxToken colon = null; 
			List<ImportSymbolSyntax> symbols = null;
			if (PeekToken().Type == TokenType.Colon)
			{
				colon = EatToken(TokenType.Colon);
				symbols = ParseImportSymbols();
			}

			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new ImportSyntax(attribs, import, moduleName, asToken, moduleAlias, colon, symbols, semicolon);
		}

		List<ImportSymbolSyntax> ParseImportSymbols()
		{
			List<ImportSymbolSyntax> symbols = new List<ImportSymbolSyntax>();

			do
			{
				NameSyntax symbol = ParseName(null);

				SyntaxToken asToken = null;
				IdentifierNameSyntax alias = null;
				if (PeekToken().Type == TokenType.As)
				{
					asToken = EatToken(TokenType.As);
					alias = ParseIdentifier(null);
				}

				SyntaxToken comma = null;
				if (PeekToken().Type == TokenType.Comma)
					comma = EatToken(TokenType.Comma);

				symbols.Add(new ImportSymbolSyntax(symbol, asToken, alias, comma));

			} while (symbols.Last().CommaToken != null);

			return symbols;
		}

		ReceiverSyntax ParseReceiver()
		{
			SyntaxToken openParen = EatToken(TokenType.LParen);
			SyntaxToken reference = EatTokenOrNull(TokenType.And);
			SyntaxToken constTok = EatTokenOrNull(TokenType.Const);
			SyntaxToken self = EatToken(TokenType.Self);
			SyntaxToken closeParen = EatToken(TokenType.RParen);

			return new ReceiverSyntax(openParen, reference, constTok, self, closeParen);
		}

		FunctionSyntax ParseFunction(AttributeSyntax attribs)
		{
			SyntaxToken func = EatToken(TokenType.Func);

			ReceiverSyntax receiver = null;
			if (PeekToken().Type == TokenType.LParen)
				receiver = ParseReceiver();

			IdentifierNameSyntax name = ParseIdentifier(null);

			SyntaxToken openTemplate = null;
			List<TemplateParameterSyntax> templateParameters = null;
			SyntaxToken closeTemplate = null;
			if (PeekToken().Type == TokenType.Less)
			{
				openTemplate = EatToken(TokenType.Less);
				templateParameters = ParseTemplateParameters();
				closeTemplate = EatToken(TokenType.Greater);
			}

			SyntaxToken openParen = EatToken(TokenType.LParen);

			List<ParameterSyntax> parameters = null;
			if (PeekToken().Type != TokenType.RParen)
				parameters = ParseParameters();
			
			SyntaxToken closeParen = EatToken(TokenType.RParen);

			SyntaxToken arrow = null;
			TypeSyntax returnType = null;
			if (PeekToken().Type == TokenType.Arrow)
			{
				arrow = EatToken(TokenType.Arrow);
				returnType = ParseType();
			}

			TemplateConstraintSyntax constraint = null;
			if (PeekToken().Type == TokenType.If)
				constraint = ParseTemplateConstraint();

			BlockStatementSyntax body = null;
			SyntaxToken semicolon = null;
			if (PeekToken().Type == TokenType.Semicolon)
			{
				semicolon = EatToken(TokenType.Semicolon);
			}
			else
			{
				body = ParseBlock();
			}

			return new FunctionSyntax(attribs, func, receiver, name, openTemplate, templateParameters, closeTemplate, openParen, parameters, closeParen, arrow, returnType, constraint, body, semicolon);
		}

		TemplateConstraintSyntax ParseTemplateConstraint()
		{
			SyntaxToken ifToken = EatToken(TokenType.If);
			SyntaxToken openParan = EatToken(TokenType.LParen);
			SimpleExpressionSyntax expression = ParseExpression(true);
			SyntaxToken closeParan = EatToken(TokenType.RParen);

			return new TemplateConstraintSyntax(ifToken, openParan, expression, closeParan);
		}

		PropertyGetSetSyntax ParseGetSet()
		{
			AttributeSyntax attribs = ParseAttributes();
			SyntaxToken iden = PeekToken();
			NextToken();
			SyntaxToken semicolon = EatTokenOrNull(TokenType.Semicolon);
			BlockStatementSyntax body = semicolon == null ? ParseBlock() : null;

			return new PropertyGetSetSyntax(attribs, iden, body, semicolon);
		}

		DeclarationSyntax ParseDeclaration(AttributeSyntax attribs, NameSyntax nameList)
		{
			if (nameList == null)
			{
				if (attribs == null)
					attribs = ParseAttributes();
				nameList = ParseNameList(attribs, true);
			}

			SyntaxToken colon = EatTokenOrNull(TokenType.Colon);
			TypeSyntax type = colon == null ? null : ParseType();

			SyntaxToken dot = colon == null ? null : EatTokenOrNull(TokenType.Dot);
			TokenType[] stopToken = {TokenType.Equals};
			SimpleExpressionSyntax bitfield = dot == null ? null : ParseExpression(true, stopTokenType: stopToken);

			SyntaxToken openBrace = EatTokenOrNull(TokenType.LBrace);
			List<PropertyGetSetSyntax> getset = null;
			if (openBrace != null)
			{
				getset = new List<PropertyGetSetSyntax>();
				while (PeekToken().Type != TokenType.RBrace)
				{
					getset.Add(ParseGetSet());
				}
			}
			SyntaxToken closeBrace = openBrace == null ? null : EatToken(TokenType.RBrace);

			SyntaxToken equals = EatTokenOrNull(TokenType.Equals) ?? EatTokenOrNull(TokenType.ColonEquals);
			SimpleExpressionSyntax initializer = equals == null ? null : ParseExpression(true, allowCommaExpression: true);

			SyntaxToken semicolon = (openBrace == null || initializer != null) ? EatTokenOrNull(TokenType.Semicolon) : null;
			SyntaxToken comma = EatTokenOrNull(TokenType.Comma);

			return new DeclarationSyntax(attribs, nameList, colon, type, openBrace, getset, closeBrace, equals, initializer, semicolon, comma);
		}

		TypeAliasSyntax ParseTypeAlias()
		{
			SyntaxToken alias = EatToken(TokenType.TypeAlias);
			SyntaxToken openTemplate = EatTokenOrNull(TokenType.Less);
			List<TemplateParameterSyntax> templateParams = null;
			SyntaxToken closeTemplate = null;
			if (openTemplate != null)
			{
				templateParams = ParseTemplateParameters();
				closeTemplate = EatToken(TokenType.Greater);
			}
			IdentifierNameSyntax name = ParseIdentifier(null);
			SyntaxToken equals = EatTokenOrNull(TokenType.Equals);
			TypeSyntax type = equals == null ? null : ParseType(null);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new TypeAliasSyntax(alias, openTemplate, templateParams, closeTemplate, name, equals, type, semicolon);
		}

		TypedefSyntax ParseTypedef()
		{
			SyntaxToken typdef = EatToken(TokenType.Typedef);
			SyntaxToken openTemplate = EatTokenOrNull(TokenType.Less);
			List<TemplateParameterSyntax> templateParams = null;
			SyntaxToken closeTemplate = null;
			if (openTemplate != null)
			{
				templateParams = ParseTemplateParameters();
				closeTemplate = EatToken(TokenType.Greater);
			}

			IdentifierNameSyntax name = ParseIdentifier(null);
			SyntaxToken equals = EatTokenOrNull(TokenType.Equals);
			TypeSyntax type = equals == null ? null : ParseType(null);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new TypedefSyntax(typdef, openTemplate, templateParams, closeTemplate, name, equals, type, semicolon);
		}

		IfElseSyntax ParseIfElse(AttributeSyntax attribs)
		{
			IfSyntax ifSyntax = ParseIf(attribs);
			SimpleIfSubStatementSyntax elseStatement = null;
			if (PeekToken().Type == TokenType.Else || PeekToken().Type == TokenType.Elif)
			{
				elseStatement = ParseElse();
			}

			return new IfElseSyntax(ifSyntax, elseStatement);
		}

		IfSyntax ParseIf(AttributeSyntax attribs)
		{
			SyntaxToken leadingElse = EatTokenOrNull(TokenType.Else);
			SyntaxToken elifIf = EatTokenOrNull(TokenType.Elif) ?? EatToken(TokenType.If);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			SimpleExpressionSyntax condition = ParseExpression(true);
			SyntaxToken closeParen = EatToken(TokenType.RParen);
			StatementSyntax then = ParseStatement();

			return new IfSyntax(attribs, leadingElse, elifIf, openParen, condition, closeParen, then);
		}

		ElseSyntax ParseElse()
		{
			SyntaxToken elseToken = EatTokenOrNull(TokenType.Else);
			StatementSyntax elseBody = ParseStatement();

			return new ElseSyntax(elseToken, elseBody);
		}

		WhileSyntax ParseWhile()
		{
			SyntaxToken whileToken = EatToken(TokenType.While);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			SimpleExpressionSyntax condition = ParseExpression(true);
			SyntaxToken closeParen = EatToken(TokenType.RParen);
			StatementSyntax body = ParseStatement();

			return new WhileSyntax(whileToken, openParen, condition, closeParen, body);
		}

		DoWhileSyntax ParseDoWhile()
		{
			SyntaxToken doToken = EatToken(TokenType.Do);
			StatementSyntax body = ParseStatement();
			SyntaxToken whileToken = EatToken(TokenType.While);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			SimpleExpressionSyntax condition = ParseExpression(true);
			SyntaxToken closeParen = EatToken(TokenType.RParen);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new DoWhileSyntax(doToken, body, whileToken, openParen, condition, closeParen, semicolon);
		}

		ForSyntax ParseFor(AttributeSyntax attribs)
		{
			SyntaxToken forToken = EatToken(TokenType.For);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			SimpleExpressionSyntax initializer = ParseExpression(true);
			SyntaxToken semicolon0 = EatTokenOrNull(TokenType.Semicolon);
			SimpleExpressionSyntax condition = ParseExpression(true);
			SyntaxToken semicolon1 = EatToken(TokenType.Semicolon);
			SimpleExpressionSyntax increment = ParseExpression(true);
			SyntaxToken closeParen = EatToken(TokenType.RParen);
			StatementSyntax body = ParseStatement();

			return new ForSyntax(attribs, forToken, openParen, initializer, semicolon0, condition, semicolon1, increment, closeParen, body);
		}

		ForeachSyntax ParseForeach(AttributeSyntax attribs)
		{
			SyntaxToken foreachToken = EatToken(TokenType.Foreach);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			NameSyntax values = ParseNameList(null, true);
			SyntaxToken inToken = EatToken(TokenType.In);
			SimpleExpressionSyntax source = ParseExpression(true);
			SyntaxToken closeParen = EatToken(TokenType.RParen);
			StatementSyntax body = ParseStatement();

			return new ForeachSyntax(attribs, foreachToken, openParen, values, inToken, source, closeParen, body);
		}

		SwitchSyntax ParseSwitch()
		{
			SyntaxToken switchToken = EatToken(TokenType.Switch);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			SimpleExpressionSyntax expression = ParseExpression(true);
			SyntaxToken closeParen = EatToken(TokenType.RParen);
			SyntaxToken openBrace = EatToken(TokenType.LBrace);

			List<SwitchCaseSyntax> cases = null;
			SyntaxToken tok = PeekToken();
			if (tok.Type != TokenType.RBrace)
			{
				cases = new List<SwitchCaseSyntax>();
				while (tok.Type != TokenType.RBrace)
				{
					SyntaxToken caseToken = EatTokenOrNull(TokenType.Case);
					TokenType[] stopToken = {TokenType.Colon};
					SimpleExpressionSyntax expr = cases == null ? null : ParseExpression(true, allowCommaExpression: true, stopTokenType: stopToken);
					;
					SyntaxToken defaultToken = caseToken != null ? null : EatToken(TokenType.Default);
					SyntaxToken colon = EatToken(TokenType.Colon);
					List<StatementSyntax> statements = null;

					tok = PeekToken();
					if (tok.Type != TokenType.Default && tok.Type != TokenType.Case && tok.Type != TokenType.RBrace)
					{
						statements = new List<StatementSyntax>();
						do
						{
							StatementSyntax statement = ParseStatement();
							statements.Add(statement);
							tok = PeekToken();
						} while (tok.Type != TokenType.Default && tok.Type != TokenType.Case && tok.Type != TokenType.RBrace);
					}

					SwitchCaseSyntax switchCase = new SwitchCaseSyntax(caseToken, expr, defaultToken, colon, statements);
					cases.Add(switchCase);
				}
			}

			SyntaxToken closeBrace = EatToken(TokenType.RBrace);

			return new SwitchSyntax(switchToken, openParen, expression, closeParen, openBrace, cases, closeBrace);
		}

		FallthroughSyntax ParseFallthrough()
		{
			SyntaxToken fallthrough = EatToken(TokenType.Fallthrough);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new FallthroughSyntax(fallthrough, semicolon);
		}

		ContinueSyntax ParseContinue()
		{
			SyntaxToken continueToken = EatToken(TokenType.Continue);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new ContinueSyntax(continueToken, semicolon);
		}

		BreakSyntax ParseBreak()
		{
			SyntaxToken breakToken = EatToken(TokenType.Break);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new BreakSyntax(breakToken, semicolon);
		}

		ReturnSyntax ParseReturn()
		{
			SyntaxToken returnToken = EatToken(TokenType.Return);
			SimpleExpressionSyntax expr = ParseExpression(true, allowCommaExpression:true);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new ReturnSyntax(returnToken, expr, semicolon);
		}

		DeferSyntax ParseDefer()
		{
			SyntaxToken deferToken = EatToken(TokenType.Defer);
			SimpleExpressionSyntax expr = ParseExpression(true);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new DeferSyntax(deferToken, expr, semicolon);
		}

		GotoSyntax ParseGoto()
		{
			SyntaxToken gotoToken = EatToken(TokenType.Goto);
			SyntaxToken defaultToken = EatTokenOrNull(TokenType.Default);
			SyntaxToken caseToken = EatTokenOrNull(TokenType.Case);
			IdentifierNameSyntax identifier = defaultToken == null ? ParseAsIdentifier(null) : null;
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new GotoSyntax(gotoToken, defaultToken, caseToken, identifier, semicolon);
		}

		LabelSyntax ParseLabel()
		{
			SyntaxToken leftColon = EatToken(TokenType.Colon);
			IdentifierNameSyntax identifier = ParseIdentifier(null);
			SyntaxToken rightColon = EatToken(TokenType.Colon);

			return new LabelSyntax(leftColon, identifier, rightColon);
		}

		ImplInterfaceFieldDefSyntax ParseImplInterfaceFieldDef()
		{
			IdentifierNameSyntax interfaceField = ParseIdentifier(null);
			SyntaxToken colon = EatToken(TokenType.Colon);
			NameSyntax aggregateField = ParseName(null);
			SyntaxToken comma = EatTokenOrNull(TokenType.Comma);

			return new ImplInterfaceFieldDefSyntax(interfaceField, colon, aggregateField, comma);
		}

		ImplInterfaceSyntax ParseImplInterface()
		{
			NameSyntax name = ParseName(null);
			SyntaxToken openParen = EatTokenOrNull(TokenType.LParen);
			List<ImplInterfaceFieldDefSyntax> fieldDefs = null;
			SyntaxToken closeParen = null;
			if (openParen != null)
			{
				fieldDefs = new List<ImplInterfaceFieldDefSyntax>();
				ImplInterfaceFieldDefSyntax fieldDef = null;
				do
				{
					fieldDef = ParseImplInterfaceFieldDef();
					fieldDefs.Add(fieldDef);
				} while (fieldDef.Comma != null);

				closeParen = EatToken(TokenType.RParen);
			}
			SyntaxToken comma = EatTokenOrNull(TokenType.Comma);

			return new ImplInterfaceSyntax(name, openParen, fieldDefs, closeParen, comma);
		}

		ImplSyntax ParseImpl(AttributeSyntax attribs)
		{
			if (attribs == null)
				attribs = ParseAttributes();

			SyntaxToken implToken = EatToken(TokenType.Impl);
			SyntaxToken openTemplate = EatTokenOrNull(TokenType.Less);
			List<TemplateParameterSyntax> templateParams = openTemplate == null ? null : ParseTemplateParameters();
			SyntaxToken closeTemplate = openTemplate == null ? null : EatToken(TokenType.Greater);
			TypeSyntax type = ParseType();
			SyntaxToken colonToken = EatTokenOrNull(TokenType.Colon);

			List<ImplInterfaceSyntax> baseList = null;
			if (colonToken != null)
			{
				baseList = new List<ImplInterfaceSyntax>();
				ImplInterfaceSyntax implInterface = null;
				do
				{
					implInterface = ParseImplInterface();
					baseList.Add(implInterface);
				} while (implInterface.Comma != null);
			}
			
			TemplateConstraintSyntax constraint = null;
			if (PeekToken().Type == TokenType.If)
				constraint = ParseTemplateConstraint();
			BlockStatementSyntax body = ParseBlock();

			return new ImplSyntax(attribs, implToken, openTemplate, templateParams, closeTemplate, type, colonToken, baseList, constraint, body);
		}

		AggregateBodySyntax ParseAggregateBody()
		{
			SyntaxToken openBody = EatToken(TokenType.LBrace);
			List<DeclarationSyntax> declarations = null;
			if (PeekToken().Type != TokenType.RBrace)
			{
				declarations = new List<DeclarationSyntax>();
				while (PeekToken().Type != TokenType.RBrace)
				{
					DeclarationSyntax decl = ParseDeclaration(null, null);
					declarations.Add(decl);
				}
			}

			SyntaxToken closeBody = EatToken(TokenType.RBrace);

			return new AggregateBodySyntax(openBody, declarations, closeBody);
		}

		StructSyntax ParseStruct(AttributeSyntax attribs)
		{
			SyntaxToken classToken = EatToken(TokenType.Struct);
			IdentifierNameSyntax name = PeekToken().Type == TokenType.LBrace ? null : ParseIdentifier(null);
			SyntaxToken openTemplate = name == null ? null : EatTokenOrNull(TokenType.Less);
			List<TemplateParameterSyntax> templateParams = openTemplate == null ? null : ParseTemplateParameters();
			SyntaxToken closeTemplate = openTemplate == null ? null : EatToken(TokenType.Greater);
			SyntaxToken colonToken = EatTokenOrNull(TokenType.Colon);
			NameSyntax baseList = colonToken == null ? null : ParseNameList(null, true);
			AggregateBodySyntax body = ParseAggregateBody();

			return new StructSyntax(attribs, classToken, name, openTemplate, templateParams, closeTemplate, colonToken, baseList, body);
		}

		InterfaceSyntax ParseInterface(AttributeSyntax attribs)
		{
			SyntaxToken classToken = EatToken(TokenType.Interface);
			IdentifierNameSyntax name = ParseIdentifier(null);
			SyntaxToken openTemplate = EatTokenOrNull(TokenType.Less);
			List<TemplateParameterSyntax> templateParams = openTemplate == null ? null : ParseTemplateParameters();
			SyntaxToken closeTemplate = openTemplate == null ? null : EatToken(TokenType.Greater);
			SyntaxToken colonToken = EatTokenOrNull(TokenType.Colon);
			NameSyntax baseList = colonToken == null ? null : ParseNameList(null, true);
			BlockStatementSyntax body = ParseBlock();

			return new InterfaceSyntax(attribs, classToken, name, openTemplate, templateParams, closeTemplate, colonToken, baseList, body);
		}

		UnionSyntax ParseUnion(AttributeSyntax attribs)
		{
			SyntaxToken unionToken = EatToken(TokenType.Union);
			IdentifierNameSyntax name = PeekToken().Type == TokenType.LBrace ? null : ParseIdentifier(null);
			SyntaxToken openTemplate = EatTokenOrNull(TokenType.Less);
			List<TemplateParameterSyntax> templateParams = openTemplate == null ? null : ParseTemplateParameters();
			SyntaxToken closeTemplate = openTemplate == null ? null : EatToken(TokenType.Greater);
			AggregateBodySyntax body = ParseAggregateBody();

			return new UnionSyntax(attribs, unionToken, name, openTemplate, templateParams, closeTemplate, body);
		}
		
		EnumMemberSyntax ParseEnumMember()
		{
			IdentifierNameSyntax name = ParseIdentifier(null);
			TupleTypeSyntax tupleType = null;
			if (PeekToken().Type == TokenType.LParen)
				tupleType = ParseType() as TupleTypeSyntax;

			AggregateBodySyntax aggrBody = null;
			if (PeekToken().Type == TokenType.LBrace)
				aggrBody = ParseAggregateBody();

			SyntaxToken equals = EatTokenOrNull(TokenType.Equals);
			SimpleExpressionSyntax expression = equals == null ? null : ParseExpression(true);
			SyntaxToken comma = EatTokenOrNull(TokenType.Comma);

			return new EnumMemberSyntax(name, aggrBody, tupleType, equals, expression, comma);
		}

		EnumSyntax ParseEnum(AttributeSyntax attribs)
		{
			SyntaxToken enumToken = EatToken(TokenType.Enum);
			IdentifierNameSyntax name = PeekToken().Type == TokenType.Identifier ? ParseIdentifier(null) : null;
			SyntaxToken openTemplate = EatTokenOrNull(TokenType.Less);
			List<TemplateParameterSyntax> templateParams = openTemplate == null ? null : ParseTemplateParameters();
			SyntaxToken closeTemplate = openTemplate == null ? null : EatToken(TokenType.Greater);
			SyntaxToken colon = EatTokenOrNull(TokenType.Colon);
			TypeSyntax type = colon == null ? null : ParseType();
			SyntaxToken openBrace = EatToken(TokenType.LBrace);
			List<EnumMemberSyntax> members = null;
			if (PeekToken().Type != TokenType.RBrace)
			{
				members = new List<EnumMemberSyntax>();
				EnumMemberSyntax member;
				do
				{
					member = ParseEnumMember();
					members.Add(member);
				} while (member.CommaToken != null && PeekToken().Type != TokenType.RBrace);
			}
			SyntaxToken closeBrace = EatToken(TokenType.RBrace);

			return new EnumSyntax(attribs, enumToken, name, openTemplate, templateParams, closeBrace, colon, type, openBrace, members, closeBrace);
		}

		UnitTestSyntax ParseUnitTest()
		{
			SyntaxToken unitTest = EatToken(TokenType.UnitTest);
			IdentifierNameSyntax name = PeekToken().Type == TokenType.Identifier ? ParseIdentifier(null) : null;
			BlockStatementSyntax body = ParseBlock();

			return new UnitTestSyntax(unitTest, name, body);
		}

		CompileConditionalSyntax ParseCompileConditional()
		{
			SyntaxToken conditional = EatTokenOrNull(TokenType.Debug) ?? EatToken(TokenType.Version);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			SimpleExpressionSyntax expression = ParseExpression(true);
			SyntaxToken closeParen = EatToken(TokenType.RParen);
			BlockStatementSyntax body = ParseBlock();
			SyntaxToken elseToken = EatTokenOrNull(TokenType.Else);
			StatementSyntax elseBody = elseToken == null ? null : ParseStatement();

			return new CompileConditionalSyntax(conditional, openParen, expression, closeParen, body, elseToken, elseBody);
		}

		AssertSyntax ParseAssert(AttributeSyntax attribs)
		{
			SyntaxToken assert = EatToken(TokenType.Assert);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			List<ArgumentSyntax> arguments = ParseArguments();
			SyntaxToken closeParen = EatToken(TokenType.RParen);
			SyntaxToken semicolon = EatToken(TokenType.Semicolon);

			return new AssertSyntax(attribs, assert, openParen, arguments, closeParen, semicolon);
		}

		#endregion

		#region Expression parsing

		SimpleExpressionSyntax ParseExpression(bool parseSimple, AttributeSyntax attribs = null, bool allowCommaExpression = false, SimpleExpressionSyntax left = null, TokenType[] stopTokenType = null)
		{
			bool run = true;
			while (run && (stopTokenType == null || !stopTokenType.Contains(PeekToken().Type)))
			{
				if (PeekToken(1).Type == TokenType.ColonColon)
				{
					left = ParseNameList(attribs, allowCommaExpression);
				}
				else
				{
					switch (PeekToken().Type)
					{
					case TokenType.Identifier:
					case TokenType.Self:
					case TokenType.SelfType:
					{
						if (left == null)
							left = ParseNameList(attribs, allowCommaExpression);
						else
							run = false;
						break;
					}
					case TokenType.Comma:
					{
						if (allowCommaExpression)
							left = ParseCommaExpression(left, stopTokenType);
						else
							run = false;
						break;
					}
					case TokenType.Mixin:
					{
						left = ParseMixin();
						break;
					}
					case TokenType.I8Literal:
					case TokenType.I16Literal:
					case TokenType.I32Literal:
					case TokenType.I64Literal:
					case TokenType.U8Literal:
					case TokenType.U16Literal:
					case TokenType.U32Literal:
					case TokenType.U64Literal:
					case TokenType.F32Literal:
					case TokenType.F64Literal:
					case TokenType.CharLiteral:
					case TokenType.StringLiteral:
					case TokenType.True:
					case TokenType.False:
					case TokenType.Null:
					case TokenType.Void:
					{
						left = new LiteralExpressionSyntax(PeekToken());
						NextToken();
						break;
					}
					case TokenType.Exclaim:
					{
						if (left is NameListSyntax leftList)
						{
							NameListElemSyntax last = leftList.Names.Last();
							IdentifierNameSyntax iden;
							{
								if (last.Name is QualifiedNameSyntax qualifiedName)
									iden = qualifiedName.Right as IdentifierNameSyntax;
								else
									iden = last.Name as IdentifierNameSyntax;
							}

							TemplateNameSyntax templateName = ParseTemplateName(null, iden?.Identifier);
							{
								if (last.Name is QualifiedNameSyntax qualifiedName)
									qualifiedName.Right = templateName;
								else
									last.Name = templateName;
							}
						}
						else
						{
							left = ParsePrefixExpression();
						}

						break;
					}
					case TokenType.PlusPlus:
					case TokenType.MinusMinus:
					{
						if (left != null)
							left = ParsePostfixExpression(left);
						else
							left = ParsePrefixExpression();
						break;
					}
					case TokenType.Plus:
					case TokenType.Minus:
					case TokenType.Asterisk:
					case TokenType.And:
					case TokenType.Tilde:
					{
						if (left != null)
							left = ParseBinaryExpression(left);
						else
							left = ParsePrefixExpression();
						break;
					}
					case TokenType.Slash:
					case TokenType.Modulo:
					case TokenType.Or:
					case TokenType.OrOr:
					case TokenType.AndAnd:
					case TokenType.Caret:
					case TokenType.CaretCaret:
					case TokenType.DotDot:
					case TokenType.LessLess:
					case TokenType.GreaterGreater:
					case TokenType.GreaterGreaterGreater:
					case TokenType.EqualsEquals:
					case TokenType.ExclaimEquals:
					case TokenType.Less:
					case TokenType.LessEquals:
					case TokenType.Greater:
					case TokenType.GreaterEquals:
					case TokenType.QuestionQuestion:
					{
						left = ParseBinaryExpression(left);
						break;
					}
					case TokenType.Equals:
					case TokenType.PlusEquals:
					case TokenType.MinusEquals:
					case TokenType.AsteriskEquals:
					case TokenType.SlashEquals:
					case TokenType.ModuloEquals:
					case TokenType.AndEquals:
					case TokenType.OrEquals:
					case TokenType.CaretEquals:
					case TokenType.LessLessEquals:
					case TokenType.GreaterGreaterEquals:
					case TokenType.GreaterGreaterGreaterEquals:
					case TokenType.TildeEquals:
					case TokenType.CaretCaretEquals:
					{
						left = ParseAssignExpression(left);
						break;
					}
					case TokenType.Question:
					{
						left = ParseTernaryExpression(left);
						break;
					}
					case TokenType.Bool:
					case TokenType.Byte:
					case TokenType.UByte:
					case TokenType.Short:
					case TokenType.UShort:
					case TokenType.Int:
					case TokenType.UInt:
					case TokenType.Long:
					case TokenType.ULong:
					case TokenType.ISize:
					case TokenType.USize:
					case TokenType.Float:
					case TokenType.Double:
					case TokenType.Char:
					case TokenType.WChar:
					case TokenType.Rune:
					case TokenType.String:
					{
						ErrorSystem.AstError(PeekToken().FullSpan, "Unexpected token");
						NextToken();
						run = false;
						break;
					}
					case TokenType.LParen:
					{
						if (left != null)
						{
							if (left is NameSyntax names)
							{
								left = ParseFunctionCall(names);
							}
						}
						else
						{
							left = ParseBracketedExpression();
						}

						break;
					}
					case TokenType.LBracket:
					{
						if (left != null)
						{
							left = ParseIndexSlice(left);
						}
						else
						{
							left = ParseArrayLiteral();
						}

						break;
					}
					case TokenType.LBrace:
					{
						if (left is NameSyntax nameSyntax)
						{
							left = ParseStructInitializer(nameSyntax);
						}
						else
						{
							ErrorSystem.AstError(PeekToken().FullSpan, "Unexpected token");
							NextToken();
							run = false;
						}

						break;
					}
					case TokenType.As:
					{
						left = ParseCast(left);
						break;
					}
					case TokenType.Transmute:
					{
						left = ParseTransmute(left);
						break;
					}
					case TokenType.UUCCast:
					{
						left = ParseCCast(left);
						break;
					}
					case TokenType.Is:
					{
						left = ParseIs();
						break;
					}
					case TokenType.New:
					{
						left = ParseNew();
						break;
					}
					case TokenType.Delete:
					{
						left = ParseDelete();
						break;
					}
					case TokenType.ColonEquals:
					case TokenType.Colon:
					{
						if (left is NameSyntax names)
						{
							left = ParseDeclaration(attribs, names);
						}
						else
						{
							ErrorSystem.AstError(PeekToken().FullSpan, "Unexpected token");
							NextToken();
						}

						break;
					}
					case TokenType.DblArrow:
					{
						left = ParseClosure(left);
						break;
					}
					case TokenType.SKFile:
					case TokenType.SKFileFullPath:
					case TokenType.SKPackage:
					case TokenType.SKModule:
					case TokenType.SKNamespace:
					case TokenType.SKLine:
					case TokenType.SKFunc:
					case TokenType.SKPrettyFunc:
					{
						left = new SpecialKeywordExpressionSyntax(PeekToken());
						NextToken();
						break;
					}
					case TokenType.Semicolon:
					case TokenType.RBrace:
					case TokenType.RBracket:
					case TokenType.RParen:
					{
						run = false;
						break;
					}
					case TokenType.AlignOf:
					case TokenType.Sizeof:
					case TokenType.Typeid:
					{
						left = ParseIntrinsic();
						break;
					}
					case TokenType.HashBrace:
					{
						left = ParseTypeExpression();
						break;
					}
					default:
						ErrorSystem.AstError(PeekToken().FullSpan, "Unexpected token");
						NextToken();
						run = false;
						break;
					}
				}
			}

			if (left == null)
				left = new EmptyExpressionSyntax(PeekToken().FullSpan.Start);

			if (parseSimple)
				return left;

			SyntaxToken semicolon = EatToken(TokenType.Semicolon);
			return new ExpressionSyntax(left, semicolon);
		}

		CommaExpressionSyntax ParseCommaExpression(SimpleExpressionSyntax left, TokenType[] stopTokenTypes)
		{
			List<CommaExpressionElemSyntax> expressions = new List<CommaExpressionElemSyntax>();
			SyntaxToken comma = null;
			CommaExpressionElemSyntax commaExpr = null;
			if (left != null)
			{
				if (left is CommaExpressionSyntax tmpCommaExpr)
				{
					comma = EatToken(TokenType.Comma);
					tmpCommaExpr.Expressions.Last().CommaToken = comma;
				}
				else
				{
					comma = EatToken(TokenType.Comma);
					commaExpr = new CommaExpressionElemSyntax(left, comma);
					expressions.Add(commaExpr);
				}
			}

			SimpleExpressionSyntax expr;
			do
			{
				expr = ParseExpression(true, stopTokenType: stopTokenTypes);
				comma = EatTokenOrNull(TokenType.Comma);
				commaExpr = new CommaExpressionElemSyntax(expr, comma);
				expressions.Add(commaExpr);

			} while (commaExpr.CommaToken != null);

			{
				if (left != null && left is CommaExpressionSyntax tmpCommaExpr)
				{
					tmpCommaExpr.Expressions.AddRange(expressions);
					return tmpCommaExpr;
				}
			}
			return new CommaExpressionSyntax(expressions);
		}

		SimpleExpressionSyntax ParseTernaryExpression(SimpleExpressionSyntax left)
		{
			SyntaxToken question = EatToken(TokenType.Question);
			SimpleExpressionSyntax then = ParseExpression(true);
			SyntaxToken colon = EatToken(TokenType.Colon);
			SimpleExpressionSyntax elseExpr = ParseExpression(true);

			SimpleExpressionSyntax binExpr = new TernaryExpressionSyntax(left, question, then, colon, elseExpr);
			return SyntaxNodeHelpers.FixPrecedenceOrder(binExpr);
		}

		SimpleExpressionSyntax ParseBinaryExpression(SimpleExpressionSyntax left)
		{
			SyntaxToken operatorToken = PeekToken();
			NextToken();
			SimpleExpressionSyntax right = ParseExpression(true);

			SimpleExpressionSyntax binExpr = new BinaryExpressionSyntax(left, operatorToken, right);
			return SyntaxNodeHelpers.FixPrecedenceOrder(binExpr);
		}

		AssignExpressionSyntax ParseAssignExpression(SimpleExpressionSyntax left)
		{
			SyntaxToken assign = PeekToken();
			NextToken();
			SimpleExpressionSyntax right = ParseExpression(true, allowCommaExpression: true);

			return new AssignExpressionSyntax(left, assign, right);
		}

		SimpleExpressionSyntax ParsePrefixExpression()
		{
			SyntaxToken operatorToken = PeekToken();
			NextToken();
			SimpleExpressionSyntax right = ParseExpression(true);

			PrefixExpressionSyntax expr = new PrefixExpressionSyntax(operatorToken, right);
			return SyntaxNodeHelpers.FixPrecedenceOrder(expr);
		}

		PostfixExpressionSyntax ParsePostfixExpression(SimpleExpressionSyntax left)
		{
			SyntaxToken operatorToken = PeekToken();
			NextToken();

			return new PostfixExpressionSyntax(left, operatorToken);
		}

		BracketedExpressionSyntax ParseBracketedExpression()
		{
			SyntaxToken openParen = EatToken(TokenType.LParen);
			SimpleExpressionSyntax expr = ParseExpression(true, allowCommaExpression: true);
			SyntaxToken closeParen = EatToken(TokenType.RParen);

			return new BracketedExpressionSyntax(openParen, expr, closeParen);
		}

		SimpleExpressionSyntax ParseFunctionCall(NameSyntax names)
		{
			NameSyntax tmp = names;
			if (names is NameListSyntax nameList)
				tmp = nameList.Names.Last().Name;

			QualifiedNameSyntax name;
			if (tmp is QualifiedNameSyntax)
				name = tmp as QualifiedNameSyntax;
			else
				name = new QualifiedNameSyntax(null, null, null, tmp as SimpleNameSyntax);

			SyntaxToken openParen = EatToken(TokenType.LParen);
			List<ArgumentSyntax> arguments = ParseArguments();
			SyntaxToken closeParen = EatToken(TokenType.RParen);

			FunctionCallExpressionSyntax call = new FunctionCallExpressionSyntax(name, openParen, arguments, closeParen);
			
			if (names is NameListSyntax exprNameList && exprNameList.Names.Count > 1)
			{
				List<NameListElemSyntax> exprNames = exprNameList.Names;
				List<CommaExpressionElemSyntax> commaList = new List<CommaExpressionElemSyntax>();

				SyntaxToken comma = exprNameList.Names[exprNameList.Names.Count - 2].CommaToken;
				exprNames[exprNames.Count - 2].CommaToken = null;
				exprNames.RemoveAt(exprNames.Count - 1);

				commaList.Add(new CommaExpressionElemSyntax(exprNameList, comma));
				commaList.Add(new CommaExpressionElemSyntax(call, null));
				return new CommaExpressionSyntax(commaList);
			}

			return call;
		}

		MixinExpressionSyntax ParseMixin()
		{
			SyntaxToken mixin = EatToken(TokenType.Mixin);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			SimpleExpressionSyntax expr = ParseExpression(true);
			SyntaxToken closeParen = EatToken(TokenType.RParen);

			return new MixinExpressionSyntax(mixin, openParen, expr, closeParen);
		}

		CastExpressionSyntax ParseCast(SimpleExpressionSyntax left)
		{
			SyntaxToken asToken = EatToken(TokenType.As);
			TypeSyntax type = ParseType();

			return new CastExpressionSyntax(left, asToken, type);
		}

		TransmuteExpressionSyntax ParseTransmute(SimpleExpressionSyntax left)
		{
			SyntaxToken transmute = EatToken(TokenType.Transmute);
			TypeSyntax type = ParseType();

			return new TransmuteExpressionSyntax(left, transmute, type);
		}

		CCastExpressionSyntax ParseCCast(SimpleExpressionSyntax left)
		{
			SyntaxToken cCastToken = EatToken(TokenType.UUCCast);
			TypeSyntax type = ParseType();

			return new CCastExpressionSyntax(left, cCastToken, type);
		}

		IndexSliceExpressionSyntax ParseIndexSlice(SimpleExpressionSyntax left)
		{
			SyntaxToken openBracket = EatToken(TokenType.LBracket);
			SimpleExpressionSyntax indexSliceExpr = ParseExpression(true, allowCommaExpression: true);
			SyntaxToken closeBracket = EatToken(TokenType.RBracket);

			return new IndexSliceExpressionSyntax(left, openBracket, indexSliceExpr, closeBracket);
		}

		IsExpressionSyntax ParseIs()
		{
			SyntaxToken isToken = EatToken(TokenType.Is);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			TokenType[] stopTypes = {TokenType.EqualsEquals, TokenType.Colon};
			SimpleExpressionSyntax expr = ParseExpression(true, stopTokenType: stopTypes);
			SyntaxToken equalsEquals = EatTokenOrNull(TokenType.EqualsEquals);
			SyntaxToken colon = EatTokenOrNull(TokenType.Colon);
			TypeSyntax type = ParseType();
			SyntaxToken closeParen = EatToken(TokenType.RParen);

			return new IsExpressionSyntax(isToken, openParen, expr, equalsEquals, colon, type, closeParen);
		}

		NewExpressionSyntax ParseNew()
		{
			SyntaxToken newToken = EatToken(TokenType.New);

			SyntaxToken openNewParen = EatTokenOrNull(TokenType.LParen);
			List<ArgumentSyntax> newArguments = openNewParen == null ? null : ParseArguments();
			SyntaxToken closeNewParen = openNewParen == null ? null : EatToken(TokenType.RParen);

			TypeSyntax type = ParseType();

			SyntaxToken openTypeParen = EatTokenOrNull(TokenType.LParen);
			List<ArgumentSyntax> typeArguments = openTypeParen == null ? null : ParseArguments();
			SyntaxToken closeTypeParen = openTypeParen == null ? null : EatToken(TokenType.RParen);

			SyntaxToken openArrayBracket = EatTokenOrNull(TokenType.LBracket);
			SimpleExpressionSyntax arrayExpr = openArrayBracket == null ? null : ParseExpression(true);
			SyntaxToken closeArrayBracket = openArrayBracket == null ? null : EatToken(TokenType.RBracket);

			return new NewExpressionSyntax(newToken, openNewParen, newArguments, closeNewParen, type, openTypeParen, typeArguments, closeTypeParen, openArrayBracket, arrayExpr, closeArrayBracket);
		}

		DeleteExpressionSyntax ParseDelete()
		{
			SyntaxToken delete = EatToken(TokenType.Delete);
			SimpleExpressionSyntax expression = ParseExpression(true);

			return new DeleteExpressionSyntax(delete, expression);
		}

		ClosureCaptureSyntax ParseClosureCapture()
		{
			SyntaxToken equalsToken = EatTokenOrNull(TokenType.Equals);
			SyntaxToken andToken = EatTokenOrNull(TokenType.And);
			NameSyntax name = null;
			if (PeekToken().Type != TokenType.Comma && PeekToken().Type != TokenType.RBracket)
				name = ParseName(null);
			SyntaxToken comma = EatTokenOrNull(TokenType.Comma);

			return new ClosureCaptureSyntax(equalsToken, andToken, name, comma);
		}

		ClosureSyntax ParseClosure(SimpleExpressionSyntax identifiers)
		{
			SyntaxToken doubleArrow = EatToken(TokenType.DblArrow);
			SyntaxToken openBracket = EatTokenOrNull(TokenType.LBracket);
			List<ClosureCaptureSyntax> captures = null;
			SyntaxToken closeBracket = null;
			if (openBracket != null)
			{
				captures = new List<ClosureCaptureSyntax>();
				ClosureCaptureSyntax capture;
				do
				{
					capture = ParseClosureCapture();
					captures.Add(capture);
				} while (capture.CommaToken != null);
				closeBracket = EatToken(TokenType.RBracket);
			}

			StatementSyntax body = ParseStatement();

			return new ClosureSyntax(identifiers, doubleArrow, openBracket, captures, closeBracket, body);
		}

		SimpleArrayLiteralExpressionSyntax ParseArrayLiteral()
		{
			SyntaxToken openBracket = EatToken(TokenType.LBracket);
			SyntaxToken closeBracket = EatTokenOrNull(TokenType.RBracket);

			if (closeBracket != null)
				return new ArrayLiteralExpressionSyntax(openBracket, null, closeBracket);

			SimpleExpressionSyntax expr = ParseExpression(true);
			if (PeekToken().Type == TokenType.Colon)
			{
				List<AssocArrayLiteralElemSyntax> elements = new List<AssocArrayLiteralElemSyntax>();

				SyntaxToken colon = EatTokenOrNull(TokenType.Colon);
				SimpleExpressionSyntax value = ParseExpression(true);
				SyntaxToken comma = EatTokenOrNull(TokenType.Comma);
				AssocArrayLiteralElemSyntax elem = new AssocArrayLiteralElemSyntax(expr, colon, value, comma);
				elements.Add(elem);

				while (comma != null)
				{
					expr = ParseExpression(true);
					colon = EatTokenOrNull(TokenType.Colon);
					value = ParseExpression(true);
					comma = EatTokenOrNull(TokenType.Comma);
					elem = new AssocArrayLiteralElemSyntax(expr, colon, value, comma);
					elements.Add(elem);
				}

				closeBracket = EatToken(TokenType.RBracket);
				return new AssocArrayLiteralExpressionSyntax(openBracket, elements, closeBracket);
			}
			else
			{
				List<ArrayLiteralElemSyntax> elements = new List<ArrayLiteralElemSyntax>();

				SyntaxToken comma = EatTokenOrNull(TokenType.Comma);
				ArrayLiteralElemSyntax elem = new ArrayLiteralElemSyntax(expr, comma);
				elements.Add(elem);

				while (comma != null)
				{
					expr = ParseExpression(true);
					comma = EatTokenOrNull(TokenType.Comma);
					elem = new ArrayLiteralElemSyntax(expr, comma);
					elements.Add(elem);
				}

				closeBracket = EatToken(TokenType.RBracket);
				return new ArrayLiteralExpressionSyntax(openBracket, elements, closeBracket);
			}
		}

		AggregateInitializerSyntax ParseStructInitializer(NameSyntax left)
		{
			SyntaxToken openBrace = EatToken(TokenType.LBrace);
			SyntaxToken closeBrace = EatTokenOrNull(TokenType.RBrace);

			if (closeBrace != null)
				return new AggregateInitializerSyntax(left, openBrace, null, closeBrace);

			List<AggregateInitializerElemSyntax> elements = new List<AggregateInitializerElemSyntax>();
			AggregateInitializerElemSyntax elem;
			do
			{
				IdentifierNameSyntax identifier = null;
				SyntaxToken equals = null;

				if (PeekToken(1).Type == TokenType.Colon)
				{
					identifier = ParseAsIdentifier(null);
					equals = EatToken(TokenType.Colon);
				}

				SimpleExpressionSyntax expr = ParseExpression(true);
				SyntaxToken comma = EatTokenOrNull(TokenType.Comma);
				elem = new AggregateInitializerElemSyntax(identifier, equals, expr, comma);
				elements.Add(elem);

			} while (elem.CommaToken != null);

			closeBrace = EatToken(TokenType.RBrace);
			return new AggregateInitializerSyntax(left, openBrace, elements, closeBrace);
		}
		
		IntrinsicSyntax ParseIntrinsic()
		{
			SyntaxToken assert = EatTokenOrNull(TokenType.AlignOf) ??
			                     EatTokenOrNull(TokenType.Sizeof) ??
			                     EatToken(TokenType.Typeid);
			SyntaxToken openParen = EatToken(TokenType.LParen);
			SimpleExpressionSyntax expr = ParseExpression(true);
			SyntaxToken closeParen = EatToken(TokenType.RParen);

			return new IntrinsicSyntax(assert, openParen, expr, closeParen);
		}

		TypeExpressionSyntax ParseTypeExpression()
		{
			SyntaxToken openExpr = EatToken(TokenType.HashBrace);
			TypeSyntax type = ParseType();
			SyntaxToken closeExpr = EatToken(TokenType.RBrace);

			return new TypeExpressionSyntax(openExpr, type, closeExpr);
		}
		
		#endregion
		
		#region Utility

		SyntaxToken PeekToken(int offset = 0)
		{
			offset += _index;
			if (offset >= _tokens.Count)
				return new SyntaxToken(new TextSpan(-1, -1), TokenType.Unknown, "");
			return _tokens[offset];
		}

		SyntaxToken NextToken()
		{
			++_index;
			if (_index >= _tokens.Count)
				return new SyntaxToken(new TextSpan(-1, -1), TokenType.Unknown, "");
			return _tokens[_index];
		}

		SyntaxToken EatToken(TokenType type)
		{
			SyntaxToken token = PeekToken();

			if (token.Type == type)
			{
				NextToken();
				return token;
			}
			else
			{
				ErrorSystem.AstError(token.FullSpan, $"Unexpected token '{token.Type}', expected '{type}'");
				return new SyntaxToken(new TextSpan(token.Span.Start, 0), TokenType.Missing, "");
			}
		}

		SyntaxToken EatToken(TokenType type0, TokenType type1)
		{
			SyntaxToken token = PeekToken();

			if (token.Type == type0 || token.Type == type1)
			{
				NextToken();
				return token;
			}
			else
			{
				ErrorSystem.AstError(token.FullSpan, $"Unexpected token '{token.Type}', expected '{type0}' or '{type1}'");
				return new SyntaxToken(new TextSpan(token.Span.Start, 0), TokenType.Missing, "");
			}
		}

		SyntaxToken EatTokenOrNull(TokenType type)
		{
			SyntaxToken token = PeekToken();

			if (token.Type == type)
			{
				NextToken();
				return token;
			}
			else
			{
				return null;
			}
		}

		#endregion
	}
}
