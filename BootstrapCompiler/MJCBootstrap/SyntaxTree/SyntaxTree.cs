﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend;

namespace MJC.SyntaxTree
{
	public class SyntaxTree
	{
		public CompilationUnitSyntax CompilationUnit { get; }
		public SymbolTable Symbols;
		public FileImports FileImports;
		public List<string> ForeignLibs = new List<string>();
		public ErrorSystemMetadata ErrorMeta;
		public string FileName; // Without extension

		public string PackageName;
		public string ModuleName;

		public Dictionary<ScopeVariable, VTable> VTables;
		public ILCompUnit CompUnit;

		public SyntaxTree(CompilationUnitSyntax compilationUnit)
		{
			CompilationUnit = compilationUnit;
		}

		public override string ToString()
		{
			return CompilationUnit.ToString();
		}

		public string ToFullString()
		{
			return CompilationUnit.ToFullString();
		}
	}
}
