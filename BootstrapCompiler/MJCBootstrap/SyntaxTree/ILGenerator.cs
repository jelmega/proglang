﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;
using MJC.ILBackend;
using MJC.ILBackend.General;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC.SyntaxTree
{
	class ILGenerator : SyntaxWalker
	{
		private ILBuilder _builder = new ILBuilder();
		private ILOperand _lastValue;
		private List<ILOperand> _lastValueGroup;
		private Dictionary<ScopeVariable, ILOperand> _varMapping = new Dictionary<ScopeVariable, ILOperand>();
		private HashSet<ScopeVariable> _structVars = new HashSet<ScopeVariable>();
		private HashSet<ScopeVariable> _unionVars = new HashSet<ScopeVariable>();
		private HashSet<ScopeVariable> _enumVars = new HashSet<ScopeVariable>();
		private bool _idenAsVar;
		private bool _inFunction;
		private bool _isGlobalVar = true;
		private bool _getIdenAddr = false;
		private Dictionary<ScopeVariable, ILOperand> _funcParams = new Dictionary<ScopeVariable, ILOperand>();
		private SymbolType _returnType;

		private bool _isTemplateArg = false;
		private bool _inImplTemplate = false;
		private Dictionary<SymbolType, SymbolType> _templateTypeMapping = new Dictionary<SymbolType, SymbolType>();
		private int _templateParamValueSpecId = 0;
		private int _templateParamValueDefId = 0;
		private int _arraySizeId = 0;

		private Scope _funcScope;
		private bool _parseQualifiedLast = true;

		private Dictionary<string, CompileAttribute> _astCompileAttribs = new Dictionary<string, CompileAttribute>();

		public string OutputFile;

		public CompilerContext CompilerContext;
		public ILCompUnit CompUnit;

		public override void Visit(SyntaxTree tree)
		{
			_builder.BeginRaw();

			foreach (KeyValuePair<ScopeVariable, VTable> pair in tree.VTables)
			{
				_builder.AddVTable(pair.Value);
			}

			CompilerContext.SetCurFileImportTable(tree.FileImports);
			base.Visit(tree);

			CompUnit = _builder.End();

			foreach (string module in tree.FileImports.UsedModules)
			{
				ILCompUnit import = CompilerContext.Imports.Modules[module].ILCompUnit;
				CompUnit.Imports.Add(import);
			}

			ILWriter.Write(CompUnit, OutputFile);
			tree.CompUnit = CompUnit;
			// Clear up some memory
			_varMapping.Clear();
		}

		protected override void VisitTypeAlias(TypeAliasSyntax syntax)
		{
			// Don't visit the type alias, we don't want an instance of the underlying type with param types as actual types
			//base.VisitTypeAlias(syntax);
			_builder.BuildAlias(syntax.Context.Symbol.MangledName, syntax.FullSpan);
		}
		
		protected override void VisitArgument(ArgumentSyntax syntax)
		{
			if (_isTemplateArg && !(syntax.Expression is TypeExpressionSyntax))
			{
				// Skip if the argument is a template param from an impl declaration, instead of an actual value
				if (_inImplTemplate && syntax.Expression is IdentifierNameSyntax idenSyntax)
				{
					Symbol tmp = idenSyntax.Context.Symbol;
					if (tmp.Kind == SymbolKind.TemplateParam)
						return;
				}

				// Get the current basic block to reset the builder after template value initializer
				ILBasicBlock prevBB = _builder.GetCurrentBasicBlock();

				// Generate initializer function
				ILAttributes attribs = new ILAttributes(ILLinkage.Hidden);
				string funcName = syntax.Context.InitFunc;
				FunctionSymbolType funcType = SymbolType.FunctionType(null, null, syntax.Expression.Context.Type);

				ScopeVariable scopeVar = new ScopeVariable(new Scope(), new NameIdentifier(funcName));
				_builder.BeginFunction(scopeVar, funcName, funcType, attribs, null, false, syntax.FullSpan);
				_builder.CreateBasicBlock("entry");
				
				VisitSimpleExpression(syntax.Expression);
				_builder.BuildReturn(_lastValue, null);

				_builder.EndFunction();

				// Reset basic block to continue the outer func
				_builder.SetCurrentBasicBlock(prevBB);
			}
			else
			{
				base.VisitArgument(syntax);
			}
		}

		protected override void VisitArrayLiteral(ArrayLiteralExpressionSyntax syntax)
		{
			base.VisitArrayLiteral(syntax);
		}

		protected override void VisitArrayType(ArrayTypeSyntax syntax)
		{
			//base.VisitArrayType(syntax);
			VisitType(syntax.BaseType);

			ArraySymbolType arrType = syntax.Context.Type as ArraySymbolType;

			if (syntax.SizeExpression != null)
			{
				string funcName = $"__asi_{_arraySizeId}";
				++_arraySizeId;

				// Get the current basic block to reset the builder after template value initializer
				ILBasicBlock prevBB = _builder.GetCurrentBasicBlock();
				ILOperand prevLastVal = _lastValue;

				ILAttributes attribs = new ILAttributes(ILLinkage.Hidden);

				FunctionSymbolType funcType = SymbolType.FunctionType(null, null, syntax.SizeExpression.Context.Type);
				ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(funcName));

				_builder.BeginFunction(scopeVar, funcName, funcType, attribs, null, false, syntax.SizeExpression.FullSpan);
				_builder.CreateBasicBlock("entry");

				VisitSimpleExpression(syntax.SizeExpression);
				_builder.BuildReturn(_lastValue, null);

				_builder.EndFunction();

				_lastValue = prevLastVal;

				// Reset basic block to continue the outer func
				_builder.SetCurrentBasicBlock(prevBB);

				arrType.ArraySizeFunc = funcName;
			}

			_builder.BuildType(arrType);
		}

		protected override void VisitAssert(AssertSyntax syntax)
		{
			base.VisitAssert(syntax);
		}

		protected override void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			bool prev = _idenAsVar;
			_idenAsVar = true;

			// Left
			_getIdenAddr = true;
			VisitSimpleExpression(syntax.Left);
			ILOperand lOp = _lastValue;
			SymbolType lType = lOp.Type;
			SymbolType baseType = lType;
			if (baseType is MemoryLocSymbolType memoryLoc)
			{
				baseType = memoryLoc.BaseType;
			}

			// Right
			_getIdenAddr = false;
			VisitSimpleExpression(syntax.Right);
			ILOperand rOp = _lastValue;
			SymbolType rType = rOp.Type;

			_idenAsVar = prev;

			ILOperand opRef = null;

			// op type will be: =, +=, -=, etc
			ILOpType opType = ILHelpers.GetILOpType(syntax.AssignToken.Text);
			if (baseType is BuiltinSymbolType lBuiltin && rType is BuiltinSymbolType rBuiltin)
			{
				if (rBuiltin != baseType)
				{
					rOp = CastOperand(rOp, baseType, ILOpType.Static, syntax.Right.FullSpan);
				}

				List<SymbolType> paramTypes = new List<SymbolType> { lType, baseType };
				opRef = _builder.BuildOperatorRef(ILOpLoc.Infix, opType, paramTypes, baseType, syntax.FullSpan);
			}
			else if (baseType is PointerSymbolType && rType is PointerSymbolType)
			{
				_builder.BuildAssign(rOp, lOp, syntax.FullSpan);
			}
			else
			{
				List<SymbolType> paramTypes = new List<SymbolType> { lType, rType };
				opRef = _builder.BuildOperatorRef(ILOpLoc.Infix, opType, paramTypes, baseType, syntax.FullSpan);
			}

			if (opRef != null)
			{
				ILOperand res = _builder.BuildCall(opRef, new List<ILOperand> {lOp, rOp}, baseType, syntax.FullSpan);
				_lastValue = res;
			}

		}

		protected override void VisitAssocArrayLiteral(AssocArrayLiteralExpressionSyntax syntax)
		{
			base.VisitAssocArrayLiteral(syntax);
		}

		protected override void VisitAtAttribute(AtAttributeSyntax syntax)
		{
			base.VisitAtAttribute(syntax);
		}
		
		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			base.VisitAttributedType(syntax);
		}

		protected override void VisitAttributes(AttributeSyntax syntax)
		{
			base.VisitAttributes(syntax);
		}

		protected override void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			SymbolType lType = GetSpecializedType(syntax.Left.Context.Type);
			SymbolType rType = GetSpecializedType(syntax.Right.Context.Type);

			bool prev = _idenAsVar;
			_idenAsVar = true;

			// Special logic for: bool || bool, and: bool && bool
			// TODO: refactor when phi-like system is added to IL
			if (lType is BuiltinSymbolType lbuiltin && lbuiltin.Builtin == BuiltinTypes.Bool &&
			    rType is BuiltinSymbolType rbuiltin && rbuiltin.Builtin == BuiltinTypes.Bool)
			{
				bool isLogicAnd = syntax.OperatorToken.Type == TokenType.AndAnd;
				if (isLogicAnd || syntax.OperatorToken.Type == TokenType.OrOr)
				{
					// Left
					VisitSimpleExpression(syntax.Left);
					ILOperand lOp = _lastValue;
					if (lOp.Type is MemoryLocSymbolType)
					{
						lOp = _builder.BuildLoad(lOp, syntax.Left.FullSpan);
					}

					string resName = "b_" + lOp.Iden + "_br_res_f_" + _builder.GetCurrentBlockLabel();
					string trueBranch = "b_" + lOp.Iden + "_br_true_f_" + _builder.GetCurrentBlockLabel();
					string falseBranch = "b_" + lOp.Iden + "_br_false_f_" + _builder.GetCurrentBlockLabel();
					string endBranch = "b_" + lOp.Iden + "_br_end_f_" + _builder.GetCurrentBlockLabel();

					// Create result value and store the left result, for when right isn't processed
					SymbolType boolType = SymbolType.BuiltinType(BuiltinTypes.Bool);
					ILOperand resOp = _builder.BuildStackVariable(boolType, resName, syntax.Left.FullSpan);
					_builder.BuildAssign(lOp, resOp, syntax.Left.FullSpan);

					if (isLogicAnd) // both need to be true
					{
						_builder.BuildCondBranch(lOp, trueBranch, endBranch, syntax.Left.FullSpan);
						_builder.CreateBasicBlock(trueBranch);
					}
					else // either can be true
					{
						_builder.BuildCondBranch(lOp, endBranch, falseBranch, syntax.Left.FullSpan);
						_builder.CreateBasicBlock(falseBranch);
					}

					// Right
					VisitSimpleExpression(syntax.Right);
					ILOperand rOp = _lastValue;
					if (rOp.Type is MemoryLocSymbolType)
					{
						rOp = _builder.BuildLoad(rOp, syntax.Right.FullSpan);
					}
					_builder.BuildAssign(rOp, resOp, syntax.Right.FullSpan);

					_builder.BuildBranch(endBranch, syntax.Right.FullSpan);
					_builder.CreateBasicBlock(endBranch);
					ILOperand retOp = _builder.BuildLoad(resOp, syntax.Right.FullSpan);
					_lastValue = retOp;
					return;
				}
			}
			else
			{
				SymbolType resType = GetSpecializedType(syntax.Context.Type);

				// Left
				VisitSimpleExpression(syntax.Left);
				ILOperand lOp = _lastValue;
				if (lOp.Type is MemoryLocSymbolType)
				{
					lOp = _builder.BuildLoad(lOp, syntax.Left.FullSpan);
				}

				// Right
				VisitSimpleExpression(syntax.Right);
				ILOperand rOp = _lastValue;
				if (rOp.Type is MemoryLocSymbolType)
				{
					rOp = _builder.BuildLoad(rOp, syntax.Right.FullSpan);
				}

				_idenAsVar = prev;

				ILOperand opRef;

				// op type will be: +, -, etc
				ILOpType opType = ILHelpers.GetILOpType(syntax.OperatorToken.Text);
				if (lType is BuiltinSymbolType lBuiltin && rType is BuiltinSymbolType rBuiltin)
				{
					SymbolType baseType = lBuiltin.GetCommonType(rBuiltin);

					if (lBuiltin != baseType)
					{
						lOp = CastOperand(lOp, baseType, ILOpType.Static, syntax.Left.FullSpan);
					}

					if (rBuiltin != baseType)
					{
						rOp = CastOperand(rOp, baseType, ILOpType.Static, syntax.Right.FullSpan);
					}

					List<SymbolType> paramTypes = new List<SymbolType> {baseType, baseType};
					opRef = _builder.BuildOperatorRef(ILOpLoc.Infix, opType, paramTypes, resType, syntax.FullSpan);
				}
				else
				{
					List<SymbolType> paramTypes = new List<SymbolType> {lType, rType};
					opRef = _builder.BuildOperatorRef(ILOpLoc.Infix, opType, paramTypes, resType, syntax.FullSpan);
				}

				ILOperand res = _builder.BuildCall(opRef, new List<ILOperand> {lOp, rOp}, resType, syntax.FullSpan);

				_lastValue = res;
			}
		}

		protected override void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			base.VisitBlockStatement(syntax);
		}

		protected override void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			base.VisitBracketedExpression(syntax);

			if (syntax.Expression is CommaExpressionSyntax)
			{
				_lastValue = _builder.BuildTuplePack(_lastValueGroup, syntax.Context.Type as TupleSymbolType, syntax.FullSpan);
			}
		}

		protected override void VisitBreak(BreakSyntax syntax)
		{
			base.VisitBreak(syntax);
		}

		protected override void VisitSwitchCase(SwitchCaseSyntax syntax)
		{
			base.VisitSwitchCase(syntax);
		}

		protected override void VisitCastExpression(CastExpressionSyntax syntax)
		{
			base.VisitCastExpression(syntax);
			
			ILOperand expr = _lastValue;
			SymbolType exprType = expr.Type;
			SymbolType castType = GetSpecializedType(syntax.Type.Context.Type);

			if (exprType == castType)
				return;

			if (expr.Type is MemoryLocSymbolType)
			{
				expr = _builder.BuildLoad(expr, syntax.FullSpan);
			}

			_lastValue = CastOperand(expr, castType, ILOpType.Static, syntax.FullSpan);
		}

		protected override void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			base.VisitCCastExpression(syntax);

			ILOperand expr = _lastValue;
			SymbolType exprType = expr.Type;
			SymbolType castType = GetSpecializedType(syntax.Type.Context.Type);

			if (exprType == castType)
				return;

			if (expr.Type is MemoryLocSymbolType)
			{
				expr = _builder.BuildLoad(expr, syntax.FullSpan);
			}

			bool isExprPtr = exprType is PointerSymbolType;
			bool isCastPtr = castType is PointerSymbolType;

			if (isExprPtr == isCastPtr)
				_lastValue = CastOperand(expr, castType, ILOpType.Static, syntax.FullSpan);
			else
				_lastValue = _builder.BuildBuiltin("bitcast", new List<ILOperand> { expr }, castType, syntax.FullSpan);
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			// Set the template type mapping
			if (syntax.Type.Context.Type is TemplateInstanceSymbolType instType)
			{
				TemplateInstanceIdentifier instIden = instType.Iden.Name as TemplateInstanceIdentifier;
				foreach (TemplateArgument arg in instIden.Arguments)
				{
					_templateTypeMapping.Add(arg.AssociatedBaseType, arg.Type);
				}
			}
			
			_inImplTemplate = true;
			base.VisitImpl(syntax);
			_inImplTemplate = false;
			_templateTypeMapping.Clear();
		}

		protected override void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			_lastValueGroup = new List<ILOperand>();
			foreach (CommaExpressionElemSyntax elemSyntax in syntax.Expressions)
			{
				SimpleExpressionSyntax simpleExpr = elemSyntax.Expression;
				base.VisitSimpleExpression(simpleExpr);

				_lastValueGroup.Add(_lastValue);
			}
		}

		protected override void VisitCompilationUnit(CompilationUnitSyntax syntax)
		{
			string package = null;
			if (syntax.Package != null)
				package = syntax.Package.Name.ToString();
			string module = syntax.Module.Name.ToString();

			_builder.BuildModuleDirective(package, module);

			// Handle imports
			foreach (string usedModule in CompilerContext.FileImport.UsedModules)
			{
				_builder.BuildImport(usedModule);
			}

			base.VisitCompilationUnit(syntax);
		}

		protected override void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			base.VisitCompileCondition(syntax);
		}
		
		protected override void VisitContinue(ContinueSyntax syntax)
		{
			base.VisitContinue(syntax);
		}
		
		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			_lastValue = null;
			
			if (_inFunction)
			{
				if (syntax.Context.SymbolList != null)
				{
					// TODO
					/*List<ILOperand> unpacked = _builder.BuildTupleUnpack(_lastValue);

					NameListSyntax nameList = syntax.Names as NameListSyntax;
					for (var i = 0; i < syntax.Context.SymbolList.Count; i++)
					{
						Symbol symbol = syntax.Context.SymbolList[i];
						TextSpan elemSpan = nameList.Names[i].FullSpan;
						ILOperand val = _builder.BuildStackVariable(symbol.Type, symbol.Identifier + ".addr", elemSpan);
						_varMapping.Add($"{symbol.IdenScopeVar}", val);

						SymbolType valType = val.Type;
						if (valType is MemoryLocSymbolType memoryLoc)
							valType = memoryLoc.BaseType;

						ILOperand expr = unpacked[i];
						if (valType != expr.Type)
						{
							expr = CastOperand(expr, valType, ILOpType.Static, syntax.Context.Scope);
						}

						_builder.BuildAssign(expr, val);

						TryCreateTemplateInstance(symbol, syntax.Type);
					}*/
				}
				else
				{
					Symbol symbol = syntax.Context.Symbol;
					SymbolType symType = GetSpecializedType(symbol.Type);

					ILOperand val = _builder.BuildStackVariable(symType, symbol.Identifier + ".addr", syntax.Names.FullSpan);
					_varMapping.Add(symbol.ScopeVar, val);

					if (syntax.Type != null)
						VisitType(syntax.Type);
					if (syntax.Initializer != null)
						VisitSimpleExpression(syntax.Initializer);

					bool isVoidInitialized = _lastValue == null && syntax.Initializer?.Context.Type is BuiltinSymbolType initBuiltin && initBuiltin.Builtin == BuiltinTypes.Void;

					if (!isVoidInitialized)
					{
						if (_lastValue == null)
						{
							_lastValue = DefaultInitialize(symType, syntax.Names.FullSpan);
						}

						ILOperand expr = _lastValue;

						// expr should always have a value, if no value is found, semantic analysis did something wrong
						//if (expr != null)
						{
							SymbolType valType = val.Type;
							if (valType is MemoryLocSymbolType memoryLoc)
								valType = memoryLoc.BaseType;

							if (valType != expr.Type)
							{
								expr = CastOperand(expr, valType, ILOpType.Static, syntax.FullSpan);
							}

							_builder.BuildAssign(expr, val, syntax.FullSpan);
						}
					}

				}
			}
			else if (_isGlobalVar)
			{
				if (syntax.Context.SymbolList != null)
				{
					// TODO
				}
				else
				{
					Symbol symbol = syntax.Context.Symbol;
					SymbolType symType = GetSpecializedType(symbol.Type);

					FunctionSymbolType initFuncType = SymbolType.FunctionType(null, null, symType);
					string initFuncName = NameMangling.MangleName(symbol) + "6__init" + NameMangling.MangleType(initFuncType);

					List<ILTemplateParam> templateParams = null;
					// TODO

					ILAttributes funcAttribs = new ILAttributes(ILLinkage.Hidden);

					ScopeVariable scopeVariable = new ScopeVariable(new Scope(symbol.ScopeVar), new NameIdentifier("__init"));
					_builder.BeginFunction(scopeVariable, initFuncName, initFuncType, funcAttribs, templateParams, false, syntax.Initializer?.FullSpan);
					_builder.CreateBasicBlock("entry");

					if (syntax.Initializer != null)
						VisitSimpleExpression(syntax.Initializer);
					else
						_lastValue = DefaultInitialize(symbol.Type, syntax.FullSpan);
					
					_builder.BuildReturn(_lastValue, syntax.Initializer?.FullSpan);

					_builder.EndFunction();
					
					ILAttributes attribs = new ILAttributes();
					attribs.GlobalFlags = GetGlobalFlags(symbol.Attribs, symbol.CompileAttribs);
					attribs.Linkage = ILLinkage.Hidden;
					if (syntax.Context.CompileAttribs.ContainsKey(ForeignCompileAttribute.Id))
						attribs.Linkage = ILLinkage.HiddenExternal;
					_builder.BuildGlobal(symbol.MangledName, symbol.Type, initFuncName, attribs, syntax.FullSpan);

				}
			}

		}

		protected override void VisitDefer(DeferSyntax syntax)
		{
			base.VisitDefer(syntax);
		}

		protected override void VisitDelegate(DelegateSyntax syntax)
		{
			base.VisitDelegate(syntax);

			Symbol sym = syntax.Context.Symbol;
			DelegateSymbolType type = syntax.Context.Type as DelegateSymbolType;

			bool asFncPtr = sym.CompileAttribs?.ContainsKey(FuncPtrAttribute.Id) ?? false;
			_builder.BuildDelegate(sym.MangledName, type.ParamTypes, type.ReturnType, asFncPtr, syntax.FullSpan);
		}

		protected override void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			base.VisitDeleteExpression(syntax);
		}
		
		protected override void VisitDoWhile(DoWhileSyntax syntax)
		{
			Scope subScope = syntax.Context.Scope.GetSubScope(_funcScope);

			string scopeName = subScope.ToString();
			string condName = scopeName + ".cond";
			string loopName = scopeName + ".loop";
			string endName = scopeName + ".end";

			_builder.BuildBranch(loopName, syntax.FullSpan);

			_builder.CreateBasicBlock(loopName);
			base.VisitStatement(syntax.Body);
			_builder.BuildBranch(condName, syntax.Condition.FullSpan);

			_builder.CreateBasicBlock(condName);
			VisitSimpleExpression(syntax.Condition);
			_builder.BuildCondBranch(_lastValue, loopName, endName, syntax.Condition.FullSpan);

			_builder.CreateBasicBlock(endName);
		}

		protected override void VisitEnum(EnumSyntax syntax)
		{
			EnumSymbolType enumType = syntax.Context.Type as EnumSymbolType;
			SymbolType baseType = enumType.BaseType;
			
			Symbol symbol = syntax.Context.Symbol;
			ILAttributes ilAttribs = ILHelpers.GetAttributes(syntax.Context.Symbol, _astCompileAttribs);

			// Get template params, if there are any
			List<ILTemplateParam> templateParams = null;
			if (syntax.Context.Identifier is TemplateDefinitionIdentifier def)
			{
				templateParams = new List<ILTemplateParam>(def.Parameters.Count);
				foreach (TemplateParameter param in def.Parameters)
				{
					string valueSpecFunc = null;
					if (param.ValueSpecializationExpr != null)
						valueSpecFunc = GenTemplateParamValueSpecFunc(param);

					string valueDefFunc = null;
					if (param.ValueDefaultExpr != null)
						valueDefFunc = GenTemplateParamValueDefaultFunc(param);

					ILTemplateParam tparam = new ILTemplateParam(param.ValueName, param.Type, param.TypeSpecialization, valueSpecFunc, valueDefFunc);
					templateParams.Add(tparam);
				}
			}

			_builder.CreateEnum(symbol.ScopeVar, symbol.MangledName, syntax.Context.Symbol, ilAttribs, templateParams, baseType, syntax.FullSpan);

			EnumMemberSyntax prevMember = null;
			foreach (EnumMemberSyntax member in syntax.Members)
			{
				string memberName = member.Name.ToString();
				_enumVars.Add(member.Context.Symbol.ScopeVar);

				// Generate function for enum value
				FunctionSymbolType funcType = SymbolType.FunctionType(null, null, baseType);
				string enumInit = NameMangling.MangleName(member.Context.Symbol) + "6__init" + NameMangling.MangleType(funcType);

				bool isUninstantiated = syntax.TemplateParameters != null;
				ScopeVariable scopeVar = new ScopeVariable(new Scope(member.Context.Symbol.ScopeVar), new NameIdentifier("__init"));
				_builder.BeginFunction(scopeVar, enumInit, funcType, ilAttribs, templateParams, isUninstantiated, member.FullSpan);
				_builder.CreateBasicBlock("entry");

				ILOperand init = null;
				if (member.DefaultValue != null)
				{
					VisitSimpleExpression(member.DefaultValue);
					init = _lastValue;
				}
				else if (prevMember != null)
				{
					ILReference prevMemberRef = new ILReference(syntax.Context.Identifier.GetSimpleName(), prevMember.Name.Identifier.Text);
					ILOperand prevVal = _builder.BuildEnumGetMemberValue(enumType, prevMemberRef, baseType, member.FullSpan);
					init = _builder.BuildBuiltin("inc", new List<ILOperand> {prevVal}, baseType, member.FullSpan);
				}
				else
				{
					init = DefaultInitialize(baseType, member.FullSpan);
				}

				_builder.BuildReturn(init, member.FullSpan);
				_builder.EndFunction();

				// Generate initializer for enum member 'body'
				string adtInit = null;
				if (member.AggrBody != null)
				{
					// Create struct
					Symbol structSymbol = (member.Context.Type as AggregateSymbolType).Symbol;

					_builder.CreateAggregate(ILAggregateType.Struct, structSymbol.ScopeVar, structSymbol.MangledName, syntax.Context.Symbol, ilAttribs, templateParams, syntax.FullSpan);

					List<VarTypePair> varList = ProcessAggregateMembers(member.AggrBody, structSymbol);

					// Create init func
					List<SymbolType> initParamTypes = new List<SymbolType>();
					foreach (VarTypePair pair in varList)
					{
						initParamTypes.Add(pair.Type);
					}

					SymbolType structType = structSymbol.Type;
					SymbolType instType = structType;

					FunctionSymbolType initType = SymbolType.FunctionType(null, initParamTypes, instType);
					adtInit = "_M" + NameMangling.MangleScopeVar(structSymbol.ScopeVar) + "6__init" + NameMangling.MangleType(initType);

					ILAttributes funcAttribs = new ILAttributes(ilAttribs);

					scopeVar = new ScopeVariable(new Scope(structSymbol.ScopeVar), new NameIdentifier("__init"));
					(List<ILOperand> ops, _) = _builder.BeginFunction(scopeVar, adtInit, initType, funcAttribs, templateParams, isUninstantiated, null);
					_builder.CreateBasicBlock("entry");

					SymbolType initStructType = templateParams != null ? instType : structType;

					ILOperand tmpStructOp = _builder.BuildStackVariable(initStructType, "struct_addr", null);

					for (var i = 0; i < varList.Count; i++)
					{
						VarTypePair pair = varList[i];
						ILReference memberRef = new ILReference(structSymbol.Identifier.GetSimpleName(), pair.Identifier);
						_builder.BuildStructInsert(tmpStructOp, memberRef, ops[i], null);
					}

					ILOperand retOp = _builder.BuildLoad(tmpStructOp, null);
					_builder.BuildReturn(retOp, null);
					_builder.EndFunction();

				}

				_builder.BuildEnumMember(symbol.MangledName, memberName, enumInit, member.Context.Type, adtInit, member.FullSpan);

				prevMember = member;
			}
		}

		protected override void VisitExpression(ExpressionSyntax syntax)
		{
			base.VisitExpression(syntax);
		}
		
		protected override void VisitFallthrough(FallthroughSyntax syntax)
		{
			base.VisitFallthrough(syntax);
		}

		protected override void VisitFor(ForSyntax syntax)
		{
			Scope subScope = syntax.Context.Scope.GetSubScope(_funcScope);

			string scopeName = subScope.ToString();
			string initName = scopeName + ".init";
			string incName = scopeName + ".inc";
			string condName = scopeName + ".cond";
			string loopName = scopeName + ".loop";
			string endName = scopeName + ".end";

			_builder.BuildBranch(initName, syntax.Initializer.FullSpan);

			_builder.CreateBasicBlock(initName);
			base.VisitSimpleExpression(syntax.Initializer);
			_builder.BuildBranch(condName, syntax.Initializer.FullSpan);

			_builder.CreateBasicBlock(condName);
			VisitSimpleExpression(syntax.Condition);
			_builder.BuildCondBranch(_lastValue, loopName, endName, syntax.Condition.FullSpan);

			_builder.CreateBasicBlock(loopName);
			base.VisitStatement(syntax.Body);
			_builder.BuildBranch(incName, syntax.Body.FullSpan);

			_builder.CreateBasicBlock(incName);
			base.VisitStatement(syntax.Increment);
			_builder.BuildBranch(condName, syntax.Increment.FullSpan);

			_builder.CreateBasicBlock(endName);
		}

		protected override void VisitForeach(ForeachSyntax syntax)
		{
			base.VisitForeach(syntax);
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			bool prevInFunc = _inFunction;
			_inFunction = true;

			ILAttributes ilAttribs = ILHelpers.GetAttributes(syntax.Context.Symbol, _astCompileAttribs);

			VisitIdentifierName(syntax.Name);
			syntax.TemplateParameters?.ForEach(VisitTemplateParameter);
			syntax.Parameters?.ForEach(VisitParameter);
			if (syntax.ReturnType != null)
			{
				VisitType(syntax.ReturnType);
			}
			if (syntax.Constraint != null)
				VisitTemplateConstraint(syntax.Constraint);

			List<ILTemplateParam> templateParams = null;
			if (syntax.Context.Identifier is TemplateDefinitionIdentifier def)
			{
				templateParams = new List<ILTemplateParam>(def.Parameters.Count);
				foreach (TemplateParameter param in def.Parameters)
				{
					string valueSpecFunc = null;
					if (param.ValueSpecializationExpr != null)
						valueSpecFunc = GenTemplateParamValueSpecFunc(param);

					string valueDefFunc = null;
					if (param.ValueDefaultExpr != null)
						valueDefFunc = GenTemplateParamValueDefaultFunc(param);

					ILTemplateParam tparam = new ILTemplateParam(param.ValueName, param.Type, param.TypeSpecialization, valueSpecFunc, valueDefFunc);
					templateParams.Add(tparam);
				}
			}

			List<ILOperand> funcParamOps;
			bool isUninstantiated = syntax.TemplateParameters != null || syntax.Context.Symbol.Parent?.Identifier is TemplateDefinitionIdentifier;
			FunctionSymbolType funcType = GetSpecializedType(syntax.Context.Symbol.Type) as FunctionSymbolType;
			Symbol funcSymbol = syntax.Context.Symbol;
			(funcParamOps, _returnType) = _builder.BeginFunction(funcSymbol.ScopeVar, funcSymbol.MangledName, funcType, ilAttribs, templateParams, isUninstantiated, syntax.FullSpan);

			if (funcParamOps != null)
			{
				int idx = 0;
				if (syntax.Receiver != null)
				{
					ILOperand op = funcParamOps[0];
					ScopeVariable scopeVar = new ScopeVariable(syntax.Context.Scope, syntax.Receiver.Context.Identifier);
					_funcParams.Add(scopeVar, op);
					++idx;
				}

				for (var i = idx; i < funcParamOps.Count; i++)
				{
					ILOperand op = funcParamOps[i];
					ParameterSyntax parameterSyntax = syntax.Parameters[i - idx];
					ScopeVariable scopeVar = new ScopeVariable(syntax.Context.Scope, parameterSyntax.Context.Identifier);
					_funcParams.Add(scopeVar, op);
				}
			}

			if (syntax.Body != null)
			{
				_builder.CreateBasicBlock("entry");
				_funcScope = syntax.Body.Context.Scope;
				VisitBlockStatement(syntax.Body);
			}

			_builder.EndFunction();

			_inFunction = prevInFunc;
			_funcParams.Clear();
		}

		protected override void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			VisitQualifiedName(syntax.QualifiedName);

			if (syntax.Context.FuncType != null)
			{
				Symbol instSymbol = syntax.Context.Symbol;

				FunctionSymbolType funcType = GetSpecializedType(syntax.Context.FuncType) as FunctionSymbolType;

				List<ILOperand> operands = null;

				int argCount = syntax.Arguments?.Count ?? 0;
				int funcParamCount = funcType.ParamTypes?.Count ?? 0;
				bool recAsArg = argCount != funcParamCount;

				if (argCount > 0)
				{
					operands = new List<ILOperand>();
					for (var i = 0; i < syntax.Arguments.Count; i++)
					{
						ArgumentSyntax argument = syntax.Arguments[i];

						SymbolType expectedType;
						if (recAsArg)
						{
							if (i == 0)
								expectedType = funcType.ReceiverType;
							else
								expectedType = funcType.ParamTypes[i - 1];
						}
						else
						{
							expectedType = funcType.ParamTypes[i];
						}

						expectedType = GetSpecializedType(expectedType);

						VisitArgument(argument);

						ILOperand value = _lastValue;

						if (value.Type is MemoryLocSymbolType memLoc)
						{
							value = _builder.BuildLoad(value, argument.FullSpan);
						}

						if (value.Type != expectedType)
						{
							value = CastOperand(value, expectedType, ILOpType.Static, argument.FullSpan);
						}

						operands.Add(value);

						if (recAsArg)
						{
							if (i != 0 && operands[i].Type == null) // Convert null to required type
							{
								operands[i - 1].Type = funcType.ParamTypes[i];
							}
						}
						else
						{
							if (operands[i].Type == null) // Convert null to required type
							{
								operands[i].Type = funcType.ParamTypes[i];
							}
						}
					}
				}

				string mangledName;
				if (instSymbol != null)
				{
					mangledName = instSymbol.MangledName;
				}
				else
				{
					mangledName = syntax.Context.Symbol.MangledName;
				}

				ILOperand funcRefOp;
				if (funcType.ReceiverType == null || recAsArg)
				{
					funcRefOp = _builder.BuildFunctionRef(mangledName, funcType, syntax.FullSpan);
				}
				else
				{
					// Get the receiver
					ScopeVariable recScopeVar = syntax.Context.IdenScopeVar;
					ILOperand receiverOp;

					_getIdenAddr = false;
					_parseQualifiedLast = false;
					VisitQualifiedName(syntax.QualifiedName);
					_parseQualifiedLast = true;
					receiverOp = _lastValue;

					if (operands == null)
						operands = new List<ILOperand> {receiverOp};
					else
						operands.Insert(0, receiverOp);

					// Get a reference to the method and call it
					if (receiverOp.Type is TemplateInstanceSymbolType instType)
					{
						TemplateInstanceIdentifier instIden = instType.Iden.Name as TemplateInstanceIdentifier;
						List<Symbol> children = instType.TemplateSymbol.GetChildrenWithType<TemplateParamSymbolType>();
						for (var i = 0; i < instIden.Arguments.Count; i++)
						{
							TemplateArgument arg = instIden.Arguments[i];

							Symbol param = children.Find(s =>
							{
								if ((s.Type as TemplateParamSymbolType).Index == i)
									return true;
								return false;
							});

							_templateTypeMapping.Add(param.Type, arg.Type);
						}

						funcType = GetSpecializedType(funcType) as FunctionSymbolType;
						_templateTypeMapping.Clear();
					}

					ILReference methodRef = new ILReference(recScopeVar.Scope.LastSubScope.GetSimpleName(), recScopeVar.Name.GetSimpleName());
					funcRefOp = _builder.BuildMethodRef(methodRef, funcType, syntax.FullSpan);
				}

				_lastValue = _builder.BuildCall(funcRefOp, operands, funcType.ReturnType, syntax.FullSpan);
			}
			else
			{
				Symbol memberSym = CompilerContext.FindDefinition(syntax.Context.Scope, syntax.Context.IdenScopeVar, syntax.FullSpan.Start);

				TupleSymbolType tupleType = memberSym.Type as TupleSymbolType;
				List<ILOperand> operands = new List<ILOperand>();
				for (var i = 0; i < syntax.Arguments.Count; i++)
				{
					ArgumentSyntax argument = syntax.Arguments[i];

					SymbolType expectedType = tupleType.SubTypes[i];

					expectedType = GetSpecializedType(expectedType);

					_getIdenAddr = false;
					VisitArgument(argument);

					ILOperand value = _lastValue;

					if (value.Type != expectedType)
					{
						value = CastOperand(value, expectedType, ILOpType.Static, argument.FullSpan);
					}

					operands.Add(value);

					if (operands[i].Type == null) // Convert null to required type
					{
						operands[i].Type = tupleType.SubTypes[i];
					}
				}

				ILReference memberRef = new ILReference(memberSym.Parent.Identifier.GetSimpleName(), memberSym.Identifier.GetSimpleName());
				_lastValue = _builder.BuildEnumValue(memberSym.Parent.Type, memberRef, operands, syntax.FullSpan);
			}
		}

		protected override void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			base.VisitTemplateConstraint(syntax);
		}

		protected override void VisitGoto(GotoSyntax syntax)
		{
			base.VisitGoto(syntax);
		}

		protected override void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
			if (_idenAsVar)
			{
				Symbol symbol = CompilerContext.FindDefinition(syntax.Context.Scope, syntax.Context.Identifier, syntax.FullSpan.Start, null);
				if (symbol != null)
				{
					if (_varMapping.ContainsKey(symbol.ScopeVar))
					{
						_lastValue = _varMapping[symbol.ScopeVar];

						if (!_getIdenAddr && _lastValue.Type is MemoryLocSymbolType)
						{
							_lastValue = _builder.BuildLoad(_lastValue, null);
						}
					}
					else if (symbol.Kind == SymbolKind.TemplateParam)
					{
						Symbol templateSym;
						TemplateInstanceSymbolType instType;
						if (symbol.Parent != null) // Template param for definition
						{
							templateSym = symbol.Parent;
							instType = GetSpecializedType(templateSym.Type) as TemplateInstanceSymbolType;
						}
						else // Template param for impl
						{
							// TODO: Does this actually work?
							Symbol instSym = symbol.TemplateSymbol;
							instType = instSym.Type as TemplateInstanceSymbolType;
							templateSym = instType.TemplateSymbol;
						}
						
						ILReference memberRef = new ILReference(templateSym.Identifier.GetSimpleName(), symbol.Identifier.GetSimpleName());
						_lastValue = _builder.BuildTemplateGetValue(instType, memberRef, symbol.Type, syntax.FullSpan);
					}
					else // Global variable
					{
						SymbolType globType = GetSpecializedType(syntax.Context.Type);
						if (_getIdenAddr)
						{
							_lastValue = _builder.BuildGlobalAddr(syntax.Context.Symbol.MangledName, globType, syntax.FullSpan);
						}
						else
						{
							_lastValue = _builder.BuildGlobalValue(syntax.Context.Symbol.MangledName, globType, syntax.FullSpan);
						}
					}
				}
			}
		}

		protected override void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
			base.VisitIdentifierType(syntax);
		}

		protected override void VisitIfElse(IfElseSyntax syntax)
		{
			VisitSimpleExpression(syntax.If.Condition);

			Scope subScope = syntax.If.Context.Scope.GetSubScope(_funcScope);

			string scopeName = subScope.ToString();
			string thenName = scopeName + ".then";
			string elseName = scopeName + ".else";
			string endName = scopeName + ".end";

			_builder.BuildCondBranch(_lastValue, thenName, syntax.Else != null ? elseName : endName, syntax.If.Condition.FullSpan);

			_builder.CreateBasicBlock(thenName);
			
			base.VisitStatement(syntax.If.Then);

			if (syntax.Else != null)
			{
				_builder.BuildBranch(endName, syntax.If.FullSpan);
				_builder.CreateBasicBlock(elseName);
				
				base.VisitStatement(syntax.Else);
			}

			_builder.BuildBranch(endName, syntax.FullSpan);
			_builder.CreateBasicBlock(endName);
		}

		protected override void VisitImport(ImportSyntax syntax)
		{
			base.VisitImport(syntax);
		}

		protected override void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			base.VisitIndexSliceExpression(syntax);
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			bool prevIsGlobalVar = _isGlobalVar;
			_isGlobalVar = false;

			// TODO
			//base.VisitInterface(syntax);

			Symbol symbol = syntax.Context.Symbol;
			_builder.CreateInterface(symbol.MangledName, null);

			_isGlobalVar = prevIsGlobalVar;
		}

		protected override void VisitIsExpression(IsExpressionSyntax syntax)
		{
			base.VisitIsExpression(syntax);
		}

		protected override void VisitLabel(LabelSyntax syntax)
		{
			base.VisitLabel(syntax);
		}

		protected override void VisitLambda(ClosureSyntax syntax)
		{
			base.VisitLambda(syntax);
		}

		protected override void VisitLiteral(LiteralExpressionSyntax syntax)
		{
			base.VisitLiteral(syntax);

			switch (syntax.LiteralToken.Type)
			{
			case TokenType.I8Literal:
			{
				_lastValue = _builder.BuildIntLiteral(syntax.LiteralToken.Values.Integer, SymbolType.BuiltinType(BuiltinTypes.I8), syntax.FullSpan);
				break;
			}
			case TokenType.I16Literal:
			{
				_lastValue = _builder.BuildIntLiteral(syntax.LiteralToken.Values.Integer, SymbolType.BuiltinType(BuiltinTypes.I16), syntax.FullSpan);
				break;
			}
			case TokenType.I32Literal:
			{
				_lastValue = _builder.BuildIntLiteral(syntax.LiteralToken.Values.Integer, SymbolType.BuiltinType(BuiltinTypes.I32), syntax.FullSpan);
				break;
			}
			case TokenType.I64Literal:
			{
				_lastValue = _builder.BuildIntLiteral(syntax.LiteralToken.Values.Integer, SymbolType.BuiltinType(BuiltinTypes.I64), syntax.FullSpan);
				break;
			}
			case TokenType.U8Literal:
			{
				_lastValue = _builder.BuildIntLiteral(syntax.LiteralToken.Values.Integer, SymbolType.BuiltinType(BuiltinTypes.U8), syntax.FullSpan);
				break;
			}
			case TokenType.U16Literal:
			{
				_lastValue = _builder.BuildIntLiteral(syntax.LiteralToken.Values.Integer, SymbolType.BuiltinType(BuiltinTypes.U16), syntax.FullSpan);
				break;
			}
			case TokenType.U32Literal:
			{
				_lastValue = _builder.BuildIntLiteral(syntax.LiteralToken.Values.Integer, SymbolType.BuiltinType(BuiltinTypes.U32), syntax.FullSpan);
				break;
			}
			case TokenType.U64Literal:
			{
				_lastValue = _builder.BuildIntLiteral(syntax.LiteralToken.Values.Integer, SymbolType.BuiltinType(BuiltinTypes.U64), syntax.FullSpan);
				break;
			}
			case TokenType.CharLiteral:
			{
				_lastValue = _builder.BuildIntLiteral(syntax.LiteralToken.Values.Character, SymbolType.BuiltinType(BuiltinTypes.Rune), syntax.FullSpan);
				break;
			}
			case TokenType.F32Literal:
			{
				_lastValue = _builder.BuildFloatLiteral(syntax.LiteralToken.Values.Double, SymbolType.BuiltinType(BuiltinTypes.F32), syntax.FullSpan);
				break;
			}
			case TokenType.F64Literal:
			{
				_lastValue = _builder.BuildFloatLiteral(syntax.LiteralToken.Values.Double, SymbolType.BuiltinType(BuiltinTypes.F64), syntax.FullSpan);
				break;
			}
			case TokenType.StringLiteral:
			{
				_lastValue = _builder.BuildStringLiteral(syntax.LiteralToken.Text.Trim('"'), syntax.FullSpan);
				break;
			}
			case TokenType.Void:
			{
				// Note: Let's declaration know that value is void initialized 
				_lastValue = null;
				break;
			}
			case TokenType.True:
			{
				_lastValue = _builder.BuildIntLiteral(1, SymbolType.BuiltinType(BuiltinTypes.Bool), syntax.FullSpan);
				break;
			}
			case TokenType.False:
			{
				_lastValue = _builder.BuildIntLiteral(0, SymbolType.BuiltinType(BuiltinTypes.Bool), syntax.FullSpan);
				break;
			}
			case TokenType.Null:
			{
				//_lastValue = _builder.BuildNull();
				_lastValue = _builder.BuildIntLiteral(0, SymbolType.BuiltinType(BuiltinTypes.Null), syntax.FullSpan);
				break;
			}
			}
		}

		protected override void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			base.VisitMixinExpression(syntax);
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			//base.VisitModuleDirective(syntax);

			if (syntax.Attributes?.CompilerAtAttribs != null)
			{
				_astCompileAttribs = syntax.Context.CompileAttribs;
			}
		}

		protected override void VisitNameList(NameListSyntax syntax)
		{
			base.VisitNameList(syntax);
		}

		protected override void VisitNamespace(NamespaceSyntax syntax)
		{
			base.VisitNamespace(syntax);
		}

		protected override void VisitNewExpression(NewExpressionSyntax syntax)
		{
			base.VisitNewExpression(syntax);
		}

		protected override void VisitNullableType(NullableTypeSyntax syntax)
		{
			base.VisitNullableType(syntax);
		}

		protected override void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
			base.VisitPackageDirective(syntax);
		}

		protected override void VisitParameter(ParameterSyntax syntax)
		{
			// Skip param, not required, visiting the default value even breaks things
			//base.VisitParameter(syntax);
		}

		protected override void VisitPointerType(PointerTypeSyntax syntax)
		{
			base.VisitPointerType(syntax);
		}

		protected override void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			base.VisitPostfixExpression(syntax);
		}

		protected override void VisitBuiltinType(BuiltinTypeSyntax syntax)
		{
			base.VisitBuiltinType(syntax);
		}

		protected override void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			SymbolType resType = GetSpecializedType(syntax.Context.Type);
			
			ILOpType opType = ILHelpers.GetILOpType(syntax.OperatorToken.Text);

			// TODO: Check for custom operators
			if (false)
			{

				bool prev = _idenAsVar;
				_idenAsVar = true;
				base.VisitPrefixExpression(syntax);
				_idenAsVar = prev;

				SymbolType type = _lastValue.Type;
				List<SymbolType> paramTypes = new List<SymbolType> { type };
				ILOperand opRef = _builder.BuildOperatorRef(ILOpLoc.Prefix, opType, paramTypes, resType, syntax.FullSpan);
				List<ILOperand> valueList = new List<ILOperand>();

				ILOperand tmpVal = _lastValue;
				if (tmpVal.Type is MemoryLocSymbolType)
				{
					ILOperand tmp = _builder.BuildLoad(tmpVal, syntax.Right.FullSpan);
					valueList.Add(tmp);
				}
				else
				{
					valueList.Add(tmpVal);
				}

				ILOperand res = _builder.BuildCall(opRef, valueList, resType, syntax.FullSpan);

				_lastValue = res;
			}
			else
			{
				if (opType == ILOpType.BAnd) // &expr
				{
					bool prev = _idenAsVar;
					_idenAsVar = true;
					_getIdenAddr = true;
					base.VisitPrefixExpression(syntax);
					_idenAsVar = prev;
					

					if (_lastValue.Type is MemoryLocSymbolType || // &name
					    _lastValue.Type is ReferenceSymbolType)   // &ref
					{
						_lastValue = _builder.BuildConvertType(_lastValue, resType, syntax.FullSpan);
					}
					else // & ...
					{
						// Should not happen
					}
				}
				else
				{

					bool prev = _idenAsVar;
					_idenAsVar = true;
					base.VisitPrefixExpression(syntax);
					_idenAsVar = prev;

					SymbolType type = _lastValue.Type;
					List<SymbolType> paramTypes = new List<SymbolType> { type };
					ILOperand opRef = _builder.BuildOperatorRef(ILOpLoc.Prefix, opType, paramTypes, resType, syntax.FullSpan);
					List<ILOperand> valueList = new List<ILOperand>();

					ILOperand tmpVal = _lastValue;
					if (tmpVal.Type is MemoryLocSymbolType)
					{
						ILOperand tmp = _builder.BuildLoad(tmpVal, syntax.Right.FullSpan);
						valueList.Add(tmp);
					}
					else
					{
						valueList.Add(tmpVal);
					}

					ILOperand res = _builder.BuildCall(opRef, valueList, resType, syntax.FullSpan);

					_lastValue = res;
				}
			}
		}

		protected override void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
			if (_idenAsVar)
			{
				_idenAsVar = false;
				base.VisitQualifiedName(syntax);
				_idenAsVar = true;

				if (syntax.SeperationToken?.Type == TokenType.Dot)
				{
					_lastValue = null;
					ScopeVariable scopeVar = syntax.Context.IdenScopeVar;
					Scope subScope = new Scope(syntax);

					Symbol symbol;
					_lastValue = null;
					for (int i = subScope.Names.Count; i < scopeVar.Scope.Names.Count; ++i)
					{
						Identifier subName = scopeVar.Scope.Names[i];
						symbol = CompilerContext.FindDefinition(syntax.Context.Scope, subName, syntax.FullSpan.Start, subScope);

						if (symbol != null)
						{
							GetLastValueFromMember(symbol, syntax.FullSpan);

							SymbolType innerType = _lastValue.Type.GetInnerType();

							if (innerType is TemplateInstanceSymbolType instType)
								innerType = instType.TemplateSymbol.Type;
									
							subScope = new Scope();
							subScope.Names.Add(innerType.GetIdentifier());
						}
						else
						{
							// TODO: Error
						}

					}

					// Only run when not trying to find a method reciever
					if (_parseQualifiedLast)
					{
						symbol = CompilerContext.FindDefinition(syntax.Context.Scope, scopeVar.Name, syntax.FullSpan.Start, subScope);
						if (symbol != null)
						{
							bool prev = _getIdenAddr;
							_getIdenAddr = false;
							GetLastValueFromMember(symbol, syntax.FullSpan);
							_getIdenAddr = prev;
						}
					}
				}
				else if (syntax.SeperationToken?.Type == TokenType.ColonColon)
				{
					ScopeVariable scopeVar = syntax.Context.IdenScopeVar;
					Symbol symbol = CompilerContext.FindDefinition(syntax.Context.Scope, scopeVar, syntax.FullSpan.Start);
					if (symbol != null)
					{
						SymbolType symType = GetSpecializedType(symbol.Type);
						if (symbol.Kind == SymbolKind.EnumMember)
						{
							ScopeVariable enumScopeVar = new ScopeVariable(scopeVar.Scope.GetBaseScope(), scopeVar.Scope.LastSubScope);
							Symbol enumSymbol = CompilerContext.FindDefinition(syntax.Context.Scope, enumScopeVar, syntax.FullSpan.Start);

							EnumSymbolType enumType = GetSpecializedType(enumSymbol.Type) as EnumSymbolType;

							ILReference memberRef = new ILReference(scopeVar.Scope.LastSubScope.GetSimpleName(), scopeVar.Name.GetSimpleName());
							_lastValue = _builder.BuildEnumValue(enumType, memberRef, null, syntax.FullSpan);
						}
						else if (symbol.Kind == SymbolKind.Variable)
						{
							if ((symbol.Attribs & SemanticAttributes.Static) != 0) // Static variable
							{
								// TODO
								
							}
							else // Global variable or template value param
							{
								ScopeVariable parentScopeVar = new ScopeVariable(syntax.Context.IdenScopeVar);

								Identifier iden = parentScopeVar.Scope.LastSubScope;
								parentScopeVar.Name = iden;
								parentScopeVar.Scope.Names.RemoveLast();

								Symbol parentSym = CompilerContext.FindDefinition(syntax.Context.Scope, parentScopeVar, syntax.FullSpan.Start);

								if (parentSym != null) // Template value param
								{
									ILReference reference = new ILReference(parentSym.Type.GetIdentifier().GetSimpleName(), symbol.Identifier.GetSimpleName());

									SymbolType parentType = GetSpecializedType(parentSym.Type);
									_lastValue = _builder.BuildTemplateGetValue(parentType, reference, symType, syntax.FullSpan);
								}
								else // Global value
								{
									if (_getIdenAddr)
										_lastValue = _builder.BuildGlobalAddr(symbol.MangledName, symType, syntax.FullSpan);
									else
										_lastValue = _builder.BuildGlobalValue(symbol.MangledName, symType, syntax.FullSpan);
								}
							}
						}
					}

				}
				else
				{
					ScopeVariable scopeVar = new ScopeVariable(syntax.Context.Scope, syntax.Context.Identifier);
					if (!_varMapping.TryGetValue(scopeVar, out _lastValue))
					{
						// Should be global variable
						Symbol symbol = CompilerContext.FindDefinition(syntax.Context.Scope, scopeVar, syntax.FullSpan.Start);
						SymbolType symType = GetSpecializedType(symbol.Type);
						_lastValue = _builder.BuildGlobalValue(symbol.MangledName, symType, syntax.FullSpan);
					}
				}
			}
			else
			{
				base.VisitQualifiedName(syntax);
			}
		}

		void GetLastValueFromMember(Symbol symbol, TextSpan span)
		{
			SymbolType symType = GetSpecializedType(symbol.Type);
			if (_structVars.Contains(symbol.ScopeVar))
			{
				ILReference memberRef = new ILReference(symbol.Scope.LastSubScope.GetSimpleName(), symbol.Identifier.GetSimpleName());

				if (_getIdenAddr)
				{
					_lastValue = _builder.BuildStructElemAddr(_lastValue, memberRef, SymbolType.MemoryLocType(symType), span);
				}
				else
				{
					_lastValue = _builder.BuildStructExtract(_lastValue, memberRef, symType, span);
				}

			}
			else if (_unionVars.Contains(symbol.ScopeVar))
			{
				ILReference memberRef = new ILReference(symbol.Scope.LastSubScope.GetSimpleName(), symbol.Identifier.GetSimpleName());

				if (_getIdenAddr)
				{
					_lastValue = _builder.BuildUnionElemAddr(_lastValue, memberRef, SymbolType.MemoryLocType(symType), span);
				}
				else
				{
					_lastValue = _builder.BuildUnionExtract(_lastValue, memberRef, symType, span);
				}
			}
			else if (_varMapping.ContainsKey(symbol.ScopeVar))
			{
				// TODO: _getIdenAddr
				if (_lastValue == null)
				{
					_lastValue = _varMapping[symbol.ScopeVar];
				}
				else
				{
					bool isPtr = _lastValue.Type is PointerSymbolType ||
					             _lastValue.Type is MemoryLocSymbolType;

					SymbolType type = isPtr ? _lastValue.Type.GetBaseType() : _lastValue.Type;

					if (type is AggregateSymbolType aggregate)
					{
						if (aggregate.Type == AggregateTypes.Struct)
						{
							ILReference memberRef = new ILReference(aggregate.Identifier.Name.GetSimpleName(), symbol.Identifier.GetSimpleName());

							if (_getIdenAddr)
								_lastValue = _builder.BuildStructElemAddr(_lastValue, memberRef, symType, span);
							else
								_lastValue = _builder.BuildStructExtract(_lastValue, memberRef, symType, span);
						}
					}
				}
			}
			else if (_funcParams != null && _funcParams.ContainsKey(symbol.ScopeVar))
			{
				_lastValue = _funcParams[symbol.ScopeVar];
			}
		}

		protected override void VisitReturn(ReturnSyntax syntax)
		{
			bool prev = _idenAsVar;
			_idenAsVar = true;
			_getIdenAddr = false;
			base.VisitReturn(syntax);
			_idenAsVar = prev;

			if (syntax.Expression is EmptyExpressionSyntax)
			{
				_builder.BuildReturn(syntax.FullSpan);
			}
			else if (syntax.Expression is CommaExpressionSyntax commaExpr)
			{
				List<ILOperand> values = new List<ILOperand>();
				TupleSymbolType tupleType = _returnType as TupleSymbolType;
				List<SymbolType> subTypes = new List<SymbolType>();

				for (var i = 0; i < _lastValueGroup.Count; i++)
				{
					ILOperand op = _lastValueGroup[i];
					ILOperand elemOp = op;
					SymbolType retElemType = tupleType.SubTypes[i];
					retElemType = GetSpecializedType(retElemType);
					subTypes.Add(retElemType);

					// Cast values if required
					if (elemOp.Type != retElemType)
					{
						elemOp = CastOperand(elemOp, retElemType, ILOpType.Static, commaExpr.Expressions[i].FullSpan);
					}
					values.Add(elemOp);
				}

				// pack and return values
				TupleSymbolType retType = SymbolType.TupleType(subTypes);
				ILOperand retOp = _builder.BuildTuplePack(values, retType, syntax.Expression.FullSpan);
				_builder.BuildReturn(retOp, null);
			}
			else
			{
				ILOperand iden = _lastValue;

				SymbolType retType = GetSpecializedType(_returnType);
				if (iden.Type != retType)
				{
					iden = CastOperand(iden, retType, ILOpType.Static, syntax.Expression.FullSpan);
				}

				_builder.BuildReturn(iden, syntax.FullSpan);
			}

		}

		protected override void VisitSimpleExpression(SimpleExpressionSyntax syntax)
		{
			bool prev = _idenAsVar;
			_idenAsVar = true;
			base.VisitSimpleExpression(syntax);
			_idenAsVar = prev;
		}

		protected override void VisitSingleAttribute(SingleAttributeSyntax syntax)
		{
			base.VisitSingleAttribute(syntax);
		}

		protected override void VisitSpecialKeyword(SpecialKeywordExpressionSyntax syntax)
		{
			base.VisitSpecialKeyword(syntax);
		}

		struct VarTypePair
		{
			internal string Identifier;
			internal SymbolType Type;
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			bool prevIsGlobalVar = _isGlobalVar;
			_isGlobalVar = false;

			ILAttributes ilAttribs = ILHelpers.GetAttributes(syntax.Context.Symbol, _astCompileAttribs);
			Symbol structSymbol = syntax.Context.Symbol;

			// Get template params, if there are any
			List<ILTemplateParam> templateParams = null;
			if (syntax.Context.Identifier is TemplateDefinitionIdentifier def)
			{
				templateParams = new List<ILTemplateParam>(def.Parameters.Count);
				foreach (TemplateParameter param in def.Parameters)
				{
					string valueSpecFunc = null;
					if (param.ValueSpecializationExpr != null)
						valueSpecFunc = GenTemplateParamValueSpecFunc(param);

					string valueDefFunc = null;
					if (param.ValueDefaultExpr != null)
						valueDefFunc = GenTemplateParamValueDefaultFunc(param);

					ILTemplateParam tparam = new ILTemplateParam(param.ValueName, param.Type, param.TypeSpecialization, valueSpecFunc, valueDefFunc);
					templateParams.Add(tparam);
				}
			}
			
			_builder.CreateAggregate(ILAggregateType.Struct, structSymbol.ScopeVar, structSymbol.MangledName, structSymbol, ilAttribs, templateParams, syntax.FullSpan);
			
			List<VarTypePair> varList = ProcessAggregateMembers(syntax.Body, structSymbol);

			// Generate initializer
			List<SymbolType> initParamTypes = new List<SymbolType>();
			foreach (VarTypePair pair in varList)
			{
				initParamTypes.Add(pair.Type);
			}

			VTable vtable = CompilerContext.GetVTable(structSymbol.ScopeVar);
			Identifier initIden = new NameIdentifier("__init");
			VTableMethod method = vtable.GetNonOverloadedMethod(initIden);

			ILAttributes funcAttribs = new ILAttributes(ilAttribs);

			bool isUninstantiated = syntax.TemplateParameters != null;
			ScopeVariable scopeVar = new ScopeVariable(new Scope(structSymbol.ScopeVar), new NameIdentifier("__init"));
			(List<ILOperand> ops, _) = _builder.BeginFunction(scopeVar, method.MangledFunc, method.Type, funcAttribs, templateParams, isUninstantiated, null);
			_builder.CreateBasicBlock("entry");

			ILOperand tmpStructOp = _builder.BuildStackVariable(method.Type.ReturnType, "struct_addr", null);

			for (var i = 0; i < varList.Count; i++)
			{
				VarTypePair pair = varList[i];
				ILReference memberRef = new ILReference(structSymbol.Identifier.GetSimpleName(), pair.Identifier);
				_builder.BuildStructInsert(tmpStructOp, memberRef, ops[i], null);
			}

			ILOperand retOp = _builder.BuildLoad(tmpStructOp, null);
			_builder.BuildReturn(retOp, null);
			_builder.EndFunction();

			_isGlobalVar = prevIsGlobalVar;
		}

		// TODO: Templated
		protected override void VisitAggregateInitializer(AggregateInitializerSyntax syntax)
		{
			Symbol aggrSymbol = CompilerContext.FindDefinition(syntax.Context.Scope, syntax.Context.IdenScopeVar, syntax.FullSpan.Start);
			SymbolTable baseTable = aggrSymbol.CompilerContext.Symbols;
			SymbolTable elemTable = baseTable.FindClosestTable(new Scope(aggrSymbol.ScopeVar));

			Dictionary<Identifier, int> elementOrder = new Dictionary<Identifier, int>();
			List<ILOperand> parameters = new List<ILOperand>();
			if (aggrSymbol.Kind == SymbolKind.Struct)
			{
				// Get __init func
				VTable vtable = CompilerContext.GetVTable(aggrSymbol.ScopeVar);
				VTableMethod method = vtable.GetMethod("__init", syntax.Context.FuncType as FunctionSymbolType);

				if (syntax.Elements != null)
				{
					// set the variable iden's index 
					int elemIdx = 0;
					foreach (KeyValuePair<Identifier, List<Symbol>> pair in elemTable.Symbols)
					{
						Symbol elemSym = pair.Value[0];

						if (elemSym.Kind != SymbolKind.Variable)
							continue;

						elementOrder.Add(elemSym.Identifier, elemIdx);

						parameters.Add(null);

						++elemIdx;
					}

					// Get all params and cast if necessary
					_getIdenAddr = false;
					for (var i = 0; i < syntax.Elements.Count; i++)
					{
						AggregateInitializerElemSyntax element = syntax.Elements[i];
						SymbolType expectedType = method.Type.ParamTypes[i];
						if (element.Identifier != null)
						{
							Identifier iden = new NameIdentifier(element.Identifier);
							if (!elementOrder.ContainsKey(iden))
								continue;

							int index = elementOrder[iden];

							VisitSimpleExpression(element.Expression);
							if (_lastValue.Type != expectedType)
							{
								_lastValue = CastOperand(_lastValue, expectedType, ILOpType.Static, element.Expression.FullSpan);
							}

							parameters[index] = _lastValue;
						}
						else
						{
							VisitSimpleExpression(element.Expression);
							if (_lastValue.Type != expectedType)
							{
								_lastValue = CastOperand(_lastValue, expectedType, ILOpType.Static, element.Expression.FullSpan);
							}

							parameters[i] = _lastValue;
						}
					}
				}

				// Create call to __init
				ILOperand funcRef = _builder.BuildFunctionRef(method.MangledFunc, method.Type, syntax.FullSpan);
				_lastValue = _builder.BuildCall(funcRef, parameters, aggrSymbol.Type, syntax.FullSpan);
			}
			else if (aggrSymbol.Kind == SymbolKind.Union)
			{
				// Find elem to init
				int elemIdx = 0;
				List<Identifier> identifiers = new List<Identifier>();
				foreach (KeyValuePair<Identifier, List<Symbol>> pair in elemTable.Symbols)
				{
					Symbol elemSym = pair.Value[0];
					AggregateInitializerElemSyntax element = syntax.Elements[elemIdx];

					if (elemSym.Kind != SymbolKind.Variable)
						continue;

					Identifier iden = new NameIdentifier(element.Identifier);
					if (elemSym.Identifier == iden)
					{
						parameters.Add(null);
						identifiers.Add(iden);
						break;
					}

					++elemIdx;
				}

				// Get __init func
				VTable vtable = CompilerContext.GetVTable(aggrSymbol.ScopeVar);
				string initName = $"__init_{elemIdx}";
				VTableMethod method = vtable.GetMethod(initName, syntax.Context.FuncType as FunctionSymbolType);

				// Get all params and cast if necessary
				_getIdenAddr = false;
				for (var i = 0; i < syntax.Elements.Count; i++)
				{
					AggregateInitializerElemSyntax element = syntax.Elements[i];
					SymbolType expectedType = method.Type.ParamTypes[i];
					if (element.Identifier != null)
					{
						Identifier iden = new NameIdentifier(element.Identifier);
						if (identifiers.Find(id => { return id == iden;}) == null)
							continue;

						VisitSimpleExpression(element.Expression);
						if (_lastValue.Type != expectedType)
						{
							_lastValue = CastOperand(_lastValue, expectedType, ILOpType.Static, element.Expression.FullSpan);
						}
						parameters[0] = _lastValue;
					}
				}

				// Create call to __init
				ILOperand funcRef = _builder.BuildFunctionRef(method.MangledFunc, method.Type, syntax.FullSpan);
				_lastValue = _builder.BuildCall(funcRef, parameters, aggrSymbol.Type, syntax.FullSpan);
			}
			else if (aggrSymbol.Kind == SymbolKind.EnumMember)
			{
				Symbol typeSym = (aggrSymbol.Type as AggregateSymbolType).Symbol;
				int elemIdx = 0;
				foreach (Symbol child in typeSym.Children)
				{
					if (child.Kind != SymbolKind.Variable)
						continue;

					elementOrder.Add(child.Identifier, elemIdx);

					parameters.Add(null);

					++elemIdx;
				}

				// Get __init func
				VTable vtable = CompilerContext.GetVTable(typeSym.ScopeVar);
				VTableMethod method = vtable.GetMethod("__init", syntax.Context.FuncType as FunctionSymbolType);

				// Get all params and cast if necessary
				_getIdenAddr = false;
				for (var i = 0; i < syntax.Elements.Count; i++)
				{
					AggregateInitializerElemSyntax element = syntax.Elements[i];
					SymbolType expectedType = method.Type.ParamTypes[i];
					if (element.Identifier != null)
					{
						Identifier iden = new NameIdentifier(element.Identifier);
						if (!elementOrder.ContainsKey(iden))
							continue;

						int index = elementOrder[iden];

						VisitSimpleExpression(element.Expression);
						if (_lastValue.Type != expectedType)
						{
							_lastValue = CastOperand(_lastValue, expectedType, ILOpType.Static, element.Expression.FullSpan);
						}
						parameters[index] = _lastValue;
					}
					else
					{
						VisitSimpleExpression(element.Expression);
						if (_lastValue.Type != expectedType)
						{
							_lastValue = CastOperand(_lastValue, expectedType, ILOpType.Static, element.Expression.FullSpan);
						}
						parameters[i] = _lastValue;
					}
				}

				// Create call to __init
				ILOperand funcRef = _builder.BuildFunctionRef(method.MangledFunc, method.Type, syntax.FullSpan);
				ILOperand structOp = _builder.BuildCall(funcRef, parameters, aggrSymbol.Type, syntax.FullSpan);

				// Create enum value
				Symbol parentSym = aggrSymbol.Parent;
				ILReference memberRef = new ILReference(parentSym.Identifier.GetSimpleName(), aggrSymbol.Identifier.GetSimpleName());
				_lastValue = _builder.BuildEnumValue(parentSym.Type, memberRef, new List<ILOperand>{ structOp }, syntax.FullSpan);
			}
		}

		protected override void VisitSwitch(SwitchSyntax syntax)
		{
			//base.VisitSwitch(syntax);

			// Generate case labels
			Identifier switchScope = syntax.Context.Scope.LastSubScope;
			List<string> labels = new List<string>();
			for (var i = 0; i < syntax.Cases.Count; i++)
			{
				SwitchCaseSyntax caseSyntax = syntax.Cases[i];

				string label;
				if (caseSyntax.DefaultToken != null)
					label = $"{switchScope}_default";
				else
					label = $"{switchScope}_case_{i}";
				labels.Add(label);
			}
			string endLabel = $"{switchScope}_end";

			// Enum switch
			if (syntax.Expression.Context.Type is EnumSymbolType enumType)
			{
				VisitSimpleExpression(syntax.Expression);
				List<ILRefCase> cases = new List<ILRefCase>();
				string defaultCaseLabel = endLabel;
				for (var i = 0; i < labels.Count; i++)
				{
					SwitchCaseSyntax caseSyntax = syntax.Cases[i];
					string label = labels[i];
					
					if (caseSyntax.CaseToken != null)
					{
						IdentifierNameSyntax idenSyntax;
						if (caseSyntax.Expression is QualifiedNameSyntax qualSyntax)
							idenSyntax = qualSyntax.Right as IdentifierNameSyntax;
						else
							idenSyntax = caseSyntax.Expression as IdentifierNameSyntax;

						ILReference reference = new ILReference(enumType.GetIdentifier().GetSimpleName(), idenSyntax.Identifier.Text);
						cases.Add(new ILRefCase(reference, label));
					}
					else
					{
						defaultCaseLabel = label;
					}
				}
				_builder.BuildSwitchEnum(_lastValue, cases, defaultCaseLabel, syntax.Expression.FullSpan);
			}
			else // Value switch
			{
				// TODO: Allow compile time calculated values

				VisitSimpleExpression(syntax.Expression);
				List<ILValCase> cases = new List<ILValCase>();
				string defaultCaseLabel = endLabel;

				for (int i = 0; i < labels.Count; i++)
				{
					SwitchCaseSyntax caseSyntax = syntax.Cases[i];
					string label = labels[i];

					if (caseSyntax.CaseToken != null)
					{
						VisitSimpleExpression(caseSyntax.Expression);
						ILOperand val = _lastValue;
						cases.Add(new ILValCase(val, label));
					}
					else
					{
						defaultCaseLabel = label;
					}
				}

				_builder.BuildSwitchValue(_lastValue, cases, defaultCaseLabel, syntax.Expression.FullSpan);
			}

			// Generate cases
			for (var i = 0; i < syntax.Cases.Count; i++)
			{
				SwitchCaseSyntax caseSyntax = syntax.Cases[i];
				string label = labels[i];
				_builder.CreateBasicBlock(label);

				if (caseSyntax.Statements != null)
					caseSyntax.Statements.ForEach(base.VisitStatement);
				SyntaxNode lastNode = caseSyntax.Statements?.LastOrDefault();
				if (!(lastNode is FallthroughSyntax))
				{
					_builder.BuildBranch(endLabel, null);
				}
				else
				{
					_builder.BuildBranch(labels[i + 1], null);
				}
			}

			_builder.CreateBasicBlock(endLabel);
		}

		protected override void VisitTemplateName(TemplateNameSyntax syntax)
		{
			_isTemplateArg = true;
			base.VisitTemplateName(syntax);
			_isTemplateArg = false;

			// Try to instantiate the template
			Symbol symbol = syntax.Context.Symbol;
			TemplateInstanceIdentifier instIden = syntax.Context.Identifier as TemplateInstanceIdentifier;

			// TODO: Should only happen for the impl type itself, not other types
			if (!_builder.HasTemplateInstance(symbol.ScopeVar) && !_inImplTemplate)
			{
				List<ILTemplateArg> args = new List<ILTemplateArg>();
				foreach (TemplateArgument arg in instIden.Arguments)
				{
					if (arg.Value != null)
					{
						ArgumentSyntax argSyn = syntax.Arguments[arg.Index];
						if (argSyn.Expression is IdentifierNameSyntax idenSyntax)
						{
							Symbol tmp = idenSyntax.Context.Symbol;
							if (tmp.Kind == SymbolKind.TemplateParam)
							{
								ILTemplateArg tArg = new ILTemplateArg(arg.Type, null);
								tArg.Value = idenSyntax.Identifier.Text;
								args.Add(tArg);

								continue;
							}
						}
					}
					
					{
						ILTemplateArg tArg = new ILTemplateArg(arg.Type, arg.ValueFunc);
						args.Add(tArg);
					}
				}
				
				TemplateInstanceSymbolType instType = symbol.Type as TemplateInstanceSymbolType;
				ScopeVariable scopeVar = new ScopeVariable(instType.TemplateSymbol.Scope, symbol.Type.GetIdentifier());
				_builder.BuildTemplateInstance(scopeVar, instType, args, syntax.FullSpan);
			}
		}

		protected override void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			base.VisitTemplateParameter(syntax);
		}

		protected override void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			base.VisitTernaryExpression(syntax);
		}

		protected override void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			base.VisitTransmuteExpression(syntax);

			ILOperand expr = _lastValue;
			SymbolType exprType = expr.Type;
			SymbolType castType = GetSpecializedType(syntax.Type.Context.Type);

			if (exprType == castType)
				return;

			if (expr.Type is MemoryLocSymbolType)
			{
				expr = _builder.BuildLoad(expr, syntax.Expression.FullSpan);
			}

			_lastValue = _builder.BuildBuiltin("bitcast", new List<ILOperand>{ expr }, castType, syntax.FullSpan);
		}

		protected override void VisitTupleType(TupleTypeSyntax syntax)
		{
			base.VisitTupleType(syntax);
		}

		protected override void VisitType(TypeSyntax syntax)
		{
			bool prev = _idenAsVar;
			_idenAsVar = false;
			base.VisitType(syntax);
			_idenAsVar = prev; 
		}

		protected override void VisitTypedef(TypedefSyntax syntax)
		{
			//base.VisitTypedef(syntax);

			Symbol symbol = syntax.Context.Symbol;
			SymbolType symType = GetSpecializedType(symbol.Type);

			List<ILTemplateParam> templateParams = null;
			if (syntax.Context.Identifier is TemplateDefinitionIdentifier def)
			{
				templateParams = new List<ILTemplateParam>(def.Parameters.Count);
				foreach (TemplateParameter param in def.Parameters)
				{
					string valueSpecFunc = null;
					if (param.ValueSpecializationExpr != null)
						valueSpecFunc = GenTemplateParamValueSpecFunc(param);

					string valueDefFunc = null;
					if (param.ValueDefaultExpr != null)
						valueDefFunc = GenTemplateParamValueDefaultFunc(param);

					ILTemplateParam tparam = new ILTemplateParam(param.ValueName, param.Type, param.TypeSpecialization, valueSpecFunc, valueDefFunc);
					templateParams.Add(tparam);
				}
			}

			_builder.BuildTypedef(symbol, symType, syntax.FullSpan);
		}

		protected override void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			//base.VisitIntrinsicExpression(syntax);
			ILCompilerIntrinType intrin;
			switch (syntax.IntrinsicToken.Type)
			{
			default:
			case TokenType.Sizeof:
			{
				intrin = ILCompilerIntrinType.Sizeof;
				break;
			}
			case TokenType.AlignOf:
			{
				intrin = ILCompilerIntrinType.Alignof;
				break;
			}
			case TokenType.Typeid:
			{
				// TODO
				goto case TokenType.Sizeof;
			}
			}

			if (syntax.Expression is TypeExpressionSyntax)
			{
				SymbolType type = GetSpecializedType(syntax.Expression.Context.Type);
				_lastValue = _builder.BuildCompilerIntrin(intrin, type, syntax.FullSpan);
			}
			/*else
			{
				VisitSimpleExpression(syntax.Expression);
				_lastValue = _builder.BuildCompilerIntrin(intrin, _lastValue, syntax.FullSpan);
			}*/
		}

		protected override void VisitTypeof(TypeofSyntax syntax)
		{
			// Expressions in typeof is only typedata, no IL needs to be generated
			//base.VisitTypeof(syntax);
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			bool prevIsGlobalVar = _isGlobalVar;
			_isGlobalVar = false;

			ILAttributes ilAttribs = ILHelpers.GetAttributes(syntax.Context.Symbol, _astCompileAttribs);
			Symbol unionSymbol = syntax.Context.Symbol;

			// Get template params, if there are any
			List<ILTemplateParam> templateParams = null;
			if (syntax.Context.Identifier is TemplateDefinitionIdentifier def)
			{
				templateParams = new List<ILTemplateParam>(def.Parameters.Count);
				foreach (TemplateParameter param in def.Parameters)
				{
					string valueSpecFunc = null;
					if (param.ValueSpecializationExpr != null)
						valueSpecFunc = GenTemplateParamValueSpecFunc(param);

					string valueDefFunc = null;
					if (param.ValueDefaultExpr != null)
						valueDefFunc = GenTemplateParamValueDefaultFunc(param);

					ILTemplateParam tparam = new ILTemplateParam(param.ValueName, param.Type, param.TypeSpecialization, valueSpecFunc, valueDefFunc);
					templateParams.Add(tparam);
				}
			}

			_builder.CreateAggregate(ILAggregateType.Union, unionSymbol.ScopeVar, unionSymbol.MangledName, syntax.Context.Symbol, ilAttribs, templateParams, syntax.FullSpan);

			List<VarTypePair> varList = ProcessAggregateMembers(syntax.Body, unionSymbol);

			// Generate initializers
			if (varList.Count > 0)
			{
				VTable vtable = CompilerContext.GetVTable(unionSymbol.ScopeVar);
				List<VTableMethod> methods = vtable.GetMethodsStartingWith("__init_");
				for (var i = 0; i < varList.Count; i++)
				{
					VTableMethod method = methods[i];
					VarTypePair pair = varList[i];

					ILAttributes funcAttribs = new ILAttributes(ilAttribs);
					bool isUninstantiated = syntax.TemplateParameters != null;

					ScopeVariable scopeVar = new ScopeVariable(new Scope(unionSymbol.ScopeVar), method.Iden);
					(List<ILOperand> ops, _) = _builder.BeginFunction(scopeVar, method.MangledFunc, method.Type, funcAttribs, templateParams, isUninstantiated, null);
					_builder.CreateBasicBlock("entry");

					ILOperand tmpStructOp = _builder.BuildStackVariable(method.Type.ReturnType, "union_addr", null);

					ILReference memberRef = new ILReference(unionSymbol.Identifier.GetSimpleName(), pair.Identifier);
					_builder.BuildUnionInsert(tmpStructOp, memberRef, ops[0], null);

					ILOperand retOp = _builder.BuildLoad(tmpStructOp, null);
					_builder.BuildReturn(retOp, null);
					_builder.EndFunction();
				}
			}

			_isGlobalVar = prevIsGlobalVar;
		}

		protected override void VisitUnitTest(UnitTestSyntax syntax)
		{
			base.VisitUnitTest(syntax);
		}

		protected override void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			base.VisitVariadicType(syntax);
		}

		protected override void VisitWhile(WhileSyntax syntax)
		{
			Scope subScope = syntax.Context.Scope.GetSubScope(_funcScope);

			string scopeName = subScope.ToString();
			string condName = scopeName + ".cond";
			string loopName = scopeName + ".loop";
			string endName = scopeName + ".end";

			_builder.BuildBranch(condName, syntax.Condition.FullSpan);
			_builder.CreateBasicBlock(condName);

			VisitSimpleExpression(syntax.Condition);
			_builder.BuildCondBranch(_lastValue, loopName, endName, syntax.Condition.FullSpan);

			_builder.CreateBasicBlock(loopName);

			base.VisitStatement(syntax.Body);

			_builder.BuildBranch(condName, syntax.FullSpan);

			_builder.CreateBasicBlock(endName);
		}

		protected override void VisitCompilerAtAttribute(CompilerAtAttributeSyntax syntax)
		{
			base.VisitCompilerAtAttribute(syntax);
		}

		List<VarTypePair> ProcessAggregateMembers(AggregateBodySyntax body, Symbol aggregate)
		{
			List<VarTypePair> varList = new List<VarTypePair>();
			int paddingIdx = 0;
			if (body.Declarations != null)
			{
				foreach (DeclarationSyntax declaration in body.Declarations)
				{
					if (declaration.Context.SymbolList != null)
					{
						foreach (Symbol symbol in declaration.Context.SymbolList)
						{
							string iden = symbol.Identifier.ToString();
							ScopeVariable scopeVar = symbol.ScopeVar;
							if (iden  == "_")
							{
								iden = $"__padding_{paddingIdx}";
								scopeVar = new ScopeVariable(scopeVar.Scope, new NameIdentifier(iden));
								++paddingIdx;
							}

							ILOperand op = _builder.BuildAggregateVariable(aggregate.ScopeVar, scopeVar.Name,symbol.MangledName, symbol.Type, declaration.FullSpan);
							_varMapping.Add(scopeVar, op);
							
							if (aggregate.Kind == SymbolKind.Struct)
								_structVars.Add(scopeVar);
							else if (aggregate.Kind == SymbolKind.Union)
								_unionVars.Add(scopeVar);

							varList.Add(new VarTypePair{ Identifier = iden, Type = symbol.Type });
						}
					}
					else
					{
						Symbol symbol = declaration.Context.Symbol;
						string iden = symbol.Identifier.ToString();
						ScopeVariable scopeVar = symbol.ScopeVar;
						if (iden == "_")
						{
							iden = $"__padding_{paddingIdx}";
							scopeVar = new ScopeVariable(scopeVar.Scope, new NameIdentifier(iden));
							++paddingIdx;
						}

						// Process base types
						List<TypeSyntax> baseTypes = GetBaseTypeSyntaxes(declaration.Type);
						foreach (TypeSyntax baseType in baseTypes)
						{
							if (baseType is InlineStructTypeSyntax inlineStruct)
							{
								VisitStruct(inlineStruct.StructSyntax);
							}
							else if (baseType is InlineUnionTypeSyntax inlineUnion)
							{
								VisitUnion(inlineUnion.UnionSyntax);
							}
							else if (baseType is DelegateSyntax delegateSyntax)
							{
								VisitDelegate(delegateSyntax);
							}
						}

						ILOperand op = _builder.BuildAggregateVariable(aggregate.ScopeVar, scopeVar.Name, symbol.MangledName, symbol.Type, declaration.FullSpan);
						_varMapping.Add(scopeVar, op);

						if (aggregate.Kind == SymbolKind.Struct)
							_structVars.Add(scopeVar);
						else if (aggregate.Kind == SymbolKind.Union)
							_unionVars.Add(scopeVar);

						varList.Add(new VarTypePair {Identifier = iden, Type = symbol.Type});
					}

					/*else if (statement is StructSyntax structSyntax)
					{
						VisitStruct(structSyntax);
					}
					else if (statement is UnionSyntax unionSyntax)
					{
						VisitUnion(unionSyntax);
					}*/
				}
			}

			return varList;
		}

		List<TypeSyntax> GetBaseTypeSyntaxes(TypeSyntax syntax)
		{
			List<TypeSyntax> typeSyntaxes = new List<TypeSyntax>();

			TypeSyntax subType = syntax;
			bool hasSubType = true;
			while (hasSubType)
			{
				switch (subType)
				{
				case PointerTypeSyntax pointer:
					subType = pointer.BaseType;
					break;
				case ReferenceTypeSyntax reference:
					subType = reference.BaseType;
					break;
				case ArrayTypeSyntax array:
					subType = array.BaseType;
					break;
				case AttributedTypeSyntax attributed:
					subType = attributed.BaseType;
					break;
				case NullableTypeSyntax nullable:
					subType = nullable.BaseType;
					break;
				case TupleTypeSyntax tuple:
				{
					foreach (TupleSubTypeSyntax tupleSubType in tuple.SubTypes)
					{
						List<TypeSyntax> res = GetBaseTypeSyntaxes(tupleSubType.BaseType);
						typeSyntaxes.AddRange(res);
					}
					break;
				}
				case VariadicTypeSyntax variadic:
					subType = variadic.BaseType;
					break;
				case BitFieldTypeSyntax bitField:
					subType = bitField.BaseType;
					break;
				default:
				{
					typeSyntaxes.Add(subType);
					hasSubType = false;
					break;
				}
				}
			}

			return typeSyntaxes;
		}

		ILOperand DefaultInitialize(SymbolType type, TextSpan span)
		{
			switch (type)
			{
			case BuiltinSymbolType builtin:
			{
				switch (builtin.Builtin)
				{
				case BuiltinTypes.Bool:
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				{
					return _builder.BuildIntLiteral(0, SymbolType.BuiltinType(BuiltinTypes.I32), span);
				}
				case BuiltinTypes.I64:
				{
					return _builder.BuildIntLiteral(0, SymbolType.BuiltinType(BuiltinTypes.I64), span);
				}
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.Char:
				case BuiltinTypes.WChar:
				case BuiltinTypes.Rune:
				{
					return _builder.BuildIntLiteral(0, SymbolType.BuiltinType(BuiltinTypes.U32), span);
				}
				case BuiltinTypes.U64:
				{
					return _builder.BuildIntLiteral(0, SymbolType.BuiltinType(BuiltinTypes.U64), span);
				}
				case BuiltinTypes.F32:
				{
					return _builder.BuildIntLiteral(0, SymbolType.BuiltinType(BuiltinTypes.F32), span);
				}
				case BuiltinTypes.F64:
				{
					return _builder.BuildIntLiteral(0, SymbolType.BuiltinType(BuiltinTypes.F64), span);
				}
				case BuiltinTypes.String:
					return _builder.BuildIntLiteral(0, SymbolType.BuiltinType(BuiltinTypes.Null), span);
				}
				break;
			}
			case PointerSymbolType pointer:
			{
				return _builder.BuildIntLiteral(0, pointer, span);
			}
			}

			return null;
		}

		ILOperand CastOperand(ILOperand op, SymbolType castRetType, ILOpType castType, TextSpan span)
		{
			// From string constant
			{
				if (op.Type is BuiltinSymbolType builtin && builtin.Builtin == BuiltinTypes.StringLiteral &&
				    ((castRetType is BuiltinSymbolType builtRet && builtRet.Builtin == BuiltinTypes.String) ||
				     (castRetType is PointerSymbolType ptrType && ptrType.BaseType is BuiltinSymbolType builtinBase &&
						(builtinBase.Builtin == BuiltinTypes.Char || 
						 builtinBase.Builtin == BuiltinTypes.WChar || 
						 builtinBase.Builtin == BuiltinTypes.Rune ||
						 builtinBase.Builtin == BuiltinTypes.Void))))
				{
					ILOperand tmp = _builder.BuildConvertType(op, castRetType, span);
					return tmp;
				}
			}

			// From null
			{
				if (castRetType is PointerSymbolType && op.Type is BuiltinSymbolType builtin && builtin.Builtin == BuiltinTypes.Null)
				{
					ILOperand tmp = _builder.BuildConvertType(op, castRetType, span);
					return tmp;
				}
			}

			// To simple enum
			{
				if (castRetType is EnumSymbolType enumType && enumType.IsSimpleEnum)
				{
					BuiltinSymbolType baseType = enumType.BaseType;
					// Cast to base type
					ILOperand tmp = op;
					if (op.Type != enumType.BaseType)
						tmp = CastOperand(op, baseType, ILOpType.Static, span);
					// Convert type
					tmp = _builder.BuildConvertType(tmp, castRetType, span);
					return tmp;
				}
			}

			// From simple enum
			{
				if (op.Type is EnumSymbolType enumType && enumType.IsSimpleEnum)
				{
					BuiltinSymbolType baseType = enumType.BaseType;
					// Convert to base type
					ILOperand tmp = _builder.BuildConvertType(op, castRetType, span);
					// Cast from base type
					if (op.Type != enumType.BaseType)
						tmp = CastOperand(tmp, baseType, ILOpType.Static, span);
					return tmp;
				}
			}

			// To void ptr
			{
				if (op.Type is PointerSymbolType && castRetType is PointerSymbolType ptrRet &&
				    ptrRet.BaseType is BuiltinSymbolType builtinRet && builtinRet.Builtin == BuiltinTypes.Void)
				{
					ILOperand tmp = _builder.BuildConvertType(op, castRetType, span);
					return tmp;
				}
			}

			// To Interface reference
			{
				if (castRetType is ReferenceSymbolType refType && refType.BaseType is InterfaceSymbolType interfaceType)
				{
					ILOperand dataPtr = null;
					if (op.Type is MemoryLocSymbolType ||
						op.Type is PointerSymbolType ||
					    op.Type is ReferenceSymbolType)
					{

					}
					else
					{
						// Create stack variable to store data
						ILOperand tmpStack = _builder.BuildStackVariable(op.Type, null, null);
						_builder.BuildAssign(op, tmpStack, null);

						dataPtr = _builder.BuildConvertType(tmpStack, SymbolType.PointerType(SymbolType.BuiltinType(BuiltinTypes.Void)), null);
					}

					// Create interface object
					Identifier[] interfaceObjIdens = { new NameIdentifier("core"), new NameIdentifier("builtin"), new NameIdentifier("InterfaceObj") };
					CompilerContext.ModuleImportDirective = new FileImportDirective("core", "builtin");
					Symbol interfaceObjSymbol = CompilerContext.FindType(Scope.Empty, new ScopeVariable(interfaceObjIdens));
					CompilerContext.ModuleImportDirective = null;
					SymbolType interfaceObjType = interfaceObjSymbol.Type;

					ILOperand interfaceObjAddr = _builder.BuildStackVariable(interfaceObjType, null, null);
					_builder.BuildStructInsert(interfaceObjAddr, new ILReference("InterfaceObj", "data"), dataPtr, null);

					// TODO: Compound interfaces
					ILOperand vtable = _builder.BuildGetVTable(op.Type, new List<SymbolType>{ interfaceType });
					_builder.BuildStructInsert(interfaceObjAddr, new ILReference("InterfaceObj", "vtable"), vtable, null);
					ILOperand interfaceObj = _builder.BuildLoad(interfaceObjAddr, null);
					return interfaceObj;
				}
			}

			ILOperand cast = _builder.BuildOperatorRef(ILOpLoc.Cast, castType, new List<SymbolType> { op.Type }, castRetType, span);

			ILOperand ret = _builder.BuildCall(cast, new List<ILOperand> { op }, castRetType, span);
			return ret;
		}

		ILGlobalFlags GetGlobalFlags(SemanticAttributes attribs, Dictionary<string, CompileAttribute> compileAttribs)
		{
			ILGlobalFlags flags = ILGlobalFlags.None;

			if ((attribs & SemanticAttributes.CConst) != 0)
				flags |= ILGlobalFlags.CompileConst;
			if ((attribs & SemanticAttributes.Const) != 0)
				flags |= ILGlobalFlags.Const;
			if ((attribs & SemanticAttributes.Immutable) != 0)
				flags |= ILGlobalFlags.Immutable;
			if ((attribs & SemanticAttributes.Synchronized) != 0)
				flags |= ILGlobalFlags.Synchronized;
			if ((attribs & SemanticAttributes.Shared) != 0)
				flags |= ILGlobalFlags.Shared;
			if ((attribs & SemanticAttributes.Global) == 0)
				flags |= ILGlobalFlags.Tls;

			return flags;
		}

		SymbolType GetSpecializedType(SymbolType type)
		{
			switch (type)
			{
			case ReferenceSymbolType refType:
			{
				SymbolType baseType = GetSpecializedType(refType.BaseType);
				return SymbolType.ReferenceType(baseType, type.Attributes);
			}
			case PointerSymbolType ptrType:
			{
				SymbolType baseType = GetSpecializedType(ptrType.BaseType);
				return SymbolType.PointerType(baseType, type.Attributes);
			}
			case ArraySymbolType arrType:
			{
				SymbolType baseType = GetSpecializedType(arrType.BaseType);
				return SymbolType.ArrayType(baseType, arrType.SizeExpr, arrType.ArraySize, type.Attributes);
			}
			case NullableSymbolType nullType:
			{
				SymbolType baseType = GetSpecializedType(nullType.BaseType);
				return SymbolType.NullableType(baseType, type.Attributes);
			}
			case TemplateParamSymbolType tparamType:
			{
				if (_templateTypeMapping.TryGetValue(tparamType, out SymbolType replType))
					return replType;
				return type;
			}
			case TemplateInstanceSymbolType instType:
			{
				TemplateInstanceIdentifier origIden = instType.Iden.Name as TemplateInstanceIdentifier;
				TemplateInstanceIdentifier instIden = new TemplateInstanceIdentifier(origIden.BaseIden);

				for (var i = 0; i < origIden.Arguments.Count; i++)
				{
					TemplateArgument origArg = origIden.Arguments[i];

					SymbolType tmp = GetSpecializedType(origArg.Type);
					instIden.Arguments[i] = new TemplateArgument(origArg.Index, tmp, origArg.Value);
				}

				return SymbolType.TemplateInstanceType(instType.Iden.Scope, instIden, instType.TemplateSymbol, type.Attributes);
			}
			case TupleSymbolType tupleType:
			{
				List<SymbolType> subTypes = new List<SymbolType>();
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					SymbolType subT = GetSpecializedType(subType);
					subTypes.Add(subT);
				}
				return SymbolType.TupleType(subTypes, type.Attributes);
			}
			case FunctionSymbolType funcType:
			{
				SymbolType recType = GetSpecializedType(funcType.ReceiverType);
				SymbolType retType = GetSpecializedType(funcType.ReturnType);

				List<SymbolType> paramTypes = null;
				if (funcType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType subType in funcType.ParamTypes)
					{
						SymbolType subT = GetSpecializedType(subType);
						paramTypes.Add(subT);
					}
				}

				return SymbolType.FunctionType(recType, paramTypes, retType, type.Attributes);
			}
			case AggregateSymbolType aggrType:
			{
				if (aggrType.Identifier.Name is NameIdentifier)
					return aggrType;

				TemplateDefinitionIdentifier defIden = aggrType.Identifier.Name as TemplateDefinitionIdentifier;
				TemplateInstanceIdentifier instIden = new TemplateInstanceIdentifier(defIden);
				for (var i = 0; i < defIden.Parameters.Count; i++)
				{
					TemplateParameter param = defIden.Parameters[i];
					if (_templateTypeMapping.TryGetValue(param.Type, out SymbolType replType))
					{
						instIden.Arguments[i].Type = replType;
					}
					else
					{
						instIden.Arguments[i].Type = param.Type;
					}

					// TODO: What about actual values???
					if (param.ValueName != null)
						instIden.Arguments[i].Value = param.ValueName;
				}

				ScopeVariable scopeVariable = new ScopeVariable(aggrType.Identifier.Scope, instIden);

				return SymbolType.TemplateInstanceType(scopeVariable, aggrType.Symbol, type.Attributes);
			}
			default:
				return type;
			}
		}

		string GenTemplateParamValueSpecFunc(TemplateParameter param)
		{
			string funcName = $"__tpvsi_{_templateParamValueSpecId}";
			++_templateParamValueSpecId;

			// Get the current basic block to reset the builder after template value initializer
			ILBasicBlock prevBB = _builder.GetCurrentBasicBlock();
			ILOperand prevLastVal = _lastValue;

			ILAttributes attribs = new ILAttributes(ILLinkage.Hidden);

			FunctionSymbolType funcType = SymbolType.FunctionType(null, null, param.ValueSpecializationExpr.Context.Type);

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(funcName));
			_builder.BeginFunction(scopeVar, funcName, funcType, attribs, null, false, param.ValueSpecializationExpr.FullSpan);
			_builder.CreateBasicBlock("entry");

			VisitSimpleExpression(param.ValueSpecializationExpr);
			_builder.BuildReturn(_lastValue, null);

			_builder.EndFunction();

			_lastValue = prevLastVal;

			// Reset basic block to continue the outer func
			_builder.SetCurrentBasicBlock(prevBB);

			return funcName;
		}

		string GenTemplateParamValueDefaultFunc(TemplateParameter param)
		{
			string funcName = $"__tpvdi_{_templateParamValueDefId}";
			++_templateParamValueDefId;

			// Get the current basic block to reset the builder after template value initializer
			ILBasicBlock prevBB = _builder.GetCurrentBasicBlock();
			ILOperand prevLastVal = _lastValue;

			ILAttributes attribs = new ILAttributes(ILLinkage.Hidden);

			FunctionSymbolType funcType = SymbolType.FunctionType(null, null, param.ValueDefaultExpr.Context.Type);

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(funcName));
			_builder.BeginFunction(scopeVar, funcName, funcType, attribs, null, false, param.ValueDefaultExpr.FullSpan);
			_builder.CreateBasicBlock("entry");

			VisitSimpleExpression(param.ValueDefaultExpr);
			_builder.BuildReturn(_lastValue, null);

			_builder.EndFunction();

			_lastValue = prevLastVal;

			// Reset basic block to continue the outer func
			_builder.SetCurrentBasicBlock(prevBB);

			return funcName;
		}

	}
}
