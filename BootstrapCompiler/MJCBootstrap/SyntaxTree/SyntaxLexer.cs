﻿using MJC.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MJC.SyntaxTree
{
	class SyntaxLexer
	{
		private string _source = "";
		private int _offset;
		private const char InvalidChar = Char.MaxValue;
		private SyntaxToken _curToken;
		private List<SyntaxTrivia> _curTrivia;
		private List<SyntaxToken> _tokens;

		static readonly char[] _endOfLine = new[] { '\r', '\n', '\u2028', '\u2029' };

		private static Dictionary<string, TokenType> _keywords = new Dictionary<string, TokenType>()
		{
			{ "alignof"             , TokenType.AlignOf },
			{ "as"                  , TokenType.As },
			{ "asm"                 , TokenType.Asm },
			{ "assert"              , TokenType.Assert },
			{ "bool"                , TokenType.Bool },
			{ "break"               , TokenType.Break },
			{ "byte"                , TokenType.Byte },
			{ "case"                , TokenType.Case },
			{ "cconst"              , TokenType.CConst },
			{ "char"                , TokenType.Char },
			{ "const"               , TokenType.Const },
			{ "continue"            , TokenType.Continue },
			{ "default"             , TokenType.Default },
			{ "defer"               , TokenType.Defer },
			{ "delegate"            , TokenType.Delegate },
			{ "delete"              , TokenType.Delete },
			{ "do"                  , TokenType.Do },
			{ "double"              , TokenType.Double },
			{ "else"                , TokenType.Else },
			{ "elif"                , TokenType.Elif },
			{ "enum"                , TokenType.Enum },
			{ "export"              , TokenType.Export },
			{ "fallthrough"         , TokenType.Fallthrough },
			{ "false"               , TokenType.False },
			{ "float"               , TokenType.Float },
			{ "for"                 , TokenType.For },
			{ "foreach"             , TokenType.Foreach },
			{ "func"                , TokenType.Func },
			{ "goto"                , TokenType.Goto },
			{ "if"                  , TokenType.If },
			{ "interface"           , TokenType.Interface },
			{ "immutable"           , TokenType.Immutable },
			{ "import"              , TokenType.Import },
			{ "int"                 , TokenType.Int },
			{ "isize"               , TokenType.ISize },
			{ "internal"            , TokenType.Internal },
			{ "is"                  , TokenType.Is },
			{ "in"                  , TokenType.In },
			{ "inout"               , TokenType.Inout },
			{ "impl"                , TokenType.Impl },
			{ "lazy"                , TokenType.Lazy },
			{ "long"                , TokenType.Long },
			{ "mixin"				, TokenType.Mixin },
			{ "module"              , TokenType.Module },
			{ "mutable"             , TokenType.Mutable },
			{ "namespace"           , TokenType.Namespace },
			{ "new"                 , TokenType.New },
			{ "null"                , TokenType.Null },
			{ "out"                 , TokenType.Out },
			{ "package"             , TokenType.Package },
			{ "private"             , TokenType.Private },
			{ "public"              , TokenType.Public },
			{ "return"              , TokenType.Return },
			{ "rune"                , TokenType.Rune },
			{ "self"				, TokenType.Self },
			{ "Self"				, TokenType.SelfType },
			{ "shared"              , TokenType.Shared },
			{ "short"               , TokenType.Short },
			{ "sizeof"              , TokenType.Sizeof },
			{ "static"              , TokenType.Static },
			{ "string"              , TokenType.String },
			{ "struct"              , TokenType.Struct },
			{ "switch"              , TokenType.Switch },
			{ "synchronized"        , TokenType.Synchronized },
			{ "transmute"           , TokenType.Transmute },
			{ "true"                , TokenType.True },
			{ "typealias"           , TokenType.TypeAlias },
			{ "typedef"             , TokenType.Typedef },
			{ "typeid"              , TokenType.Typeid },
			{ "typeof"              , TokenType.Typeof },
			{ "ubyte"               , TokenType.UByte },
			{ "uint"                , TokenType.UInt },
			{ "ulong"               , TokenType.ULong },
			{ "union"               , TokenType.Union },
			{ "unittest"            , TokenType.UnitTest },
			{ "ushort"              , TokenType.UShort },
			{ "usize"               , TokenType.USize },
			{ "void"                , TokenType.Void },
			{ "wchar"               , TokenType.WChar },
			{ "while"               , TokenType.While },
			{ "__c_cast"            , TokenType.UUCCast },
			{ "__global"            , TokenType.UUGlobal },
			{ "__FILE__"            , TokenType.SKFile },
			{ "__FILE_FULL_PATH__"  , TokenType.SKFileFullPath },
			{ "__PACKAGE__"         , TokenType.SKPackage },
			{ "__MODULE__"          , TokenType.SKModule },
			{ "__NAMESPACE__"       , TokenType.SKNamespace },
			{ "__LINE__"            , TokenType.SKLine },
			{ "__FUNC__"            , TokenType.SKFunc },
			{ "__PRETTY_FUNC__"     , TokenType.SKPrettyFunc },

			{"b8"					, TokenType.Bool},
			{"i8"					, TokenType.Byte},
			{"i16"                  , TokenType.Short},
			{"i32"                  , TokenType.Int},
			{"i64"                  , TokenType.Long},
			{"u8"                   , TokenType.UByte},
			{"u16"                  , TokenType.UShort},
			{"u32"                  , TokenType.UInt},
			{"u64"                  , TokenType.ULong},
			{"f32"                  , TokenType.Float},
			{"f64"                  , TokenType.Double},
		};

		public List<SyntaxToken> Parse(string source)
		{
			_source = source;
			_tokens = new List<SyntaxToken>();
			_curTrivia = new List<SyntaxTrivia>();
			_offset = 0;

			while (Peek() != InvalidChar)
			{
				bool setCurToken = true;
				switch (Peek())
				{
				case '+':
				{
					if (Peek(1) == '+')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.PlusPlus, "++"));
						AdvanceToken(2);
					}
					else if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.PlusEquals, "+="));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Plus, "+"));
						AdvanceToken();
					}
					break;
				}
				case '-':
				{
					if (Peek(1) == '-')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.MinusMinus, "--"));
						AdvanceToken(2);
					}
					else if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.MinusEquals, "-="));
						AdvanceToken(2);
					}
					else if (Peek(1) == '>')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.Arrow, "->"));
						AdvanceToken(2);
					}
					else if (char.IsDigit(Peek(1)) || Peek(1) == '.')
					{
						ParseArithmaticLiteral();
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Minus, "-"));
						AdvanceToken();
					}
					break;
				}
				case '*':
				{
					if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.AsteriskEquals, "*="));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Asterisk, "*"));
						AdvanceToken();
					}
					break;
				}
				case '/':
				{
					if (Peek(1) == '/')
					{
						ParseSingleLineComment();
						setCurToken = false;
					}
					else if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.AsteriskEquals, "/="));
						AdvanceToken(2);
					}
					else if (Peek(1) == '*')
					{
						ParseBlockComment();
						setCurToken = false;
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Asterisk, "/"));
						AdvanceToken();
					}
					break;
				}
				case '|':
				{
					if (Peek(1) == '|')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.OrOr, "||"));
						AdvanceToken(2);
					}
					else if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.OrEquals, "|="));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Or, "|"));
						AdvanceToken();
					}
					break;
				}
				case '&':
				{
					if (Peek(1) == '&')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.AndAnd, "&&"));
						AdvanceToken(2);
					}
					else if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.AndEquals, "&="));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.And, "&"));
						AdvanceToken();
					}
					break;
				}
				case '%':
				{
					if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.ModuloEquals, "%="));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Modulo, "%"));
						AdvanceToken();
					}
					break;
				}
				case '~':
				{
					if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.TildeEquals, "~="));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Tilde, "~"));
						AdvanceToken();
					}
					break;
				}
				case '!':
				{
					if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.ExclaimEquals, "!="));
						AdvanceToken(2);
					}
					else if (Peek(1) == '<')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.ExclaimLess, "!<"));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Exclaim, "!"));
						AdvanceToken();
					}
					break;
				}
				case '=':
				{
					if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.EqualsEquals, "=="));
						AdvanceToken(2);
					}
					else if (Peek(1) == '>')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.DblArrow, "=>"));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Equals, "="));
						AdvanceToken();
					}
					break;
				}
				case ':':
				{
					if (Peek(1) == ':')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.ColonColon, "::"));
						AdvanceToken(2);
					}
					else if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.ColonEquals, ":="));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Colon, ":"));
						AdvanceToken();
					}
					break;
				}
				case '?':
				{
					if (Peek(1) == '?')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.QuestionQuestion, "??"));
						AdvanceToken(2);
					}
					else if (Peek(1) == '.')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.QuestionDot, "?."));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Question, "?"));
						AdvanceToken();
					}
					break;
				}
				case '^':
				{
					if (Peek(1) == '^')
					{
						if (Peek(2) == '=')
						{
							AddToken(new SyntaxToken(new TextSpan(_offset, 3), TokenType.CaretCaretEquals, "^^="));
							AdvanceToken(3);
						}
						else
						{
							AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.CaretCaret, "^^"));
							AdvanceToken(2);
						}
					}
					else if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.CaretEquals, "^="));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Caret, "^"));
						AdvanceToken();
					}
					break;
				}
				case '<':
				{
					if (Peek(1) == '<')
					{
						if (Peek(2) == '=')
						{
							AddToken(new SyntaxToken(new TextSpan(_offset, 3), TokenType.LessLessEquals, "<<="));
							AdvanceToken(3);
						}
						else
						{
							AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.LessLess, "<<"));
							AdvanceToken(2);
						}
					}
					else if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.LessEquals, "<="));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Less, "<"));
						AdvanceToken();
					}
					break;
				}
				case '>':
				{
					if (Peek(1) == '>')
					{
						if (Peek(2) == '=')
						{
							AddToken(new SyntaxToken(new TextSpan(_offset, 3), TokenType.GreaterGreaterEquals, ">>="));
							AdvanceToken(3);
						}
						else if (Peek(2) == '>')
						{
							if (Peek(3) == '=')
							{
								AddToken(new SyntaxToken(new TextSpan(_offset, 3), TokenType.GreaterGreaterGreaterEquals, ">>>="));
								AdvanceToken(4);
							}
							else
							{
								AddToken(new SyntaxToken(new TextSpan(_offset, 3), TokenType.GreaterGreaterGreater, ">>>"));
								AdvanceToken(3);
							}
						}
						else
						{
							AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.GreaterGreater, ">>"));
							AdvanceToken(2);
						}
					}
					else if (Peek(1) == '=')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.GreaterEquals, ">="));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Greater, ">"));
						AdvanceToken();
					}
					break;
				}
				case '.':
				{
					if (Peek(1) == '.')
					{
						if (Peek(2) == '.')
						{
							AddToken(new SyntaxToken(new TextSpan(_offset, 3), TokenType.DotDotDot, "..."));
							AdvanceToken(3);
						}
						else
						{
							AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.DotDot, ".."));
							AdvanceToken(2);
						}
					}
					else if (Char.IsNumber(Peek(1)) && !IsKeywordOrIden(_tokens.LastOrDefault()))
					{
						ParseArithmaticLiteral();
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Dot, "."));
						AdvanceToken();
					}
					break;
				}
				case '(':
				{
					AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.LParen, "("));
					AdvanceToken();
					break;
				}
				case ')':
				{
					AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.RParen, ")"));
					AdvanceToken();
					break;
				}
				case '[':
				{
					AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.LBracket, "["));
					AdvanceToken();
					break;
				}
				case ']':
				{
					AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.RBracket, "]"));
					AdvanceToken();
					break;
				}
				case '{':
				{
					AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.LBrace, "{"));
					AdvanceToken();
					break;
				}
				case '}':
				{
					AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.RBrace, "}"));
					AdvanceToken();
					break;
				}
				case ',':
				{
					AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Comma, ","));
					AdvanceToken();
					break;
				}
				case ';':
				{
					AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Semicolon, ";"));
					AdvanceToken();
					break;
				}
				case '@':
				{
					if (Peek(1) == ':')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.AtColon, "@:"));
						AdvanceToken(2);
					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.At, "@"));
						AdvanceToken();
					}
					break;
				}
				case '#':
				{
					if (Peek(1) == '{')
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 2), TokenType.HashBrace, "#{"));
						AdvanceToken(2);
					}
					else if (Peek(1) == ':')
					{
						if (_source.IndexOf("debug", _offset + 2, 5, StringComparison.Ordinal) != -1)
						{
							AddToken(new SyntaxToken(new TextSpan(_offset, 7), TokenType.Debug, "#:debug"));
							AdvanceToken(7);
						}
						else if (_source.IndexOf("version", _offset + 2, 7, StringComparison.Ordinal) != -1)
						{
							AddToken(new SyntaxToken(new TextSpan(_offset, 9), TokenType.Version, "#:version"));
							AdvanceToken(9);
						}
						else
						{
							// TODO: Error
						}

					}
					else
					{
						AddToken(new SyntaxToken(new TextSpan(_offset, 1), TokenType.Hash, "#"));
						AdvanceToken();
					}

					break;
				}
				case '"':
				{
					ParseString();
					break;
				}

				case '\'':
				{
					ParseCharacter();
					break;
				}
				case '\t':
				case '\u000B':
				case '\u000C':
				case ' ':
				{
					ParseWhitespace();
					setCurToken = false;
					break;
				}
				case '\r':
				case '\n':
				case '\u2028':
				case '\u2029':
				{
					ParseEndOfLine();
					setCurToken = false;
					break;
				}
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				{
					ParseArithmaticLiteral();
					break;
				}

				default:
				{
					ParseIdentifierOrKeyword();
					break;
				}
				}

				if (setCurToken)
					_curToken = _tokens[_tokens.Count - 1];

			}

			SyntaxToken eof = new SyntaxToken(new TextSpan(_offset, 0), TokenType.EndOfFile, "");
			eof.LeadTrivia.AddRange(_curTrivia);
			_tokens.Add(eof);

			return _tokens;
		}

		#region Parsing

		private void ParseSingleLineComment() // test
		{
			int start = _offset;

			EndOfLineTrivia eol = FindAndParseEndOfLine();

			int end = eol.Span.Start;
			int len = end - start;
			SingleLineCommentTrivia comment = new SingleLineCommentTrivia(new TextSpan(start, len), _source.Substring(start, len));

			AddTrivia(comment);
			AddTrivia(eol, true);

			_offset = eol.Span.End;
		}

		private void ParseBlockComment()
		{
			const string beginChars = "/*";
			const string endChars = "*/";

			int next = _source.IndexOf(beginChars, _offset + 2, StringComparison.Ordinal);
			int end = _source.IndexOf(endChars, _offset + 2, StringComparison.Ordinal);

			if (next != -1 && next < end)
			{
				int indent = 1;
				while (indent > 0)
				{
					int floc;
					if (next < end)
					{
						++indent;
						floc = next + 2;
					}
					else
					{
						--indent;
						floc = end + 2;
					}

					next = _source.IndexOf(beginChars, floc, StringComparison.Ordinal);
					end = _source.IndexOf(endChars, floc, StringComparison.Ordinal);
				}
			}

			int len = end - _offset + 2;
			BlockCommentTrivia comment = new BlockCommentTrivia(new TextSpan(_offset, len), _source.Substring(_offset, len));
			AdvanceToken(len);

			AddTrivia(comment);

			_offset = comment.Span.End;
		}

		private EndOfLineTrivia FindAndParseEndOfLine()
		{
			int end = _source.IndexOfAny(_endOfLine, _offset);

			if (end == -1)
				return null;

			if (_source[end] == '\r' && end + 1 < _source.Length && _source[end + 1] == '\n')
				return new EndOfLineTrivia(new TextSpan(end, 2), _source.Substring(end, 2));

			return new EndOfLineTrivia(new TextSpan(end, 1), _source[end].ToString());
		}

		private void ParseEndOfLine()
		{
			EndOfLineTrivia trivia;
			if (_source[_offset] == '\r' && _offset + 1 < _source.Length && _source[_offset + 1] == '\n')
			{
				trivia = new EndOfLineTrivia(new TextSpan(_offset, 2), _source.Substring(_offset, 2));
				AdvanceToken(2);
			}
			else
			{
				trivia = new EndOfLineTrivia(new TextSpan(_offset, 1), $"{_source[_offset]}");
				AdvanceToken();
			}

			AddTrivia(trivia, true);
		}

		private SyntaxToken ParseIdentifierOrKeyword()
		{
			int len = 0;
			while (Char.IsLetterOrDigit(Peek(len)) || Peek(len) == '_')
			{
				++len;
			}
			string text = _source.Substring(_offset, len);

			if (_keywords.ContainsKey(text))
			{
				TokenType type = _keywords[text];
				AddToken(new SyntaxToken(new TextSpan(_offset, len), type, text));
			}
			else
			{
				AddToken(new SyntaxToken(new TextSpan(_offset, len), TokenType.Identifier, text));
			}

			_offset += len;

			return null;
		}

		private void ParseWhitespace()
		{
			string whiteSpace = "";

			int offset = 0;
			while (IsWhiteSpace(Peek(offset)))
			{
				whiteSpace += _source[_offset + offset];
				++offset;
			}
			
			WhiteSpaceTrivia comment = new WhiteSpaceTrivia(new TextSpan(_offset, offset), whiteSpace);
			AdvanceToken(offset);

			if (_curToken != null)
			{
				_curToken.TrailTrivia.Add(comment);
				_curToken = null;
			}
			else
			{
				_curTrivia.Add(comment);
			}

			_offset = comment.Span.End;
		}

		private bool IsWhiteSpace(char c)
		{
			return c == '\t' ||
			       c == ' ' ||
			       c == '\u000B' ||
			       c == '\u000C';
		}

		private void ParseArithmaticLiteral()
		{
			if (Peek() == '0')
			{
				char c = Peek(1);
				if (c == 'b' || c == 'B')
				{
					ParseBinaryLiteral();
				}
				else if (c == 'x' || c == 'X')
				{
					ParseHexadecimalLiteral();
				}
				else if (c == '.')
				{
					ParseDecimalLiteral();
				}
				else if ("01234567'_".IndexOf(Peek(1)) != -1)
				{
					ParseOctalLiteral();
				}
				else
				{
					ParseDecimalLiteral();
				}
			}
			else
			{
				ParseDecimalLiteral();
			}

		}

		private void ParseBinaryLiteral()
		{
			const string validBin = "01'_";
			string text = _source.Substring(_offset, 2);
			int offset = 2;
			while (validBin.IndexOf(Peek(offset)) != -1)
			{
				text += _source[_offset + offset];
				++offset;
			}

			TokenType type = GetArithmeticLiteralType(ref offset);

			// Actual conversion
			string valString = "";
			for (int i = 2; i < text.Length; ++i)
			{
				char ch = text[i];
				if (ch != '\'' && ch != '_')
					valString += ch;
				else if (validBin.IndexOf(ch) == -1)
					break;
			}

			if (type == TokenType.U8Literal ||
			    type == TokenType.U16Literal ||
			    type == TokenType.U32Literal ||
			    type == TokenType.U64Literal)
			{
				ulong value = Convert.ToUInt64(valString, 16);
				
				type = type == TokenType.Unknown ? (value <= UInt32.MaxValue ? TokenType.U32Literal : TokenType.U64Literal) : type;

				AddToken(new SyntaxToken(new TextSpan(_offset, offset), type, text, value));
			}
			else
			{
				long value = Convert.ToInt64(valString, 16);

				type = type == TokenType.Unknown ? (value <= Int32.MaxValue ? TokenType.I32Literal : TokenType.I64Literal) : type;

				AddToken(new SyntaxToken(new TextSpan(_offset, offset), type, text, value));
			}
			_offset += offset;
		}

		private void ParseOctalLiteral()
		{
			const string validOct = "01234567'_";
			string text = "";
			int offset = 0;
			while (validOct.IndexOf(Peek(offset)) != -1)
			{
				text += _source[_offset + offset];
				++offset;
			}

			TokenType type = GetArithmeticLiteralType(ref offset);

			// Actual conversion
			string valString = "";
			for (int i = 1; i < text.Length; ++i)
			{
				char ch = text[i];
				if (ch != '\'' && ch != '_')
					valString += ch;
				else if (validOct.IndexOf(ch) == -1)
					break;
			}

			if (type == TokenType.U8Literal ||
			    type == TokenType.U16Literal ||
			    type == TokenType.U32Literal ||
			    type == TokenType.U64Literal)
			{
				ulong value = Convert.ToUInt64(valString, 8);
				
				type = type == TokenType.Unknown ? (value <= UInt32.MaxValue ? TokenType.U32Literal : TokenType.U64Literal) : type;

				AddToken(new SyntaxToken(new TextSpan(_offset, offset), type, text, value));
			}
			else
			{
				long value = Convert.ToInt64(valString, 8);

				type = type == TokenType.Unknown ? (value <= Int32.MaxValue ? TokenType.I32Literal : TokenType.I64Literal) : type;

				AddToken(new SyntaxToken(new TextSpan(_offset, offset), type, text, value));
			}
			_offset += offset;
		}

		private void ParseHexadecimalLiteral()
		{
			const string validHex = "0123456789aAbBcCdDeEfF'_";
			string text = _source.Substring(_offset, 2);
			int offset = 2;
			while (validHex.IndexOf(Peek(offset)) != -1)
			{
				text += _source[_offset + offset];
				++offset;
			}

			TokenType type = GetArithmeticLiteralType(ref offset);

			// Actual conversion
			string valString = "";
			for (int i = 2; i < text.Length; ++i)
			{
				char ch = text[i];
				if (ch != '\'' && ch != '_')
					valString += ch;
				else if (validHex.IndexOf(ch) == -1)
					break;
			}

			if (type == TokenType.U8Literal ||
			    type == TokenType.U16Literal ||
			    type == TokenType.U32Literal ||
			    type == TokenType.U64Literal)
			{
				ulong value = Convert.ToUInt64(valString, 16);

				type = type == TokenType.Unknown ? (value <= UInt32.MaxValue ? TokenType.U32Literal : TokenType.U64Literal) : type;

				AddToken(new SyntaxToken(new TextSpan(_offset, offset), type, text, value));
			}
			else
			{
				long value = Convert.ToInt64(valString, 16);

				type = type == TokenType.Unknown ? (value <= Int32.MaxValue ? TokenType.I32Literal : TokenType.I64Literal) : type;

				AddToken(new SyntaxToken(new TextSpan(_offset, offset), type, text, value));
			}
			_offset += offset;
		}

		private void ParseDecimalLiteral()
		{
			bool dotEncountered = false;

			bool neg = false;
			if (Peek() == '-')
			{
				neg = true;
				++_offset;
			}

			int offset = 0;
			string text = "";
			while (Char.IsNumber(Peek(offset)) || (!dotEncountered && Peek(offset) == '.'))
			{
				if (_source[_offset + offset] == '.' && (Char.IsNumber(Peek(offset + 1)) || Peek(offset + 1) == 'f'))
					dotEncountered = true;
				else if (_source[_offset + offset] == '.')
					break;

				text += _source[_offset + offset];
				++offset;
			}

			TokenType type = GetArithmeticLiteralType(ref offset);

			if (dotEncountered || type == TokenType.F32Literal || type == TokenType.F64Literal)
			{
				// Actual conversion
				string valString = "";
				for (int i = 0; i < text.Length; ++i)
				{
					char ch = text[i];
					if (ch != '\'' && ch != '_')
						valString += ch;
					else if (!Char.IsDigit(ch) && ch != '\'' && ch != '_')
						break;
				}

				double value = Convert.ToDouble(valString);
				
				type = type == TokenType.Unknown ? TokenType.F32Literal : type;

				AddToken(new SyntaxToken(new TextSpan(_offset, offset), type, text, value));
			}
			else
			{
				// Actual conversion
				string valString = "";
				for (int i = 0; i < text.Length; ++i)
				{
					char ch = text[i];
					if (ch != '\'' && ch != '_')
						valString += ch;
					else if (!Char.IsDigit(ch) && ch != '\'' && ch != '_')
						break;
				}

				if (type == TokenType.U8Literal ||
				    type == TokenType.U16Literal ||
				    type == TokenType.U32Literal ||
				    type == TokenType.U64Literal)
				{
					ulong value = Convert.ToUInt64(valString);
					
					type = type == TokenType.Unknown ? (value <= UInt32.MaxValue ? TokenType.U32Literal : TokenType.U64Literal) : type;

					AddToken(new SyntaxToken(new TextSpan(_offset, offset), type, text, value));
				}
				else
				{
					long value = Convert.ToInt64(valString);

					type = type == TokenType.Unknown ? (value <= Int32.MaxValue ? TokenType.I32Literal : TokenType.I64Literal) : type;

					if (neg)
						value = -value;

					AddToken(new SyntaxToken(new TextSpan(_offset, offset), type, text, value));
				}
			}

			_offset += offset;
		}

		TokenType GetArithmeticLiteralType(ref int offset)
		{
			char c = Peek(offset);
			switch (c)
			{
			case 'i':
			{
				++offset;
				string len = Peek(offset).ToString();
				++offset;
				if (len == "8")
					return TokenType.I8Literal;
				
				len += Peek(offset);
				++offset;

				switch (len)
				{
				case "16":
					return TokenType.I16Literal;
				case "64":
					return TokenType.I64Literal;
				default:
					return TokenType.I32Literal;
				}
			}
			case 'u':
			{
				++offset;
				string len = Peek(offset).ToString();
				++offset;
				if (len == "8")
					return TokenType.U8Literal;
				
				len += Peek(offset);
				++offset;

				switch (len)
				{
				case "16":
					return TokenType.U16Literal;
				case "64":
					return TokenType.U64Literal;
				default:
					return TokenType.U32Literal;
				}
			}
			case 'f':
			{
				string len = Peek(offset + 1) + "" + Peek(offset + 2);
				offset += 3;

				if (len == "64")
					return TokenType.F64Literal;
				return TokenType.F32Literal;
			}
			}

			return TokenType.Unknown;
		}

		void ParseString()
		{
			string value = "";
			value += Peek();
			int offset = 1;
			while (true)
			{
				char c = Peek(offset);
				value += c;
				++offset;
				if (c == '"' && !value.EndsWith("\\"))
					break;
			}
			AddToken(new SyntaxToken(new TextSpan(_offset, 0), TokenType.StringLiteral, value));
			_offset += offset;
		}

		void ParseCharacter()
		{
			string value = "";

			int offset = 1;
			while (true)
			{
				char c = Peek(offset);
				++offset;
				if (c == '\'' && !value.EndsWith("\\"))
					break;
				else
					value += c;
			}
			AddToken(new SyntaxToken(new TextSpan(_offset, 0), TokenType.CharLiteral, value, char.Parse(value)));
			_offset += offset;
		}

		#endregion

		#region Utils

		void AddToken(SyntaxToken token)
		{
			if (_curTrivia.Count > 0)
			{
				token.LeadTrivia.AddRange(_curTrivia);
				_curTrivia.Clear();
			}
			_tokens.Add(token);
		}

		void AddTrivia(SyntaxTrivia trivia, bool reset = false)
		{
			if (_curToken != null)
			{
				_curToken.TrailTrivia.Add(trivia);
				if (reset)
					_curToken = null;
			}
			else
			{
				_curTrivia.Add(trivia);
			}
		}

		private char Peek()
		{
			if (_offset >= _source.Length)
				return InvalidChar;
			return _source[_offset];
		}

		private char Peek(int offset)
		{
			offset += _offset;
			if (offset >= _source.Length)
				return InvalidChar;
			return _source[offset];
		}

		private void AdvanceToken()
		{
			++_offset;
		}

		private void AdvanceToken(int n)
		{
			_offset += n;
		}

		private bool IsKeywordOrIden(SyntaxToken token)
		{
			switch (token.Type)
			{
			case TokenType.Identifier:
			case TokenType.AlignOf:
			case TokenType.As:
			case TokenType.Asm:
			case TokenType.Assert:
			case TokenType.Bool:
			case TokenType.Break:
			case TokenType.Byte:
			case TokenType.Case:
			case TokenType.CConst:
			case TokenType.Char:
			case TokenType.Const:
			case TokenType.Continue:
			case TokenType.Debug:
			case TokenType.Default:
			case TokenType.Defer:
			case TokenType.Delegate:
			case TokenType.Delete:
			case TokenType.Do:
			case TokenType.Double:
			case TokenType.Else:
			case TokenType.Elif:
			case TokenType.Enum:
			case TokenType.Export:
			case TokenType.Fallthrough:
			case TokenType.False:
			case TokenType.Float:
			case TokenType.For:
			case TokenType.Foreach:
			case TokenType.Func:
			case TokenType.Goto:
			case TokenType.If:
			case TokenType.Interface:
			case TokenType.Immutable:
			case TokenType.Import:
			case TokenType.Int:
			case TokenType.Internal:
			case TokenType.Is:
			case TokenType.ISize:
			case TokenType.In:
			case TokenType.Inout:
			case TokenType.Impl:
			case TokenType.Lazy:
			case TokenType.Long:
			case TokenType.Mixin:
			case TokenType.Module:
			case TokenType.Mutable:
			case TokenType.Namespace:
			case TokenType.New:
			case TokenType.Null:
			case TokenType.Out:
			case TokenType.Package:
			case TokenType.Private:
			case TokenType.Public:
			case TokenType.Return:
			case TokenType.Rune:
			case TokenType.Self:
			case TokenType.SelfType:
			case TokenType.Shared:
			case TokenType.Short:
			case TokenType.Sizeof:
			case TokenType.Static:
			case TokenType.String:
			case TokenType.Struct:
			case TokenType.Switch:
			case TokenType.Synchronized:
			case TokenType.Transmute:
			case TokenType.True:
			case TokenType.TypeAlias:
			case TokenType.Typedef:
			case TokenType.Typeid:
			case TokenType.Typeof:
			case TokenType.UByte:
			case TokenType.UInt:
			case TokenType.ULong:
			case TokenType.Union:
			case TokenType.UnitTest:
			case TokenType.UShort:
			case TokenType.USize:
			case TokenType.Version:
			case TokenType.Void:
			case TokenType.WChar:
			case TokenType.While:
			case TokenType.UUGlobal:
				return true;
			default:
				return false;
			}
		}

		#endregion

	}
}
