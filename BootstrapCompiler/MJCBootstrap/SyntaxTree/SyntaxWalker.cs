﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.SyntaxTree
{
	public class SyntaxWalker
	{
		public virtual void Visit(SyntaxTree tree)
		{
			VisitCompilationUnit(tree.CompilationUnit);
		}

		protected virtual void VisitCompilationUnit(CompilationUnitSyntax syntax)
		{
			if (syntax.Package != null)
				VisitPackageDirective(syntax.Package);
			if (syntax.Module != null)
				VisitModuleDirective(syntax.Module);
			if (syntax.Block != null)
				VisitBlockStatement(syntax.Block);
		}

		protected virtual void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
			VisitName(syntax.Name);
		}

		protected virtual void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitIdentifierName(syntax.Name);
		}

		protected virtual void VisitName(NameSyntax syntax)
		{
			switch (syntax)
			{
			case QualifiedNameSyntax qualifiedName:
				VisitQualifiedName(qualifiedName);
				break;
			case IdentifierNameSyntax identifierName:
				VisitIdentifierName(identifierName);
				break;
			case TemplateNameSyntax templateName:
				VisitTemplateName(templateName);
				break;
			case NameListSyntax nameList:
				VisitNameList(nameList);
				break;
			}
		}

		protected virtual void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
			if (syntax.Attribs != null)
				VisitAttributes(syntax.Attribs);
			if (syntax.Left != null)
				VisitName(syntax.Left);
			VisitName(syntax.Right);
		}

		protected virtual void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
			if (syntax.Attribs != null)
				VisitAttributes(syntax.Attribs);
		}

		protected virtual void VisitTemplateName(TemplateNameSyntax syntax)
		{
			if (syntax.Attribs != null)
				VisitAttributes(syntax.Attribs);
			syntax.Arguments?.ForEach(VisitArgument);
		}
		
		protected virtual void VisitNameList(NameListSyntax syntax)
		{
			foreach (NameListElemSyntax name in syntax.Names)
			{
				VisitName(name.Name);
			}
		}

		protected virtual void VisitAttributes(AttributeSyntax syntax)
		{
			syntax.AtAttribs?.ForEach(VisitAtAttribute);
			syntax.CompilerAtAttribs?.ForEach(VisitCompilerAtAttribute);
			syntax.SingleAttribs?.ForEach(VisitSingleAttribute);
		}

		protected virtual void VisitAtAttribute(AtAttributeSyntax syntax)
		{
			VisitName(syntax.Name);
			if (syntax.OpenParenToken != null)
			{
				syntax.Arguments.ForEach(VisitArgument);
			}
		}

		protected virtual void VisitCompilerAtAttribute(CompilerAtAttributeSyntax syntax)
		{
			VisitName(syntax.Name);
			if (syntax.OpenParenToken != null)
			{
				syntax.Arguments.ForEach(VisitArgument);
			}
		}

		protected virtual void VisitSingleAttribute(SingleAttributeSyntax syntax)
		{
		}
		
		protected virtual void VisitArgument(ArgumentSyntax syntax)
		{
			if (syntax.Identifier != null)
			{
				VisitIdentifierName(syntax.Identifier);
			}
			VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitParameter(ParameterSyntax syntax)
		{
			VisitIdentifierName(syntax.Identifier);
			if (syntax.Type != null)
				VisitType(syntax.Type);
			if (syntax.DefaultValue != null)
				VisitSimpleExpression(syntax.DefaultValue);
		}

		protected virtual void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			VisitIdentifierName(syntax.Identifier);
			if (syntax.Type != null)
				VisitType(syntax.Type);
			if (syntax.SpecializedValue != null)
				VisitSimpleExpression(syntax.SpecializedValue);
			if (syntax.DefaultValue != null)
				VisitSimpleExpression(syntax.DefaultValue);
		}

		protected virtual void VisitType(TypeSyntax syntax)
		{
			switch (syntax)
			{
			case BuiltinTypeSyntax builtinTypeSyntax:
				VisitBuiltinType(builtinTypeSyntax);
				break;
			case IdentifierTypeSyntax identifierTypeSyntax:
				VisitIdentifierType(identifierTypeSyntax);
				break;
			case PointerTypeSyntax pointerTypeSyntax:
				VisitPointerType(pointerTypeSyntax);
				break;
			case ReferenceTypeSyntax refSyntax:
				VisitReferenceType(refSyntax);
				break;
				case ArrayTypeSyntax arrayTypeSyntax:
				VisitArrayType(arrayTypeSyntax);
				break;
			case AttributedTypeSyntax attributedTypeSyntax:
				VisitAttributedType(attributedTypeSyntax);
				break;
			case TupleTypeSyntax tupleTypeSyntax:
				VisitTupleType(tupleTypeSyntax);
				break;
			case NullableTypeSyntax nullableTypeSyntax:
				VisitNullableType(nullableTypeSyntax);
				break;
			case VariadicTypeSyntax variadicTypeSyntax:
				VisitVariadicType(variadicTypeSyntax);
				break;
			case TypeofSyntax typeofSyntax:
				VisitTypeof(typeofSyntax);
				break;
			case DelegateSyntax delegateSyntax:
				VisitDelegate(delegateSyntax);
				break;
			case InlineStructTypeSyntax inlineStructSyntax:
				VisitInlineStruct(inlineStructSyntax);
				break;
			case InlineUnionTypeSyntax inlineUnionSyntax:
				VisitInlineUnion(inlineUnionSyntax);
				break;
			case BitFieldTypeSyntax bitFieldSyntax:
				VisitBitField(bitFieldSyntax);
				break;
			}
		}

		protected virtual void VisitBuiltinType(BuiltinTypeSyntax syntax)
		{
		}

		protected virtual void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
			VisitName(syntax.Name);
		}

		protected virtual void VisitPointerType(PointerTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
		}

		protected virtual void VisitReferenceType(ReferenceTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
		}

		protected virtual void VisitArrayType(ArrayTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
			if (syntax.SizeExpression != null)
				VisitSimpleExpression(syntax.SizeExpression);
		}

		protected virtual void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			VisitAttributes(syntax.Attributes);
			VisitType(syntax.BaseType);
		}

		protected virtual void VisitTupleType(TupleTypeSyntax syntax)
		{
			foreach (TupleSubTypeSyntax subType in syntax.SubTypes)
			{
				VisitType(subType.BaseType);
			}
		}

		protected virtual void VisitNullableType(NullableTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
		}

		protected virtual void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
		}

		protected virtual void VisitTypeof(TypeofSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitInlineStruct(InlineStructTypeSyntax syntax)
		{
			VisitStruct(syntax.StructSyntax);
		}

		protected virtual void VisitInlineUnion(InlineUnionTypeSyntax syntax)
		{
			VisitUnion(syntax.UnionSyntax);
		}

		protected virtual void VisitDelegate(DelegateSyntax syntax)
		{
			if (syntax.Identifier != null)
				VisitIdentifierName(syntax.Identifier);
			if (syntax.Parameters != null)
			{
				foreach (ParameterSyntax parameter in syntax.Parameters)
				{
					VisitParameter(parameter);
				}
			}
			if (syntax.ArrowToken != null)
			{
				VisitType(syntax.ReturnType);
			}
		}

		protected void VisitStatement(StatementSyntax syntax)
		{
			switch (syntax)
			{
			case BlockStatementSyntax blockStatementSyntax:
				VisitBlockStatement(blockStatementSyntax);
				break;
			case NamespaceSyntax namespaceSyntax:
				VisitNamespace(namespaceSyntax);
				break;
			case ImportSyntax importSyntax:
				VisitImport(importSyntax);
				break;
			case FunctionSyntax functionSyntax:
				VisitFunction(functionSyntax);
				break;
			case TypeAliasSyntax aliasSyntax:
				VisitTypeAlias(aliasSyntax);
				break;
			case TypedefSyntax typedefSyntax:
				VisitTypedef(typedefSyntax);
				break;
			case IfElseSyntax ifElseSyntax:
				VisitIfElse(ifElseSyntax);
				break;
			case IfSyntax ifSyntax:
				VisitIf(ifSyntax);
				break;
			case ElseSyntax elseSyntax:
				VisitElse(elseSyntax);
				break;
			case WhileSyntax whileSyntax:
				VisitWhile(whileSyntax);
				break;
			case DoWhileSyntax doWhileSyntax:
				VisitDoWhile(doWhileSyntax);
				break;
			case ForSyntax forSyntax:
				VisitFor(forSyntax);
				break;
			case ForeachSyntax foreachSyntax:
				VisitForeach(foreachSyntax);
				break;
			case SwitchSyntax switchSyntax:
				VisitSwitch(switchSyntax);
				break;
			case FallthroughSyntax fallthroughSyntax:
				VisitFallthrough(fallthroughSyntax);
				break;
			case ContinueSyntax continueSyntax:
				VisitContinue(continueSyntax);
				break;
			case BreakSyntax breakSyntax:
				VisitBreak(breakSyntax);
				break;
			case ReturnSyntax returnSyntax:
				VisitReturn(returnSyntax);
				break;
			case DeferSyntax deferSyntax:
				VisitDefer(deferSyntax);
				break;
			case GotoSyntax gotoSyntax:
				VisitGoto(gotoSyntax);
				break;
			case LabelSyntax labelSyntax:
				VisitLabel(labelSyntax);
				break;
			case ImplSyntax implSyntax:
				VisitImpl(implSyntax);
				break;
			case StructSyntax structSyntax:
				VisitStruct(structSyntax);
				break;
			case InterfaceSyntax interfaceSyntax:
				VisitInterface(interfaceSyntax);
				break;
			case UnionSyntax unionSyntax:
				VisitUnion(unionSyntax);
				break;
			case EnumSyntax enumSyntax:
				VisitEnum(enumSyntax);
				break;
			case UnitTestSyntax unitTestSyntax:
				VisitUnitTest(unitTestSyntax);
				break;
			case CompileConditionalSyntax compileConditionalSyntax:
				VisitCompileCondition(compileConditionalSyntax);
				break;
			case AssertSyntax assertSyntax:
				VisitAssert(assertSyntax);
				break;
			case SimpleExpressionSyntax simpleExpressionSyntax:
				VisitSimpleExpression(simpleExpressionSyntax);
				break;
			}
		}

		protected virtual void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			syntax.Statements?.ForEach(VisitStatement);
		}

		protected virtual void VisitNamespace(NamespaceSyntax syntax)
		{
			VisitName(syntax.Name);
			VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitImport(ImportSyntax syntax)
		{
			if (syntax.Attribs != null)
				VisitAttributes(syntax.Attribs);
			VisitName(syntax.ImportedModule);
			if (syntax.AsToken != null)
			{
				VisitIdentifierName(syntax.ModuleAlias);
			}
			if (syntax.ColonToken != null)
			{
				foreach (ImportSymbolSyntax symbol in syntax.Symbols)
				{
					VisitName(symbol.Symbol);
					if (symbol.AsToken != null)
					{
						VisitIdentifierName(symbol.Alias);
					}
				}
			}
		}

		protected virtual void VisitReceiver(ReceiverSyntax syntax)
		{
		}

		protected virtual void VisitFunction(FunctionSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			if (syntax.Receiver != null)
				VisitReceiver(syntax.Receiver);
			VisitIdentifierName(syntax.Name);
			if (syntax.OpenTemplateToken != null)
			{
				syntax.TemplateParameters?.ForEach(VisitTemplateParameter);
			}
			syntax.Parameters?.ForEach(VisitParameter);
			if (syntax.ArrowToken != null)
			{
				VisitType(syntax.ReturnType);
			}
			if (syntax.Constraint != null)
				VisitTemplateConstraint(syntax.Constraint);
			if (syntax.Body != null)
				VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitTypeAlias(TypeAliasSyntax syntax)
		{
			if (syntax.TemplateParameters != null)
			{
				foreach (TemplateParameterSyntax param in syntax.TemplateParameters)
				{
					VisitTemplateParameter(param);
				}
			}
			VisitSimpleExpression(syntax.Type);

			if (syntax.Name != null)
				VisitIdentifierName(syntax.Name);
		}

		protected virtual void VisitTypedef(TypedefSyntax syntax)
		{
			if (syntax.TemplateParameters != null)
			{
				foreach (TemplateParameterSyntax param in syntax.TemplateParameters)
				{
					VisitTemplateParameter(param);
				}
			}
			VisitType(syntax.Type);

			if (syntax.Name != null)
				VisitIdentifierName(syntax.Name);
		}

		protected virtual void VisitIfElse(IfElseSyntax syntax)
		{
			VisitIf(syntax.If);
			if (syntax.Else is IfSyntax ifSub)
			{
				VisitIf(ifSub);
			}
			else if (syntax.Else is ElseSyntax elseSyntax)
			{
				VisitElse(elseSyntax);
			}
		}

		protected virtual void VisitIf(IfSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitSimpleExpression(syntax.Condition);
			VisitStatement(syntax.Then);
		}

		protected virtual void VisitElse(ElseSyntax syntax)
		{
			VisitStatement(syntax.Statement);
		}

		protected virtual void VisitWhile(WhileSyntax syntax)
		{
			VisitSimpleExpression(syntax.Condition);
			VisitStatement(syntax.Body);
		}

		protected virtual void VisitDoWhile(DoWhileSyntax syntax)
		{
			VisitStatement(syntax.Body);
			VisitSimpleExpression(syntax.Condition);
		}

		protected virtual void VisitFor(ForSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitSimpleExpression(syntax.Initializer);
			VisitSimpleExpression(syntax.Condition);
			VisitSimpleExpression(syntax.Increment);
			VisitStatement(syntax.Body);
		}

		protected virtual void VisitForeach(ForeachSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitSimpleExpression(syntax.Values);
			VisitSimpleExpression(syntax.Source);
			VisitStatement(syntax.Body);
		}

		protected virtual void VisitSwitch(SwitchSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			syntax.Cases.ForEach(VisitSwitchCase);
		}

		protected virtual void VisitSwitchCase(SwitchCaseSyntax syntax)
		{
			if (syntax.Expression != null)
				VisitSimpleExpression(syntax.Expression);
			syntax.Statements?.ForEach(VisitStatement);
		}

		protected virtual void VisitFallthrough(FallthroughSyntax syntax)
		{
		}

		protected virtual void VisitContinue(ContinueSyntax syntax)
		{
		}

		protected virtual void VisitBreak(BreakSyntax syntax)
		{
		}

		protected virtual void VisitReturn(ReturnSyntax syntax)
		{
			if (syntax.Expression != null)
				VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitDefer(DeferSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitGoto(GotoSyntax syntax)
		{
			if (syntax.Identifier != null)
				VisitIdentifierName(syntax.Identifier);
		}

		protected virtual void VisitLabel(LabelSyntax syntax)
		{
			VisitIdentifierName(syntax.Name);
		}

		protected virtual void VisitImplInterfaceFieldDef(ImplInterfaceFieldDefSyntax syntax)
		{
			VisitIdentifierName(syntax.InterfaceField);
			VisitName(syntax.AggregateField);
		}

		protected virtual void VisitImplInterface(ImplInterfaceSyntax syntax)
		{
			VisitName(syntax.Name);
			if (syntax.OpenParen != null)
			{
				syntax.FieldDefs.ForEach(VisitImplInterfaceFieldDef);
			}
		}

		protected virtual void VisitImpl(ImplSyntax syntax)
		{
			if (syntax.OpenTemplateToken != null)
			{
				syntax.TemplateParameters.ForEach(VisitTemplateParameter);
			}
			VisitType(syntax.Type);
			if (syntax.ColonToken != null)
			{
				syntax.BaseList.ForEach(VisitImplInterface);
			}
			if (syntax.Constraint != null)
				VisitTemplateConstraint(syntax.Constraint);
			VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitAggregateBody(AggregateBodySyntax syntax)
		{
			if (syntax.Declarations != null)
			{
				foreach (DeclarationSyntax decl in syntax.Declarations)
				{
					VisitDeclaration(decl);
				}
			}
		}

		protected virtual void VisitStruct(StructSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			if (syntax.Name != null)
				VisitIdentifierName(syntax.Name);
			if (syntax.OpenTemplateToken != null)
			{
				syntax.TemplateParameters.ForEach(VisitTemplateParameter);
			}
			if (syntax.ColonToken != null)
			{
				VisitName(syntax.BaseList);
			}
			VisitAggregateBody(syntax.Body);
		}

		protected virtual void VisitInterface(InterfaceSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitIdentifierName(syntax.Name);
			if (syntax.OpenTemplateToken != null)
			{
				syntax.TemplateParameters.ForEach(VisitTemplateParameter);
			}
			if (syntax.ColonToken != null)
			{
				VisitName(syntax.BaseList);
			}
			VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitUnion(UnionSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			if (syntax.Name != null)
				VisitIdentifierName(syntax.Name);
			if (syntax.OpenTemplateToken != null)
			{
				syntax.TemplateParameters.ForEach(VisitTemplateParameter);
			}
			VisitAggregateBody(syntax.Body);
		}

		protected virtual void VisitEnum(EnumSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitIdentifierName(syntax.Name);
			if (syntax.TemplateParameters != null)
			{
				foreach (TemplateParameterSyntax param in syntax.TemplateParameters)
				{
					VisitTemplateParameter(param);
				}
			}
			if (syntax.Type != null)
			{
				VisitType(syntax.Type);
			}
			if (syntax.Members != null)
			{
				foreach (EnumMemberSyntax member in syntax.Members)
				{
					VisitIdentifierName(member.Name);
					if (member.TupleType != null)
					{
						VisitType(member.TupleType);
					}
					if (member.AggrBody != null)
					{
						foreach (DeclarationSyntax decl in member.AggrBody.Declarations)
						{
							VisitDeclaration(decl);
						}
					}
					if (member.EqualsToken != null)
					{
						VisitSimpleExpression(member.DefaultValue);
					}
				}
			}
		}

		protected virtual void VisitUnitTest(UnitTestSyntax syntax)
		{
			if (syntax.Name != null)
				VisitIdentifierName(syntax.Name);
			VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			if (syntax.Context.AllowVisitAll)
			{
				VisitSimpleExpression(syntax.Expression);
				VisitBlockStatement(syntax.Body);
				if (syntax.ElseToken != null)
				{
					VisitStatement(syntax.ElseBody);
				}
			}
			else if (syntax.Context.AllowCompilation)
			{
				VisitBlockStatement(syntax.Body);
			}
			else if (syntax.ElseToken != null)
			{
				VisitStatement(syntax.ElseBody);
			}
		}

		protected virtual void VisitAssert(AssertSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			syntax.Arguments?.ForEach(VisitArgument);
		}
		
		protected virtual void VisitSimpleExpression(SimpleExpressionSyntax syntax)
		{
			switch (syntax)
			{
			case NameSyntax nameSyntax:
				VisitName(nameSyntax);
				break;
			case TypeSyntax typeSyntax:
				VisitType(typeSyntax);
				break;
			case ExpressionSyntax expressionSyntax:
				VisitExpression(expressionSyntax);
				break;
			case CommaExpressionSyntax commaExpression:
				VisitCommaExpression(commaExpression);
				break;
			case DeclarationSyntax declarationSyntax:
				VisitDeclaration(declarationSyntax);
				break;
			case TernaryExpressionSyntax ternaryExpressionSyntax:
				VisitTernaryExpression(ternaryExpressionSyntax);
				break;
			case BinaryExpressionSyntax binaryExpressionSyntax:
				VisitBinaryExpression(binaryExpressionSyntax);
				break;
			case AssignExpressionSyntax assignExpressionSyntax:
				VisitAssignExpression(assignExpressionSyntax);
				break;
			case PrefixExpressionSyntax prefixExpressionSyntax:
				VisitPrefixExpression(prefixExpressionSyntax);
				break;
			case PostfixExpressionSyntax postfixExpressionSyntax:
				VisitPostfixExpression(postfixExpressionSyntax);
				break;
			case BracketedExpressionSyntax bracketedExpressionSyntax:
				VisitBracketedExpression(bracketedExpressionSyntax);
				break;
			case CastExpressionSyntax castExpressionSyntax:
				VisitCastExpression(castExpressionSyntax);
				break;
			case TransmuteExpressionSyntax transmuteExpressionSyntax:
				VisitTransmuteExpression(transmuteExpressionSyntax);
				break;
			case CCastExpressionSyntax castExpressionSyntax:
				VisitCCastExpression(castExpressionSyntax);
				break;
			case IndexSliceExpressionSyntax indexSliceExpressionSyntax:
				VisitIndexSliceExpression(indexSliceExpressionSyntax);
				break;
			case FunctionCallExpressionSyntax functionCallExpressionSyntax:
				VisitFunctionCall(functionCallExpressionSyntax);
				break;
			case MixinExpressionSyntax mixinExpressionSyntax:
				VisitMixinExpression(mixinExpressionSyntax);
				break;
			case IntrinsicSyntax intrinsicSyntax:
				VisitIntrinsicExpression(intrinsicSyntax);
				break;
			case IsExpressionSyntax isExpressionSyntax:
				VisitIsExpression(isExpressionSyntax);
				break;
			case NewExpressionSyntax newExpressionSyntax:
				VisitNewExpression(newExpressionSyntax);
				break;
			case DeleteExpressionSyntax deleteExpressionSyntax:
				VisitDeleteExpression(deleteExpressionSyntax);
				break;
			case ClosureSyntax lambdaSyntax:
				VisitLambda(lambdaSyntax);
				break;
			case LiteralExpressionSyntax literalExpressionSyntax:
				VisitLiteral(literalExpressionSyntax);
				break;
			case ArrayLiteralExpressionSyntax arrayLiteralExpressionSyntax:
				VisitArrayLiteral(arrayLiteralExpressionSyntax);
				break;
			case AssocArrayLiteralExpressionSyntax assocArrayLiteralExpressionSyntax:
				VisitAssocArrayLiteral(assocArrayLiteralExpressionSyntax);
				break;
			case AggregateInitializerSyntax structInitializer:
				VisitAggregateInitializer(structInitializer);
				break;
			case SpecialKeywordExpressionSyntax specialKeywordExpressionSyntax:
				VisitSpecialKeyword(specialKeywordExpressionSyntax);
				break;
			case TypeExpressionSyntax typeExprSyntax:
				VisitTypeExpression(typeExprSyntax);
				break;
			}
		}

		protected virtual void VisitExpression(ExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			foreach (CommaExpressionElemSyntax elem in syntax.Expressions)
			{
				VisitSimpleExpression(elem.Expression);
			}
		}

		protected virtual void VisitDeclaration(DeclarationSyntax syntax)
		{
			VisitName(syntax.Names);
			if (syntax.TypeSpecificationToken != null)
			{
				VisitType(syntax.Type);
			}
			if (syntax.OpenBraceToken != null)
			{
				foreach (PropertyGetSetSyntax getset in syntax.GetSet)
				{
					if (getset.Attributes != null)
						VisitAttributes(getset.Attributes);
					if (getset.Body != null)
						VisitBlockStatement(getset.Body);
				}
			}
			if (syntax.AssignmentToken != null)
			{
				VisitSimpleExpression(syntax.Initializer);
			}
		}

		protected virtual void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Condition);
			VisitSimpleExpression(syntax.Then);
			VisitSimpleExpression(syntax.Else);
		}

		protected virtual void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Left);
			VisitSimpleExpression(syntax.Right);
		}

		protected virtual void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Left);
			VisitSimpleExpression(syntax.Right);
		}

		protected virtual void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Right);
		}

		protected virtual void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Left);
		}

		protected virtual void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitCastExpression(CastExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			VisitType(syntax.Type);
		}

		protected virtual void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			VisitType(syntax.Type);
		}

		protected virtual void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			VisitType(syntax.Type);
		}

		protected virtual void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			VisitSimpleExpression(syntax.IndexSliceExpression);
		}

		protected virtual void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			VisitQualifiedName(syntax.QualifiedName);
			syntax.Arguments?.ForEach(VisitArgument);
		}

		protected virtual void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitIsExpression(IsExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			VisitType(syntax.Type);
		}

		protected virtual void VisitNewExpression(NewExpressionSyntax syntax)
		{
			if (syntax.OpenNewParen != null)
			{
				syntax.NewArguments.ForEach(VisitArgument);
			}
			VisitType(syntax.Type);
			if (syntax.OpenTypeParen != null)
			{
				syntax.TypeArguments.ForEach(VisitArgument);
			}
			if (syntax.OpenArrayBracket != null)
			{
				VisitSimpleExpression(syntax.ArrayExpression);
			}
		}

		protected virtual void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitLambda(ClosureSyntax syntax)
		{
			VisitSimpleExpression(syntax.Identifiers);
			if (syntax.OpenBracketToken != null)
			{
				foreach (ClosureCaptureSyntax capture in syntax.Captures)
				{
					if (capture.Name != null)
						VisitName(capture.Name);
				}
			}
			VisitStatement(syntax.Body);
		}

		protected virtual void VisitLiteral(LiteralExpressionSyntax syntax) 
		{
		}

		protected virtual void VisitArrayLiteral(ArrayLiteralExpressionSyntax syntax)
		{
			foreach (ArrayLiteralElemSyntax element in syntax.Elements)
			{
				VisitSimpleExpression(element.Expression);
			}
		}

		protected virtual void VisitAssocArrayLiteral(AssocArrayLiteralExpressionSyntax syntax)
		{
			foreach (AssocArrayLiteralElemSyntax element in syntax.Elements)
			{
				VisitSimpleExpression(element.Left);
				VisitSimpleExpression(element.Right);
			}
		}

		protected virtual void VisitAggregateInitializer(AggregateInitializerSyntax syntax)
		{
			VisitName(syntax.TypeIden);
			if (syntax.Elements != null)
			{
				foreach (AggregateInitializerElemSyntax element in syntax.Elements)
				{
					if (element.Identifier != null)
					{
						VisitIdentifierName(element.Identifier);
					}

					VisitSimpleExpression(element.Expression);
				}
			}
		}

		protected virtual void VisitSpecialKeyword(SpecialKeywordExpressionSyntax syntax)
		{
		}

		protected virtual void VisitBitField(BitFieldTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
			VisitSimpleExpression(syntax.ExprSyntax);
		}

		protected virtual void VisitTypeExpression(TypeExpressionSyntax syntax)
		{
			VisitType(syntax.Type);
		}
	}
}
