﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using MJC.General;
using MJC.ILBackend;
using MJC.ILBackend.Interpret;
using MJC.IRBackend;
using MJC.Module;
using MJC.SyntaxTree;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC
{
    class Program
	{
		static void Main(string[] args)
        {
			CmdLine.Parse(args);
	        if (!CmdLine.Compile)
		        return;

	        Stopwatch timer = new Stopwatch();
	        Stopwatch totalTimer = new Stopwatch();
	        totalTimer.Start();

			if (CmdLine.Stats)
		        timer.Start();

			List<string> VersionConditionalValues = new List<string>();
	        List<string> DebugConditionalValues = new List<string>();

			// Initialize default Version conditional values
			VersionConditionalValues.Add("MJC");
			VersionConditionalValues.Add("All");

	        // TODO: Auto generate version conditionals
			//VersionConditionalValues.Add("Windows");
			//VersionConditionalValues.Add("Win32");
			//VersionConditionalValues.Add("Win64");
			VersionConditionalValues.Add("Interp");
			VersionConditionalValues.Add("X64");

			VersionConditionalValues.AddRange(CmdLine.AdditionalVersionValues);
	        DebugConditionalValues.AddRange(CmdLine.AdditionalDebugValues);


	        List<SyntaxTree.SyntaxTree> trees = new List<SyntaxTree.SyntaxTree>();
		    SyntaxLexer lexer = new SyntaxLexer();
		    SyntaxParser parser = new SyntaxParser();

	        CompilerContext compilerContext = new CompilerContext();
	        SemanticAnalyzer analyzer = new SemanticAnalyzer(compilerContext);
	        analyzer.VersionConditionalVariables = VersionConditionalValues;
	        analyzer.DebugConditionalVariables = DebugConditionalValues;

	        ILGenerator ilGenerator = new ILGenerator();
			ILBackend.ILBackend ilBackend = new ILBackend.ILBackend(compilerContext);
	        //IRBackend.IRBackend irBackend = new IRBackend.IRBackend(compilerContext);
			ILInterpreter interpreter = new ILInterpreter(ILInterpretType.Exec);

			if (CmdLine.Stats)
			{
				timer.Stop();
				Console.WriteLine($"Compiler setup took {timer.Elapsed.TotalMilliseconds}ms");
			}

			Dictionary<string, Module.Module> loadedModules = new Dictionary<string, Module.Module>();

			//GC.TryStartNoGCRegion(32 * 1024 * 1024);

			if (CmdLine.GenerateModule)
			{
				foreach (string file in CmdLine.Files)
				{
					if (!File.Exists(file))
					{
						ErrorSystem.Error($"Cannot find a file named '{file}'");
						continue;
					}

					string              source        = File.ReadAllText(file);
					ErrorSystemMetadata errorMetadata = new ErrorSystemMetadata(file, source);
					ErrorSystem.Metadata = errorMetadata;

					if (CmdLine.Stats)
						timer.Restart();

					List<SyntaxToken> tokens = lexer.Parse(source);

					if (CmdLine.Stats)
					{
						timer.Stop();
						int lines = errorMetadata.Lines.Count;
						int chars = errorMetadata.Source.Length;
						Console.WriteLine($"Lexer took {timer.Elapsed.TotalMilliseconds}ms ({lines} lines, {chars} characters)");
					}

					if (CmdLine.OutLex)
					{
						foreach (SyntaxToken token in tokens)
						{
							if (token.LeadTrivia.Count > 0)
							{
								foreach (SyntaxTrivia trivia in token.LeadTrivia)
								{
									Console.WriteLine($"\tLead : {trivia.ToDebugString()}");
								}
							}

							Console.WriteLine(token.ToDebugString());
							if (token.TrailTrivia.Count > 0)
							{
								foreach (SyntaxTrivia trivia in token.TrailTrivia)
								{
									Console.WriteLine($"\tTrail: {trivia.ToDebugString()}");
								}
							}
						}

						Console.WriteLine();
					}

					if (CmdLine.Stats)
						timer.Restart();

					SyntaxTree.SyntaxTree ast = parser.Parse(tokens);
					ast.ErrorMeta = errorMetadata;
					ast.FileName  = Path.GetFileNameWithoutExtension(file);

					if (CmdLine.Stats)
					{
						timer.Stop();
						Console.WriteLine($"Parser took {timer.Elapsed.TotalMilliseconds}ms");
					}

					if (ErrorSystem.NumErrors > 0)
					{
						Console.ForegroundColor = ConsoleColor.DarkRed;
						Console.WriteLine($"Encountered {ErrorSystem.NumErrors} error{(ErrorSystem.NumErrors > 1 ? "s" : "")} while parsing!\n");
						Console.ForegroundColor = ConsoleColor.Gray;
					}

					if (CmdLine.OutAstObf)
					{
						Console.WriteLine();
						Console.WriteLine(ast.ToString());
						Console.WriteLine();
					}

					if (CmdLine.OutAstFull)
					{
						Console.WriteLine();
						Console.WriteLine(ast.ToFullString());
						Console.WriteLine();
					}

					if (CmdLine.Stats)
						timer.Restart();

					analyzer.AnalyzePartial(ast);

					if (CmdLine.Stats)
					{
						timer.Stop();
						Console.WriteLine($"Partial semantic analysis took {timer.Elapsed.TotalMilliseconds}ms");
					}

					// If the module of the file is not core.builtin, add core.builtin as a top level import
					if (ast.PackageName != "core" || ast.ModuleName != "builtin")
					{
						ast.FileImports.AddImport(ast.CompilationUnit.Block.Context.Scope, -1, "core.builtin");
						ast.FileImports.AddUsedModule(ast.FileImports.Directives.Last());
					}

					//ast.Symbols?.PrintTable();
					//ast.FileImports?.PrintTable();

					trees.Add(ast);
				}

				if (CmdLine.Stats)
					timer.Restart();

				compilerContext.Symbols.ReplaceAliases();

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"Replacing aliases took {timer.Elapsed.TotalMilliseconds}ms");
					timer.Restart();
				}

				compilerContext.Symbols.UpdateTypes();

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"Updating types took {timer.Elapsed.TotalMilliseconds}ms");
					timer.Restart();
				}

				// Handle imports
				foreach (SyntaxTree.SyntaxTree tree in trees)
				{
					compilerContext.Imports.AddFileImports(tree.FileImports);
				}

				compilerContext.LoadImportModules();

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"Load import modules took {timer.Elapsed.TotalMilliseconds}ms");
					timer.Restart();
				}

				foreach (SyntaxTree.SyntaxTree tree in trees)
				{
					analyzer.AnalyzeFull(tree, compilerContext);
				}

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"Full semantic analysis took {timer.Elapsed.TotalMilliseconds}ms");
					timer.Restart();
				}

				compilerContext.Symbols.GenerateMangledNames();

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"Generating mangled names took {timer.Elapsed.TotalMilliseconds}ms");
					timer.Restart();
				}

				foreach (SyntaxTree.SyntaxTree tree in trees)
				{
					analyzer.Finalize(tree, compilerContext);
				}

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"Finalizing semantic analysis took {timer.Elapsed.TotalMilliseconds}ms");
				}

				//Console.WriteLine("Final symbol table");
				//globCompilerContext.Symbols.PrintTable();

				if (ErrorSystem.NumErrors > 0)
				{
					Console.WriteLine();
					Console.ForegroundColor = ConsoleColor.DarkRed;
					Console.Write("Compilation process terminated: ");
					Console.ForegroundColor = ConsoleColor.Gray;
					Console.WriteLine("An error has occured");
					Console.Read();
					return;
				}

				if (CmdLine.Stats)
					timer.Restart();

				compilerContext.PrepareILStage(compilerContext);

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"Preparing for IL generation took {timer.Elapsed.TotalMilliseconds}ms");
					timer.Restart();
				}

				// Generate IL
				ilGenerator.CompilerContext = compilerContext;
				List<string>               ilFiles     = new List<string>();
				List<ILBackend.ILCompUnit> ilCompUnits = new List<ILBackend.ILCompUnit>();
				foreach (SyntaxTree.SyntaxTree tree in trees)
				{
					ilGenerator.OutputFile = Path.ChangeExtension(tree.ErrorMeta.SourceFile, "mjil");
					ilGenerator.Visit(tree);
					ilFiles.Add(ilGenerator.OutputFile);
					ilCompUnits.Add(ilGenerator.CompUnit);
				}

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"Generating IL took {timer.Elapsed.TotalMilliseconds}ms");
					timer.Restart();
				}

				// IL processing

				(List<string> irFiles, List<IRModule> irModules) = ilBackend.Execute(ilFiles, ilCompUnits);

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"IL Backend took {timer.Elapsed.TotalMilliseconds}ms");
					timer.Restart();
				}

				//Console.Read();
				//GC.EndNoGCRegion();
				//return;
				// IR processing
				/*List<string> objectFiles = irBackend.Execute(irFiles, irModules);
	
			    if (CmdLine.Stats)
			    {
				    timer.Stop();
				    Console.WriteLine($"IR Backend took {timer.Elapsed.TotalMilliseconds}ms");
			    }*/


				// Link
				/* if (CmdLine.Link)
				{
					if (CmdLine.Stats)
						timer.Restart();
					
					string linkArgs = "/subsystem:console /entry:_start";
	
					// Will only work on windows with kit 10.0.17763.0 installed
					string windowsLibPath = "\"C:\\Program Files (x86)\\Windows Kits\\10\\Lib\\10.0.17763.0\\um\\x64\"";
	
					// Will not work on any other pc
					string msvcrtPath = "\"D:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Tools\\MSVC\\14.16.27023\\lib\\x64\"";
	
	
					linkArgs += " /libpath:" + windowsLibPath;
					linkArgs += " /libpath:" + msvcrtPath;
					foreach (string file in objectFiles)
					{
						linkArgs += ' ';
						if (!string.IsNullOrEmpty(CmdLine.IntermediateDirectory))
							linkArgs += CmdLine.IntermediateDirectory;
						linkArgs += file;
					}
	
					List<string> curExtLibs = new List<string>();
					foreach (SyntaxTree.SyntaxTree tree in trees)
					{
						foreach (string foreignLib in tree.ForeignLibs)
						{
							if (!curExtLibs.Contains(foreignLib))
								curExtLibs.Add(foreignLib);
						}
					}
	
					foreach (string lib in curExtLibs)
					{
						linkArgs += ' ' + lib;
					}
	
					linkArgs += " Kernel32.lib";
					linkArgs += " msvcrt.lib";
	
					linkArgs += ' ' + compilerContext.PrepareForLinking();
	
					if (CmdLine.Stats)
					{
						timer.Stop();
						Console.WriteLine($"Linker setup took {timer.Elapsed.TotalMilliseconds}ms");
						timer.Restart();
					}
	
					Process linkProcess = new Process();
					linkProcess.StartInfo.FileName = "lld-link.exe";
					linkProcess.StartInfo.Arguments = linkArgs;
					linkProcess.Start();
					linkProcess.WaitForExit();
	
					//globCompilerContext.CleanupAfterLinking();
	
					if (CmdLine.Stats)
					{
						timer.Stop();
						Console.WriteLine($"Linker took {timer.Elapsed.TotalMilliseconds}ms");
					}
				}
				else */
				//if (CmdLine.GenerateModule)
				{
					if (CmdLine.Stats)
						timer.Restart();

					Dictionary<string, List<SyntaxTree.SyntaxTree>> moduleTrees = new Dictionary<string, List<SyntaxTree.SyntaxTree>>();

					foreach (SyntaxTree.SyntaxTree tree in trees)
					{
						string iden = "";
						if (!string.IsNullOrEmpty(tree.PackageName))
							iden += tree.PackageName + '.';
						iden += tree.ModuleName;

						if (!moduleTrees.ContainsKey(iden))
							moduleTrees.Add(iden, new List<SyntaxTree.SyntaxTree>());

						moduleTrees[iden].Add(tree);
					}

					foreach (KeyValuePair<string, List<SyntaxTree.SyntaxTree>> pair in moduleTrees)
					{
						SyntaxTree.SyntaxTree tmpTree = pair.Value[0];

						Module.Module module = ModuleGenerator.Generate(pair.Value, null, tmpTree.PackageName, tmpTree.ModuleName, CmdLine.DynamicModule, CmdLine.LTO);

						string outputLoc = CmdLine.OutDirectory + '/';
						if (!string.IsNullOrEmpty(CmdLine.OutputFile))
							outputLoc += CmdLine.OutputFile;
						else
							outputLoc += pair.Key;

						if (!outputLoc.EndsWith(".mjm"))
							outputLoc += ".mjm";

						ModuleGenerator.ModuleOut(module, outputLoc);
						loadedModules.Add(pair.Key, module);
					}

					if (CmdLine.Stats)
					{
						timer.Stop();
						Console.WriteLine($"module generation took {timer.Elapsed.TotalMilliseconds}ms");
					}
				}
			}

			//Debugger.Break();
			if (CmdLine.ModuleToInterpret != null) // Interpret program
			{
				Console.WriteLine($"Interpreting {CmdLine.ModuleToInterpret}");

				if (CmdLine.Stats)
					timer.Restart();

				Dictionary<string, Module.Module> modules = ModuleImporter.ImportModulesAndRequiredImports(CmdLine.ModuleToInterpret, loadedModules);

				foreach (KeyValuePair<string, Module.Module> pair in modules)
				{
					if (pair.Value.ILCompUnit == null)
						pair.Value.GenerateILCompUnit(compilerContext);
				}

				ILCompUnit mainCompUnit = modules[CmdLine.ModuleToInterpret].ILCompUnit;
				mainCompUnit.Builtins = ilBackend.Builtins;

				foreach (KeyValuePair<string, Module.Module> pair in modules)
				{
					if (pair.Key != CmdLine.ModuleToInterpret)
					{
						mainCompUnit.Imports.Add(pair.Value.ILCompUnit);
					}
				}

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"Setting up modules for interpreting took {timer.Elapsed.TotalMilliseconds}ms");
					timer.Restart();
				}

				interpreter.Context = new ILInterpContext(mainCompUnit, compilerContext, CmdLine.IsX64);
				interpreter.Interpret(mainCompUnit);

				if (CmdLine.Stats)
				{
					timer.Stop();
					Console.WriteLine($"interpreting took {timer.Elapsed.TotalMilliseconds}ms");
				}
			}

			totalTimer.Stop();
	        double totalTime = totalTimer.Elapsed.TotalMilliseconds;
			Console.WriteLine($"\nCompilation took {totalTime}ms (including console and debug output)\n");

			//Console.Read();

        }

    }
}