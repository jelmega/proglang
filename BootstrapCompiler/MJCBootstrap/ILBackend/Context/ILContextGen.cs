﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Context
{
	public static class ILContextGen
	{
		public static void ProcessModule(ILCompUnit compUnit, CompilerContext compilerContext)
		{
			compUnit.Context = new ILCompUnitContext();
			compUnit.Context.CompUnit = compUnit;
			compUnit.Context.CompilerContext = compilerContext;

			foreach (KeyValuePair<string, ILFunction> pair in compUnit.Functions)
			{
				ProcessFunction(pair.Value, compUnit);
			}
		}

		public static void ProcessFunction(ILFunction function, ILCompUnit compUnit)
		{
			// Skip template definitions
			if (function.IsUninstantiated)
				return;

			function.Context = compUnit.Context.CreateContext();

			if (function.Operands != null)
			{
				foreach (ILOperand op in function.Operands)
				{
					ILVarContext context = new ILVarContext(op, ILVarType.Param);
					function.Context.AddVarContext(context);
				}
			}

			foreach (ILBasicBlock block in function.Blocks)
			{
				ProcessBasicBlock(block, function.Context);
			}
		}

		public static void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			foreach (ILInstruction instruction in basicBlock.Instructions)
			{
				switch (instruction.InstructionType)
				{
				case ILInstructionType.Unknown:
					break;
				case ILInstructionType.IntLiteral:
					GenIntLiteral(instruction as ILIntLiteralInstruction, funcContext);
					break;
				case ILInstructionType.FloatLiteral:
					GenFloatLiteral(instruction as ILFloatLiteralInstruction, funcContext);
					break;
				case ILInstructionType.StringLiteral:
					GenStringLiteral(instruction as ILStringLiteralInstruction, funcContext);
					break;
				case ILInstructionType.AllocStack:
					GenAllocStack(instruction as ILAllocStackInstruction, funcContext);
					break;
				case ILInstructionType.Assign:
					GenAssign(instruction as ILAssignInstruction, funcContext);
					break;
				case ILInstructionType.Store:
					GenStore(instruction as ILStoreInstruction, funcContext);
					break;
				case ILInstructionType.Load:
					GenLoad(instruction as ILLoadInstruction, funcContext);
					break;
				case ILInstructionType.OperatorRef:
					GenOperatorRef(instruction as ILOperatorRefInstruction, funcContext);
					break;
				case ILInstructionType.FunctionRef:
					GenFunctionRef(instruction as ILFunctionRefInstruction, funcContext);
					break;
				case ILInstructionType.MethodRef:
					GenMethodRef(instruction as ILMethodRefInstruction, funcContext);
					break;
				case ILInstructionType.Call:
					GenCall(instruction as ILCallInstruction, funcContext);
					break;
				case ILInstructionType.Builtin:
					GenBuiltin(instruction as ILBuiltinInstruction, funcContext);
					break;
				case ILInstructionType.StructExtract:
					GenStructExtract(instruction as ILStructExtractInstruction, funcContext);
					break;
				case ILInstructionType.StructInsert:
					GenStructInsert(instruction as ILStructInsertInstruction, funcContext);
					break;
				case ILInstructionType.StructElemAddr:
					GenStructElemAddr(instruction as ILStructElemAddrInstruction, funcContext);
					break;
				case ILInstructionType.UnionExtract:
					GenUnionExtract(instruction as ILUnionExtractInstruction, funcContext);
					break;
				case ILInstructionType.UnionInsert:
					GenUnionInsert(instruction as ILUnionInsertInstruction, funcContext);
					break;
				case ILInstructionType.UnionElemAddr:
					GenUnionElemAddr(instruction as ILUnionElemAddrInstruction, funcContext);
					break;
				case ILInstructionType.EnumValue:
					GenEnumValue(instruction as ILEnumValueInstruction, funcContext);
					break;
				case ILInstructionType.EnumGetValue:
					GenEnumGetValue(instruction as ILEnumGetValueInstruction, funcContext);
					break;
				case ILInstructionType.EnumGetMemberValue:
					GenEnumGetMemberValue(instruction as ILEnumGetMemberValueInstruction, funcContext);
					break;
				case ILInstructionType.GlobalAddr:
					GenGlobalAddr(instruction as ILGlobalAddrInstruction, funcContext);
					break;
				case ILInstructionType.GlobalValue:
					GenGlobalValue(instruction as ILGlobalValueInstruction, funcContext);
					break;
				case ILInstructionType.TemplateGetValue:
					GenTemplateGetValue(instruction as ILTemplateGetValueInstruction, funcContext);
					break;
				case ILInstructionType.ConvType:
					GenConvType(instruction as ILConvertTypeInstruction, funcContext);
					break;
				case ILInstructionType.GetVTable:
					GenGetVTable(instruction as ILGetVTableInstruction, funcContext);
					break;
				case ILInstructionType.Return:
					GenReturn(instruction as ILReturnInstruction, funcContext);
					break;
				case ILInstructionType.CondBranch:
					GenCondBranch(instruction as ILCondBranchInstruction, funcContext);
					break;
				case ILInstructionType.SwitchEnum:
					GenSwitchEnum(instruction as ILSwitchEnumInstruction, funcContext);
					break;
				case ILInstructionType.SwitchValue:
					GenSwitchValue(instruction as ILSwitchValueInstruction, funcContext);
					break;
				}
			}
		}

		public static void GenIntLiteral(ILIntLiteralInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenFloatLiteral(ILFloatLiteralInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenStringLiteral(ILStringLiteralInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenAllocStack(ILAllocStackInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Stack);
			context.AddVarContext(varContext);
		}

		public static void GenAssign(ILAssignInstruction instruction, ILContext context)
		{
			ILVarContext src = context.GetVarContext(instruction.Src.Iden);
			ILVarContext dst = context.GetVarContext(instruction.Dst.Iden);

			dst.DependentOn.Add(src.Operand.Iden);
			++dst.StoredToCount;
			++src.UseCount;
		}

		public static void GenStore(ILStoreInstruction instruction, ILContext context)
		{
			ILVarContext src = context.GetVarContext(instruction.Src.Iden);
			ILVarContext dst = context.GetVarContext(instruction.Dst.Iden);

			dst.DependentOn.Add(src.Operand.Iden);
			++dst.StoredToCount;
			++src.UseCount;
		}

		public static void GenLoad(ILLoadInstruction instruction, ILContext context)
		{
			ILVarContext src = context.GetVarContext(instruction.Src.Iden);
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);

			varContext.DependentOn.Add(src.Operand.Iden);
			++src.UseCount;

			context.AddVarContext(varContext);
		}

		public static void GenOperatorRef(ILOperatorRefInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenFunctionRef(ILFunctionRefInstruction instruction, ILContext context)
		{
			if (instruction.RetOperand == null)
				return;

			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);

			context.AddVarContext(varContext);

			ILCompUnit compUnit = context.Parent.CompUnit;
			bool external = compUnit.IsFunctionExternal(instruction.MangledName);
			if (external && !context.Parent.ExternalFunctions.ContainsKey(instruction.MangledName))
			{
				ILFunction func = compUnit.GetFunction(instruction.MangledName);
				context.Parent.ExternalFunctions.Add(instruction.MangledName, func.GetDeclaration());
			}
		}

		public static void GenMethodRef(ILMethodRefInstruction instruction, ILContext context)
		{
			if (instruction.RetOperand == null)
				return;

			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);

			context.AddVarContext(varContext);
		}

		public static void GenCall(ILCallInstruction instruction, ILContext context)
		{
			ILVarContext varContext = null;
			if (instruction.RetOperand != null)
			{
				varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);

				context.AddVarContext(varContext);
			}

			ILVarContext func = context.GetVarContext(instruction.Func.Iden);
			++func.UseCount;

			if (varContext == null)
				return;

			varContext?.DependentOn.Add(varContext.Operand.Iden);

			if (instruction.Operands != null)
			{
				foreach (ILOperand operand in instruction.Operands)
				{
					ILVarContext param = context.GetVarContext(operand.Iden);
					++param.UseCount;
					varContext?.DependentOn.Add(operand.Iden);
				}
			}

		}

		public static void GenBuiltin(ILBuiltinInstruction instruction, ILContext context)
		{
			ILVarContext varContext = null;
			if (instruction.RetOperand != null)
			{
				varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
				context.AddVarContext(varContext);
			}

			if (instruction.Operands != null)
			{
				foreach (ILOperand operand in instruction.Operands)
				{
					ILVarContext param = context.GetVarContext(operand.Iden);
					++param.UseCount;
					varContext?.DependentOn.Add(operand.Iden);
				}
			}
		}

		public static void GenStructInsert(ILStructInsertInstruction instruction, ILContext context)
		{
			ILVarContext structContext = context.GetVarContext(instruction.Struct.Iden);
			ILVarContext srcContext = context.GetVarContext(instruction.Value.Iden);
			++srcContext.UseCount;
			structContext.DependentOn.Add(instruction.Value.Iden);
		}

		public static void GenStructExtract(ILStructExtractInstruction instruction, ILContext context)
		{
			ILVarContext structContext = context.GetVarContext(instruction.Struct.Iden);
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			varContext.DependentOn.Add(instruction.Struct.Iden);
			++structContext.UseCount;
			context.AddVarContext(varContext);
		}

		public static void GenStructElemAddr(ILStructElemAddrInstruction instruction, ILContext context)
		{
			ILVarContext structContext = context.GetVarContext(instruction.Struct.Iden);
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Stack);
			varContext.DependentOn.Add(instruction.Struct.Iden);
			++structContext.UseCount;
			context.AddVarContext(varContext);
		}

		public static void GenUnionInsert(ILUnionInsertInstruction instruction, ILContext context)
		{
			ILVarContext unionContext = context.GetVarContext(instruction.Union.Iden);
			ILVarContext srcContext = context.GetVarContext(instruction.Value.Iden);
			++srcContext.UseCount;
			unionContext.DependentOn.Add(instruction.Value.Iden);
		}

		public static void GenUnionExtract(ILUnionExtractInstruction instruction, ILContext context)
		{
			ILVarContext unionContext = context.GetVarContext(instruction.Union.Iden);
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			varContext.DependentOn.Add(instruction.Union.Iden);
			++unionContext.UseCount;
			context.AddVarContext(varContext);
		}

		public static void GenUnionElemAddr(ILUnionElemAddrInstruction instruction, ILContext context)
		{
			ILVarContext unionContext = context.GetVarContext(instruction.Union.Iden);
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			varContext.DependentOn.Add(instruction.Union.Iden);
			++unionContext.UseCount;
			context.AddVarContext(varContext);
		}

		private static void GenEnumValue(ILEnumValueInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);

			if (instruction.Operands != null)
			{
				foreach (ILOperand operand in instruction.Operands)
				{
					ILVarContext param = context.GetVarContext(operand.Iden);
					++param.UseCount;
					varContext.DependentOn.Add(operand.Iden);
				}
			}
		}

		public static void GenEnumGetValue(ILEnumGetValueInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);

			ILVarContext opContext = context.GetVarContext(instruction.Operand.Iden);
			++opContext.UseCount;
		}

		public static void GenEnumGetMemberValue(ILEnumGetMemberValueInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenGlobalAddr(ILGlobalAddrInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);

			ILCompUnitContext parent = context.Parent;
			if (parent.CompUnit.IsGlobalExternal(instruction.Identifier) &&
			    !parent.ExternalGlobals.ContainsKey(instruction.Identifier))
			{
				ILGlobal tmp = parent.CompUnit.GetGlobal(instruction.Identifier);
				ILAttributes attribs = new ILAttributes(tmp.Attributes, true);
				ILGlobal global = new ILGlobal(tmp.MangledIdentifier, tmp.Type, tmp.Initializer, attribs);
				global.Value = tmp.Value;
				parent.ExternalGlobals.Add(instruction.Identifier, global);
			}
		}

		public static void GenGlobalValue(ILGlobalValueInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);

			ILCompUnitContext parent = context.Parent;
			if (parent.CompUnit.IsGlobalExternal(instruction.Identifier) &&
			    !parent.ExternalGlobals.ContainsKey(instruction.Identifier))
			{
				ILGlobal tmp = parent.CompUnit.GetGlobal(instruction.Identifier);
				ILAttributes attribs = new ILAttributes(tmp.Attributes, true);
				ILGlobal global = new ILGlobal(tmp.MangledIdentifier, tmp.Type, tmp.Initializer, attribs);
				global.Value = tmp.Value;
				parent.ExternalGlobals.Add(instruction.Identifier, global);
			}
		}

		public static void GenTemplateGetValue(ILTemplateGetValueInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenConvType(ILConvertTypeInstruction instruction, ILContext context)
		{
			ILVarContext srcContext = context.GetVarContext(instruction.Operand.Iden);
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			++srcContext.UseCount;
			varContext.DependentOn.Add(instruction.Operand.Iden);
			context.AddVarContext(varContext);
		}

		public static void GenGetVTable(ILGetVTableInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenReturn(ILReturnInstruction instruction, ILContext context)
		{
			if (instruction.Operand != null)
			{
				ILVarContext varContext = context.GetVarContext(instruction.Operand.Iden);
				++varContext.UseCount;
			}
		}

		public static void GenCondBranch(ILCondBranchInstruction instruction, ILContext context)
		{
			ILVarContext varContext = context.GetVarContext(instruction.Condition.Iden);
			++varContext.UseCount;
		}

		public static void GenSwitchEnum(ILSwitchEnumInstruction instruction, ILContext context)
		{
			ILVarContext varContext = context.GetVarContext(instruction.Enum.Iden);
			++varContext.UseCount;
		}

		public static void GenSwitchValue(ILSwitchValueInstruction instruction, ILContext context)
		{
			ILVarContext varContext = context.GetVarContext(instruction.Value.Iden);
			++varContext.UseCount;
		}

	}
}
