﻿using System.Collections.Generic;
using MJC.General;

namespace MJC.ILBackend.Context
{
	public enum ILVarType
	{
		Intermediate,
		Stack,
		Param,
	}

	public class ILVarContext
	{
		public ILOperand Operand;
		public ILVarType VarType;
		public int UseCount;
		public int StoredToCount;

		// What identifiers is the variable dependent on?
		public List<string> DependentOn = new List<string>();

		public ILVarContext(ILOperand op, ILVarType varType)
		{
			Operand = op;
			VarType = varType;
		}
	}

	public class ILContext
	{
		public Dictionary<string, ILVarContext> Variables = new Dictionary<string, ILVarContext>();

		public ILCompUnitContext Parent;

		public ILCallGraphNode CallGraphNode;

		public ILContext(ILCompUnitContext parent)
		{
			Parent = parent;
		}

		public void AddVarContext(ILVarContext context)
		{
			Variables.Add(context.Operand.Iden, context);
		}

		public ILVarContext GetVarContext(string name)
		{
			if (Variables.ContainsKey(name))
				return Variables[name];
			return null;
		}

		public void RemoveVarContext(string name)
		{
			if (Variables.ContainsKey(name))
				Variables.Remove(name);
		}
	}

	public class ILCompUnitContext
	{
		public List<ILContext> Contexts = new List<ILContext>();

		public ILCallGraph CallGraph = new ILCallGraph();

		public ILCompUnit CompUnit;

		public CompilerContext CompilerContext;

		public Dictionary<string, ILFunction> ExternalFunctions = new Dictionary<string, ILFunction>();
		public Dictionary<string, ILGlobal> ExternalGlobals = new Dictionary<string, ILGlobal>();

		public ILContext CreateContext()
		{
			ILContext context = new ILContext(this);
			Contexts.Add(context);
			return context;
		}
	}
}
