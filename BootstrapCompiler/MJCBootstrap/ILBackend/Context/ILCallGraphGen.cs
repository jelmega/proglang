﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Context
{
	public static class ILCallGraphGen
	{
		public static void ProcessModule(ILCompUnit compUnit)
		{
			compUnit.Context.CallGraph = new ILCallGraph();

			foreach (KeyValuePair<string, ILFunction> pair in compUnit.Functions)
			{
				ProcessFunction(pair.Value);
			}
		}

		public static void ProcessFunction(ILFunction function)
		{
			// Skip template definitions
			if (function.IsUninstantiated)
				return;

			ILCompUnitContext parentContext = function.Context.Parent;
			function.Context.CallGraphNode = parentContext.CallGraph.AddNode(function);

			// Pregen nodes
			foreach (ILBasicBlock block in function.Blocks)
			{
				function.Context.CallGraphNode.CreateSubNode(block);
			}

			foreach (ILBasicBlock block in function.Blocks)
			{
				ProcessBasicBlock(function, block, function.Context);
			}
		}

		public static void ProcessBasicBlock(ILFunction function, ILBasicBlock block, ILContext funcContext)
		{
			Dictionary<string, ILInstruction> refs = new Dictionary<string, ILInstruction>();
			ILCallGraph callgraph = funcContext.Parent.CallGraph;
			ILCompUnit compUnit = funcContext.Parent.CompUnit;

			ILSubGraphNode curSubNode = funcContext.CallGraphNode.GetSubNode(block);

			foreach (ILInstruction instruction in block.Instructions)
			{
				switch (instruction.InstructionType)
				{
				case ILInstructionType.FunctionRef:
				{
					ILFunctionRefInstruction funcRef = instruction as ILFunctionRefInstruction;
					refs.Add(funcRef.RetOperand.Iden, funcRef);
					break;
				}
				case ILInstructionType.MethodRef:
				{
					ILMethodRefInstruction funcRef = instruction as ILMethodRefInstruction;
					refs.Add(funcRef.RetOperand.Iden, funcRef);
					break;
				}
				case ILInstructionType.OperatorRef:
				{
					ILOperatorRefInstruction opRef = instruction as ILOperatorRefInstruction;
					refs.Add(opRef.RetOperand.Iden, opRef);
					break;
				}
				case ILInstructionType.Call:
				{
					ILCallInstruction call = instruction as ILCallInstruction;
					ILInstruction callRef = refs[call.Func.Iden];

					if (callRef is ILFunctionRefInstruction funcRef)
					{
						var node = callgraph.GetNode(funcRef.MangledName);
						if (node == null)
						{
							ILFunction func = compUnit.GetFunction(funcRef.MangledName);
							node = callgraph.AddNode(func);
						}

						curSubNode.AddCall(node);
					}
					else if (callRef is ILMethodRefInstruction methodRef)
					{
						// Lookup method mangled name in VTable
						SymbolType recType = methodRef.Type.ReceiverType;
						if (recType is ReferenceSymbolType refType)
							recType = refType.BaseType;

						VTable vTable;
						if (recType is TemplateInstanceSymbolType tmp)
						{
							vTable = compUnit.GetVTable(tmp.Iden);
						}
						else
						{
							vTable = compUnit.GetVTable(recType.ToILString());
						}

						string methodName = null;
						if (recType is TemplateInstanceSymbolType instType)
						{
							List<VTable> vtableVariants = compUnit.GetVTableVariants(instType.Iden.GetBaseTemplate());
							TemplateInstanceIdentifier instIden = instType.Iden.Name as TemplateInstanceIdentifier;
							TemplateVTables tables = TemplateHelpers.GetValidVTables(vtableVariants, instIden);
							ILTemplateInstance instance = compUnit.TemplateInstances[instType.Iden];
							
							VTableMethod method = null;
							if (tables.FullSpecTable != null)
							{
								method = tables.FullSpecTable.GetMethod(methodRef.MethodRef.Element, methodRef.Type, instance);
							}
							else if (tables.PartSpecTables.Count != 0)
							{
								// Temp
								foreach (VTable table in tables.PartSpecTables)
								{
									method = table.GetMethod(methodRef.MethodRef.Element, methodRef.Type, instance);
									if (method != null)
										break;
								}
							}
							else
							{
								method = tables.BaseTable.GetMethod(methodRef.MethodRef.Element, methodRef.Type, instance);
							}
							methodName = method.MangledFunc;
						}
						else
						{
							VTableMethod method = vTable.GetMethod(methodRef.MethodRef.Element, methodRef.Type);
							methodName = method.MangledFunc;
						}

						var node = callgraph.GetNode(methodName);
						if (node == null)
						{
							ILFunction func = compUnit.GetFunction(methodName);
							node = callgraph.AddNode(func);
						}

						curSubNode.AddCall(node);
					}
					else if (callRef is ILOperatorRefInstruction opRef)
					{
						/*string typedName = ILHelpers.GetTypedIlOpName(opRef);
						var node = callgraph.GetNode(typedName);
						if (node == null)
						{
							ILOperator op = compUnit.GetOperator(typedName);
							node = callgraph.AddNode(op);
						}

						_curSubNode.AddCall(node);*/
					}

					break;
				}
				}
			}

			// Continue depending on last instruction
			if (!block.ExitsFunction())
			{
				ILInstruction lastInstr = block.Instructions.LastOrDefault();

				if (lastInstr != null)
				{
					switch (lastInstr.InstructionType)
					{
					case ILInstructionType.Branch:
					{
						ILBranchInstruction branch = lastInstr as ILBranchInstruction;
						ILSubGraphNode nextNode = funcContext.CallGraphNode.GetSubNode(branch.Label);
						curSubNode.AddSuccessor(nextNode);
						break;
					}
					case ILInstructionType.CondBranch:
					{
						ILCondBranchInstruction condBranch = lastInstr as ILCondBranchInstruction;
						ILSubGraphNode trueNode = funcContext.CallGraphNode.GetSubNode(condBranch.TrueLabel);
						ILSubGraphNode falseNode = funcContext.CallGraphNode.GetSubNode(condBranch.FalseLabel);
						curSubNode.AddSuccessor(trueNode);
						curSubNode.AddSuccessor(falseNode);
						break;
					}
					case ILInstructionType.SwitchEnum:
					{
						ILSwitchEnumInstruction switchEnum = lastInstr as ILSwitchEnumInstruction;

						if (switchEnum.Cases != null)
						{
							foreach (ILRefCase refCase in switchEnum.Cases)
							{
								ILSubGraphNode nextNode = funcContext.CallGraphNode.GetSubNode(refCase.Label);
								curSubNode.AddSuccessor(nextNode);
							}
						}
						
						ILSubGraphNode defNode = funcContext.CallGraphNode.GetSubNode(switchEnum.DefaultLabel);
						curSubNode.AddSuccessor(defNode);

						break;
					}
					case ILInstructionType.SwitchValue:
					{
						ILSwitchValueInstruction switchVal = lastInstr as ILSwitchValueInstruction;

						if (switchVal.Cases != null)
						{
							foreach (ILValCase valCase in switchVal.Cases)
							{
								ILSubGraphNode nextNode = funcContext.CallGraphNode.GetSubNode(valCase.Label);
								curSubNode.AddSuccessor(nextNode);
							}
						}
						
						ILSubGraphNode defNode = funcContext.CallGraphNode.GetSubNode(switchVal.DefaultLabel);
						curSubNode.AddSuccessor(defNode);

						break;
					}
					}
				}
				else // No terminal yet, so it will be a branch to the next block after canonicalization
				{
					int index = function.Blocks.FindIndex(bb => { return bb == block; }) + 1;
					if (function.Blocks.TryGetValue(index, out ILBasicBlock nextBlock))
					{
						ILSubGraphNode nextNode = funcContext.CallGraphNode.GetSubNode(nextBlock);
						curSubNode.AddSuccessor(nextNode);
					}
				}
			}
		}


	}
}
