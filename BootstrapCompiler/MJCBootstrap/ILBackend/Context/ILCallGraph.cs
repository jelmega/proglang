﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.ILBackend.Context
{
	public class ILSubGraphNode
	{
		public List<ILSubGraphNode> Predecessors = new List<ILSubGraphNode>();
		public List<ILSubGraphNode> Successors = new List<ILSubGraphNode>();
		public List<ILCallGraphNode> CalledGraphs = new List<ILCallGraphNode>();
		public ILBasicBlock Block;

		public ILCallGraphNode Parent;

		public ILSubGraphNode(ILBasicBlock block, ILCallGraphNode parent)
		{
			Block = block;
			Parent = parent;
		}

		public void AddPredecessor(ILSubGraphNode node)
		{
			if (!Predecessors.Contains(node))
			{
				Predecessors.Add(node);
				node.Successors.Add(this);
			}
		}

		public void AddSuccessor(ILSubGraphNode node)
		{
			if (!Successors.Contains(node))
			{
				Successors.Add(node);
				node.Predecessors.Add(this);
			}
		}

		public void AddCall(ILCallGraphNode node)
		{
			CalledGraphs.Add(node);
			Parent.AddSuccessor(node);
		}
	}

	public class ILCallGraphNode
	{
		public List<ILCallGraphNode> Predecessors = new List<ILCallGraphNode>();
		public List<ILSubGraphNode> SubNodes = new List<ILSubGraphNode>();
		public List<ILCallGraphNode> Successors = new List<ILCallGraphNode>();

		private Dictionary<ILBasicBlock, ILSubGraphNode> SubNodeMapping = new Dictionary<ILBasicBlock, ILSubGraphNode>();
		private Dictionary<string, ILSubGraphNode> SubNodeLabelMapping = new Dictionary<string, ILSubGraphNode>();

		public ILFunction Function;

		public ILCallGraph CallGraph;

		public ILCallGraphNode(ILFunction function, ILCallGraph callGraph)
		{
			Function = function;
			CallGraph = callGraph;
		}

		public void AddPredecessor(ILCallGraphNode node)
		{
			if (!Predecessors.Contains(node))
			{
				Predecessors.Add(node);
				node.Successors.Add(this);
			}
		}

		public void AddSuccessor(ILCallGraphNode node)
		{
			if (!Successors.Contains(node))
			{
				Successors.Add(node);
				node.Predecessors.Add(this);
			}
		}

		public ILSubGraphNode CreateSubNode(ILBasicBlock block)
		{
			ILSubGraphNode subNode = new ILSubGraphNode(block, this);
			SubNodes.Add(subNode);
			SubNodeMapping.Add(block, subNode);
			SubNodeLabelMapping.Add(block.Label, subNode);
			return subNode;
		}

		public ILSubGraphNode GetSubNode(ILBasicBlock block)
		{
			ILSubGraphNode subNode;
			if (SubNodeMapping.TryGetValue(block, out subNode))
				return subNode;
			return null;
		}

		public ILSubGraphNode GetSubNode(string label)
		{
			ILSubGraphNode subNode;
			if (SubNodeLabelMapping.TryGetValue(label, out subNode))
				return subNode;
			return null;
		}
	}

	public class ILCallGraph
	{
		private List<ILCallGraphNode> Nodes = new List<ILCallGraphNode>();
		private Dictionary<ILFunction, ILCallGraphNode> _functionMapping = new Dictionary<ILFunction, ILCallGraphNode>();
		private Dictionary<string, ILCallGraphNode> _nameMapping = new Dictionary<string, ILCallGraphNode>();

		public ILCallGraphNode GetNode(ILFunction function)
		{
			ILCallGraphNode node;
			if (_functionMapping.TryGetValue(function, out node))
				return node;
			return null;
		}

		public ILCallGraphNode GetNode(string name)
		{
			ILCallGraphNode node;
			if (_nameMapping.TryGetValue(name, out node))
				return node;
			return null;
		}

		public ILCallGraphNode AddNode(ILFunction function)
		{
			if (_functionMapping.TryGetValue(function, out ILCallGraphNode foundNode))
				return foundNode;

			ILCallGraphNode node = new ILCallGraphNode(function, this);
			Nodes.Add(node);
			_functionMapping.Add(function, node);
			_nameMapping.Add(function.MangledIdentifier, node);
			return node;
		}
	}
}
