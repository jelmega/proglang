﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;
using MJC.ILBackend.Interpret;

namespace MJC.ILBackend.Canonical
{
	// TODO: allow templated methods in templated aggregates
	class ILTemplateInitPass : ILCanonicalizePass
	{
		private ILInterpContext _context;

		public override void ProcessModule(ILCompUnit compUnit)
		{
			//base.ProcessModule(compUnit);
			_context = new ILInterpContext(compUnit, compUnit.Context.CompilerContext, CmdLine.IsX64);

			// Update all template value specializations
			foreach (KeyValuePair<ScopeVariable, ILAggregate> pair in compUnit.Aggregates)
			{
				ILAggregate aggr = pair.Value;
				if (aggr.TemplateParams != null)
					ResolveValueSpecializationAndDefault(aggr, compUnit);
			}

			// Update fixed size array sizes
			foreach (KeyValuePair<ArraySymbolType, TemplateInstanceSymbolType> pair in compUnit.FixedSizeArrayTypes)
			{
				ArraySymbolType arrType = pair.Key;

				ILFunction func = compUnit.GetFunction(arrType.ArraySizeFunc);
				ILInterpOperand op = ILInterpreter.InterpFunction(func, null, compUnit, _context);

				arrType.ArraySize = op.Value.ULong;
			}
			
			// Instantiate all used template instances
			Dictionary<ScopeVariable, ILTemplateInstance> instantiated = new Dictionary<ScopeVariable, ILTemplateInstance>();
			foreach (KeyValuePair<ScopeVariable, ILTemplateInstance> pair in compUnit.TemplateInstances)
			{
				ILTemplateInstance instance = pair.Value;

				// Find best template specialization
				UpdateBestTemplateDefinition(instance, compUnit);

				// Update the default values of the template instance
				TemplateHelpers.UpdateAndInferDefaults(instance.Iden.Name as TemplateInstanceIdentifier, instance.BestTemplateDefinition.Identifier as TemplateDefinitionIdentifier);

				if (instance.Type.TemplateSymbol.Kind == SymbolKind.TypeAlias)
					continue;

				if (instance.IsTypeInstance())
				{
					InstantiateType(instance, compUnit);
				}
				else // func inst
				{
					string mangledBase = instance.BestTemplateDefinition.MangledName;
					string mangled = instance.MangledName;

					// If function is already instantiated, skip it
					if (compUnit.GetFunction(mangled) != null)
						continue;

					ILFunction baseFunction = compUnit.GetFunction(mangledBase);
					InstantiateFunction(instance, baseFunction, compUnit);
				}

				instantiated.Add(pair.Key, pair.Value);
			}

			// Instantiate all used []T (Array<T>) and [N]T (Array<T,N>) instances
			// Array<T> and Array<T, N> are located in core.builtin
			if (compUnit.ArrayTypes.Count > 0 || compUnit.FixedSizeArrayTypes.Count > 0)
			{
				CompilerContext context = compUnit.Context.CompilerContext;
				FileImportDirective arrayImportDirective = new FileImportDirective("core", "builtin");
				context.ModuleImportDirective = arrayImportDirective;

				Dictionary<SymbolType, TemplateInstanceSymbolType> newArrTypes = new Dictionary<SymbolType, TemplateInstanceSymbolType>();
				Dictionary<ArraySymbolType, TemplateInstanceSymbolType> newFixedSizeArrTypes = new Dictionary<ArraySymbolType, TemplateInstanceSymbolType>();
				
				// []T
				foreach (KeyValuePair<SymbolType, TemplateInstanceSymbolType> pair in compUnit.ArrayTypes)
				{
					InstantiateArray(pair.Key, newArrTypes, newFixedSizeArrTypes, compUnit, instantiated);
				}

				// [N]T
				foreach (KeyValuePair<ArraySymbolType, TemplateInstanceSymbolType> pair in compUnit.FixedSizeArrayTypes)
				{
					InstantiateFixedSizeArray(pair.Key, newArrTypes, newFixedSizeArrTypes, compUnit, instantiated);
				}
				compUnit.ArrayTypes = newArrTypes;
				compUnit.FixedSizeArrayTypes = newFixedSizeArrTypes;

				context.ModuleImportDirective = null;
			}

			compUnit.TemplateInstances = instantiated;
		}

		void InstantiateType(ILTemplateInstance instance, ILCompUnit compUnit)
		{
			// Resolve value parameters
			ResolveValueParameters(instance, compUnit);
			TemplateInstanceIdentifier instIden = instance.Iden.Name as TemplateInstanceIdentifier;
			for (var i = 0; i < instance.Arguments.Count; i++)
			{
				ILTemplateArg ilArg = instance.Arguments[i];
				if (ilArg.Value != null)
				{
					TemplateArgument instArg = instIden.Arguments[i];
					TemplateDefinitionIdentifier defIden =  instance.Type.TemplateSymbol.Identifier as TemplateDefinitionIdentifier;

					instArg.Value = ilArg.Value;
					instArg.Type = defIden.Parameters[i].Type;
				}
			}

			// Generate symbol
			Symbol templateSymbol = instance.Type.TemplateSymbol;
			Symbol symbol = new Symbol(instance.Iden.Scope, null, instance.Iden.Name, SymbolKind.TemplateInstance, templateSymbol.Visibility, templateSymbol.Attribs);
			symbol.Type = instance.Type;

			// Generate mangled name
			string mangled = NameMangling.Mangle(symbol);
			instance.MangledName = mangled;
			symbol.MangledName = mangled;
			instance.Type.MangledName = mangled;

			compUnit.Context.CompilerContext.Symbols.AddSymbol(symbol);

			// If function is already instantiated, skip it
			if (compUnit.GetAggregate(mangled) != null ||
			    compUnit.GetEnum(mangled) != null ||
			    compUnit.GetTypedef(mangled) != null)
				return;

			switch (templateSymbol.Kind)
			{
			case SymbolKind.Struct:
			case SymbolKind.Union:
			{
				ILAggregate baseAggr = ILTemplateHelpers.GetBestBaseAggregate(instance, compUnit);
				InstantiateAggregate(instance, baseAggr, compUnit);

				// vtable
				InstantiateVTables(instance, compUnit);
				break;
			}
			case SymbolKind.Enum:
			{
				ILEnum baseEnum = ILTemplateHelpers.GetBestBaseEnum(instance, compUnit);
				InstantiateEnum(instance, baseEnum, compUnit);

				InstantiateVTables(instance, compUnit);
				break;
			}
			case SymbolKind.Typedef:
			{
				SymbolType innerType = templateSymbol.Type.GetInnerType();
				TemplateInstanceSymbolType tdType = innerType as TemplateInstanceSymbolType;
				TemplateInstanceIdentifier tdInstIden = tdType.Iden.Name as TemplateInstanceIdentifier;
				TemplateDefinitionIdentifier tdDefIden = tdInstIden.BaseIden;
				
				ScopeVariable scopeVar = new ScopeVariable(tdType.Iden.Scope, null);
				List<ILTemplateArg> args = new List<ILTemplateArg>();

				TemplateInstanceIdentifier newInstIden = new TemplateInstanceIdentifier(tdDefIden);
				for (var i = 0; i < tdInstIden.Arguments.Count; i++)
				{
					TemplateArgument argument = tdInstIden.Arguments[i];
					SymbolType type = ILTemplateHelpers.GetInstancedType(argument.Type, instance);
					newInstIden.Arguments[i].Type = type;
					newInstIden.Arguments[i].Value = argument.Value;

					ILTemplateArg ilArg = new ILTemplateArg(type, null);
					ilArg.Value = argument.Value;
					args.Add(ilArg);
				}

				scopeVar.Name = newInstIden;
				TemplateInstanceSymbolType subInstType = SymbolType.TemplateInstanceType(scopeVar, tdType.TemplateSymbol);
				ILTemplateInstance newInst = new ILTemplateInstance(scopeVar, subInstType, args);

				InstantiateType(newInst, compUnit);

				// Typedef instantiation to the compile unit
				ILTypedef typedef = new ILTypedef(symbol, subInstType);
				compUnit.AddTypedef(typedef);

				break;
			}
			}
		}

		void InstantiateAggregate(ILTemplateInstance instance, ILAggregate baseAggr, ILCompUnit compUnit)
		{
			ILAggregate aggregate = new ILAggregate(baseAggr.Type, instance.Iden, instance.MangledName, instance.Type.TemplateSymbol, baseAggr.Attributes, null, baseAggr);

			foreach (ILAggregateVariable baseVar in baseAggr.Variables)
			{
				SymbolType varType = ILTemplateHelpers.GetInstancedType(baseVar.Type, instance);
				string mangledVar = "_M" + NameMangling.MangleScopeVar(instance.Iden) + NameMangling.MangleIdentifier(baseVar.Identifier) + 'V' + NameMangling.MangleType(varType);

				ILAggregateVariable aggrVar = new ILAggregateVariable(baseVar.Identifier, mangledVar, varType);
				aggregate.Variables.Add(aggrVar);
			}

			compUnit.AddAggregate(aggregate, true);
		}

		void InstantiateEnum(ILTemplateInstance instance, ILEnum baseEnum, ILCompUnit compUnit)
		{
			ILEnum ilEnum = new ILEnum(instance.Iden, instance.MangledName, instance.Type.TemplateSymbol, baseEnum.Attributes, null, baseEnum.BaseType, baseEnum);

			foreach (ILEnumMember baseMember in baseEnum.Members)
			{
				SymbolType memberType = null;
				string mangledInit = null;
				if (baseMember.AdtType is AggregateSymbolType aggrType)
				{
					Symbol aggrSym = aggrType.Symbol;
					TemplateDefinitionIdentifier aggrDefIden = aggrSym.Identifier as TemplateDefinitionIdentifier;
					TemplateInstanceIdentifier aggrInstIden = ILTemplateHelpers.GetInstanceName(aggrDefIden, instance.Arguments);
					Scope scope = new Scope(ilEnum.Identifier);
					ScopeVariable scopeVar = new ScopeVariable(scope, aggrInstIden);
					TemplateInstanceSymbolType instType = SymbolType.TemplateInstanceType(scopeVar, aggrSym);

					Symbol symbol = new Symbol(scope, null, aggrInstIden, SymbolKind.TemplateInstance, aggrSym.Visibility, aggrSym.Attribs);
					symbol.Type = instance.Type;
					string mangled = NameMangling.Mangle(symbol);
					symbol.MangledName = mangled;
					compUnit.Context.CompilerContext.Symbols.AddSymbol(symbol);

					ILTemplateInstance aggrInst = new ILTemplateInstance(scopeVar, instType, instance.Arguments);
					aggrInst.MangledName = mangled;
					aggrInst.Type.MangledName = mangled;
					ILAggregate baseAggr = compUnit.GetAggregate(aggrSym.ScopeVar);

					InstantiateAggregate(aggrInst, baseAggr, compUnit);

					memberType = instType;
					
					ILFunction func = compUnit.GetFunction(baseMember.AdtInit);
					Identifier funcIden = func.Identifier.Name;

					ScopeVariable funcScopeVar = new ScopeVariable(new Scope(scopeVar), funcIden);

					SymbolType funcType = ILTemplateHelpers.GetInstancedType(func.FuncType, instance);
					mangledInit = "_M" + NameMangling.MangleScopeVar(funcScopeVar) + NameMangling.MangleType(funcType);

					InstantiateFunction(instance, func, compUnit, mangledInit);
				}
				else if (baseMember.AdtType is TupleSymbolType)
				{
					memberType = ILTemplateHelpers.GetInstancedType(baseMember.AdtType, instance);
				}
				
				ILEnumMember member = new ILEnumMember(baseMember.Identifier, baseMember.EnumInit, memberType, mangledInit);
				ilEnum.AddMember(member);
			}

			compUnit.AddEnum(ilEnum, true);
		}

		void InstantiateVTables(ILTemplateInstance instance, ILCompUnit compUnit)
		{
			VTable vTable = new VTable(instance.Iden, instance.MangledName);
			CompilerContext compilerContext = compUnit.Context.CompilerContext;
			Dictionary<ScopeVariable, List<VTable>> vtables = compilerContext.GetAllVTables();
			List<VTableMethod> methods = TemplateHelpers.GetInstanceMethods(instance.Iden, instance.Type.TemplateSymbol, vtables);

			foreach (VTableMethod baseMethod in methods)
			{
				FunctionSymbolType funcType = ILTemplateHelpers.GetInstancedType(baseMethod.Type, instance) as FunctionSymbolType;

				string mangledMethod = "_M" + NameMangling.MangleScopeVar(instance.Iden) + NameMangling.MangleIdentifier(baseMethod.Iden) + NameMangling.MangleType(funcType);
				VTableMethod method = new VTableMethod(baseMethod.Iden, funcType, mangledMethod);

				vTable.AddMethod(method, instance.Type.TemplateSymbol.Interfaces);
				if (compUnit.GetFunction(mangledMethod) != null)
					continue;

				ILFunction baseFunc = compUnit.GetFunction(baseMethod.MangledFunc);
				InstantiateFunction(instance, baseFunc, compUnit, mangledMethod);
			}

			compUnit.AddVTable(vTable, true);
			compUnit.Context.CompilerContext.AddVTable(vTable);
		}

		void InstantiateFunction(ILTemplateInstance instance, ILFunction baseFunction, ILCompUnit compUnit, string mangledMethod = null)
		{
			List<ILOperand> operands = new List<ILOperand>();
			Dictionary<string, ILOperand> opMapping = new Dictionary<string, ILOperand>();
			foreach (ILOperand baseOp in baseFunction.Operands)
			{
				SymbolType type = ILTemplateHelpers.GetInstancedType(baseOp.Type, instance);
				ILOperand op = new ILOperand(baseOp.Iden, type);
				operands.Add(op);
				opMapping.Add(op.Iden, op);
			}

			SymbolType funcType = ILTemplateHelpers.GetInstancedType(baseFunction.FuncType, instance);
			ILFunction function = new ILFunction(instance.Iden, mangledMethod ?? instance.MangledName, operands, funcType as FunctionSymbolType, baseFunction.Attributes, null, false);

			foreach (ILBasicBlock baseBB in baseFunction.Blocks)
			{
				ILBasicBlock bb = InstantiateBasicBlock(instance, baseBB, function, opMapping);
				function.Blocks.Add(bb);
			}

			compUnit.AddFunction(function);

			// Generate context and callgraph
			ILContextGen.ProcessFunction(function, compUnit);
			ILCallGraphGen.ProcessFunction(function);
		}

		ILBasicBlock InstantiateBasicBlock(ILTemplateInstance instance, ILBasicBlock baseBB, ILFunction function, Dictionary<string, ILOperand> opMapping)
		{
			ILBasicBlock bb = new ILBasicBlock(baseBB.Label);
			
			foreach (ILInstruction baseInstr in baseBB.Instructions)
			{
				ILInstruction instr = null;

				switch (baseInstr.InstructionType)
				{
				case ILInstructionType.IntLiteral:
				{
					ILIntLiteralInstruction baseIntLit = baseInstr as ILIntLiteralInstruction;
					ILOperand op = ILTemplateHelpers.GetInstancedOperand(baseIntLit.RetOperand, instance);
					opMapping.Add(op.Iden, op);
					instr = new ILIntLiteralInstruction(op, baseIntLit.Value, baseIntLit.Span);
					break;
				}
				case ILInstructionType.FloatLiteral:
				{
					ILFloatLiteralInstruction baseFloatLit = baseInstr as ILFloatLiteralInstruction;
					ILOperand op = ILTemplateHelpers.GetInstancedOperand(baseFloatLit.RetOperand, instance);
					opMapping.Add(op.Iden, op);
					instr = new ILFloatLiteralInstruction(op, baseFloatLit.HexValue, baseFloatLit.Span);
					break;
				}
				case ILInstructionType.StringLiteral:
				{
					ILStringLiteralInstruction baseStrLit = baseInstr as ILStringLiteralInstruction;
					ILOperand op = ILTemplateHelpers.GetInstancedOperand(baseStrLit.RetOperand, instance);
					instr = new ILStringLiteralInstruction(op, baseStrLit.String, baseStrLit.Span);
					opMapping.Add(op.Iden, op);
					break;
				}
				case ILInstructionType.AllocStack:
				{
					ILAllocStackInstruction baseAlloc = baseInstr as ILAllocStackInstruction;
					SymbolType type = ILTemplateHelpers.GetInstancedType(baseAlloc.Type, instance);
					ILOperand op = new ILOperand(baseAlloc.RetOperand.Iden, SymbolType.MemoryLocType(type));
					opMapping.Add(op.Iden, op);
					instr = new ILAllocStackInstruction(op, type, baseAlloc.Span);
					break;
				}
				case ILInstructionType.DeallocStack:
				{
					ILDeallocStackInstruction baseDealloc = baseInstr as ILDeallocStackInstruction;
					ILOperand op = opMapping[baseDealloc.Operand.Iden];
					instr = new ILDeallocStackInstruction(op);
					break;
				}
				case ILInstructionType.Assign:
				{
					ILAssignInstruction baseAssign = baseInstr as ILAssignInstruction;
					ILOperand src = opMapping[baseAssign.Src.Iden];
					ILOperand dst = opMapping[baseAssign.Dst.Iden];
					instr = new ILAssignInstruction(src, dst, baseAssign.Span);
					break;
				}
				case ILInstructionType.Store:
				{
					ILStoreInstruction baseStore = baseInstr as ILStoreInstruction;
					ILOperand src = opMapping[baseStore.Src.Iden];
					ILOperand dst = opMapping[baseStore.Dst.Iden];
					instr = new ILStoreInstruction(src, dst, baseStore.Span);
					break;
				}
				case ILInstructionType.Load:
				{
					ILLoadInstruction baseLoad = baseInstr as ILLoadInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseLoad.RetOperand, instance);
					ILOperand op = opMapping[baseLoad.Src.Iden];
					opMapping.Add(ret.Iden, ret);
					instr = new ILLoadInstruction(ret, op, baseLoad.Span);
					break;
				}
				case ILInstructionType.FunctionRef:
				{
					ILOperatorRefInstruction baseRef = baseInstr as ILOperatorRefInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseRef.RetOperand, instance);
					FunctionSymbolType type = ILTemplateHelpers.GetInstancedType(baseRef.Type, instance) as FunctionSymbolType;
					opMapping.Add(ret.Iden, ret);
					instr = new ILOperatorRefInstruction(ret, baseRef.Loc, baseRef.Op, type, baseRef.Span);
					break;
				}
				case ILInstructionType.MethodRef:
				{
					ILMethodRefInstruction baseRef = baseInstr as ILMethodRefInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseRef.RetOperand, instance);
					FunctionSymbolType type = ILTemplateHelpers.GetInstancedType(baseRef.Type, instance) as FunctionSymbolType;
					opMapping.Add(ret.Iden, ret);
					instr = new ILMethodRefInstruction(ret, baseRef.MethodRef, type, baseRef.Span);
					break;
				}
				case ILInstructionType.OperatorRef:
				{
					ILOperatorRefInstruction baseRef = baseInstr as ILOperatorRefInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseRef.RetOperand, instance);
					FunctionSymbolType type = ILTemplateHelpers.GetInstancedType(baseRef.Type, instance) as FunctionSymbolType;
					opMapping.Add(ret.Iden, ret);
					instr = new ILOperatorRefInstruction(ret, baseRef.Loc, baseRef.Op, type, baseRef.Span);
					break;
				}
				case ILInstructionType.Call:
				{
					ILCallInstruction baseCall = baseInstr as ILCallInstruction;
					ILOperand ret = baseCall.RetOperand != null ? ILTemplateHelpers.GetInstancedOperand(baseCall.RetOperand, instance) : null;
					ILOperand func = opMapping[baseCall.Func.Iden];

					List<ILOperand> ops = null;
					if (baseCall.Operands != null)
					{
						ops = new List<ILOperand>();
						foreach (ILOperand param in baseCall.Operands)
						{
							ops.Add(opMapping[param.Iden]);
						}
					}

					if (ret != null)
						opMapping.Add(ret.Iden, ret);
					instr = new ILCallInstruction(ret, func, ops, baseCall.Span);
					break;
				}
				case ILInstructionType.Builtin:
				{
					ILBuiltinInstruction baseBuiltin = baseInstr as ILBuiltinInstruction;
					ILOperand ret = baseBuiltin.RetOperand != null ? ILTemplateHelpers.GetInstancedOperand(baseBuiltin.RetOperand, instance) : null;

					List<ILOperand> ops = null;
					if (baseBuiltin.Operands != null)
					{
						ops = new List<ILOperand>();
						foreach (ILOperand param in baseBuiltin.Operands)
						{
							ops.Add(new ILOperand(param));
						}
					}

					if (ret != null)
						opMapping.Add(ret.Iden, ret);
					instr = new ILBuiltinInstruction(ret, baseBuiltin.Builtin, ops, baseBuiltin.Span);
					break;
				}
				case ILInstructionType.CompilerIntrin:
				{
					ILCompilerIntrinInstruction baseIntrin = baseInstr as ILCompilerIntrinInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseIntrin.RetOperand, instance);
					SymbolType type = ILTemplateHelpers.GetInstancedType(baseIntrin.Type, instance);
					opMapping.Add(ret.Iden, ret);
					instr = new ILCompilerIntrinInstruction(ret, baseIntrin.Intrin, type, baseIntrin.Span);
					break;
				}
				case ILInstructionType.TupleUnpack:
				{
					ILTupleUnpackInstruction baseUnpack = baseInstr as ILTupleUnpackInstruction;

					List<ILOperand> retOps = new List<ILOperand>();
					foreach (ILOperand baseRet in baseUnpack.RetOperands)
					{
						ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseRet, instance);
						retOps.Add(ret);
						opMapping.Add(ret.Iden, ret);
					}

					ILOperand tuple = opMapping[baseUnpack.Operand.Iden];
					instr = new ILTupleUnpackInstruction(retOps, tuple, baseUnpack.Span);

					break;
				}
				case ILInstructionType.TuplePack:
				{
					ILTuplePackInstruction basePack = baseInstr as ILTuplePackInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(basePack.RetOperand, instance);

					List<ILOperand> ops = new List<ILOperand>();
					foreach (ILOperand baseOp in basePack.Operands)
					{
						ILOperand op = opMapping[baseOp.Iden];
						ops.Add(op);
					}

					opMapping.Add(ret.Iden, ret);
					instr = new ILTuplePackInstruction(ret, ops, basePack.Span);
					break;
				}
				case ILInstructionType.TupleInsert:
				{
					ILTupleInsertInstruction baseInsert = baseInstr as ILTupleInsertInstruction;
					ILOperand tup = opMapping[baseInsert.Tuple.Iden];
					ILOperand val = opMapping[baseInsert.Value.Iden];
					instr = new ILTupleInsertInstruction(tup, baseInsert.Index, val, baseInsert.Span);
					break;
				}
				case ILInstructionType.TupleExtract:
				{
					ILTupleExtractInstruction baseExtract = baseInstr as ILTupleExtractInstruction;
					ILOperand tup = opMapping[baseExtract.Tuple.Iden];
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseExtract.RetOperand, instance);
					opMapping.Add(ret.Iden, ret);
					instr = new ILTupleExtractInstruction(ret, tup, baseExtract.Index, baseExtract.Span);
					break;
				}
				case ILInstructionType.TupleElemAddr:
				{
					ILTupleElemAddrInstruction baseElemAddr = baseInstr as ILTupleElemAddrInstruction;
					ILOperand tup = opMapping[baseElemAddr.Tuple.Iden];
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseElemAddr.RetOperand, instance);
					opMapping.Add(ret.Iden, ret);
					instr = new ILTupleElemAddrInstruction(ret, tup, baseElemAddr.Index, baseElemAddr.Span);
					break;
				}
				case ILInstructionType.StructExtract:
				{
					ILStructExtractInstruction baseExtract = baseInstr as ILStructExtractInstruction;
					ILOperand str = opMapping[baseExtract.Struct.Iden];
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseExtract.RetOperand, instance);
					opMapping.Add(ret.Iden, ret);
					instr = new ILStructExtractInstruction(ret, str, baseExtract.ElemRef, baseExtract.Span);
					break;
				}
				case ILInstructionType.StructInsert:
				{
					ILStructInsertInstruction baseInsert = baseInstr as ILStructInsertInstruction;
					ILOperand str = opMapping[baseInsert.Struct.Iden];
					ILOperand val = opMapping[baseInsert.Value.Iden];
					instr = new ILStructInsertInstruction(str, baseInsert.ElemRef, val, baseInsert.Span);
					break;
				}
				case ILInstructionType.StructElemAddr:
				{
					ILStructElemAddrInstruction baseElemAddr = baseInstr as ILStructElemAddrInstruction;
					ILOperand str = opMapping[baseElemAddr.Struct.Iden];
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseElemAddr.RetOperand, instance);
					opMapping.Add(ret.Iden, ret);
					instr = new ILStructElemAddrInstruction(ret, str, baseElemAddr.ElemRef, baseElemAddr.Span);
					break;
				}
				case ILInstructionType.UnionExtract:
				{
					ILUnionExtractInstruction baseInsert = baseInstr as ILUnionExtractInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseInsert.RetOperand, instance);
					ILOperand str = opMapping[baseInsert.Union.Iden];
					opMapping.Add(ret.Iden, ret);
					instr = new ILStructExtractInstruction(ret, str, baseInsert.ElemRef, baseInsert.Span);
					break;
				}
				case ILInstructionType.UnionInsert:
				{
					ILUnionInsertInstruction baseInsert = baseInstr as ILUnionInsertInstruction;
					ILOperand str = opMapping[baseInsert.Union.Iden];
					ILOperand val = opMapping[baseInsert.Value.Iden];
					instr = new ILStructInsertInstruction(str, baseInsert.ElemRef, val, baseInsert.Span);
					break;
				}
				case ILInstructionType.UnionElemAddr:
				{
					ILUnionElemAddrInstruction baseElemAddr = baseInstr as ILUnionElemAddrInstruction;
					ILOperand uni = opMapping[baseElemAddr.Union.Iden];
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseElemAddr.RetOperand, instance);
					opMapping.Add(ret.Iden, ret);
					instr = new ILUnionElemAddrInstruction(ret, uni, baseElemAddr.ElemRef, baseElemAddr.Span);
					break;
				}
				case ILInstructionType.Destruct:
				{
					// TODO
					break;
				}
				case ILInstructionType.EnumValue:
				{
					ILEnumValueInstruction baseEnuVal = baseInstr as ILEnumValueInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseEnuVal.RetOperand, instance);
					SymbolType type = ILTemplateHelpers.GetInstancedType(baseEnuVal.Type, instance);

					List<ILOperand> operands = null;
					if (baseEnuVal.Operands != null)
					{
						operands = new List<ILOperand>();
						foreach (ILOperand baseOp in baseEnuVal.Operands)
						{
							ILOperand op = opMapping[baseOp.Iden];
							operands.Add(op);
						}
					}

					opMapping.Add(ret.Iden, ret);
					instr = new ILEnumValueInstruction(ret, type, baseEnuVal.ElemRef, operands, baseEnuVal.Span);
					break;
				}
				case ILInstructionType.EnumGetValue:
				{
					ILEnumGetValueInstruction baseGetVal = baseInstr as ILEnumGetValueInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseGetVal.RetOperand, instance);
					ILOperand op = opMapping[baseGetVal.Operand.Iden];
					opMapping.Add(ret.Iden, ret);
					instr = new ILEnumGetValueInstruction(ret, op, baseGetVal.Span);
					break;
				}
				case ILInstructionType.EnumGetMemberValue:
				{
					ILEnumGetMemberValueInstruction baseGetMemVal = baseInstr as ILEnumGetMemberValueInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseGetMemVal.RetOperand, instance);
					SymbolType type = ILTemplateHelpers.GetInstancedType(baseGetMemVal.Type, instance);
					opMapping.Add(ret.Iden, ret);
					instr = new ILEnumGetMemberValueInstruction(ret, type, baseGetMemVal.MemberRef, baseGetMemVal.Span);
					break;
				}
				case ILInstructionType.TemplateGetValue:
				{
					ILTemplateGetValueInstruction baseGetVal = baseInstr as ILTemplateGetValueInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseGetVal.RetOperand, instance);
					SymbolType type = ILTemplateHelpers.GetInstancedType(baseGetVal.TemplateType, instance);
					opMapping.Add(ret.Iden, ret);
					instr = new ILTemplateGetValueInstruction(ret, type, baseGetVal.ElemRef, baseGetVal.Span);
					break;
				}
				case ILInstructionType.GlobalAddr:
				{
					ILGlobalAddrInstruction baseGlob = baseInstr as ILGlobalAddrInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseGlob.RetOperand, instance);
					opMapping.Add(ret.Iden, ret);
					instr = new ILGlobalAddrInstruction(ret, baseGlob.Identifier, baseGlob.Span);
					break;
				}
				case ILInstructionType.GlobalValue:
				{
					ILGlobalValueInstruction baseGlob = baseInstr as ILGlobalValueInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseGlob.RetOperand, instance);
					opMapping.Add(ret.Iden, ret);
					instr = new ILGlobalValueInstruction(ret, baseGlob.Identifier, baseGlob.Span);
					break;
				}
				case ILInstructionType.ConvType:
				{
					ILConvertTypeInstruction baseConv = baseInstr as ILConvertTypeInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseConv.RetOperand, instance);
					ILOperand op = opMapping[baseConv.Operand.Iden];
					opMapping.Add(ret.Iden, ret);
					instr = new ILConvertTypeInstruction(ret, op, baseConv.Span);
					break;
				}
				case ILInstructionType.GetVTable:
				{
					ILGetVTableInstruction baseVTable = baseInstr as ILGetVTableInstruction;
					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseVTable.RetOperand, instance);
					SymbolType type = ILTemplateHelpers.GetInstancedType(baseVTable.Type, instance);
					List<SymbolType> interfaceTypes = new List<SymbolType>();
					foreach (SymbolType interfaceType in baseVTable.InterfaceTypes)
					{
						interfaceTypes.Add(ILTemplateHelpers.GetInstancedType(interfaceType, instance));
					}

					instr = new ILGetVTableInstruction(ret, type, interfaceTypes);
					break;
				}
				case ILInstructionType.Return:
				{
					ILReturnInstruction baseRet = baseInstr as ILReturnInstruction;
					ILOperand op = baseRet.Operand != null ? opMapping[baseRet.Operand.Iden] : null;
					instr = new ILReturnInstruction(op, baseRet.Span);
					break;
				}
				case ILInstructionType.Branch:
				{
					ILBranchInstruction baseBranch = baseInstr as ILBranchInstruction;
					instr = new ILBranchInstruction(baseBranch.Label, baseBranch.Span);
					break;
				}
				case ILInstructionType.CondBranch:
				{
					ILCondBranchInstruction baseBranch = baseInstr as ILCondBranchInstruction;
					ILOperand op = opMapping[baseBranch.Condition.Iden];
					instr = new ILCondBranchInstruction(op, baseBranch.TrueLabel, baseBranch.FalseLabel, baseBranch.Span);
					break;
				}
				case ILInstructionType.SwitchEnum:
				{
					ILSwitchEnumInstruction baseSwitch = baseInstr as ILSwitchEnumInstruction;
					ILOperand op = opMapping[baseSwitch.Enum.Iden];
					instr = new ILSwitchEnumInstruction(op, baseSwitch.Cases, baseSwitch.DefaultLabel, baseSwitch.Span);
					break;
				}
				case ILInstructionType.SwitchValue:
				{
					ILSwitchValueInstruction baseSwitch = baseInstr as ILSwitchValueInstruction;
					ILOperand op = opMapping[baseSwitch.Value.Iden];
					instr = new ILSwitchValueInstruction(op, baseSwitch.Cases, baseSwitch.DefaultLabel, baseSwitch.Span);
					break;
				}
				default:
					instr = null;
					break;
				}

				bb.Instructions.Add(instr);
			}
			return bb;
		}

		void ResolveValueParameters(ILTemplateInstance instance, ILCompUnit compUnit)
		{
			foreach (ILTemplateArg arg in instance.Arguments)
			{
				if (arg.ValueFunc != null)
				{
					ILFunction func = compUnit.GetFunction(arg.ValueFunc);
					ILInterpOperand interpOp = ILInterpreter.InterpFunction(func, null, compUnit, _context);

					// Interpreter will only support long types
					arg.Value = interpOp.Value.Long.ToString();

					/*BuiltinSymbolType builtin = interpOp.Operand.Type as BuiltinSymbolType;
					switch (builtin.Builtin)
					{
					//
					default:
						arg.Value = interpOp.Value.Long.ToString();
						break;
					case BuiltinTypes.StringLiteral:
						arg.Value = '"' + sval + '"';
						break;
					}*/
				}
			}
		}

		void ResolveValueSpecializationAndDefault(ILAggregate aggr, ILCompUnit compUnit)
		{
			TemplateDefinitionIdentifier defIden = aggr.Identifier.Name as TemplateDefinitionIdentifier;
			for (var i = 0; i < aggr.TemplateParams.Count; i++)
			{
				ILTemplateParam ilParam = aggr.TemplateParams[i];
				TemplateParameter param = defIden.Parameters[i];
				if (ilParam.ValueSpecializationFunc != null && string.IsNullOrEmpty(ilParam.ValueSpecialization))
				{
					ILFunction func = compUnit.GetFunction(ilParam.ValueSpecializationFunc);
					ILInterpOperand interpOp = ILInterpreter.InterpFunction(func, null, compUnit, _context);
					ilParam.ValueSpecialization = interpOp.Value.Long.ToString();

					/*switch (interpOp.Value)
					{
					case long _:
					case double _:
						ilParam.ValueSpecialization = interpOp.Value.ToString();
						break;
					case string sval:
						ilParam.ValueSpecialization = '"' + sval + '"';
						break;
					}*/
					param.ValueSpecialization = ilParam.ValueSpecialization;
				}

				if (ilParam.ValueDefaultFunc != null && string.IsNullOrEmpty(ilParam.ValueDefault))
				{
					ILFunction func = compUnit.GetFunction(ilParam.ValueDefaultFunc);
					ILInterpOperand interpOp = ILInterpreter.InterpFunction(func, null, compUnit, _context);
					ilParam.ValueSpecialization = interpOp.Value.Long.ToString();

					/*switch (interpOp.Value)
					{
					case long _:
					case double _:
						ilParam.ValueDefault = interpOp.Value.ToString();
						break;
					case string sval:
						ilParam.ValueDefault = '"' + sval + '"';
						break;
					}*/
					param.ValueDefault = ilParam.ValueDefault;
				}
			}
		}

		void UpdateBestTemplateDefinition(ILTemplateInstance instance, ILCompUnit compUnit)
		{
			CompilerContext compilerContext = compUnit.Context.CompilerContext;

			List<Symbol> compatibleTemplates = compilerContext.FindCompatibleTemplates(instance.Iden.Scope, instance.Iden.Name as TemplateInstanceIdentifier, null);

			if (compatibleTemplates.Count == 1)
			{
				instance.BestTemplateDefinition = compatibleTemplates[0];
				return;
			}

			int minParams = -1;
			BitArray bestMatch = null;
			Symbol bestSymbol = null;
			foreach (Symbol symbol in compatibleTemplates)
			{
				TemplateDefinitionIdentifier defIden = symbol.Identifier as TemplateDefinitionIdentifier;
				int numParams = defIden.Parameters.Count;

				BitArray specParams = new BitArray(numParams);
				for (var i = 0; i < defIden.Parameters.Count; i++)
				{
					TemplateParameter param = defIden.Parameters[i];
					ILTemplateArg arg = instance.Arguments[i];

					if (param.ValueName == null)
					{
						if (param.TypeSpecialization != null)
						{
							if (param.TypeSpecialization != arg.Type)
								continue;

							specParams[i] = true;
						}
					}
					else
					{
						if (param.ValueSpecialization != null)
						{
							if (param.ValueSpecialization != arg.Value)
								continue;

							specParams[i] = true;
						}
					}
				}

				if (bestSymbol == null ||
				    (minParams <= numParams &&
				     specParams.NumBitsSet() > bestMatch.NumBitsSet()))
				{
					minParams = numParams;
					bestMatch = specParams;
					bestSymbol = symbol;
				}
			}
		}

		void InstantiateArray(SymbolType baseType, Dictionary<SymbolType, TemplateInstanceSymbolType> arrays, Dictionary<ArraySymbolType, TemplateInstanceSymbolType> fixedSizeArrays, ILCompUnit compUnit, Dictionary<ScopeVariable, ILTemplateInstance> instances)
		{
			if (arrays.ContainsKey(baseType))
				return;

			// baseType: T
			// return:   core::builtin::Array!<#{T}>
			SymbolType instBase = GetNoAttribReplacedArrType(baseType, arrays, fixedSizeArrays, compUnit, instances);

			TemplateInstanceIdentifier nameIden = new TemplateInstanceIdentifier("Array", 1);
			nameIden.Arguments[0].Type = instBase;

			// core::builtin::Array<T>
			ScopeVariable scopeVar = new ScopeVariable(new Identifier[] { new NameIdentifier("core"), new NameIdentifier("builtin"), nameIden });

			CompilerContext context = compUnit.Context.CompilerContext;
			Symbol templateSym = context.FindTemplate(Scope.Empty, scopeVar);

			TemplateInstanceSymbolType type = SymbolType.TemplateInstanceType(scopeVar, templateSym);
			ILTemplateArg ilArg = new ILTemplateArg(instBase, null);

			ILTemplateInstance instance = new ILTemplateInstance(scopeVar, type, new List<ILTemplateArg> { ilArg });

			InstantiateType(instance, compUnit);

			arrays.Add(baseType, type);
			instances.Add(scopeVar, instance);
		}

		void InstantiateFixedSizeArray(ArraySymbolType arrType, Dictionary<SymbolType, TemplateInstanceSymbolType> arrays, Dictionary<ArraySymbolType, TemplateInstanceSymbolType> fixedSizeArrays, ILCompUnit compUnit, Dictionary<ScopeVariable, ILTemplateInstance> instances)
		{
			if (fixedSizeArrays.ContainsKey(arrType))
				return;

			// arrType: [N]T
			// return:  core::builtin::Array!<#{T}, N>
			SymbolType instBase = GetNoAttribReplacedArrType(arrType.BaseType, arrays, fixedSizeArrays, compUnit, instances);

			SymbolType indexType = SymbolType.BuiltinType(CmdLine.IsX64 ? 8 : 4, BuiltinTypes.U8);

			TemplateInstanceIdentifier nameIden = new TemplateInstanceIdentifier("Array", 2);
			nameIden.Arguments[0].Type = instBase;
			nameIden.Arguments[1].Type = indexType;
			nameIden.Arguments[1].Value = arrType.ArraySize.ToString();

			// core::builtin::Array<T, N:usize>
			ScopeVariable scopeVar = new ScopeVariable(new Identifier[] { new NameIdentifier("core"), new NameIdentifier("builtin"), nameIden });

			CompilerContext context = compUnit.Context.CompilerContext;
			Symbol templateSym = context.FindTemplate(Scope.Empty, scopeVar);

			TemplateInstanceSymbolType type = SymbolType.TemplateInstanceType(scopeVar, templateSym);
			ILTemplateArg ilArg0 = new ILTemplateArg(arrType.BaseType, null);
			ILTemplateArg ilArg1 = new ILTemplateArg(indexType, null);
			ilArg1.Value = arrType.ArraySize.ToString();

			ILTemplateInstance instance = new ILTemplateInstance(scopeVar, type, new List<ILTemplateArg> { ilArg0, ilArg1 });

			InstantiateType(instance, compUnit);

			fixedSizeArrays.Add(arrType, type);
			instances.Add(scopeVar, instance);
		}

		SymbolType GetNoAttribReplacedArrType(SymbolType type, Dictionary<SymbolType, TemplateInstanceSymbolType> arrays, Dictionary<ArraySymbolType, TemplateInstanceSymbolType> fixedSizeArrays, ILCompUnit compUnit, Dictionary<ScopeVariable, ILTemplateInstance> instances)
		{
			switch (type)
			{
			default:
				return ILHelpers.GetTypeWithoutAttribs(type);
			case ArraySymbolType arrType:
			{
				TemplateInstanceSymbolType instType;
				if (arrType.ArraySizeFunc != null)
				{
					if (!fixedSizeArrays.ContainsKey(arrType))
						InstantiateFixedSizeArray(arrType, arrays, fixedSizeArrays, compUnit, instances);

					instType = fixedSizeArrays[arrType];
				}
				else
				{
					if (!arrays.ContainsKey(arrType))
						InstantiateArray(arrType.BaseType, arrays, fixedSizeArrays, compUnit, instances);

					instType = arrays[arrType.BaseType];
				}
				return instType;
			}
			case BitFieldSymbolType bitFieldType:
			{
				SymbolType baseType = GetNoAttribReplacedArrType(bitFieldType.BaseType, arrays, fixedSizeArrays, compUnit, instances);
				return SymbolType.BitFieldType(baseType, bitFieldType.Bits);
			}
			case DelegateSymbolType delType:
			{
				List<SymbolType> paramTypes = null;
				if (delType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType paramType in delType.ParamTypes)
					{
						SymbolType tmp = GetNoAttribReplacedArrType(paramType, arrays, fixedSizeArrays, compUnit, instances);
						paramTypes.Add(tmp);
					}
				}

				SymbolType retType = delType.ReturnType == null ? null : GetNoAttribReplacedArrType(delType.ReturnType, arrays, fixedSizeArrays, compUnit, instances);

				return SymbolType.DelegateType(delType.Identifier, paramTypes, retType);
			}
			case FunctionSymbolType funcType:
			{
				List<SymbolType> paramTypes = null;
				if (funcType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType paramType in funcType.ParamTypes)
					{
						SymbolType tmp = GetNoAttribReplacedArrType(paramType, arrays, fixedSizeArrays, compUnit, instances);
						paramTypes.Add(tmp);
					}
				}

				SymbolType recType = funcType.ReceiverType == null ? null : GetNoAttribReplacedArrType(funcType.ReceiverType, arrays, fixedSizeArrays, compUnit, instances);
				SymbolType retType = funcType.ReturnType == null ? null : GetNoAttribReplacedArrType(funcType.ReturnType, arrays, fixedSizeArrays, compUnit, instances);

				return SymbolType.FunctionType(recType, paramTypes, retType);
			}
			case MemoryLocSymbolType memoryLocType:
			{
				SymbolType baseType = GetNoAttribReplacedArrType(memoryLocType.BaseType, arrays, fixedSizeArrays, compUnit, instances);
				return SymbolType.MemoryLocType(baseType);
			}
			case NullableSymbolType nullType:
			{
				SymbolType baseType = GetNoAttribReplacedArrType(nullType.BaseType, arrays, fixedSizeArrays, compUnit, instances);
				return SymbolType.NullableType(baseType);
			}
			case PointerSymbolType ptrType:
			{
				SymbolType baseType = GetNoAttribReplacedArrType(ptrType.BaseType, arrays, fixedSizeArrays, compUnit, instances);
				return SymbolType.PointerType(baseType);
			}
			case ReferenceSymbolType refType:
			{
				SymbolType baseType = GetNoAttribReplacedArrType(refType.BaseType, arrays, fixedSizeArrays, compUnit, instances);
				return SymbolType.ReferenceType(baseType);
			}
			case TupleSymbolType tupleType:
			{
				List<SymbolType> subTypes = new List<SymbolType>();
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					SymbolType tmp = GetNoAttribReplacedArrType(subType, arrays, fixedSizeArrays, compUnit, instances);
					subTypes.Add(tmp);
				}

				return SymbolType.TupleType(subTypes);
			}
			case VariadicSymbolType variadicType:
			{
				SymbolType baseType = GetNoAttribReplacedArrType(variadicType.BaseType, arrays, fixedSizeArrays, compUnit, instances);
				return SymbolType.VariadicType(baseType);
			}
			}
		}


	}
}
