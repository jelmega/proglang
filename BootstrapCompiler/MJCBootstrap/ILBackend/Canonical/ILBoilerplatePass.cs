﻿using System.Collections.Generic;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Canonical
{
	// Generate the required boilerplate
	class ILBoilerplatePass : ILCanonicalizePass
	{
		public override void ProcessModule(ILCompUnit compUnit)
		{
			bool hasGlobals = GenGlobals(compUnit);

			// If a module does not have a '_main' function, skip '_start'
			if (compUnit.GetFunction("_main") == null)
				return;

			GenUUStart(compUnit, hasGlobals);

			//base.ProcessModule(compUnit);
		}

		bool GenGlobals(ILCompUnit compUnit)
		{
			// Are there globals that need to be initialized?
			bool hasGlobals = false;
			foreach (KeyValuePair<string, ILGlobal> ilGlobal in compUnit.Globals)
			{
				if ((ilGlobal.Value.Attributes.GlobalFlags & ILGlobalFlags.CompileConst) == 0)
				{
					hasGlobals = true;
					break;
				}
			}

			// When there are, pregen the _init_globals and _deinit_globals functions
			if (hasGlobals)
			{
				ILAttributes attributes = new ILAttributes(ILLinkage.Hidden);
				FunctionSymbolType funcType = SymbolType.FunctionType(null, null, null);

				// Create initializer for globals
				ScopeVariable iden = new ScopeVariable(new NameIdentifier("_init_globals"));
				ILFunction initFunc = new ILFunction(iden, "_init_globals", null, funcType, attributes, null, false);
				ILBasicBlock bb = new ILBasicBlock("entry");
				initFunc.Blocks.Add(bb);
				compUnit.AddFunction(initFunc);

				ILContextGen.ProcessFunction(initFunc, compUnit);
				ILCallGraphGen.ProcessFunction(initFunc);

				// Create de-initializer for globals
				iden = new ScopeVariable(new NameIdentifier("_deinit_globals"));
				ILFunction deinitFunc = new ILFunction(iden, "_deinit_globals", null, funcType, attributes, null, false);
				bb = new ILBasicBlock("entry");
				deinitFunc.Blocks.Add(bb);
				compUnit.AddFunction(deinitFunc);

				ILContextGen.ProcessFunction(deinitFunc, compUnit);
				ILCallGraphGen.ProcessFunction(deinitFunc);
			}

			return hasGlobals;
		}

		void GenUUStart(ILCompUnit compUnit, bool hasGlobals)
		{
			ILAttributes attributes = new ILAttributes(ILLinkage.Public);

			// Generate __start
			ScopeVariable iden = new ScopeVariable(new NameIdentifier("__start"));
			FunctionSymbolType funcType = SymbolType.FunctionType(null, null, null);
			ILFunction startProc = new ILFunction(iden, "_start", null, funcType, attributes, null, false);

			ILBasicBlock bb = new ILBasicBlock("entry");
			startProc.Blocks.Add(bb);

			if (hasGlobals)
			{
				ILOperand initGlobalOp = new ILOperand("initGlobalFunc", funcType);
				bb.Instructions.Add(new ILFunctionRefInstruction(initGlobalOp, "_init_globals", funcType, null));
				bb.Instructions.Add(new ILCallInstruction(null, initGlobalOp, null, null));
			}

			ILOperand mainOp = new ILOperand("mainFunc", funcType);
			bb.Instructions.Add(new ILFunctionRefInstruction(mainOp, "_main", funcType, null));
			bb.Instructions.Add(new ILCallInstruction(null, mainOp, null, null));

			if (hasGlobals)
			{
				ILOperand deinitGlobalOp = new ILOperand("deinitGlobalFunc", funcType);
				bb.Instructions.Add(new ILFunctionRefInstruction(deinitGlobalOp, "_deinit_globals", funcType, null));
				bb.Instructions.Add(new ILCallInstruction(null, deinitGlobalOp, null, null));
			}

			compUnit.AddFunction(startProc);

			ILContextGen.ProcessFunction(startProc, compUnit);
			ILCallGraphGen.ProcessFunction(startProc);
		}
	}
}
