﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.Canonical
{
	class ILStaticDispatchPass : ILCanonicalizePass
	{
		public override void ProcessModule(ILCompUnit compUnit)
		{
			base.ProcessModule(compUnit);
		}

		public override void ProcessFunction(ILFunction function)
		{
			base.ProcessFunction(function);
		}

		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			CompilerContext compileContext = funcContext.Parent.CompilerContext;
			for (var i = 0; i < basicBlock.Instructions.Count; i++)
			{
				ILInstruction instruction = basicBlock.Instructions[i];
				switch (instruction.InstructionType)
				{
				case ILInstructionType.MethodRef:
				{
					ILMethodRefInstruction instr = instruction as ILMethodRefInstruction;

					SymbolType recType = instr.Type.ReceiverType;
					if (recType is ReferenceSymbolType refType)
						recType = refType.BaseType;

					TemplateInstanceSymbolType instType = recType as TemplateInstanceSymbolType;
					VTable table = compileContext.GetVTable(instType.Iden);
					VTableMethod method = table.GetMethod(instr.MethodRef.Element, instr.Type);

					ILFunctionRefInstruction funcRef = new ILFunctionRefInstruction(instr.RetOperand, method.MangledFunc, method.Type, null);
					basicBlock.Instructions[i] = funcRef;

					break;
				}
				}
			}
		}

		public override void ProcessVTable(VTable vTable)
		{
			base.ProcessVTable(vTable);
		}
	}
}
