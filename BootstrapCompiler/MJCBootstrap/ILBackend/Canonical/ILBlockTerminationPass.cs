﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.Canonical
{
	class ILBlockTerminationPass : ILCanonicalizePass
	{
		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext context)
		{
			if (!basicBlock.IsTerminal())
			{
				basicBlock.Instructions.Add(new ILReturnInstruction(null, null));
			}
		}
	}
}
