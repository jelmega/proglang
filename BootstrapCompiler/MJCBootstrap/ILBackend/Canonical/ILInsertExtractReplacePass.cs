﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.Canonical
{
	class ILInsertExtractReplacePass : ILCanonicalizePass
	{
		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			//base.ProcessBasicBlock(basicBlock, funcContext);

			List<ILInstruction> instructions = basicBlock.Instructions;
			for (var i = 0; i < instructions.Count; i++)
			{
				ILInstruction instr = instructions[i];
				switch (instr.InstructionType)
				{
				case ILInstructionType.StructInsert:
				{
					ILStructInsertInstruction structInsert = instr as ILStructInsertInstruction;
					TextSpan span = instr.Span;

					string iden = structInsert.Struct.Iden + '_' +
					              structInsert.ElemRef.Element + '_' +
					              structInsert.Value.Iden + "_addr";

					SymbolType retType = SymbolType.MemoryLocType(structInsert.Value.Type);
					ILOperand addressOp = new ILOperand(iden, retType);
					instructions[i] = new ILStructElemAddrInstruction(addressOp, structInsert.Struct, structInsert.ElemRef, span);

					++i;
					instructions.Insert(i, new ILAssignInstruction(structInsert.Value, addressOp, span));

					// Update context
					ILVarContext addrContext = new ILVarContext(addressOp, ILVarType.Stack);
					addrContext.DependentOn.Add(structInsert.Value.Iden);
					addrContext.UseCount = 1;
					funcContext.AddVarContext(addrContext);

					break;
				}
				case ILInstructionType.StructExtract:
				{
					ILStructExtractInstruction structExtract = instr as ILStructExtractInstruction;
					TextSpan span = instr.Span;

					string addrIden = structExtract.RetOperand.Iden + '_' +
									  structExtract.Struct.Iden + '_' +
									  structExtract.ElemRef.Element + "_addr";

					SymbolType retType = SymbolType.MemoryLocType(structExtract.RetOperand.Type);
					ILOperand addressOp = new ILOperand(addrIden, retType);
					instructions[i] = new ILStructElemAddrInstruction(addressOp, structExtract.Struct, structExtract.ElemRef, span);

					++i;
					instructions.Insert(i, new ILLoadInstruction(structExtract.RetOperand, addressOp, span));

					// Update context
					ILVarContext addrContext = new ILVarContext(addressOp, ILVarType.Stack);
					addrContext.UseCount = 1;
					funcContext.AddVarContext(addrContext);

					ILVarContext retContext = funcContext.GetVarContext(structExtract.RetOperand.Iden);
					retContext.DependentOn.Remove(structExtract.Struct.Iden); // Remove old dependency
					retContext.DependentOn.Add(addrIden); // Add the new dependency

					break;
				}

				case ILInstructionType.UnionInsert:
				{
					ILUnionInsertInstruction unionInsert = instr as ILUnionInsertInstruction;
					TextSpan span = instr.Span;

					string iden = unionInsert.Union.Iden + '_' +
					              unionInsert.ElemRef.Element + '_' +
					              unionInsert.Value.Iden + "_addr";

					SymbolType retType = SymbolType.MemoryLocType(unionInsert.Value.Type);
					ILOperand addressOp = new ILOperand(iden, retType);
					instructions[i] = new ILUnionElemAddrInstruction(addressOp, unionInsert.Union, unionInsert.ElemRef, span);

					++i;
					instructions.Insert(i, new ILAssignInstruction(unionInsert.Value, addressOp, span));

					// Update context
					ILVarContext addrContext = new ILVarContext(addressOp, ILVarType.Stack);
					addrContext.DependentOn.Add(unionInsert.Value.Iden);
					addrContext.UseCount = 1;
					funcContext.AddVarContext(addrContext);

					break;
				}
				case ILInstructionType.UnionExtract:
				{
					ILUnionExtractInstruction unionExtract = instr as ILUnionExtractInstruction;
					TextSpan span = instr.Span;

					string addrIden = unionExtract.RetOperand.Iden + '_' +
									  unionExtract.Union.Iden + '_' +
									  unionExtract.ElemRef.Element + "_addr";

					SymbolType retType = SymbolType.MemoryLocType(unionExtract.RetOperand.Type);
					ILOperand addressOp = new ILOperand(addrIden, retType);
					instructions[i] = new ILUnionElemAddrInstruction(addressOp, unionExtract.Union, unionExtract.ElemRef, span);

					++i;
					instructions.Insert(i, new ILLoadInstruction(unionExtract.RetOperand, addressOp, span));

					// Update context
					ILVarContext addrContext = new ILVarContext(addressOp, ILVarType.Stack);
					addrContext.UseCount = 1;
					funcContext.AddVarContext(addrContext);

					ILVarContext retContext = funcContext.GetVarContext(unionExtract.RetOperand.Iden);
					retContext.DependentOn.Remove(unionExtract.Union.Iden); // Remove old dependency
					retContext.DependentOn.Add(addrIden); // Add the new dependency

					break;
				}
				}
			}
		}
	}
}
