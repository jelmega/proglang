﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.Canonical
{
	class ILBitcastPass : ILCanonicalizePass
	{
		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			foreach (ILInstruction instruction in basicBlock.Instructions)
			{
				if (instruction.InstructionType == ILInstructionType.Builtin)
				{
					ILBuiltinInstruction builtin = instruction as ILBuiltinInstruction;

					if (builtin.Builtin == "bitcast")
					{
						if (builtin.Operands[0].Type is BuiltinSymbolType && builtin.RetOperand.Type is PointerSymbolType)
						{
							builtin.Builtin = "itoptr";
						}
						else if (builtin.Operands[0].Type is PointerSymbolType && builtin.RetOperand.Type is BuiltinSymbolType)
						{
							builtin.Builtin = "ptrtoi";
						}
					}

				}
			}
		}
	}
}
