﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Canonical
{
	// Remove cast that return the same value as it's argument
	// i.e.: Remove casts with (T) -> (T)
	class ILRemoveSelfCastPass : ILCanonicalizePass
	{
		public override void ProcessFunction(ILFunction function)
		{
			HashSet<ILOperand> selfCasts = new HashSet<ILOperand>();
			for (var blockIdx = 0; blockIdx < function.Blocks.Count; blockIdx++)
			{
				ILBasicBlock basicBlock = function.Blocks[blockIdx];
				for (var i = 0; i < basicBlock.Instructions.Count; i++)
				{
					ILInstruction instruction = basicBlock.Instructions[i];
					switch (instruction.InstructionType)
					{
					case ILInstructionType.OperatorRef:
					{
						ILOperatorRefInstruction opRef = instruction as ILOperatorRefInstruction;

						if (opRef.Loc == ILOpLoc.Cast &&
						    opRef.Op == ILOpType.Static)
						{
							FunctionSymbolType funcType = opRef.Type;
							if (funcType.ParamTypes[0] == funcType.ReturnType)
							{
								selfCasts.Add(opRef.RetOperand);

								basicBlock.Instructions.RemoveAt(i);
								--i;
							}
						}

						break;
					}
					case ILInstructionType.Call:
					{
						ILCallInstruction call = instruction as ILCallInstruction;

						if (selfCasts.Contains(call.Func))
						{
							ILHelpers.ReplaceOperand(function, call.RetOperand, call.Operands[0], blockIdx, i);
							basicBlock.Instructions.RemoveAt(i);
							--i;
						}

						break;
					}
					}
				}
			}
		}
	}
}
