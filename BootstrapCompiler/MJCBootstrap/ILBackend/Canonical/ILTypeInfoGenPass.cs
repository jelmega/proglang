﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Canonical
{
	class ILTypeInfoGenPass : ILCanonicalizePass
	{
		public override void ProcessModule(ILCompUnit compUnit)
		{
			CompilerContext context = compUnit.Context.CompilerContext;

			// Try to gen for all aggregates
			foreach (KeyValuePair<string, ILAggregate> pair in compUnit.AggregatesMangled)
			{
				// Skip when the aggregate is a template definition
				if (pair.Value.Identifier.Name is TemplateDefinitionIdentifier)
					continue;

				// Skip when type info is already created

				if (context.GetTypeInfo(pair.Key) != null)
					continue;


				ILTypeInfoHelpers.GetMinimalAggrTypeInfo(pair.Value, compUnit, context);
			}

			foreach (KeyValuePair<string, ILEnum> pair in compUnit.Enums)
			{
				// Skip when the aggregate is a template definition
				if (pair.Value.Identifier.Name is TemplateDefinitionIdentifier)
					continue;

				// Skip when type info is already created
				ILEnum ilEnum = pair.Value;
				string mangle = ilEnum.MangledIdentifier;
				mangle = 'E' + mangle.Substring(2, mangle.Length - 4);

				if (context.GetTypeInfo(mangle) != null)
					continue;


				ILTypeInfoHelpers.GetMinimalEnumTypeInfo(pair.Value, compUnit, context);
			}
		}

		
	}
}
