﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Canonical
{
	class ILNullCastPass : ILCanonicalizePass
	{
		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			for (var i = 0; i < basicBlock.Instructions.Count; i++)
			{
				ILInstruction instruction = basicBlock.Instructions[i];

				HashSet<ILOperand> nullCastOps = new HashSet<ILOperand>();

				switch (instruction.InstructionType)
				{
				case ILInstructionType.OperatorRef:
				{
					ILOperatorRefInstruction opRef = instruction as ILOperatorRefInstruction;

					if (opRef.Op == ILOpType.Bit)
					{
						SymbolType opType = opRef.Type.ParamTypes[0];
						if (opType is BuiltinSymbolType builtin && builtin.Builtin == BuiltinTypes.Null)
						{
							nullCastOps.Add(opRef.RetOperand);
							basicBlock.Instructions.RemoveAt(i);
							--i;

							funcContext.RemoveVarContext(opRef.RetOperand.Iden);
						}
					}

					break;
				}
				case ILInstructionType.Call:
				{
					ILCallInstruction call = instruction as ILCallInstruction;

					if (nullCastOps.Contains(call.Func))
					{
						basicBlock.Instructions[i] = new ILConvertTypeInstruction(call.RetOperand, call.Operands[0], call.Span);
					}

					break;
				}
				}
			}
		}
	}
}
