﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Canonical
{
	class ILTypeAttribRemovePass : ILCanonicalizePass
	{
		public override void ProcessFunction(ILFunction function)
		{
			if (function.Operands != null)
			{
				foreach (ILOperand operand in function.Operands)
				{
					operand.Type = ILHelpers.GetTypeWithoutAttribs(operand.Type);
				}
			}

			function.FuncType = ILHelpers.GetTypeWithoutAttribs(function.FuncType) as FunctionSymbolType;

			base.ProcessFunction(function);
		}

		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			foreach (ILInstruction instruction in basicBlock.Instructions)
			{
				switch (instruction.InstructionType)
				{
				case ILInstructionType.Unknown: break;
				case ILInstructionType.IntLiteral: break;
				case ILInstructionType.FloatLiteral: break;
				case ILInstructionType.StringLiteral: break;
				case ILInstructionType.AllocStack:
				{
					ILAllocStackInstruction alloc = instruction as ILAllocStackInstruction;
					alloc.Type = ILHelpers.GetTypeWithoutAttribs(alloc.Type);
					alloc.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(alloc.RetOperand.Type);
					break;
				}
				case ILInstructionType.DeallocStack: break;
				case ILInstructionType.Assign: break;
				case ILInstructionType.Store: break;
				case ILInstructionType.Load:
				{
					ILLoadInstruction load = instruction as ILLoadInstruction;
					load.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(load.RetOperand.Type);
					break;
				}
				case ILInstructionType.FunctionRef:
				{
					ILFunctionRefInstruction funcRef = instruction as ILFunctionRefInstruction;
					funcRef.Type = ILHelpers.GetTypeWithoutAttribs(funcRef.Type) as FunctionSymbolType;
					funcRef.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(funcRef.RetOperand.Type);
					break;
				}
				case ILInstructionType.MethodRef:
				{
					ILMethodRefInstruction methodRef = instruction as ILMethodRefInstruction;
					methodRef.Type = ILHelpers.GetTypeWithoutAttribs(methodRef.Type) as FunctionSymbolType;
					methodRef.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(methodRef.RetOperand.Type);
					break;
				}
				case ILInstructionType.OperatorRef:
				{
					ILOperatorRefInstruction opRef = instruction as ILOperatorRefInstruction;
					opRef.Type = ILHelpers.GetTypeWithoutAttribs(opRef.Type) as FunctionSymbolType;
					opRef.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(opRef.RetOperand.Type);
					break;
				}
				case ILInstructionType.Call:
				{
					ILCallInstruction call = instruction as ILCallInstruction;
					if (call.RetOperand != null)
						call.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(call.RetOperand.Type);
					break;
				}
				case ILInstructionType.Builtin:
				{
					ILBuiltinInstruction builtin = instruction as ILBuiltinInstruction;
					if (builtin.RetOperand != null)
						builtin.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(builtin.RetOperand.Type);
					break;
				}
				case ILInstructionType.CompilerIntrin: break;
				case ILInstructionType.TupleUnpack:
				{
					ILTupleUnpackInstruction unpack = instruction as ILTupleUnpackInstruction;
					foreach (ILOperand operand in unpack.RetOperands)
					{
						operand.Type = ILHelpers.GetTypeWithoutAttribs(operand.Type);
					}
					break;
				}
				case ILInstructionType.TuplePack:
				{
					ILTuplePackInstruction pack = instruction as ILTuplePackInstruction;
					pack.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(pack.RetOperand.Type);
					break;
				}
				case ILInstructionType.TupleInsert: break;
				case ILInstructionType.TupleExtract:
				{
					ILTupleExtractInstruction extract = instruction as ILTupleExtractInstruction;
					extract.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(extract.RetOperand.Type);
					break;
				}
				case ILInstructionType.TupleElemAddr:
				{
					ILTupleElemAddrInstruction elemAddr = instruction as ILTupleElemAddrInstruction;
					elemAddr.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(elemAddr.RetOperand.Type);
					break;
				}
				case ILInstructionType.StructExtract:
				{
					ILStructExtractInstruction extract = instruction as ILStructExtractInstruction;
					extract.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(extract.RetOperand.Type);
					break;
				}
				case ILInstructionType.StructInsert: break;
				case ILInstructionType.StructElemAddr:
				{
					ILStructElemAddrInstruction elemAddr = instruction as ILStructElemAddrInstruction;
					elemAddr.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(elemAddr.RetOperand.Type);
					break;
				}
				case ILInstructionType.UnionExtract:
				{
					ILUnionExtractInstruction extract = instruction as ILUnionExtractInstruction;
					extract.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(extract.RetOperand.Type);
					break;
				}
				case ILInstructionType.UnionInsert: break;
				case ILInstructionType.UnionElemAddr:
				{
					ILUnionElemAddrInstruction elemAddr = instruction as ILUnionElemAddrInstruction;
					elemAddr.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(elemAddr.RetOperand.Type);
					break;
				}
				case ILInstructionType.Destruct: break;
				case ILInstructionType.EnumValue:
				{
					ILEnumValueInstruction enumValue = instruction as ILEnumValueInstruction;
					enumValue.Type = ILHelpers.GetTypeWithoutAttribs(enumValue.Type);
					enumValue.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(enumValue.RetOperand.Type);
					break;
				}
				case ILInstructionType.EnumGetValue:
				{
					ILEnumGetValueInstruction enumGetVal = instruction as ILEnumGetValueInstruction;
					enumGetVal.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(enumGetVal.RetOperand.Type);
					break;
				}
				case ILInstructionType.EnumGetMemberValue: break;
				case ILInstructionType.GlobalAddr:
				{
					ILGlobalAddrInstruction globAddr = instruction as ILGlobalAddrInstruction;
					globAddr.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(globAddr.RetOperand.Type);
					break;
				}
				case ILInstructionType.GlobalValue:
				{
					ILGlobalValueInstruction globVal = instruction as ILGlobalValueInstruction;
					globVal.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(globVal.RetOperand.Type);
					break;
				}
				case ILInstructionType.TemplateGetValue: break;
				case ILInstructionType.ConvType:
				{
					ILConvertTypeInstruction convType = instruction as ILConvertTypeInstruction;
					convType.RetOperand.Type = ILHelpers.GetTypeWithoutAttribs(convType.RetOperand.Type);
					break;
				}
				case ILInstructionType.GetVTable:
				{
					ILGetVTableInstruction getVTable = instruction as ILGetVTableInstruction;
					getVTable.Type = ILHelpers.GetTypeWithoutAttribs(getVTable.Type);
					for (var i = 0; i < getVTable.InterfaceTypes.Count; i++)
					{
						getVTable.InterfaceTypes[i] = ILHelpers.GetTypeWithoutAttribs(getVTable.InterfaceTypes[i]);
					}

					break;
				}
				case ILInstructionType.Return: break;
				case ILInstructionType.Branch: break;
				case ILInstructionType.CondBranch: break;
				case ILInstructionType.SwitchEnum: break;
				}
			}
		}
	}
}
