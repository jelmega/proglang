﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Canonical
{
	class ILGlobalInitPass : ILCanonicalizePass
	{
		public override void ProcessModule(ILCompUnit compUnit)
		{
			List<ILGlobal> collectedGlobals = new List<ILGlobal>();
			foreach (KeyValuePair<string, ILGlobal> pair in compUnit.Globals)
			{
				if ((pair.Value.Attributes.GlobalFlags & ILGlobalFlags.CompileConst) == 0)
					collectedGlobals.Add(pair.Value);
			}

			if (collectedGlobals.Count == 0)
				return;

			// Correctly order initializations
			List<ILGlobal> orderedGlobals = new List<ILGlobal>();

			// TODO
			orderedGlobals = collectedGlobals;

			ILFunction initFunc = compUnit.GetFunction("_init_globals");
			ILBasicBlock bb = initFunc.Blocks[0];
			foreach (ILGlobal ilGlobal in orderedGlobals)
			{
				ILOperand globalOp = new ILOperand(ilGlobal.MangledIdentifier, ilGlobal.Type);
				ILOperand valueOp = new ILOperand(ilGlobal.MangledIdentifier + ".value", ilGlobal.Type);

				FunctionSymbolType initType = new FunctionSymbolType(null, null, ilGlobal.Type, TypeAttributes.None);
				ILOperand initOp = new ILOperand(ilGlobal.MangledIdentifier + ".init", initType);

				bb.Instructions.Add(new ILFunctionRefInstruction(initOp, ilGlobal.Initializer, initType, null));
				bb.Instructions.Add(new ILCallInstruction(valueOp, initOp, null, null));
				bb.Instructions.Add(new ILGlobalAddrInstruction(globalOp, ilGlobal.MangledIdentifier, null));
				bb.Instructions.Add(new ILStoreInstruction(valueOp, globalOp, null));
			}
		}
	}
}
