﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Canonical
{
	class ILArrayReplacementPass : ILCanonicalizePass
	{
		public override void ProcessModule(ILCompUnit compUnit)
		{
			_compUnit = compUnit;

			// Process aggregates
			foreach (KeyValuePair<ScopeVariable, ILAggregate> pair in compUnit.Aggregates)
			{
				foreach (ILAggregateVariable variable in pair.Value.Variables)
				{
					variable.Type = ILHelpers.GetArrayReplacedType(variable.Type, _compUnit);
				}
			}


			base.ProcessModule(compUnit);
		}

		public override void ProcessFunction(ILFunction function)
		{
			function.FuncType = ILHelpers.GetArrayReplacedType(function.FuncType, _compUnit) as FunctionSymbolType;

			if (function.Operands != null)
			{
				foreach (ILOperand operand in function.Operands)
				{
					operand.Type = ILHelpers.GetArrayReplacedType(operand.Type, _compUnit);
				}
			}

			base.ProcessFunction(function);
		}

		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			foreach (ILInstruction instruction in basicBlock.Instructions)
			{
				switch (instruction.InstructionType)
				{
				case ILInstructionType.AllocStack:
				{
					ILAllocStackInstruction alloc = instruction as ILAllocStackInstruction;
					alloc.Type = ILHelpers.GetArrayReplacedType(alloc.Type, _compUnit);
					alloc.RetOperand.Type = ILHelpers.GetArrayReplacedType(alloc.RetOperand.Type, _compUnit);

					break;
				}
				case ILInstructionType.Load:
				{
					ILLoadInstruction load = instruction as ILLoadInstruction;
					load.RetOperand.Type = ILHelpers.GetArrayReplacedType(load.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.FunctionRef:
				{
					ILFunctionRefInstruction funcRef = instruction as ILFunctionRefInstruction;
					funcRef.Type = ILHelpers.GetArrayReplacedType(funcRef.Type, _compUnit) as FunctionSymbolType;
					funcRef.RetOperand.Type = ILHelpers.GetArrayReplacedType(funcRef.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.MethodRef:
				{
					ILMethodRefInstruction methodRef = instruction as ILMethodRefInstruction;
					methodRef.Type = ILHelpers.GetArrayReplacedType(methodRef.Type, _compUnit) as FunctionSymbolType;
					methodRef.RetOperand.Type = ILHelpers.GetArrayReplacedType(methodRef.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.OperatorRef:
				{
					ILOperatorRefInstruction opRef = instruction as ILOperatorRefInstruction;
					opRef.Type = ILHelpers.GetArrayReplacedType(opRef.Type, _compUnit) as FunctionSymbolType;
					opRef.RetOperand.Type = ILHelpers.GetArrayReplacedType(opRef.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.Call:
				{
					ILCallInstruction call = instruction as ILCallInstruction;
					if (call.RetOperand != null)
						call.RetOperand.Type = ILHelpers.GetArrayReplacedType(call.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.Builtin:
				{
					ILBuiltinInstruction builtin = instruction as ILBuiltinInstruction;
					if (builtin.RetOperand != null)
						builtin.RetOperand.Type = ILHelpers.GetArrayReplacedType(builtin.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.TupleUnpack:
				{
					ILTupleUnpackInstruction unpack = instruction as ILTupleUnpackInstruction;
					foreach (ILOperand retOp in unpack.RetOperands)
					{
						retOp.Type = ILHelpers.GetArrayReplacedType(retOp.Type, _compUnit);
					}
					break;
				}
				case ILInstructionType.TuplePack:
				{
					ILTuplePackInstruction pack = instruction as ILTuplePackInstruction;
					pack.RetOperand.Type = ILHelpers.GetArrayReplacedType(pack.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.TupleExtract:
				{
					ILTupleExtractInstruction extract = instruction as ILTupleExtractInstruction;
					extract.RetOperand.Type = ILHelpers.GetArrayReplacedType(extract.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.TupleElemAddr:
				{
					ILTupleElemAddrInstruction elemAddr = instruction as ILTupleElemAddrInstruction;
					elemAddr.RetOperand.Type = ILHelpers.GetArrayReplacedType(elemAddr.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.StructExtract:
				{
					ILStructExtractInstruction extract = instruction as ILStructExtractInstruction;
					extract.RetOperand.Type = ILHelpers.GetArrayReplacedType(extract.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.StructElemAddr:
				{
					ILStructElemAddrInstruction elemAddr = instruction as ILStructElemAddrInstruction;
					elemAddr.RetOperand.Type = ILHelpers.GetArrayReplacedType(elemAddr.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.UnionExtract:
				{
					ILUnionExtractInstruction extract = instruction as ILUnionExtractInstruction;
					extract.RetOperand.Type = ILHelpers.GetArrayReplacedType(extract.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.UnionElemAddr:
				{
					ILUnionElemAddrInstruction elemAddr = instruction as ILUnionElemAddrInstruction;
					elemAddr.RetOperand.Type = ILHelpers.GetArrayReplacedType(elemAddr.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.EnumValue:
				{
					ILEnumValueInstruction enumVal = instruction as ILEnumValueInstruction;
					enumVal.RetOperand.Type = ILHelpers.GetArrayReplacedType(enumVal.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.EnumGetValue:
				{
					ILEnumGetValueInstruction enumGetVal = instruction as ILEnumGetValueInstruction;
					enumGetVal.Operand.Type = ILHelpers.GetArrayReplacedType(enumGetVal.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.EnumGetMemberValue:
				{
					ILEnumGetMemberValueInstruction enumGetMemVal = instruction as ILEnumGetMemberValueInstruction;
					enumGetMemVal.RetOperand.Type = ILHelpers.GetArrayReplacedType(enumGetMemVal.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.GlobalAddr:
				{
					ILGlobalAddrInstruction globAddr = instruction as ILGlobalAddrInstruction;
					globAddr.RetOperand.Type = ILHelpers.GetArrayReplacedType(globAddr.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.GlobalValue:
				{
					ILGlobalValueInstruction globVal = instruction as ILGlobalValueInstruction;
					globVal.RetOperand.Type = ILHelpers.GetArrayReplacedType(globVal.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.TemplateGetValue:
				{
					ILTemplateGetValueInstruction templateVal = instruction as ILTemplateGetValueInstruction;
					templateVal.RetOperand.Type = ILHelpers.GetArrayReplacedType(templateVal.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.ConvType:
				{
					ILConvertTypeInstruction convType = instruction as ILConvertTypeInstruction;
					convType.RetOperand.Type = ILHelpers.GetArrayReplacedType(convType.RetOperand.Type, _compUnit);
					break;
				}
				case ILInstructionType.GetVTable: break;
				}
			}
		}


		
	}
}
