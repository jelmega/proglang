﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend
{
	public class ILBuilder
	{
		private int _blockCounter;
		private int _varCounter;

		private ILCompUnit _compUnit;
		private ILFunction _curFunc;
		private ILBasicBlock _curBasicBlock;
		
		public ILCompUnit End()
		{
			ILCompUnit tmp = _compUnit;
			_compUnit = null;
			return tmp;
		}

		public void BeginRaw()
		{
			_compUnit = new ILCompUnit();
			_compUnit.Stage = ILStage.Raw;
		}

		public void BeginLoaded()
		{
			_compUnit       = new ILCompUnit();
			_compUnit.Stage = ILStage.Opt;
		}

		public void BuildModuleDirective(string package, string module)
		{
			string modName;

			if (package == null)
				modName = $"{module}";
			else
			modName = $"{package}.{module}";

			_compUnit.ILModule = modName;
		}

		public void BuildImport(string module)
		{
			_compUnit.ImportNames.Add(module);
		}

		public (List<ILOperand>, SymbolType) BeginFunction(ScopeVariable iden, string mangled, FunctionSymbolType funcType, ILAttributes ilAttribs, List<ILTemplateParam> templateParams, bool isUninstantiated, TextSpan span)
		{
			// TODO: Use span
			_blockCounter = 0;
			_varCounter = 0;
			
			if (funcType == null)
				return (null, null);
			
			List<ILOperand> paramVars = null;

			if (funcType.ReceiverType != null)
			{
				paramVars = new List<ILOperand>();
				string var = $"{_varCounter}";
				ILOperand operand = new ILOperand(var, funcType.ReceiverType);
				paramVars.Add(operand);
				++_varCounter;
			}

			if (funcType.ParamTypes != null && funcType.ParamTypes.Count > 0)
			{
				if (paramVars == null)
					paramVars = new List<ILOperand>();

				foreach (SymbolType parameterType in funcType.ParamTypes)
				{
					string var = $"{_varCounter}";
					ILOperand operand = new ILOperand(var, parameterType);
					paramVars.Add(operand);
					++_varCounter;
				}
			}

			// TODO: Multiple blocks
			if (_compUnit != null)
			{
				_curFunc = new ILFunction (iden, mangled, paramVars, funcType, ilAttribs, templateParams, isUninstantiated);

				_compUnit.AddFunction(_curFunc);
			}

			return (paramVars, funcType.ReturnType);
		}

		public void EndFunction()
		{
			// Deallocation happens in canonicalizer step

			_curBasicBlock = null;
			_curFunc = null;
		}

		public ILBasicBlock CreateBasicBlock(string name = null)
		{
			if (name == null)
			{
				name = $"bb{_blockCounter}";
				++_blockCounter;
			}

			_curBasicBlock = new ILBasicBlock(name);
			
			_curFunc.Blocks.Add(_curBasicBlock);

			return _curBasicBlock;
		}

		public ILOperand BuildStackVariable(SymbolType type, string iden, TextSpan span)
		{
			string var = iden;

			if (var == null)
			{
				var = $"{_varCounter}";
				++_varCounter;
			}

			ILOperand ret = new ILOperand(var, SymbolType.MemoryLocType(type));

			_curBasicBlock.Instructions.Add(new ILAllocStackInstruction(ret, type, span));

			return ret;
		}

		public ILOperand BuildIntLiteral(long literal, SymbolType type, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, type);

			if (_curBasicBlock != null)
				_curBasicBlock.Instructions.Add(new ILIntLiteralInstruction(ret, literal, span));

			return ret;
		}

		public ILOperand BuildFloatLiteral(double literal, SymbolType type, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			BuiltinSymbolType builtinType = type as BuiltinSymbolType;
			ILOperand ret;
			long lval;
			if (builtinType?.Builtin == BuiltinTypes.F32)
			{
				lval = BitConverter.SingleToInt32Bits((float)literal);
				ret = new ILOperand(var, SymbolType.BuiltinType(BuiltinTypes.F32));
			}
			else
			{
				lval = BitConverter.DoubleToInt64Bits(literal);
				ret = new ILOperand(var, SymbolType.BuiltinType(BuiltinTypes.F64));
			}

			if (_curBasicBlock != null)
				_curBasicBlock.Instructions.Add(new ILFloatLiteralInstruction(ret, lval, span));

			return ret;
		}

		public ILOperand BuildStringLiteral(string literal, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, SymbolType.BuiltinType(BuiltinTypes.StringLiteral));

			if (_curBasicBlock != null)
				_curBasicBlock.Instructions.Add(new ILStringLiteralInstruction(ret, literal, span));

			return ret;
		}

		public void BuildAssign(ILOperand src, ILOperand dst, TextSpan span)
		{
			_curBasicBlock.Instructions.Add(new ILStoreInstruction(src, dst, span));
		}

		public ILOperand BuildLoad(ILOperand src, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			SymbolType retType = (src.Type as MemoryLocSymbolType)?.BaseType;
			ILOperand ret = new ILOperand(var, retType);

			if (_curBasicBlock != null)
				_curBasicBlock.Instructions.Add(new ILLoadInstruction(ret, src, span));

			return ret;
		}

		public ILOperand BuildOperatorRef(ILOpLoc opLoc, ILOpType op, List<SymbolType> paramTypes, SymbolType resType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, SymbolType.FunctionType(null, paramTypes, resType));

			if (_curBasicBlock != null)
			{
				FunctionSymbolType funcType = SymbolType.FunctionType(null, paramTypes, resType);

				_curBasicBlock.Instructions.Add(new ILOperatorRefInstruction(ret, opLoc, op, funcType, span));
			}

			return ret;
		}

		public ILOperand BuildFunctionRef(string mangledName, FunctionSymbolType funcType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;
			
			ILOperand ret = new ILOperand(var, funcType);

			if (_curBasicBlock != null)
			{
				_curBasicBlock.Instructions.Add(new ILFunctionRefInstruction(ret, mangledName, funcType, span));
			}

			return ret;
		}

		public ILOperand BuildMethodRef(ILReference methodRef, FunctionSymbolType funcType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;
			
			ILOperand ret = new ILOperand(var, funcType);

			if (_curBasicBlock != null)
			{
				_curBasicBlock.Instructions.Add(new ILMethodRefInstruction(ret, methodRef, funcType, span));
			}

			return ret;
		}

		public ILOperand BuildCall(ILOperand func, List<ILOperand> operands, SymbolType resType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = null;
			if (resType != null)
				ret = new ILOperand(var, resType);

			_curBasicBlock.Instructions.Add(new ILCallInstruction(ret, func, operands, span));

			return ret;
		}

		public ILOperand BuildBuiltin(string builtin, List<ILOperand> operands, SymbolType resType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = null;
			if (resType != null)
				ret = new ILOperand(var, resType);

			_curBasicBlock.Instructions.Add(new ILBuiltinInstruction(ret, builtin, operands, span));

			return ret;
		}

		public ILOperand BuildCompilerIntrin(ILCompilerIntrinType intrin, SymbolType type, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			SymbolType resType = null;
			switch (intrin)
			{
			case ILCompilerIntrinType.Sizeof:
				resType = SymbolType.BuiltinType(BuiltinTypes.U64);
				break;
			case ILCompilerIntrinType.Alignof:
				resType = SymbolType.BuiltinType(BuiltinTypes.U8);
				break;
			}

			ILOperand ret = new ILOperand(var, resType);

			_curBasicBlock.Instructions.Add(new ILCompilerIntrinInstruction(ret, intrin, type, span));

			return ret;
		}

		public ILOperand BuildTuplePack(List<ILOperand> operands, TupleSymbolType tupleType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand retOperand = new ILOperand(var, tupleType);
			_curBasicBlock.Instructions.Add(new ILTuplePackInstruction(retOperand, operands, span));

			return retOperand;
		}

		public List<ILOperand> BuildTupleUnpack(ILOperand operand, TextSpan span)
		{
			List<ILOperand> retOperands = null;
			if (operand.Type is TupleSymbolType tupleType)
			{
				retOperands = new List<ILOperand>();
				
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					string var = $"{_varCounter}";
					++_varCounter;

					retOperands.Add(new ILOperand(var, subType));
				}

				_curBasicBlock.Instructions.Add(new ILTupleUnpackInstruction(retOperands, operand, span));
			}

			return retOperands;
		}

		public void BuildTupleInsert(ILOperand tuple, long index, ILOperand value, TextSpan span)
		{
			_curBasicBlock.Instructions.Add(new ILTupleInsertInstruction(tuple, index, value, span));
		}

		public ILOperand BuildTupleExtract(ILOperand tuple, long index, SymbolType elemType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, elemType);
			_curBasicBlock.Instructions.Add(new ILTupleExtractInstruction(ret, tuple, index, span));

			return ret;
		}

		public ILOperand BuildTupleElemAddr(ILOperand tuple, long index, SymbolType elemType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			SymbolType memType = SymbolType.MemoryLocType(elemType);
			ILOperand ret = new ILOperand(var, memType);
			_curBasicBlock.Instructions.Add(new ILTupleElemAddrInstruction(ret, tuple, index, span));

			return ret;
		}

		public ILOperand BuildStructExtract(ILOperand structOperand, ILReference memberRef, SymbolType memberType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, memberType);
			_curBasicBlock.Instructions.Add(new ILStructExtractInstruction(ret, structOperand, memberRef, span));

			return ret;
		}

		public void BuildStructInsert(ILOperand structOperand, ILReference memberRef, ILOperand value, TextSpan span)
		{
			_curBasicBlock.Instructions.Add(new ILStructInsertInstruction(structOperand, memberRef, value, span));
		}

		public ILOperand BuildStructElemAddr(ILOperand structOperand, ILReference memberRef, SymbolType memberType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, memberType);
			_curBasicBlock.Instructions.Add(new ILStructElemAddrInstruction(ret, structOperand, memberRef, span));

			return ret;
		}

		public ILOperand BuildTemplateGetValue(SymbolType templateType, ILReference memberRef, SymbolType memberType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, memberType);
			_curBasicBlock.Instructions.Add(new ILTemplateGetValueInstruction(ret, templateType, memberRef, span));

			return ret;
		}

		public ILOperand BuildUnionExtract(ILOperand unionOperand, ILReference memberRef, SymbolType memberType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, memberType);
			_curBasicBlock.Instructions.Add(new ILUnionExtractInstruction(ret, unionOperand, memberRef, span));

			return ret;
		}

		public void BuildUnionInsert(ILOperand unionOperand, ILReference memberRef, ILOperand value, TextSpan span)
		{
			_curBasicBlock.Instructions.Add(new ILUnionInsertInstruction(unionOperand, memberRef, value, span));
		}

		public ILOperand BuildUnionElemAddr(ILOperand unionOperand, ILReference memberRef, SymbolType memberType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, memberType);
			_curBasicBlock.Instructions.Add(new ILUnionElemAddrInstruction(ret, unionOperand, memberRef, span));

			return ret;
		}

		public ILOperand BuildEnumValue(SymbolType type, ILReference memberRef, List<ILOperand> operands, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, type);
			_curBasicBlock.Instructions.Add(new ILEnumValueInstruction(ret, type, memberRef, operands, span));

			return ret;
		}

		public ILOperand BuildEnumGetMemberValue(SymbolType type, ILReference memberRef, SymbolType retType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, retType);
			_curBasicBlock.Instructions.Add(new ILEnumGetMemberValueInstruction(ret, type, memberRef, span));

			return ret;
		}

		public ILOperand BuildGlobalAddr(string identifier, SymbolType retType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, SymbolType.MemoryLocType(retType));
			_curBasicBlock.Instructions.Add(new ILGlobalAddrInstruction(ret, identifier, span));

			return ret;
		}

		public ILOperand BuildGlobalValue(string identifier, SymbolType retType, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, retType);
			_curBasicBlock.Instructions.Add(new ILGlobalValueInstruction(ret, identifier, span));

			return ret;
		}

		public ILOperand BuildConvertType(ILOperand operand, SymbolType type, TextSpan span)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, type);
			_curBasicBlock.Instructions.Add(new ILConvertTypeInstruction(ret, operand, span));

			return ret;
		}

		public ILOperand BuildGetVTable(SymbolType type, List<SymbolType> interfaceTypes)
		{
			string var = $"{_varCounter}";
			++_varCounter;

			ILOperand ret = new ILOperand(var, SymbolType.PointerType(SymbolType.BuiltinType(BuiltinTypes.Void)));
			_curBasicBlock.Instructions.Add(new ILGetVTableInstruction(ret, type, interfaceTypes));

			return ret;
		}

		public void BuildReturn(ILOperand var, TextSpan span)
		{
			_curBasicBlock.Instructions.Add(new ILReturnInstruction(var, span));
		}

		public void BuildReturn(TextSpan span)
		{
			_curBasicBlock.Instructions.Add(new ILReturnInstruction(null, span));
		}

		public void BuildBranch(string label, TextSpan span)
		{
			_curBasicBlock.Instructions.Add(new ILBranchInstruction(label, span));
		}

		public void BuildCondBranch(ILOperand condition, string trueLabel, string falseLabel, TextSpan span)
		{
			_curBasicBlock.Instructions.Add(new ILCondBranchInstruction(condition, trueLabel, falseLabel, span));
		}

		public void BuildSwitchEnum(ILOperand enumOp, List<ILRefCase> cases, string defaultLabel, TextSpan span)
		{
			_curBasicBlock.Instructions.Add(new ILSwitchEnumInstruction(enumOp, cases, defaultLabel, span));
		}

		public void BuildSwitchValue(ILOperand enumOp, List<ILValCase> cases, string defaultLabel, TextSpan span)
		{
			_curBasicBlock.Instructions.Add(new ILSwitchValueInstruction(enumOp, cases, defaultLabel, span));
		}

		public void CreateAggregate(ILAggregateType type, ScopeVariable identifier, string mangled, Symbol symbol, ILAttributes ilAttribs, List<ILTemplateParam> templateParams, TextSpan span)
		{
			// TODO: Use span
			ILAggregate aggregate = new ILAggregate(type, identifier, mangled, symbol, ilAttribs, templateParams);
			_compUnit.AddAggregate(aggregate);
		}

		public ILOperand BuildAggregateVariable(ScopeVariable aggrIden, Identifier identifier, string mangledName, SymbolType type, TextSpan span)
		{
			// TODO: Use span
			ILAggregate aggregate = _compUnit.GetAggregate(aggrIden);
			aggregate.Variables.Add(new ILAggregateVariable(identifier, mangledName, type));
			return new ILOperand(identifier.GetSimpleName(), type);
		}

		public void AddVTable(VTable vtable)
		{
			_compUnit.AddVTable(vtable);
		}

		public void AddVTableMethod(ScopeVariable vtableIden, Identifier iden, FunctionSymbolType type, string funcIden)
		{
			VTableMethod method = new VTableMethod(iden, type, funcIden);
			VTable vtable = _compUnit.GetVTable(vtableIden);
			vtable.AddMethod(method, null);
		}

		public ILEnum CreateEnum(ScopeVariable identifier, string mangledName, Symbol symbol, ILAttributes ilAttribs, List<ILTemplateParam> templateParams, SymbolType baseType, TextSpan span)
		{
			// TODO: Use span
			ILEnum ilEnum = new ILEnum(identifier, mangledName, symbol, ilAttribs, templateParams, baseType);
			_compUnit.AddEnum(ilEnum);
			return ilEnum;
		}

		public void BuildEnumMember(string enumIdentifier, string identifier, string enumInit, SymbolType adtType, string adtInit, TextSpan span)
		{
			// TODO: Use span
			ILEnum ilEnum = _compUnit.GetEnum(enumIdentifier);
			ILEnumMember member = new ILEnumMember(identifier, enumInit, adtType, adtInit);

			ilEnum.AddMember(member);
		}

		public void BuildDelegate(string mangledName, List<SymbolType> paramTypes, SymbolType retType, bool asFuncPtr, TextSpan span)
		{
			// TODO: Use span
			if (_compUnit.Delegates.ContainsKey(mangledName))
				return;

			ILDelegate del = new ILDelegate(mangledName, paramTypes, retType, asFuncPtr);
			_compUnit.AddDelegate(del);
		}

		public void BuildTypedef(Symbol typedefSym, SymbolType type, TextSpan span)
		{
			// TODO: Use span
			ILTypedef typedef = new ILTypedef(typedefSym, type);
			_compUnit.AddTypedef(typedef);
		}

		public void BuildGlobal(string mangledName, SymbolType type, string initializer, ILAttributes attribs, TextSpan span)
		{
			// TODO: Use span
			ILGlobal global = new ILGlobal(mangledName, type, initializer, attribs);
			_compUnit.AddGlobal(global);
		}

		public void BuildTemplateInstance(ScopeVariable iden, TemplateInstanceSymbolType type, List<ILTemplateArg> args, TextSpan span)
		{
			// TODO: Use span
			ILTemplateInstance instance = new ILTemplateInstance(iden, type, args);
			_compUnit.AddTemplateInstance(instance);
		}

		public void BuildAlias(string mangledAlias, TextSpan span)
		{
			// TODO: Use span
			_compUnit.Aliases.Add(mangledAlias);
		}

		public void BuildType(SymbolType type)
		{
			_compUnit.AddType(type);
		}

		public void CreateInterface(string mangledName, TextSpan span)
		{
			// TODO: Use span
			_compUnit.Interfaces.Add(mangledName, new ILInterface(mangledName));
		}

		public int GetInstructionIndex()
		{
			return _curBasicBlock.Instructions.Count;
		}

		public ILInstruction GetInstruction(int index)
		{
			return _curBasicBlock.Instructions[index];
		}

		public string GetCurrentBlockLabel()
		{
			return _curBasicBlock?.Label;
		}

		public ILBasicBlock GetCurrentBasicBlock()
		{
			return _curBasicBlock;
		}

		public void SetCurrentBasicBlock(ILBasicBlock block)
		{
			_curBasicBlock = block;
		}

		public bool HasTemplateInstance(ScopeVariable iden)
		{
			return _compUnit.TemplateInstances.ContainsKey(iden);
		}

		public bool HasVTable(ScopeVariable iden)
		{
			return _compUnit.VTables.ContainsKey(iden);
		}
	}
}
