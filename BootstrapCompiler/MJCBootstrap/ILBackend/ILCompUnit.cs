﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.ByteCode;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;

namespace MJC.ILBackend
{
	public enum ILStage
	{
		Invalid,
		Raw,
		Canonical,
		CompExec,
		Opt
	}

	public class ILCompUnit
	{
		public ILStage Stage;
		public string ILModule;

		public List<string> ImportNames = new List<string>();

		public ILCompUnit Builtins;
		public List<ILCompUnit> ModuleCompUnits = new List<ILCompUnit>();
		public List<ILCompUnit> Imports = new List<ILCompUnit>();
		
		public Dictionary<string, ILFunction> Functions = new Dictionary<string, ILFunction>();

		public Dictionary<ScopeVariable, ILAggregate> Aggregates = new Dictionary<ScopeVariable, ILAggregate>();
		public Dictionary<string, ILAggregate> AggregatesMangled = new Dictionary<string, ILAggregate>();
		public Dictionary<ScopeVariable, List<ILAggregate>> AggregatesVariants = new Dictionary<ScopeVariable, List<ILAggregate>>();

		public Dictionary<string, ILEnum> Enums = new Dictionary<string, ILEnum>();
		public Dictionary<ScopeVariable, List<ILEnum>> EnumVariants = new Dictionary<ScopeVariable, List<ILEnum>>();

		public Dictionary<string, ILInterface> Interfaces = new Dictionary<string, ILInterface>();

		public Dictionary<string, ILTypedef> Typedefs = new Dictionary<string, ILTypedef>();

		public Dictionary<ScopeVariable, VTable> VTables = new Dictionary<ScopeVariable, VTable>();
		public Dictionary<ScopeVariable, List<VTable>> VTablesVariants = new Dictionary<ScopeVariable, List<VTable>>();
		public Dictionary<string, VTable> VTablesMangled = new Dictionary<string, VTable>();
		public Dictionary<string, ILDelegate> Delegates = new Dictionary<string, ILDelegate>();
		public Dictionary<string, ILGlobal> Globals = new Dictionary<string, ILGlobal>();
		public Dictionary<ScopeVariable, ILTemplateInstance> TemplateInstances = new Dictionary<ScopeVariable, ILTemplateInstance>();
		public List<string> Aliases = new List<string>();

		// Builtin types
		public Dictionary<ArraySymbolType, TemplateInstanceSymbolType> FixedSizeArrayTypes = new Dictionary<ArraySymbolType, TemplateInstanceSymbolType>();
		public Dictionary<SymbolType, TemplateInstanceSymbolType> ArrayTypes = new Dictionary<SymbolType, TemplateInstanceSymbolType>();

		public ILCompUnitContext Context;

		//public List<ILAggregate> TemplateAggregates = new List<ILAggregate>();
		//public List<ILEnum> TemplateEnums = new List<ILEnum>();

		public List<ILFunction> TemplateFunctions = new List<ILFunction>();
		public ILByteCodeModule ByteCodeModule;

		public void AddVTable(VTable vtable, bool isTemplateInstance = false)
		{
			VTables.Add(vtable.Iden, vtable);

			ScopeVariable baseScopeVar = vtable.Iden.GetBaseTemplate();
			if (!isTemplateInstance && baseScopeVar.Name is TemplateDefinitionIdentifier)
			{
				if (!VTablesVariants.ContainsKey(baseScopeVar))
					VTablesVariants.Add(baseScopeVar, new List<VTable>());

				VTablesVariants[baseScopeVar].Add(vtable);
			}

			if (!(vtable.Iden.Name is TemplateDefinitionIdentifier))
				VTablesMangled.Add(vtable.MangledIden, vtable);
		}

		public VTable GetVTable(ScopeVariable iden)
		{
			if (VTables.TryGetValue(iden, out VTable vtable))
			{
				return vtable;
			}

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.VTables.TryGetValue(iden, out vtable))
				{
					return vtable;
				}
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.VTables.TryGetValue(iden, out vtable))
				{
					return vtable;
				}
			}

			return null;
		}

		public VTable GetVTable(string iden)
		{
			if (VTablesMangled.TryGetValue(iden, out VTable vtable))
			{
				return vtable;
			}

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.VTablesMangled.TryGetValue(iden, out vtable))
				{
					return vtable;
				}
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.VTablesMangled.TryGetValue(iden, out vtable))
				{
					return vtable;
				}
			}

			return null;
		}

		public List<VTable> GetVTableVariants(ScopeVariable iden)
		{
			VTablesVariants.TryGetValue(iden, out List<VTable> tables);

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.VTablesVariants.TryGetValue(iden, out List<VTable> tmp))
				{
					if (tables == null)
						tables = new List<VTable>();
					tables.AddRange(tmp);
				}

				if (tables != null)
					return tables;
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.VTablesVariants.TryGetValue(iden, out tables))
				{
					return tables;
				}
			}
			
			return null;
		}

		public void AddFunction(ILFunction func)
		{
			Functions.Add(func.MangledIdentifier, func);

			if (func.IsUninstantiated)
				TemplateFunctions.Add(func);
		}

		public ILFunction GetFunction(string name)
		{
			if (Functions.TryGetValue(name, out ILFunction func))
			{
				return func;
			}

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.Functions.TryGetValue(name, out func))
				{
					return func;
				}
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.Functions.TryGetValue(name, out func))
				{
					return func;
				}
			}

			return null;
		}

		public bool IsFunctionExternal(string name)
		{
			// TODO: is a function in another compilation unit, but the same module, external? depends on how the module is built
			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.Functions.ContainsKey(name))
				{
					return true;
				}
			}

			return false;
		}

		public void RemoveFunction(string name)
		{
			Functions.Remove(name);
		}

		public void AddAggregate(ILAggregate aggregate, bool isTemplateInstance = false)
		{
			Aggregates.Add(aggregate.Identifier, aggregate);

			if (!isTemplateInstance)
			{
				if (!AggregatesVariants.ContainsKey(aggregate.BaseIdentifier))
					AggregatesVariants.Add(aggregate.BaseIdentifier, new List<ILAggregate>());

				AggregatesVariants[aggregate.BaseIdentifier].Add(aggregate);
			}
			//else if (aggregate.TemplateParams != null)
			//	TemplateAggregates.Add(aggregate);

			if (aggregate.TemplateParams == null)
				AggregatesMangled.Add(aggregate.MangledIdentifier, aggregate);
		}

		public ILAggregate GetAggregate(ScopeVariable iden)
		{
			if (Aggregates.TryGetValue(iden, out ILAggregate aggr))
				return aggr;

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.Aggregates.TryGetValue(iden, out aggr))
					return aggr;
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.Aggregates.TryGetValue(iden, out aggr))
					return aggr;
			}

			return null;
		}

		public ILAggregate GetAggregate(string name)
		{
			if (AggregatesMangled.TryGetValue(name, out ILAggregate aggr))
				return aggr;

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.AggregatesMangled.TryGetValue(name, out aggr))
					return aggr;
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.AggregatesMangled.TryGetValue(name, out aggr))
					return aggr;
			}

			return null;
		}

		public List<ILAggregate> GetAggregateVariants(ScopeVariable iden)
		{
			if (AggregatesVariants.TryGetValue(iden, out List<ILAggregate> aggrs))
				return aggrs;

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.AggregatesVariants.TryGetValue(iden, out List<ILAggregate> tmp))
				{
					if (aggrs == null)
						aggrs = new List<ILAggregate>();
					aggrs.AddRange(tmp);
				}

				if (aggrs != null)
					return aggrs;
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.AggregatesVariants.TryGetValue(iden, out aggrs))
					return aggrs;
			}

			return null;
		}

		public void AddEnum(ILEnum ilEnum, bool isTemplateInstance = false)
		{
			Enums.Add(ilEnum.MangledIdentifier, ilEnum);

			if (!isTemplateInstance)
			{
				if (!EnumVariants.ContainsKey(ilEnum.BaseIdentifier))
					EnumVariants.Add(ilEnum.BaseIdentifier, new List<ILEnum>());

				EnumVariants[ilEnum.BaseIdentifier].Add(ilEnum);
			}
			//else if (ilEnum.TemplateParams != null)
			//	TemplateEnums.Add(ilEnum);
		}

		public ILEnum GetEnum(string name)
		{
			if (Enums.TryGetValue(name, out ILEnum ilEnum))
			{
				return ilEnum;
			}

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.Enums.TryGetValue(name, out ilEnum))
				{
					return ilEnum;
				}
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.Enums.TryGetValue(name, out ilEnum))
				{
					return ilEnum;
				}
			}

			return null;
		}

		public List<ILEnum> GetEnumVariants(ScopeVariable iden)
		{
			EnumVariants.TryGetValue(iden, out List<ILEnum> enums);

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.EnumVariants.TryGetValue(iden, out List<ILEnum> tmp))
				{
					if (enums == null)
						enums = new List<ILEnum>();
					enums.AddRange(tmp);
				}

				if (enums != null)
					return enums;
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.EnumVariants.TryGetValue(iden, out enums))
				{
					return enums;
				}
			}

			return null;
		}

		public void AddTypedef(ILTypedef typedef)
		{
			Typedefs.Add(typedef.TypedefSym.MangledName, typedef);
		}

		public ILTypedef GetTypedef(string name)
		{
			if (Typedefs.TryGetValue(name, out ILTypedef td))
			{
				return td;
			}

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.Typedefs.TryGetValue(name, out td))
				{
					return td;
				}
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.Typedefs.TryGetValue(name, out td))
				{
					return td;
				}
			}

			return null;
		}

		public void AddDelegate(ILDelegate del)
		{
			Delegates.Add(del.MangledIdentifier, del);
		}

		public void AddGlobal(ILGlobal global)
		{
			Globals.Add(global.MangledIdentifier, global);
		}

		public ILGlobal GetGlobal(string name)
		{
			if (Globals.TryGetValue(name, out ILGlobal global))
			{
				return global;
			}

			foreach (ILCompUnit compUnit in ModuleCompUnits)
			{
				if (compUnit.Globals.TryGetValue(name, out global))
				{
					return global;
				}
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.Globals.TryGetValue(name, out global))
				{
					return global;
				}
			}

			return null;
		}

		public bool IsGlobalExternal(string name)
		{
			// TODO: is a global in another compilation unit, but the same module, external? depends on how the module is built
			foreach (ILCompUnit compUnit in Imports)
			{
				if (compUnit.Globals.ContainsKey(name))
				{
					return true;
				}
			}

			return false;
		}

		public void AddTemplateInstance(ILTemplateInstance instance)
		{
			TemplateInstances.Add(instance.Iden, instance);
		}

		public void AddType(SymbolType type)
		{
			switch (type)
			{
			case ArraySymbolType arrType:
			{
				// Make sure to add the array type without attributes, since replacement happens after type attributes are removed in ILArrayReplacementPass
				ArraySymbolType noAttribArrType = ILHelpers.GetTypeWithoutAttribs(arrType) as ArraySymbolType;

				if (arrType.ArraySizeFunc != null)
				{
					if (!FixedSizeArrayTypes.ContainsKey(noAttribArrType))
						FixedSizeArrayTypes.Add(arrType, null);

				}
				else
				{
					if (!ArrayTypes.ContainsKey(noAttribArrType.BaseType))
						ArrayTypes.Add(arrType.BaseType, null);
				}
				break;
			}
			}
		}

		// TODO: allow monolithic IL compilation unit
		// Currently, only template funcs are merged
		public void MergeInto(ILCompUnit compUnit)
		{
			if (ILModule == null)
				ILModule = compUnit.ILModule;

			// TODO: Check for duplicates
			TemplateFunctions.AddRange(compUnit.TemplateFunctions);

			foreach (KeyValuePair<string, ILFunction> pair in compUnit.Functions)
			{
				Functions.Add(pair.Key, pair.Value);
			}

			foreach (KeyValuePair<string, ILEnum> pair in compUnit.Enums)
			{
				Enums.Add(pair.Key, pair.Value);
			}

			foreach (KeyValuePair<string, ILAggregate> pair in compUnit.AggregatesMangled)
			{
				AggregatesMangled.Add(pair.Key, pair.Value);
			}
		}
	}
}
