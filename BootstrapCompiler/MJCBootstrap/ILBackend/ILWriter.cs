﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend
{
	class ILWriter
	{
		public static void Write(ILCompUnit compUnit, string path)
		{
			string stage = compUnit.Stage.ToString().ToLower();

			string actPath = CmdLine.IntermediateDirectory + Helpers.GetModuleFilePath(compUnit.ILModule);

			if (compUnit.Stage != ILStage.Raw)
			{
				string ext = Path.GetExtension(path);
				actPath += Path.GetFileNameWithoutExtension(path) + '-' + stage + ext;
			}
			else
			{
				actPath += Path.GetFileName(path);
			}

			Helpers.EnsureDirectoryExists(Path.GetDirectoryName(actPath));
			using (StreamWriter writer = new StreamWriter(actPath, false))
			{
				writer.WriteLine($".stage {stage}");
				writer.WriteLine($".module {compUnit.ILModule}");
				writer.WriteLine();
				foreach (string import in compUnit.ImportNames)
				{
					writer.WriteLine($".import {import}");
				}

				writer.WriteLine();
				foreach (KeyValuePair<ScopeVariable, ILAggregate> pair in compUnit.Aggregates)
				{
					WriteAggregate(pair.Value, writer);
					writer.WriteLine();
				}

				foreach (KeyValuePair<string, ILEnum> pair in compUnit.Enums)
				{
					WriteEnum(pair.Value, writer);
					writer.WriteLine();
				}

				foreach (KeyValuePair<string, ILInterface> pair in compUnit.Interfaces)
				{
					WriteInterface(pair.Value, writer);
					writer.WriteLine();
				}

				foreach (KeyValuePair<ScopeVariable, VTable> pair in compUnit.VTables)
				{
					WriteVTable(pair.Value, writer);
					writer.WriteLine();
				}

				foreach (KeyValuePair<string, ILDelegate> pair in compUnit.Delegates)
				{
					WriteDelegate(pair.Value, writer);
				}
				if (compUnit.Delegates.Count > 0)
					writer.WriteLine();

				foreach (KeyValuePair<string, ILTypedef> pair in compUnit.Typedefs)
				{
					WriteTypedef(pair.Value, writer);
				}
				if (compUnit.Typedefs.Count > 0)
					writer.WriteLine();

				foreach (KeyValuePair<ScopeVariable, ILTemplateInstance> pair in compUnit.TemplateInstances)
				{
					WriteTemplateInstance(pair.Value, writer);
				}
				if (compUnit.TemplateInstances.Count > 0)
					writer.WriteLine();

				foreach (KeyValuePair<string, ILGlobal> pair in compUnit.Globals)
				{
					WriteGlobal(pair.Value, writer);
				}
				if (compUnit.Globals.Count > 0)
					writer.WriteLine();

				foreach (KeyValuePair<string, ILFunction> pair in compUnit.Functions)
				{
					WriteFunction(pair.Value, writer);
				}

				foreach (string alias in compUnit.Aliases)
				{
					WriteAlias(alias, writer);
				}
			}
		}

		private static void WriteAggregate(ILAggregate aggregate, StreamWriter writer)
		{
			string typeStr = '.' + aggregate.Type.ToString().ToLower();

			writer.Write($"{typeStr} {aggregate.Attributes} {aggregate.MangledIdentifier}");

			if (aggregate.TemplateParams != null)
			{
				writer.Write('<');
				for (var i = 0; i < aggregate.TemplateParams.Count; i++)
				{
					if (i != 0)
						writer.Write(", ");

					ILTemplateParam param = aggregate.TemplateParams[i];
					writer.Write(param.ToString());
				}

				writer.Write('>');
			}

			writer.WriteLine('{');

			foreach (ILAggregateVariable variable in aggregate.Variables)
			{
				writer.WriteLine($"\t{variable}");
			}

			writer.WriteLine("}");
		}

		private static void WriteVTable(VTable vTable, StreamWriter writer)
		{
			writer.Write($".vtable {vTable.MangledIden}");
			if (vTable.Iden.Name is TemplateDefinitionIdentifier defName)
			{
				writer.Write('<');
				for (var i = 0; i < defName.Parameters.Count; i++)
				{
					if (i != 0)
						writer.Write(", ");
					TemplateParameter param = defName.Parameters[i];

					if (param.TypeSpecialization != null)
					{
						writer.Write($"${param.TypeSpecialization}");
					}
					else if (param.ValueSpecialization != null)
					{
						writer.Write($"{param.ValueSpecialization} : ${param.Type}");
					}
					/*else if (param.ValueSpecializationFunc != null)
					{
						writer.Write($"@{param.ValueSpecializationFunc} : ${param.Type}");
					}*/
					else
					{
						if (param.ValueName != null)
							writer.Write($"%{param.ValueName} : ");
						writer.Write($"${param.Type}");
					}
				}

				writer.Write('>');
			}
			else if (vTable.Iden.Name is TemplateInstanceIdentifier instName)
			{
				writer.Write('<');
				for (var i = 0; i < instName.Arguments.Count; i++)
				{
					if (i != 0)
						writer.Write(", ");
					TemplateArgument arg = instName.Arguments[i];
					if (arg.Value != null)
						writer.Write($"{arg.Value} : ${arg.Type.ToILString()}");
					else
						writer.Write(arg.Type);
				}

				writer.Write('>');
			}
			writer.WriteLine(" {");

			foreach (KeyValuePair<ScopeVariable, VTableSection> pair in vTable.Sections)
			{
				VTableSection section = pair.Value;
				writer.WriteLine($"\t.section {section.Mangled ?? "_"}");

				foreach (KeyValuePair<string, List<VTableMethod>> methodPair in section.Methods)
				{
					foreach (VTableMethod method in methodPair.Value)
					{
						writer.WriteLine($"\t\t{method.Iden} ${method.Type.ToILString()} : @{method.MangledFunc}");
					}
				}
			}

			

			writer.WriteLine("}");
		}

		private static void WriteEnum(ILEnum ilEnum, StreamWriter writer)
		{
			writer.Write($".enum {ilEnum.MangledIdentifier}");

			string templateParamStr = null;
			if (ilEnum.TemplateParams != null)
			{
				templateParamStr += "<";
				for (var i = 0; i < ilEnum.TemplateParams.Count; i++)
				{
					if (i != 0)
						templateParamStr += ", ";

					ILTemplateParam param = ilEnum.TemplateParams[i];
					templateParamStr += param.ToString();
				}
				templateParamStr += ">";
				writer.Write(templateParamStr);
			}
			writer.WriteLine($": ${ilEnum.BaseType.ToILString()} {{");

			foreach (ILEnumMember member in ilEnum.Members)
			{
				if (member.EnumVal != null)
					writer.Write($"\t{member.Identifier} , {member.EnumVal}");
				else
					writer.Write($"\t{member.Identifier} , @{member.EnumInit}");

				if (member.AdtType != null)
				{
					writer.Write($" : ${member.AdtType.ToILString()}");
					if (member.AdtInit != null)
					{
						if (templateParamStr != null)
							writer.Write(templateParamStr);
						writer.Write($", @{member.AdtInit}");
						if (templateParamStr != null)
							writer.Write(templateParamStr);
					}
				}

				writer.WriteLine();
			}

			writer.WriteLine("}");
		}

		private static void WriteInterface(ILInterface ilInterface, StreamWriter writer)
		{
			writer.Write($".interface {ilInterface.MangledIdentifier}");

			/*if (ilInterface.TemplateParams != null)
			{
				writer.Write('<');
				for (var i = 0; i < ilInterface.TemplateParams.Count; i++)
				{
					if (i != 0)
						writer.Write(", ");

					ILTemplateParam param = ilInterface.TemplateParams[i];
					writer.Write(param.ToString());
				}

				writer.Write('>');
			}*/

			writer.WriteLine('{');

			/*foreach (ILAggregateVariable variable in ilInterface.Variables)
			{
				writer.WriteLine($"\t{variable}");
			}*/

			writer.WriteLine("}");
		}

		private static void WriteDelegate(ILDelegate del, StreamWriter writer)
		{
			string paramStr = "";
			if (del.ParamTypes != null)
			{
				paramStr = " ";
				foreach (SymbolType paramType in del.ParamTypes)
				{
					if (paramStr == " ")
						paramStr = paramType.ToILString();
					else
						paramStr += ", " + paramType.ToILString();
				}
				paramStr += " ";
			}

			writer.Write(".delegate ");
			writer.Write($"{del.MangledIdentifier} $({paramStr}) -> ");

			if (del.ReturnType is TupleSymbolType)
				writer.WriteLine($"{del.ReturnType}");
			else
				writer.WriteLine($"({del.ReturnType})");
		}

		public static void WriteFunction(ILFunction function, StreamWriter writer)
		{
			writer.Write(function.IsOperator ? ".operator" : ".func"); 
			if (function.Attributes != null)
				writer.Write($" {function.Attributes}");
			writer.Write($" @{function.MangledIdentifier}");

			if (function.TemplateParams != null)
			{
				writer.Write('<');
				for (var i = 0; i < function.TemplateParams.Count; i++)
				{
					if (i != 0)
						writer.Write(", ");
					ILTemplateParam param = function.TemplateParams[i];
					if (param.ValueName != null)
						writer.Write($"{param.ValueName}:");
					writer.Write($"${param.Type.ToILString()}");
				}

				writer.Write('>');
			}

			writer.Write($" ( ");
			if (function.Operands != null)
			{
				for (var i = 0; i < function.Operands.Count; i++)
				{
					if (i != 0)
						writer.Write(", ");

					writer.Write($"{function.Operands[i]}");
				}
			}

			writer.Write($" ) -> ${(function.FuncType.ReturnType == null ? "()" : function.FuncType.ReturnType.ToILString())}");

			if (function.Blocks.Count > 0)
			{
				writer.WriteLine(" {");

				foreach (ILBasicBlock block in function.Blocks)
				{
					writer.WriteLine($"{block.Label}:");
					foreach (ILInstruction instruction in block.Instructions)
					{
						writer.WriteLine($"\t{instruction}");
					}
				}

				writer.WriteLine("}");
				writer.WriteLine();
			}
			else
			{
				writer.WriteLine(';');
			}
		}

		private static void WriteTypedef(ILTypedef typedef, StreamWriter writer)
		{
			writer.Write($".typedef ${typedef.TypedefSym.MangledName}");

			if (typedef.TypedefSym.Identifier is TemplateDefinitionIdentifier defName)
			{
				if (defName.Parameters != null)
				{
					writer.Write('<');
					for (var i = 0; i < defName.Parameters.Count; i++)
					{
						if (i != 0)
							writer.Write(", ");

						TemplateParameter param = defName.Parameters[i];
						writer.Write(param.ToString());
					}

					writer.Write('>');
				}
			}
			else if (typedef.TypedefSym.Identifier is TemplateInstanceIdentifier instName)
			{
				if (instName.Arguments != null)
				{
					writer.Write('<');
					for (var i = 0; i < instName.Arguments.Count; i++)
					{
						if (i != 0)
							writer.Write(", ");

						TemplateArgument arg = instName.Arguments[i];
						writer.Write(arg.ToString());
					}

					writer.Write('>');
				}
			}

			writer.WriteLine($" : ${typedef.Type.ToILString()}");
		}

		public static void WriteTemplateInstance(ILTemplateInstance instance, StreamWriter writer)
		{

			if (instance.IsTypeInstance())
			{
				writer.Write($".template_type_instance ${instance.Type.TemplateSymbol.MangledName}!<");
			}
			else
			{
				writer.Write($".template_func_instance @{instance.Type.TemplateSymbol.MangledName}!<");
			}

			for (var i = 0; i < instance.Arguments.Count; i++)
			{
				if (i != 0)
					writer.Write(", ");
				ILTemplateArg arg = instance.Arguments[i];
				writer.Write(arg.ToILString());
			}

			if (instance.IsTypeInstance())
			{
				writer.WriteLine('>');
			}
			else
			{
				writer.WriteLine($"> ${instance.Type.ToILString()}");
			}

			
		}

		public static void WriteGlobal(ILGlobal global, StreamWriter writer)
		{
			writer.Write(".global");
			if (global.Attributes != null)
				writer.Write($" {global.Attributes}");

			writer.Write($" {global.MangledIdentifier} : ${global.Type}, ");

			if (global.Value != null)
				writer.WriteLine(global.Value);
			else
				writer.WriteLine('@' + global.Initializer);
		}

		private static void WriteAlias(string alias, StreamWriter writer)
		{
			writer.WriteLine(".typealias " + alias);
		}

	}
}
