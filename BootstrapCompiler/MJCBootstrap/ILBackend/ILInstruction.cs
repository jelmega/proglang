﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend
{
	public enum ILInstructionType
	{
		Unknown,
		IntLiteral,
		FloatLiteral,
		StringLiteral,

		AllocStack,
		DeallocStack,

		Assign,
		Store,
		Load,

		FunctionRef,
		MethodRef,
		OperatorRef,

		Call,
		Builtin,
		CompilerIntrin,

		TupleUnpack,
		TuplePack,
		TupleExtract,
		TupleInsert,
		TupleElemAddr,

		StructExtract,
		StructInsert,
		StructElemAddr,

		UnionExtract,
		UnionInsert,
		UnionElemAddr,

		Destruct,

		EnumValue,
		EnumGetValue,
		EnumGetMemberValue,

		GlobalAddr,
		GlobalValue,

		TemplateGetValue,

		ConvType,
		GetVTable,

		Return,
		Branch,
		CondBranch,
		SwitchEnum,
		SwitchValue,
	}

	public class ILInstruction
	{
		public ILInstructionType InstructionType;
		public TextSpan Span;

		public bool IsTerminal()
		{
			switch (InstructionType)
			{
			case ILInstructionType.Return:
			case ILInstructionType.Branch:
			case ILInstructionType.CondBranch:
			case ILInstructionType.SwitchEnum:
			case ILInstructionType.SwitchValue:
				return true;
			default:
				return false;
			}
		}

		public bool ExitsFunction()
		{
			switch (InstructionType)
			{
			case ILInstructionType.Return:
				return true;
			default:
				return false;
			}
		}

		public bool IsCompileTimeOnly()
		{
			switch (InstructionType)
			{
			case ILInstructionType.CompilerIntrin:
			case ILInstructionType.TemplateGetValue:
				return true;
			default:
				return false;
			}
		}
	}

	public class ILIntLiteralInstruction : ILInstruction
	{
		public long Value;
		public ILOperand RetOperand;

		public ILIntLiteralInstruction(ILOperand retOperand, long value, TextSpan span)
		{
			Value = value;
			RetOperand = retOperand;
			InstructionType = ILInstructionType.IntLiteral;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = int_literal {Value} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILFloatLiteralInstruction : ILInstruction
	{
		public long HexValue;
		public ILOperand RetOperand;

		public ILFloatLiteralInstruction(ILOperand retOperand, long hexValue, TextSpan span)
		{
			HexValue = hexValue;
			RetOperand = retOperand;
			InstructionType = ILInstructionType.FloatLiteral;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = float_literal {HexValue:X8} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILStringLiteralInstruction : ILInstruction
	{
		public string String;
		public ILOperand RetOperand;

		public ILStringLiteralInstruction(ILOperand retOperand, string str, TextSpan span)
		{
			String = str;
			RetOperand = retOperand;
			InstructionType = ILInstructionType.StringLiteral;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = string_literal \"{String}\"";
		}
	}



	public class ILAllocStackInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public SymbolType Type;

		public ILAllocStackInstruction(ILOperand retOperand, SymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			Type = type;
			InstructionType = ILInstructionType.AllocStack;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = alloc_stack ${Type.ToILString()}";
		}
	}

	public class ILDeallocStackInstruction : ILInstruction
	{
		public ILOperand Operand;

		public ILDeallocStackInstruction(ILOperand operand)
		{
			InstructionType = ILInstructionType.DeallocStack;
			Operand = operand;
		}

		public override string ToString()
		{
			return $"dealloc_stack {Operand}";
		}
	}



	public class ILAssignInstruction : ILInstruction
	{
		public ILOperand Src;
		public ILOperand Dst;

		public ILAssignInstruction(ILOperand src, ILOperand dst, TextSpan span)
		{
			Src = src;
			Dst = dst;
			InstructionType = ILInstructionType.Assign;
			Span = span;
		}

		public override string ToString()
		{
			return $"assign %{Src.Iden} to {Dst}";
		}
	}

	public class ILStoreInstruction : ILInstruction
	{
		public ILOperand Src;
		public ILOperand Dst;

		public ILStoreInstruction(ILOperand src, ILOperand dst, TextSpan span)
		{
			Src = src;
			Dst = dst;
			InstructionType = ILInstructionType.Store;
			Span = span;
		}

		public override string ToString()
		{
			return $"store %{Src.Iden} in {Dst}";
		}
	}

	public class ILLoadInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Src;

		public ILLoadInstruction(ILOperand retOperand, ILOperand src, TextSpan span)
		{
			RetOperand = retOperand;
			Src = src;
			InstructionType = ILInstructionType.Load;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = load {Src}";
		}
	}

	public class ILFunctionRefInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public string MangledName;
		public FunctionSymbolType Type;

		public ILFunctionRefInstruction(ILOperand retOperand, string mangledName, FunctionSymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			MangledName = mangledName;
			Type = type;
			InstructionType = ILInstructionType.FunctionRef;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = function_ref @{MangledName} : ${Type.ToILString()}";
		}
	}

	public class ILMethodRefInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILReference MethodRef;
		public FunctionSymbolType Type;

		public ILMethodRefInstruction(ILOperand retOperand, ILReference methodRef, FunctionSymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			MethodRef = methodRef;
			Type = type;
			InstructionType = ILInstructionType.MethodRef;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = method_ref {MethodRef} : ${Type.ToILString()}";
		}
	}

	public class ILOperatorRefInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOpLoc Loc;
		public ILOpType Op;
		public FunctionSymbolType Type;

		public ILOperatorRefInstruction(ILOperand retOperand, ILOpLoc opLoc, ILOpType op, FunctionSymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			Loc = opLoc;
			Op = op;
			Type = type;
			InstructionType = ILInstructionType.OperatorRef;
			Span = span;
		}

		public override string ToString()
		{
			string opName = ILHelpers.GetIlOpName(Loc, Op).Replace('_', ' ');
			return $"%{RetOperand.Iden} = operator_ref {opName} : ${Type.ToILString()}";
		}
	}


	public class ILCallInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Func;
		public List<ILOperand> Operands;

		public ILCallInstruction(ILOperand retOperand, ILOperand func, List<ILOperand> parameters, TextSpan span)
		{
			RetOperand = retOperand;
			Func = func;
			Operands = parameters;
			InstructionType = ILInstructionType.Call;
			Span = span;
		}

		public override string ToString()
		{
			string str;
			if (RetOperand != null)
				str = $"%{RetOperand.Iden} = call %{Func.Iden} ( ";
			else
				str = $"call %{Func.Iden} ( ";

			if (Operands != null)
			{
				for (var i = 0; i < Operands.Count; i++)
				{
					ILOperand operand = Operands[i];
					str += $"{operand}";
					if (i != Operands.Count - 1)
						str += " , ";
				}
			}

			if (RetOperand != null)
				str += $" ) : ${RetOperand.Type.ToILString()}";
			else
				str += " )";

			return str;
		}
	}

	public class ILBuiltinInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public string Builtin;
		public List<ILOperand> Operands;

		public ILBuiltinInstruction(ILOperand retOperand, string builtin, List<ILOperand> parameters, TextSpan span)
		{
			RetOperand = retOperand;
			Builtin = builtin;
			Operands = parameters;
			InstructionType = ILInstructionType.Builtin;
			Span = span;
		}

		public override string ToString()
		{
			string str;
			if (RetOperand != null)
				str = $"%{RetOperand.Iden} = builtin \"{Builtin}\" ( ";
			else
				str = $"builtin \"{Builtin}\" ( ";

			if (Operands != null)
			{
				for (var i = 0; i < Operands.Count; i++)
				{
					ILOperand operand = Operands[i];
					str += $"{operand}";
					if (i != Operands.Count - 1)
						str += " , ";
				}
			}

			if (RetOperand != null)
				str += $" ) : ${RetOperand.Type.ToILString()}";
			else
				str += " )";

			return str;
		}
	}

	public enum ILCompilerIntrinType
	{
		Sizeof,
		Alignof
	}

	public class ILCompilerIntrinInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILCompilerIntrinType Intrin;
		public SymbolType Type;
		
		public ILCompilerIntrinInstruction(ILOperand retOperand, ILCompilerIntrinType intrin, SymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			Intrin = intrin;
			Type = type;
			InstructionType = ILInstructionType.CompilerIntrin;
			Span = span;
		}

		public override string ToString()
		{
			string str = $"%{RetOperand.Iden} = compile_intrin \"";
			switch (Intrin)
			{
			case ILCompilerIntrinType.Sizeof:
				str += "sizeof";
				break;
			case ILCompilerIntrinType.Alignof:
				str += "alignof";
				break;
			}
			str += "\" ( ";

			str += '$' + Type.ToILString();


			str += " ) : ";
			return str + '$' + RetOperand.Type.ToILString();
		}
	}



	public class ILTupleUnpackInstruction : ILInstruction
	{
		public List<ILOperand> RetOperands;
		public ILOperand Operand;

		public ILTupleUnpackInstruction(List<ILOperand> retOperands, ILOperand operand, TextSpan span)
		{
			RetOperands = retOperands;
			Operand = operand;
			InstructionType = ILInstructionType.TupleUnpack;
			Span = span;
		}

		public override string ToString()
		{
			string str = "";
			for (var i = 0; i < RetOperands.Count; i++)
			{
				ILOperand operand = RetOperands[i];
				str += $"%{operand.Iden}";
				if (i != RetOperands.Count - 1)
					str += " , ";
			}

			str += $" = tuple_unpack {Operand}";

			return str;
		}
	}

	public class ILTuplePackInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public List<ILOperand> Operands;

		public ILTuplePackInstruction(ILOperand retOperand, List<ILOperand> operands, TextSpan span)
		{
			RetOperand = retOperand;
			Operands = operands;
			InstructionType = ILInstructionType.TuplePack;
			Span = span;
		}

		public override string ToString()
		{
			string str = $"%{RetOperand.Iden} = tuple_pack ";

			for (var i = 0; i < Operands.Count; i++)
			{
				ILOperand operand = Operands[i];
				str += $"{operand}";
				if (i != Operands.Count - 1)
					str += " , ";
			}

			return str;
		}
	}

	public class ILTupleInsertInstruction : ILInstruction
	{
		public ILOperand Tuple;
		public long Index;
		public ILOperand Value;

		public ILTupleInsertInstruction(ILOperand tuple, long index, ILOperand value, TextSpan span)
		{
			Tuple = tuple;
			Index = index;
			Value = value;
			InstructionType = ILInstructionType.TupleInsert;
			Span = span;
		}

		public override string ToString()
		{
			return $"tuple_insert {Tuple} , {Index} , {Value}";
		}
	}

	public class ILTupleExtractInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Tuple;
		public long Index;

		public ILTupleExtractInstruction(ILOperand retOperand, ILOperand tuple, long index, TextSpan span)
		{
			RetOperand = retOperand;
			Tuple = tuple;
			Index = index;
			InstructionType = ILInstructionType.TupleExtract;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = tuple_extract {Tuple} , {Index} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILTupleElemAddrInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Tuple;
		public long Index;
		public ILOperand Value;

		public ILTupleElemAddrInstruction(ILOperand retOperand, ILOperand tuple, long index, TextSpan span)
		{
			RetOperand = retOperand;
			Tuple = tuple;
			Index = index;
			InstructionType = ILInstructionType.TupleElemAddr;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = tuple_elem_addr {Tuple} , {Index}";
		}
	}

	public class ILStructExtractInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Struct;
		public ILReference ElemRef;

		public ILStructExtractInstruction(ILOperand retOperand, ILOperand structOp, ILReference elemRef, TextSpan span)
		{
			RetOperand = retOperand;
			Struct = structOp;
			ElemRef = elemRef;
			InstructionType = ILInstructionType.StructExtract;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = struct_extract {Struct} , {ElemRef} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILStructInsertInstruction : ILInstruction
	{
		public ILOperand Struct;
		public ILReference ElemRef;
		public ILOperand Value;

		public ILStructInsertInstruction(ILOperand structOp, ILReference elemRef, ILOperand value, TextSpan span)
		{
			Struct = structOp;
			ElemRef = elemRef;
			Value = value;
			InstructionType = ILInstructionType.StructInsert;
			Span = span;
		}

		public override string ToString()
		{
			return $"struct_insert {Struct} , {ElemRef} , {Value}";
		}
	}

	public class ILStructElemAddrInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Struct;
		public ILReference ElemRef;

		public ILStructElemAddrInstruction(ILOperand retOperand, ILOperand structOp, ILReference elemRef, TextSpan span)
		{
			RetOperand = retOperand;
			Struct = structOp;
			ElemRef = elemRef;
			InstructionType = ILInstructionType.StructElemAddr;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = struct_elem_addr {Struct} , {ElemRef} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILTemplateGetValueInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public SymbolType TemplateType;
		public ILReference ElemRef;

		public ILTemplateGetValueInstruction(ILOperand retOperand, SymbolType templateType, ILReference elemRef, TextSpan span)
		{
			RetOperand = retOperand;
			TemplateType = templateType;
			ElemRef = elemRef;
			InstructionType = ILInstructionType.TemplateGetValue;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = template_get_value ${TemplateType.ToILString()} , {ElemRef} : ${RetOperand.Type.ToILString()}";
		}
	}


	public class ILUnionExtractInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Union;
		public ILReference ElemRef;

		public ILUnionExtractInstruction(ILOperand retOperand, ILOperand unionOp, ILReference elemRef, TextSpan span)
		{
			RetOperand = retOperand;
			Union = unionOp;
			ElemRef = elemRef;
			InstructionType = ILInstructionType.UnionExtract;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = union_extract {Union} , {ElemRef} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILUnionInsertInstruction : ILInstruction
	{
		public ILOperand Union;
		public ILReference ElemRef;
		public ILOperand Value;

		public ILUnionInsertInstruction(ILOperand unionOp, ILReference elemRef, ILOperand value, TextSpan span)
		{
			Union = unionOp;
			ElemRef = elemRef;
			Value = value;
			InstructionType = ILInstructionType.UnionInsert;
			Span = span;
		}

		public override string ToString()
		{
			return $"union_insert {Union} , {ElemRef} , {Value}";
		}
	}

	public class ILUnionElemAddrInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Union;
		public ILReference ElemRef;

		public ILUnionElemAddrInstruction(ILOperand retOperand, ILOperand unionOp, ILReference elemRef, TextSpan span)
		{
			RetOperand = retOperand;
			Union = unionOp;
			ElemRef = elemRef;
			InstructionType = ILInstructionType.UnionElemAddr;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = union_elem_addr {Union} , {ElemRef} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILEnumValueInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public SymbolType Type;
		public ILReference ElemRef;
		public List<ILOperand> Operands;

		public ILEnumValueInstruction(ILOperand retOperand, SymbolType type, ILReference elemRef, List<ILOperand> operands, TextSpan span)
		{
			RetOperand = retOperand;
			Type = type;
			ElemRef = elemRef;
			Operands = operands;
			InstructionType = ILInstructionType.EnumValue;
			Span = span;
		}

		public override string ToString()
		{
			string str = $"%{RetOperand.Iden} = enum_value %{Type.ToILString()} , {ElemRef}";
			if (Operands != null)
			{
				str += " , " + Operands.ToListString(", ");
			}

			return str;
		}
	}

	public class ILEnumGetValueInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Operand;

		public ILEnumGetValueInstruction(ILOperand retOperand, ILOperand operand, TextSpan span)
		{
			RetOperand = retOperand;
			Operand = operand;
			InstructionType = ILInstructionType.EnumValue;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = enum_get_value {Operand}";
		}
	}

	public class ILEnumGetMemberValueInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public SymbolType Type;
		public ILReference MemberRef;

		public ILEnumGetMemberValueInstruction(ILOperand retOperand, SymbolType type, ILReference memberRef, TextSpan span)
		{
			RetOperand = retOperand;
			Type = type;
			MemberRef = memberRef;
			InstructionType = ILInstructionType.EnumGetMemberValue;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = enum_get_member_value ${Type.ToILString()} , {MemberRef}";
		}
	}



	public class ILGlobalAddrInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public string Identifier;

		public ILGlobalAddrInstruction(ILOperand retOperand, string identifier, TextSpan span)
		{
			RetOperand = retOperand;
			Identifier = identifier;
			InstructionType = ILInstructionType.GlobalAddr;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = global_addr @{Identifier} : ${RetOperand.Type}";
		}
	}

	public class ILGlobalValueInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public string Identifier;

		public ILGlobalValueInstruction(ILOperand retOperand, string identifier, TextSpan span)
		{
			RetOperand = retOperand;
			Identifier = identifier;
			InstructionType = ILInstructionType.GlobalValue;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = global_value @{Identifier} : ${RetOperand.Type}";
		}
	}

	public class ILConvertTypeInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Operand;

		public ILConvertTypeInstruction(ILOperand retOperand, ILOperand operand, TextSpan span)
		{
			RetOperand = retOperand;
			Operand = operand;
			InstructionType = ILInstructionType.ConvType;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = convert_type {Operand} to ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILGetVTableInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public SymbolType Type;
		public List<SymbolType> InterfaceTypes;

		public ILGetVTableInstruction(ILOperand retOp, SymbolType type, List<SymbolType> interfaceTypes)
		{
			RetOperand = retOp;
			Type = type;
			InterfaceTypes = interfaceTypes;
			InstructionType = ILInstructionType.GetVTable;
		}

		public override string ToString()
		{
			string str = $"%{RetOperand.Iden} = get_vtable ${Type.ToILString()} : ";
			for (var i = 0; i < InterfaceTypes.Count; i++)
			{
				if (i != 0)
					str += ", ";

				SymbolType type = InterfaceTypes[i];
				str += '$' + type.ToILString();
			}
			return str;
		}
	}



	public class ILReturnInstruction : ILInstruction
	{
		public ILOperand Operand;

		public ILReturnInstruction(ILOperand operand, TextSpan span)
		{
			Operand = operand;
			InstructionType = ILInstructionType.Return;
			Span = span;
		}

		public override string ToString()
		{
			if (Operand != null)
				return $"return {Operand}";

			return "return";
		}
	}

	public class ILBranchInstruction : ILInstruction
	{
		public string Label;

		public ILBranchInstruction(string label, TextSpan span)
		{
			Label = label;
			InstructionType = ILInstructionType.Branch;
			Span = span;
		}

		public override string ToString()
		{
			return $"branch {Label}";
		}
	}

	public class ILCondBranchInstruction : ILInstruction
	{
		public ILOperand Condition;
		public string TrueLabel;
		public string FalseLabel;

		public ILCondBranchInstruction(ILOperand condition, string trueLabel, string falseLabel, TextSpan span)
		{
			Condition = condition;
			TrueLabel = trueLabel;
			FalseLabel = falseLabel;
			InstructionType = ILInstructionType.CondBranch;
			Span = span;
		}

		public override string ToString()
		{
			return $"cond_branch {Condition} , {TrueLabel} , {FalseLabel}";
		}
	}

	public class ILRefCase
	{
		public ILRefCase(ILReference member, string label)
		{
			Reference = member;
			Label = label;
		}

		public ILReference Reference;
		public string Label;
	}

	public class ILSwitchEnumInstruction : ILInstruction
	{
		public ILOperand Enum;
		public List<ILRefCase> Cases;
		public string DefaultLabel;

		public ILSwitchEnumInstruction(ILOperand enumOp, List<ILRefCase> cases, string defaultLabel, TextSpan span)
		{
			Enum = enumOp;
			Cases = cases;
			DefaultLabel = defaultLabel;
			InstructionType = ILInstructionType.SwitchEnum;
			Span = span;
		}

		public override string ToString()
		{
			string str = $"switch_enum {Enum}";

			if (Cases != null)
			{
				foreach (ILRefCase refCase in Cases)
				{
					str += $" , case {refCase.Reference} : {refCase.Label}";
				}
			}

			if (DefaultLabel != null)
				str += $" , default : {DefaultLabel}";

			return str;
		}
	}

	public class ILValCase
	{
		public ILOperand Value;
		public string Label;

		public ILValCase(ILOperand value, string label)
		{
			Value = value;
			Label = label;
		}
	}

	public class ILSwitchValueInstruction : ILInstruction
	{
		public ILOperand Value;
		public List<ILValCase> Cases;
		public string DefaultLabel;

		public ILSwitchValueInstruction(ILOperand value, List<ILValCase> cases, string defLabel, TextSpan span)
		{
			Value = value;
			Cases = cases;
			DefaultLabel = defLabel;
			InstructionType = ILInstructionType.SwitchValue;
			Span = span;
		}

		public override string ToString()
		{
			string str = $"switch_enum {Value}";

			if (Cases != null)
			{
				foreach (ILValCase valCase in Cases)
				{
					str += $" , case {valCase.Value} : {valCase.Label}";
				}
			}

			if (DefaultLabel != null)
				str += $" , default : {DefaultLabel}";

			return str;
		}
	}

}
