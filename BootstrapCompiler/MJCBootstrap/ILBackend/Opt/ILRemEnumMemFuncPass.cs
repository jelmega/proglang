﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.ILBackend.Opt
{
	class ILRemEnumMemFuncPass : IILOptimizePass
	{
		public override void ProcessModule(ILCompUnit compUnit)
		{
			foreach (KeyValuePair<string, ILEnum> pair in compUnit.Enums)
			{
				foreach (ILEnumMember member in pair.Value.Members)
				{
					string initFunc = member.EnumInit;
					compUnit.RemoveFunction(initFunc);
				}
			}
		}
	}
}
