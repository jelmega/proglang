﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.ILBackend.Opt
{
	class ILRemGlobalConstFuncPass : IILOptimizePass
	{
		public override void ProcessModule(ILCompUnit compUnit)
		{
			foreach (KeyValuePair<string, ILGlobal> pair in compUnit.Globals)
			{
				ILGlobal global = pair.Value;
				if (global.Value != null)
				{
					string initFunc = global.Initializer;
					compUnit.RemoveFunction(initFunc);
				}
			}
		}
	}
}
