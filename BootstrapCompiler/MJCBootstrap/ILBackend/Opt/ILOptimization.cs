﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.ILBackend.Opt
{
	public class ILOptimization
	{
		private List<IILOptimizePass> _passes = new List<IILOptimizePass>();

		public void AddPass(IILOptimizePass pass)
		{
			_passes.Add(pass);
		}

		public void Process(List<ILCompUnit> compUnits)
		{
			foreach (ILCompUnit compUnit in compUnits)
			{
				foreach (IILOptimizePass pass in _passes)
				{
					pass.ProcessModule(compUnit);
				}

				compUnit.Stage = ILStage.Opt;
			}
		}

	}
}
