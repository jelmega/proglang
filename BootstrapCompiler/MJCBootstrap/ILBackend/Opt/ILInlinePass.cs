﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;
using MJC.IRBackend;

namespace MJC.ILBackend.Opt
{
	class ILInlinePass : IILOptimizePass
	{
		private enum InlineStage
		{
			Always
		}

		private List<ILCompUnit> _refCompUnits = null;
		private ILCompUnit _builtins = null;

		private Dictionary<string, ILFunction>[] _functions;

		private Dictionary<string, ILFunction> _funcMapping = new Dictionary<string, ILFunction>();
		private Stack<int> _removeIndices = new Stack<int>();

		private InlineMode _maxInline;
		private InlineStage _curInline;

		public ILInlinePass(InlineMode maxInline = InlineMode.OnlyAlways)
		{
			_maxInline = maxInline;

			_functions = new Dictionary<string, ILFunction>[(int)ILInline.Count];

			for (int i = 0; i < (int)ILInline.Count; i++)
			{
				_functions[i] = new Dictionary<string, ILFunction>();
			}
		}

		public void Setup(List<ILCompUnit> refCompUnits, ILCompUnit builtins)
		{
			_refCompUnits = refCompUnits;
			_builtins = builtins;

			foreach (Dictionary<string, ILFunction> dict in _functions)
			{
				dict.Clear();
			}

			foreach (ILCompUnit compUnit in _refCompUnits)
			{
				foreach (KeyValuePair<string, ILFunction> pair in compUnit.Functions)
				{
					ILFunction function = pair.Value;
					var inlineAttrib = function.Attributes.Inline;
					_functions[(int)inlineAttrib].Add(function.MangledIdentifier, function);
				}
			}

			foreach (KeyValuePair<string, ILFunction> pair in _builtins.Functions)
			{
				ILFunction function = pair.Value;
				var inlineAttrib = function.Attributes.Inline;
				_functions[(int)inlineAttrib].Add(function.MangledIdentifier, function);
			}
		}


		public override void ProcessModule(ILCompUnit compUnit)
		{
			Setup(compUnit.ModuleCompUnits, compUnit.Builtins);

			if (CompileSettings.Inline == InlineMode.None ||
			    _maxInline == InlineMode.None ||
				_refCompUnits == null && _builtins == null)
				return;

			_curInline = InlineStage.Always;
			base.ProcessModule(compUnit);

			if (CompileSettings.Inline == InlineMode.OnlyAlways ||
			    _maxInline == InlineMode.OnlyAlways)
				return;
		}

		public override void ProcessFunction(ILFunction function)
		{
			_funcMapping.Clear();

			base.ProcessFunction(function);
		}

		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			switch (_curInline)
			{
			case InlineStage.Always:
				ProcessInline(basicBlock, ILInline.Always);
				break;
			}
		}

		void ProcessInline(ILBasicBlock block, ILInline inline)
		{
			for (var instrIdx = 0; instrIdx < block.Instructions.Count; instrIdx++)
			{
				ILInstruction instruction = block.Instructions[instrIdx];
				if (instruction.InstructionType == ILInstructionType.OperatorRef)
				{
					ILOperatorRefInstruction opRef = instruction as ILOperatorRefInstruction;
					string opName = ILHelpers.GetTypedIlOpName(opRef);

					Dictionary<string, ILFunction> alwaysOp = _functions[(int)inline];
					if (!alwaysOp.ContainsKey(opName) || !alwaysOp[opName].IsOperator)
						continue;

					_funcMapping.Add(opRef.RetOperand.Iden, alwaysOp[opName]);
					_removeIndices.Push(instrIdx);
				}
				else if (instruction.InstructionType == ILInstructionType.Call)
				{
					ILCallInstruction call = instruction as ILCallInstruction; ;
					if (_funcMapping.ContainsKey(call.Func.Iden))
					{
						InsertFunc(block, instrIdx, _funcMapping[call.Func.Iden]);
					}
				}
			}

			while (_removeIndices.Count > 0)
			{
				int idx = _removeIndices.Pop();
				block.Instructions.RemoveAt(idx);
			}
		}

		void InsertFunc(ILBasicBlock block, int callIdx, ILFunction op)
		{
			ILCallInstruction callInstr = block.Instructions[callIdx] as ILCallInstruction;

			if (op.Blocks.Count == 1)
			{
				block.Instructions.RemoveAt(callIdx);

				ILInstruction lastInstr = op.Blocks[0].Instructions.Last();
				
				Dictionary<string, ILOperand> replaceMapping = new Dictionary<string, ILOperand>();

				for (var i = 0; i < op.Operands.Count; i++)
				{
					ILOperand origOperand = op.Operands[i];
					ILOperand operand = callInstr.Operands[i];

					replaceMapping.Add(origOperand.Iden, operand);
				}

				if (lastInstr.InstructionType == ILInstructionType.Return)
				{
					ILReturnInstruction retInstr = lastInstr as ILReturnInstruction;
					ILOperand origOperand = retInstr.Operand;
					ILOperand operand = callInstr.RetOperand;

					if (!replaceMapping.ContainsKey(origOperand.Iden))
						replaceMapping.Add(origOperand.Iden, operand);
				}

				InsertNonBlock(block, callIdx, op.Blocks[0], callInstr.Func.Iden, replaceMapping);
			}
		}



		void InsertNonBlock(ILBasicBlock block, int callIdx, ILBasicBlock insertBlock, string callerId, Dictionary<string, ILOperand> replaceMapping)
		{
			foreach (ILInstruction opInstr in insertBlock.Instructions)
			{
				switch (opInstr.InstructionType)
				{
				case ILInstructionType.Unknown:
					break;
				case ILInstructionType.IntLiteral:
					break;
				case ILInstructionType.FloatLiteral:
					break;
				case ILInstructionType.StringLiteral:
					break;
				case ILInstructionType.AllocStack:
					break;
				case ILInstructionType.Assign:
					break;
				case ILInstructionType.Store:
				{
					ILStoreInstruction actInstruction = opInstr as ILStoreInstruction;

					ILOperand srcOperand = GetReplacementOperand(actInstruction.Src, callerId, replaceMapping);
					ILOperand dstOperand = GetReplacementOperand(actInstruction.Dst, callerId, replaceMapping);

					block.Instructions.Insert(callIdx, new ILStoreInstruction(srcOperand, dstOperand, null));
					++callIdx;

					break;
				}
				case ILInstructionType.Load:
				{
					ILLoadInstruction actInstruction = opInstr as ILLoadInstruction;

					ILOperand op = GetReplacementOperand(actInstruction.Src, callerId, replaceMapping);
					ILOperand retOp = GetReplacementOperand(actInstruction.RetOperand, callerId, replaceMapping);

					block.Instructions.Insert(callIdx, new ILLoadInstruction(retOp, op, null));
					++callIdx;

					break;
				}
				case ILInstructionType.OperatorRef:
					break;
				case ILInstructionType.Call:
					break;
				case ILInstructionType.Builtin:
				{
					List<ILOperand> builtinOperands = new List<ILOperand>();
					List<string> builtinReturns = new List<string>();
					ILBuiltinInstruction actInstr = opInstr as ILBuiltinInstruction;

					foreach (ILOperand operand in actInstr.Operands)
					{
						ILOperand newOperand = GetReplacementOperand(operand, callerId, replaceMapping);
						builtinOperands.Add(newOperand);
					}

					ILOperand origRetVar = actInstr.RetOperand;

					ILOperand retOperand = GetReplacementOperand(origRetVar, callerId, replaceMapping);

					block.Instructions.Insert(callIdx, new ILBuiltinInstruction(retOperand, actInstr.Builtin, builtinOperands, null));
					++callIdx;

					break;
				}
				case ILInstructionType.ConvType:
				{
					ILConvertTypeInstruction actInstr = opInstr as ILConvertTypeInstruction;

					ILOperand operand = GetReplacementOperand(actInstr.Operand, callerId, replaceMapping);
					ILOperand retOperand = GetReplacementOperand(actInstr.RetOperand, callerId, replaceMapping);

					block.Instructions.Insert(callIdx, new ILConvertTypeInstruction(retOperand, operand, null));
					++callIdx;

					break;
				}
				case ILInstructionType.Return:
					break;
				}
			}
		}


		ILOperand GetReplacementOperand(ILOperand origOperand, string callerId, Dictionary<string, ILOperand> replaceMapping)
		{
			if (origOperand == null)
				return null;
			if (replaceMapping.ContainsKey(origOperand.Iden))
				return replaceMapping[origOperand.Iden];

			return new ILOperand($"{callerId}_{origOperand.Iden}", origOperand.Type);
		}



	}
}
