﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.Interpret
{
	public class ILInterpMemory
	{
		public List<byte> Data;
		public ulong Offset;
		public ulong Size;

		public ILInterpMemory(List<byte> data, ulong offset, ulong size)
		{
			Data = data;
			Offset = offset;
			Size = size;
		}

		public void Insert(SymbolType type, object obj, bool isX64, int index = 0, CompilerContext context = null)
		{
			switch (type)
			{
			case AggregateSymbolType aggrType:
				break;
			case ArraySymbolType arrType:
				break;
			case BitFieldSymbolType bitFieldType:
				break;
			case BuiltinSymbolType builtinType:
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.Void:
					break;
				case BuiltinTypes.I8:
				{
					sbyte val = (obj as sbyte?).Value;
					Data[(int)Offset] = (byte)val;
					break;
				}
				case BuiltinTypes.Bool:
				case BuiltinTypes.U8:
				case BuiltinTypes.Char:
				{
					byte val = (obj as byte?).Value;
					Data[(int) Offset] = val;
					break;
				}
				case BuiltinTypes.I16:
				{
					short val = (obj as short?).Value;
					byte[] bytes = BitConverter.GetBytes(val);
					Data.ReplaceRange((int)Offset, bytes);
					break;
				}
				case BuiltinTypes.U16:
				case BuiltinTypes.WChar:
				{
					ushort val = (obj as ushort?).Value;
					byte[] bytes = BitConverter.GetBytes(val);
					Data.ReplaceRange((int)Offset, bytes);
					break;
				}
				case BuiltinTypes.I32:
				case BuiltinTypes.ISize when !isX64:
				{
					int val = (obj as int?).Value;
					byte[] bytes = BitConverter.GetBytes(val);
					Data.ReplaceRange((int)Offset, bytes);
					break;
				}
				case BuiltinTypes.U32:
				case BuiltinTypes.USize when !isX64:
				case BuiltinTypes.Rune:
				{
					uint val = (obj as uint?).Value;
					byte[] bytes = BitConverter.GetBytes(val);
					Data.ReplaceRange((int)Offset, bytes);
					break;
				}
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize when isX64:
				{
					long val = (obj as long?).Value;
					byte[] bytes = BitConverter.GetBytes(val);
					Data.ReplaceRange((int)Offset, bytes);
					break;
				}
				case BuiltinTypes.U64:
				case BuiltinTypes.USize when isX64:
				{
					ulong val = (obj as ulong?).Value;
					byte[] bytes = BitConverter.GetBytes(val);
					Data.ReplaceRange((int)Offset, bytes);
					break;
				}
				case BuiltinTypes.F32:
				{
					float val = (obj as float?).Value;
					byte[] bytes = BitConverter.GetBytes(val);
					Data.ReplaceRange((int)Offset, bytes);
					break;
				}
				case BuiltinTypes.F64:
				{
					double val = (obj as double?).Value;
					byte[] bytes = BitConverter.GetBytes(val);
					Data.ReplaceRange((int)Offset, bytes);
					break;
				}
				case BuiltinTypes.String:
				case BuiltinTypes.StringLiteral:
					break;
				case BuiltinTypes.Null:
					break;
				case BuiltinTypes.Any:
					break;
				}
				break;
			}
			case DelegateSymbolType delType:
				break;
			case EnumSymbolType enumType:
			{
				ulong val = (obj as ulong?).Value;
				byte[] range = ILInterpHelpers.CastIntegerToByteArr(enumType.BaseType, (long)val, isX64);
				Data.ReplaceRange((int)Offset, range);
				if (!enumType.IsSimpleEnum)
				{
					// TODO
				}

				break;
			}
			case FunctionSymbolType funcType:
				break;
			case InterfaceSymbolType interfaceType:
				break;
			case MemoryLocSymbolType memoryLocType:
				break;
			case NullableSymbolType nullType:
				break;
			case PointerSymbolType ptrType:
				break;
			case ReferenceSymbolType refType:
				break;
			case TemplateInstanceSymbolType templateInstType:
				break;
			case TemplateParamSymbolType templateParamType:
				break;
			case TupleSymbolType tupType:
			{
				(ulong pos, TypeInfo ti) = ILInterpHelpers.GetElemOffsetAndInfo(tupType, index, context);

				ILInterpMemory mem = new ILInterpMemory(Data, Offset + pos, ti.Size);
				mem.Insert(type, obj, isX64);
				break;
			}
			case VariadicSymbolType varType:
				break;
			}
		}

		public ILInterpValUnion Extract(SymbolType type, bool isX64)
		{
			switch (type)
			{
			case AggregateSymbolType aggrType:
				break;
			case ArraySymbolType arrType:
			{
				int elemSize = (int)(Size / arrType.ArraySize);
				ILInterpMemory[] array = new ILInterpMemory[arrType.ArraySize];
				for (int i = 0; i < (int)arrType.ArraySize; i++)
				{
					array[i] = new ILInterpMemory(Data, (ulong)((int)Offset + i * elemSize), (ulong)elemSize);
				}

				return new ILInterpValUnion{ Array = array };
			}
			case BitFieldSymbolType bitFieldType:
				break;
			case BuiltinSymbolType builtinType:
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
					return new ILInterpValUnion { SByte = (sbyte)Data[(int)Offset] };
				case BuiltinTypes.I16:
				{
					byte[] mem = Data.GetRange((int) Offset, sizeof(short)).ToArray();
					return new ILInterpValUnion { Short = BitConverter.ToInt16(mem, 0) };
				}
				case BuiltinTypes.I32:
				case BuiltinTypes.ISize when !isX64:
				{
					byte[] mem = Data.GetRange((int)Offset, sizeof(int)).ToArray();
					return new ILInterpValUnion { Int = BitConverter.ToInt32(mem, 0) };
				}
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize when isX64:
				{
					byte[] mem = Data.GetRange((int)Offset, sizeof(long)).ToArray();
					return new ILInterpValUnion { Long = BitConverter.ToInt64(mem, 0) };
				}
				case BuiltinTypes.Bool:
				case BuiltinTypes.U8:
				case BuiltinTypes.Char:
					return new ILInterpValUnion { Byte = Data[(int)Offset] };
				case BuiltinTypes.U16:
				case BuiltinTypes.WChar:
				{
					byte[] mem = Data.GetRange((int)Offset, sizeof(ushort)).ToArray();
					return new ILInterpValUnion { UShort = BitConverter.ToUInt16(mem, 0) };
				}
				case BuiltinTypes.U32:
				case BuiltinTypes.USize when !isX64:
				case BuiltinTypes.Rune:
				{
					byte[] mem = Data.GetRange((int)Offset, sizeof(uint)).ToArray();
					return new ILInterpValUnion { UInt = BitConverter.ToUInt32(mem, 0) };
				}
				case BuiltinTypes.U64:
				case BuiltinTypes.USize when isX64:
				{
					byte[] mem = Data.GetRange((int)Offset, sizeof(ulong)).ToArray();
					return new ILInterpValUnion { ULong = BitConverter.ToUInt64(mem, 0) };
				}
				case BuiltinTypes.F32:
				{
					byte[] mem = Data.GetRange((int)Offset, sizeof(float)).ToArray();
					return new ILInterpValUnion { Float = BitConverter.ToSingle(mem, 0) };
				}
				case BuiltinTypes.F64:
				{
					byte[] mem = Data.GetRange((int)Offset, sizeof(double)).ToArray();
					return new ILInterpValUnion { Double = BitConverter.ToDouble(mem, 0) };
				}
				}
				break;
			}
			case DelegateSymbolType delType:
				break;
			case EnumSymbolType enumType:
				break;
			case FunctionSymbolType funcType:
				break;
			case InterfaceSymbolType interfaceType:
				break;
			case MemoryLocSymbolType memoryLocType:
				break;
			case NullableSymbolType nullType:
				break;
			case PointerSymbolType ptrType:
				break;
			case ReferenceSymbolType refType:
				break;
			case TemplateInstanceSymbolType templateInstType:
				break;
			case TemplateParamSymbolType templateParamType:
				break;
			case TupleSymbolType tupType:
				break;
			case VariadicSymbolType varType:
				break;
			}

			return null;
		}

	}

	public class ILInterpStack
	{
		private List<byte> _stack = new List<byte>();
		private Stack<(SymbolType, TypeInfo)> _allocatedTypes = new Stack<(SymbolType, TypeInfo)>();
		private int _offset;
		private bool _isX64;

		private ILCompUnit _compUnit;
		private CompilerContext _compilerContext;

		public ILInterpStack(ILCompUnit compUnit, CompilerContext compilerContext)
		{
			_compUnit = compUnit;
			_compilerContext = compilerContext;
		}

		public ILInterpMemory Push(SymbolType type)
		{
			switch (type)
			{
			case AggregateSymbolType aggrType:
			{
				TypeInfo ti = _compilerContext.GetTypeInfo(aggrType.MangledIdentifier);

				_stack.AddRange(new byte[ti.Size]);
				ILInterpMemory mem = new ILInterpMemory(_stack, (ulong)_offset, ti.Size);
				_allocatedTypes.Push((type, ti));
				_offset += (int)ti.Size;
				return mem;
			}

			case ArraySymbolType arrType:
			{
				TypeInfo ti = _compilerContext.GetTypeInfo(arrType.BaseType.ToILString());
				int size = (int) (ti.Size * arrType.ArraySize);
				_stack.AddRange(new byte[size]);
				ILInterpMemory mem = new ILInterpMemory(_stack, (ulong)_offset, (ulong)size);
				_allocatedTypes.Push((type, ti));
				_offset += size;
				return mem;
			}
			case BitFieldSymbolType bitFieldType:
				break;
			case BuiltinSymbolType builtinType:
			{
				int size = 0;
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.Bool:
				case BuiltinTypes.I8:
				case BuiltinTypes.U8:
				case BuiltinTypes.Char:
					size = 1;
					break;
				case BuiltinTypes.I16:
				case BuiltinTypes.U16:
				case BuiltinTypes.WChar:
					size = 2;
					break;
				case BuiltinTypes.I32:
				case BuiltinTypes.U32:
				case BuiltinTypes.ISize when !_isX64:
				case BuiltinTypes.USize when !_isX64:
				case BuiltinTypes.F32:
					size = 4;
					break;
				case BuiltinTypes.I64:
				case BuiltinTypes.U64:
				case BuiltinTypes.ISize when _isX64:
				case BuiltinTypes.USize when _isX64:
				case BuiltinTypes.F64:
				case BuiltinTypes.Rune:
					size = 8;
					break;
				}

				_stack.AddRange(new byte[size]);
				ILInterpMemory mem = new ILInterpMemory(_stack, (ulong)_offset, (ulong)size);
				_allocatedTypes.Push((type, new TypeInfo((byte)size, (ulong)size)));
				_offset += size;
				return mem;
			}
			case DelegateSymbolType delType:
				break;
			case EnumSymbolType enumType:
				break;
			case FunctionSymbolType funcType:
				break;
			case InterfaceSymbolType interfaceType:
				break;
			case MemoryLocSymbolType memoryLocType:
				break;
			case NullableSymbolType nullType:
				break;
			case PointerSymbolType ptrType:
				break;
			case ReferenceSymbolType refType:
				break;
			case TemplateInstanceSymbolType templateInstType:
				break;
			case TemplateParamSymbolType templateParamType:
				break;
			case TupleSymbolType tupType:
				break;
			case VariadicSymbolType varType:
				break;
			}

			return null;
		}

		public void Pop()
		{
			(SymbolType type, TypeInfo ti) = _allocatedTypes.Pop();
			_offset -= (int)ti.Size;
			_stack.RemoveRange(_stack.Count - (int)ti.Size, (int)ti.Size);
		}

	}

	public class ILInterpHeap
	{
		internal class SpaceData
		{
			internal ulong offset;
			internal ulong Size;

			internal bool Free;
			internal SpaceData Prev;
			internal SpaceData Next;
		}

		private List<byte> _data = new List<byte>();
		private Stack<(SymbolType, TypeInfo)> _allocatedTypes = new Stack<(SymbolType, TypeInfo)>();
		private List<SpaceData> FreeSpace;
		private List<SpaceData> UsedSpace;
		private int _offset;
		private bool _isX64;

		private ILCompUnit _compUnit;
		private CompilerContext _compilerContext;

		public ILInterpHeap(ILCompUnit compUnit, CompilerContext compilerContext)
		{
			_compUnit = compUnit;
			_compilerContext = compilerContext;

			FreeSpace.Add(new SpaceData{ Size = UInt64.MaxValue, Free = true });
		}

		public ILInterpMemory Allocate(ulong size)
		{
			SpaceData freeSpace = FreeSpace.Find(sd => { return sd.Size >= size; });

			SpaceData spaceDat = new SpaceData { offset = freeSpace.offset, Size = size };
			spaceDat.Prev = freeSpace.Prev;

			UsedSpace.Add(spaceDat);

			ulong offset = freeSpace.offset;
			if (freeSpace.Size == size)
			{
				FreeSpace.Remove(freeSpace);
				spaceDat.Next = freeSpace.Next;
			}
			else
			{
				freeSpace.Size -= size;
				freeSpace.offset += size;
				spaceDat.Next = freeSpace;
			}

			UsedSpace.Add(freeSpace);

			return new ILInterpMemory(_data, offset, size);
		}

		public void Free(ILInterpMemory mem)
		{
			SpaceData usedSpace = UsedSpace.Find(sd => { return sd.offset == mem.Offset; });

			if (usedSpace.Next != null && usedSpace.Next.Free)
			{
				usedSpace.Size += usedSpace.Next.Size;
				SpaceData next = usedSpace.Next;
				usedSpace.Next = next.Next;
				FreeSpace.Remove(next);
			}

			if (usedSpace.Prev != null && usedSpace.Prev.Free)
			{
				usedSpace.Prev.Size += usedSpace.Size;
			}
			else
			{
				usedSpace.Free = true;
				FreeSpace.Add(usedSpace);
			}
			
		}
	}


	/*
		switch (type)
		{
		case AggregateSymbolType aggrType:
			break;
		case ArraySymbolType arrType:
			break;
		case BitFieldSymbolType bitFieldType:
			break;
		case BuiltinSymbolType builtinType:
			break;
		case DelegateSymbolType delType:
			break;
		case EnumSymbolType enumType:
			break;
		case FunctionSymbolType funcType:
			break;
		case InterfaceSymbolType interfaceType:
			break;
		case MemoryLocSymbolType memoryLocType:
			break;
		case NullableSymbolType nullType:
			break;
		case PointerSymbolType ptrType:
			break;
		case ReferenceSymbolType refType:
			break;
		case TemplateInstanceSymbolType templateInstType:
			break;
		case TemplateParamSymbolType templateParamType:
			break;
		case TupleSymbolType tupType:
			break;
		case VariadicSymbolType varType:
			break;
		}
	 */
}
