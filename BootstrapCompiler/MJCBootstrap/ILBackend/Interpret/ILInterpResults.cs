﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.ILBackend.Interpret
{
	public class ILCompileInterpResult
	{
		public long BasicBlockId;
		public long InstructionId;

		public ILInstruction CompileResult;
	}
}
