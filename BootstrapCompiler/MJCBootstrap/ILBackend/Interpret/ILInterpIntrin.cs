﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.Interpret
{
	public enum FileError
	{
		None,
		Unknown,
		FileNotFound
	}

	public class ILInterpIntrin
	{
		private Dictionary<int, FileStream> _fileStreams = new Dictionary<int, FileStream>();
		private int _filestreamId;

		private ILInterpContext _context;

		public ILInterpIntrin(ILInterpContext context)
		{
			_context = context;
		}

		public ILInterpOperand Dispatch(string funcName, List<ILInterpOperand> ops, SymbolType retType)
		{
			switch (funcName)
			{
			case "OpenFile":
				return OpenFile(ops[0], retType);
			case "CloseFile":
				return CloseFile(ops[0]);
			case "Allocate":
				return Allocate(ops[0]);
			case "Free":
				Deallocate(ops[0]);
				return null;
			}

			return null;
		}

		public ILInterpOperand OpenFile(ILInterpOperand fileNameOp, SymbolType retType)
		{
			string filename = "";

			ulong handle;
			FileError error;

			switch (fileNameOp.Operand.Type)
			{
			case ArraySymbolType arrType:
			{
				BuiltinSymbolType builtinType = arrType.BaseType as BuiltinSymbolType;
				ILInterpMemory[] charArr = fileNameOp.Value.Array;
				foreach (ILInterpMemory c in charArr)
				{
					filename += c.Extract(builtinType, _context.IsX64).Byte;
				}
				break;
			}
			}

			try
			{
				FileStream stream = File.Open(" ", FileMode.Open);
				_fileStreams.Add(_filestreamId, stream);
				++_filestreamId;


				handle = UInt64.MaxValue;
				error = FileError.FileNotFound;

				
			}
			catch (FileNotFoundException)
			{
				handle = UInt64.MaxValue;
				error = FileError.FileNotFound;
			}
			catch (Exception _)
			{
				handle = UInt64.MaxValue;
				error = FileError.FileNotFound;
			}

			ILInterpMemory retMem = _context.Stack.Push(retType);
			retMem.Insert(_context.IsX64 ? SymbolType.BuiltinType(BuiltinTypes.U64) : SymbolType.BuiltinType(BuiltinTypes.U32) , _context.IsX64 ? (object)handle : (object)(uint)handle, _context.IsX64, 0);
			retMem.Insert(SymbolType.BuiltinType(BuiltinTypes.I32), (int)error, _context.IsX64, 1);	

			return new ILInterpOperand(null, new ILInterpValUnion{Memory = retMem});
		}


		public ILInterpOperand CloseFile(ILInterpOperand handleOp)
		{
			int id = handleOp.Value.Int;
			FileStream stream = _fileStreams[id];
			stream.Close();
			_fileStreams.Remove(id);

			return null;
		}

		public ILInterpOperand Allocate(ILInterpOperand sizeOp)
		{
			long size = sizeOp.Value.Long;
			ILInterpMemory mem = _context.Heap.Allocate((ulong) size);

			return new ILInterpOperand(null, new ILInterpValUnion { Memory = mem });
		}

		public void Deallocate(ILInterpOperand memOp)
		{
			_context.Heap.Free(memOp.Value.Memory);
		}






	}
}
