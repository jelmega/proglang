﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace MJC.ILBackend.Interpret
{
	public class ILInterpArray
	{
		public ILInterpMemory[] Mem;
		public ulong Size;

		public ILInterpArray(ILInterpMemory[] mem, ulong size)
		{
			Mem = mem;
			Size = size;
		}
	}

	// All values are little endian, so offsets are 0
	[StructLayout(LayoutKind.Explicit)]
	public class ILInterpValUnion
	{
		[FieldOffset(0)] public sbyte SByte;
		[FieldOffset(0)] public byte Byte;
		[FieldOffset(0)] public short Short;
		[FieldOffset(0)] public ushort UShort;
		[FieldOffset(0)] public int Int;
		[FieldOffset(0)] public uint UInt;
		[FieldOffset(0)] public long Long;
		[FieldOffset(0)] public ulong ULong;
		[FieldOffset(0)] public float Float;
		[FieldOffset(0)] public double Double;
		[FieldOffset(8)] public string String;
		//[FieldOffset(0)] public object Object;
		[FieldOffset(16)] public ILInterpMemory Memory;
		[FieldOffset(24)] public ILInterpMemory[] Array;
		[FieldOffset(32)] public ILFunction Function;
	}

	public class ILInterpOperand
	{
		public ILOperand Operand;

		public ILInterpValUnion Value;

		public ILInterpOperand(ILOperand operand, ILInterpValUnion value)
		{
			Operand = operand;
			Value = value;
		}
	}
}
