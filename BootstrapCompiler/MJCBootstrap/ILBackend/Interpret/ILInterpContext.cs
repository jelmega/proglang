﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.Interpret
{
	public class ILInterpContext
	{
		public ILInterpStack Stack;
		public ILInterpHeap Heap;


		public ILInterpIntrin Intrin;

		public ILCompUnit CompUnit;
		public CompilerContext CompContext;
		public bool IsX64;

		public ILInterpContext(ILCompUnit compUnit, CompilerContext compContext, bool isX64)
		{
			CompUnit = compUnit;
			CompContext = compContext;
			IsX64 = isX64;

			Stack = new ILInterpStack(CompUnit, CompContext);

			Intrin = new ILInterpIntrin(this);
		}

	}
}
