﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Interpret
{
	public enum ILInterpretType
	{
		None,
		Compile,
		Exec
	}

	public class ILInterpreter
	{
		private ILInterpretType _interpretType;

		public List<ILCompileInterpResult> CompileResults = null;
		public ILInterpContext Context;

		public ILInterpreter(ILInterpretType type)
		{
			_interpretType = type;

			switch (_interpretType)
			{
			case ILInterpretType.Compile:
			{
				CompileResults = new List<ILCompileInterpResult>();
				break;
			}
			}
		}

		public void Interpret(ILCompUnit compUnit)
		{
			Context = new ILInterpContext(compUnit, compUnit.Context.CompilerContext, CmdLine.IsX64);

			if (_interpretType == ILInterpretType.Compile)
			{
				if (compUnit.Stage != ILStage.CompExec || compUnit.Stage != ILStage.Opt)
				{
					InterpCompile(compUnit);
				}
			}
			else if (_interpretType == ILInterpretType.Exec)
			{
				// Interpret IL
				InterpExec(compUnit);
			}

		}

		void InterpCompile(ILCompUnit compUnit)
		{
			// Update enum values
			foreach (KeyValuePair<string, ILEnum> pair in compUnit.Enums)
			{
				foreach (ILEnumMember member in pair.Value.Members)
				{
					string funcName = member.EnumInit;
					ILFunction func = compUnit.GetFunction(funcName);
					ILInterpOperand retOp = InterpFunction(func, null, compUnit, Context);
					member.EnumVal = retOp.Value.Long;
				}
			}

			// Update compile time globals
			foreach (KeyValuePair<string, ILGlobal> pair in compUnit.Globals)
			{
				ILGlobal ilGlobal = pair.Value;
				if ((ilGlobal.Attributes.GlobalFlags & ILGlobalFlags.CompileConst) != 0)
				{
					string funcName = ilGlobal.Initializer;
					ILFunction func = compUnit.GetFunction(funcName);
					ILInterpOperand retOp = InterpFunction(func, null, compUnit, Context);

					if (retOp.Operand.Type is BuiltinSymbolType builtin)
					{
						switch (builtin.Builtin)
						{
						case BuiltinTypes.Unkown:
							break;
						case BuiltinTypes.Void:
							break;
						case BuiltinTypes.Bool:
						case BuiltinTypes.I8:
						case BuiltinTypes.I16:
						case BuiltinTypes.I32:
						case BuiltinTypes.I64:
						case BuiltinTypes.ISize:
						case BuiltinTypes.U8:
						case BuiltinTypes.U16:
						case BuiltinTypes.U32:
						case BuiltinTypes.U64:
						case BuiltinTypes.USize:
						case BuiltinTypes.Char:
						case BuiltinTypes.WChar:
						case BuiltinTypes.Rune:
						{
							ilGlobal.Value = retOp.Value.Long.ToString();
							break;
						}
						case BuiltinTypes.F32:
						{
							int hexVal = BitConverter.SingleToInt32Bits(retOp.Value.Float);
							ilGlobal.Value = $"0x{hexVal:x8}";
							break;
						}
						case BuiltinTypes.F64:
						{
							long hexVal = BitConverter.DoubleToInt64Bits(retOp.Value.Double);
							ilGlobal.Value = $"0x{hexVal:x16}";
							break;
						}
						case BuiltinTypes.String:
							break;
						case BuiltinTypes.StringLiteral:
						{
							string str = retOp.Value.String;
							ilGlobal.Value = '"' + str + '"';
							break;
						}
						case BuiltinTypes.Null:
							break;
						case BuiltinTypes.Count:
							break;
						default:
							throw new ArgumentOutOfRangeException();
						}
					}
					else if (retOp.Operand.Type is PointerSymbolType ptrType)
					{
						long longVal = retOp.Value.Long;
						ilGlobal.Value = longVal.ToString();
					}
					else if (retOp.Operand.Type is EnumSymbolType enumType && enumType.IsSimpleEnum)
					{
						long longVal = retOp.Value.Long;
						ilGlobal.Value = longVal.ToString();
					}
				}
			}

			// Replace compile time only instructions
			foreach (KeyValuePair<string, ILFunction> pair in compUnit.Functions)
			{
				if (pair.Value.IsUninstantiated)
					continue;

				foreach (ILBasicBlock block in pair.Value.Blocks)
				{
					ReplaceCompileTimeOnlyInstructions(block, compUnit);
				}
			}

		}

		void InterpExec(ILCompUnit compUnit)
		{
			// Find _start and interpret it
			ILFunction start = compUnit.GetFunction("_start");
			InterpFunction(start, null, compUnit, Context);
		}

		public static ILInterpOperand InterpFunction(ILFunction function, List<ILInterpOperand> args, ILCompUnit compUnit, ILInterpContext context)
		{
			Dictionary<string, ILInterpOperand> opMapping = new Dictionary<string, ILInterpOperand>();
			if (args != null)
			{
				for (var i = 0; i < args.Count; i++)
				{
					ILInterpOperand arg = args[i];
					opMapping.Add(i.ToString(), arg);
				}
			}

			ILInterpOperand lastOp = null;
			string nextBlock;
			ILBasicBlock block = function.Blocks[0];
			
			while (block != null)
			{
				(lastOp, nextBlock) = InterpBasicBlock(block, opMapping, compUnit, context);

				block = null;
				foreach (ILBasicBlock basicBlock in function.Blocks)
				{
					if (basicBlock.Label == nextBlock)
					{
						block = basicBlock;
						break;
					}
				}
			}

			return lastOp;
		}

		static (ILInterpOperand, string) InterpBasicBlock(ILBasicBlock block, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit, ILInterpContext context)
		{
			ILInterpOperand lastOp = null;
			string blockName = null;
			for (var i = 0; i < block.Instructions.Count; i++)
			{
				ILInstruction instruction = block.Instructions[i];
				(ILInterpOperand tmp, string tmpBlock) = ILInterpInstruction.InterpInstruction(instruction, compUnit, context, opMapping);

				if (tmp != null)
					lastOp = tmp;
				if (tmpBlock != null)
					blockName = tmpBlock;
			}
			
			return (lastOp, blockName);
		}

		void ReplaceCompileTimeOnlyInstructions(ILBasicBlock block, ILCompUnit compUnit)
		{
			for (var i = 0; i < block.Instructions.Count; i++)
			{
				ILInstruction instruction = block.Instructions[i];
				if (instruction.IsCompileTimeOnly())
				{
					(ILInterpOperand tmp, _) = ILInterpInstruction.InterpInstruction(instruction, compUnit, Context, null);

					BuiltinSymbolType builtinType = tmp.Operand.Type as BuiltinSymbolType;
					switch (builtinType.Builtin)
					{
					case BuiltinTypes.I8:
					case BuiltinTypes.I16:
					case BuiltinTypes.I32:
					case BuiltinTypes.I64:
					case BuiltinTypes.U8:
					case BuiltinTypes.U16:
					case BuiltinTypes.U32:
					case BuiltinTypes.U64:
					case BuiltinTypes.Char:
					case BuiltinTypes.WChar:
					case BuiltinTypes.Rune:
					{
						ILIntLiteralInstruction instr = new ILIntLiteralInstruction(tmp.Operand, tmp.Value.Long, instruction.Span);
						block.Instructions[i] = instr;
						break;
					}
					case BuiltinTypes.F32:
					{
						int hexVal = BitConverter.SingleToInt32Bits(tmp.Value.Float);
						ILFloatLiteralInstruction instr = new ILFloatLiteralInstruction(tmp.Operand, hexVal, instruction.Span);
						block.Instructions[i] = instr;
						break;
					}
					case BuiltinTypes.F64:
					{
						long hexVal = BitConverter.DoubleToInt64Bits(tmp.Value.Double);
						ILFloatLiteralInstruction instr = new ILFloatLiteralInstruction(tmp.Operand, hexVal, instruction.Span);
						block.Instructions[i] = instr;
						break;
					}
					case BuiltinTypes.String:
					{
						string val = tmp.Value.String;
						ILStringLiteralInstruction instr = new ILStringLiteralInstruction(tmp.Operand, val, instruction.Span);
						block.Instructions[i] = instr;
						break;
					}
					}
				}
			}
		}

	}
}
