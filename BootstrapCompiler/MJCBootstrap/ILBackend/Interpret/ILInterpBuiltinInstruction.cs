﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.Interpret
{
	public static partial class ILInterpInstruction
	{

		static ILInterpOperand InterpBuiltin(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			string[] builtin = instruction.Builtin.Split('.');
			switch (builtin[0])
			{
			case "neg":		return InterpBuiltinNeg		(instruction, opMapping, isX64);
			case "compl":	return InterpBuiltinCompl	(instruction, opMapping, isX64);
			case "inc":		return InterpBuiltinInc		(instruction, opMapping, isX64);
			case "dec":		return InterpBuiltinDec		(instruction, opMapping, isX64);
			case "add":		return InterpBuiltinAdd		(instruction, opMapping, isX64);
			case "sub":		return InterpBuiltinSub		(instruction, opMapping, isX64);
			case "mul":		return InterpBuiltinMul		(instruction, opMapping, isX64);
			case "div":		return InterpBuiltinDiv		(instruction, opMapping, isX64);
			case "rem":		return InterpBuiltinRem		(instruction, opMapping, isX64);
			case "shift":	return InterpBuiltinShift	(instruction, opMapping, isX64);
			case "or":		return InterpBuiltinOr		(instruction, opMapping, isX64);
			case "xor":		return InterpBuiltinXor		(instruction, opMapping, isX64);
			case "and":		return InterpBuiltinAnd		(instruction, opMapping, isX64);
			case "ext":		return InterpBuiltinExt		(instruction, opMapping, isX64);
			case "trunc":	return InterpBuiltinTrunc	(instruction, opMapping, isX64);
			case "fptoi":	return InterpBuiltinFptoi	(instruction, opMapping, isX64);
			case "itofp":	return InterpBuiltinItofp	(instruction, opMapping, isX64);
			case "ptrtoi":	return InterpBuiltinPtrtoi	(instruction, opMapping, isX64);
			case "itoptr":	return InterpBuiltinItoptr	(instruction, opMapping, isX64);
			case "bitcast":	return InterpBuiltinBitCast	(instruction, opMapping, isX64);
			case "cmp":		return InterpBuiltinCmp		(instruction, opMapping, isX64);
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinNeg(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				{
					long val = src.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, -val, isX64));
					break;
				}
				case BuiltinTypes.F32:
				{
					float val = -src.Value.Float;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion{Float = val});
					break;
				}
				case BuiltinTypes.F64:
				{
					double val = -src.Value.Double;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion{Double = val});
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinCompl(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, ~val, isX64));
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinInc(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val + 1, isX64));
					break;
				}
				case BuiltinTypes.F32:
				{
					float val = src.Value.Float + 1;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = val });
					break;
				}
				case BuiltinTypes.F64:
				{
					double val = src.Value.Double + 1;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = val });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinDec(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val - 1, isX64));
					break;
				}
				case BuiltinTypes.F32:
				{
					float val = src.Value.Float - 1;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = val });
					break;
				}
				case BuiltinTypes.F64:
				{
					double val = src.Value.Double - 1;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = val });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinAdd(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Operands[0];
			ILOperand op1 = instruction.Operands[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src0.Value.Long + src1.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val, isX64));
					break;
				}
				case BuiltinTypes.F32:
				{
					float val = src0.Value.Float + src1.Value.Float;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = val });
					break;
				}
				case BuiltinTypes.F64:
				{
					double val = src0.Value.Double + src1.Value.Double;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = val });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinSub(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Operands[0];
			ILOperand op1 = instruction.Operands[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src0.Value.Long - src1.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val, isX64));
					break;
				}
				case BuiltinTypes.F32:
				{
					float val = src0.Value.Float - src1.Value.Float;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = val });
					break;
				}
				case BuiltinTypes.F64:
				{
					double val = src0.Value.Double - src1.Value.Double;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = val });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinMul(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Operands[0];
			ILOperand op1 = instruction.Operands[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src0.Value.Long * src1.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val, isX64));
					break;
				}
				case BuiltinTypes.F32:
				{
					float val = src0.Value.Float * src1.Value.Float;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = val });
					break;
				}
				case BuiltinTypes.F64:
				{
					double val = src0.Value.Double * src1.Value.Double;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = val });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinDiv(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Operands[0];
			ILOperand op1 = instruction.Operands[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src0.Value.Long / src1.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val, isX64));
					break;
				}
				case BuiltinTypes.F32:
				{
					float val = src0.Value.Float / src1.Value.Float;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = val });
					break;
				}
				case BuiltinTypes.F64:
				{
					double val = src0.Value.Double / src1.Value.Double;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = val });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinRem(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Operands[0];
			ILOperand op1 = instruction.Operands[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src0.Value.Long % src1.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val, isX64));
					break;
				}
				case BuiltinTypes.F32:
				{
					float val = src0.Value.Float % src1.Value.Float;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = val });
					break;
				}
				case BuiltinTypes.F64:
				{
					double val = src0.Value.Double % src1.Value.Double;
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = val });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinShift(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Operands[0];
			ILOperand op1 = instruction.Operands[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			string[] parts = instruction.Builtin.Split('.');
			string dir = parts[1];

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val0 = src0.Value.Long;
					long val1 = src1.Value.Long;

					long val = 0;
					switch (dir)
					{
					case "l":
					{
						val = val0 << (int)val1;
						break;
					}
					case "lr":
					{
						val = val0 >> (int)val1;
						break;
					}
					case "ar":
					{
						val = val0 >> (int)val1;
						break;
					}
					}

					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val, isX64));
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinOr(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Operands[0];
			ILOperand op1 = instruction.Operands[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src0.Value.Long | src1.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val, isX64));
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinXor(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Operands[0];
			ILOperand op1 = instruction.Operands[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src0.Value.Long ^ src1.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val, isX64));
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinAnd(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Operands[0];
			ILOperand op1 = instruction.Operands[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src0.Value.Long & src1.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, val, isX64));
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinExt(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;
			bool zeroExtend = instruction.Builtin.Contains("z");

			if (instruction.Operands[0].Type is BuiltinSymbolType builtinType &&
			    instruction.RetOperand.Type is BuiltinSymbolType retBuiltinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					sbyte? srcVal = src.Value.SByte;
					long val = srcVal.Value;
					if (zeroExtend && val < 0)
					{
						val = val & 0x7f;
						val |= 0x80;
					}
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(retBuiltinType, val, isX64));
					break;
				}
				case BuiltinTypes.I16:
				{
					short? srcVal = src.Value.Short;
					long val = srcVal.Value;
					if (zeroExtend && val < 0)
					{
						val = val & 0x7fff;
						val |= 0x8000;
					}
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(retBuiltinType, val, isX64));
					break;
				}
				case BuiltinTypes.I32:
				case BuiltinTypes.ISize when !isX64:
				{
					int? srcVal = src.Value.Int;
					long val = srcVal.Value;
					if (zeroExtend && val < 0)
					{
						val = val & 0x7fff_ffff;
						val |= 0x8000_0000;
					}
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(retBuiltinType, val, isX64));
					break;
				}
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize when isX64:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(retBuiltinType, val, isX64));
					break;
				}
				case BuiltinTypes.F32:
				{
					if (retBuiltinType.Builtin == BuiltinTypes.F32)
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = src.Value.Float });
					else
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = src.Value.Float });
					break;
				}
				case BuiltinTypes.F64:
				{
					if (retBuiltinType.Builtin == BuiltinTypes.F32)
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = (float)src.Value.Double });
					else
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = src.Value.Double });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinTrunc(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.Operands[0].Type is BuiltinSymbolType builtinType &&
			    instruction.RetOperand.Type is BuiltinSymbolType retBuiltinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				{
					long val = src.Value.Long;
					ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(retBuiltinType, val, isX64));
					break;
				}
				case BuiltinTypes.F32:
				{
					if (retBuiltinType.Builtin == BuiltinTypes.F32)
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = src.Value.Float });
					else
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = src.Value.Float });
					break;
				}
				case BuiltinTypes.F64:
				{
					if (retBuiltinType.Builtin == BuiltinTypes.F32)
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = (float)src.Value.Double });
					else
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = src.Value.Double });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinFptoi(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I32:
				case BuiltinTypes.ISize when !isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Int = (int)src.Value.Float });
					break;
				}
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize when isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Long = (long)src.Value.Double });
					break;
				}
				case BuiltinTypes.U32:
				case BuiltinTypes.USize when !isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { UInt = (uint)src.Value.Float });
					break;
				}
				case BuiltinTypes.U64:
				case BuiltinTypes.USize when isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { ULong = (ulong)src.Value.Double });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinItofp(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.F32:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = src.Value.Long });
					break;
				}
				case BuiltinTypes.F64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = src.Value.Long });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinPtrtoi(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				ulong addr = src.Value.ULong;

				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I32:
				case BuiltinTypes.ISize when !isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Int = (int)addr });
					break;
				}
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize when isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Long = (long)addr });
					break;
				}
				case BuiltinTypes.U32:
				case BuiltinTypes.USize when !isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { UInt = (uint)addr });
					break;
				}
				case BuiltinTypes.U64:
				case BuiltinTypes.USize when isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { ULong = (ulong)addr });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinItoptr(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (op.Type is BuiltinSymbolType builtinType)
			{
				ulong addr = src.Value.ULong;
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I32:
				case BuiltinTypes.ISize when !isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { UInt = (uint)addr });
					break;
				}
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize when isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { ULong = addr });
					break;
				}
				case BuiltinTypes.U32:
				case BuiltinTypes.USize when !isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { UInt = (uint)addr });
					break;
				}
				case BuiltinTypes.U64:
				case BuiltinTypes.USize when isX64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { ULong = addr });
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinBitCast(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Operands[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.Operands[0].Type is BuiltinSymbolType fromBuiltin &&
			    instruction.RetOperand.Type is BuiltinSymbolType toBuiltin)
			{
				switch (fromBuiltin.Builtin)
				{
				case BuiltinTypes.I8:
				{
					if (toBuiltin.Builtin == BuiltinTypes.I8)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Byte = src.Value.Byte });
					}
					break;
				}
				case BuiltinTypes.I16:
				{
					if (toBuiltin.Builtin == BuiltinTypes.I16)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { UShort = src.Value.UShort });
					}
					break;
				}
				case BuiltinTypes.ISize when !isX64:
				case BuiltinTypes.I32:
				{
					if (toBuiltin.Builtin == BuiltinTypes.I32 ||
					    toBuiltin.Builtin == BuiltinTypes.ISize)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else if (toBuiltin.Builtin == BuiltinTypes.F32)
					{
						float val = BitConverter.Int32BitsToSingle(src.Value.Int);
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = val });
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { UInt = src.Value.UInt });
					}
					break;
				}
				case BuiltinTypes.ISize when isX64:
				case BuiltinTypes.I64:
				{
					if (toBuiltin.Builtin == BuiltinTypes.I64 ||
					    toBuiltin.Builtin == BuiltinTypes.ISize)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else if (toBuiltin.Builtin == BuiltinTypes.F64)
					{
						double val = BitConverter.Int64BitsToDouble(src.Value.Long);
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = val });
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { ULong = src.Value.ULong });
					}
					break;
				}
				case BuiltinTypes.Bool:
				case BuiltinTypes.U8:
				{
					if (toBuiltin.Builtin == BuiltinTypes.U8 || 
					    toBuiltin.Builtin == BuiltinTypes.Bool)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { SByte = src.Value.SByte });
					}
					break;
				}
				case BuiltinTypes.U16:
				{
					if (toBuiltin.Builtin == BuiltinTypes.U16)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Short = src.Value.Short });
					}
					break;
				}
				case BuiltinTypes.USize when !isX64:
				case BuiltinTypes.U32:
				{
					if (toBuiltin.Builtin == BuiltinTypes.U32 ||
					    toBuiltin.Builtin == BuiltinTypes.USize)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else if (toBuiltin.Builtin == BuiltinTypes.F32)
					{
						float val = BitConverter.Int32BitsToSingle(src.Value.Int);
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Float = val });
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Int = src.Value.Int });
					}
					break;
				}
				case BuiltinTypes.USize when isX64:
				case BuiltinTypes.U64:
				{
					if (toBuiltin.Builtin == BuiltinTypes.U64 ||
					    toBuiltin.Builtin == BuiltinTypes.USize)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else if (toBuiltin.Builtin == BuiltinTypes.F64)
					{
						double val = BitConverter.Int64BitsToDouble(src.Value.Long);
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = val });
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Long = src.Value.Long });
					}
					break;
				}
				case BuiltinTypes.F32:
				{
					int val = BitConverter.SingleToInt32Bits(src.Value.Float);
					if (toBuiltin.Builtin == BuiltinTypes.I32)
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Int = val });
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { UInt = (uint)val });
					}
					break;
				}
				case BuiltinTypes.F64:
				{
					long val = BitConverter.DoubleToInt64Bits(src.Value.Double);
					if (toBuiltin.Builtin == BuiltinTypes.I32)
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Long = val });
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { ULong = (ulong)val });
					}
					break;
				}
				}
			}
			else if (instruction.Operands[0].Type is PointerSymbolType &&
			         instruction.RetOperand.Type is PointerSymbolType)
			{
				return new ILInterpOperand(instruction.RetOperand, src.Value);
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinCmp(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Operands[0];
			ILOperand op1 = instruction.Operands[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			bool bRes = false;

			string opName = instruction.Builtin.Split('.')[1];

			if (op0.Type is BuiltinSymbolType builtin)
			{
				switch (builtin.Builtin)
				{
				case BuiltinTypes.Bool:
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				case BuiltinTypes.Char:
				case BuiltinTypes.WChar:
				case BuiltinTypes.Rune:
				{
					long src0Val = src0.Value.Long;
					long src1Val = src1.Value.Long;

					switch (opName)
					{
					case "eq":
						bRes = src0Val == src1Val;
						break;
					case "ne":
						bRes = src0Val != src1Val;
						break;
					case "gt":
						bRes = src0Val > src1Val;
						break;
					case "gte":
						bRes = src0Val >= src1Val;
						break;
					case "lt":
						bRes = src0Val < src1Val;
						break;
					case "lte":
						bRes = src0Val <= src1Val;
						break;
					}
					break;
				}
				case BuiltinTypes.F32:
				{
					float src0Val = src0.Value.Float;
					float src1Val = src1.Value.Float;

					switch (opName)
					{
					case "eq":
						bRes = src0Val == src1Val;
						break;
					case "ne":
						bRes = src0Val != src1Val;
						break;
					case "gt":
						bRes = src0Val > src1Val;
						break;
					case "gte":
						bRes = src0Val >= src1Val;
						break;
					case "lt":
						bRes = src0Val < src1Val;
						break;
					case "lte":
						bRes = src0Val <= src1Val;
						break;
					}
					break;
				}
				case BuiltinTypes.F64:
				{
					double src0Val = src0.Value.Double;
					double src1Val = src1.Value.Double;

					switch (opName)
					{
					case "eq":
						bRes = src0Val == src1Val;
						break;
					case "ne":
						bRes = src0Val != src1Val;
						break;
					case "gt":
						bRes = src0Val > src1Val;
						break;
					case "gte":
						bRes = src0Val >= src1Val;
						break;
					case "lt":
						bRes = src0Val < src1Val;
						break;
					case "lte":
						bRes = src0Val <= src1Val;
						break;
					}
					break;
				}
				}
			}
			else if (op0.Type is PointerSymbolType)
			{
				ulong src0Val = src0.Value.ULong;
				ulong src1Val = src1.Value.ULong;
				switch (opName)
				{
				case "eq":
					bRes = src0Val == src1Val;
					break;
				case "ne":
					bRes = src0Val != src1Val;
					break;
				case "gt":
					bRes = src0Val > src1Val;
					break;
				case "gte":
					bRes = src0Val >= src1Val;
					break;
				case "lt":
					bRes = src0Val < src1Val;
					break;
				case "lte":
					bRes = src0Val <= src1Val;
					break;
				}
			}

			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion{Int = bRes ? 1 : 0});
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return null;
		}
	}
}
