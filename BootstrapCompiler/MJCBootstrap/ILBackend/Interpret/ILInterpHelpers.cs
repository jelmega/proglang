﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.Interpret
{
	public static class ILInterpHelpers
	{
		public static (ulong, TypeInfo) GetElemOffsetAndInfo(ILAggregate aggr, ILReference member, CompilerContext context)
		{
			ulong offset = 0;

			foreach (ILAggregateVariable variable in aggr.Variables)
			{
				TypeInfo ti = context.GetTypeInfo(variable.Type);

				if (variable.Identifier.GetSimpleName() == member.Element)
					return (offset, ti);

				offset += ti.Size;
			}

			return (UInt64.MaxValue, null);
		}

		public static (ulong, TypeInfo) GetElemOffsetAndInfo(TupleSymbolType tupType, int index, CompilerContext context)
		{
			ulong offset = 0;

			for (var i = 0; i < tupType.SubTypes.Count; i++)
			{
				SymbolType type = tupType.SubTypes[i];
				TypeInfo ti = context.GetTypeInfo(type);

				if (i == index)
					return (offset, ti);

				offset += ti.Size;
			}

			return (UInt64.MaxValue, null);
		}

		public static ILInterpValUnion CastInteger(BuiltinSymbolType builtin, long value, bool isX64)
		{
			ILInterpValUnion valUnion = new ILInterpValUnion();
			switch (builtin.Builtin)
			{
			case BuiltinTypes.I8:
				valUnion.SByte = (sbyte)value;
				break;
			case BuiltinTypes.I16:
				valUnion.Short = (short)value;
				break;
			case BuiltinTypes.I32:
			case BuiltinTypes.ISize when !isX64:
				valUnion.Int = (int)value;
				break;
			case BuiltinTypes.I64:
			case BuiltinTypes.ISize when isX64:
				valUnion.Long = (long)value;
				break;
			case BuiltinTypes.Bool:
			case BuiltinTypes.U8:
			case BuiltinTypes.Char:
				valUnion.Byte = (byte)value;
				break;
			case BuiltinTypes.U16:
			case BuiltinTypes.WChar:
				valUnion.UShort = (ushort)value;
				break;
			case BuiltinTypes.U32:
			case BuiltinTypes.USize when !isX64:
			case BuiltinTypes.Rune:
				valUnion.UInt = (uint)value;
				break;
			case BuiltinTypes.U64:
			case BuiltinTypes.USize when isX64:
				valUnion.ULong = (ulong)value;
				break;
			}
			return valUnion;
		}

		public static byte[] CastIntegerToByteArr(BuiltinSymbolType builtin, long value, bool isX64)
		{
			switch (builtin.Builtin)
			{
			case BuiltinTypes.I8:
				return BitConverter.GetBytes((sbyte)value);
			case BuiltinTypes.I16:
				return BitConverter.GetBytes((short)value);
			case BuiltinTypes.I32:
			case BuiltinTypes.ISize when !isX64:
				return BitConverter.GetBytes((int)value);
			case BuiltinTypes.I64:
			case BuiltinTypes.ISize when isX64:
				return BitConverter.GetBytes((long)value);
			case BuiltinTypes.Bool:
			case BuiltinTypes.U8:
			case BuiltinTypes.Char:
				return BitConverter.GetBytes((byte)value);
			case BuiltinTypes.U16:
			case BuiltinTypes.WChar:
				return BitConverter.GetBytes((ushort)value);
			case BuiltinTypes.U32:
			case BuiltinTypes.USize when !isX64:
			case BuiltinTypes.Rune:
				return BitConverter.GetBytes((uint)value);
			case BuiltinTypes.U64:
			case BuiltinTypes.USize when isX64:
				return BitConverter.GetBytes((ulong)value);
			default:
				return null;
			}
		}
	}
}
