﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Interpret
{
	public static partial class ILInterpInstruction
	{

		public static (ILInterpOperand, string) InterpInstruction(ILInstruction instruction, ILCompUnit compUnit, ILInterpContext context, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand ret = null;
			string nextBlock = null;

			switch (instruction.InstructionType)
			{
			case ILInstructionType.IntLiteral:			ret = InterpIntLiteral(instruction as ILIntLiteralInstruction, opMapping, context.IsX64); break;
			case ILInstructionType.FloatLiteral:		ret = InterpFloatLiteral(instruction as ILFloatLiteralInstruction, opMapping); break;
			case ILInstructionType.StringLiteral:		ret = InterpStringLiteral(instruction as ILStringLiteralInstruction, opMapping); break;
			case ILInstructionType.AllocStack:			ret = InterpAllocStack(instruction as ILAllocStackInstruction, opMapping, context);	break;
			case ILInstructionType.DeallocStack:		ret = null;	break;
			case ILInstructionType.Assign:				ret = null;	break;
			case ILInstructionType.Store:				InterpStore(instruction as ILStoreInstruction, opMapping, context.IsX64);	break;
			case ILInstructionType.Load:				ret = InterpLoad(instruction as ILLoadInstruction, opMapping, context.IsX64);	break;
			case ILInstructionType.OperatorRef:			ret = InterpOperatorRef(instruction as ILOperatorRefInstruction, opMapping, compUnit); break;
			case ILInstructionType.FunctionRef:			ret = InterpFunctionRef(instruction as ILFunctionRefInstruction, opMapping, compUnit); break;
			case ILInstructionType.MethodRef:			ret = null; break;
			case ILInstructionType.Call:				ret = InterpCall(instruction as ILCallInstruction, opMapping, compUnit, context); break;
			case ILInstructionType.Builtin:				ret = InterpBuiltin(instruction as ILBuiltinInstruction, opMapping, context.IsX64); break;
			case ILInstructionType.TupleUnpack:			ret = null;	break;
			case ILInstructionType.TuplePack:			ret = null;	break;
			case ILInstructionType.TupleInsert:			ret = null;	break;
			case ILInstructionType.TupleExtract:		ret = null;	break;
			case ILInstructionType.TupleElemAddr:		ret = null;	break;
			case ILInstructionType.StructInsert:		ret = null; break; //InterpStructInsert(instruction as ILStructInsertInstruction, opMapping, compUnit, context);	break;
			case ILInstructionType.StructExtract:		ret = null; break; //ret = InterpStructExtract(instruction as ILStructExtractInstruction, opMapping, compUnit, context);	break;
			case ILInstructionType.StructElemAddr:		ret = InterpStructElemAddr(instruction as ILStructElemAddrInstruction, opMapping, compUnit, context);	break;
			case ILInstructionType.TemplateGetValue:	ret = InterpTemplateGetValue(instruction as ILTemplateGetValueInstruction, compUnit); break;
			case ILInstructionType.Destruct:			ret = null; break;
			case ILInstructionType.EnumGetMemberValue:	ret = InterpEnumGetMemberValue(instruction as ILEnumGetMemberValueInstruction, compUnit, opMapping); break;
			case ILInstructionType.ConvType:			ret = InterpConvType(instruction as ILConvertTypeInstruction, opMapping); break;
			case ILInstructionType.GetVTable:			ret = null; break;
			case ILInstructionType.Return:				ret = InterpReturn(instruction as ILReturnInstruction, opMapping); break;
			case ILInstructionType.Branch:				nextBlock = InterpBranch(instruction as ILBranchInstruction);	break;
			case ILInstructionType.CondBranch:			nextBlock = InterpCondBranch(instruction as ILCondBranchInstruction, opMapping); break;
			}

			return (ret, nextBlock);
		}

		static ILInterpOperand InterpIntLiteral(ILIntLiteralInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILInterpOperand ret;
			BuiltinSymbolType builtinType = instruction.RetOperand.Type as BuiltinSymbolType;
			ret = new ILInterpOperand(instruction.RetOperand, ILInterpHelpers.CastInteger(builtinType, instruction.Value, isX64));
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpFloatLiteral(ILFloatLiteralInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand ret;
			if ((instruction.RetOperand.Type as BuiltinSymbolType).Builtin == BuiltinTypes.F32)
			{
				float floatVal = BitConverter.Int32BitsToSingle((int)instruction.HexValue);
				ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion{Float = floatVal});
			}
			else
			{
				double floatVal = BitConverter.Int64BitsToDouble(instruction.HexValue);
				ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion { Double = floatVal });
			}
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpStringLiteral(ILStringLiteralInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion{String = instruction.String});
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpAllocStack(ILAllocStackInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILInterpContext context)
		{
			ILInterpMemory mem = context.Stack.Push(instruction.Type);
			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion {Memory = mem});
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static void InterpDeallocStack(ILDeallocStackInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILInterpContext context)
		{
			context.Stack.Pop();

		}

		static void InterpStore(ILStoreInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILInterpOperand dst = opMapping[instruction.Dst.Iden];
			ILInterpOperand src = opMapping[instruction.Src.Iden];
			dst.Value.Memory.Insert(src.Operand.Type, src.Value, isX64);
		}

		static ILInterpOperand InterpLoad(ILLoadInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILInterpOperand src = opMapping[instruction.Src.Iden];
			ILInterpMemory srcMem = src.Value.Memory;

			ILInterpValUnion val = srcMem.Extract(instruction.RetOperand.Type, isX64);
			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, val);
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpFunctionRef(ILFunctionRefInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit)
		{
			string opName = instruction.MangledName;

			// 1. Lookup builtin function
			ILFunction function = compUnit.Builtins.GetFunction(opName);
			// 2. If no function was found, lookup local function
			if (function == null)
			{
				function = compUnit.GetFunction(opName);
			}
			// 4. If no function was found, lookup function in other module compile units
			if (function == null)
			{
				foreach (ILCompUnit refCompUnit in compUnit.ModuleCompUnits)
				{
					function = refCompUnit.GetFunction(opName);
					if (function != null)
						break;
				}
			}
			// 4. If no function was found, we can't handle binary formats
			if (function == null)
			{
				// TODO: Error
			}

			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion{Function = function});
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpOperatorRef(ILOperatorRefInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit)
		{
			string opName = ILHelpers.GetMangledIlOpName(instruction);

			// TODO: will lickely cause issues, since opname is 'func(params...):ret'
			// 1. Lookup builtin operator
			ILFunction op = compUnit.Builtins.GetFunction(opName);
			// 2. If no op was found, lookup local operator
			if (op == null)
			{
				op = compUnit.GetFunction(opName);
			}
			// 4. If no op was found, lookup operator in other module compile units
			if (op == null)
			{
				foreach (ILCompUnit refCompUnit in compUnit.ModuleCompUnits)
				{
					op = refCompUnit.GetFunction(opName);
					if (op != null)
						break;
				}
			}
			// 4. If no operator was found, we can't handle binary formats
			if (op == null)
			{
				// TODO: Error
			}

			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion{ Function = op });
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpCall(ILCallInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit, ILInterpContext context)
		{
			ILInterpOperand callee = opMapping[instruction.Func.Iden];

			List<ILInterpOperand> args = null;
			if (instruction.Operands != null)
			{
				args = new List<ILInterpOperand>();
				foreach (ILOperand param in instruction.Operands)
				{
					ILInterpOperand interpOp = opMapping[param.Iden];
					args.Add(interpOp);
				}
			}

			ILFunction function = callee.Value.Function;

			ILInterpOperand ret;
			if (function.Attributes.InterpFunc != null)
			{
				ret = context.Intrin.Dispatch(function.Attributes.InterpFunc, args, function.FuncType.ReturnType);
			}
			else
			{
				ret = ILInterpreter.InterpFunction(function, args, compUnit, context);
			}

			if (ret != null)
				ret = new ILInterpOperand(instruction.RetOperand, ret.Value);

			if (instruction.RetOperand != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}
			return null;
		}

		/*static void InterpStructInsert(ILStructInsertInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit, CompilerContext context)
		{
			AggregateSymbolType aggrType = instruction.Struct.Type as AggregateSymbolType;
			ILAggregate aggregate = compUnit.GetAggregate(aggrType.Identifier);

			(ulong offset, _) = ILInterpHelpers.GetElemOffsetAndInfo(aggregate, instruction.ElemRef, context);

			byte[] data = opMapping[instruction.Struct.Iden].Value as byte[];
			//return new ILInterpOperand(instruction.RetOperand, new ILInterpMemory(data, offset));
		}

		static ILInterpOperand InterpStructExtract(ILStructExtractInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit, CompilerContext context)
		{
			AggregateSymbolType aggrType = instruction.Struct.Type as AggregateSymbolType;
			ILAggregate aggregate = compUnit.GetAggregate(aggrType.Identifier);

			(ulong offset, _) = ILInterpHelpers.GetElemOffsetAndInfo(aggregate, instruction.ElemRef, context);

			ILInterpMemory data = opMapping[instruction.Struct.Iden].Value as ILInterpMemory;
			return new ILInterpOperand(instruction.RetOperand, new ILInterpMemory(data.Data, offset, instruction.RetOperand.Type));
		}*/

		static ILInterpOperand InterpStructElemAddr(ILStructElemAddrInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit, ILInterpContext context)
		{
			if (instruction.Struct.Type is MemoryLocSymbolType memType)
			{
				AggregateSymbolType aggrType = memType.BaseType as AggregateSymbolType;
				ILAggregate aggregate = compUnit.GetAggregate(aggrType.Identifier);

				(ulong offset, TypeInfo ti) = ILInterpHelpers.GetElemOffsetAndInfo(aggregate, instruction.ElemRef, context.CompContext);

				ILInterpMemory data = opMapping[instruction.Struct.Iden].Value.Memory;
				return new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion{ Memory = new ILInterpMemory(data.Data, offset, ti.Size) });
			}
			else
			{
				// TODO: Heap
				return null;
			}
		}

		static ILInterpOperand InterpTemplateGetValue(ILTemplateGetValueInstruction instruction, ILCompUnit compUnit)
		{
			ILReference paramRef = instruction.ElemRef;
			string paramIden = paramRef.Element;

			TemplateInstanceSymbolType instType = instruction.TemplateType as TemplateInstanceSymbolType;

			// Get param index
			TemplateDefinitionIdentifier defIden = instType.TemplateSymbol.Identifier as TemplateDefinitionIdentifier;
			int idx;
			for (idx = 0; idx < defIden.Parameters.Count; idx++)
			{
				if (defIden.Parameters[idx].ValueName == paramIden)
					break;
			}

			ILTemplateInstance instance = compUnit.TemplateInstances[instType.Iden];
			ILTemplateArg arg = instance.Arguments[idx];

			if (arg.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.Char:
				case BuiltinTypes.WChar:
				case BuiltinTypes.Rune:
				{
					long val = long.Parse(arg.Value);
					return new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion{ Long = val });
				}
				case BuiltinTypes.F32:
				{
					return new ILInterpOperand(instruction.RetOperand, null);
				}
				case BuiltinTypes.F64:
				{
					return new ILInterpOperand(instruction.RetOperand, null);
				}
				case BuiltinTypes.String:
				{
					return new ILInterpOperand(instruction.RetOperand, null);
				}
				}
			}

			return null;
		}


		static ILInterpOperand InterpEnumGetMemberValue(ILEnumGetMemberValueInstruction instruction, ILCompUnit compUnit, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILReference memberRef = instruction.MemberRef;

			ILEnum ilEnum = compUnit.GetEnum((instruction.Type as EnumSymbolType).MangledIdentifier);
			ILEnumMember member = ilEnum.GetMember(memberRef.Element);

			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, new ILInterpValUnion{ Long = member.EnumVal.Value});
			opMapping.Add(ret.Operand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpConvType(ILConvertTypeInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand op = opMapping[instruction.Operand.Iden];
			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, op.Value);
			opMapping.Add(ret.Operand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpReturn(ILReturnInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			if (instruction.Operand == null)
				return null;

			return opMapping[instruction.Operand.Iden];
		}

		static string InterpBranch(ILBranchInstruction instruction)
		{
			return instruction.Label;
		}

		static string InterpCondBranch(ILCondBranchInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand op = opMapping[instruction.Condition.Iden];
			return op.Value.Long == 0 ? instruction.FalseLabel : instruction.TrueLabel;
		}

	}
}
