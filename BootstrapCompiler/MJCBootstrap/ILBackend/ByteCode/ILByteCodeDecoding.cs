﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.ByteCode
{
	class ILByteCodeDecoding
	{
		public static ILByteCodeModule DecodeModule(byte[] blob, CompilerContext context, string packageName, string moduleName)
		{
			ByteBuffer buffer = new ByteBuffer(blob);
			return DecodeModule(buffer, context, packageName, moduleName);
		}

		public static ILByteCodeModule DecodeModule(ByteBuffer buffer, CompilerContext context, string packageName, string moduleName)
		{
			ILByteCodeModule module = new ILByteCodeModule();
			context.ModuleImportDirective = new FileImportDirective(packageName, moduleName);

			buffer.Read(out uint magic); // magic should be checked
			buffer.Read(out uint fileSize); // file size should be checked
			buffer.Read(out ushort flags); // TODO
			buffer.Read(out byte tableCount);
			buffer.Read(out ushort functionCount);

			for (int i = 0; i < tableCount; i++)
			{
				buffer.ReadNullTerminated(out string tableType);

				if (tableType == "typ")
				{
					module.TypeTable = DecodeTypeTable(buffer, context);
				}
				else
				{
					List<string> table = DecodeTable(buffer);
					switch (tableType)
					{
					case "str": module.StringTable = table; break;
					case "mem": module.MemberRefTable = table; break;
					case "fun": module.FunctionRefTable = table; break;
					case "opr": module.OperatorTable = table; break;
					case "glo": module.GlobalTable = table; break;
					}
				}
			}

			for (int i = 0; i < functionCount; i++)
			{
				ILFunction func = DecodeFunction(buffer, module);
				module.ILFunctions.Add(func);
			}

			context.ModuleImportDirective = null;
			return module;
		}

		public static List<SymbolType> DecodeTypeTable(ByteBuffer buffer, CompilerContext context)
		{
			buffer.Read(out ushort elemCount);
			List<SymbolType> table = new List<SymbolType>(elemCount);

			for (int i = 0; i < elemCount; i++)
			{
				buffer.ReadNullTerminated(out string elem);
				SymbolType type = NameMangling.DemangleType(elem);
				type = type.GetUpdatedType(null, Scope.Empty, context);
				table.Add(type);
			}

			return table;
		}

		public static List<string> DecodeTable(ByteBuffer buffer)
		{
			buffer.Read(out ushort elemCount);
			List<string> table = new List<string>(elemCount);

			for (int i = 0; i < elemCount; i++)
			{
				buffer.ReadNullTerminated(out string elem);
				table.Add(elem);
			}

			return table;
		}

		public static ILFunction DecodeFunction(ByteBuffer buffer, ILByteCodeModule module)
		{
			Dictionary<ushort, ILOperand> opMapping = new Dictionary<ushort, ILOperand>{{ ILByteCode.InvalidId, null }};

			buffer.Read(out ushort funcId);
			buffer.Read(out ushort typeId);

			string funcIden = module.GetFunctionRef(funcId);

			NameMangling.Demangle(funcIden, out _, out Symbol sym);

			FunctionSymbolType funcType = module.GetType(typeId) as FunctionSymbolType;
			List<ILOperand> operands = new List<ILOperand>();

			ushort paramidx = 0
;			if (funcType.ReceiverType != null)
			{
				ILOperand op = new ILOperand(paramidx.ToString(), funcType.ReceiverType);
				operands.Add(op);
				opMapping.Add(paramidx, op);
			}

			for (ushort i = 0; i < funcType.ParamTypes.Count; i++, paramidx++)
			{
				SymbolType paramType = funcType.ParamTypes[i];
				ILOperand op = new ILOperand(paramidx.ToString(), paramType);
				operands.Add(op);
				opMapping.Add(paramidx, op);
			}

			ILAttributes attribs = new ILAttributes();
			List<ILTemplateParam> templateParams = null;
			ILFunction function = new ILFunction(sym.ScopeVar, funcIden, operands, funcType, attribs, templateParams, templateParams == null);

			buffer.Read(out ushort blockCount);
			for (int i = 0; i < blockCount; i++)
			{
				ILBasicBlock bb = DecodeBlock(buffer, module, opMapping);
				function.Blocks.Add(bb);
			}

			return function;
		}

		public static ILBasicBlock DecodeBlock(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort blockId);
			buffer.Read(out ushort instrCount);
			ILBasicBlock block = new ILBasicBlock($"bb{blockId}");
			
			for (int i = 0; i < instrCount; i++)
			{
				buffer.Read(out byte id);
				ILInstructionType instType = ILByteCode.InstructionIdToType[id];

				ILInstruction instr = null;
				switch (instType)
				{
				case ILInstructionType.IntLiteral:			instr = DecodeIntLit            (buffer, module, opMapping); break;
				case ILInstructionType.FloatLiteral:		instr = DecodeFloatLit          (buffer, module, opMapping); break;
				case ILInstructionType.StringLiteral:		instr = DecodeStringLit         (buffer, module, opMapping); break;
				case ILInstructionType.AllocStack:			instr = DecodeAllocStack        (buffer, module, opMapping); break;
				case ILInstructionType.DeallocStack:		instr = DecodeDeallocStack      (buffer, module, opMapping); break;
				case ILInstructionType.Assign:				instr = DecodeAssign            (buffer, module, opMapping); break;
				case ILInstructionType.Store:				instr = DecodeStore             (buffer, module, opMapping); break;
				case ILInstructionType.Load:				instr = DecodeLoad              (buffer, module, opMapping); break;
				case ILInstructionType.FunctionRef:			instr = DecodeFunctionRef       (buffer, module, opMapping); break;
				case ILInstructionType.MethodRef:			instr = DecodeMethodRef         (buffer, module, opMapping); break;
				case ILInstructionType.OperatorRef:			instr = DecodeOperatorRef       (buffer, module, opMapping); break;
				case ILInstructionType.Call:				instr = DecodeCall              (buffer, module, opMapping); break;
				case ILInstructionType.Builtin:				instr = DecodeBuiltin           (buffer, module, opMapping); break;
				case ILInstructionType.CompilerIntrin:		instr = DecodeCompilerIntrin    (buffer, module, opMapping); break;
				case ILInstructionType.TupleUnpack:			instr = DecodeTupleUnpack       (buffer, module, opMapping); break;
				case ILInstructionType.TuplePack:			instr = DecodeTuplePack         (buffer, module, opMapping); break;
				case ILInstructionType.TupleInsert:			instr = DecodeTupleInsert       (buffer, module, opMapping); break;
				case ILInstructionType.TupleExtract:		instr = DecodeTupleExtract      (buffer, module, opMapping); break;
				case ILInstructionType.TupleElemAddr:		instr = DecodeTupleElemAddr     (buffer, module, opMapping); break;
				case ILInstructionType.StructExtract:		instr = DecodeStructExtract     (buffer, module, opMapping); break;
				case ILInstructionType.StructInsert:		instr = DecodeStructInsert      (buffer, module, opMapping); break;
				case ILInstructionType.StructElemAddr:		instr = DecodeStructElemAddr    (buffer, module, opMapping); break;
				case ILInstructionType.UnionExtract:		instr = DecodeUnionExtract      (buffer, module, opMapping); break;
				case ILInstructionType.UnionInsert:			instr = DecodeUnionInsert       (buffer, module, opMapping); break;
				case ILInstructionType.UnionElemAddr:		instr = DecodeUnionElemAddr     (buffer, module, opMapping); break;
				case ILInstructionType.Destruct:
					break;
				case ILInstructionType.EnumValue:			instr = DecodeEnumValue         (buffer, module, opMapping); break;
				case ILInstructionType.EnumGetValue:		instr = DecodeEnumGetValue      (buffer, module, opMapping); break;
				case ILInstructionType.EnumGetMemberValue:	instr = DecodeEnumGetMemberValue(buffer, module, opMapping); break;
				case ILInstructionType.GlobalAddr:			instr = DecodeGlobalAddr        (buffer, module, opMapping); break;
				case ILInstructionType.GlobalValue:			instr = DecodeGlobalValue       (buffer, module, opMapping); break;
				case ILInstructionType.TemplateGetValue:	instr = DecodeTemplateGetValue  (buffer, module, opMapping); break;
				case ILInstructionType.ConvType:			instr = DecodeConvertType       (buffer, module, opMapping); break;
				case ILInstructionType.GetVTable:			instr = DecodeGetVTable        (buffer, module, opMapping); break;
				case ILInstructionType.Return:				instr = DecodeReturn            (buffer, module, opMapping); break;
				case ILInstructionType.Branch:				instr = DecodeBranch            (buffer, module, opMapping); break;
				case ILInstructionType.CondBranch:			instr = DecodeCondBranch        (buffer, module, opMapping); break;
				case ILInstructionType.SwitchEnum:          instr = DecodeSwitchEnum        (buffer, module, opMapping); break;
				case ILInstructionType.SwitchValue:         instr = DecodeSwitchValue       (buffer, module, opMapping); break;
				}

				block.Instructions.Add(instr);
			}
			
			return block;
		}


		static ILInstruction DecodeIntLit(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out long value);
			buffer.Read(out ushort typeId);

			SymbolType type = module.GetType(typeId);
			ILOperand retOp = new ILOperand(retId.ToString(), type);

			opMapping.Add(retId, retOp);
			return new ILIntLiteralInstruction(retOp, value, null);
		}

		static ILInstruction DecodeFloatLit(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out long hexVal);
			buffer.Read(out ushort typeId);

			SymbolType type = module.GetType(typeId);
			ILOperand retOp = new ILOperand(retId.ToString(), type);

			opMapping.Add(retId, retOp);
			return new ILFloatLiteralInstruction(retOp, hexVal, null);
		}

		static ILInstruction DecodeStringLit(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort stringId);
			buffer.Read(out ushort typeId);

			SymbolType type = module.GetType(typeId);
			ILOperand retOp = new ILOperand(retId.ToString(), type);
			string str = module.GetString(stringId);

			opMapping.Add(retId, retOp);
			return new ILStringLiteralInstruction(retOp, str, null);
		}

		static ILInstruction DecodeAllocStack(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort typeId);

			SymbolType type = module.GetType(typeId);
			ILOperand retOp = new ILOperand(retId.ToString(), SymbolType.MemoryLocType(type));

			opMapping.Add(retId, retOp);
			return new ILAllocStackInstruction(retOp, type, null);
		}

		static ILInstruction DecodeDeallocStack(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort opId);
			
			ILOperand op = opMapping[opId];
			return new ILDeallocStackInstruction(op);
		}

		static ILInstruction DecodeAssign(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort srcId);
			buffer.Read(out ushort dstId);

			ILOperand src = opMapping[srcId];
			ILOperand dst = opMapping[dstId];
			return new ILAssignInstruction(src, dst, null);
		}

		static ILInstruction DecodeStore(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort srcId);
			buffer.Read(out ushort dstId);

			ILOperand src = opMapping[srcId];
			ILOperand dst = opMapping[dstId];
			return new ILStoreInstruction(src, dst, null);
		}

		static ILInstruction DecodeLoad(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort srcId);

			ILOperand src = opMapping[srcId];
			MemoryLocSymbolType memType = src.Type as MemoryLocSymbolType;
			ILOperand retOp = new ILOperand(retId.ToString(), memType.BaseType);

			opMapping.Add(retId, retOp);
			return new ILLoadInstruction(retOp, src, null);
		}

		static ILInstruction DecodeFunctionRef(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort funcId);
			buffer.Read(out ushort typeId);

			FunctionSymbolType type = module.GetType(typeId) as FunctionSymbolType;
			string funcName = module.GetFunctionRef(funcId);
			ILOperand retOp = new ILOperand(retId.ToString(), type);

			opMapping.Add(retId, retOp);
			return new ILFunctionRefInstruction(retOp, funcName, type, null);
		}

		static ILInstruction DecodeMethodRef(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort methodId);
			buffer.Read(out ushort typeId);

			FunctionSymbolType type = module.GetType(typeId) as FunctionSymbolType;
			ILReference methodRef = module.GetMemberRef(methodId);
			ILOperand retOp = new ILOperand(retId.ToString(), type);

			opMapping.Add(retId, retOp);
			return new ILMethodRefInstruction(retOp, methodRef, type, null);
		}

		static ILInstruction DecodeOperatorRef(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort opId);
			buffer.Read(out ushort typeId);

			FunctionSymbolType type = module.GetType(typeId) as FunctionSymbolType;
			(ILOpLoc opLoc, ILOpType opType) = module.GetOperator(opId);
			ILOperand retOp = new ILOperand(retId.ToString(), type);

			opMapping.Add(retId, retOp);
			return new ILOperatorRefInstruction(retOp, opLoc, opType, type, null);
		}

		static ILInstruction DecodeCall(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort funcId);
			buffer.Read(out byte opCount);

			List<ILOperand> operands = opCount == 0 ? null : new List<ILOperand>();
			for (int i = 0; i < opCount; i++)
			{
				buffer.Read(out ushort opId);

				operands.Add(opMapping[opId]);
			}

			buffer.Read(out ushort typeid);

			ILOperand funcOp = opMapping[funcId];
			SymbolType retType = typeid == ILByteCode.InvalidId ? null : module.GetType(typeid);
			ILOperand retOp = retType == null ? null : new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILCallInstruction(retOp, funcOp, operands, null);
		}

		static ILInstruction DecodeBuiltin(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out byte builtinId);
			buffer.Read(out byte opCount);

			List<ILOperand> operands = opCount == 0 ? null : new List<ILOperand>();
			for (int i = 0; i < opCount; i++)
			{
				buffer.Read(out ushort opId);

				operands.Add(opMapping[opId]);
			}

			buffer.Read(out ushort typeid);

			string builtin = null; // TODO
			SymbolType retType = typeid == ILByteCode.InvalidId ? null : module.GetType(typeid);
			ILOperand retOp = retType == null ? null : new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILBuiltinInstruction(retOp, builtin, operands, null);
		}

		static ILInstruction DecodeCompilerIntrin(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out byte intrinId);
			buffer.Read(out ushort typeId);
			buffer.Read(out ushort retTypeId);

			ILCompilerIntrinType intrin = (ILCompilerIntrinType)intrinId;
			SymbolType type = module.GetType(typeId);
			SymbolType retType = module.GetType(retTypeId);
			ILOperand retOp = new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILCompilerIntrinInstruction(retOp, intrin, type, null);
		}

		static ILInstruction DecodeTuplePack(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out byte opCount);

			List<ILOperand> operands = new List<ILOperand>();
			List<SymbolType> subTypes = new List<SymbolType>();
			for (int i = 0; i < opCount; i++)
			{
				buffer.Read(out ushort opId);

				ILOperand op = opMapping[opId];
				operands.Add(op);
				subTypes.Add(op.Type);
			}

			ILOperand retOp = new ILOperand(retId.ToString(), SymbolType.TupleType(subTypes));

			opMapping.Add(retId, retOp);
			return new ILTuplePackInstruction(retOp, operands, null);
		}

		static ILInstruction DecodeTupleUnpack(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out byte retCount);

			List<ushort> retIds = new List<ushort>();
			for (int i = 0; i < retCount; i++)
			{
				buffer.Read(out ushort retId);
				retIds.Add(retId);
			}

			buffer.Read(out ushort tupId);

			ILOperand tuple = opMapping[tupId];

			List<ILOperand> retOps = new List<ILOperand>();
			TupleSymbolType tupType = tuple.Type as TupleSymbolType;
			for (var i = 0; i < retIds.Count; i++)
			{
				ushort id = retIds[i];
				SymbolType retType = tupType.SubTypes[i];
				ILOperand retOp = new ILOperand(id.ToString(), retType);

				opMapping.Add(id, retOp);
				retOps.Add(retOp);
			}

			return new ILTupleUnpackInstruction(retOps, tuple, null);
		}

		static ILInstruction DecodeTupleInsert(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort tupId);
			buffer.Read(out byte index);
			buffer.Read(out ushort valId);

			ILOperand tupOp = opMapping[tupId];
			ILOperand valOp = opMapping[valId];
			return new ILTupleInsertInstruction(tupOp, index, valOp, null);
		}

		static ILInstruction DecodeTupleExtract(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort tupId);
			buffer.Read(out byte index);

			ILOperand tupOp = opMapping[tupId];
			SymbolType retType = (tupOp.Type as TupleSymbolType).SubTypes[index];
			ILOperand retOp = new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILTupleExtractInstruction(retOp, tupOp, index, null);
		}

		static ILInstruction DecodeTupleElemAddr(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort tupId);
			buffer.Read(out byte index);

			ILOperand tupOp = opMapping[tupId];
			SymbolType retType = (tupOp.Type as TupleSymbolType).SubTypes[index];
			ILOperand retOp = new ILOperand(retId.ToString(), SymbolType.MemoryLocType(retType));

			opMapping.Add(retId, retOp);
			return new ILTupleElemAddrInstruction(retOp, tupOp, index, null);
		}

		static ILInstruction DecodeStructInsert(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort tupId);
			buffer.Read(out ushort memberId);
			buffer.Read(out ushort valId);

			ILOperand tupOp = opMapping[tupId];
			ILReference memberRef = module.GetMemberRef(memberId);
			ILOperand valOp = opMapping[valId];
			return new ILStructInsertInstruction(tupOp, memberRef, valOp, null);
		}

		static ILInstruction DecodeStructExtract(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort structId);
			buffer.Read(out ushort memberId);
			buffer.Read(out ushort typeid);

			ILOperand structOp = opMapping[structId];
			ILReference memberRef = module.GetMemberRef(memberId);
			SymbolType retType = module.GetType(typeid);
			ILOperand retOp = new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILStructExtractInstruction(retOp, structOp, memberRef, null);
		}

		static ILInstruction DecodeStructElemAddr(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort structId);
			buffer.Read(out ushort memberId);
			buffer.Read(out ushort typeid);

			ILOperand structOp = opMapping[structId];
			ILReference memberRef = module.GetMemberRef(memberId);
			SymbolType retType = module.GetType(typeid);
			ILOperand retOp = new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILStructElemAddrInstruction(retOp, structOp, memberRef, null);
		}

		static ILInstruction DecodeUnionInsert(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort tupId);
			buffer.Read(out ushort memberId);
			buffer.Read(out ushort valId);

			ILOperand tupOp = opMapping[tupId];
			ILReference memberRef = module.GetMemberRef(memberId);
			ILOperand valOp = opMapping[valId];
			return new ILUnionInsertInstruction(tupOp, memberRef, valOp, null);
		}

		static ILInstruction DecodeUnionExtract(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort structId);
			buffer.Read(out ushort memberId);
			buffer.Read(out ushort typeid);

			ILOperand structOp = opMapping[structId];
			ILReference memberRef = module.GetMemberRef(memberId);
			SymbolType retType = module.GetType(typeid);
			ILOperand retOp = new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILUnionExtractInstruction(retOp, structOp, memberRef, null);
		}

		static ILInstruction DecodeUnionElemAddr(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort structId);
			buffer.Read(out ushort memberId);
			buffer.Read(out ushort typeid);

			ILOperand structOp = opMapping[structId];
			ILReference memberRef = module.GetMemberRef(memberId);
			SymbolType retType = module.GetType(typeid);
			ILOperand retOp = new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILUnionElemAddrInstruction(retOp, structOp, memberRef, null);
		}

		static ILInstruction DecodeEnumValue(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort typeId);
			buffer.Read(out ushort memberId);
			buffer.Read(out byte opCount);

			List<ILOperand> operands = new List<ILOperand>();
			for (int i = 0; i < opCount; i++)
			{
				buffer.Read(out ushort opId);
				operands.Add(opMapping[opId]);
			}

			SymbolType retType = module.GetType(typeId);
			ILOperand retOp = new ILOperand(retId.ToString(), retType);
			ILReference memberRef = module.GetMemberRef(memberId);

			opMapping.Add(retId, retOp);
			return new ILEnumValueInstruction(retOp, retType, memberRef, operands, null);
		}

		static ILInstruction DecodeEnumGetValue(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort opId);
			buffer.Read(out ushort typeId);

			ILOperand enumOp = opMapping[opId];
			SymbolType retType = module.GetType(typeId);
			ILOperand ret = new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, ret);
			return new ILEnumGetValueInstruction(ret, enumOp, null);
		}

		static ILInstruction DecodeEnumGetMemberValue(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort typeId);
			buffer.Read(out ushort memberId);
			buffer.Read(out ushort retTypeId);

			SymbolType retType = module.GetType(retTypeId);
			ILOperand retOp = new ILOperand(retId.ToString(), retType);
			SymbolType type = module.GetType(typeId);
			ILReference memberRef = module.GetMemberRef(memberId);

			opMapping.Add(retId, retOp);
			return new ILEnumGetMemberValueInstruction(retOp, type, memberRef, null);
		}

		static ILInstruction DecodeGlobalAddr(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort globalId);
			buffer.Read(out ushort typeId);

			string global = module.GetGlobal(globalId);
			SymbolType retType = module.GetType(typeId);
			ILOperand retOp = new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILGlobalAddrInstruction(retOp, global, null);
		}

		static ILInstruction DecodeGlobalValue(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort globalId);
			buffer.Read(out ushort typeId);

			string global = module.GetGlobal(globalId);
			SymbolType retType = module.GetType(typeId);
			ILOperand retOp = new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILGlobalValueInstruction(retOp, global, null);
		}

		static ILInstruction DecodeTemplateGetValue(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort typeId);
			buffer.Read(out ushort memberId);
			buffer.Read(out ushort retTypeId);

			SymbolType type = module.GetType(typeId);
			ILReference memberRef = module.GetMemberRef(memberId);
			SymbolType retType = module.GetType(retTypeId);
			ILOperand retOp = new ILOperand(retId.ToString(), retType);

			opMapping.Add(retId, retOp);
			return new ILTemplateGetValueInstruction(retOp, type, memberRef, null);
		}

		static ILInstruction DecodeConvertType(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort opId);
			buffer.Read(out ushort typeId);

			ILOperand op = opMapping[opId];
			SymbolType type = module.GetType(typeId);
			ILOperand retOp = new ILOperand(retId.ToString(), type);

			opMapping.Add(retId, retOp);
			return new ILConvertTypeInstruction(retOp, op, null);
		}

		static ILInstruction DecodeGetVTable(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort retId);
			buffer.Read(out ushort typeId);
			buffer.Read(out byte interfaceCount);

			List<SymbolType> interfaceTypes = new List<SymbolType>();
			for (int i = 0; i < interfaceCount; i++)
			{
				buffer.Read(out ushort interfaceId);
				interfaceTypes.Add(module.GetType(interfaceId));
			}

			ILOperand retOp = new ILOperand(retId.ToString(), SymbolType.PointerType(SymbolType.BuiltinType(BuiltinTypes.Void)));
			SymbolType type = module.GetType(typeId);

			opMapping.Add(retId, retOp);
			return new ILGetVTableInstruction(retOp, type, interfaceTypes);
		}

		static ILInstruction DecodeReturn(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort opId);

			ILOperand op = opMapping[opId];
			return new ILReturnInstruction(op, null);
		}

		static ILInstruction DecodeBranch(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort branchId);

			return new ILBranchInstruction($"bb{branchId}", null);
		}

		static ILInstruction DecodeCondBranch(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort opId);
			buffer.Read(out ushort trueId);
			buffer.Read(out ushort falseId);

			ILOperand op = opMapping[opId];
			return new ILCondBranchInstruction(op, $"bb{trueId}", $"bb{falseId}", null);
		}

		static ILInstruction DecodeSwitchEnum(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort enumId);
			buffer.Read(out ushort caseCount);

			ILOperand enumOp = opMapping[enumId];

			List<ILRefCase> cases = null;
			if (caseCount > 0)
			{
				cases = new List<ILRefCase>(caseCount);
				for (int i = 0; i < caseCount; i++)
				{
					buffer.Read(out ushort memberId);
					buffer.Read(out ushort blockId);

					ILReference member = module.GetMemberRef(memberId);
					ILRefCase refCase = new ILRefCase(member, $"bb{blockId}");
					cases.Add(refCase);
				}
			}

			buffer.Read(out ushort defaultId);
			string defaultLabel = defaultId == ILByteCode.InvalidId ? null : $"bb{defaultId}";

			return new ILSwitchEnumInstruction(enumOp, cases, defaultLabel, null);
		}

		static ILInstruction DecodeSwitchValue(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			buffer.Read(out ushort enumId);
			buffer.Read(out ushort caseCount);

			ILOperand enumOp = opMapping[enumId];

			List<ILValCase> cases = null;
			if (caseCount > 0)
			{
				cases = new List<ILValCase>(caseCount);
				for (int i = 0; i < caseCount; i++)
				{
					buffer.Read(out ushort valId);
					buffer.Read(out ushort blockId);

					ILOperand val = opMapping[valId];
					ILValCase refCase = new ILValCase(val, $"bb{blockId}");
					cases.Add(refCase);
				}
			}

			buffer.Read(out ushort defaultId);
			string defaultLabel = defaultId == ILByteCode.InvalidId ? null : $"bb{defaultId}";

			return new ILSwitchValueInstruction(enumOp, cases, defaultLabel, null);
		}

		static ILInstruction Decode(ByteBuffer buffer, ILByteCodeModule module, Dictionary<ushort, ILOperand> opMapping)
		{
			return null;
		}


	}
}
