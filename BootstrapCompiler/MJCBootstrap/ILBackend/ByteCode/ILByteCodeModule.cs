﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.ByteCode
{
	public class ILByteCodeModule
	{
		public List<SymbolType> TypeTable = new List<SymbolType>();
		public List<string> StringTable = new List<string>();
		public List<string> MemberRefTable = new List<string>();
		public List<string> FunctionRefTable = new List<string>();
		public List<string> OperatorTable = new List<string>();
		public List<string> GlobalTable = new List<string>();

		public Dictionary<SymbolType, ushort> TypeMapping = new Dictionary<SymbolType, ushort>();
		public Dictionary<string, ushort> StringMapping = new Dictionary<string, ushort>();
		public Dictionary<string, ushort> MemberRefMapping = new Dictionary<string, ushort>();
		public Dictionary<string, ushort> FunctionRefMapping = new Dictionary<string, ushort>();
		public Dictionary<string, ushort> OperatorMapping = new Dictionary<string, ushort>();
		public Dictionary<string, ushort> GlobalMapping = new Dictionary<string, ushort>();

		public List<ILByteCodeFunction> Functions = new List<ILByteCodeFunction>();
		public List<ILFunction> ILFunctions = new List<ILFunction>();

		public ushort GetOrAddType(SymbolType type)
		{
			if (TypeMapping.TryGetValue(type, out ushort tmp))
				return tmp;

			ushort id = (ushort)TypeTable.Count;
			TypeTable.Add(type);
			TypeMapping.Add(type, id);
			return id;
		}

		public SymbolType GetType(ushort id)
		{
			return TypeTable[id];
		}

		public ushort GetOrAddString(string str)
		{
			if (StringMapping.TryGetValue(str, out ushort tmp))
				return tmp;

			ushort id = (ushort)StringTable.Count;
			StringTable.Add(str);
			StringMapping.Add(str, id);
			return id;
		}

		public string GetString(ushort id)
		{
			return StringTable[id];
		}

		public ushort GetOrAddFunctionRef(string mangled)
		{
			if (FunctionRefMapping.TryGetValue(mangled, out ushort tmp))
				return tmp;

			ushort id = (ushort)FunctionRefTable.Count;
			FunctionRefTable.Add(mangled);
			FunctionRefMapping.Add(mangled, id);
			return id;
		}

		public string GetFunctionRef(ushort id)
		{
			return FunctionRefTable[id];
		}

		public ushort GetOrAddMemberRef(ILReference memberRef)
		{
			string str = $"{memberRef.Base}.{memberRef.Element}";

			if (MemberRefMapping.TryGetValue(str, out ushort tmp))
				return tmp;

			ushort id = (ushort)MemberRefTable.Count;
			MemberRefTable.Add(str);
			MemberRefMapping.Add(str, id);
			return id;
		}

		public ILReference GetMemberRef(ushort id)
		{
			string[] parts = MemberRefTable[id].Split('.');
			return new ILReference(parts[0], parts[1]);
		}

		public ushort GetOrAddOperator(ILOpLoc opLoc, ILOpType opType)
		{
			string str = ILHelpers.GetIlOpName(opLoc, opType);
			if (OperatorMapping.TryGetValue(str, out ushort tmp))
				return tmp;

			ushort id = (ushort)OperatorTable.Count;
			OperatorTable.Add(str);
			OperatorMapping.Add(str, id);
			return id;
		}

		public (ILOpLoc, ILOpType) GetOperator(ushort id)
		{
			return ILHelpers.GetILOpLocAndType(OperatorTable[id]);
		}

		public ushort GetOrAddGlobal(string str)
		{
			if (GlobalMapping.TryGetValue(str, out ushort tmp))
				return tmp;

			ushort id = (ushort)GlobalTable.Count;
			GlobalTable.Add(str);
			GlobalMapping.Add(str, id);
			return id;
		}

		public string GetGlobal(ushort id)
		{
			return GlobalTable[id];
		}

		public void Write(BinaryWriter writer)
		{
			int startPos = (int)writer.BaseStream.Position;

			writer.Write(new byte[]{ 0x80, 0x4D, 0x4A, 0x4C }); // 0x80 M J L
			writer.Write((uint)0); // dummy file size
			writer.Write((ushort)0); // flags
			writer.Write((byte)0); // dummy table count
			writer.Write((ushort)Functions.Count);

			byte tableCount = 0;
			if (TypeTable.Count > 0)
			{
				ByteUtils.WriteNullTerminatedCStr(writer, "typ");
				writer.Write(BitConverter.GetBytes((ushort)TypeTable.Count));
				foreach (SymbolType type in TypeTable)
				{
					string mangled = NameMangling.MangleType(type);
					ByteUtils.WriteNullTerminatedCStr(writer, mangled);
				}

				++tableCount;
			}

			tableCount += WriteTable(writer, StringTable, "str");
			tableCount += WriteTable(writer, MemberRefTable, "mem");
			tableCount += WriteTable(writer, FunctionRefTable, "fun");
			tableCount += WriteTable(writer, OperatorTable, "opr");
			tableCount += WriteTable(writer, GlobalTable, "glo");

			foreach (ILByteCodeFunction func in Functions)
			{
				func.Write(writer, this);
			}

			int len = (int) writer.BaseStream.Position - startPos;
			writer.Seek(startPos + 4, SeekOrigin.Begin);
			writer.Write(len);
			writer.Seek(2, SeekOrigin.Current);
			writer.Write(tableCount);
			writer.Seek(0, SeekOrigin.End);
		}

		byte WriteTable(BinaryWriter writer, List<string> table, string tableIden)
		{
			if (table.Count > 0)
			{
				ByteUtils.WriteNullTerminatedCStr(writer, tableIden);
				writer.Write(BitConverter.GetBytes((ushort)table.Count));
				foreach (string elem in table)
				{
					ByteUtils.WriteNullTerminatedCStr(writer, elem);
				}

				return 1;
			}

			return 0;
		}
	}

	public class ILByteCodeFunction
	{
		public ushort FunctionId;
		public ushort TypeId;
		public List<ushort> OperandTypeIds = new List<ushort>();
		public List<ILByteCodeBasicBlock> BasicBlocks = new List<ILByteCodeBasicBlock>();
		public Dictionary<string, ushort> BasicBlockMapping = new Dictionary<string, ushort>();

		public ushort GetBasicBlockId(string label)
		{
			return BasicBlockMapping[label];
		}

		public void Write(BinaryWriter writer, ILByteCodeModule module)
		{
			writer.Write(FunctionId);
			writer.Write(TypeId);

			ushort numBlocks = (ushort)BasicBlocks.Count;
			writer.Write(numBlocks);

			foreach (ILByteCodeBasicBlock block in BasicBlocks)
			{
				block.Write(writer);
			}
		}
	}

	public class ILByteCodeBasicBlock
	{
		public ushort BlockId;
		public List<byte[]> Instructions = new List<byte[]>();

		public void Write(BinaryWriter writer)
		{
			writer.Write(BlockId);
			writer.Write((ushort)Instructions.Count);

			foreach (byte[] instr in Instructions)
			{
				writer.Write(instr);
			}
		}
	}
}
