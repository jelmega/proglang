﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.ILBackend.ByteCode
{
	public static class ILByteCode
	{
		public const ushort InvalidId = 0xFFFF;

		public static Dictionary<ILInstructionType, byte> InstructionTypeToId = new Dictionary<ILInstructionType, byte>
		{
			{ ILInstructionType.IntLiteral        , 0x00 },
			{ ILInstructionType.FloatLiteral      , 0x01 },
			{ ILInstructionType.StringLiteral     , 0x02 },

			{ ILInstructionType.AllocStack        , 0x10 },
			{ ILInstructionType.DeallocStack      , 0x11 },
			{ ILInstructionType.Assign            , 0x12 },
			{ ILInstructionType.Store             , 0x13 },
			{ ILInstructionType.Load              , 0x14 },

			{ ILInstructionType.FunctionRef       , 0x20 },
			{ ILInstructionType.MethodRef         , 0x21 },
			{ ILInstructionType.OperatorRef       , 0x22 },
			{ ILInstructionType.Call              , 0x23 },
			{ ILInstructionType.Builtin           , 0x24 },
			{ ILInstructionType.CompilerIntrin    , 0x25 },

			{ ILInstructionType.TuplePack         , 0x30 },
			{ ILInstructionType.TupleUnpack       , 0x31 },
			{ ILInstructionType.TupleInsert       , 0x32 },
			{ ILInstructionType.TupleExtract      , 0x33 },
			{ ILInstructionType.TupleElemAddr     , 0x34 },

			{ ILInstructionType.StructInsert      , 0x40 },
			{ ILInstructionType.StructExtract     , 0x41 },
			{ ILInstructionType.StructElemAddr    , 0x42 },

			{ ILInstructionType.UnionInsert       , 0x50 },
			{ ILInstructionType.UnionExtract      , 0x51 },
			{ ILInstructionType.UnionElemAddr     , 0x52 },

			{ ILInstructionType.EnumValue         , 0x60 },
			{ ILInstructionType.EnumGetValue      , 0x61 },
			{ ILInstructionType.EnumGetMemberValue, 0x62 },

			{ ILInstructionType.GlobalAddr        , 0x70 },
			{ ILInstructionType.GlobalValue       , 0x71 },

			{ ILInstructionType.TemplateGetValue  , 0x80 },

			{ ILInstructionType.ConvType          , 0x90 },
			{ ILInstructionType.GetVTable         , 0x91 },

			{ ILInstructionType.Return            , 0xA0 },
			{ ILInstructionType.Branch            , 0xA1 },
			{ ILInstructionType.CondBranch        , 0xA2 },
			{ ILInstructionType.SwitchEnum        , 0xA3 },
			{ ILInstructionType.SwitchValue       , 0xA4 },
		};


		public static Dictionary<byte, ILInstructionType> InstructionIdToType = new Dictionary<byte, ILInstructionType>
		{
			{ 0x00, ILInstructionType.IntLiteral         },
			{ 0x01, ILInstructionType.FloatLiteral       },
			{ 0x02, ILInstructionType.StringLiteral      },

			{ 0x10, ILInstructionType.AllocStack         },
			{ 0x11, ILInstructionType.DeallocStack       },
			{ 0x12, ILInstructionType.Assign             },
			{ 0x13, ILInstructionType.Store              },
			{ 0x14, ILInstructionType.Load               },

			{ 0x20, ILInstructionType.FunctionRef        },
			{ 0x21, ILInstructionType.MethodRef          },
			{ 0x22, ILInstructionType.OperatorRef        },
			{ 0x23, ILInstructionType.Call               },
			{ 0x24, ILInstructionType.Builtin            },
			{ 0x25, ILInstructionType.CompilerIntrin     },

			{ 0x30, ILInstructionType.TuplePack          },
			{ 0x31, ILInstructionType.TupleUnpack        },
			{ 0x32, ILInstructionType.TupleInsert        },
			{ 0x33, ILInstructionType.TupleExtract       },
			{ 0x34, ILInstructionType.TupleElemAddr      },

			{ 0x40, ILInstructionType.StructInsert       },
			{ 0x41, ILInstructionType.StructExtract      },
			{ 0x42, ILInstructionType.StructElemAddr     },

			{ 0x50, ILInstructionType.UnionInsert        },
			{ 0x51, ILInstructionType.UnionExtract       },
			{ 0x52, ILInstructionType.UnionElemAddr      },

			{ 0x60, ILInstructionType.EnumValue          },
			{ 0x61, ILInstructionType.EnumGetValue       },
			{ 0x62, ILInstructionType.EnumGetMemberValue },

			{ 0x70, ILInstructionType.GlobalAddr         },
			{ 0x71, ILInstructionType.GlobalValue        },

			{ 0x80, ILInstructionType.TemplateGetValue   },

			{ 0x90, ILInstructionType.ConvType           },
			{ 0x91, ILInstructionType.GetVTable          },

			{ 0xA0, ILInstructionType.Return             },
			{ 0xA1, ILInstructionType.Branch             },
			{ 0xA2, ILInstructionType.CondBranch         },
			{ 0xA3, ILInstructionType.SwitchEnum         },
			{ 0xA4, ILInstructionType.SwitchValue        },
		};



	}
}
