﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.ByteCode
{
	class ILByteCodeEncoding
	{
		public static ILByteCodeModule Encode(ILCompUnit compUnit, List<ILFunction> toEncode = null)
		{
			ILByteCodeModule module = new ILByteCodeModule();
			if (toEncode == null || toEncode.Count == 0)
			{
				foreach (KeyValuePair<string, ILFunction> pair in compUnit.Functions)
				{
					ILByteCodeFunction func = EncodeFunction(pair.Value, module);
					module.Functions.Add(func);
				}
			}
			else
			{
				foreach (ILFunction func in toEncode)
				{
					ILByteCodeFunction bcFunc = EncodeFunction(func, module);
					module.Functions.Add(bcFunc);
				}
			}

			return module;
		}

		public static ILByteCodeFunction EncodeFunction(ILFunction ilFunction, ILByteCodeModule module)
		{
			Dictionary<string, ushort> valueIdMapping = new Dictionary<string, ushort>();

			ILByteCodeFunction function = new ILByteCodeFunction();
			function.FunctionId = module.GetOrAddFunctionRef(ilFunction.MangledIdentifier);
			function.TypeId = module.GetOrAddType(ilFunction.FuncType);

			// Set argument types and operands
			if (ilFunction.Operands != null)
			{
				for (ushort i = 0; i < ilFunction.Operands.Count; i++)
				{
					ILOperand operand = ilFunction.Operands[i];
					valueIdMapping.Add(operand.Iden, i);

					ushort typeId = module.GetOrAddType(operand.Type);
					function.OperandTypeIds.Add(typeId);
				}
			}

			// Assign block ids
			for (ushort i = 0; i < ilFunction.Blocks.Count; i++)
			{
				ILBasicBlock block = ilFunction.Blocks[i];
				function.BasicBlockMapping.Add(block.Label, i);
			}

			// Create basic blocks
			ushort curValueId = (ushort)valueIdMapping.Count;
			foreach (ILBasicBlock block in ilFunction.Blocks)
			{
				ILByteCodeBasicBlock bb = EncodeBasicBlock(block, function, module, valueIdMapping, ref curValueId);
				function.BasicBlocks.Add(bb);
			}

			return function;
		}

		public static ILByteCodeBasicBlock EncodeBasicBlock(ILBasicBlock block, ILByteCodeFunction function, ILByteCodeModule module, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			ILByteCodeBasicBlock bb = new ILByteCodeBasicBlock();
			bb.BlockId = function.GetBasicBlockId(block.Label);

			foreach (ILInstruction instruction in block.Instructions)
			{
				switch (instruction.InstructionType)
				{
				case ILInstructionType.IntLiteral:
					EncodeIntLit(instruction as ILIntLiteralInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.FloatLiteral:
					EncodeFloatLit(instruction as ILFloatLiteralInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.StringLiteral:
					EncodeStringLit(instruction as ILStringLiteralInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.AllocStack:
					EncodeAllocStack(instruction as ILAllocStackInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.DeallocStack:
					EncodeDeallocStack(instruction as ILDeallocStackInstruction, bb, valueIdMapping);
					break;
				case ILInstructionType.Assign:
					EncodeAssign(instruction as ILAssignInstruction, bb, valueIdMapping);
					break;
				case ILInstructionType.Store:
					EncodeStore(instruction as ILStoreInstruction, bb, valueIdMapping);
					break;
				case ILInstructionType.Load:
					EncodeLoad(instruction as ILLoadInstruction, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.FunctionRef:
					EncodeFunctionRef(instruction as ILFunctionRefInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.MethodRef:
					EncodeMethodRef(instruction as ILMethodRefInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.OperatorRef:
					EncodeOperatorRef(instruction as ILOperatorRefInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.Call:
					EncodeCall(instruction as ILCallInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.Builtin:
					EncodeBuiltin(instruction as ILBuiltinInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.CompilerIntrin:
					EncodeCompilerIntrin(instruction as ILCompilerIntrinInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.TupleUnpack:
					EncodeTupleUnpack(instruction as ILTupleUnpackInstruction, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.TuplePack:
					EncodeTuplePack(instruction as ILTuplePackInstruction, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.TupleInsert:
					EncodeTupleInsert(instruction as ILTupleInsertInstruction, bb, valueIdMapping);
					break;
				case ILInstructionType.TupleExtract:
					EncodeTupleExtract(instruction as ILTupleExtractInstruction, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.TupleElemAddr:
					EncodeTupleElemAddr(instruction as ILTupleElemAddrInstruction, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.StructExtract:
					EncodeStructExtract(instruction as ILStructExtractInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.StructInsert:
					EncodeStructInsert(instruction as ILStructInsertInstruction, module, bb, valueIdMapping);
					break;
				case ILInstructionType.StructElemAddr:
					EncodeStructElemAddr(instruction as ILStructElemAddrInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.UnionExtract:
					EncodeUnionExtract(instruction as ILUnionExtractInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.UnionInsert:
					EncodeUnionInsert(instruction as ILUnionInsertInstruction, module, bb, valueIdMapping);
					break;
				case ILInstructionType.UnionElemAddr:
					EncodeUnionElemAddr(instruction as ILUnionElemAddrInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.Destruct:
					break;
				case ILInstructionType.EnumValue:
					EncodeEnumValue(instruction as ILEnumValueInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.EnumGetValue:
					EncodeEnumGetValue(instruction as ILEnumGetValueInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.EnumGetMemberValue:
					EncodeEnumGetMemberValue(instruction as ILEnumGetMemberValueInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.GlobalAddr:
					EncodeGlobalAddr(instruction as ILGlobalAddrInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.GlobalValue:
					EncodeGlobalValue(instruction as ILGlobalValueInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.TemplateGetValue:
					EncodeTemplateGetValue(instruction as ILTemplateGetValueInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.ConvType:
					EncodeConvertType(instruction as ILConvertTypeInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.GetVTable:
					EncodeGetVTable(instruction as ILGetVTableInstruction, module, bb, valueIdMapping, ref curValueId);
					break;
				case ILInstructionType.Return:
					EncodeReturn(instruction as ILReturnInstruction, bb, valueIdMapping);
					break;
				case ILInstructionType.Branch:
					EncodeBranch(instruction as ILBranchInstruction, function, bb);
					break;
				case ILInstructionType.CondBranch:
					EncodeCondBranch(instruction as ILCondBranchInstruction, function, bb, valueIdMapping);
					break;
				case ILInstructionType.SwitchEnum:
					EncodeSwitchEnum(instruction as ILSwitchEnumInstruction, module, function, bb, valueIdMapping);
					break;
				case ILInstructionType.SwitchValue:
					EncodeSwitchValue(instruction as ILSwitchValueInstruction, module, function, bb, valueIdMapping);
					break;
				default:
					throw new ArgumentOutOfRangeException();
				}
			}

			return bb;
		}



		public static void EncodeIntLit(ILIntLiteralInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[13];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.IntLiteral];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);
			ByteUtils.WriteToBuffer(bytes, 3, instruction.Value);

			ushort typeIdx = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 11, typeIdx);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeFloatLit(ILFloatLiteralInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[13];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.FloatLiteral];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);
			ByteUtils.WriteToBuffer(bytes, 3, instruction.HexValue);

			ushort typeId = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 11, typeId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}


		public static void EncodeStringLit(ILStringLiteralInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[5];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.StringLiteral];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort stringId = module.GetOrAddString(instruction.String);
			ByteUtils.WriteToBuffer(bytes, 3, stringId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeAllocStack(ILAllocStackInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[5];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.AllocStack];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort typeId = module.GetOrAddType(instruction.Type);
			ByteUtils.WriteToBuffer(bytes, 3, typeId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeDeallocStack(ILDeallocStackInstruction instruction, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping)
		{
			byte[] bytes = new byte[5];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.DeallocStack];

			ushort valId = valueIdMapping[instruction.Operand.Iden];
			ByteUtils.WriteToBuffer(bytes, 1, valId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeAssign(ILAssignInstruction instruction, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping)
		{
			byte[] bytes = new byte[5];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.Assign];

			ushort srcId = valueIdMapping[instruction.Src.Iden];
			ByteUtils.WriteToBuffer(bytes, 1, srcId);

			ushort dstId = valueIdMapping[instruction.Dst.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, dstId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeStore(ILStoreInstruction instruction, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping)
		{
			byte[] bytes = new byte[5];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.Store];

			ushort srcId = valueIdMapping[instruction.Src.Iden];
			ByteUtils.WriteToBuffer(bytes, 1, srcId);

			ushort dstId = valueIdMapping[instruction.Dst.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, dstId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeLoad(ILLoadInstruction instruction, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[5];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.Load];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort srcId = valueIdMapping[instruction.Src.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, srcId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeFunctionRef(ILFunctionRefInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[7];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.FunctionRef];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort funcId = module.GetOrAddFunctionRef(instruction.MangledName);
			ByteUtils.WriteToBuffer(bytes, 3, funcId);

			ushort funcTypeId = module.GetOrAddType(instruction.Type);
			ByteUtils.WriteToBuffer(bytes, 5, funcTypeId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeMethodRef(ILMethodRefInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[7];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.MethodRef];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort methodId = module.GetOrAddMemberRef(instruction.MethodRef);
			ByteUtils.WriteToBuffer(bytes, 3, methodId);

			ushort funcTypeId = module.GetOrAddType(instruction.Type);
			ByteUtils.WriteToBuffer(bytes, 5, funcTypeId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeOperatorRef(ILOperatorRefInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[7];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.OperatorRef];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort operatorId = module.GetOrAddOperator(instruction.Loc, instruction.Op);
			ByteUtils.WriteToBuffer(bytes, 3, operatorId);

			ushort funcTypeId = module.GetOrAddType(instruction.Type);
			ByteUtils.WriteToBuffer(bytes, 5, funcTypeId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeCall(ILCallInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte opCount = (byte)(instruction.Operands?.Count ?? 0);

			byte[] bytes = new byte[8 + opCount * 2];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.Call];

			ushort retId = instruction.RetOperand == null ? ILByteCode.InvalidId : curValueId;
			ByteUtils.WriteToBuffer(bytes, 1, retId);

			ushort funcId = valueIdMapping[instruction.Func.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, funcId);

			ByteUtils.WriteToBuffer(bytes, 5, opCount);

			int offset = 6;
			if (instruction.Operands != null)
			{
				foreach (ILOperand operand in instruction.Operands)
				{
					ushort srcId = valueIdMapping[operand.Iden];
					ByteUtils.WriteToBuffer(bytes, offset, srcId);
					offset += 2;
				}
			}

			ushort retTypeId = instruction.RetOperand == null ? ILByteCode.InvalidId : module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, offset, retTypeId);

			block.Instructions.Add(bytes);

			if (instruction.RetOperand != null)
			{
				valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
				++curValueId;
			}
		}

		public static void EncodeBuiltin(ILBuiltinInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte opCount = (byte) instruction.Operands.Count;

			byte[] bytes = new byte[5 + opCount * 2];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.Builtin];

			ushort retId = instruction.RetOperand == null ? (ushort)0xFFFF : curValueId;
			ByteUtils.WriteToBuffer(bytes, 1, retId);

			byte builtinId = 0; // TODO
			ByteUtils.WriteToBuffer(bytes, 3, builtinId);

			ByteUtils.WriteToBuffer(bytes, 4, opCount);

			int offset = 5;
			foreach (ILOperand operand in instruction.Operands)
			{
				ushort srcId = valueIdMapping[operand.Iden];
				ByteUtils.WriteToBuffer(bytes, offset, srcId);
				offset += 2;
			}

			ushort retTypeId = instruction.RetOperand == null ? ILByteCode.InvalidId : module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, offset, retTypeId);

			block.Instructions.Add(bytes);

			if (instruction.RetOperand != null)
			{
				valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
				++curValueId;
			}
		}

		public static void EncodeCompilerIntrin(ILCompilerIntrinInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[8];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.CompilerIntrin];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			byte intrinId = (byte)instruction.Intrin;
			ByteUtils.WriteToBuffer(bytes, 3, intrinId);

			ushort typeId = module.GetOrAddType(instruction.Type);
			ByteUtils.WriteToBuffer(bytes, 4, typeId);

			ushort retTypeId = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 6, retTypeId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeTuplePack(ILTuplePackInstruction instruction, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte opCount = (byte)instruction.Operands.Count;

			byte[] bytes = new byte[4 + opCount * 2];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.TuplePack];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ByteUtils.WriteToBuffer(bytes, 3, opCount);

			int offset = 4;
			foreach (ILOperand operand in instruction.Operands)
			{
				ushort srcId = valueIdMapping[operand.Iden];
				ByteUtils.WriteToBuffer(bytes, offset, srcId);
				offset += 2;
			}

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeTupleUnpack(ILTupleUnpackInstruction instruction, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte retCount = (byte)instruction.RetOperands.Count;

			byte[] bytes = new byte[4 + retCount * 2];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.TupleUnpack];

			ByteUtils.WriteToBuffer(bytes, 1, retCount);

			int offset = 2;
			foreach (ILOperand operand in instruction.RetOperands)
			{
				ByteUtils.WriteToBuffer(bytes, offset, curValueId);
				offset += 2;

				valueIdMapping.Add(operand.Iden, curValueId);
				++curValueId;
			}

			ushort tupId = valueIdMapping[instruction.Operand.Iden];
			ByteUtils.WriteToBuffer(bytes, offset, tupId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeTupleInsert(ILTupleInsertInstruction instruction, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping)
		{
			byte[] bytes = new byte[6];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.TupleInsert];

			ushort tupleId = valueIdMapping[instruction.Tuple.Iden];
			ByteUtils.WriteToBuffer(bytes, 1, tupleId);

			ByteUtils.WriteToBuffer(bytes, 3, (byte) instruction.Index);

			ushort valId = valueIdMapping[instruction.Value.Iden];
			ByteUtils.WriteToBuffer(bytes, 4, valId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeTupleExtract(ILTupleExtractInstruction instruction, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[6];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.TupleExtract];
			
			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort tupleId = valueIdMapping[instruction.Tuple.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, tupleId);

			ByteUtils.WriteToBuffer(bytes, 5, (byte)instruction.Index);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeTupleElemAddr(ILTupleElemAddrInstruction instruction, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[6];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.TupleElemAddr];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort tupleId = valueIdMapping[instruction.Tuple.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, tupleId);

			ByteUtils.WriteToBuffer(bytes, 5, (byte)instruction.Index);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeStructInsert(ILStructInsertInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping)
		{
			byte[] bytes = new byte[7];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.StructInsert];

			ushort tupleId = valueIdMapping[instruction.Struct.Iden];
			ByteUtils.WriteToBuffer(bytes, 1, tupleId);

			ushort memberId = module.GetOrAddMemberRef(instruction.ElemRef);
			ByteUtils.WriteToBuffer(bytes, 3, memberId);

			ushort valId = valueIdMapping[instruction.Value.Iden];
			ByteUtils.WriteToBuffer(bytes, 5, valId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeStructExtract(ILStructExtractInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[9];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.StructExtract];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort tupleId = valueIdMapping[instruction.Struct.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, tupleId);

			ushort memberId = module.GetOrAddMemberRef(instruction.ElemRef);
			ByteUtils.WriteToBuffer(bytes, 5, memberId);

			ushort typeid = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 7, typeid);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeStructElemAddr(ILStructElemAddrInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[9];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.StructElemAddr];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort tupleId = valueIdMapping[instruction.Struct.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, tupleId);

			ushort memberId = module.GetOrAddMemberRef(instruction.ElemRef);
			ByteUtils.WriteToBuffer(bytes, 5, memberId);

			ushort typeid = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 7, typeid);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeUnionInsert(ILUnionInsertInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping)
		{
			byte[] bytes = new byte[7];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.UnionInsert];

			ushort tupleId = valueIdMapping[instruction.Union.Iden];
			ByteUtils.WriteToBuffer(bytes, 1, tupleId);

			ushort memberId = module.GetOrAddMemberRef(instruction.ElemRef);
			ByteUtils.WriteToBuffer(bytes, 3, memberId);

			ushort valId = valueIdMapping[instruction.Value.Iden];
			ByteUtils.WriteToBuffer(bytes, 5, valId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeUnionExtract(ILUnionExtractInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[9];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.UnionExtract];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort tupleId = valueIdMapping[instruction.Union.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, tupleId);

			ushort memberId = module.GetOrAddMemberRef(instruction.ElemRef);
			ByteUtils.WriteToBuffer(bytes, 5, memberId);

			ushort typeid = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 7, typeid);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeUnionElemAddr(ILUnionElemAddrInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[9];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.UnionElemAddr];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort tupleId = valueIdMapping[instruction.Union.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, tupleId);

			ushort memberId = module.GetOrAddMemberRef(instruction.ElemRef);
			ByteUtils.WriteToBuffer(bytes, 5, memberId);

			ushort typeid = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 7, typeid);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeEnumValue(ILEnumValueInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte opCount = (byte)instruction.Operands.Count;

			byte[] bytes = new byte[8 + 2 * opCount];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.EnumValue];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort typeId = module.GetOrAddType(instruction.Type);
			ByteUtils.WriteToBuffer(bytes, 3, typeId);

			ushort memberId = module.GetOrAddMemberRef(instruction.ElemRef);
			ByteUtils.WriteToBuffer(bytes, 5, memberId);

			ByteUtils.WriteToBuffer(bytes, 7, opCount);

			int offset = 8;
			foreach (ILOperand operand in instruction.Operands)
			{
				ByteUtils.WriteToBuffer(bytes, offset, curValueId);
				offset += 2;

				valueIdMapping.Add(operand.Iden, curValueId);
				++curValueId;
			}

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeEnumGetValue(ILEnumGetValueInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[7];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.EnumGetValue];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort opId = valueIdMapping[instruction.Operand.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, opId);

			ushort typeId = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 5, typeId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeEnumGetMemberValue(ILEnumGetMemberValueInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[9];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.EnumGetMemberValue];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort typeId = module.GetOrAddType(instruction.Type);
			ByteUtils.WriteToBuffer(bytes, 3, typeId);

			ushort memberId = module.GetOrAddMemberRef(instruction.MemberRef);
			ByteUtils.WriteToBuffer(bytes, 5, memberId);

			ushort retTypeId = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 7, retTypeId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeGlobalAddr(ILGlobalAddrInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[5];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.GlobalAddr];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort globalId = module.GetOrAddGlobal(instruction.Identifier);
			ByteUtils.WriteToBuffer(bytes, 3, globalId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeGlobalValue(ILGlobalValueInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[5];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.GlobalValue];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort globalId = module.GetOrAddGlobal(instruction.Identifier);
			ByteUtils.WriteToBuffer(bytes, 3, globalId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeTemplateGetValue(ILTemplateGetValueInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[9];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.TemplateGetValue];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort typeId = module.GetOrAddType(instruction.TemplateType);
			ByteUtils.WriteToBuffer(bytes, 3, typeId);

			ushort memberId = module.GetOrAddMemberRef(instruction.ElemRef);
			ByteUtils.WriteToBuffer(bytes, 5, memberId);

			ushort retTypeId = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 7, retTypeId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeConvertType(ILConvertTypeInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[7];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.ConvType];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort opId = valueIdMapping[instruction.Operand.Iden];
			ByteUtils.WriteToBuffer(bytes, 3, opId);

			ushort typeId = module.GetOrAddType(instruction.RetOperand.Type);
			ByteUtils.WriteToBuffer(bytes, 5, typeId);

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeGetVTable(ILGetVTableInstruction instruction, ILByteCodeModule module, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
			byte[] bytes = new byte[6 + instruction.InterfaceTypes.Count * 2];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.GetVTable];

			ByteUtils.WriteToBuffer(bytes, 1, curValueId);

			ushort typeId = module.GetOrAddType(instruction.Type);
			ByteUtils.WriteToBuffer(bytes, 3, typeId);

			byte interfaceCount = (byte) instruction.InterfaceTypes.Count;
			ByteUtils.WriteToBuffer(bytes, 5, interfaceCount);

			int offset = 6;
			foreach (SymbolType interfaceType in instruction.InterfaceTypes)
			{
				ushort interfaceId = module.GetOrAddType(interfaceType);
				ByteUtils.WriteToBuffer(bytes, offset, interfaceId);
				offset += sizeof(ushort);
			}

			block.Instructions.Add(bytes);

			valueIdMapping.Add(instruction.RetOperand.Iden, curValueId);
			++curValueId;
		}

		public static void EncodeReturn(ILReturnInstruction instruction, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping)
		{
			byte[] bytes = new byte[3];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.Return];

			ushort opId = instruction.Operand == null ? ILByteCode.InvalidId : valueIdMapping[instruction.Operand.Iden];
			ByteUtils.WriteToBuffer(bytes, 1, opId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeBranch(ILBranchInstruction instruction, ILByteCodeFunction function, ILByteCodeBasicBlock block)
		{
			byte[] bytes = new byte[3];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.Return];

			ushort bbId = function.GetBasicBlockId(instruction.Label);
			ByteUtils.WriteToBuffer(bytes, 1, bbId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeCondBranch(ILCondBranchInstruction instruction, ILByteCodeFunction function, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping)
		{
			byte[] bytes = new byte[7];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.Return];

			ushort opId = valueIdMapping[instruction.Condition.Iden];
			ByteUtils.WriteToBuffer(bytes, 1, opId);

			ushort trueId = function.GetBasicBlockId(instruction.TrueLabel);
			ByteUtils.WriteToBuffer(bytes, 3, trueId);

			ushort falseId = function.GetBasicBlockId(instruction.FalseLabel);
			ByteUtils.WriteToBuffer(bytes, 5, falseId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeSwitchEnum(ILSwitchEnumInstruction instruction, ILByteCodeModule module, ILByteCodeFunction function, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping)
		{
			byte[] bytes = new byte[7 + instruction.Cases.Count * 4];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.SwitchEnum];

			ushort enumId = valueIdMapping[instruction.Enum.Iden];
			ByteUtils.WriteToBuffer(bytes, 1, enumId);

			ByteUtils.WriteToBuffer(bytes, 3, (ushort)instruction.Cases.Count);

			int offset = 5;
			if (instruction.Cases != null)
			{
				foreach (ILRefCase refCase in instruction.Cases)
				{
					ushort memberId = module.GetOrAddMemberRef(refCase.Reference);
					ByteUtils.WriteToBuffer(bytes, offset, memberId);
					offset += sizeof(ushort);

					ushort blockId = function.GetBasicBlockId(refCase.Label);
					ByteUtils.WriteToBuffer(bytes, offset, blockId);
					offset += sizeof(ushort);
				}
			}

			ushort defaultId = instruction.DefaultLabel == null ? ILByteCode.InvalidId : function.GetBasicBlockId(instruction.DefaultLabel);
			ByteUtils.WriteToBuffer(bytes, offset, defaultId);

			block.Instructions.Add(bytes);
		}

		public static void EncodeSwitchValue(ILSwitchValueInstruction instruction, ILByteCodeModule module, ILByteCodeFunction function, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping)
		{
			byte[] bytes = new byte[7 + instruction.Cases.Count * 4];
			bytes[0] = ILByteCode.InstructionTypeToId[ILInstructionType.SwitchEnum];

			ushort enumId = valueIdMapping[instruction.Value.Iden];
			ByteUtils.WriteToBuffer(bytes, 1, enumId);

			ByteUtils.WriteToBuffer(bytes, 3, (ushort)instruction.Cases.Count);

			int offset = 5;
			if (instruction.Cases != null)
			{
				foreach (ILValCase valCase in instruction.Cases)
				{
					ushort valId = valueIdMapping[valCase.Value.Iden];
					ByteUtils.WriteToBuffer(bytes, offset, valId);
					offset += sizeof(ushort);

					ushort blockId = function.GetBasicBlockId(valCase.Label);
					ByteUtils.WriteToBuffer(bytes, offset, blockId);
					offset += sizeof(ushort);
				}
			}

			ushort defaultId = instruction.DefaultLabel == null ? ILByteCode.InvalidId : function.GetBasicBlockId(instruction.DefaultLabel);
			ByteUtils.WriteToBuffer(bytes, offset, defaultId);

			block.Instructions.Add(bytes);
		}

		public static void Encode(ILInstruction instruction, ILByteCodeModule module, ILByteCodeFunction function, ILByteCodeBasicBlock block, Dictionary<string, ushort> valueIdMapping, ref ushort curValueId)
		{
		}


	}
}
