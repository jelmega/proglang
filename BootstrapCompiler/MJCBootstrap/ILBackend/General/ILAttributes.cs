﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC.ILBackend.General
{
	public enum ILLinkage
	{
		Export,
		Import,
		Public,
		PublicExternal,
		Hidden,
		HiddenExternal,
		Shared,
		Private,
	}

	public enum ILCallConv
	{
		None,
		MJay,
		C,
		Windows,
	}

	public enum ILInline
	{
		Unspecified,
		Never,
		Prefered,
		Always,

		Count
	}

	[Flags]
	public enum ILGlobalFlags : byte
	{
		None = 0b0000_0000,
		CompileConst = 0b0000_0001,
		Const = 0b0000_0010,
		Immutable = 0b0000_0100,
		Tls = 0b0000_1000,
		Synchronized = 0b0001_0000,
		Shared = 0b0010_0000
	}


	public class ILAttributes
	{
		public ILLinkage Linkage = ILLinkage.Private;
		
		public ILInline Inline;

		public int Alignment;

		public ILCallConv CallConv;

		public ILGlobalFlags GlobalFlags;

		public string InterpFunc;

		public ILAttributes()
		{
		}

		public ILAttributes(ILLinkage linkage)
		{

		}

		public ILAttributes(ILAttributes other, bool external)
		{
			Alignment = other.Alignment;
			CallConv = other.CallConv;

			if (external)
			{
				if (other.Linkage == ILLinkage.Public || other.Linkage == ILLinkage.PublicExternal)
					Linkage = ILLinkage.PublicExternal;
				else if (other.Linkage == ILLinkage.Export)
					Linkage = ILLinkage.Export;
				else
					Linkage = ILLinkage.HiddenExternal;
			}
			else
			{
				Linkage = other.Linkage;
				Inline = other.Inline;
			}
		}

		public ILAttributes(ILAttributes other)
		{
			Linkage = other.Linkage;
			Inline = other.Inline;
			Alignment = other.Alignment;
			CallConv = other.CallConv;
		}


		public override string ToString()
		{
			string str = "";

			str += ILHelpers.LinkageToString(Linkage);

			string inlineStr = ILHelpers.InlineToString(Inline);
			if (inlineStr != "")
				str += $" {inlineStr}";

			if (Alignment != 0)
				str += $" align({Alignment})";

			if (CallConv != ILCallConv.None)
				str += $" call_conv({CallConv.ToString().ToLower()})";

			if (GlobalFlags != ILGlobalFlags.None)
			{
				string flagStr = "";
				if ((GlobalFlags & ILGlobalFlags.CompileConst) != 0)
					flagStr += " constant";
				if ((GlobalFlags & ILGlobalFlags.Const) != 0)
					flagStr += " compile_const";
				if ((GlobalFlags & ILGlobalFlags.Immutable) != 0)
					flagStr += " immutable";
				if ((GlobalFlags & ILGlobalFlags.Tls) != 0)
					flagStr += " tls";
				if ((GlobalFlags & ILGlobalFlags.Synchronized) != 0)
					flagStr += " sync";
				if ((GlobalFlags & ILGlobalFlags.Shared) != 0)
					flagStr += " shared";
			}

			return str;
		}
	}


	public static partial class ILHelpers
	{
		public static string LinkageToString(ILLinkage linkage)
		{
			switch (linkage)
			{
			case ILLinkage.Export:			return "export";
			case ILLinkage.Import:			return "import";
			case ILLinkage.Public:			return "public";
			case ILLinkage.PublicExternal:	return "public_external";
			case ILLinkage.Hidden:			return "hidden";
			case ILLinkage.HiddenExternal:	return "hidden_external";
			case ILLinkage.Shared:			return "shared";
			case ILLinkage.Private:			return "private";
			default:						return "";
			}
		}

		public static ILLinkage GetLinkage(SemanticVisibility visibility, bool foreign, bool dymanicallyLoaded, SemanticVisibility defaultVisitbility = SemanticVisibility.Private)
		{
			if (dymanicallyLoaded)
				return ILLinkage.Import;

			switch (visibility)
			{
			case SemanticVisibility.Default:
				return GetLinkage(defaultVisitbility, foreign, false);
			case SemanticVisibility.Private:
				return foreign ? ILLinkage.HiddenExternal : ILLinkage.Private;
			case SemanticVisibility.Internal:
				return foreign ? ILLinkage.HiddenExternal : ILLinkage.Hidden;
			case SemanticVisibility.Public:
				return foreign ? ILLinkage.PublicExternal : ILLinkage.Public;
			case SemanticVisibility.Export:
				return ILLinkage.Export;
			}

			return ILLinkage.Private;
		}

		public static string InlineToString(ILInline inline)
		{
			switch (inline)
			{
			case ILInline.Never:	return "noinline";
			case ILInline.Prefered: return "inline";
			case ILInline.Always:	return "alwaysinline";
			default:				return "";
			}
		}

		public static ILAttributes GetAttributes(Symbol symbol, Dictionary<string, CompileAttribute> moduleAttributes)
		{
			ILAttributes attribs = new ILAttributes();

			bool isForeign = symbol.CompileAttribs.ContainsKey(ForeignCompileAttribute.Id);
			bool isDynamicLoad = symbol.CompileAttribs.ContainsKey(DynamicallyLoadedCompileAttribute.Id);

			if (moduleAttributes != null && moduleAttributes.ContainsKey(DefaultVisibilityAttribute.Id))
			{
				DefaultVisibilityAttribute dva = moduleAttributes[DefaultVisibilityAttribute.Id] as DefaultVisibilityAttribute;
				attribs.Linkage = GetLinkage(symbol.Visibility, isForeign, isDynamicLoad, dva.DefaultVisibility);
			}
			else
			{
				attribs.Linkage = GetLinkage(symbol.Visibility, isForeign, isDynamicLoad);
			}


			if (symbol.CompileAttribs.ContainsKey(AlignCompileAttribute.Id))
			{
				AlignCompileAttribute align = symbol.CompileAttribs[AlignCompileAttribute.Id] as AlignCompileAttribute;
				attribs.Alignment = align.Alignment;
			}

			if (symbol.CompileAttribs.TryGetValue(CallingConventionAttribute.Id, out CompileAttribute tmpCallConv))
			{
				CallingConventionAttribute callConv = tmpCallConv as CallingConventionAttribute;

				switch (callConv.CallConv)
				{
				case "c":
					attribs.CallConv = ILCallConv.C;
					break;
				case "windows":
					attribs.CallConv = ILCallConv.Windows;
					break;
				default:
					attribs.CallConv = ILCallConv.MJay;
					break;
				}
			}

			if (symbol.CompileAttribs.TryGetValue(InterpFuncAttribute.Id, out CompileAttribute tmpInterpFunc))
			{
				InterpFuncAttribute interpFunc = tmpInterpFunc as InterpFuncAttribute;
				attribs.InterpFunc = interpFunc.FuncName;
			}

			return attribs;
		}
	}
}
