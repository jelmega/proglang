﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.General
{
	public static class ILTypeInfoHelpers
	{

		public static TypeInfo GetMinimalAggrTypeInfo(ILAggregate aggr, ILCompUnit compUnit, CompilerContext context)
		{
			string mangle = aggr.MangledIdentifier;

			TypeInfo typeInfo = context.GetTypeInfo(mangle);
			if (typeInfo != null)
				return typeInfo;

			byte align = 0;
			ulong size = 0;
			foreach (ILAggregateVariable variable in aggr.Variables)
			{
				TypeInfo varInfo = context.GetTypeInfo(NameMangling.MangleType(variable.Type));
				if (varInfo == null)
					varInfo = GetMinimalTypeInfo(variable.Type, compUnit, context);

				if (align < varInfo.Alignment)
					align = varInfo.Alignment;

				ulong diff = size % align;
				if (diff != 0)
					size += (align - diff);

				size += varInfo.Size;
			}

			typeInfo = new TypeInfo(align, size);
			context.AddTypeInfo(mangle, typeInfo);
			return typeInfo;
		}

		public static TypeInfo GetMinimalEnumTypeInfo(ILEnum ilEnum, ILCompUnit compUnit, CompilerContext context)
		{
			string mangle = ilEnum.MangledIdentifier;

			TypeInfo typeInfo = context.GetTypeInfo(mangle);
			if (typeInfo != null)
				return typeInfo;

			TypeInfo baseInfo = context.GetTypeInfo(NameMangling.MangleType(ilEnum.BaseType));

			if (!ilEnum.IsADT)
			{
				context.AddTypeInfo(mangle, baseInfo);
				return baseInfo;
			}

			byte align = 0;
			ulong size = 0;
			foreach (ILEnumMember member in ilEnum.Members)
			{
				byte tmpAlign = baseInfo.Alignment;
				ulong tmpSize = baseInfo.Size;

				if (member.AdtType == null)
				{
					if (align < tmpAlign)
						align = tmpAlign;
					if (size < tmpSize)
						size = tmpSize;
					continue;
				}

				TypeInfo varInfo = context.GetTypeInfo(NameMangling.MangleType(member.AdtType));

				if (varInfo == null)
					varInfo = GetMinimalTypeInfo(member.AdtType, compUnit, context);

				if (tmpAlign < varInfo.Alignment)
					tmpAlign = varInfo.Alignment;

				ulong diff = tmpSize % tmpAlign;
				if (diff != 0)
					tmpSize += (tmpAlign - diff);

				tmpSize += varInfo.Size;

				if (align < tmpAlign)
					align = tmpAlign;
				if (size < tmpSize)
					size = tmpSize;
			}

			typeInfo = new TypeInfo(align, size);
			context.AddTypeInfo(mangle, typeInfo);
			return typeInfo;
		}

		public static TypeInfo GetMinimalTypeInfo(SymbolType type, ILCompUnit compUnit, CompilerContext context)
		{
			switch (type)
			{
			case PointerSymbolType _:
				return context.GetTypeInfo("P");
			case ReferenceSymbolType _:
				return context.GetTypeInfo("P");
			case BuiltinSymbolType builtin:
			{
				string iden = NameMangling.MangleType(builtin);
				return context.GetTypeInfo(iden);
			}
			case AggregateSymbolType aggrType:
			{
				ILAggregate aggr = compUnit.GetAggregate(aggrType.MangledIdentifier);
				return GetMinimalAggrTypeInfo(aggr, compUnit, context);
			}
			case EnumSymbolType enumType:
			{
				ILEnum ilEnum = compUnit.GetEnum(enumType.MangledIdentifier);
				return GetMinimalEnumTypeInfo(ilEnum, compUnit, context);
			}
			case ArraySymbolType arrType:
			{
				if (arrType.ArraySize != ulong.MaxValue)
				{
					TypeInfo tmp = GetMinimalTypeInfo(arrType.BaseType, compUnit, context);
					return new TypeInfo(tmp.Alignment, tmp.Size * arrType.ArraySize);
				}

				return context.GetTypeInfo("A");
			}
			case TupleSymbolType tupleType:
			{
				string iden = "__tup_" + NameMangling.MangleType(tupleType);
				TypeInfo info = context.GetTypeInfo(iden);
				if (info != null)
					return info;

				byte align = 0;
				ulong size = 0;
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					TypeInfo tmp = GetMinimalTypeInfo(subType, compUnit, context);

					if (align < tmp.Alignment)
						align = tmp.Alignment;

					ulong diff = size % align;
					if (diff != 0)
						size += (align - diff);

					size += tmp.Size;
				}

				info = new TypeInfo(align, size);
				context.AddTypeInfo(iden, info);
				return info;
			}
			}

			return null;
		}

	}
}
