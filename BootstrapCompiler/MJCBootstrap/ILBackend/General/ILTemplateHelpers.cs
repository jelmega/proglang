﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.General
{
	public static class ILTemplateHelpers
	{
		public static ILAggregate GetBestBaseAggregate(ILTemplateInstance instance, ILCompUnit compUnit)
		{
			ScopeVariable baseIden = instance.Iden.GetBaseTemplate();
			List<ILAggregate> variants = compUnit.GetAggregateVariants(baseIden);

			if (variants.Count == 1)
				return variants[0];

			foreach (ILAggregate aggregate in variants)
			{
				if (aggregate.Symbol == instance.BestTemplateDefinition)
					return aggregate;
			}
			return null;
		}

		public static ILEnum GetBestBaseEnum(ILTemplateInstance instance, ILCompUnit compUnit)
		{
			ScopeVariable baseIden = instance.Iden.GetBaseTemplate();
			List<ILEnum> variants = compUnit.GetEnumVariants(baseIden);

			if (variants.Count == 1)
				return variants[0];

			foreach (ILEnum ilEnum in variants)
			{
				if (ilEnum.Symbol == instance.BestTemplateDefinition)
					return ilEnum;
			}
			return null;
		}

		public static SymbolType GetInstancedType(SymbolType type, ILTemplateInstance instance)
		{
			switch (type)
			{
			case BuiltinSymbolType builtinType:
				return builtinType;
			case PointerSymbolType ptrType:
			{
				SymbolType baseType = GetInstancedType(ptrType.BaseType, instance);
				return SymbolType.PointerType(baseType, ptrType.Attributes);
			}
			case ReferenceSymbolType refType:
			{
				SymbolType baseType = GetInstancedType(refType.BaseType, instance);
				return SymbolType.ReferenceType(baseType, refType.Attributes);
			}
			case MemoryLocSymbolType memLocType:
			{
				SymbolType baseType = GetInstancedType(memLocType.BaseType, instance);
				return SymbolType.MemoryLocType(baseType, memLocType.Attributes);
			}
			case AggregateSymbolType aggrType:
			{
				Identifier iden = aggrType.GetIdentifier();

				if (iden is TemplateDefinitionIdentifier defName)
				{
					Scope scope = GetInstancedScope(aggrType.Identifier.Scope, instance);
					TemplateInstanceIdentifier instIden = new TemplateInstanceIdentifier(defName);
					for (var i = 0; i < defName.Parameters.Count; i++)
					{
						TemplateParameter param = defName.Parameters[i];
						ILTemplateArg ilArg = instance.Arguments[param.Index];
						instIden.Arguments[i].Type = ilArg.Type;
						instIden.Arguments[i].Value = ilArg.Value;
					}
					ScopeVariable scopeVar = new ScopeVariable(scope, instIden);
					TemplateInstanceSymbolType instType = SymbolType.TemplateInstanceType(scopeVar, aggrType.Symbol, type.Attributes);

					if (instType.MangledName == null)
						instType.MangledName = "_M" + NameMangling.MangleType(instType) + "TJ";

					return instType;
				}

				return aggrType;
			}
			case FunctionSymbolType funcType:
			{
				List<SymbolType> paramTypes = null;
				if (funcType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType baseParamType in funcType.ParamTypes)
					{
						SymbolType paramType = GetInstancedType(baseParamType, instance);
						paramTypes.Add(paramType);
					}
				}

				SymbolType recType = GetInstancedType(funcType.ReceiverType, instance);
				SymbolType resType = GetInstancedType(funcType.ReturnType, instance);

				return SymbolType.FunctionType(recType, paramTypes, resType, funcType.Attributes);
			}
			case TupleSymbolType tupleType:
			{
				List<SymbolType> subTypes = new List<SymbolType>();
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					SymbolType tmp = GetInstancedType(subType, instance);
					subTypes.Add(tmp);
				}

				return SymbolType.TupleType(subTypes, tupleType.Attributes);
			}
			case TemplateParamSymbolType templateParamType:
			{
				int idx = templateParamType.Index;
				ILTemplateArg arg = instance.Arguments[idx];
				return SymbolType.AttributedType(arg.Type, templateParamType.Attributes);
			}
			case TemplateInstanceSymbolType templateInstType:
			{
				TemplateInstanceIdentifier typeIden = templateInstType.Iden.Name as TemplateInstanceIdentifier;

				List<Symbol> paramSyms = templateInstType.TemplateSymbol.GetChildrenOfKind(SymbolKind.TemplateParam);

				bool isSelfInst = true;
				for (var i = 0; i < instance.Arguments.Count; i++)
				{
					ILTemplateArg instArg = instance.Arguments[i];
					TemplateArgument typeArg = typeIden.Arguments[i];

					Symbol paramSym = paramSyms.Find(s =>
					{
						if (s.TemplateIdx == i)
							return true;
						return false;
					});

					if (instArg.ValueFunc == null && instArg.Value == null)
					{
						if (typeArg.Type is TemplateParamSymbolType tparamType)
						{
							if (tparamType.Index != i)
								isSelfInst = false;
						}
						else
						{
							TemplateParamSymbolType paramType = paramSym.Type as TemplateParamSymbolType;
							if (paramType != typeArg.Type && typeArg.AssociatedBaseType == null)
								isSelfInst = false;

							// Check associated type
							if (typeArg.AssociatedBaseType != null)
							{
								if (paramType != typeArg.AssociatedBaseType)
									isSelfInst = false;
							}
						}

					}
					else
					{
						if (paramSym.Type is TemplateParamSymbolType)
							isSelfInst = false;
					}
				}


				if (isSelfInst)
					return instance.Type;
				return templateInstType;
			}
			default:
				return null;
			}
		}

		public static ILOperand GetInstancedOperand(ILOperand operand, ILTemplateInstance instance)
		{
			SymbolType type = GetInstancedType(operand.Type, instance);
			return new ILOperand(operand.Iden, type);
		}

		public static TemplateInstanceIdentifier GetInstanceName(TemplateDefinitionIdentifier defIden, List<ILTemplateArg> ilArgs)
		{
			TemplateInstanceIdentifier instIden = new TemplateInstanceIdentifier(defIden);

			for (var i = 0; i < ilArgs.Count; i++)
			{
				ILTemplateArg ilArg = ilArgs[i];

				TemplateArgument arg = new TemplateArgument(i, ilArg.Type, ilArg.Value);
				instIden.Arguments[i] = arg;
			}

			return instIden;
		}

		public static Scope GetInstancedScope(Scope defScope, ILTemplateInstance instance)
		{
			Scope scope = new Scope();

			foreach (Identifier iden in defScope.Names)
			{
				if (iden is TemplateDefinitionIdentifier defName)
				{
					TemplateInstanceIdentifier instIden = new TemplateInstanceIdentifier(defName);
					for (var i = 0; i < defName.Parameters.Count; i++)
					{
						TemplateParameter param = defName.Parameters[i];
						ILTemplateArg ilArg = instance.Arguments[param.Index];
						instIden.Arguments[i].Type = ilArg.Type;
					}
					scope.Names.Add(instIden);
				}
				else
				{
					scope.Names.Add(iden);
				}
			}

			return scope;
		}
	}
}
