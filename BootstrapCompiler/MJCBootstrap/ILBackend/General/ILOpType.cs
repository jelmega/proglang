﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.General
{
	public enum ILOpLoc
	{
		Unkown,
		Prefix,
		Infix,
		Postfix,
		Cast
	}

	public enum ILOpType
	{
		Unkown,
		Add,
		Inc,
		Sub,
		Dec,
		Mul,
		Div,
		Rem,
		Shl,
		LShr,
		AShr,
		BOr,
		BXor,
		BAnd,
		LOr,
		LAnd,
		Pow,
		Concat,
		Index,
		Not,

		Assign,
		AddAssign,
		SubAssign,
		MulAssign,
		DivAssign,
		RemAssign,
		ShlAssign,
		LShrAssign,
		AShrAssign,
		OrAssign,
		XorAssign,
		AndAssign,
		PowAssign,
		ConcatAssign,
		IndexAssign,

		Eq,
		Ne,
		Lt,
		Lte,
		Gt,
		Gte,

		Static,
		Bit
	}

	public static partial class ILHelpers
	{
		public static ILOpType GetILOpType(string str)
		{
			switch (str)
			{
			case "+":		return ILOpType.Add;
			case "++":		return ILOpType.Inc;
			case "-":		return ILOpType.Sub;
			case "--":		return ILOpType.Dec;
			case "*":		return ILOpType.Mul;
			case "/":		return ILOpType.Div;
			case "%":		return ILOpType.Rem;
			case "<<":		return ILOpType.Shl;
			case ">>":		return ILOpType.LShr;
			case ">>>":		return ILOpType.AShr;
			case "|":		return ILOpType.BOr;
			case "^":		return ILOpType.BXor;
			case "&":		return ILOpType.BAnd;
			case "||":		return ILOpType.LOr;
			case "&&":		return ILOpType.LAnd;
			case "^^":		return ILOpType.Pow;
			case "~":		return ILOpType.Concat;
			case "[]":		return ILOpType.Index;
			case "!":		return ILOpType.Not;

			case "=":		return ILOpType.Assign;
			case "+=":		return ILOpType.AddAssign;
			case "-=":		return ILOpType.SubAssign;
			case "*=":		return ILOpType.MulAssign;
			case "/=":		return ILOpType.DivAssign;
			case "%=":		return ILOpType.RemAssign;
			case "<<=":		return ILOpType.ShlAssign;
			case ">>=":		return ILOpType.LShrAssign;
			case ">>>=":	return ILOpType.AShrAssign;
			case "|=":		return ILOpType.OrAssign;
			case "^=":		return ILOpType.XorAssign;
			case "&=":		return ILOpType.AndAssign;
			case "^^=":		return ILOpType.PowAssign;
			case "~=":		return ILOpType.ConcatAssign;
			case "[]=":		return ILOpType.IndexAssign;

			case "==":		return ILOpType.Eq;
			case "!=":		return ILOpType.Ne;
			case "<":		return ILOpType.Lt;
			case "<=":		return ILOpType.Lte;
			case ">":		return ILOpType.Gt;
			case ">=":		return ILOpType.Gte;

			case "static":	return ILOpType.Static;
			case "bit":		return ILOpType.Bit;

			default:		return ILOpType.Unkown;
			}
		}

		public static ILOpLoc GetILOpLoc(string str)
		{
			switch (str)
			{
			case "prefix":	return ILOpLoc.Prefix;
			case "infix":	return ILOpLoc.Infix;
			case "postfix": return ILOpLoc.Postfix;
			case "cast":	return ILOpLoc.Cast;
			default:		return ILOpLoc.Unkown;
			}
		}

		public static (ILOpLoc, ILOpType) GetILOpLocAndType(string str)
		{
			string[] parts = str.Split('_');
			return (GetILOpLoc(parts[0]), GetILOpType(parts[1]));
		}

		public static string GetIlOpName(ILOpLoc loc, ILOpType type)
		{
			string opLoc;
			switch (loc)
			{
			default:
			case ILOpLoc.Unkown:  opLoc = "unknown"; break;
			case ILOpLoc.Prefix:  opLoc = "prefix";  break;
			case ILOpLoc.Infix:   opLoc = "infix";   break;
			case ILOpLoc.Postfix: opLoc = "postfix"; break;
			case ILOpLoc.Cast:    opLoc = "cast"; break;
			}


			string opName;
			switch (type)
			{
			default:
			case ILOpType.Unkown:		opName =  "unknown";	break;
			case ILOpType.Add:			opName =  "+";			break;
			case ILOpType.Inc:			opName =  "++";			break;
			case ILOpType.Sub:			opName =  "-";			break;
			case ILOpType.Dec:			opName =  "--";			break;
			case ILOpType.Mul:			opName =  "*";			break;
			case ILOpType.Div:			opName =  "/";			break;
			case ILOpType.Rem:			opName =  "%";			break;
			case ILOpType.Shl:			opName =  "<<";			break;
			case ILOpType.LShr:			opName =  ">>";			break;
			case ILOpType.AShr:			opName =  ">>>";		break;
			case ILOpType.BOr:			opName =  "|";			break;
			case ILOpType.BXor:			opName =  "^";			break;
			case ILOpType.BAnd:			opName =  "&";			break;
			case ILOpType.LOr:			opName =  "||";			break;
			case ILOpType.LAnd:			opName =  "&&";			break;
			case ILOpType.Pow:			opName =  "^^";			break;
			case ILOpType.Concat:		opName =  "~";			break;
			case ILOpType.Index:		opName =  "[]";			break;
			case ILOpType.Not:			opName =  "!";			break;
			case ILOpType.Assign:		opName =  "=";			break;
			case ILOpType.AddAssign:	opName =  "+=";			break;
			case ILOpType.SubAssign:	opName =  "-=";			break;
			case ILOpType.MulAssign:	opName =  "*=";			break;
			case ILOpType.DivAssign:	opName =  "/=";			break;
			case ILOpType.RemAssign:	opName =  "%=";			break;
			case ILOpType.ShlAssign:	opName =  "<<=";		break;
			case ILOpType.LShrAssign:	opName =  ">>=";		break;
			case ILOpType.AShrAssign:	opName =  ">>>=";		break;
			case ILOpType.OrAssign:		opName =  "|=";			break;
			case ILOpType.XorAssign:	opName =  "^=";			break;
			case ILOpType.AndAssign:	opName =  "&=";			break;
			case ILOpType.PowAssign:	opName =  "^^=";		break;
			case ILOpType.ConcatAssign:	opName =  "~=";			break;
			case ILOpType.IndexAssign:	opName =  "[]=";		break;
			case ILOpType.Eq:			opName =  "==";			break;
			case ILOpType.Ne:			opName =  "!=";			break;
			case ILOpType.Lt:			opName =  "<";			break;
			case ILOpType.Lte:		opName =  "<=";			break;
			case ILOpType.Gt:		opName =  ">";			break;
			case ILOpType.Gte:	opName =  ">=";			break;
			case ILOpType.Static:		opName =  "static";		break;
			case ILOpType.Bit:			opName =  "bit";		break;
			}

			return $"{opLoc}_{opName}";
		}
	}

	
}
