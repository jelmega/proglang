﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.General
{
	public class ILTemplateParam
	{
		public string ValueName;
		public SymbolType Type;
		public SymbolType TypeSpecialization;
		public string ValueSpecialization;
		public string ValueSpecializationFunc;
		public string ValueDefault;
		public string ValueDefaultFunc;

		public ILTemplateParam(string iden, SymbolType type, SymbolType typeSpecialization, string valueSpecializationFunc, string valueDefaultFunc)
		{
			ValueName = iden;
			Type = type;
			TypeSpecialization = typeSpecialization;
			ValueSpecializationFunc = valueSpecializationFunc;
			ValueDefaultFunc = valueDefaultFunc;
		}

		public override string ToString()
		{
			string str;
			if (TypeSpecialization != null)
			{
				str = $"${TypeSpecialization}";
			}
			else if (ValueSpecializationFunc != null)
			{
				str = $"@{ValueSpecializationFunc} : ${Type}";
			}
			else if (ValueSpecialization != null)
			{
				str = $"%{ValueSpecialization} : ${Type}";
			}
			else
			{
				str = "";
				if (ValueName != null)
					str += $"%{ValueName} : ";
				str += $"${Type}";
			}

			return str;
		}
	}

	public class ILTemplateArg
	{
		public SymbolType Type;
		public string ValueFunc;
		public string Value;

		public ILTemplateArg(SymbolType type, string valFunc)
		{
			Type = type;
			ValueFunc = valFunc;
		}

		public override string ToString()
		{
			if (Value != null)
				return $"{Value} : {Type}";
			if (ValueFunc != null)
				return $"@{ValueFunc} : {Type}";
			return Type.ToString();
		}

		public string ToILString()
		{
			if (Value != null)
				return $"{Value} : ${Type}";
			if (ValueFunc != null)
				return $"@{ValueFunc} : ${Type}";
			return $"${Type.ToILString()}";
		}
	}

	public class ILTemplateInstance
	{
		public ScopeVariable Iden;
		public string MangledName; // Generated in ILBackend
		public TemplateInstanceSymbolType Type;
		public List<ILTemplateArg> Arguments;
		public Symbol BestTemplateDefinition;

		public ILTemplateInstance(ScopeVariable iden, TemplateInstanceSymbolType type, List<ILTemplateArg> args)
		{
			Iden = iden;
			Type = type;
			Arguments = args;
		}

		public bool IsTypeInstance()
		{
			return Type.TemplateSymbol.Kind != SymbolKind.Function && Type.TemplateSymbol.Kind != SymbolKind.Method;
		}
	}

}
