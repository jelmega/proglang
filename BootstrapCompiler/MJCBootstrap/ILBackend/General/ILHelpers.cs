﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.General
{
	public static partial class ILHelpers
	{
		public static string GetTypedIlOpName(ILOpLoc loc, ILOpType type, List<SymbolType> paramTypes, SymbolType retType)
		{
			string iden = GetIlOpName(loc, type) + '(';

			for (var i = 0; i < paramTypes.Count; i++)
			{
				iden += paramTypes[i].ToString();
				if (i + 1 != paramTypes.Count)
					iden += ",";
			}

			iden += "):";
			iden += retType.ToString();

			return iden;
		}

		public static string GetTypedIlOpName(ILOperatorRefInstruction op)
		{
			string iden = GetIlOpName(op.Loc, op.Op) + '(';

			FunctionSymbolType opType = op.Type;
			for (var i = 0; i < opType.ParamTypes.Count; i++)
			{
				iden += opType.ParamTypes[i].ToString();
				if (i + 1 != opType.ParamTypes.Count)
					iden += ",";
			}

			iden += "):";
			iden += (op.RetOperand.Type as FunctionSymbolType).ReturnType.ToString();

			return iden;
		}

		public static string GetMangledIlOpName(ILOperatorRefInstruction op)
		{
			string iden = GetIlOpName(op.Loc, op.Op);
			string mangledType = NameMangling.MangleType(op.Type);
			return $"_M{iden.Length}{iden}{mangledType}";
		}

		public static SymbolType GetTypeWithoutAttribs(SymbolType type)
		{
			switch (type)
			{
			default:
			case AggregateSymbolType _:
			case ArraySymbolType _:
			case BuiltinSymbolType _:
			case EnumSymbolType _:
			case InterfaceSymbolType _:
			case TemplateInstanceSymbolType _:
			case TemplateParamSymbolType _:
				return SymbolType.AttributedType(type, TypeAttributes.None);

			case BitFieldSymbolType bitFieldType:
			{
				SymbolType baseType = GetTypeWithoutAttribs(bitFieldType.BaseType);
				return SymbolType.BitFieldType(baseType, bitFieldType.Bits);
			}
			case DelegateSymbolType delType:
			{
				List<SymbolType> paramTypes = null;
				if (delType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType paramType in delType.ParamTypes)
					{
						SymbolType tmp = GetTypeWithoutAttribs(paramType);
						paramTypes.Add(tmp);
					}
				}
				
				SymbolType retType = GetTypeWithoutAttribs(delType.ReturnType);

				return SymbolType.DelegateType(delType.Identifier, paramTypes, retType);
			}
			case FunctionSymbolType funcType:
			{
				List<SymbolType> paramTypes = null;
				if (funcType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType paramType in funcType.ParamTypes)
					{
						SymbolType tmp = GetTypeWithoutAttribs(paramType);
						paramTypes.Add(tmp);
					}
				}

				SymbolType recType = GetTypeWithoutAttribs(funcType.ReceiverType);
				SymbolType retType = GetTypeWithoutAttribs(funcType.ReturnType);

				return SymbolType.FunctionType(recType, paramTypes, retType);
			}
			case MemoryLocSymbolType memoryLocType:
			{
				SymbolType baseType = GetTypeWithoutAttribs(memoryLocType.BaseType);
				return SymbolType.MemoryLocType(baseType);
			}
			case NullableSymbolType nullType:
			{
				SymbolType baseType = GetTypeWithoutAttribs(nullType.BaseType);
				return SymbolType.NullableType(baseType);
			}
			case PointerSymbolType ptrType:
			{
				SymbolType baseType = GetTypeWithoutAttribs(ptrType.BaseType);
				return SymbolType.PointerType(baseType);
			}
			case ReferenceSymbolType refType:
			{
				SymbolType baseType = GetTypeWithoutAttribs(refType.BaseType);
				return SymbolType.ReferenceType(baseType);
			}
			case TupleSymbolType tupleType:
			{
				List<SymbolType> subTypes = new List<SymbolType>();
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					SymbolType tmp = GetTypeWithoutAttribs(subType);
					subTypes.Add(tmp);
				}
				return SymbolType.TupleType(subTypes);
			}
			case VariadicSymbolType variadicType:
			{
				SymbolType baseType = GetTypeWithoutAttribs(variadicType.BaseType);
				return SymbolType.VariadicType(baseType);
			}
			}
		}

		public static SymbolType GetArrayReplacedType(SymbolType type, ILCompUnit compUnit)
		{
			switch (type)
			{
			default:
				return type;
			case ArraySymbolType arrType:
			{
				if ((arrType.Attributes & TypeAttributes.StackArray) != 0)
					return arrType;

				TemplateInstanceSymbolType instType;
				if (arrType.ArraySizeFunc != null)
				{
					instType = compUnit.FixedSizeArrayTypes[arrType];
				}
				else
				{
					instType = compUnit.ArrayTypes[arrType.BaseType];
				}
				return instType;
			}
			case BitFieldSymbolType bitFieldType:
			{
				SymbolType baseType = GetArrayReplacedType(bitFieldType.BaseType, compUnit);
				return SymbolType.BitFieldType(baseType, bitFieldType.Bits);
			}
			case DelegateSymbolType delType:
			{
				List<SymbolType> paramTypes = null;
				if (delType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType paramType in delType.ParamTypes)
					{
						SymbolType tmp = GetArrayReplacedType(paramType, compUnit);
						paramTypes.Add(tmp);
					}
				}

				SymbolType retType = delType.ReturnType == null ? null : GetArrayReplacedType(delType.ReturnType, compUnit);

				return SymbolType.DelegateType(delType.Identifier, paramTypes, retType);
			}
			case FunctionSymbolType funcType:
			{
				List<SymbolType> paramTypes = null;
				if (funcType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType paramType in funcType.ParamTypes)
					{
						SymbolType tmp = GetArrayReplacedType(paramType, compUnit);
						paramTypes.Add(tmp);
					}
				}

				SymbolType recType = funcType.ReceiverType == null ? null : GetArrayReplacedType(funcType.ReceiverType, compUnit);
				SymbolType retType = funcType.ReturnType == null ? null : GetArrayReplacedType(funcType.ReturnType, compUnit);

				return SymbolType.FunctionType(recType, paramTypes, retType);
			}
			case MemoryLocSymbolType memoryLocType:
			{
				SymbolType baseType = GetArrayReplacedType(memoryLocType.BaseType, compUnit);
				return SymbolType.MemoryLocType(baseType);
			}
			case NullableSymbolType nullType:
			{
				SymbolType baseType = GetArrayReplacedType(nullType.BaseType, compUnit);
				return SymbolType.NullableType(baseType);
			}
			case PointerSymbolType ptrType:
			{
				SymbolType baseType = GetArrayReplacedType(ptrType.BaseType, compUnit);
				return SymbolType.PointerType(baseType);
			}
			case ReferenceSymbolType refType:
			{
				SymbolType baseType = GetArrayReplacedType(refType.BaseType, compUnit);
				return SymbolType.ReferenceType(baseType);
			}
			case TupleSymbolType tupleType:
			{
				List<SymbolType> subTypes = new List<SymbolType>();
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					SymbolType tmp = GetArrayReplacedType(subType, compUnit);
					subTypes.Add(tmp);
				}

				return SymbolType.TupleType(subTypes);
			}
			case VariadicSymbolType variadicType:
			{
				SymbolType baseType = GetArrayReplacedType(variadicType.BaseType, compUnit);
				return SymbolType.VariadicType(baseType);
			}
			case TemplateInstanceSymbolType instType:
			{
				TemplateInstanceIdentifier origIden = instType.Iden.Name as TemplateInstanceIdentifier;
				TemplateInstanceIdentifier iden = new TemplateInstanceIdentifier(origIden.Iden, origIden.Arguments.Count);

				for (var i = 0; i < origIden.Arguments.Count; i++)
				{
					TemplateArgument origArg = origIden.Arguments[i];
					TemplateArgument arg = iden.Arguments[i];

					arg.Type = GetArrayReplacedType(origArg.Type, compUnit);
					arg.AssociatedBaseType = origArg.AssociatedBaseType;
					arg.Index = origArg.Index;
					arg.Value = origArg.Value;
				}

				return SymbolType.TemplateInstanceType(instType.Iden.Scope, iden, instType.TemplateSymbol);
			}
			}
		}

		public static void ReplaceOperand(ILFunction function, ILOperand origOp, ILOperand newOp, int startBlock, int startInstr)
		{
			ILContext context = function.Context;
			for (int blockIdx = startBlock; blockIdx < function.Blocks.Count; blockIdx++)
			{
				ILBasicBlock bb = function.Blocks[blockIdx];
				int start = blockIdx == startBlock ? startInstr : 0;
				for (int instrIdx = start; instrIdx < bb.Instructions.Count; instrIdx++)
				{
					ILInstruction instruction = bb.Instructions[instrIdx];
					ReplaceOperand(instruction, context, origOp, newOp);
				}
			}
		}

		public static void ReplaceOperand(ILInstruction instruction, ILContext context, ILOperand origOp, ILOperand newOp)
		{
			switch (instruction.InstructionType)
			{
			case ILInstructionType.Unknown: break;
			case ILInstructionType.IntLiteral: break;
			case ILInstructionType.FloatLiteral: break;
			case ILInstructionType.StringLiteral: break;
			case ILInstructionType.AllocStack: break;
			case ILInstructionType.DeallocStack:
			{
				ILDeallocStackInstruction dealloc = instruction as ILDeallocStackInstruction;
				if (dealloc.Operand == origOp)
				{
					dealloc.Operand = newOp;
					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.Assign:
			{
				ILAssignInstruction assign = instruction as ILAssignInstruction;
				if (assign.Src == origOp)
				{
					assign.Src = newOp;
					ILVarContext dstCtx = context.GetVarContext(assign.Dst.Iden);
					dstCtx.DependentOn.Remove(origOp.Iden);
					dstCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				else if (assign.Dst == origOp)
				{
					assign.Dst = newOp;
					ILVarContext dstCtx = context.GetVarContext(newOp.Iden);
					dstCtx.DependentOn.Add(assign.Src.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.Store:
			{
				ILStoreInstruction store = instruction as ILStoreInstruction;
				if (store.Src == origOp)
				{
					store.Src = newOp;
					ILVarContext dstCtx = context.GetVarContext(store.Dst.Iden);
					dstCtx.DependentOn.Remove(origOp.Iden);
					dstCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				else if (store.Dst == origOp)
				{
					store.Dst = newOp;
					ILVarContext dstCtx = context.GetVarContext(store.Dst.Iden);
					dstCtx.DependentOn.Add(store.Src.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.Load:
			{
				ILLoadInstruction load = instruction as ILLoadInstruction;
				if (load.Src == origOp)
				{
					load.Src = newOp;
					ILVarContext retCtx = context.GetVarContext(load.RetOperand.Iden);
					retCtx.DependentOn.Remove(origOp.Iden);
					retCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.FunctionRef: break;
			case ILInstructionType.MethodRef: break;
			case ILInstructionType.OperatorRef: break;
			case ILInstructionType.Call:
			{
				ILCallInstruction call = instruction as ILCallInstruction;
				for (int i = 0; i < call.Operands.Count; i++)
				{
					if (call.Operands[i] == origOp)
					{
						call.Operands[i] = newOp;

						ILVarContext opContext = context.GetVarContext(newOp.Iden);
						++opContext.UseCount;

						if (call.RetOperand != null)
						{
							ILVarContext retCtx = context.GetVarContext(call.RetOperand.Iden);
							retCtx.DependentOn.Remove(origOp.Iden);
							retCtx.DependentOn.Add(newOp.Iden);
						}
					}
				}
				break;
			}
			case ILInstructionType.Builtin:
			{
				ILBuiltinInstruction builtin = instruction as ILBuiltinInstruction;
				for (int i = 0; i < builtin.Operands.Count; i++)
				{
					if (builtin.Operands[i] == origOp)
					{
						builtin.Operands[i] = newOp;

						ILVarContext opContext = context.GetVarContext(newOp.Iden);
						++opContext.UseCount;

						if (builtin.RetOperand != null)
						{
							ILVarContext retCtx = context.GetVarContext(builtin.RetOperand.Iden);
							retCtx.DependentOn.Remove(origOp.Iden);
							retCtx.DependentOn.Add(newOp.Iden);
						}
					}
				}
				break;
			}
			case ILInstructionType.CompilerIntrin:
				break;
			case ILInstructionType.TupleUnpack:
			{
				ILTupleUnpackInstruction unpack = instruction as ILTupleUnpackInstruction;
				if (unpack.Operand == origOp)
				{
					unpack.Operand = newOp;

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;

					foreach (ILOperand retOp in unpack.RetOperands)
					{
						ILVarContext retCtx = context.GetVarContext(retOp.Iden);
						retCtx.DependentOn.Remove(origOp.Iden);
						retCtx.DependentOn.Add(newOp.Iden);
					}
				}
				break;
			}
			case ILInstructionType.TuplePack:
			{
				ILTuplePackInstruction pack = instruction as ILTuplePackInstruction;
				for (int i = 0; i < pack.Operands.Count; i++)
				{
					if (pack.Operands[i] == origOp)
					{
						pack.Operands[i] = newOp;
						ILVarContext retCtx = context.GetVarContext(pack.RetOperand.Iden);
						retCtx.DependentOn.Remove(origOp.Iden);
						retCtx.DependentOn.Add(newOp.Iden);

						ILVarContext opContext = context.GetVarContext(newOp.Iden);
						++opContext.UseCount;
					}
				}
				break;
			}
			case ILInstructionType.TupleExtract:
			{
				ILTupleExtractInstruction extract = instruction as ILTupleExtractInstruction;
				if (extract.Tuple == origOp)
				{
					extract.Tuple = newOp;
					ILVarContext retCtx = context.GetVarContext(extract.RetOperand.Iden);
					retCtx.DependentOn.Remove(origOp.Iden);
					retCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.TupleInsert:
			{
				ILTupleInsertInstruction insert = instruction as ILTupleInsertInstruction;
				if (insert.Value == origOp)
				{
					insert.Value = newOp;
					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				else if (insert.Tuple == origOp)
				{
					insert.Tuple = newOp;
					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.TupleElemAddr:
			{
				ILTupleElemAddrInstruction elemAddr = instruction as ILTupleElemAddrInstruction;
				if (elemAddr.Tuple == origOp)
				{
					elemAddr.Tuple = newOp;
					ILVarContext retCtx = context.GetVarContext(elemAddr.RetOperand.Iden);
					retCtx.DependentOn.Remove(origOp.Iden);
					retCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.StructExtract:
			{
				ILStructElemAddrInstruction extract = instruction as ILStructElemAddrInstruction;
				if (extract.Struct == origOp)
				{
					extract.Struct = newOp;
					ILVarContext retCtx = context.GetVarContext(extract.RetOperand.Iden);
					retCtx.DependentOn.Remove(origOp.Iden);
					retCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.StructInsert:
			{
				ILStructInsertInstruction insert = instruction as ILStructInsertInstruction;
				if (insert.Value == origOp)
				{
					insert.Value = newOp;
					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				else if (insert.Struct == origOp)
				{
					insert.Struct = newOp;
					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.StructElemAddr:
			{
				ILStructElemAddrInstruction elemAddr = instruction as ILStructElemAddrInstruction;
				if (elemAddr.Struct == origOp)
				{
					elemAddr.Struct = newOp;
					ILVarContext retCtx = context.GetVarContext(elemAddr.RetOperand.Iden);
					retCtx.DependentOn.Remove(origOp.Iden);
					retCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.UnionExtract:
			{
				ILUnionExtractInstruction extract = instruction as ILUnionExtractInstruction;
				if (extract.Union == origOp)
				{
					extract.Union = newOp;
					ILVarContext retCtx = context.GetVarContext(extract.RetOperand.Iden);
					retCtx.DependentOn.Remove(origOp.Iden);
					retCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.UnionInsert:
			{
				ILUnionInsertInstruction insert = instruction as ILUnionInsertInstruction;
				if (insert.Value == origOp)
				{
					insert.Value = newOp;
					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				else if (insert.Union == origOp)
				{
					insert.Union = newOp;
					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.UnionElemAddr:
			{
				ILUnionElemAddrInstruction elemAddr = instruction as ILUnionElemAddrInstruction;
				if (elemAddr.Union == origOp)
				{
					elemAddr.Union = newOp;
					ILVarContext retCtx = context.GetVarContext(elemAddr.RetOperand.Iden);
					retCtx.DependentOn.Remove(origOp.Iden);
					retCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.Destruct:
			{

				break;
			}
			case ILInstructionType.EnumValue:
			{
				ILEnumValueInstruction enumVal = instruction as ILEnumValueInstruction;
				if (enumVal.Operands != null)
				{
					for (var i = 0; i < enumVal.Operands.Count; i++)
					{
						if (enumVal.Operands[i] == origOp)
						{
							enumVal.Operands[i] = newOp;

							ILVarContext retCtx = context.GetVarContext(enumVal.RetOperand.Iden);
							retCtx.DependentOn.Remove(origOp.Iden);
							retCtx.DependentOn.Add(newOp.Iden);

							ILVarContext opContext = context.GetVarContext(newOp.Iden);
							++opContext.UseCount;
						}
					}
				}
				break;
			}
			case ILInstructionType.EnumGetValue:
			{
				ILEnumGetValueInstruction enumGetVal = instruction as ILEnumGetValueInstruction;
				if (enumGetVal.Operand == origOp)
				{
					enumGetVal.Operand = newOp;

					ILVarContext retCtx = context.GetVarContext(enumGetVal.RetOperand.Iden);
					retCtx.DependentOn.Remove(origOp.Iden);
					retCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.EnumGetMemberValue: break;
			case ILInstructionType.GlobalAddr: break;
			case ILInstructionType.GlobalValue: break;
			case ILInstructionType.TemplateGetValue: break;
			case ILInstructionType.ConvType:
			{
				ILConvertTypeInstruction convType = instruction as ILConvertTypeInstruction;
				if (convType.Operand == origOp)
				{
					convType.Operand = newOp;

					ILVarContext retCtx = context.GetVarContext(convType.RetOperand.Iden);
					retCtx.DependentOn.Remove(origOp.Iden);
					retCtx.DependentOn.Add(newOp.Iden);

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.GetVTable: break;
			case ILInstructionType.Return:
			{
				ILReturnInstruction ret = instruction as ILReturnInstruction;
				if (ret.Operand == origOp)
				{
					ret.Operand = newOp;

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			case ILInstructionType.Branch: break;
			case ILInstructionType.CondBranch:
			{
				ILCondBranchInstruction ret = instruction as ILCondBranchInstruction;
				if (ret.Condition == origOp)
				{
					ret.Condition = newOp;

					ILVarContext opContext = context.GetVarContext(newOp.Iden);
					++opContext.UseCount;
				}
				break;
			}
			}
		}
	}
}
