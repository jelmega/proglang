﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;

namespace MJC.ILBackend
{
	public class ILBasicBlock
	{
		public string Label;
		public List<ILInstruction> Instructions = new List<ILInstruction>();

		public ILBasicBlock(string label)
		{
			Label = label;
		}

		public bool IsTerminal()
		{
			if (Instructions.Count == 0)
				return false;
			return Instructions.Last().IsTerminal();
		}

		public bool ExitsFunction()
		{
			if (Instructions.Count == 0)
				return false;
			return Instructions.Last().ExitsFunction();
		}
	}

	public class ILFunction
	{
		public ILContext Context;
		public ScopeVariable Identifier;
		public string MangledIdentifier;
		public FunctionSymbolType FuncType;

		public ILAttributes Attributes;

		public List<ILOperand> Operands;

		public bool IsUninstantiated;
		public List<ILTemplateParam> TemplateParams;

		public List<ILBasicBlock> Blocks = new List<ILBasicBlock>();

		public bool IsOperator;

		public ILFunction(ScopeVariable iden, string mangledIden, List<ILOperand> operands, FunctionSymbolType funcType, ILAttributes attribs, List<ILTemplateParam> templateParams, bool isUninstantiated, bool isOperator = false)
		{
			Identifier = iden;
			MangledIdentifier = mangledIden;
			Operands = operands;
			FuncType = funcType;
			Attributes = attribs;
			TemplateParams = templateParams;
			IsUninstantiated = isUninstantiated;
			IsOperator = isOperator;
		}

		public ILFunction GetDeclaration()
		{
			ILAttributes attribs = new ILAttributes(Attributes, true);
			return new ILFunction(Identifier, MangledIdentifier, Operands, FuncType, attribs, TemplateParams, IsUninstantiated);
		}
	}

	public class ILOperand
	{
		public ILOperand(string iden, SymbolType type)
		{
			Iden = iden;
			Type = type;
		}

		public ILOperand(ILOperand other)
		{
			Iden = other.Iden;
			Type = other.Type;
		}

		public string Iden;
		public SymbolType Type;

		public override string ToString()
		{
			return $"%{Iden} : ${Type.ToILString()}";
		}
	}

	public class ILReference
	{
		public ILReference(string refBase, string element)
		{
			Base = refBase;
			Element = element;
		}

		public string Base;
		public string Element;

		public override string ToString()
		{
			return $"#{Base}.{Element}";
		}
	}

	public enum ILAggregateType
	{
		Struct,
		Union
	}

	public class ILAggregateVariable
	{
		public Identifier Identifier;
		public string MangledIdentifier;
		public SymbolType Type;

		public ILAggregateVariable(Identifier iden, string mangledName, SymbolType type)
		{
			Identifier = iden;
			MangledIdentifier = mangledName;
			Type = type;
		}

		public override string ToString()
		{
			return $"%{MangledIdentifier} : ${Type.ToILString()}";
		}
	}

	public class ILAggregate
	{
		public Symbol Symbol;

		public ILAggregateType Type;

		public ScopeVariable Identifier;
		public ScopeVariable BaseIdentifier;
		public string MangledIdentifier;

		public ILAttributes Attributes;

		public List<ILTemplateParam> TemplateParams;
		public ILAggregate BaseAggregate;
		
		public List<ILAggregateVariable> Variables = new List<ILAggregateVariable>();

		public ILAggregate(ILAggregateType aggregateType, ScopeVariable identifier, string mangleName, Symbol symbol, ILAttributes attribs, List<ILTemplateParam> templateParams, ILAggregate baseAggregate = null)
		{
			Type = aggregateType;
			Identifier = identifier;
			BaseAggregate = baseAggregate;
			BaseIdentifier = BaseAggregate?.Identifier ?? identifier.GetBaseTemplate();
			MangledIdentifier = mangleName;
			Symbol = symbol;
			Attributes = attribs;
			TemplateParams = templateParams;
		}
	}

	public class ILTypedef
	{
		public SymbolType Type;
		public Symbol TypedefSym;

		public ILTypedef(Symbol typedefSym, SymbolType type)
		{
			TypedefSym = typedefSym;
			Type = type;
		}
	}

	public class ILEnumMember
	{
		public string Identifier;
		public string EnumInit;
		public long? EnumVal = null;
		public SymbolType AdtType;
		public string AdtInit;

		public ILEnumMember(string iden, string enumInit, SymbolType adtType, string adtInit)
		{
			Identifier = iden;
			EnumInit = enumInit;
			AdtType = adtType;
			AdtInit = adtInit;
		}
	}

	public class ILEnum
	{
		public Symbol Symbol;

		public ScopeVariable Identifier;
		public ScopeVariable BaseIdentifier;
		public string MangledIdentifier;

		public ILAttributes Attributes;

		public List<ILTemplateParam> TemplateParams;
		public ILEnum BaseEnum;

		public SymbolType BaseType;
		public List<ILEnumMember> Members = new List<ILEnumMember>();
		public bool IsADT;


		public ILEnum(ScopeVariable identifier, string mangledIdentifier, Symbol symbol, ILAttributes attribs, List<ILTemplateParam> templateParams, SymbolType baseType, ILEnum baseEnum = null)
		{
			Identifier = identifier;
			MangledIdentifier = mangledIdentifier;
			BaseEnum = baseEnum;
			BaseIdentifier = BaseEnum?.Identifier ?? identifier.GetBaseTemplate();
			BaseType = baseType;

			Symbol = symbol;
			Attributes = attribs;
			TemplateParams = templateParams;
		}

		public void AddMember(ILEnumMember member)
		{
			Members.Add(member);

			if (!IsADT)
			{
				IsADT = member.AdtType != null;
			}
		}

		public ILEnumMember GetMember(string iden)
		{
			foreach (ILEnumMember member in Members)
			{
				if (member.Identifier == iden)
					return member;
			}

			return null;
		}
	}

	public class ILInterface
	{
		public string MangledIdentifier;

		public ILInterface(string mangled)
		{
			MangledIdentifier = mangled;
		}
	}

	public class ILDelegate
	{
		public string MangledIdentifier;
		public List<SymbolType> ParamTypes;
		public SymbolType ReturnType;
		public bool AsFuncPtr;

		public ILDelegate(string mangledName, List<SymbolType> paramTypes, SymbolType returnType, bool asFuncPtr)
		{
			MangledIdentifier = mangledName;
			ParamTypes = paramTypes;
			ReturnType = returnType;
			AsFuncPtr = asFuncPtr;
		}
	}

	public class ILGlobal
	{
		public string MangledIdentifier;
		public SymbolType Type;
		public string Initializer;
		public string Value;
		public ILAttributes Attributes;

		public ILGlobal(string mangledName, SymbolType type, string initializer, ILAttributes attribs)
		{
			MangledIdentifier = mangledName;
			Type = type;
			Initializer = initializer;
			Attributes = attribs;
		}
	}

	public class ILTypeAlias
	{
		public string MangledAlias;

		public ILTypeAlias(string mangledAlias)
		{
			MangledAlias = mangledAlias;
		}
	}

}
