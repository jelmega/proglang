﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Builtin
{
	class BuiltinModule
	{
		// TODO: Make part of core lib (with macros) ??
		public static ILCompUnit GenerateBuiltins()
		{
			ILCompUnit compUnit = new ILCompUnit();
			compUnit.ILModule = "MJay";
			compUnit.Stage = ILStage.Canonical;

			List<SymbolType> sigInts = new List<SymbolType>
			{
				SymbolType.BuiltinType(BuiltinTypes.I8),
				SymbolType.BuiltinType(BuiltinTypes.I16),
				SymbolType.BuiltinType(BuiltinTypes.I32),
				SymbolType.BuiltinType(BuiltinTypes.I64),
			};

			List<SymbolType> unsigInts = new List<SymbolType>
			{
				SymbolType.BuiltinType(BuiltinTypes.U8),
				SymbolType.BuiltinType(BuiltinTypes.U16),
				SymbolType.BuiltinType(BuiltinTypes.U32),
				SymbolType.BuiltinType(BuiltinTypes.U64),
			};

			List<SymbolType> fps = new List<SymbolType>
			{
				SymbolType.BuiltinType(BuiltinTypes.F32),
				SymbolType.BuiltinType(BuiltinTypes.F64),
			};

			SymbolType boolType = SymbolType.BuiltinType(BuiltinTypes.Bool);

			List<SymbolType> chars = new List<SymbolType>
			{
				SymbolType.BuiltinType(BuiltinTypes.Char),
				SymbolType.BuiltinType(BuiltinTypes.WChar),
				SymbolType.BuiltinType(BuiltinTypes.Rune),
			};

			List<SymbolType> integers = new List<SymbolType>();
			integers.AddRange(sigInts);
			integers.AddRange(unsigInts);

			List<SymbolType> arith = new List<SymbolType>();
			arith.AddRange(sigInts);
			arith.AddRange(unsigInts);
			arith.AddRange(fps);

			List<SymbolType> builtins = new List<SymbolType>();
			builtins.AddRange(arith);
			builtins.AddRange(chars);
			builtins.Add(boolType);

			// unary plus
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateUnaryDummy(ILOpLoc.Prefix, ILOpType.Add, type);
				compUnit.AddFunction(op);
			}

			// unary minus
			foreach (SymbolType type in sigInts)
			{
				ILFunction op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Sub, "neg", type);
				compUnit.AddFunction(op);
			}
			foreach (SymbolType type in fps)
			{
				ILFunction op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Sub, "neg", type);
				compUnit.AddFunction(op);
			}

			// unary complement
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Concat, "compl", type);
				compUnit.AddFunction(op);
			}

			// unary not
			{
				ILFunction op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Concat, "not", boolType);
				compUnit.AddFunction(op);
			}

			// prefix inc
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Inc, "inc", type);
				compUnit.AddFunction(op);
			}

			// postfix inc
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateUnary(ILOpLoc.Postfix, ILOpType.Inc, "inc", type);
				compUnit.AddFunction(op);
			}

			// prefix dec
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Dec, "dec", type);
				compUnit.AddFunction(op);
			}

			// postfix dec
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateUnary(ILOpLoc.Postfix, ILOpType.Dec, "dec", type);
				compUnit.AddFunction(op);
			}

			// not
			{
				ILFunction op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Not, "neg", boolType);
				compUnit.AddFunction(op);
			}


			// infix add
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateBinary(ILOpType.Add, "add", type);
				compUnit.AddFunction(op);
			}

			// infix sub
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateBinary(ILOpType.Sub, "sub", type);
				compUnit.AddFunction(op);
			}

			// infix mul
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateBinary(ILOpType.Mul, "mul", type);
				compUnit.AddFunction(op);
			}

			// infix div
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateBinary(ILOpType.Div, "div", type);
				compUnit.AddFunction(op);
			}

			// infix rem
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateBinary(ILOpType.Rem, "rem", type);
				compUnit.AddFunction(op);
			}

			// infix shift.l
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateBinary(ILOpType.Shl, "shift.l", type);
				compUnit.AddFunction(op);
			}

			// infix shift.lr (>>)
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateBinary(ILOpType.LShr, "shift.lr", type);
				compUnit.AddFunction(op);
			}

			// infix shift.ar (>>>)
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateBinary(ILOpType.AShr, "shift.ar", type);
				compUnit.AddFunction(op);
			}

			// infix or
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateBinary(ILOpType.BOr, "or", type);
				compUnit.AddFunction(op);
			}

			// infix xor
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateBinary(ILOpType.BXor, "xor", type);
				compUnit.AddFunction(op);
			}

			// infix and
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateBinary(ILOpType.BAnd, "and", type);
				compUnit.AddFunction(op);
			}

			// infix assign
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateAssignSimple(ILOpType.Assign, type);
				compUnit.AddFunction(op);
			}

			// infix add assign
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateAssign(ILOpType.AddAssign, "add", type);
				compUnit.AddFunction(op);
			}

			// infix sub assign
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateAssign(ILOpType.SubAssign, "sub", type);
				compUnit.AddFunction(op);
			}

			// infix mul assign
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateAssign(ILOpType.MulAssign, "mul", type);
				compUnit.AddFunction(op);
			}

			// infix div assign
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateAssign(ILOpType.DivAssign, "div", type);
				compUnit.AddFunction(op);
			}

			// infix rem assign
			foreach (SymbolType type in arith)
			{
				ILFunction op = GenerateAssign(ILOpType.RemAssign, "rem", type);
				compUnit.AddFunction(op);
			}

			// infix shift.l assign
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateAssign(ILOpType.ShlAssign, "shift.l", type);
				compUnit.AddFunction(op);
			}

			// infix shift.lr (>>) assign
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateAssign(ILOpType.LShrAssign, "shift.lr", type);
				compUnit.AddFunction(op);
			}

			// infix shift.ar (>>>) assign
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateAssign(ILOpType.AShrAssign, "shift.ar", type);
				compUnit.AddFunction(op);
			}

			// infix or assign
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateAssign(ILOpType.OrAssign, "or", type);
				compUnit.AddFunction(op);
			}

			// infix xor assign
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateAssign(ILOpType.XorAssign, "xor", type);
				compUnit.AddFunction(op);
			}

			// infix and assign
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateAssign(ILOpType.AndAssign, "and", type);
				compUnit.AddFunction(op);
			}


			// infix eq
			foreach (SymbolType type in builtins)
			{
				ILFunction op = GenerateBinaryBool(ILOpType.Eq, "cmp.eq", type, boolType);
				compUnit.AddFunction(op);
			}

			// infix ne
			foreach (SymbolType type in builtins)
			{
				ILFunction op = GenerateBinaryBool(ILOpType.Ne, "cmp.ne", type, boolType);
				compUnit.AddFunction(op);
			}

			// infix gt
			foreach (SymbolType type in builtins)
			{
				ILFunction op = GenerateBinaryBool(ILOpType.Lt, "cmp.gt", type, boolType);
				compUnit.AddFunction(op);
			}

			// infix gte
			foreach (SymbolType type in builtins)
			{
				ILFunction op = GenerateBinaryBool(ILOpType.Lte, "cmp.gte", type, boolType);
				compUnit.AddFunction(op);
			}

			// infix lt
			foreach (SymbolType type in builtins)
			{
				ILFunction op = GenerateBinaryBool(ILOpType.Gt, "cmp.lt", type, boolType);
				compUnit.AddFunction(op);
			}

			// infix lte
			foreach (SymbolType type in builtins)
			{
				ILFunction op = GenerateBinaryBool(ILOpType.Gte, "cmp.lte", type, boolType);
				compUnit.AddFunction(op);
			}

			// cast static to i1
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateCastToBool(type, boolType);
				compUnit.AddFunction(op);
			}

			// cast static from i1
			foreach (SymbolType type in integers)
			{
				ILFunction op = GenerateCast(boolType, type, "ext");
				compUnit.AddFunction(op);
			}

			// cast static (integer to integer)
			foreach (SymbolType from in integers)
			{
				int fromSize = (from as BuiltinSymbolType).GetNumBytes();
				foreach (SymbolType to in integers)
				{
					if (to == from)
						continue;

					int toSize = (to as BuiltinSymbolType).GetNumBytes();

					ILFunction op;
					if (fromSize == toSize)
					{
						op = GenerateConvType(from, to);
					}
					else if (fromSize < toSize)
					{
						op = GenerateCast(from, to, "ext");
					}
					else
					{
						op = GenerateCast(from, to, "trunc");
					}
					compUnit.AddFunction(op);
				}
			}

			// cast static (integer to char)
			foreach (SymbolType from in integers)
			{
				int fromSize = (from as BuiltinSymbolType).GetNumBytes();
				foreach (SymbolType to in chars)
				{
					if (to == from)
						continue;

					int toSize = (to as BuiltinSymbolType).GetNumBytes();

					ILFunction op;
					if (fromSize == toSize)
					{
						op = GenerateConvType(from, to);
					}
					else if (fromSize < toSize)
					{
						op = GenerateCast(from, to, "ext");
					}
					else
					{
						op = GenerateCast(from, to, "trunc");
					}
					compUnit.AddFunction(op);
				}
			}

			// cast static (char to integer)
			foreach (SymbolType from in chars)
			{
				int fromSize = (from as BuiltinSymbolType).GetNumBytes();
				foreach (SymbolType to in integers)
				{
					if (to == from)
						continue;

					int toSize = (to as BuiltinSymbolType).GetNumBytes();

					ILFunction op;
					if (fromSize == toSize)
					{
						op = GenerateConvType(from, to);
					}
					else if (fromSize < toSize)
					{
						op = GenerateCast(from, to, "ext");
					}
					else
					{
						op = GenerateCast(from, to, "trunc");
					}
					compUnit.AddFunction(op);
				}
			}

			// cast static (chars to chars)
			foreach (SymbolType from in chars)
			{
				int fromSize = (from as BuiltinSymbolType).GetNumBytes();
				foreach (SymbolType to in chars)
				{
					if (to == from)
						continue;

					int toSize = (to as BuiltinSymbolType).GetNumBytes();

					ILFunction op;
					if (fromSize == toSize)
					{
						op = GenerateConvType(from, to);
					}
					else if (fromSize < toSize)
					{
						op = GenerateCast(from, to, "ext");
					}
					else
					{
						op = GenerateCast(from, to, "trunc");
					}
					compUnit.AddFunction(op);
				}
			}

			// cast static (fp to fp)
			foreach (SymbolType from in fps)
			{
				int fromSize = (from as BuiltinSymbolType).GetNumBytes();
				foreach (SymbolType to in fps)
				{
					if (to == from)
						continue;

					int toSize = (to as BuiltinSymbolType).GetNumBytes();

					ILFunction op;
					if (fromSize == toSize)
					{
						op = GenerateConvType(from, to);
					}
					else if (fromSize < toSize)
					{
						op = GenerateCast(from, to, "ext");
					}
					else
					{
						op = GenerateCast(from, to, "trunc");
					}
					compUnit.AddFunction(op);
				}
			}

			// cast static (fp to integer)
			foreach (SymbolType from in fps)
			{
				foreach (SymbolType to in integers)
				{
					if (to == from)
						continue;

					ILFunction op = GenerateCast(from, to, "fptoi");
					compUnit.AddFunction(op);
				}
			}

			// cast static (integer to fp)
			foreach (SymbolType from in integers)
			{
				foreach (SymbolType to in fps)
				{
					if (to == from)
						continue;

					ILFunction op = GenerateCast(from, to, "itofp");
					compUnit.AddFunction(op);
				}
			}

			return compUnit;
		}

		static ILFunction GenerateUnaryDummy(ILOpLoc loc, ILOpType opType, SymbolType type)
		{
			string opName = ILHelpers.GetIlOpName(loc, opType);
			ILOperand op = new ILOperand("0", type);
			List<ILOperand> operands = new List<ILOperand> { op };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(opName));
			FunctionSymbolType funcType = SymbolType.FunctionType(null, new List<SymbolType> {type}, type);
			string mangled = "_M" + NameMangling.MangleScopeVar(scopeVar) + NameMangling.MangleType(funcType);

			ILFunction ilOperator = new ILFunction(scopeVar, mangled, operands, funcType, attribs, null, false, true);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);
			
			basicBlock.Instructions.Add(new ILReturnInstruction(op, null));

			return ilOperator;
		}

		static ILFunction GenerateUnary(ILOpLoc loc, ILOpType opType, string unaryBuiltin, SymbolType type)
		{
			string opName = ILHelpers.GetIlOpName(loc, opType);
			ILOperand op = new ILOperand("0", type);
			List<ILOperand> operands = new List<ILOperand>{ op };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(opName));
			FunctionSymbolType funcType = SymbolType.FunctionType(null, new List<SymbolType> { type }, type);
			string mangled = "_M" + NameMangling.MangleScopeVar(scopeVar) + NameMangling.MangleType(funcType);

			ILFunction ilOperator = new ILFunction(scopeVar, mangled, operands, funcType, attribs, null, false, true);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand retOp = new ILOperand("1", type);
			basicBlock.Instructions.Add(new ILBuiltinInstruction(retOp, unaryBuiltin, operands, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILFunction GenerateBinary(ILOpType opType, string binaryBuiltin, SymbolType type)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Infix, opType);
			ILOperand op0 = new ILOperand("0", type);
			ILOperand op1 = new ILOperand("1", type);
			List<ILOperand> operands = new List<ILOperand> { op0, op1 };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(opName));
			FunctionSymbolType funcType = SymbolType.FunctionType(null, new List<SymbolType> { type, type }, type);
			string mangled = "_M" + NameMangling.MangleScopeVar(scopeVar) + NameMangling.MangleType(funcType);

			ILFunction ilOperator = new ILFunction(scopeVar, mangled, operands, funcType, attribs, null, false, true);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand retOp = new ILOperand("2", type);
			basicBlock.Instructions.Add(new ILBuiltinInstruction(retOp, binaryBuiltin, operands, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILFunction GenerateBinaryBool(ILOpType opType, string binaryBuiltin, SymbolType type, SymbolType boolType)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Infix, opType);
			ILOperand op0 = new ILOperand("0", type);
			ILOperand op1 = new ILOperand("1", type);
			List<ILOperand> operands = new List<ILOperand> { op0, op1 };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(opName));
			FunctionSymbolType funcType = SymbolType.FunctionType(null, new List<SymbolType> { type, type }, type);
			string mangled = "_M" + NameMangling.MangleScopeVar(scopeVar) + NameMangling.MangleType(funcType);

			ILFunction ilOperator = new ILFunction(scopeVar, mangled, operands, funcType, attribs, null, false, true);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand retOp = new ILOperand("2", boolType);
			basicBlock.Instructions.Add(new ILBuiltinInstruction(retOp, binaryBuiltin, operands, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILFunction GenerateAssignSimple(ILOpType opType, SymbolType type)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Infix, opType);
			SymbolType memType = SymbolType.MemoryLocType(type);
			ILOperand op0 = new ILOperand("0", memType);
			ILOperand op1 = new ILOperand("1", type);
			List<ILOperand> operands = new List<ILOperand> { op0, op1 };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(opName));
			FunctionSymbolType funcType = SymbolType.FunctionType(null, new List<SymbolType> { memType, type }, type);
			string mangled = "_M" + NameMangling.MangleScopeVar(scopeVar) + NameMangling.MangleType(funcType);

			ILFunction ilOperator = new ILFunction(scopeVar, mangled, operands, funcType, attribs, null, false, true);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			basicBlock.Instructions.Add(new ILStoreInstruction(op1, op0, null));
			ILOperand retOp = new ILOperand("2", type);
			basicBlock.Instructions.Add(new ILLoadInstruction(retOp, op0, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILFunction GenerateAssign(ILOpType opType, string binaryBuiltin, SymbolType type)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Infix, opType);
			SymbolType memType = SymbolType.MemoryLocType(type);
			ILOperand op0 = new ILOperand("0", memType);
			ILOperand op1 = new ILOperand("1", type);
			List<ILOperand> operands = new List<ILOperand> {op0, op1};

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(opName));
			FunctionSymbolType funcType = SymbolType.FunctionType(null, new List<SymbolType> { memType, type }, type);
			string mangled = "_M" + NameMangling.MangleScopeVar(scopeVar) + NameMangling.MangleType(funcType);

			ILFunction ilOperator = new ILFunction(scopeVar, mangled, operands, funcType, attribs, null, false, true);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand op2 = new ILOperand("2", type);
			basicBlock.Instructions.Add(new ILLoadInstruction(op2, op0, null));

			ILOperand op3 = new ILOperand("3", type);
			List<ILOperand> builtinOps = new List<ILOperand>{op2, op1};
			basicBlock.Instructions.Add(new ILBuiltinInstruction(op3, binaryBuiltin, builtinOps, null));
			basicBlock.Instructions.Add(new ILStoreInstruction(op3, op0, null));

			ILOperand retOp = new ILOperand("4", type);
			basicBlock.Instructions.Add(new ILLoadInstruction(retOp, op0, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILFunction GenerateCastToBool(SymbolType type, SymbolType boolType)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Infix, ILOpType.Static);
			ILOperand op0 = new ILOperand("0", type);
			List<ILOperand> operands = new List<ILOperand> { op0 };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(opName));
			FunctionSymbolType funcType = SymbolType.FunctionType(null, new List<SymbolType> { type }, boolType);
			string mangled = "_M" + NameMangling.MangleScopeVar(scopeVar) + NameMangling.MangleType(funcType);

			ILFunction ilOperator = new ILFunction(scopeVar, mangled, operands, funcType, attribs, null, false, true);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand op1 = new ILOperand("1", type);
			basicBlock.Instructions.Add(new ILIntLiteralInstruction(op1, 0, null));
			ILOperand retOp = new ILOperand("2", boolType);
			basicBlock.Instructions.Add(new ILBuiltinInstruction(retOp, "cmp.ne", new List<ILOperand>{ op0, op1 }, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILFunction GenerateCast(SymbolType from, SymbolType to, string convBuiltin)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Cast, ILOpType.Static);
			ILOperand op = new ILOperand("0", from);
			List<ILOperand> operands = new List<ILOperand> { op };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(opName));
			FunctionSymbolType funcType = SymbolType.FunctionType(null, new List<SymbolType> { from }, to);
			string mangled = "_M" + NameMangling.MangleScopeVar(scopeVar) + NameMangling.MangleType(funcType);

			ILFunction ilOperator = new ILFunction(scopeVar, mangled, operands, funcType, attribs, null, false, true);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand retOp = new ILOperand("1", to);
			basicBlock.Instructions.Add(new ILBuiltinInstruction(retOp, convBuiltin, operands, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}
		
		static ILFunction GenerateConvType(SymbolType from, SymbolType to)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Cast, ILOpType.Static);
			ILOperand op = new ILOperand("0", from);
			List<ILOperand> operands = new List<ILOperand> { op };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ScopeVariable scopeVar = new ScopeVariable(new NameIdentifier(opName));
			FunctionSymbolType funcType = SymbolType.FunctionType(null, new List<SymbolType> { from }, to);
			string mangled = "_M" + NameMangling.MangleScopeVar(scopeVar) + NameMangling.MangleType(funcType);

			ILFunction ilOperator = new ILFunction(scopeVar, mangled, operands, funcType, attribs, null, false, true);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand retOp = new ILOperand("1", to);
			basicBlock.Instructions.Add(new ILConvertTypeInstruction(retOp, op, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

	}
}
