﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.IRBackend.General;

namespace MJC.IRBackend
{
	public class IRBuilder
	{	
		private string _moduleName;

		private IRModule _module;
		private IRFunction _curFunc;
		private IRBasicBlock _curBasicBlock;

		public void Begin(string name)
		{
			_moduleName = name;
			_module = new IRModule(_moduleName);
		}

		public IRModule End()
		{
			IRModule tmp = _module;
			_module = null;
			return tmp;
		}

		public List<IROperand> BeginFunction(string funcName, IRType retType, List<IROperand> operands, IRAttributes attribs)
		{
			_curFunc = new IRFunction();
			_curFunc.Identifier = funcName;
			_curFunc.RetType = retType;
			_curFunc.Params = operands;
			_curFunc.Attribs = attribs;
			_module.Functions.Add(_curFunc);

			return operands;
		}

		public void EndFunction()
		{
			_curBasicBlock = null;
			_curFunc = null;   
		}

		public void CreateBasicBlock(string iden)
		{
			_curBasicBlock = new IRBasicBlock();
			_curBasicBlock.Label = iden;
			_curFunc.Blocks.Add(_curBasicBlock);
		}

		public void CreateType(string name, IRTypeDef typeDef)
		{
			_module.TypeDefs.Add(typeDef);
		}

		public IROperand BuildGlobal(string iden, IRType type, IRAttributes attribs)
		{
			IROperand ret = new IROperand(iden, type, true);

			IRGlobal global = new IRGlobal(iden, type, attribs);

			_module.Globals.Add(global);

			return ret;
		}

		public IROperand BuildGlobal(string iden, IRType type, IRAttributes attribs, string value)
		{
			IROperand ret = new IROperand(iden, type, true);

			IRGlobal global = new IRGlobal(iden, type, attribs);
			global.StringValue = value;
			global.HasValue = true;

			_module.Globals.Add(global);
			 
			return ret;
		}

		public IROperand BuildGlobal(string iden, IRType type, IRAttributes attribs, long value)
		{
			IROperand ret = new IROperand(iden, type, true);

			IRGlobal global = new IRGlobal(iden, type, attribs);
			global.NumValue = value;
			global.HasValue = true;

			_module.Globals.Add(global);

			return ret;
		}

		public IROperand BuildAlloca(string iden, IRType type)
		{
			IROperand ret = new IROperand(iden, new IRType(type, false, true));

			_curBasicBlock.Instructions.Add(new IRAllocaInstruction(ret, type));

			return ret;
		}

		public void BuildStore(IROperand src, IROperand dst)
		{
			_curBasicBlock.Instructions.Add(new IRStoreInstruction(src, dst));
		}

		public IROperand BuildLoad(IROperand src, string dst)
		{
			IROperand ret = new IROperand(dst, new IRType(src.Type, false));

			_curBasicBlock.Instructions.Add(new IRLoadInstruction(ret, src));

			return ret;
		}

		public IROperand BuildGep(IROperand operand, string retVar, IRType retType, params int[] indices)
		{
			IRType actRetType = new IRType(retType, forceAddress: retType.PtrCount == 0);
			IROperand ret = new IROperand(retVar, actRetType);

			_curBasicBlock.Instructions.Add(new IRGepInstruction(ret, operand, indices));

			return ret;
		}

		public IROperand BuildNeg(string retVar, IROperand operand, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRNegInstruction(ret, operand));

			return ret;
		}

		public IROperand BuildCompl(string retVar, IROperand operand, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRComplInstruction(ret, operand));

			return ret;
		}

		public IROperand BuildAdd(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRAddInstruction(ret, op0, op1));

			return ret;
		}

		public IROperand BuildSub(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRSubInstruction(ret, op0, op1));

			return ret;
		}

		public IROperand BuildMul(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRMulInstruction(ret, op0, op1));

			return ret;
		}

		public IROperand BuildDiv(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRDivInstruction(ret, op0, op1));

			return ret;
		}

		public IROperand BuildRem(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRRemInstruction(ret, op0, op1));

			return ret;
		}

		public IROperand BuildShift(string retVar, IRShiftDir dir, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRShiftInstruction(ret, dir, op0, op1));

			return ret;
		}

		public IROperand BuildOr(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IROrInstruction(ret, op0, op1));

			return ret;
		}

		public IROperand BuildXor(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRXorInstruction(ret, op0, op1));

			return ret;
		}

		public IROperand BuildAnd(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRAndInstruction(ret, op0, op1));

			return ret;
		}

		public IROperand BuildTrunc(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRTruncInstruction(ret, op));

			return ret;
		}

		public IROperand BuildExt(string retVar, IRExtType extType, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRExtInstruction(ret, extType, op));

			return ret;
		}

		public IROperand BuildFptoi(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRFptoiInstruction(ret, op));

			return ret;
		}

		public IROperand BuildItofp(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRItofpInstruction(ret, op));

			return ret;
		}

		public IROperand BuildPtrtoi(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRPtrtoiInstruction(ret, op));

			return ret;
		}

		public IROperand BuildItoptr(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRItoptrInstruction(ret, op));

			return ret;
		}

		public IROperand BuildBitcast(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRBitcastInstruction(ret, op));

			return ret;
		}

		public IROperand BuildExtractValue(string retVar, IROperand aggregate, IRType retType, int index)
		{
			IRType actRetType = new IRType(retType);
			IROperand ret = new IROperand(retVar, actRetType);
			
			_curBasicBlock.Instructions.Add(new IRExtractValueInstruction(ret, aggregate, index));

			return ret;
		}

		public IROperand BuildInsertValue(string retName, IROperand aggrOp, IROperand value, int index)
		{
			IROperand retOp = new IROperand(retName, aggrOp.Type);

			_curBasicBlock.Instructions.Add(new IRInsertValueInstruction(retOp, aggrOp, value, index));
			return retOp;
		}

		public IROperand BuildUnionTagSet(string retName, IROperand unionOp, IROperand tagOp)
		{
			IROperand retOp = new IROperand(retName, unionOp.Type);

			_curBasicBlock.Instructions.Add(new IRUnionTagSetInstruction(retOp, unionOp, tagOp));
			return retOp;
		}

		public IROperand BuildUnionTagGet(string retName, IROperand unionOp, IRType tagType)
		{
			IROperand retOp = new IROperand(retName, tagType);

			_curBasicBlock.Instructions.Add(new IRUnionTagGetInstruction(retOp, unionOp));
			return retOp;
		}

		public IROperand BuildUnionTagAddr(string retName, IROperand unionOp, IRType tagType)
		{
			IROperand retOp = new IROperand(retName, tagType);

			_curBasicBlock.Instructions.Add(new IRUnionTagAddrInstruction(retOp, unionOp));
			return retOp;
		}
		
		public IROperand BuildUnionSet(string retName, IROperand unionOp, IROperand valOp, int index, bool tagUnion)
		{
			IROperand retOp = new IROperand(retName, unionOp.Type);

			_curBasicBlock.Instructions.Add(new IRUnionSetInstruction(retOp, unionOp, valOp, index, tagUnion));
			return retOp;
		}

		public IROperand BuildUnionGet(string retName, IROperand unionOp, int index, bool tagUnion)
		{
			IROperand retOp = new IROperand(retName, unionOp.Type);
			_curBasicBlock.Instructions.Add(new IRUnionGetInstruction(retOp, unionOp, index, tagUnion));
			return retOp;
		}

		public IROperand BuildUnionGep(string retName, IROperand unionOp, IRType retType, int index, bool tagUnion)
		{
			IROperand retOp = new IROperand(retName, retType);

			_curBasicBlock.Instructions.Add(new IRUnionGepInstruction(retOp, unionOp, index, tagUnion));
			return retOp;
		}

		public IROperand BuildCall(string retVar, string identifier, List<IROperand> operands, IRType retType)
		{
			IROperand retOperand = null;
			if (retType != null)
				retOperand = new IROperand(retVar, retType);

			_curBasicBlock.Instructions.Add(new IRCallInstruction(identifier, operands, retType, retOperand));

			return retOperand;
		}

		public IROperand BuildCmp(string retVar, IRCmpType cmpType, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(IRBaseType.I1));

			_curBasicBlock.Instructions.Add(new IRCmpInstruction(ret, cmpType, op0, op1));

			return ret;
		}

		public void BuildRet()
		{
			_curBasicBlock.Instructions.Add(new IRReturnInstruction());
		}

		public void BuildRet(IROperand operand)
		{
			_curBasicBlock.Instructions.Add(new IRReturnInstruction(operand));
		}

		public void BuildBranch(string label)
		{
			_curBasicBlock.Instructions.Add(new IRBranchInstruction(label));
		}

		public void BuildCondBranch(IROperand condition, string trueLabel, string falseLabel)
		{
			_curBasicBlock.Instructions.Add(new IRCondBranchInstruction(condition, trueLabel, falseLabel));
		}

		public void BuildSwitch(IROperand operand, List<IRSwitchCase> cases, string defLabel)
		{
			_curBasicBlock.Instructions.Add(new IRSwitchInstruction(operand, cases, defLabel));
		}
	}
}
