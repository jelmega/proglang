﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.IRBackend
{
	public enum IRBaseType
	{
		I1,
		I8,
		U8,
		I16,
		U16,
		I32,
		U32,
		I64,
		U64,
		F32,
		F64,
		Custom,
		Array,
		Void
	}

	public class IRType
	{
		public bool Address;
		public byte PtrCount;
		public ulong ArraySize;

		public IRBaseType BaseType;
		public string CustomIden;
		public IRType SubType;

		public IRType()
		{
		}

		public IRType(IRBaseType baseType)
		{
			BaseType = baseType;
		}

		public IRType(bool address, IRBaseType baseType, byte ptrCount)
		{
			Address = address;
			BaseType = baseType;
			PtrCount = ptrCount;
		}

		public IRType(bool address, string iden, byte ptrCount)
		{
			Address = address;
			BaseType = IRBaseType.Custom;
			CustomIden = iden;
			PtrCount = ptrCount;
		}

		public IRType(string iden)
		{
			BaseType = IRBaseType.Custom;
			CustomIden = iden;
		}

		public IRType(IRType type, bool allowAddress = true, bool forceAddress = false)
		{
			Address = type.Address && allowAddress || forceAddress;
			BaseType = type.BaseType;
			CustomIden = type.CustomIden;
			PtrCount = type.PtrCount;
		}

		public IRType(IRType subType, ulong arraySize)
		{
			BaseType = IRBaseType.Array;
			SubType = subType;
			ArraySize = arraySize;
		}

		public override string ToString()
		{
			string builtin;
			if (BaseType == IRBaseType.Custom)
			{
				builtin = "?" + CustomIden;
			}
			else if (BaseType == IRBaseType.Array)
			{
				return $"[{ArraySize} x {SubType}]";
			}
			else
			{
				builtin = BaseType.ToString().ToLower();
			}

			return $"{(Address ? "^" : "")}{"".PadLeft(PtrCount, '*')}{builtin}";
		}

		public static bool operator==(IRType type0, IRType type1)
		{
			if (ReferenceEquals(type0, null) || ReferenceEquals(type1, null))
				return ReferenceEquals(type0, type1);

			if (type0.BaseType != type1.BaseType ||
			    type0.PtrCount != type1.PtrCount ||
			    type0.Address != type1.Address)
				return false;

			if (type0.BaseType == IRBaseType.Custom)
			{
				if (type0.CustomIden == type1.CustomIden)
					return true;
			}
			else if (type0.BaseType == IRBaseType.Array)
			{
				if (type0.SubType == type1.SubType)
					return true;
			}

			return true;
		}

		public static bool operator!=(IRType type0, IRType type1)
		{
			return !(type0 == type1);
		}
	}

	public enum IRTypeDefKind
	{
		Struct,
		Union,
		TagUnion
	}

	public class IRTypeDef
	{
		public string Iden;
		public IRType TagType;
		public List<IRType> SubTypes;
		public IRTypeDefKind Kind;

		public IRTypeDef(string iden, List<IRType> subTypes, IRTypeDefKind kind)
		{
			Iden = iden;
			SubTypes = subTypes;
			Kind = kind;
		}

		public IRTypeDef(string iden, IRType tagType, List<IRType> subTypes)
		{
			Iden = iden;
			TagType = tagType;
			SubTypes = subTypes;
			Kind = IRTypeDefKind.TagUnion;
		}
	}

}
