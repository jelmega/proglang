﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using LLVMSharp;

namespace MJC.IRBackend.LLVMIR
{
	public class LLVMBackend
	{
		private string _triple;

		private string _cpu = "generic";
		private string _features = "";

		public LLVMBackend()
		{
			LLVM.InitializeX86TargetMC();
			LLVM.InitializeX86Target();
			LLVM.InitializeX86TargetInfo();
			LLVM.InitializeX86AsmPrinter();
			LLVM.InitializeX86AsmParser();
			LLVM.InitializeX86Disassembler();

			_triple = Marshal.PtrToStringAnsi(LLVM.GetDefaultTargetTriple());
		}


		public void Process(LLVMModuleRef module, string filepath)
		{
			string asmFile = CmdLine.IntermediateDirectory + Path.ChangeExtension(filepath, "s");
			string objFile = CmdLine.IntermediateDirectory + Path.ChangeExtension(filepath, "o");


			LLVMTargetRef target;
			string error;
			LLVMBool res = LLVM.GetTargetFromTriple(_triple, out target, out error);

			if (res.Value != 0)
			{
				LLVM.DisposeModule(module);
				return;
			}

			LLVMCodeGenOptLevel optLevel = LLVMCodeGenOptLevel.LLVMCodeGenLevelNone;
			LLVMTargetMachineRef machine = LLVM.CreateTargetMachine(target, _triple, _cpu, _features, optLevel, LLVMRelocMode.LLVMRelocDefault, LLVMCodeModel.LLVMCodeModelDefault);

			// out asm
			WithHString(asmFile, filePtr =>
			{
				LLVM.TargetMachineEmitToFile(machine, module, filePtr, LLVMCodeGenFileType.LLVMAssemblyFile, out error);
			});

			// out obj
			WithHString(objFile, filePtr =>
			{
				LLVM.TargetMachineEmitToFile(machine, module, filePtr, LLVMCodeGenFileType.LLVMObjectFile, out error);
			});

			LLVM.DisposeTargetMachine(machine);
			LLVM.DisposeModule(module);
		}

		void WithHString(string text, Action<IntPtr> action)
		{
			IntPtr strPtr = Marshal.StringToHGlobalAnsi(text);
			action(strPtr);
			Marshal.FreeHGlobal(strPtr);
		}
	}
}
