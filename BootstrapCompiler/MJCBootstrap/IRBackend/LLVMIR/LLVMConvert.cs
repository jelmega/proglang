﻿using System;
using System.Collections.Generic;
using System.Text;
using LLVMSharp;
using MJC.General;
using MJC.IRBackend.General;

namespace MJC.IRBackend.LLVMIR
{
	class LLVMConvert
	{
		private LLVMModuleRef _module;
		private LLVMBasicBlockRef _curBlock;

		private Dictionary<string, LLVMValueRef> _valueRefMap = new Dictionary<string, LLVMValueRef>();
		private Dictionary<string, LLVMValueRef> _globalRefMap = new Dictionary<string, LLVMValueRef>();
		private Dictionary<string, LLVMValueRef> _funcRefMap = new Dictionary<string, LLVMValueRef>();

		private Dictionary<string, Dictionary<string, LLVMValueRef>> _argsMap = new Dictionary<string, Dictionary<string, LLVMValueRef>>();
		Dictionary<string, LLVMValueRef> _curArgs;

		private Dictionary<string, List<IRType>> _expectedTypes = new Dictionary<string, List<IRType>>();

		private List<LLVMBasicBlockRef> _llvmBasicBlockRefs = new List<LLVMBasicBlockRef>();
		private Dictionary<string, LLVMBasicBlockRef> _llvmBasicBlockNameMap = new Dictionary<string, LLVMBasicBlockRef>();

		private Dictionary<string, LLVMTypeRef> _llvmTypeRefs = new Dictionary<string, LLVMTypeRef>();

		private LLVMContextRef _llvmContext;
		private LLVMSharp.IRBuilder _builder;

		public LLVMModuleRef Convert(IRModule irModule, CompilerContext context)
		{
			_module = LLVM.ModuleCreateWithName(irModule.Name);

			_llvmContext = LLVM.GetModuleContext(_module);
			_builder = new LLVMSharp.IRBuilder(_llvmContext);

			// Generate types
			ConvTypedefs(irModule.TypeDefs, context);

			ConvGlobals(irModule.Globals);

			// Pre-generate functions
			foreach (IRFunction function in irModule.Functions)
			{
				PregenFunction(function);
			}

			foreach (IRFunction function in irModule.Functions)
			{
				ConvFunction(function);
			}

			_builder.Dispose();
			_builder = null;

			// Validate
			LLVMBool res = LLVM.VerifyModule(_module, LLVMVerifierFailureAction.LLVMPrintMessageAction, out string error);
			if (res.Value != 0)
			{
				Console.WriteLine($"invalid llvm ir for module: {irModule.Name}, error: {error}");
			}

			// Output module
			string outName = CmdLine.IntermediateDirectory + irModule.Name + ".ll";

			res = LLVM.PrintModuleToFile(_module, outName, out error);
			if (res.Value != 0)
			{
				Console.WriteLine($"Failed to print out llvm ir for module: {irModule.Name}, error: {error}");
			}

			return _module;
		}


		private void PregenFunction(IRFunction function)
		{
			_llvmBasicBlockNameMap.Clear();

			LLVMTypeRef retType = ConvType(function.RetType);

			_expectedTypes.Add(function.Identifier, new List<IRType>());
			List<IRType> expectedTypes = _expectedTypes[function.Identifier];

			LLVMTypeRef[] paramTypes;
			if (function.Params != null && function.Params.Count > 0)
			{
				paramTypes = new LLVMTypeRef[function.Params.Count];
				for (var i = 0; i < function.Params.Count; i++)
				{
					IROperand irOperand = function.Params[i];
					paramTypes[i] = ConvType(irOperand.Type);
					expectedTypes.Add(irOperand.Type);
				}
			}
			else
			{
				paramTypes = new LLVMTypeRef[0];
			}

			// TODO: Allow variadic
			LLVMTypeRef funcType = LLVMTypeRef.FunctionType(retType, paramTypes, false);

			LLVMValueRef llvmFunc = LLVM.AddFunction(_module, function.Identifier, funcType);
			_funcRefMap.Add(function.Identifier, llvmFunc);

			SetAttributes(llvmFunc, function.Attribs, false);

			// Get the corresponding llvm valueref for each param and set their names
			_argsMap.Add(function.Identifier, new Dictionary<string, LLVMValueRef>());
			Dictionary<string, LLVMValueRef> args = _argsMap[function.Identifier];

			if (function.Params != null)
			{
				for (var i = 0; i < function.Params.Count; i++)
				{
					IROperand operand = function.Params[i];
					LLVMValueRef llvmOp = LLVM.GetParam(llvmFunc, (uint)i);

					LLVM.SetValueName(llvmOp, operand.Iden);
					args.Add(operand.Iden, llvmOp);
				}
			}
		}

		void ConvFunction(IRFunction function)
		{
			_llvmBasicBlockNameMap.Clear();
			_valueRefMap.Clear();
			_curArgs = _argsMap[function.Identifier];

			LLVMValueRef llvmFunc = _funcRefMap[function.Identifier];

			// Generate all block before writing to them, they need to exist for branches
			foreach (IRBasicBlock block in function.Blocks)
			{
				LLVMBasicBlockRef blockRef = LLVM.AppendBasicBlock(llvmFunc, block.Label);
				_llvmBasicBlockRefs.Add(blockRef);
				_llvmBasicBlockNameMap.Add(block.Label, blockRef);
			}

			for (var i = 0; i < function.Blocks.Count; i++)
			{
				_curBlock = _llvmBasicBlockRefs[i];
				ConvBasicBlock(function.Blocks[i], llvmFunc);
			}

			LLVM.VerifyFunction(llvmFunc, LLVMVerifierFailureAction.LLVMPrintMessageAction);

			_llvmBasicBlockRefs.Clear();
		}

		void ConvBasicBlock(IRBasicBlock block, LLVMValueRef llvmFunc)
		{
			_builder.PositionBuilderAtEnd(_curBlock);

			foreach (IRInstruction instruction in block.Instructions)
			{
				switch (instruction.InstructionType)
				{
				case IRInstructionType.Alloca:
					ConvAlloca(instruction as IRAllocaInstruction);
					break;
				case IRInstructionType.Store:
					ConvStore(instruction as IRStoreInstruction);
					break;
				case IRInstructionType.Load:
					ConvLoad(instruction as IRLoadInstruction);
					break;
				case IRInstructionType.Gep:
					ConvGep(instruction as IRGepInstruction);
					break;
				
				case IRInstructionType.Neg:
					ConvNeg(instruction as IRNegInstruction);
					break;
				case IRInstructionType.Inc:
					ConvInc(instruction as IRIncInstruction);
					break;
				case IRInstructionType.Dec:
					ConvDec(instruction as IRDecInstruction);
					break;
				case IRInstructionType.Compl:
					ConvCompl(instruction as IRComplInstruction);
					break;

				case IRInstructionType.Add:
					ConvAdd(instruction as IRAddInstruction);
					break;
				case IRInstructionType.Sub:
					ConvSub(instruction as IRSubInstruction);
					break;
				case IRInstructionType.Mul:
					ConvMul(instruction as IRMulInstruction);
					break;
				case IRInstructionType.Div:
					ConvDiv(instruction as IRDivInstruction);
					break;
				case IRInstructionType.Rem:
					ConvRem(instruction as IRRemInstruction);
					break;
				case IRInstructionType.Shift:
					ConvShift(instruction as IRShiftInstruction);
					break;
				case IRInstructionType.Or:
					ConvOr(instruction as IROrInstruction);
					break;
				case IRInstructionType.Xor:
					ConvXor(instruction as IRXorInstruction);
					break;
				case IRInstructionType.And:
					ConvAnd(instruction as IRAndInstruction);
					break;

				case IRInstructionType.Trunc:
					ConvTrunc(instruction as IRTruncInstruction);
					break;
				case IRInstructionType.Ext:
					ConvExt(instruction as IRExtInstruction);
					break;
				case IRInstructionType.Fptoi:
					ConvFptoi(instruction as IRFptoiInstruction);
					break;
				case IRInstructionType.Itofp:
					ConvItofp(instruction as IRItofpInstruction);
					break;
				case IRInstructionType.Ptrtoi:
					ConvPtrtoi(instruction as IRPtrtoiInstruction);
					break;
				case IRInstructionType.Itoptr:
					ConvItoptr(instruction as IRItoptrInstruction);
					break;
				case IRInstructionType.Bitcast:
					ConvBitcast(instruction as IRBitcastInstruction);
					break;

				case IRInstructionType.ExtractValue:
					ConvExtractValue(instruction as IRExtractValueInstruction);
					break;
				case IRInstructionType.InsertValue:
					ConvInsertValue(instruction as IRInsertValueInstruction);
					break;

				case IRInstructionType.UnionTagSet:
					ConvUnionTagSet(instruction as IRUnionTagSetInstruction);
					break;
				case IRInstructionType.UnionTagGet:
					ConvUnionTagGet(instruction as IRUnionTagGetInstruction);
					break;
				case IRInstructionType.UnionTagAddr:
					ConvUnionTagAddr(instruction as IRUnionTagAddrInstruction);
					break;
				case IRInstructionType.UnionSet:
					ConvUnionSet(instruction as IRUnionSetInstruction);
					break;
				case IRInstructionType.UnionGet:
					ConvUnionGet(instruction as IRUnionGetInstruction);
					break;
				case IRInstructionType.UnionGep:
					ConvUnionGep(instruction as IRUnionGepInstruction);
					break;

				case IRInstructionType.Call:
					ConvCall(instruction as IRCallInstruction);
					break;

				case IRInstructionType.Cmp:
					ConvCmp(instruction as IRCmpInstruction);
					break;

				case IRInstructionType.Return:
					ConvRet(instruction as IRReturnInstruction);
					break;
				case IRInstructionType.Branch:
					ConvBranch(instruction as IRBranchInstruction);
					break;
				case IRInstructionType.CondBranch:
					ConvCondBranch(instruction as IRCondBranchInstruction);
					break;
				case IRInstructionType.Switch:
					ConvSwitch(instruction as IRSwitchInstruction);
					break;
				}
			}

		}


		void ConvAlloca(IRAllocaInstruction instruction)
		{
			LLVMTypeRef llvmType = ConvType(instruction.Type);

			LLVMValueRef valueRef = _builder.CreateAlloca(llvmType, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvStore(IRStoreInstruction instruction)
		{
			LLVMValueRef srcRef = ValueRefFromOperand(instruction.Src);
			LLVMValueRef dstRef = ValueRefFromOperand(instruction.Dst);
			_builder.CreateStore(srcRef, dstRef);
		}

		void ConvLoad(IRLoadInstruction instruction)
		{
			LLVMValueRef srcRef = ValueRefFromOperand(instruction.Src);
			LLVMValueRef valueRef = _builder.CreateLoad(srcRef, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvGep(IRGepInstruction instruction)
		{
			LLVMValueRef srcRef = ValueRefFromOperand(instruction.Src);

			LLVMValueRef[] indices = new LLVMValueRef[instruction.Indices.Count];
			for (var i = 0; i < instruction.Indices.Count; i++)
			{
				int index = instruction.Indices[i];
				indices[i] = LLVM.ConstInt(LLVM.Int32Type(), (ulong)index, false);
			}

			string retName = instruction.RetOperand.Iden;
			LLVMValueRef valueRef = _builder.CreateInBoundsGEP(srcRef, indices, retName);
			_valueRefMap.Add(retName, valueRef);
		}

		void ConvNeg(IRNegInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);

			if (IsFloatingPoint(instruction.RetOperand.Type))
			{
				LLVMValueRef valueRef = _builder.CreateFNeg(op, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMValueRef valueRef = _builder.CreateNeg(op, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvCompl(IRComplInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);
			
			LLVMTypeRef xorType = ConvType(instruction.RetOperand.Type);
			LLVMValueRef xorValue = LLVM.ConstInt(xorType, UInt64.MaxValue, new LLVMBool(0));
			LLVMValueRef valueRef = _builder.CreateXor(op, xorValue, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvInc(IRIncInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);
			LLVMValueRef one = LLVM.ConstInt(LLVM.Int32Type(), 1, new LLVMBool(0));
			LLVMValueRef valueRef = _builder.CreateFAdd(op, one, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvDec(IRDecInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);
			LLVMValueRef one = LLVM.ConstInt(LLVM.Int32Type(), 1, new LLVMBool(0));
			LLVMValueRef valueRef = _builder.CreateSub(op, one, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvAdd(IRAddInstruction instruction)
		{
			LLVMValueRef op1 = ValueRefFromOperand(instruction.Operand1);
			LLVMValueRef op0 = ValueRefFromOperand(instruction.Operand0);
			if (IsFloatingPoint(instruction.RetOperand.Type))
			{
				LLVMValueRef valueRef = _builder.CreateFAdd(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMValueRef valueRef = _builder.CreateAdd(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvSub(IRSubInstruction instruction)
		{
			LLVMValueRef op1 = ValueRefFromOperand(instruction.Operand1);
			LLVMValueRef op0 = ValueRefFromOperand(instruction.Operand0);
			if (IsFloatingPoint(instruction.RetOperand.Type))
			{
				LLVMValueRef valueRef = _builder.CreateFSub(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMValueRef valueRef = _builder.CreateSub(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvMul(IRMulInstruction instruction)
		{
			LLVMValueRef op1 = ValueRefFromOperand(instruction.Operand1);
			LLVMValueRef op0 = ValueRefFromOperand(instruction.Operand0);
			if (IsFloatingPoint(instruction.RetOperand.Type))
			{
				LLVMValueRef valueRef = _builder.CreateFMul(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else if (IsUnsigned(instruction.RetOperand.Type))
			{
				LLVMValueRef valueRef = _builder.CreateMul(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvDiv(IRDivInstruction instruction)
		{
			LLVMValueRef op1 = ValueRefFromOperand(instruction.Operand1);
			LLVMValueRef op0 = ValueRefFromOperand(instruction.Operand0);
			if (IsFloatingPoint(instruction.RetOperand.Type))
			{
				LLVMValueRef valueRef = _builder.CreateFDiv(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else if (IsUnsigned(instruction.RetOperand.Type))
			{
				LLVMValueRef valueRef = _builder.CreateUDiv(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMValueRef valueRef = _builder.CreateSDiv(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvRem(IRRemInstruction instruction)
		{
			LLVMValueRef op1 = ValueRefFromOperand(instruction.Operand1);
			LLVMValueRef op0 = ValueRefFromOperand(instruction.Operand0);
			if (IsFloatingPoint(instruction.RetOperand.Type))
			{
				LLVMValueRef valueRef = _builder.CreateFRem(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else if (IsUnsigned(instruction.RetOperand.Type))
			{
				LLVMValueRef valueRef = _builder.CreateURem(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMValueRef valueRef = _builder.CreateSRem(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvShift(IRShiftInstruction instruction)
		{
			LLVMValueRef op1 = ValueRefFromOperand(instruction.Operand1);
			LLVMValueRef op0 = ValueRefFromOperand(instruction.Operand0);
			if (instruction.Dir == IRShiftDir.Left)
			{
				LLVMValueRef valueRef = _builder.CreateShl(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else if (instruction.Dir == IRShiftDir.LogicRight)
			{
				LLVMValueRef valueRef = _builder.CreateLShr(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMValueRef valueRef = _builder.CreateAShr(op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvOr(IROrInstruction instruction)
		{
			LLVMValueRef op1 = ValueRefFromOperand(instruction.Operand1);
			LLVMValueRef op0 = ValueRefFromOperand(instruction.Operand0);
			LLVMValueRef valueRef = _builder.CreateOr(op0, op1, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvXor(IRXorInstruction instruction)
		{
			LLVMValueRef op1 = ValueRefFromOperand(instruction.Operand1);
			LLVMValueRef op0 = ValueRefFromOperand(instruction.Operand0);
			LLVMValueRef valueRef = _builder.CreateXor(op0, op1, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvAnd(IRAndInstruction instruction)
		{
			LLVMValueRef op1 = ValueRefFromOperand(instruction.Operand1);
			LLVMValueRef op0 = ValueRefFromOperand(instruction.Operand0);
			LLVMValueRef valueRef = _builder.CreateAnd(op0, op1, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvTrunc(IRTruncInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);

			if (IsFloatingPoint(instruction.RetOperand.Type))
			{
				LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
				LLVMValueRef valueRef = _builder.CreateFPTrunc(op, dstType, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
				LLVMValueRef valueRef = _builder.CreateTrunc(op, dstType, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvExt(IRExtInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);

			if (IsFloatingPoint(instruction.RetOperand.Type))
			{
				LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
				LLVMValueRef valueRef = _builder.CreateFPExt(op, dstType, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else if (instruction.Ext == IRExtType.Zero || 
			         instruction.Ext == IRExtType.Default && IsUnsigned(instruction.RetOperand.Type))
			{
				LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
				LLVMValueRef valueRef = _builder.CreateZExt(op, dstType, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
				LLVMValueRef valueRef = _builder.CreateSExt(op, dstType, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvFptoi(IRFptoiInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);

			if (IsUnsigned(instruction.RetOperand.Type))
			{
				LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
				LLVMValueRef valueRef = _builder.CreateFPToUI(op, dstType, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
				LLVMValueRef valueRef = _builder.CreateFPToSI(op, dstType, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvItofp(IRItofpInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);

			if (IsUnsigned(instruction.Operand.Type))
			{
				LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
				LLVMValueRef valueRef = _builder.CreateUIToFP(op, dstType, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
				LLVMValueRef valueRef = _builder.CreateSIToFP(op, dstType, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvPtrtoi(IRPtrtoiInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);
			LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
			LLVMValueRef valueRef = _builder.CreatePtrToInt(op, dstType, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvItoptr(IRItoptrInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);
			LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
			LLVMValueRef valueRef = _builder.CreateIntToPtr(op, dstType, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvBitcast(IRBitcastInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);
			LLVMTypeRef dstType = ConvType(instruction.RetOperand.Type);
			LLVMValueRef valueRef = _builder.CreateBitCast(op, dstType, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvExtractValue(IRExtractValueInstruction instruction)
		{
			LLVMValueRef structOp = ValueRefFromOperand(instruction.Aggregate);
			LLVMValueRef ret = _builder.CreateExtractValue(structOp, (uint)instruction.Index, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, ret);
		}

		void ConvInsertValue(IRInsertValueInstruction instruction)
		{
			LLVMValueRef structOp = ValueRefFromOperand(instruction.Aggregate);
			LLVMValueRef valOp = ValueRefFromOperand(instruction.Src);
			LLVMValueRef ret = _builder.CreateInsertValue(structOp, valOp, (uint)instruction.Index, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, ret);
		}

		void ConvUnionTagSet(IRUnionTagSetInstruction instruction)
		{
			LLVMValueRef aggr = ValueRefFromOperand(instruction.UnionOperand);
			LLVMValueRef val = ValueRefFromOperand(instruction.TagOperand);
			
			string baseTypeIden = instruction.UnionOperand.Type.CustomIden;
			LLVMTypeRef aggrType = _llvmTypeRefs[baseTypeIden];
			string tmpNameBase = instruction.RetOperand.Iden;
			
			LLVMValueRef unionAddr = _builder.CreateAlloca(aggrType, $"{tmpNameBase}__addr");
			_builder.CreateStore(aggr, unionAddr);

			LLVMValueRef tmp = _builder.CreateStructGEP(unionAddr, 0, $"{tmpNameBase}__elem_addr");
			_builder.CreateStore(val, tmp);

			tmp = _builder.CreateLoad(unionAddr, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, tmp);
		}

		void ConvUnionTagGet(IRUnionTagGetInstruction instruction)
		{
			LLVMValueRef aggr = ValueRefFromOperand(instruction.UnionOperand);
			IRType aggrType = instruction.UnionOperand.Type;

			string tmpName = $"{instruction.UnionOperand.Iden}__{instruction.RetOperand.Iden}";
			
			LLVMTypeRef llvmAggrType = ConvType(aggrType);
				
			LLVMValueRef unionAddr = _builder.CreateAlloca(llvmAggrType, $"{tmpName}__addr");
			_builder.CreateStore(aggr, unionAddr);

			LLVMValueRef tmp = _builder.CreateStructGEP(unionAddr, 0, $"{tmpName}__tag");
			tmp = _builder.CreateLoad(tmp, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, tmp);
			
		}

		void ConvUnionTagAddr(IRUnionTagAddrInstruction instruction)
		{
			LLVMValueRef aggr = ValueRefFromOperand(instruction.UnionOperand);

			string tmpName = $"{instruction.UnionOperand.Iden}__{instruction.RetOperand.Iden}";

			LLVMValueRef tmp = _builder.CreateStructGEP(aggr, 0, $"{tmpName}__tag");
			tmp = _builder.CreateLoad(tmp, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, tmp);
		}

		void ConvUnionSet(IRUnionSetInstruction instruction)
		{
			LLVMValueRef aggr = ValueRefFromOperand(instruction.UnionOperand);
			LLVMValueRef val = ValueRefFromOperand(instruction.ValueOperand);

			uint index = (uint) (instruction.TagUnion ? 1 : 0);

			string baseTypeIden = instruction.UnionOperand.Type.CustomIden;
			string instIden = $"{baseTypeIden}_{instruction.Index}";
			LLVMTypeRef unionInst = _llvmTypeRefs[instIden];
			LLVMTypeRef aggrType = _llvmTypeRefs[baseTypeIden];
			string tmpNameBase = instruction.RetOperand.Iden;
			
			LLVMValueRef unionAddr = _builder.CreateAlloca(aggrType, $"{tmpNameBase}__addr");
			_builder.CreateStore(aggr, unionAddr);

			LLVMValueRef castAddr = _builder.CreateBitCast(unionAddr, LLVMTypeRef.PointerType(unionInst, 0), $"{tmpNameBase}__cast_addr");
			LLVMValueRef tmp = _builder.CreateStructGEP(castAddr, index, $"{tmpNameBase}__elem_addr");
			_builder.CreateStore(val, tmp);

			tmp = _builder.CreateLoad(unionAddr, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, tmp);
		}

		void ConvUnionGet(IRUnionGetInstruction instruction)
		{
			LLVMValueRef aggr = ValueRefFromOperand(instruction.UnionOperand);
			IRType aggrType = instruction.UnionOperand.Type;

			string tmpName = $"{instruction.UnionOperand.Iden}__{instruction.RetOperand.Iden}";
			uint index = (uint)(instruction.TagUnion ? 1 : 0);
			
			LLVMTypeRef llvmAggrType = ConvType(aggrType);

			LLVMValueRef unionAddr = _builder.CreateAlloca(llvmAggrType, $"{tmpName}__addr");
			_builder.CreateStore(aggr, unionAddr);

			string baseTypeIden = instruction.UnionOperand.Type.CustomIden;
			string instIden = $"{baseTypeIden}_{instruction.Index}";
			LLVMTypeRef unionInst = _llvmTypeRefs[instIden];

			LLVMValueRef castAddr = _builder.CreateBitCast(unionAddr, LLVMTypeRef.PointerType(unionInst, 0), $"{tmpName}__cast_addr");
			LLVMValueRef tmp = _builder.CreateStructGEP(castAddr, index, $"{tmpName}__tag");
			tmp = _builder.CreateLoad(tmp, instruction.RetOperand.Iden);
			_valueRefMap.Add(instruction.RetOperand.Iden, tmp);
		}

		void ConvUnionGep(IRUnionGepInstruction instruction)
		{
			LLVMValueRef aggr = ValueRefFromOperand(instruction.UnionOperand);

			string tmpName = $"{instruction.UnionOperand.Iden}__{instruction.RetOperand.Iden}";
			uint index = (uint)(instruction.TagUnion ? 1 : 0);

			string baseTypeIden = instruction.UnionOperand.Type.CustomIden;
			string instIden = $"{baseTypeIden}_{instruction.Index}";
			LLVMTypeRef unionInst = _llvmTypeRefs[instIden];

			LLVMValueRef castAddr = _builder.CreateBitCast(aggr, LLVMTypeRef.PointerType(unionInst, 0), $"{tmpName}__cast_addr");

			LLVMValueRef tmp = _builder.CreateStructGEP(castAddr, index, $"{tmpName}__tag");
			_valueRefMap.Add(instruction.RetOperand.Iden, tmp);
		}

		void ConvCall(IRCallInstruction instruction)
		{
			List<LLVMValueRef> operands = null;
			List<IRType> expectedTypes = _expectedTypes[instruction.Identifier];
			if (instruction.Operands != null && instruction.Operands.Count > 0)
			{
				operands = new List<LLVMValueRef>();
				for (var i = 0; i < instruction.Operands.Count; i++)
				{
					IROperand operand = instruction.Operands[i];
					LLVMValueRef op = ValueRefFromOperand(operand);

					operands.Add(op);
				}
			}

			LLVMValueRef funcRef = LLVM.GetNamedFunction(_module, instruction.Identifier);
			LLVMValueRef valueRef = _builder.CreateCall(funcRef, operands?.ToArray() ?? new LLVMValueRef[0], instruction.RetOperand?.Iden ?? "");

			if (instruction.RetOperand != null)
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
		}

		void ConvCmp(IRCmpInstruction instruction)
		{
			LLVMValueRef op0 = ValueRefFromOperand(instruction.Operand0);
			LLVMValueRef op1 = ValueRefFromOperand(instruction.Operand1);

			if (IsFloatingPoint(instruction.RetOperand.Type))
			{
				LLVMRealPredicate pred = GetFPCmpMode(instruction.Cmp);
				LLVMValueRef valueRef = _builder.CreateFCmp(pred, op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else if (IsUnsigned(instruction.RetOperand.Type))
			{
				LLVMIntPredicate pred = GetUIntCmpMode(instruction.Cmp);
				LLVMValueRef valueRef = _builder.CreateICmp(pred, op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
			else
			{
				LLVMIntPredicate pred = GetSIntCmpMode(instruction.Cmp);
				LLVMValueRef valueRef = _builder.CreateICmp(pred, op0, op1, instruction.RetOperand.Iden);
				_valueRefMap.Add(instruction.RetOperand.Iden, valueRef);
			}
		}

		void ConvRet(IRReturnInstruction instruction)
		{
			if (instruction.Operand == null)
			{
				_builder.CreateRetVoid();
			}
			else
			{
				LLVMValueRef valueRef = ValueRefFromOperand(instruction.Operand);
				_builder.CreateRet(valueRef);
			}
		}

		void ConvBranch(IRBranchInstruction instruction)
		{
			LLVMBasicBlockRef basicBlock = _llvmBasicBlockNameMap[instruction.Label];
			_builder.CreateBr(basicBlock);
		}

		void ConvCondBranch(IRCondBranchInstruction instruction)
		{
			LLVMValueRef valueRef = ValueRefFromOperand(instruction.Condition);
			LLVMBasicBlockRef trueBlock = _llvmBasicBlockNameMap[instruction.TrueLabel];
			LLVMBasicBlockRef falseBlock = _llvmBasicBlockNameMap[instruction.FalseLabel];
			_builder.CreateCondBr(valueRef, trueBlock, falseBlock);
		}

		void ConvSwitch(IRSwitchInstruction instruction)
		{
			LLVMValueRef op = ValueRefFromOperand(instruction.Operand);
			LLVMBasicBlockRef defaultBlock = _llvmBasicBlockNameMap[instruction.DefaultLabel];
			LLVMValueRef sw = _builder.CreateSwitch(op, defaultBlock, (uint)(instruction.Cases?.Count ?? 0));

			if (instruction.Cases != null)
			{
				foreach (IRSwitchCase switchCase in instruction.Cases)
				{
					LLVMValueRef caseOp = ValueRefFromOperand(switchCase.Value);
					LLVMBasicBlockRef caseBlock = _llvmBasicBlockNameMap[switchCase.Label];
					sw.AddCase(caseOp, caseBlock);
				}
			}
		}

		LLVMTypeRef ConvType(IRType type)
		{
			if (type == null ||
			    type.BaseType == IRBaseType.Void &&
			    type.PtrCount == 0 &&
			    type.Address == false)
				return LLVM.VoidType();

			LLVMTypeRef llvmType = new LLVMTypeRef(IntPtr.Zero);

			if (type.BaseType == IRBaseType.Array)
			{
				LLVMTypeRef inner = ConvType(type.SubType);
				llvmType = LLVM.ArrayType(inner, (uint)type.ArraySize);
			}
			else
			{
				switch (type.BaseType)
				{
				case IRBaseType.I1:
					llvmType = LLVMTypeRef.Int1Type();
					break;
				case IRBaseType.I8:
				case IRBaseType.U8:
					llvmType = LLVMTypeRef.Int8Type();
					break;
				case IRBaseType.I16:
				case IRBaseType.U16:
					llvmType = LLVMTypeRef.Int16Type();
					break;
				case IRBaseType.I32:
				case IRBaseType.U32:
					llvmType = LLVMTypeRef.Int32Type();
					break;
				case IRBaseType.I64:
				case IRBaseType.U64:
					llvmType = LLVMTypeRef.Int64Type();
					break;
				case IRBaseType.F32:
					llvmType = LLVMTypeRef.FloatType();
					break;
				case IRBaseType.F64:
					llvmType = LLVMTypeRef.DoubleType();
					break;
				case IRBaseType.Custom:
				{
					if (!_llvmTypeRefs.TryGetValue(type.CustomIden, out llvmType))
					{
						return new LLVMTypeRef();
					}
					break;
				}
				}

			}

			if (type.PtrCount > 0)
			{
				for (int i = 0; i < type.PtrCount; i++)
				{
					// TODO: Check LLVM address space
					llvmType = LLVMTypeRef.PointerType(llvmType, 0);
				}
			}

			if (type.Address)
			{
				llvmType = LLVMTypeRef.PointerType(llvmType, 0);
			}

			return llvmType;
		}

		void ConvTypedefs(List<IRTypeDef> block, CompilerContext context)
		{
			// Predefine types a opaque
			{
				foreach (IRTypeDef def in block)
				{
					if (def.Kind == IRTypeDefKind.Struct)
					{
						LLVMTypeRef typeRef = LLVMTypeRef.StructCreateNamed(_llvmContext, def.Iden);
						_llvmTypeRefs.Add(def.Iden, typeRef);
					}
					else if (def.Kind == IRTypeDefKind.Union)
					{
						LLVMTypeRef typeRef;
						for (var i = 0; i < def.SubTypes.Count; i++)
						{
							string elemIden = $"{def.Iden}_{i}";
							typeRef = LLVMTypeRef.StructCreateNamed(_llvmContext, elemIden);
							_llvmTypeRefs.Add(elemIden, typeRef);
						}

						// dummy (default type)
						typeRef = LLVMTypeRef.StructCreateNamed(_llvmContext, def.Iden);
						_llvmTypeRefs.Add(def.Iden, typeRef);
					}
					else if (def.Kind == IRTypeDefKind.TagUnion)
					{
						LLVMTypeRef typeRef;
						for (var i = 0; i < def.SubTypes.Count; i++)
						{
							string elemIden = $"{def.Iden}_{i}";
							typeRef = LLVMTypeRef.StructCreateNamed(_llvmContext, elemIden);
							_llvmTypeRefs.Add(elemIden, typeRef);
						}

						// dummy (default type)
						typeRef = LLVMTypeRef.StructCreateNamed(_llvmContext, def.Iden);
						_llvmTypeRefs.Add(def.Iden, typeRef);
					}
				}
			}

			// Fill in opaque types
			{
				foreach (IRTypeDef def in block)
				{
					if (def.Kind == IRTypeDefKind.Struct)
					{
						LLVMTypeRef[] llvmSubTypes = new LLVMTypeRef[def.SubTypes.Count];
						for (var i = 0; i < def.SubTypes.Count; i++)
						{
							IRType subType = def.SubTypes[i];
							llvmSubTypes[i] = ConvType(subType);
						}

						LLVMTypeRef typeRef = _llvmTypeRefs[def.Iden];
						// TODO: Packed
						typeRef.StructSetBody(llvmSubTypes, false);
					}
					else if (def.Kind == IRTypeDefKind.Union)
					{
						string iden = "U" + def.Iden.Substring(2, def.Iden.Length - 4);
						TypeInfo typeInfo = context.GetTypeInfo(iden);

						for (var i = 0; i < def.SubTypes.Count; i++)
						{
							IRType subType = def.SubTypes[i];
							LLVMTypeRef elemType = ConvType(subType);
							string elemIden = $"{def.Iden}_{i}";
							LLVMTypeRef valType = _llvmTypeRefs[elemIden];

							TypeInfo elemInfo = IRHelpers.GetTypeInfo(subType, context);
							LLVMTypeRef[] types;
							if (elemInfo.Size < typeInfo.Size)
							{
								ulong diff = typeInfo.Size - elemInfo.Size;
								LLVMTypeRef paddingType = LLVMTypeRef.ArrayType(LLVMTypeRef.Int8Type(), (uint)diff);

								types = new[] {elemType, paddingType};
							}
							else
							{
								types = new[] {elemType};
							}

							valType.StructSetBody(types, false);
						}

						// update dummy
						LLVMTypeRef dummy = _llvmTypeRefs[def.Iden];
						LLVMTypeRef dummyDataType = LLVM.ArrayType(LLVM.Int8Type(), (uint)typeInfo.Size);
						dummy.StructSetBody(new[] { dummyDataType }, false);
					}
					else if (def.Kind == IRTypeDefKind.TagUnion)
					{
						LLVMTypeRef tagType = ConvType(def.TagType);
						string iden = "E" + def.Iden.Substring(2, def.Iden.Length - 4);
						TypeInfo typeInfo = context.GetTypeInfo(iden);
						TypeInfo tagInfo = IRHelpers.GetTypeInfo(def.TagType, context);

						for (var i = 0; i < def.SubTypes.Count; i++)
						{
							IRType subType = def.SubTypes[i];
							LLVMTypeRef elemType = ConvType(subType);
							string elemIden = $"{def.Iden}_{i}";
							LLVMTypeRef valType = _llvmTypeRefs[elemIden];

							TypeInfo elemInfo = IRHelpers.GetTypeInfo(subType, context);
							LLVMTypeRef[] types;
							if (elemInfo.Size + tagInfo.Size < typeInfo.Size)
							{
								ulong diff = typeInfo.Size - elemInfo.Size - tagInfo.Size;
								LLVMTypeRef paddingType = LLVMTypeRef.ArrayType(LLVMTypeRef.Int8Type(), (uint)diff);

								types = new[] { tagType, elemType, paddingType };
							}
							else
							{
								types = new[] { tagType, elemType };
							}

							valType.StructSetBody(types, false);
						}

						// update dummy
						LLVMTypeRef dummy = _llvmTypeRefs[def.Iden];
						ulong dummyDiff = typeInfo.Size - tagInfo.Size;
						LLVMTypeRef dummyDataType = LLVM.ArrayType(LLVM.Int8Type(), (uint)dummyDiff);
						dummy.StructSetBody(new []{ tagType, dummyDataType }, false);
					}
				}
			}
		}

		void ConvGlobals(List<IRGlobal> block)
		{
			foreach (IRGlobal global in block)
			{
				LLVMTypeRef typeRef = ConvType(global.Type);
				LLVMValueRef valueRef = LLVM.AddGlobal(_module, typeRef, global.Identifier);
				LLVM.SetGlobalConstant(valueRef, new LLVMBool((global.Attribs.GlobalFlags & IRGlobalFlags.Const) != 0 ? 1 : 0));
				SetAttributes(valueRef, global.Attribs, true);

				LLVMValueRef initRef;
				if (IsFloatingPoint(global.Type))
				{
					LLVMTypeRef baseType;
					if (global.Type.BaseType == IRBaseType.F32)
						baseType = LLVMTypeRef.Int32Type();
					else
						baseType = LLVMTypeRef.Int64Type();

					initRef = LLVM.ConstInt(baseType, 0, new LLVMBool(0));
					initRef = LLVM.ConstBitCast(initRef, typeRef);
				}
				else if (global.Type.Address || global.Type.PtrCount > 0)
				{
					LLVMTypeRef baseType;
					if (CmdLine.IsX64)
						baseType = LLVMTypeRef.Int64Type();
					else
						baseType = LLVMTypeRef.Int32Type();

					initRef = LLVM.ConstInt(baseType, 0, new LLVMBool(0));
					initRef = LLVM.ConstBitCast(initRef, typeRef);
				}
				else
				{
					initRef = LLVM.ConstInt(typeRef, 0, new LLVMBool(0));
				}

				LLVM.SetInitializer(valueRef, initRef);

				if (global.HasValue)
				{
					LLVMValueRef initValue;
					if (global.StringValue != null)
					{
						initValue = LLVM.ConstString(global.StringValue, (uint)global.StringValue.Length, false);
					}
					else
					{
						if (global.Type.Address ||
						    global.Type.PtrCount > 0)
						{
							initValue = LLVM.ConstInt(LLVMTypeRef.Int64Type(), (ulong) global.NumValue, new LLVMBool(0));
							initValue = LLVMValueRef.ConstIntToPtr(initValue, typeRef);
						}
						else
						{
							bool fp = !global.Type.Address &&
							          global.Type.PtrCount == 0 &&
							          global.Type.ArraySize == 0 &&
							          (global.Type.BaseType == IRBaseType.F32 ||
							           global.Type.BaseType == IRBaseType.F64);

							LLVMTypeRef baseType = typeRef;
							if (fp)
							{
								if (global.Type.BaseType == IRBaseType.F32)
									baseType = LLVMTypeRef.Int32Type();
								else
									baseType = LLVMTypeRef.Int64Type();
							}

							initValue = LLVM.ConstInt(baseType, (ulong) global.NumValue, new LLVMBool(global.NumValue < 0 ? 1 : 0));

							if (fp)
							{
								initValue = LLVM.ConstSIToFP(initValue, typeRef);
							}
						}
					}
					LLVM.SetInitializer(valueRef, initValue);

				}
				_globalRefMap.Add(global.Identifier, valueRef);
			}
		}

		LLVMTypeRef LLVMTypeFromBaseType(IRBaseType type)
		{
			switch (type)
			{
			case IRBaseType.I1:
				return LLVMTypeRef.Int1Type();
			case IRBaseType.I8:
			case IRBaseType.U8:
				return LLVMTypeRef.Int8Type();
			case IRBaseType.I16:
			case IRBaseType.U16:
				return LLVMTypeRef.Int16Type();
			case IRBaseType.I32:
			case IRBaseType.U32:
				return LLVMTypeRef.Int32Type();
			case IRBaseType.I64:
			case IRBaseType.U64:
				return LLVMTypeRef.Int64Type();
			case IRBaseType.F32:
				return LLVMTypeRef.FloatType();
			case IRBaseType.F64:
				return LLVMTypeRef.DoubleType();
			default:
				return new LLVMTypeRef(IntPtr.Zero);
			}
		}

		LLVMValueRef ValueRefFromOperand(IROperand operand)
		{
			if (operand.Constant)
			{
				if (!operand.Type.Address && operand.Type.PtrCount == 0)
				{
					if (operand.Type.BaseType == IRBaseType.F32 || operand.Type.BaseType == IRBaseType.F64)
					{
						LLVMTypeRef intType = LLVMTypeFromBaseType(operand.Type.BaseType == IRBaseType.F32 ? IRBaseType.I32 : IRBaseType.I64);
						LLVMValueRef constRef = LLVM.ConstInt(intType, (ulong)operand.IntLiteral, new LLVMBool(operand.IntLiteral < 0 ? 0 : 1));
						return LLVM.ConstBitCast(constRef, ConvType(operand.Type));
					}
					else
					{
						return LLVM.ConstInt(ConvType(operand.Type), (ulong)operand.IntLiteral, new LLVMBool(operand.IntLiteral < 0 ? 0 : 1));
					}
				}
				
				return LLVM.ConstPointerNull(ConvType(operand.Type));
			}
			else if (operand.Global)
			{
				if (_globalRefMap.TryGetValue(operand.Iden, out LLVMValueRef global))
					return global;

				return _funcRefMap[operand.Iden];
			}
			else if (operand.Undef)
			{
				LLVMTypeRef aggrType = ConvType(operand.Type);
				return LLVM.GetUndef(aggrType);
			}
			else if (_curArgs.ContainsKey(operand.Iden))
			{
				return _curArgs[operand.Iden];
			}
			else
			{
				return _valueRefMap[operand.Iden];
			}
		}

		LLVMIntPredicate GetSIntCmpMode(IRCmpType type)
		{
			switch (type)
			{
			default:
			case IRCmpType.Eq:	return LLVMIntPredicate.LLVMIntEQ;
			case IRCmpType.Ne:	return LLVMIntPredicate.LLVMIntNE;
			case IRCmpType.Lt:	return LLVMIntPredicate.LLVMIntSLT;
			case IRCmpType.Le:	return LLVMIntPredicate.LLVMIntSLE;
			case IRCmpType.Gt:	return LLVMIntPredicate.LLVMIntSGT;
			case IRCmpType.Ge:	return LLVMIntPredicate.LLVMIntSGE;
			}
		}

		LLVMIntPredicate GetUIntCmpMode(IRCmpType type)
		{
			switch (type)
			{
			default:
			case IRCmpType.Eq:	return LLVMIntPredicate.LLVMIntEQ;
			case IRCmpType.Ne:	return LLVMIntPredicate.LLVMIntNE;
			case IRCmpType.Lt:	return LLVMIntPredicate.LLVMIntULT;
			case IRCmpType.Le:	return LLVMIntPredicate.LLVMIntULE;
			case IRCmpType.Gt:	return LLVMIntPredicate.LLVMIntUGT;
			case IRCmpType.Ge:	return LLVMIntPredicate.LLVMIntUGE;
			}
		}

		LLVMRealPredicate GetFPCmpMode(IRCmpType type)
		{
			switch (type)
			{
			default:
			case IRCmpType.Eq:	return LLVMRealPredicate.LLVMRealOEQ;
			case IRCmpType.Ne:	return LLVMRealPredicate.LLVMRealONE;
			case IRCmpType.Lt:	return LLVMRealPredicate.LLVMRealOLT;
			case IRCmpType.Le:	return LLVMRealPredicate.LLVMRealOLE;
			case IRCmpType.Gt:	return LLVMRealPredicate.LLVMRealOGT;
			case IRCmpType.Ge:	return LLVMRealPredicate.LLVMRealOGE;
			case IRCmpType.UEq:	return LLVMRealPredicate.LLVMRealUEQ;
			case IRCmpType.UNe:	return LLVMRealPredicate.LLVMRealUNE;
			case IRCmpType.ULt:	return LLVMRealPredicate.LLVMRealULT;
			case IRCmpType.ULe:	return LLVMRealPredicate.LLVMRealULE;
			case IRCmpType.UGt:	return LLVMRealPredicate.LLVMRealUGT;
			case IRCmpType.UGe:	return LLVMRealPredicate.LLVMRealUGE;
			case IRCmpType.UNo:	return LLVMRealPredicate.LLVMRealUNO;
			}
		}

		bool IsFloatingPoint(IRType type)
		{
			return type.Address == false && type.PtrCount == 0 &&
				(type.BaseType == IRBaseType.F32 || type.BaseType == IRBaseType.F64);
		}

		bool IsUnsigned(IRType type)
		{
			return type.Address == false && type.PtrCount == 0 &&
			       (type.BaseType == IRBaseType.U8 || type.BaseType == IRBaseType.U16 ||
			        type.BaseType == IRBaseType.U32 || type.BaseType == IRBaseType.U64);
		}

		void SetAttributes(LLVMValueRef valueRef, IRAttributes attribs, bool isValue)
		{
			// TODO: LLVM linkage and visibility may not be correct
			LLVMLinkage linkage = LLVMLinkage.LLVMGhostLinkage;
			LLVMVisibility visibility = LLVMVisibility.LLVMDefaultVisibility;
			LLVMDLLStorageClass dllStorageClass = LLVMDLLStorageClass.LLVMDefaultStorageClass;
			switch (attribs.Linkage)
			{
			case IRLinkage.Export:
				dllStorageClass = LLVMDLLStorageClass.LLVMDLLExportStorageClass;
				break;
			case IRLinkage.Import:
				dllStorageClass = LLVMDLLStorageClass.LLVMDLLImportStorageClass;
				break;
			case IRLinkage.Public:
				break;
			case IRLinkage.PublicExternal:
				linkage = LLVMLinkage.LLVMExternalLinkage;
				break;
			case IRLinkage.Hidden:
				visibility = LLVMVisibility.LLVMHiddenVisibility;
				if (isValue)
					linkage = LLVMLinkage.LLVMLinkOnceAnyLinkage;
				break;
			case IRLinkage.HiddenExternal:
				//visibility = LLVMVisibility.LLVMHiddenVisibility;
				linkage = LLVMLinkage.LLVMExternalLinkage;
				break;
			case IRLinkage.Shared:
				visibility = LLVMVisibility.LLVMHiddenVisibility;
				break;
			case IRLinkage.Private:
			{
				if (isValue)
				{
					linkage = LLVMLinkage.LLVMLinkerPrivateLinkage;
				}
				else
				{
					visibility = LLVMVisibility.LLVMHiddenVisibility;
				}

				break;
			}
			default:
				throw new ArgumentOutOfRangeException();
			}
			LLVM.SetLinkage(valueRef, linkage);
			LLVM.SetVisibility(valueRef, visibility);
			LLVM.SetDLLStorageClass(valueRef, dllStorageClass);

			if (attribs.Alignment != 0)
			{
				LLVM.SetAlignment(valueRef, (uint)attribs.Alignment);
			}

			if (attribs.CallConv != IRCallConv.None)
			{
				LLVMCallConv callConv = LLVMCallConv.LLVMCCallConv;
				switch (attribs.CallConv)
				{
				case IRCallConv.MJay:
					break;
				case IRCallConv.C:
					break;
				case IRCallConv.Windows:
					callConv = LLVMCallConv.LLVMX86StdcallCallConv;
					break;
				}
				LLVM.SetFunctionCallConv(valueRef, (uint)callConv);
			}

		}


	}
}
