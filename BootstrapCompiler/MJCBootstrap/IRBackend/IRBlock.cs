﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.IRBackend.General;

namespace MJC.IRBackend
{
	public class IRFunction
	{
		public string Identifier;
		public List<IROperand> Params;
		public IRType RetType;
		public IRAttributes Attribs;

		public List<IRBasicBlock> Blocks = new List<IRBasicBlock>();
	}

	public class IRBasicBlock
	{
		public string Label;
		public List<IRInstruction> Instructions = new List<IRInstruction>();
	}
}
