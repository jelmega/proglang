﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;

namespace MJC.IRBackend
{
	public enum IRInstructionType
	{
		Alloca,

		Store,
		Load,
		Gep,

		Neg,
		Inc,
		Dec,
		Compl,

		Add,
		Sub,
		Mul,
		Div,
		Rem,
		Shift,
		Or,
		Xor,
		And,

		Trunc,
		Ext,
		Fptoi,
		Itofp,
		Ptrtoi,
		Itoptr,
		Bitcast,

		ExtractValue,
		InsertValue,

		UnionTagSet,
		UnionTagGet,
		UnionTagAddr,
		UnionSet,
		UnionGet,
		UnionGep,

		Call,
		Cmp,

		Return,
		Branch,
		CondBranch,
		Switch,
	}

	public class IRInstruction
	{
		public IRInstructionType InstructionType;
	}

	public class IRAllocaInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IRType Type;

		public IRAllocaInstruction(IROperand retOperand, IRType type)
		{
			RetOperand = retOperand;
			Type = type;
			InstructionType = IRInstructionType.Alloca;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = alloca {Type}";
		}
	}

	public class IRStoreInstruction : IRInstruction
	{
		public IROperand Src;
		public IROperand Dst;

		public IRStoreInstruction(IROperand src, IROperand dst)
		{
			Src = src;
			Dst = dst;
			InstructionType = IRInstructionType.Store;
		}

		public override string ToString()
		{
			return $"store {Src} , {Dst}";
		}
	}

	public class IRLoadInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand Src;

		public IRLoadInstruction(IROperand retOperand, IROperand src)
		{
			RetOperand = retOperand;
			Src = src;
			InstructionType = IRInstructionType.Load;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = load {Src}";
		}
	}

	public class IRGepInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand Src;
		public List<int> Indices;

		public IRGepInstruction(IROperand retOperand, IROperand src, params int[] indices)
		{
			RetOperand = retOperand;
			Src = src;
			Indices = indices.ToList();
			InstructionType = IRInstructionType.Gep;
		}

		public override string ToString()
		{
			string indexStr = null;
			foreach (int i in Indices)
			{
				if (indexStr == null)
					indexStr = $"{i}";
				else
					indexStr += $" , {i}";
			}

			return $"%{RetOperand.Iden} = gep : {RetOperand.Type} , {Src} , {indexStr}";
		}
	}

	public class IRNegInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand Operand;

		public IRNegInstruction(IROperand retOperand, IROperand op)
		{
			Operand = op;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Neg;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = neg {Operand}";
		}
	}

	public class IRComplInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand Operand;

		public IRComplInstruction(IROperand retOperand, IROperand op)
		{
			Operand = op;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Compl;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = compl {Operand}";
		}
	}

	public class IRIncInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand Operand;

		public IRIncInstruction(IROperand retOperand, IROperand op)
		{
			Operand = op;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Inc;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = inc {Operand}";
		}
	}

	public class IRDecInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand Operand;

		public IRDecInstruction(IROperand retOperand, IROperand op)
		{
			Operand = op;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Dec;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = sub {Operand}";
		}
	}

	public class IRAddInstruction : IRInstruction
	{
		public IROperand Operand0;
		public IROperand Operand1;
		public IROperand RetOperand;

		public IRAddInstruction(IROperand retOperand, IROperand op0, IROperand op1)
		{
			Operand0 = op0;
			Operand1 = op1;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Add;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = add %{Operand0.Iden} , %{Operand1.Iden} : {RetOperand.Type}";
		}
	}

	public class IRSubInstruction : IRInstruction
	{

		public IROperand Operand0;
		public IROperand Operand1;
		public IROperand RetOperand;

		public IRSubInstruction(IROperand retOperand, IROperand op0, IROperand op1)
		{
			Operand0 = op0;
			Operand1 = op1;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Sub;
		}
		public override string ToString()
		{
			return $"%{RetOperand.Iden} = sub %{Operand0.Iden} , %{Operand1.Iden} : {RetOperand.Type}";
		}
	}

	public class IRMulInstruction : IRInstruction
	{
		public IROperand Operand0;
		public IROperand Operand1;
		public IROperand RetOperand;

		public IRMulInstruction(IROperand retOperand, IROperand op0, IROperand op1)
		{
			Operand0 = op0;
			Operand1 = op1;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Mul;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = mul %{Operand0.Iden} , %{Operand1.Iden} : {RetOperand.Type}";
		}
	}

	public class IRDivInstruction : IRInstruction
	{
		public IROperand Operand0;
		public IROperand Operand1;
		public IROperand RetOperand;

		public IRDivInstruction(IROperand retOperand, IROperand op0, IROperand op1)
		{
			Operand0 = op0;
			Operand1 = op1;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Div;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = div %{Operand0.Iden} , %{Operand1.Iden} : {RetOperand.Type}";
		}
	}

	public class IRRemInstruction : IRInstruction
	{
		public IROperand Operand0;
		public IROperand Operand1;
		public IROperand RetOperand;

		public IRRemInstruction(IROperand retOperand, IROperand op0, IROperand op1)
		{
			Operand0 = op0;
			Operand1 = op1;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Rem;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = rem %{Operand0.Iden} , %{Operand1.Iden} : {RetOperand.Type}";
		}
	}

	public enum IRShiftDir : byte
	{
		Left,
		LogicRight,
		ArithRight,
	}

	public class IRShiftInstruction : IRInstruction
	{
		public IRShiftDir Dir;
		public IROperand Operand0;
		public IROperand Operand1;
		public IROperand RetOperand;

		public IRShiftInstruction(IROperand retOperand, IRShiftDir dir, IROperand op0, IROperand op1)
		{
			Dir = dir;
			Operand0 = op0;
			Operand1 = op1;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Shift;
		}

		public override string ToString()
		{
			string shiftDir = "";
			switch (Dir)
			{
			case IRShiftDir.Left:		shiftDir = "l";		break;
			case IRShiftDir.LogicRight:	shiftDir = "lr";	break;
			case IRShiftDir.ArithRight:	shiftDir = "ar";	break;
			}

			return $"%{RetOperand.Iden} = shift {shiftDir} %{Operand0.Iden} , %{Operand1.Iden} : {RetOperand.Type}";
		}
	}

	public class IROrInstruction : IRInstruction
	{
		public IROperand Operand0;
		public IROperand Operand1;
		public IROperand RetOperand;

		public IROrInstruction(IROperand retOperand, IROperand op0, IROperand op1)
		{
			Operand0 = op0;
			Operand1 = op1;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Or;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = or %{Operand0.Iden} , %{Operand1.Iden} : {RetOperand.Type}";
		}
	}

	public class IRXorInstruction : IRInstruction
	{
		public IROperand Operand0;
		public IROperand Operand1;
		public IROperand RetOperand;

		public IRXorInstruction(IROperand retOperand, IROperand op0, IROperand op1)
		{
			Operand0 = op0;
			Operand1 = op1;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Xor;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = xor %{Operand0.Iden} , %{Operand1.Iden} : {RetOperand.Type}";
		}
	}

	public class IRAndInstruction : IRInstruction
	{
		public IROperand Operand0;
		public IROperand Operand1;
		public IROperand RetOperand;

		public IRAndInstruction(IROperand retOperand, IROperand op0, IROperand op1)
		{
			Operand0 = op0;
			Operand1 = op1;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.And;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = and %{Operand0.Iden} , %{Operand1.Iden} : {RetOperand.Type}";
		}
	}

	public class IRTruncInstruction : IRInstruction
	{
		public IROperand Operand;
		public IROperand RetOperand;

		public IRTruncInstruction(IROperand retOperand, IROperand op)
		{
			Operand = op;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Trunc;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = trunc {Operand} to {RetOperand.Type}";
		}
	}

	public enum IRExtType : byte
	{
		Default,
		Zero,
		Signed
	}

	public class IRExtInstruction : IRInstruction
	{
		public IRExtType Ext;
		public IROperand Operand;
		public IROperand RetOperand;

		public IRExtInstruction(IROperand retOperand, IRExtType ext, IROperand op)
		{
			Ext = ext;
			Operand = op;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Ext;
		}

		public override string ToString()
		{
			string extTypeStr = "";
			switch (Ext)
			{
			case IRExtType.Zero:	extTypeStr = " z"; break;
			case IRExtType.Signed:	extTypeStr = " s"; break;
			}

			return $"%{RetOperand.Iden} = shift{extTypeStr} {Operand} to {RetOperand.Type}";
		}
	}

	public class IRFptoiInstruction : IRInstruction
	{
		public IROperand Operand;
		public IROperand RetOperand;

		public IRFptoiInstruction(IROperand retOperand, IROperand op)
		{
			Operand = op;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Fptoi;
		}
		
		public override string ToString()
		{
			return $"%{RetOperand.Iden} = fptoi {Operand} to {RetOperand.Type}";
		}
	}

	public class IRItofpInstruction : IRInstruction
	{
		public IROperand Operand;
		public IROperand RetOperand;

		public IRItofpInstruction(IROperand retOperand, IROperand op)
		{
			Operand = op;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Itofp;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = itofp {Operand} to {RetOperand.Type}";
		}
	}

	public class IRPtrtoiInstruction : IRInstruction
	{
		public IROperand Operand;
		public IROperand RetOperand;

		public IRPtrtoiInstruction(IROperand retOperand, IROperand op)
		{
			RetOperand = retOperand;
			Operand = op;
			InstructionType = IRInstructionType.Ptrtoi;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = ptrtoi {Operand} to {RetOperand.Type}";
		}
	}

	public class IRItoptrInstruction : IRInstruction
	{
		public IROperand Operand;
		public IROperand RetOperand;

		public IRItoptrInstruction(IROperand retOperand, IROperand op)
		{
			Operand = op;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Itoptr;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = itoptr {Operand} to {RetOperand.Type}";
		}
	}

	public class IRBitcastInstruction : IRInstruction
	{
		public IROperand Operand;
		public IROperand RetOperand;

		public IRBitcastInstruction(IROperand retOperand, IROperand op)
		{
			Operand = op;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Bitcast;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = bitcast {Operand} to {RetOperand.Type}";
		}
	}

	public class IRExtractValueInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand Aggregate;
		public int Index;

		public IRExtractValueInstruction(IROperand retOperand, IROperand aggregate, int index)
		{
			RetOperand = retOperand;
			Aggregate = aggregate;
			Index = index;
			InstructionType = IRInstructionType.ExtractValue;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = extract_value : {RetOperand.Type} , {Aggregate} , {Index}";
		}
	}

	public class IRInsertValueInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand Aggregate;
		public IROperand Src;
		public int Index;

		public IRInsertValueInstruction(IROperand retOp, IROperand aggregate, IROperand src, int index)
		{
			RetOperand = retOp;
			Aggregate = aggregate;
			Src = src;
			Index = index;
			InstructionType = IRInstructionType.InsertValue;
		}

		public override string ToString()
		{
			if (RetOperand != null)
				return $"%{RetOperand.Iden} = insert_value {Aggregate} , {Src} , {Index}";
			return $"insert_value {Aggregate} , {Src} , {Index}";
		}
	}

	public class IRUnionTagSetInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand UnionOperand;
		public IROperand TagOperand;

		public IRUnionTagSetInstruction(IROperand retOp, IROperand unionOp, IROperand tagOp)
		{
			RetOperand = retOp;
			UnionOperand = unionOp;
			TagOperand = tagOp;
			InstructionType = IRInstructionType.UnionTagSet;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = union_tag_set {UnionOperand} , {TagOperand}";
		}
	}

	public class IRUnionTagGetInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand UnionOperand;

		public IRUnionTagGetInstruction(IROperand retOp, IROperand unionOp)
		{
			RetOperand = retOp;
			UnionOperand = unionOp;
			InstructionType = IRInstructionType.UnionTagGet;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = union_tag_get {UnionOperand} : {RetOperand.Type}";
		}
	}

	public class IRUnionTagAddrInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand UnionOperand;

		public IRUnionTagAddrInstruction(IROperand retOp, IROperand unionOp)
		{
			RetOperand = retOp;
			UnionOperand = unionOp;
			InstructionType = IRInstructionType.UnionTagAddr;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = union_tag_addr {UnionOperand} : {RetOperand.Type}";
		}
	}

	public class IRUnionSetInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand UnionOperand;
		public IROperand ValueOperand;
		public int Index;
		public bool TagUnion;

		public IRUnionSetInstruction(IROperand retOp, IROperand unionOp, IROperand valOp, int index, bool tagUnion)
		{
			RetOperand = retOp;
			UnionOperand = unionOp;
			ValueOperand = valOp;
			Index = index;
			TagUnion = tagUnion;
			InstructionType = IRInstructionType.UnionSet;
		}

		public override string ToString()
		{
			string str = $"%{RetOperand.Iden} = union_set ";
			if (TagUnion)
				str += "tagged ";
			return str + $"{UnionOperand} , {ValueOperand} , {Index}";
		}
	}

	public class IRUnionGetInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand UnionOperand;
		public int Index;
		public bool TagUnion;

		public IRUnionGetInstruction(IROperand retOp, IROperand unionOp, int index, bool tagUnion)
		{
			RetOperand = retOp;
			UnionOperand = unionOp;
			Index = index;
			TagUnion = tagUnion;
			InstructionType = IRInstructionType.UnionGet;
		}

		public override string ToString()
		{
			string str = $"%{RetOperand.Iden} = union_get ";
			if (TagUnion)
				str += "tagged";
			return str + $"{UnionOperand} , {Index} : {RetOperand.Type}";
		}
	}

	public class IRUnionGepInstruction : IRInstruction
	{
		public IROperand RetOperand;
		public IROperand UnionOperand;
		public int Index;
		public bool TagUnion;

		public IRUnionGepInstruction(IROperand retOp, IROperand unionOp, int index, bool tagUnion)
		{
			RetOperand = retOp;
			UnionOperand = unionOp;
			Index = index;
			TagUnion = tagUnion;
			InstructionType = IRInstructionType.UnionGep;
		}

		public override string ToString()
		{
			string str = $"%{RetOperand.Iden} = union_gep ";
			if (TagUnion)
				str += "tagged";
			return str + $"{UnionOperand} , {Index} : {RetOperand.Type}";
		}
	}

	public class IRCallInstruction : IRInstruction
	{
		public string Identifier;
		public List<IROperand> Operands;
		public IRType RetType;
		public IROperand RetOperand;

		public IRCallInstruction(string identifier, List<IROperand> operands, IRType retType, IROperand retOperand)
		{
			Identifier = identifier;
			Operands = operands;
			RetType = retType;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Call;
		}

		public override string ToString()
		{
			string opsStr = null;
			if (Operands != null)
			{
				foreach (IROperand operand in Operands)
				{
					if (opsStr == null)
						opsStr = $"{operand}";
					else
						opsStr += $" , {operand}";
				}
			}

			if (RetOperand != null)
				return $"%{RetOperand.Iden} = call @{Identifier} ( {opsStr} ) : {RetType}";
			return $"call @{Identifier} ( {opsStr} )";
		}
	}

	public enum IRCmpType
	{
		Eq,
		Ne,
		Gt,
		Ge,
		Lt,
		Le,
		UEq,
		UNe,
		UGt,
		UGe,
		ULt,
		ULe,
		UNo
	}

	public class IRCmpInstruction : IRInstruction
	{
		public IRCmpType Cmp;
		public IROperand Operand0;
		public IROperand Operand1;
		public IROperand RetOperand;

		public IRCmpInstruction(IROperand retOperand, IRCmpType cmp, IROperand op0, IROperand op1)
		{
			Cmp = cmp;
			Operand0 = op0;
			Operand1 = op1;
			RetOperand = retOperand;
			InstructionType = IRInstructionType.Cmp;
		}

		public override string ToString()
		{
			string cmpTypeStr = Cmp.ToString().ToLower();
			
			return $"%{RetOperand.Iden} = cmp {cmpTypeStr} {Operand0} , {Operand1}";
		}
	}

	public class IRReturnInstruction : IRInstruction
	{
		public IROperand Operand;

		public IRReturnInstruction(IROperand operand = null)
		{
			Operand = operand;
			InstructionType = IRInstructionType.Return;
		}

		public override string ToString()
		{
			if (Operand != null)
				return $"ret {Operand}";
			return "ret";
		}
	}

	public class IRBranchInstruction : IRInstruction
	{
		public string Label;

		public IRBranchInstruction(string label)
		{
			Label = label;
			InstructionType = IRInstructionType.Branch;
		}

		public override string ToString()
		{
			return $"br {Label}";
		}
	}

	public class IRCondBranchInstruction : IRInstruction
	{
		public IROperand Condition;
		public string TrueLabel;
		public string FalseLabel;

		public IRCondBranchInstruction(IROperand condition, string trueLabel, string falseLabel)
		{
			Condition = condition;
			TrueLabel = trueLabel;
			FalseLabel = falseLabel;
			InstructionType = IRInstructionType.CondBranch;
		}

		public override string ToString()
		{
			return $"cond_br {Condition} , {TrueLabel} , {FalseLabel}";
		}
	}

	public class IRSwitchCase
	{
		public IROperand Value;
		public string Label;

		public IRSwitchCase(IROperand val, string label)
		{
			Value = val;
			Label = label;
		}
	}

	public class IRSwitchInstruction : IRInstruction
	{
		public IROperand Operand;
		public List<IRSwitchCase> Cases;
		public string DefaultLabel;

		public IRSwitchInstruction(IROperand operand, List<IRSwitchCase> cases, string defLabel)
		{
			Operand = operand;
			Cases = cases;
			DefaultLabel = defLabel;
			InstructionType = IRInstructionType.Switch;
		}

		public override string ToString()
		{
			string str = $"switch {Operand}";
			if (Cases != null)
			{
				foreach (IRSwitchCase switchCase in Cases)
				{
					str += $" , case {switchCase.Value} : {switchCase.Label}";
				}
			}
			str += $" , default : {DefaultLabel}";
			return str;
		}
	}
}
