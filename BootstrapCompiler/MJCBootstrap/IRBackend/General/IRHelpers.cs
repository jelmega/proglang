﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;

namespace MJC.IRBackend.General
{
	partial class IRHelpers
	{

		public static IRCmpType GetCmpType(string str)
		{
			switch (str)
			{
			default:
			case "eq":		return IRCmpType.Eq;
			case "ne":		return IRCmpType.Ne;
			case "gt":		return IRCmpType.Gt;
			case "ge":		return IRCmpType.Ge;
			case "lt":		return IRCmpType.Lt;
			case "le":		return IRCmpType.Le;
			case "ueq":		return IRCmpType.UEq;
			case "une":		return IRCmpType.UNe;
			case "ugt":		return IRCmpType.UGt;
			case "uge":		return IRCmpType.UGe;
			case "ult":		return IRCmpType.ULt;
			case "ule":		return IRCmpType.ULe;
			case "uno":		return IRCmpType.UNo;
			}
		}

		public static string GetString(IRCmpType type)
		{
			switch (type)
			{
			default:			return "unknown";
			case IRCmpType.Eq:	return "eq";	
			case IRCmpType.Ne:	return "ne";	
			case IRCmpType.Gt:	return "gt";	
			case IRCmpType.Ge:	return "ge";	
			case IRCmpType.Lt:	return "lt";	
			case IRCmpType.Le:	return "le";	
			case IRCmpType.UEq:	return "ueq";	
			case IRCmpType.UNe:	return "une";	
			case IRCmpType.UGt:	return "ugt";	
			case IRCmpType.UGe:	return "uge";	
			case IRCmpType.ULt:	return "ult";	
			case IRCmpType.ULe:	return "ule";	
			case IRCmpType.UNo:	return "uno";	
			}
		}


		public static TypeInfo GetTypeInfo(IRType type, CompilerContext context)
		{
			if (type.Address ||
			    type.PtrCount > 0)
				return context.GetTypeInfo("P");

			switch (type.BaseType)
			{
			case IRBaseType.I1: return context.GetTypeInfo("b");
			case IRBaseType.I8: return context.GetTypeInfo("g");
			case IRBaseType.U8: return context.GetTypeInfo("h");
			case IRBaseType.I16: return context.GetTypeInfo("s");
			case IRBaseType.U16: return context.GetTypeInfo("t");
			case IRBaseType.I32: return context.GetTypeInfo("i");
			case IRBaseType.U32: return context.GetTypeInfo("j");
			case IRBaseType.I64: return context.GetTypeInfo("l");
			case IRBaseType.U64: return context.GetTypeInfo("m");
			case IRBaseType.F32: return context.GetTypeInfo("f");
			case IRBaseType.F64: return context.GetTypeInfo("d");
			case IRBaseType.Void: return context.GetTypeInfo("v");
			case IRBaseType.Custom:
			{
				string iden = type.CustomIden;
				if (iden.StartsWith("_M"))
				{
					char c = iden.Last();
					iden = c + iden.Substring(2, iden.Length - 4);
				}
				return context.GetTypeInfo(iden);
			}
			case IRBaseType.Array:
			{
				TypeInfo tmp = GetTypeInfo(type.SubType, context);
				return new TypeInfo(tmp.Alignment, tmp.Size * type.ArraySize);
			}
			default:
				return null;
			}
		}

	}
}
