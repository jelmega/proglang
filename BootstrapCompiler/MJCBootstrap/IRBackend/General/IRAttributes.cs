﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.IRBackend.General
{
	public enum IRLinkage
	{
		Export,
		Import,
		Public,
		PublicExternal,
		Hidden,
		HiddenExternal,
		Shared,
		Private,
	}

	public enum IRCallConv
	{
		None,
		MJay,
		C,
		Windows
	}

	[Flags]
	public enum IRGlobalFlags
	{
		None = 0b0000_0000,
		Const = 0b0000_0001,
	}

	public class IRAttributes
	{
		public IRLinkage Linkage;

		public int Alignment;

		public IRCallConv CallConv;

		public IRGlobalFlags GlobalFlags;

		public override string ToString()
		{
			string str = "";

			str += IRHelpers.LinkageToString(Linkage);

			/*string inlineStr = IRHelpers.InlineToString(Inline);
			if (inlineStr != "")
				str += $" {inlineStr}";*/

			if (Alignment != 0)
				str += $" align({Alignment})";

			if (CallConv != IRCallConv.None)
				str += $" call_conv({CallConv.ToString().ToLower()})";

			return str;
		}
	}

	public static partial class IRHelpers
	{
		public static string LinkageToString(IRLinkage linkage)
		{
			switch (linkage)
			{
			case IRLinkage.Export:			return "export";
			case IRLinkage.Import:			return "import";
			case IRLinkage.Public:			return "public";
			case IRLinkage.PublicExternal:	return "public_external";
			case IRLinkage.Hidden:			return "hidden";
			case IRLinkage.HiddenExternal:	return "hidden_external";
			case IRLinkage.Shared:			return "shared";
			case IRLinkage.Private:			return "private";
			default:						return "";
			}
		}
	}
}
