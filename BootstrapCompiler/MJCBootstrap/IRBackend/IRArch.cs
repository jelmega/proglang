﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.ILBackend;

namespace MJC.IRBackend
{
	public enum ArchType
	{
		Unknown,
		X86,
		X64
	}

	public class Arch
	{
		public static Dictionary<string, ArchType> TypeMapping = new Dictionary<string, ArchType>()
		{
			{ "x64", ArchType.X64 },
			{ "x86", ArchType.X86 },
		};

		public static Dictionary<ArchType, Func<IArchBackend>> Backends = new Dictionary<ArchType, Func<IArchBackend>>
		{
			{ ArchType.Unknown, () => { return null; }},
			{ ArchType.X86, () => { return new X86.ArchBackend(); }},
			{ ArchType.X64, () => { return new X64.ArchBackend(); }},
		};
	}

	public interface IArchBackend
	{
		/// <summary>
		/// Architecture specific optimizations and conversions
		/// </summary>
		/// <param name="module"></param>
		void Process(IRModule module);

		/// <summary>
		/// Translate IL to the textual representation of platform-specific Assembly
		/// </summary>
		IAsmUnit Translate(IRModule module);

		/// <summary>
		/// Assembles the textual representation to its 'binary' representation
		/// </summary>
		void Assemble(IAsmUnit unit);
	}

	public interface IAsmUnit
	{
		int InstructionCount { get; }

		void OutputFile(string filepath);

		AsmInstruction GetInstruction(int index);
	}

	public struct AsmInstruction
	{
		public AsmInstruction(string text = "")
		{
			Text = text;
			Bytes = null;
		}

		public byte[] Bytes;
		public string Text;

		public override string ToString()
		{
			string tmp = Text;
			if (Bytes != null)
			{
				tmp += ';' + BitConverter.ToString(Bytes).Replace('-', ' ');
			}
			return tmp;
		}
	}
}
