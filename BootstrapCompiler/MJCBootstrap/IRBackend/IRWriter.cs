﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.General;
using MJC.IRBackend.General;

namespace MJC.IRBackend
{
	public static class IRWriter
	{
		public static void Write(IRModule module, string path)
		{
			string actPath = CmdLine.IntermediateDirectory + Helpers.GetModuleFilePath(module.Name);
			actPath += Path.GetFileName(path);

			Helpers.EnsureDirectoryExists(Path.GetDirectoryName(actPath));
			using (StreamWriter writer = new StreamWriter(actPath))
			{
				WriteTypeDefs(module.TypeDefs, writer);
				WriteGlobals(module.Globals, writer);

				foreach (IRFunction function in module.Functions)
				{
					WriteFunction(function, writer);
					writer.WriteLine();
				}

				WriteAliases(module.Aliases, writer);
			}
		}

		private static void WriteFunction(IRFunction function, StreamWriter writer)
		{
			string paramStr = null;
			if (function.Params != null)
			{
				foreach (IROperand param in function.Params)
				{
					if (paramStr == null)
						paramStr = param.ToString();
					else
						paramStr += $", {param}";
				}
			}

			string retStr = "";
			if (function.RetType != null)
			{
				retStr = $" : {function.RetType}";
			}

			writer.Write($"def {function.Attribs} @{function.Identifier} ({paramStr}) {retStr}");

			if (function.Blocks.Count > 0)
			{
				writer.WriteLine(" {");

				foreach (IRBasicBlock block in function.Blocks)
				{
					writer.WriteLine($"{block.Label}:");
					foreach (IRInstruction instruction in block.Instructions)
					{
						writer.WriteLine($"\t{instruction}");
					}
				}

				writer.WriteLine("}");
			}
		}

		private static void WriteTypeDefs(List<IRTypeDef> typedefs, StreamWriter writer)
		{
			foreach (IRTypeDef typeDef in typedefs)
			{
				string subTypesStr = null;
				foreach (IRType subType in typeDef.SubTypes)
				{
					if (subTypesStr == null)
						subTypesStr = $"{subType}";
					else
						subTypesStr += $" , {subType}";
				}

				if (typeDef.Kind == IRTypeDefKind.TagUnion)
					writer.WriteLine($"?{typeDef.Iden} = tag_union {{ {typeDef.TagType} , {subTypesStr} }}");
				else
					writer.WriteLine($"?{typeDef.Iden} = {typeDef.Kind.ToString().ToLower()} {{ {subTypesStr} }}");
			}
		}

		private static void WriteGlobals(List<IRGlobal> globals, StreamWriter writer)
		{
			foreach (IRGlobal global in globals)
			{
				if (global.HasValue)
				{
					string globalValue = (global.StringValue != null ? $"c\"{global.StringValue}\" " : $"{global.NumValue} ");

					writer.WriteLine($"global {global.Attribs} @{global.Identifier} : {global.Type} =  {globalValue}");
				}
				else
				{
					writer.WriteLine($"global {global.Attribs} @{global.Identifier} : {global.Type}");
				}
			}
		}

		private static void WriteAliases(List<string> aliases, StreamWriter writer)
		{
			writer.WriteLine();
			foreach (string alias in aliases)
			{
				writer.WriteLine("typealias " + alias);
			}
		}

	}
}
