﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.IRBackend
{
	public static class Platform
	{
		public static PlatformInfo Info = new PlatformInfo();

		public static void InitPlatform()
		{
			if (CmdLine.Architecture != null)
			{
				Info.ArchitectureType = Arch.TypeMapping[CmdLine.Architecture];
			}
			else
			{
				ErrorSystem.Fatal("No architecture supplied, bootstrap require explicit architecture definition");
			}
		}
	}

	public struct PlatformInfo
	{
		public ArchType ArchitectureType;
		public SIMD SimdInstructionSets;
	}

	public enum SIMD : uint
	{
		MMX		= 0x0000_0000,
		SSE		= 0x0000_0001,
		SSE2	= 0x0000_0002,
		SSE3	= 0x0000_0004,
		SSSE3	= 0x0000_0008,
		SSE41	= 0x0000_0010,
		SSE42	= 0x0000_0020,

		AVX		= 0x0000_0100,
		AVX2	= 0x0000_0200,
		AVX512	= 0x0000_0400,

		FMA3	= 0x0000_1000,
		FMA4	= 0x0000_2000,

		NEON	= 0x0001_0000,
	}
}
