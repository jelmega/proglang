﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.IRBackend
{
	public class IRModule
	{
		public string Name;

		public List<IRFunction> Functions = new List<IRFunction>();
		public List<IRTypeDef> TypeDefs = new List<IRTypeDef>();
		public List<IRGlobal> Globals = new List<IRGlobal>();
		public List<string> Aliases = new List<string>();

		public IRModule(string name)
		{
			Name = name;
		}
	}
}
