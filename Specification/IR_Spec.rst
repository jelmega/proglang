::

	 ___      ___       ___      __       ___  ___  
	|"  \    /"  |     |"  |    /""\     |"  \/"  | 
	 \   \  //   |     ||  |   /    \     \   \  /  
	 /\\  \/.    |     |:  |  /' /\  \     \\  \/   
	|: \.        |  ___|  /  //  __'  \    /   /    
	|.  \    /:  | /  :|_/ )/   /  \\  \  /   /     
	|___|\__/|___|(_______/(___/    \___)|___/      

==============================================
IR (Intermediate Representation) Specification
==============================================

.. contents::

Introduction
------------

MJay IR is the intermediate representation created by the compiler, it is a very low level representations which is entirely processor agnostic. The IR is used for low level and processor specific optimizations. The IR can be converted to assembled code or to other intermediate representations.

MJay IR can also be stored as a binary format, making these files a lot more compact, but these may be a lot harder to read.

Functions
---------
::

	func-decl ::= "def" [<func-attribs>] <func-iden> "(" { <operand> [","] } ")" ":" <type>


Globals
-------
::

	global ::= "global" "@" <identifier> "=" ( "global" | "constant" ) [<constant>] ":" <type>


Types
-----
::

	type ::= <builtin-type>
		   | <aggregate-type>
		   | <pointer-type>
		   | <array-type>

builtin types
`````````````
::

	i1
	i8     u8
	i16    u16
	i32    u32
	i64    u64

Aggregate type
``````````````
::

	aggregate-type ::= ("struct" | "union" | "tag_union") "{" { <type> [","] } "}" 

Pointer type
````````````
::

	pointer-type ::= "*" <type>

Array type
``````````
::

	array-type ::= "[" <number> "x" <type> "]"


Type definition
```````````````
::

	typedef ::= "?" <identifier> "=" "type" "{" { <type> [","] } "}"

Constants
---------

Simple constants
````````````````
::

	simple-const ::= <bool-const>
				   | <int-const>
				   | <float-const>

	bool-const ::= ( "true" | "false" ) ":" <type>
	int-const ::= <number> ":" <type>
	float-const ::= <hex-number> ":" <type>

Array constants
```````````````
::

	array-const ::= [ { <simple-const> [","] } ]
	char-arr-const ::= 'c"' <text> '"'

Instruction set
---------------

Unary instructions
``````````````````

neg
^^^
::

	neg-instruction ::= "neg" <operand>

Get the negation (2s complement) of the value, i.e. -val

compl
^^^^^
::

	compl-instruction ::= "compl" <operand>

Get the negation (2s complement) of the value

inc
^^^
::

	inc-instruction ::= "inc" <operand>

Increment the value by 1

dec
^^^
::

	dec-instruction ::= "dec" <operand>

Decrement the value by 1

Binary instructions
```````````````````

add
^^^
::

	add-instruction ::= "add" <value> "," <value> ":" <type>

Calculate the addition of 2 values

sub
^^^
::

	sub-instruction ::= "sub" <value> "," <value> ":" <type>

Calculate the subtraction of 2 values

mul
^^^
::

	mul-instruction ::= "mul" <value> "," <value> ":" <type>

Calculate the multiplication of 2 values

div
^^^
::

	div-instruction ::= "div" <value> "," <value> ":" <type>

Calculate the division of 2 values

rem
^^^
::

	rem-instruction ::= "rem" <value> "," <value> ":" <type>

Calculate the remainder of 2 values

shift
^^^^^
::

	shift-instruction ::= "shift" ("l"|"lr"|"ar") <value> "," <value> : <type>

Calculate the binary shift of a value with `n` bits. The type of shift depends on the first parameter of the instructions, these can be `l`, `lr` or `ar`.

`l` does a logical left shift

`lr` does a logical right shift

`ar` does a arithmatic right shift

or
^^
::

	rem-instruction ::= "or" <value> "," <value> ":" <type>

Calculate the binary or of 2 values

xor
^^^
::

	rem-instruction ::= "xor" <value> "," <value> ":" <type>

Calculate the binary xor of 2 values

and
^^^
::

	and-instruction ::= "and" <value> "," <value> ":" <type>

Calculate the binary and of 2 values

Convertion instructions
```````````````````````

trunc
^^^^^
::

	trunc-instruction ::= "trunc" <operand> "to" <type>

Truncate a value to a type with a smaller bit count

ext
^^^
::

	ext-instruction ::= "ext" [ "s" | "z" ] <operand> "to" <type>

Extend a value to a type with a larger bit count. Exactly how the value is extended can decided by the first operand, which is either `s` or `z`. The first value is optional and will by default match the type being extended, i.e. signed extension for signed types and zero extension for unsigned types.

`s` means that the extension is a sign extension. This will preserve the sign of the value, by copying the original sign bit until the desired bit count is reached.

`z` meant that the extension is a zero extension, This will not preserve the sign of the value, since 0 will always be prepended when extending the value.

fptoi
^^^^^
::

	fptoi-instruction ::= "fptoi" <operand> "to" <type>

Convert a floating-point valiue to an integer value

itofp
^^^^^
::

	itofp-instruction ::= "itofp" <operand> "to" <type>

Convert an integer value to a floating-point number


ptrtoi
^^^^^
::

	ptrtoi-instruction ::= "ptrtoi" <operand> "to" <type>

Convert a pointer to an integer value, truncate or extend when neccesary.

itoptr
^^^^^
::

	itoptr-instruction ::= "itoptr" <operand> "to" <type>

Convert an integer value to a pointer, truncate or extend when neccesary.

bitcast
^^^^^^^
::

	bitcast-instruction ::= "bitcast" <operand> "to" <type>

Cast the bits of a value to another type with the same bit length

Memory instructions
```````````````````

alloca
^^^^^^
::

	alloca-instruction ::= "alloca" <type>

Allocate a variable on the stack

store
^^^^^
::

	store-instruction ::= "store" <operand> "," <operand>

Store a value to memory

load
^^^^
::

	load-instruction ::= load <value> ":" <type>

Load a value from memory

gep
^^^
::

	gep-instruction ::= "gep" ":" <type> "," <operand> "," { <index> [","] }+

`gep` or `get element ptr`

Get the memory address of a value in an aggregate type, depending on the indices supplied

Aggregate instructions
``````````````````````

extract_value
^^^^^^^^^^^^^
::

	extract-value-instruction ::= "extract_value" ":" <type> "," <operand> "," { <index> [","] }+

Extract value in the memory address of a member of an aggregate. `extract_value` is a simplified version of `gep`, which treats the memory address provided as a pointer to the aggregate, instead of interpreting it as an array, and followed up by a `load`. So, unlike `gep`, the `extract_value` instruction return the value stored in the member, instead of the memory address of that member.

insert_value
^^^^^^^^^^^^^
::

	insert-value-instruction ::= "insert_value" <operand> "," <operand> "," { <index> [","] }+

Insert a value into the memory adress of a member of an aggregate. `insert_value` is a simplified version of `gep`, which treats the memory address provided as a pointer to the aggregate, instead of interpreting it as an array, and followed up by a `store`. So, unlike `gep`, the `insert_value` instruction stores the value in the member, instead of having to get the memory address of that member and manually store the value.

union_tag_set
^^^^^^^^^^^^^
::

	union-tag-set-instruction ::= "union_tag_set" <operand> "," <operand>

Sets the tag of a tagged union.
When the first operand points to a location in memory, the tag will be inserted in memory and no value will be returned, otherwise the instruction will return a new version of the first operand, with the tag inserted.

union_tag_get
^^^^^^^^^^^^^
::

	union-tag-get-instruction ::= "union_tag_get" <operand> ":" <type>

Gets the tag of a tagged union.

union_set
^^^^^^^^^
::

	union-set-instruction ::= "union_set" ["tagged"] <operand> "," <operand> "," <index>

Sets data in an union, the value will get inserted into the element at the index that is given.
When the first operand points to a location in memory, the data will be inserted in memory and no value will be returned, otherwise the instruction will return a new version of the first operand, with the data inserted.
Tagged needs to be set when a tagged union is used.

union_get
^^^^^^^^^
::

	union-get-instruction ::= "union_set" <operand> "," <index> ":" <type>

Gets data form a union.
Tagged needs to be set when a tagged union is used.

union_gep
^^^^^^^^^
::

	union-gep-instruction ::= "union_gep" ":" <type> "," <operand> "," <index>

`gep` or `get element ptr`

Get the memory address of a value in an union type, depending on the index supplied

union_tag_addr
^^^^^^^^^^^^^^
::

	union-gep-instruction ::= "union_gep" ":" <type> "," <operand> "," <index>

`gep` or `get element ptr`

Get the memory address of a value in an union type, depending on the index supplied


Other instructions
``````````````````

call
^^^^
::

	call-instruction ::= "call" <func-iden> "(" { <operand> [","] } ")" [ ":" <type> ]

Call a function with certain parameters and possibly, a return type

cmp
^^^
::

	cmp-instruction ::= "cmp" <comparison-type> <value> , <value> : <type>

	comparison-type ::= "eq" |
						"ne" |
						"gt" |
						"gte" |
						"lt" |
						"lte" |
						"ueq" |
						"une" |
						"ugt" |
						"ugte" |
						"ult" |
						"ulte" |
						"uno"

Compare 2 values. The type of shift depends on the first parameter of the instructions.

For integer comparisons, only comparison types without a `u` prefix are valid.

For floating point types, any comparison type without the `u` prefix are ordered comparisons, this means that either value is `NaN`, false will be returned.

When the comparison type starts with a `u` prefix, the comparison is an unordered comparison, this means that if either value is `NaN` true will be returned.

`uno` is a special comparison type and only returns true if one of the operands is `Nan`

Terminal instructions
`````````````````````

ret
^^^
::

	ret-instruction ::= ret [<operand>]

Return from a function, with an optional return value

br
^^
::

	br-instruction :: br <identifier>

Unconditionally branch into a basic block

cond_br
^^^^^^^
::

	cond-br-instruction ::= cond_br <operand> "," <identifier> "," <identifier>

Conditionally branch to an other instructions, if the operand represents true, branch to the first label, otherwise branch to the second label

switch
^^^^^^
::

	switch-instruction ::= switch <operand> { "," "case" <operand> ":" <identifier> } ["," "default" ":" <identifier>]

Branch depending on the value given, when a case with the given value exists in the jumo table, goto it's label, otherwise to the default label