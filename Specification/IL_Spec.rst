::

     ___      ___       ___      __       ___  ___  
    |"  \    /"  |     |"  |    /""\     |"  \/"  | 
     \   \  //   |     ||  |   /    \     \   \  /  
     /\\  \/.    |     |:  |  /' /\  \     \\  \/   
    |: \.        |  ___|  /  //  __'  \    /   /    
    |.  \    /:  | /  :|_/ )/   /  \\  \  /   /     
    |___|\__/|___|(_______/(___/    \___)|___/      

========================================
IL (Intermediate Language) Specification
========================================

.. contents::

Introduction
------------

MJay IL is an intermediate language, wich sits between the AST created from the source code, and the IR code used in the backend. The purpose of the IL, is to better allow language specific optimizations, which would be harder to process when operating on the AST. The IL is also an SSA (Single Static Assignment) language, unlike regular MJay code.

Stage
-----
::

    stage ::= "raw"
            | "canonical"
            | "comp_exec"
            | "opt"

Each IL module has it's stage defined, this stages informs the compiler what has already been processed, MJIL has 3 stages:

1. Raw. The raw stage is what is generated when transposing the syntax tree into MJIL. This means that this stage has incomplete blocks, intermediate instruction and possible data flow errors.
2. Canonical. The canonical stage contains valid MJIL, without errors, that could be converted into MJIR. All blocks are correctly terminated, all intermediate instructions are processed and data flow errors are eliminated.
3. Comp exec. The compile time execution stage is similar to the canonical representation, with the exception that all compile time execution is processed.
4. Opt. The opt stage contains MJIL, with the same meaning as the canonical stage, but in an optimized form.

Linkage
-------

The linkage controls how two options in different MJIL modules will be linked, i.e. are treated as the same object.

There are 8 linkage types:

- export: Similar to `public`, but this symbol will also be available in the generated dynamic library
- import: Similar to `public_external`, but this symbol can link to a symbol in an external dynamic library
- public: Symbols are available in any module that links with the current module
- public_external: Symbols are defined in another module that is linked with this module
- hidden: Symbols are available in this MJay module that links with the current module
- hidden_external: Symbols are defined in another MJay module that is linked with this module
- shared: Symbols may be defined in multiple modules
- private: Symbols only availble in this MJIL module

Calling convention
------------------
::

    call-conv ::= "call_conv" "(" <identifier> ")"

The calling convention controls how the function will be called in the binary

The following conventions are valid:

- mjay: Default calling convention
- c: C calling convention
- windows: Win32 calling convention

Types
-----
::

    type ::= "$" <sub-type>
    sub-type ::= ["^"] ( <identifier> | <tuple-type> ) { "*" }

A type defines the properties of a value.

If a type starts with `^`, it means that the type is located on the stack

Builtin types
`````````````

MJIL has some builtin types which do not need to be defined to be able to be used::

    i8  u8
    i16 u16
    i32 u32
    i64 u64
    f32
    f64
    bool

Tuple types
```````````
::

    tuple-type ::= "(" { <sub-type> [","] } ")"

Aggregate types
```````````````
Aggregate types represent a collection of data in memory

Struct type
^^^^^^^^^^^
::

    struct-decl ::= ".struct" <identifier> [ "<" <template-params> ">"] "{" <var-decl>* "}"

    struct-type ::= <identifier>
    
A structure represents a group of values, linearly in memory

Typedefs
````````
::

    typedef ::= ".typedef" <type> "as" <type>

A typedef allows the use of a strongly typed alias which represents another type, but which is incompattible with it

Function types
``````````````
::

    func-type ::= "$" [ "(" <sub-type> ")" ] <tuple-type> "->" ( ( "(" <sub-type> ")" ) | <tuple-type> )

Templates
---------
::

    template-params ::= { <template-param> [ "," ] }
    template-param  ::= <identifier> [ ":" <type> ]

Templates allow the IL backend to create specific code for the instances of a type

Template instance
^^^^^^^^^^^^^^^^^
::

    template-type-instance ::= ".template_type_instance" <type> "<" { <template-arg> [","] } ">"
    template-func-instance ::= ".template_func_instance" <identifier> <type> "<" { <template-arg> [","] } ">"
    template-arg      ::= <identifier> | <literal>

A template instance notifies the compiler to generate a new instance of a template type, if no instance exists already.

Functions
---------
::

    function ::= ".func" [<attributes>] <identifier> "{" <basic-block>* "}"

Methods
-------
::

    method   ::= ".method" [<attributes>] <identifier> "{" <basic-block>* "}"

Operators
---------
::

    operator ::= ".operator" [<attributes>] <identifier> "{" <basic-block>* "}"


Operator table
--------------
::

    op-table ::= .op_table "{" <op-table-member>* "}"
    op-table-member ::= <identifier> <type> : <identifier>

The operator table is used to map operators to their corresponding IL functions

Aggregates
----------
::

    struct ::= ".struct" [<attributes>] <identifier> "{" <aggregate-var>* "}"
    union ::= ".union" [<attributes>] <identifier> "{" <aggregate-var>* "}"

    aggregate-var ::= <identifier> : <type>

    default-initializer ::= <identifier>

An aggregate is a collection of variables and methods

V-Table
-------
::

    v-table ::= ".vtable" <identifier> {"<" <template-parameter> [","] ">"} "{" <vtable-method>* "}"
    v-table-method ::= <identifier> <type> : <identifier>

The V-table or `virtual table` is used to map methods to their corresponding IL functions

Enumerations
------------
::

    enum ::= ".enum" [<attributes>] <identifier> : <type> "{" <enum-var>* "}"

    enum-var ::= <identifier> "," <default-initializer> [ ":" <type> "," <default-initializer>]
    
An enum member can represent a single value representation when the type is excluded, when the type is included, the member represents an ADT enum member.

Reference
---------
::

    reference ::= "#" <identifier> "." <identifier>

A reference points to a member of an object.

This can be used to point to enum members, aggregate variables and vtable functions.

Constants
---------
::

    constant ::= ".constant" <identifier> ":" <type> "," <initializer>

Typedef
-------
::

    typedef ::= ".typedef" <identifier> : <type>

Typedefs are special, while they are not an actual type, they are still distinct used as if they were their own separate type

Type alias
----------
::

    constant ::= ".typealias" <identifier>

While typealiases can be defined, unless the code will be converted a module, they will be removed when creating the executable.

No type is explicitly defined, since this is included in the mangled name and no access to the type is required.

Instruction Set
---------------

Literal instructions
````````````````````

int_literal
^^^^^^^^^^^
::

    int-literal-instruction ::= "int_literal" <lit> ":" <type>

Defines an integer literal

float_literal
^^^^^^^^^^^
::

    float-literal-instruction ::= "float_literal" <hex-lit> ":" <type>

Defines an floatint point literal

string_literal
^^^^^^^^^^^^^
::

    string-literal-instruction ::= "string_literal" <string-lit> ":" <type>

Defines an integer literal

Memory instructions
```````````````````

alloc_stack
^^^^^^^^^^^
::

    alloc-stack-instruction ::= "alloc_stack" <type>

Allocates uninitialized memory, with a sufficient allignement, on the stack capable of storing a value of `<type>`

dealloc_stack
^^^^^^^^^^^^^
::

    dealloc-stack-instruction ::= "dealloc_stack" <operand>

Deallocated a value on the stack, this instruction also decides in a `destruct` instruction needs to be called depending on the value's type


assign
^^^^^^
::

    assign-instruction ::= "assign" <value> "to" <operand>

An intermediate instruction, which is only valid in the `raw` stage. This instruction gets converted to the correct set of instructions, depending on the type of the operand.
The replacement happens during canonicalization.

store
^^^^^
::

    store-instruction ::= "store" <value> "in" <operand>

Stores a value into a memory location.

load
^^^^
::

    load-instruction ::= "load" <operand>

Loads a value from memory.

Function instructions
`````````````````````

function_ref
^^^^^^^^^^^^
::

    function-ref-instruction ::= "function_ref" [<call-conv>] <func-iden> ":" <type>

Get a reference to a function

method_ref
^^^^^^^^^^
::

    method-ref-instruction ::= "method_ref" [<call-conv>] <reference> ":" <type>

operator_ref
^^^^^^^^^^^^
::

    operator-ref-instruction ::= "operator_ref" <op-loc> <op> ":" <type>

    op-loc ::= "prefix" | "infix" | "postfix"

Get a reference to an operator

call
^^^^
::

    call-instruction ::= "call" <value> "(" { <operand> [","] } ")" [ ":" <type> ]

Call a function with the given operands

builtin
^^^^^^^
::

    builtin-instruction ::= "builtin" <string-lit> "(" { <operand> [","] } ")" ":" <type>

Call a builtin IR instruction

compiler_intrin
^^^^^^^^^^^^^^^
::

    compiler-intrin-instruction ::= "compiler_intrin" <sring-literal> <type> ":" <type>

Call a compiler intrinsic

The following intrinsics are valid:
- sizeof: Get the size of the operand or type
- alignof: Get the alignment of the operand or type

Tuple instructions
``````````````````

tuple_pack
^^^^^^^^^^^^
::

    tuple-pack-instruction ::= "tuple_pack" "(" <operand-list> ")"

Pack multiple values into 1 singular value

tuple_unpack
^^^^^^^^^^^^
::

    tuple-unpack-instruction ::= "tuple_unpack" <operand>

Unpack the values in a tuple to multiple distinct values

tuple_insert
^^^^^^^^^^^^
::

    tuple-insert-instruction ::= "tuple_insert" <operand> "," <lit> "," <operand>

Insert a value into a tuple. The tuple is indexed using the element index.

tuple_extract
^^^^^^^^^^^^
::

    tuple-extract-instruction ::= "tuple_extract" <operand> "," <lit> ":" <type>

Insert a value into a tuple. The tuple is indexed using the element index.

tuple_elem_addr
^^^^^^^^^^^^^^^
::

    tuple-elem-addr-instruction ::= "tuple_elem_addr" <operand> "," <lit>

Struct instructions
```````````````````

struct_extract
^^^^^^^^^^^^^^
::

    struct-extract-instruction ::= "struct_extract" <operand> "," <reference> ":" <type>

Extract a value of a member in a structure. This can be used on memory locations or values.

struct_insert
^^^^^^^^^^^^^^
::

    struct-insert-instruction ::= "struct_insert" <operand> "," <reference> "," <operand>

Insert a value into a member of a structure. This can be used on memory locations or values.

struct_elem_addr
^^^^^^^^^^^^^^^^
::

    struct-elem-addr-instruction ::= "struct_elem_addr" <operand> "," <reference> : <type>

Extract the memory address of a value from a struct in memory.

Union instructions
``````````````````

union_extract
^^^^^^^^^^^^^^
::

    union-extract-instruction ::= "union_extract" <operand> "," <reference> ":" <type>

Extract a value of a member in an union. This can be used on memory locations or values.

union_insert
^^^^^^^^^^^^^^
::

    union-insert-instruction ::= "union_insert" <operand> "," <reference> "," <operand>

Insert a value into a member of an union. This can be used on memory locations or values.

union_elem_addr
^^^^^^^^^^^^^^^^
::

    union-elem-addr-instruction ::= "union_elem_addr" <operand> "," <reference> : <type>

Extract the memory address of a value from an union in memory.

Enumeration instructions
````````````````````````

enum_value
^^^^^^^^^^
::

    enum-value ::= "enum_value" <type> "," <reference> { "," <operand> }

Creates an enum value, which is not stored on memory, of a certain type. The enum value is given as a reference to an enum member. If the enum member can contain data, then the data is given after the member reference.

enum_get_value
^^^^^^^^^^^^^^
::

    enum-get-value ::= "enum_get_value" <operand>

Get the enum value. The type of the returned value is the value type of the enum.

enum_get_member_value
^^^^^^^^^^^^^^^^^^^^^
::

    enum-get-member-value ::= "enum_get_member_value" <type> "," <reference>

Get the value of an enum member. The type of the returned value is the value type of the enum.

enum_addr_value
enum_addr_data

Global var instructions
```````````````````````

global_addr
^^^^^^^^^^^
::

    global-addr ::= "global_addr" <operand>

Get the address of a global variable

global_value
^^^^^^^^^^^^
::

    global-value ::= "global_value" <operand>

Get the value of a global variable

Template instructions
`````````````````````

template_get_value
^^^^^^^^^^^^^^^^^^
::

    template-get-value ::= "template_get_value" <type> , <reference> : <type>

Get a value of a template value argument

Other instructions
``````````````````

convert_type
^^^^^^^^^^^^
::

    convert-type-instruction ::= "convert_type" <operand> "to" <type>

Cast an operand to another type. Not that this instruction only exist at the intermediate level and will not be converted to an instruction in the binary. This instruction solely exists to notify the IL that the value will be used as a value of another type.

get_vtable
^^^^^^^^^^
::

    get-vtable-instruction ::= "gen_vtable" <type> : <type> { "," <type> }

Get the vtable for a specific type, that will be used by an interface or compound interface

Terminators
```````````

return
^^^^^^
::

    return-instruction ::= "return" [<operand>]

Return from an instruction, if no operand is provided, `return` will just exit the current value, otherwise, the value will be given to the calling instruction

branch
^^^^^^
::

    branch-instruction ::= "branch" <identifier>

Unconditionally branch into a basic block

cond_branch
^^^^^^^^^^^
::

    cond-branch-instruction ::= "cond_branch" <operand> "," <identifier> "," <identifier>

Conditionally branch to another basic block, if the operand represents true, branch to the first label, otherwise branch to the second label

switch_enum
```````````
::

    switch-enum-instruction ::= "switch_enum" <operand> "," { "case" <reference> ":" <identifier> [","] } [ "," "default" ":" <identifier> ]

Switch to a another basic block depending on the enum value passed, if no case matches the value of the enum, goto the default basic block

switch_value
```````````
::

    switch-enum-instruction ::= "switch_enum" <operand> "," { "case" <operand> ":" <identifier> [","] } [ "," "default" ":" <identifier> ]

Switch to a another basic block depending on a non-enum value passed, if no case matches the value, goto the default basic block