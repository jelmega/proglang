::

     ___      ___       ___      __       ___  ___  
    |"  \    /"  |     |"  |    /""\     |"  \/"  | 
     \   \  //   |     ||  |   /    \     \   \  /  
     /\\  \/.    |     |:  |  /' /\  \     \\  \/   
    |: \.        |  ___|  /  //  __'  \    /   /    
    |.  \    /:  | /  :|_/ )/   /  \\  \  /   /     
    |___|\__/|___|(_______/(___/    \___)|___/      

=================
ABI Specification
=================

Introduction
------------
This document defines the specification for the MJay ABI.

ABI
---

This section defines the entire ABI used by MJay

::

	<mangled-name> ::= "_M" <qualified-name>
	<qualified-name> ::= <qualified-identifier> <qualified-type>

A mangled name consists out of 2 core parts, the identifier to the symbol, and the type of that symbol.

Qualified identifier
--------------------
::

	qualified-identifier ::=  { <lname> | <template-name> | <iden-backref> }+

	lname ::= <number> <name>

When the qualified identifier includes a template instance, the ABI follows the following rules

Templates
`````````
A qualified identifier can be a template declaration or instance, when this occurs, the following mangling is used

::

	template-name ::= <template-definition> | <template-instance>

There are 2 types of template names

Template definition
^^^^^^^^^^^^^^^^^^^
::

	template-definition ::= "__T" <lname> <template-definition-arguments>

	template-definition-arguments ::= <number> { <templatee-definition-argument> [","] }+
	template-definition-argument  ::= ( "T" <lname> [ "G" <type> ] [ "H" <type>] "Z" )
	                                | ( "V" <lname> <type> [ "G" <number> "_" <lit> ] [ "H" <number> "_" <lit> ] "Z" )

An argument can be followed up by the following values
- `G`: The parameter is specialized
- `H`: The parameter has a defualt value

Template definition arguments follow the following rules

- `T`: Type, when specialized or having a default value, the identifiers are followed up by the type
- `V`: Value, when specialized or having a defualt value, the identifiers are followed up by the length of the literal, an underscore and the literal value


Template instance
^^^^^^^^^^^^^^^^^
::

	template-instance ::= "__I" <lname> <template-instance-arguments>

	template-instance-arguments ::= <number> { <template-instance-argument> [","] }+
	template-instance-argument  ::= ( "T" <type> "Z" )
	                              | ( "V" <type> <number> "_" <lit> "Z" )

A template instance argument can be one of the following type

- Type: what type is used
- Value: what value is used


Qualified type
--------------


::
	type ::= <func-type>
	       | <method-type>
	       | <def-type>
	       | <var-type>

	func-type ::= "F" <call-conv> <mangle-type>
	method-type ::= "M" <call-conv> <mangle-type>
	def-type ::= "T" <mangle-type>
	var-type ::= "V" <mangle-type>

	call-conv ::= "m" ; MJay
	            | "c" ; C
	            | "w" ; Windows


	mangle-param ::= {<mangle-param-mod>} <mangle-type>
	mangle-param-mod ::= "J" ; out
	                   | "K" ; lazy

	mangle-param-close ::= "Y" ; Variadic: `t...)` or `t : T...)`
	                     | "Z" ; Non-varaidic


	mangle-type ::= [<mangle-type-mods>] <mangle-type-x>


	mangle-type-mods  ::= <mangle-type-const>
	                    | <mangle-type-cconst>
	                    | <mangle-type-immutable>
	                    | <mangle-type-shared>
	                    | <mangle-type-global>

	mangle-type-const     ::= "Nc"
	mangle-type-cconst    ::= "Nd"
	mangle-type-immutable ::= "Ni"
	mangle-type-shared    ::= "Ns"
	mangle-type-global    ::= "Ng"

	mangle-type-x ::= <mangle-type-array>
	                | <mangle-type-pointer>
	                | <mangle-type-reference>
	                | <mangle-type-method-receiver>
	                | <mangle-type-struct>
	                | <mangle-type-union>
	                | <mangle-type-enum>
	                | <mangle-type-typedef>
	                | <mangle-type-delegate>
	                | <mangle-type-alias>
	                | <mangle-type-void>
	                | <mangle-type-byte>
	                | <mangle-type-ubyte>
	                | <mangle-type-short>
	                | <mangle-type-ushort>
	                | <mangle-type-int>
	                | <mangle-type-uint>
	                | <mangle-type-long>
	                | <mangle-type-ulong>
	                | <mangle-type-float>
	                | <mangle-type-double>
	                | <mangle-type-bool>
	                | <mangle-type-char>
	                | <mangle-type-wchar>
	                | <mangle-type-rune>
	                | <mangle-type-string>
	                | <mangle-type-null>
	                | <mangle-type-tuple>
	
	mangle-type-array             ::= "A" <abi-type>
	mangle-type-pointer           ::= "P" <abi-type>
	mangle-type-reference         ::= "R" <abi-type>
	mangle-type-method-receiver   ::= "O" <abi-type>
	mangle-type-struct            ::= "S" [<qualified-name>]
	mangle-type-union             ::= "U" [<qualified-name>]
	mangle-type-enum              ::= "E" <qualified-name> | <type>
	mangle-type-interface         ::= "I" [<qualified-name>]
	mangle-type-typedef           ::= "T" [<qualified-name>]
	mangle-type-delegate          ::= "D" <type-mods> <func-type>
	mangle-type-alias             ::= "L" <type>
	mangle-type-void              ::= "v"
	mangle-type-byte              ::= "g"
	mangle-type-ubyte             ::= "h"
	mangle-type-short             ::= "s"
	mangle-type-ushort            ::= "t"
	mangle-type-int               ::= "i"
	mangle-type-uint              ::= "j"
	mangle-type-long              ::= "l"
	mangle-type-ulong             ::= "m"
	mangle-type-float             ::= "f"
	mangle-type-double            ::= "d"
	mangle-type-bool              ::= "b"
	mangle-type-char              ::= "c"
	mangle-type-wchar             ::= "w"
	mangle-type-rune              ::= "r"
	mangle-type-string            ::= "u"
	mangle-type-null              ::= "n"
	mangle-type-tuple             ::= "M" <parameter>+ "Z"
	mangle-type-bitset            ::= "B" <type> <lit> "Z"
	mangle-type-template-instance ::= "J" <qualified-name>
	mangle-type-template-param    ::= "K" <int_lit> [ "_" <qualified-name> ]

