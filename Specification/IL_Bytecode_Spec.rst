::

	 ___      ___       ___      __       ___  ___  
	|"  \    /"  |     |"  |    /""\     |"  \/"  | 
	 \   \  //   |     ||  |   /    \     \   \  /  
	 /\\  \/.    |     |:  |  /' /\  \     \\  \/   
	|: \.        |  ___|  /  //  __'  \    /   /    
	|.  \    /:  | /  :|_/ )/   /  \\  \  /   /     
	|___|\__/|___|(_______/(___/    \___)|___/      

=================================================
IL (Intermediate Language) Bytecode Specification
=================================================

.. contents::

Introduction
------------

MJay IL Bytecode is a compact for of the IL, this is meant to be exported to modules, for both LTO and templates.

File structure
--------------

The bytecode is output in a little endian order.

Magic number
````````````

Each module file start using a magic number. The magic number consists out of 4 bytes.

::

	x80 'M' 'J' 'L'

The first byte contains a value outside of the ascii range. This is than followed up by MJM, which stands for MJay Module.

Header
``````

The file header defines the general meta data of the module and defines what sections are part of the library

The following table defines the components and byte width of each element

=============== ======= ===================================
      name       bytes     Description
=============== ======= ===================================
   File size       4     Size of the file (including magic)
     Flags         2     Flags (see below)
  Table count      1     Number of tables before functions
   Func count      2     Number of functions
=============== ======= ===================================

Tables
---------

Tables contain the data reference by the instructions

=============== ======= ===================================
      name       bytes     Description
=============== ======= ===================================
   Table iden      4     Table identifier (4 byte 0-term)
 Element count     2     Amount of element in the table
      Data         N     Elements (0-term strings)
=============== ======= ===================================

============ ===================================
 identifier   Description
============ ===================================
   typ\0      Type table
   str\0      string table
   mem\0      member references
   fun\0      function references
   opr\0      operator references
   glo\0      global table
============ ===================================

Function encoding
-----------------
=============== ======= ===================================
      name       bytes     Description
=============== ======= ===================================
   Func ref id     2    Function reference id
    Type id        2    Type id
   instr count     2    Number of basic blocks
  instructions     N    Basic blocks
=============== ======= ===================================

Basic block encoding
^^^^^^^^^^^^^^^^^^^^
=============== ======= ===================================
      name       bytes     Description
=============== ======= ===================================
    Block id       2    Basic block index
   instr count     2    Number of instructions in the block
  instructions     N    Instructions
=============== ======= ===================================


Instructions
------------

All instruction have there specific 'string' of bytes. Instructions are not fixed lenght, this is caused by the varying parameters each instruction has

Instruction Id table
````````````````````
The following table contains all current instruction ids

====================== ======
      Instruction        ID 
====================== ======
      int_literal       0x00 
     float_literal      0x01 
     string_literal     0x02 

      alloc_stack       0x10
     dealloc_stack      0x11
        assign          0x12
         store          0x13
         load           0x14

      function_ref      0x20
       method_ref       0x21
      operator_ref      0x22
         call           0x23
        builtin         0x24
    compiler_intrin     0x25

      tuple_pack        0x30
     tuple_unpack       0x31
     tuple_insert       0x32
     tuple_extract      0x33
    tuple_elem_addr     0x34

     struct_insert      0x40
     struct_extract     0x41
   struct_elem_addr     0x42

      union_insert      0x50
      union_extract     0x51
     union_elem_addr    0x52

        enum_value      0x60
      enum_get_value    0x61
 enum_get_member_value  0x62

     global_addr        0x70
     global_value       0x71

  template_get_value    0x80

       conv_type        0x90

         return         0xA0
         branch         0xA1
      cond_branch       0xA2
      switch_enum       0xA3
      switch_value      0xA4
====================== ======

Literal instructions
````````````````````

int_literal
^^^^^^^^^^^
::

	00 RRRR LLLLLLLLLLLLLLLL TTTT

	RRRR             -> Return value id
	LLLLLLLLLLLLLLLL -> Integer value
	TTTT             -> Type table index

float_literal
^^^^^^^^^^^^^
::

	01 RRRR LLLLLLLLLLLLLLLL TTTT

	RRRR             -> Return value id
	LLLLLLLLLLLLLLLL -> Hex value
	TTTT             -> Type table index

string_literal
^^^^^^^^^^^^^^

::

	01 RRRR SSSS

	RRRR -> Return value id
	SSSS -> String table index

Memory instructions
```````````````````

alloc_stack
^^^^^^^^^^^
::

	10 RRRR TTTT

	RRRR -> Return value id
	TTTT -> Type table index

dealloc_stack
^^^^^^^^^^^^^
::

	11 SSSS

	SSSS -> stack value id

assign
^^^^^^
::

	12 SSSS DDDD

	SSSS -> Source value id
	DDDD -> Dest value id

store
^^^^^
::

	13 SSSS DDDD

	SSSS -> Source value id
	DDDD -> Dest value id

load
^^^^
::

	14 RRRR FFFF

	RRRR -> Return value id
	DDDD -> Dest value id

Function instructions
`````````````````````

function_ref
^^^^^^^^^^^^
::

	20 RRRR FFFF TTTT

	RRRR -> Return value id
	FFFF -> Function table index
	TTTT -> Type table index

method_ref
^^^^^^^^^^
::

	21 RRRR MMMM TTTT

	RRRR -> Return value id
	MMMM -> Method ref table index
	TTTT -> Type table index

operator_ref
^^^^^^^^^^^^
::

	22 RRRR OOOO TTTT

	RRRR -> Return value id
	OOOO -> Operator table index
	TTTT -> Type table index


call
^^^^
::

	23 RRRR FFFF CC OOOO... TTTT

	RRRR    -> Return value id
	FFFF    -> Function value id
	CC      -> Operand count
	OOOO... -> Operands value ids
	TTTT    -> Type table index


builtin
^^^^^^^
::

	24 RRRR BB CC OOOO... TTTT

	RRRR    -> Return value id
	BB      -> Builtin id (id of IR instruction)
	CC      -> Operand count
	OOOO... -> Operand value ids
	TTTT    -> Type table index


compiler_intrin
^^^^^^^^^^^^^^^
::

	25 RRRR II TTTT RTRT

	RRRR    -> Return value id
	II      -> Instrinsic id
	TTTT    -> Type table id
	RTRT    -> Return type table id


Tuple instructions
``````````````````

tuple_pack
^^^^^^^^^^^^
::

	30 RRRR CC OOOO...

	RRRR    -> Return value id
	CC      -> Operand count
	OOOO... -> Operand value ids

tuple_unpack
^^^^^^^^^^^^
::

	31 CC RRRR... OOOO

	CC      -> Operand count
	RRRR... -> Return value ids
	OOOO    -> Operand value

tuple_insert
^^^^^^^^^^^^
::

	32 TTTT II OOOO

	TTTT -> Tuple value id
	II   -> Tuple index
	VVVV -> Operand value id


tuple_extract
^^^^^^^^^^^^
::

	33 RRRR TTTT II

	RRRR -> Return value id
	TTTT -> Tuple value id
	II   -> Tuple index


tuple_elem_addr
^^^^^^^^^^^^^^^
::

	34 RRRR TTTT II

	RRRR -> Return value id
	TTTT -> Tuple value id
	II   -> Tuple index


Struct instructions
```````````````````

struct_insert
^^^^^^^^^^^^^^
::

	40 SSSS MMMM OOOO

	SSSS -> Struct value id
	MMMM -> Member ref table index
	OOOO -> Operand value id


struct_extract
^^^^^^^^^^^^^^
::

	41 RRRR SSSS MMMM TTTT

	RRRR -> Return value id
	SSSS -> Struct value id
	MMMM -> Member ref table index
	TTTT -> Type table id

struct_elem_addr
^^^^^^^^^^^^^^^^
::

	42 RRRR SSSS MMMM TTTT

	RRRR -> Return value id
	SSSS -> Struct value id
	MMMM -> Member ref table index
	TTTT -> Type table id


Union instructions
``````````````````

union_extract
^^^^^^^^^^^^^^
::

	50 UUUU MMMM OOOO

	UUUU -> Union value id
	MMMM -> Member ref table index
	OOOO -> Operand value id

union_insert
^^^^^^^^^^^^
::

	51 RRRR UUUU MMMM TTTT

	RRRR -> Return value id
	UUUU -> Union value id
	MMMM -> Member ref table index
	TTTT -> Type table id


union_elem_addr
^^^^^^^^^^^^^^^^
::

	52 RRRR UUUU MMMM TTTT

	RRRR -> Return value id
	UUUU -> Union value id
	MMMM -> Member ref table index
	TTTT -> Type table id

Enumeration instructions
````````````````````````

enum_value
^^^^^^^^^^
::

	60 RRRR TTTT MMMM CC OOOO...

	RRRR    -> Return value id
	TTTT    -> Type table index
	MMMM    -> Method ref table index
	CC      -> Operand count
	OOOO... -> Operand value ids

enum_get_value
^^^^^^^^^^^^^^
::

	61 RRRR OOOO RTRT

	RRRR -> Return value id
	OOOO -> Operand value id
	RTRT -> Return type table index

enum_get_member_value
^^^^^^^^^^^^^^^^^^^^^
::

	62 RRRR TTTT MMMM RTRT

	RRRR -> Return value id
	TTTT -> Type table index
	MMMM -> Method ref table index
	RTRT -> Return type table index


Global var instructions
```````````````````````

global_addr
^^^^^^^^^^^
::

	70 RRRR GGGG TTTT

	RRRR -> Return value id
	GGGG -> Global table index
	TTTT -> Type table index

global_value
^^^^^^^^^^^^
::

	71 RRRR GGGG TTTT

	RRRR -> Return value id
	GGGG -> Global table index
	TTTT -> Type table index


Template instructions
`````````````````````

template_get_value
^^^^^^^^^^^^^^^^^^
::

	80 RRRR TTTT MMMM RTRT

	RRRR -> Return value id
	TTTT -> Type table index
	MMMM -> Method ref table index
	RTRT -> Return type index

Other instructions
```````````````````````

convert_type
^^^^^^^^^^^^
::

	90 RRRR OOOO TTTT

	RRRR -> Return value id
	OOOO -> Operand value id
	TTTT -> To type table index

get_vtable
^^^^^^^^^^
::

	91 RRRR TTTT CC IIII...

	RRRR -> Return value id
	TTTT -> Type table index
	CC   -> Interface count
	IIII -> Interface type table index


Terminators
```````````

return
^^^^^^
::

	A0 OOOO
	OOOO -> Operand value id

branch
^^^^^^
::

	A1 BBBB

	BBBB -> Block index

cond_branch
^^^^^^^^^^^
::

	A2 OOOO TTTT FFFF

	OOOO -> Operand value id
	TTTT -> True block index
	FFFF -> False block index

switch_enum
^^^^^^^^^^^
::

	A3 EEEE CCCC (MMMM BBBB)... DDDD

	EEEE -> Enum operand id
	CCCC -> Case count
	MMMM -> Member ref id
	BBBB -> Block index
	DDDD -> Default block index

switch_value
^^^^^^^^^^^^
::

	A3 VVVV CCCC (SSSS BBBB)... DDDD

	VVVV -> Value operand id
	CCCC -> Case count
	SSSS -> Switch operand value id
	BBBB -> Block index
	DDDD -> Default block index
