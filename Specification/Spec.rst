::

	 ___      ___       ___      __       ___  ___  
	|"  \    /"  |     |"  |    /""\     |"  \/"  | 
	 \   \  //   |     ||  |   /    \     \   \  /  
	 /\\  \/.    |     |:  |  /' /\  \     \\  \/   
	|: \.        |  ___|  /  //  __'  \    /   /    
	|.  \    /:  | /  :|_/ )/   /  \\  \  /   /     
	|___|\__/|___|(_______/(___/    \___)|___/      

==================================
Programming Language Specification
==================================

.. contents::

Introduction
------------
This document defines the specification for the programming language

MJay is a general purpose programming language that has both low and high level features. 
The language is strongly typed.

Notation
--------

The language is based on the extended backus-naur form (EBNF), with some slight changes::

	literal             = "value" or 'value'
	node                = <value>
	alternatives        = <value> | <value>
	grouping            = ( <values> )
	optional            = [ <value> ]
	optional, 0 or more = { <values> }
	optional, 1 or more = { <values> }+
	0 or more           = <value> *
	1 or more           = <value> +
	range               = <value> .. <value>
	assign              = identifier ::= <value>

Source code representation
--------------------------

Source code is represented as Unicode text encoded in UTF-8. The text is expected to not be canonicalized, character consisting out multiple code points will be interpreted as seperate characters.

Whitespace, End of Line and End of File
---------------------------------------
::

	space ::= "\u0009"
	        | "\u000B"
	        | "\u000C"
	        | "\u0020"

	white-space ::= <space>+

	eol ::= "\u000A"
	      | "\u000D"
	      | "\u000D\u000A"
	      | "\u2028"
	      | "\u2029"

Unicode, letters and digits
---------------------------
::

	unicode-char    ::= /* any Unicode character, with the exception of \u0000 */
	unicode-letter  ::= /* any <character> classified as "Letter" */
	unicode-digit   ::= /* any <character> classified as "Number" or "Digit" */

	letter          ::= <unicode-letter> | "_"
	decimal-digit   ::= "0" .. "9"
	octal-digit     ::= "0" .. "7"
	hex-digit       ::= "0" .. "9" | "a" .. "f" | "A" .. "F"
	binary-digit    ::= "0" | "1"

Lexical elements
----------------

Comments
````````

Comments serve as additional info or documention for code.

There are 2 types of comment in MJay

1. Single line comments, they start with the sequence "//" and stop when an 'eol' is encountered
2. Multi line comments, they start with the sequence "/\*" and stop with the sequence "\*/". Multi line comments can be nested, meaning that the amount of "/\*" sequences should match the number of "\*/" sequences

Semicolons
``````````

The grammar requires the use of semicolons ";", these character terminate each statement. There are certain exceptions on this rule.

- At the end of certain statements that end with ")" or "}". Cases of these exceptions will be clearly specified in the specification

Identifiers
```````````
Identifier are used to define and reference entities like variables or types. An identifier is a sequence of letters and digits. Identifier are required to start with a 'letter'::

	identifier ::= <letter> { <letter> | <unicode-digit> }

Some examples::

	a
	variable891
	AlsoAVariable
	_with_underscores_
	αβ

There are also identifiers which are reserved for the programming language, and which are invalid to be used as identifier for user defined variables or types.

Other than variables or types, names starting with a double underscore '__' are also reserved, although not explicitly, this could cause name collision with the `core` and `std` packages or compiler generated code. 
If the application doesn't have name collisions when created, this does not mean that this would cause issues in the future.

All other exceptions can be found later on in the specification.

Keywords
````````

The following keywords are reserved and by extension, they cannot be used as identifiers::

	alignof
	as
	asm
	assert
	break
	case
	cconst
	const
	continue
	debug

	default
	defer
	delegate
	delete
	do
	else
	elif
	enum
	export
	fallthrough

	false
	for
	foreach
	func
	goto
	if
	interface
	immutable
	import
	internal

	is
	in
	inout
	impl
	lazy
	module
	mutable
	namespace
	new
	out

	package
	private
	public
	return
	self
	Self
	shared
	sizeof
	static
	struct

	switch
	synchronized
	transmute
	typealias
	typedef
	typeid
	typeof
	union
	unittest
	version
	
	while
	__c_cast
	__global

Some reserved keywords are not shown here, since they are either types or constants.

Special keywords
````````````````

Special keywords are keywords which get replaced at compiler timeat their corresponding value:

- `__FILE__`: Inserts the file name
- `__FILE_FULL_PATH__`: Inserts the full file path
- `__PACKAGE__`: Inserts the current package name
- `__MODULE__`: Inserts the current module name
- `__NAMESPACE__`: Inserts the current namespace
- `__LINE__`: Inserts the current line number
- `__FUNC__`: Insert the current function name
- `__PRETTY_FUNC__`: Inserts the current function name, including namespaces

Operators and punctuation
`````````````````````````

The following characters sequences are valid operators and puntiuation::

         operators                  puntuation

    =       :=      &&              (       )
    +       +=      ||              [       ]
    -       -=      ++              {       }
    *       *=      --              ,       ;
    /       /=      ==              :       ...
    %       %=      !=              @       @:
    ~       ~=      >               =>      ->
    &       &=      >=              !<      ?
    |       |=      <               $		$"
    ^       ^=      <=
    ^^      ^^=     ?
    >>      >>=     ?.
    <<      <<=     ??
    >>>     >>>=    .
    ::      ..

More explenation about all these operators and punctuation will be given further on in the specification.

Integer literals
````````````````

An integer literal is a sequaence of digits representing an intereger constant. An optional prefix can be used to define numbers in a non-decimal base. Integer literals may be separated using `'` or `_` to make these literal more readable.

An integer literal can also contain a suffix, allowing the user to explicilty define the type of the resulting constant.
::

	int_lit      ::= (decimal_lit | octal-lit | hex-lit | bin-lit) [<lit-suffix>]
	decimal-lit  ::= ["-"] ( "1" .. "9" ) { [ "'" | "_" ] <decimal-digit> }
	octal-lit    ::= "0" { [ "'" | "_" ] <decimal-digit> }+
	hex-lit      ::= "0" ( "x" | "X" ) { [ "'" | "_" ] <decimal-digit> }+
	bin-lit      ::= "0" ( "b" | "B" ) { [ "'" | "_" ] <decimal-digit> }+
	lit-suffix   ::= <int-suffix> | <float-suffix>
	int-suffix   ::= ( "u" | "i" ) ( "8" | "16" | "32" | "64" )
	float-suffix ::= "f" ( "32" | "64" )

Examples::

	42
	2365ul
	1253LU
	0753
	0xDeadBeef
	0b10011010
	157_916_713_817
	6'210'450'489

Floating-point literals
```````````````````````

A floating point literal is a decimal representation of a floating point literal. It can consist out of an integer part, a decimal point, a fractional part and an exponent part. The exponent part is represented by either `e` or `E` followed by an optionally signed integer literal. A valid floating-point literals is required to at least have an integer or fractional part and a decimal or exponent.

Like an integer literal, a floating point literal also allows the addition of a suffix the specify the type of the resulting type.
::

	decimals  ::= <decimal-digit>+
	exponent  ::= ( "e" | "E" ) [ "+" | "-" ] <decimals>
	fp-lit    ::= ( <decimals> "." [<decimals>] [exponent] )
	            | ( "." <decimals> [<exponent>] )
	            | ( <decimal> <exponent> )
	fp-suffix ::= [ "f" | "F" ]
	float-lit ::= <fp-lit> <fp-suffix>

Examples::

	0.
	52.63
	052.63
	2.71828f
	1.e+0
	6.67428e-11
	1E6
	.25
	.12345E+5

Rune literals
`````````````

A rune literal, also known as a character literal, represents a rune constant, an integer value representing a Unicode code point. A rune literal is expressed as a character or a special sequence enclosed in single quotes `'a'` or `'\n'`. Betweens these 2 quotes, any character may appear with the exceptions of unescaped single quotes. A literal consisting of a single character represents the value of its Unicode point, while multiple characters, starting with a backslash `\` represent a special value.

The simplest, a single character, represents its Unicode value encoded in UTF-8, this means that a single character can consist out of multiple bytes, compared to other encoding systems, like ASCII. As an example, the rune literal `'a'` representes the value of the literal `a`, takes up 1 byte, Unicode `U+0061` or the value `0x61`. While the character `'ä'`, representing the literan `a`-dieresis, takes up 2 bytes, Unicode U+004E or value 0x4e. Note that even when the actual value could be stored in 1 byte, the UTF-8 encoding causes this to take up 2 bytes

There are several valid backslach escape codes, which allow arbitrary values to be encoded in ASCII. There are 4 of these representations and they are the following: A `\x` followed by exactly 2 hexadecimal digits; A `\u` followed by exactly 4 hexadecimal digits; A `\U` followed by exactly 8 hexadecimal digits and a plain `\` followed by exactly 3 octal digits, this value is also required to fall in the range 0-0xFF or 0000-0377.

While any code point can be represented using `'\u'` or `'\U'`, some of these are illegal, these include code points that are not represented in the Unicode specification (0x10FFFF and above) and surrogate halfs

There are also several single character escape codes, representing special values:

	\a	U+0007	alert or bell
	\b	U+0008	backspace
	\f	U+000C	form feed
	\n	U+000A	line feed or new line
	\r	U+000D	carriage return
	\t	U+0009	horizontal tab
	\v	U+000B	vertical tab
	\\	U+005C	backslash
	\'	U+0027	single quote	(only valid inside of rune literals)
	\"	U+0022	double quote	(only valid inside of string literals)

All other sequences starting with a backslash are illegal, as are multi character sequences.
::

	rune-lit         ::= "'" ( <unicode-lit> | <byte-value> ) "'"
	unicode-lit      ::= <unicode-char> | <little-u-value> | <big-u-value> | <escaped-char>
	byte-value       ::= <octal-byte-value> | <hex-byte-value>
	octal-byte-value ::= "\" <octal-digit> <octal-digit> <octal-digit>
	hex-byte-value   ::= "\x" <hex-digit> <hex-digit>
	little-u-value   ::= "\u" <hex-digit> <hex-digit> <hex-digit> <hex-digit>
	big-u-value      ::= "\U" <hex-digit> <hex-digit> <hex-digit> <hex-digit>
	                          <hex-digit> <hex-digit> <hex-digit> <hex-digit>
	escaped-char     ::= "\" ( "a" | "b" | "f" | "n" | "r" | "t" | "v" | "\" | "'" | '"' )

Examples::

	'a'
	'ä'
	'本'
	'\t'
	'\000'
	'\007'
	'\377'
	'\x07'
	'\xff'
	'\u12E4'
	'\U00101234'
	'\''          // rune literal containing single quote
	'aa'          // illegal: too many characters
	'\xa'         // illegal: too few heximal digits
	'\0'          // illegal: too few octal digits
	'\DFFF'       // illegal: surrogate half
	'\U00110000'  // illegal: invalid code point

String literals
```````````````
A string literal represents a string constant, obtained by concatinating a sequence of characters. MJay allows string to be represented in 3 forms: a basic string, an interpolated string and a wysiwyg string.

A basic string literal represents a sequence of character between double quotes, as in `"foo"`. Within this string any character can appear, with the exception of a double quote. The value of the raw string is composed of (implicitly encoded UTF-8) characters, included escape sequences, except for `\'`, which is illegal, but including `\"`. Any three-digit octal string and two-digit hexadecimal escape sequences will be interpreted as there respecting single byte character. Both `\u` and `\U` will be interpreted as their respective values encoded as UTF-8 encodings.

An interpolated string is an easier way to format a string, but instead of explicitly using a string format, the values that will be used can be inserted inside of the string and will be automatically be converted to its correct format. The string is defined similarly to a basic string, but it is prefixes with `$`, notifiying that the string is interpolated

A wysiwig or 'what you see is what you get' string represents a sequence of characters between back quotes ``, with the exlusion of a back quote an escape sequences, therefore any escapes sequences will be interpreted as if they would be starting with an escaped backslash. Unlike other string literals, a wysiwyg string is allowed to contained physical new lines and tabs, as they will be interpreted as their respective escape sequences.
::

	string-lit ::= <basic-string-lit> | <interpolated-string-lit> | <wysiwyg-string-lit>
	basic-string-lit        ::= '"' { <unicode-value> | <byte-value> } '"'
	interpolated-string-lit ::= '$"' { <unicode-value> | <byte-value> } '"'
	wysiwyg-string-lit      ::= "`" { <unicode-digit> | <eol> } "`"

Examples::

	`abc`                // same as "abc"
	`\n
	\n`                  // same as "\\n\n\\n"
	"\n"
	"\""                 // same as `"`
	"Hello, world!\n"
	"日本語"
	"\u65e5本\U00008a9e"
	"\xff\u00FF"
	"\uD800"             // illegal: surrogate half
	"\U00110000"         // illegal: invalid Unicode code point

These examples all represent the same string:
::

	"日本語"                                 // UTF-8 input text
	`日本語`                                 // UTF-8 input text as a wysiwig literal
	"\u65e5\u672c\u8a9e"                    // the explicit Unicode code points
	"\U000065e5\U0000672c\U00008a9e"        // the explicit Unicode code points
	"\xe6\x97\xa5\xe6\x9c\xac\xe8\xaa\x9e"  // the explicit UTF-8 bytes

If the source code represents a character as two code points, such as a combining for involving an accent and a letter, the result will e an error if placed in a rune literal (it is not a single code point), and will appear as two code point if places in a string literal.

Boolean literals
````````````````

A boolean literal is a keyword, representing a boolean literal.
::

	bool-lit ::= "true" | "false"

Null literal
````````````

The null literal is a keyword, represents=ing a special type of constant, the null constant.
::

	null-lit ::= "null"

Constants
---------

There are multiple types of constants: boolean constants, rune constants, integer constants, floating-point constants, string constant and a special null constant.

Variables
---------

A variable is a location in memory that stores a value. The permissible values of a variable is defined by its type.

To try to help make user-code more thread safe, any variable outside of a function or aggregate, will be generated in thread local space, instead of the global data of the executable. To be able to have the variable in the global data of the executable, check `__global`_

Default initialization
``````````````````````
When a variable is declared using an explicit declaration and no value is assigned to them, the compiler will generate a default value::

	arithmatic types: 0 or 0.0
	pointer types: null
	aggregate types: default constructor

Types
-----

A type determines the allowed operations and methods can be used by varaible of that type.

::

	type ::= {<type-modifier>} <sub-type> [<template-arguments>] ["..."]

	sub-type ::= ( ["("] <type> [")"] )
	           | ( "*" type )
	           | ( "[" [<expression>] "]" <type> )
	           | ( "?" <type> )
	           | <basic-type>

	basic-type ::= <builtin-type>
	             | <identifier-type>
	             | <delegate-type>
	             | <tuple-type>
	             | <typeof> 

	typeof ::= ( "typeof" "(" <expression> ")" ) |
	         | ( "typeof" "(" "return" [ "," <expression> ] ")" )

Builtin types
`````````````
::

	builtin-type ::= "b8"    | "bool"
	               | "i8"    | "byte"
	               | "i16"   | "short"
	               | "i32"   | "int"
	               | "i64"   | "long"
	               | "u8"    | "ubyte"
	               | "u16"   | "ushort"
	               | "u32"   | "uint"
	               | "u64"   | "ulong"
	               | "f32"   | "float"
	               | "f64"   | "double"
	               | "isize" | "usize"
	               | "char"
	               | "wchar"
	               | "rune"
	               | "string"

There are a couple types already built in to the core language.

Boolean type
^^^^^^^^^^^^

A boolean type can represent 2 values, either `true` or `false`.

A boolean type takes up 1 byte and has 2 aliases: `bool` and `b8`

Artihmatic types
^^^^^^^^^^^^^^^^

An arithmatic types represent a numeric value, either an integer or floating point value. There are couple different types, each with 2 aliasses::

	i8  or byte  , 1 byte integer type, -128 to 127
	i16 or short , 2 byte integer type, -32'768 to 32'767
	i32 or int   , 4 byte integer type, -2'147'483'648 to 2'147'843'647
	i64 or long  , 8 byte integer type, -9'223'372'036'854'775'808 to 9'223'372'036'854'775'807
	u8  or ubyte , 1 byte integer type, 0 to 255
	u16 or ushort, 2 byte integer type, 0 to 65'535
	u32 or uint  , 4 byte integer type, 0 to 4'294'967'296
	u64 or ulong , 8 byte integer type, 0 to 18'446'744'073'709'551'616

	f32 or float , 4 byte floating-point type, -3.4e-38 to 3.4e+38
	f64 or double, 8 byte floating-point type, 1.7e-308 to 1.7e+308

Character type
^^^^^^^^^^^^^^

A character type represents a character in the UTF-8 code space, their are 3 different character types, each with a max amount of codepoints that can be stored.

::

	char , 1 byte character type
	wchar, 2 byte character type
	rune , 4 byte character type

String type
^^^^^^^^^^^

A string type holds a range of character representing a textual string.

::

	string

Identifier types
````````````````

An identifier type references a custom type.

::

	identifier-type ::= ( <identifier-type> "." <identifier-type> )
	                  | ( "." <identifier> )
	                  | <template-instance>
	                  | <identifier> 


Array types
```````````

An array is a numbered sequence of a single type, also known as the element type. An array can be created with and without a predefined length, allowing a user to have an array that is created at runtime with a variable size, but still be represented as a single type.

::

	array-type ::= "[" [<expression>] "]" <type>

When an array is created with a constant size, the size of that array needs to be a positive value, which needs to be a able to be computer at compile time, and also has to be an integer type. A value in an array can be accessed with index in the range of `0` to `size - 1`, similar to most programming languages. An array type will always be represented as a 1-dimensional type, but can be used to compose a multi dimensional type.

Examples::

	[4]i32		// An array of 4 ints
	[3][5]u8	// An array of 3 arrays, each with 5 values
	[]f32		// An array of floats with a size that is unknown at compile time, and can thus represent any size of array

Pointer Types
`````````````

A pointer type represent a type that points to the memory location of the `base type`. The default value of a pointer is `null`.

::

	pointer-type ::= "*" <type>

Function types
``````````````

A function type defines a set of functions with the same parameters and result types. Do note that function types cannot be used independant of any declarations, to be able to use functions independently, `Delegate types`_ should be used

::

	signature      ::= <parameters> ["->" <result>]
	result         ::= <parameters> | <type>
	parameters     ::= "(" { <parameter-decl> [","] } ")"
	parameter-decl ::= [<func-param-attribute>*] [<identifier-list> ":"] ["..."] <type>

A list of parameters or results can either have all names be present or absent. If present, each names refers to a single item of the specified type and all non blank names must be unique. When absent, each type stands for a single item. Parameters and results are always parameterized, with the exception if a single unnamed type is returned.

The final parameter of the parameters, may be type prefixed with `...` meaning that this parameter is `variadic` and can accept 0 or more parameters of that type. The final parameter can also be used without any type, but only if the name is followed up by `...`, this also means that this variable is `variadic` but the parameters given in for this value can be of any type.

Example of possible signatures::

	()                                     // A signature without any parameter or results
	(x : int)                              // A signature with 1 int, x
	(a, b : int, z float) -> bool          // A signature with 2 int, a and b and a float, z, returning a bool
	(a, _ : int, z float) -> bool          // The same signature as above, with the exception that instead of b a blank identifier is used
	(prefix : string, values ...int)       // A function with a string, prefix and a varaidic paremeter of type int
	(format : string, values...)           // A function with a string, format and a varaidic paremeters of unknown types
	(int, int, float) -> ([]float, *[]int) // A function returning multiple result values

Delegate types
``````````````

A delegate type represents a standalone type, with the same base type as the function type.

::

	delegate-type ::= "delegate" <signature>

	delegate-decl ::= "delegate" <identifier> <signature>

A delegate can also be created with an identifying name
::

	delegate DELPROC(int) -> *void;
	// is the same as
	typedef delegate(int) -> *void as DELPROC;

Implementation detail
^^^^^^^^^^^^^^^^^^^^^

Since a delegate is required to be able to store functions, methods and closures, the delegate create code based on the needs for the specific delegate.

The delegate gets converted into a structure, able to store the required data for the delegate to work and implements the correct interfaces.
\
When a delegate is declared with `func_ptr`_, the delegates is syntaxtic sugar on top of a simple function pointer.

Tuple types
```````````

A tuple types represents a collection of multiple different types.

::

	tuple-type ::= "(" <type> { "," <type> }+ ")"

Structure types
```````````````
::

	struct-decl ::= [<attributes>] struct [<identifier>] [<template-parameters>] [<template-constraint>] "{" { <var-decl> [","] } "}"

A structure is the only aggregate type that exists in the language, meaning that this is a collection of values, each having its own identifier and type.

A structure type is mostly usefull as a collection of data.

Inheriting from a structure is illegal. It is possible to have some kind of pseudo-inheritence, by using the `Using statement`_.

A structure can also be anonymous, meaning that the structure does not have an identifier. While the use cases of this type of structure are limited, since it direclty exposes variables to type in which is defined and cannot be referenced. It can be used in a `Union types`_ to represent data that the union should count as 1 value.

By default, all fields inside of the structure are public.

Examples::

	// A structure containing 3 variables, int `x` and floats `y` and `z` and a function called Bar
	struct Foo
	{
		x : i32;
		y, z : f32;
	}

	// Illegal, inheritence does not exist
	struct FooDerived : Foo
	{
		w : u64;			// 'w' would come after 'z', which would be located in the inherited class
	}

	// pseudo-inheritence from Foo
	struct FooDerived
	{
		using Foo;
		w : u64;			// 'w' comes after 'z', which is located in the base class
	}

	// Anonymous structure, mostly usefull in unions
	struct
	{
		x, y, z : i32;
	}

A structure type can also be directly used as the type of a variable in the form of an anonymous inline structure. This type of structure is limited to only contain declarations. Declaration in an inline structure are separated by `,` , instead of ';'

::

	anon-inline-struct ::= "{" <declaration>* "}"

Examples::

	foo : { x, y : i32, z : f32 }; // an inline structure with 2 ints: `x` and `y`, and a float `z`

	foo.x	// Variable can be accessed like a normal member

Union types
```````````
::

	union-decl ::= "union" [<identifier>] [<template-parameters>] [<template-constraint>] "{" <declaration>* "}"

A union can represent a single value as multiple different types. This could be compared to using `Transmute` to cast to a different type, but simplified as being it's own type, in which each possible `Transmute` to a type has its own identifier. Unlike a `Transmute`, a union guarentees to contain enough space to contain the largest type an will thus keep all data stored into it.

Like a structure, a union can be anonymous, allowing the use of larger union and structure hierachies without having to used excessively long identifier to be able to access these different representation of a value.

Example::

	union Foo 
	{
		a : u64;
		b : f64;

		// This structure gets interpreted as if it's its own type
		// c will access the first 4 bytes of the value stored in the union
		// 
		struct
		{
			c : i32; // c points to the first 4 bytes of the value stored in the union
			d : u32; // d points to the last 4 bytes of the value stored in the union
		}
	}

	u : Foo;
	u.a = 152;
	v : u.b;	// v is now equal to the value of u.a, but transmuted to a double

::

	anon-inline-union ::= "union" "{" <declaration>* "}"

Examples::

	foo : union { x : i32, z : f32 }; // an inline union with 2 ints: of which the value can be represented as an int or a float

	foo.x	// Variable can be accessed like a normal member (in this case as an int)

Interface types
```````````````
::

	interface-decl ::= "interface" <identifier> "{" { <function> | <interface-property> } "}"

An interface is a way of guaranteeing the existance of various properties and methods for aggregates that implement the interface. An interface can however not contain any data or function implementations of its own.

While an interface can guarantee the existance of specific function, it cannot guarantee the existance of certain allocators or deallocators.

If 2 interfaces contain the exact same function prototype, then only 1 implementation is to be made. After this, if the aggregate is downcast to any of the 2 interfaces, the exact same function will be available to them.

Example::

	// Any method_set implementing from IFile, is required to implement both `Read` and `Write`
	interface IFile
	{
		func(file:const *File) Read(b : Buffer) -> bool;
		func(file:*File) Write(b : Buffer) -> bool;
	}

	impl File : IFile
	{
		func(file: const*File) Read(b : Buffer) -> bool { ... } // Implementation of Read
		func(file:*File) Write(b : Buffer) -> bool { ... } // Implementation of Write
	}

	// Only partially implementing an interface is illegal
	impl IllegalFile : IFile
	{
		func(file:const *File) Read(b : Buffer) -> bool { ... } // Implementation of Read
		// Error, Write() has not been implemented
	}

To be able to implement an interface without a function, the interface can be added to any `Impl`_ for the type. Interfaces without any methods can also be implemented with an empty `Impl`_.

An interface can also define properties and fields that it expects to exist.

Properties are defined using the following syntax.
::

	interface-property ::= <identifier> ":" <type> "{" { ( "get" ";" ) | ( "set" ";" ) } "}"

While interfaces allow a default implementation of a method, any variable that is used from `self` is required to exist as an interface field.

::

	interface-field ::= <identifier> ":" <type> ";"

When the methods for an interface that uses fields is declared, the interface fields need to have the corresponding values in the structure defined to them.

::

	interface-field-def ::= <identifier> ":" <qualified-iden>

In this definition, the first identifier points to the interface field and the qualified identifier points to the corresponding value in the structure.

Compund interface
^^^^^^^^^^^^^^^^^
::

	compound-interface ::= <identifier> { "+" <identifier> }

A compund interface a way of combining multiple interfaces and that will act as a single interface.

Implementation detail
^^^^^^^^^^^^^^^^^^^^^

If a variable is stored as an instance of an interface, the varaible should be stored in the following way:

- Pointer to the data
- Pointer to the VTable

When using an interface, static dispatching is not possible, this result in a method call having the be dynamically dispatch, by using the provided VTable

The implementation also disallows for casting between different interfaces, including from compound to member interfaces, since the vtables cannot yet be resolved. This might be possible in the future, when the RTTI sytem has been implemented, but this will still have some runtime cost.

Enum types
``````````
::

	enum-decl ::= "enum" [<identifier>] [":" <type>] "{" <enum-member>* "}"

	enum-member ::= <identifier> [ "(" <arguments> ")" | "{" <declaration>* "}" | "=" <expression> ] [","]

An enum represents a range of variants of a single type. These variants can be represented by a single numeric value (a simple enum) or using an ADT (Algebraic Data Type) enum.

Simple enums
^^^^^^^^^^^^
::

	enum SimpleEnum
	{
		Value0,
		Value1,
	}

A simple enum is standard enum that most languages include. Each member of the enum represents a single numeric value. A simple enum can, for example, be used to store flags, since the simple enum allows the use of the builtin integer operators to be used on the simple enum.

ADT enums
^^^^^^^^^
::

	enum AdtEnum
	{
		Value0,
		Value1(string),
		Value2 { x : int; y : float; }
	}

An ADT enum can also be refered to as a tagged union. Each member can represent a single numeric value, like a simple enum, or it can contain its own structure. A member that can contain one of these structures is defined as the identifier, followed by a tuple representing the types of the variaables inside of that enum member.

The type used to represent the value in a simple enum or the tag of the ADT enum can be specified before the enum body. This type has to be an integer type. If this type isn't present, the type will automatically be assigned as a 32-bit integer. If any value is larger than 32-bits, these values will be truncated to 32-bit values (this may change in the future).

While it is possible to also define the type of the tag for an ADT enum, there will still be padding to correctly align the sub structures, unless a pack attribute is specified.

typeof 
``````
::

	typeof ::= "typeof" "(" <expression> ")"

Typeof is not a type by itself, but it gets the type from the expression given to it.

Blocks
------

A block is a possibly empty collection of declarations and statements, surrounded by matching braces.

::

	block-decl ::= "{" <statement>* "}"

Each block has its own implicit scope, meaning that, if a block is defined within a function, all declared variable will only be available inside of that block.

Certain statement will implicitly generate a block and might add additional variables to the explicity defined blocks for certain statements.

- `for`, `foreach`, `while`, `switch` and `if` all add variables that are declared in the statements to the body of statements, if no block is explicitly created, these functions will also created an implicit block around them.
- `dowhile` will implicitly create a block if the body has no block, but doesn't allow addition varaibles in its statement.

Note: All anonymous blocks outside of a function will not produce any scope!!!

Scopes and namespaces
---------------------

The scope of any declared identifier defines where in the code the identifier is defined.

Scopes are defined in terms of names blocks, these are blocks that introduce an explicit identifier into the scope.

::

	namespace-scope ::= { [ "::" ] <identifier> | <template-identifier> }
	accessor-scope ::= [namespace-scope "::"] { <identifier> | <template-identifier> ["."] }

Namespace
`````````

A namespace is similar to a block, but it defines a named scope.

::

	namespace-decl ::= "namespace" <identifier> <block-decl>

Any code within a file is explicitly wrapped inside the modules namespace, the identifier of this namespace will be the name of the package, followed by '.' and the name of the module. If no package is defined, this namespace will just have the modules name as the identifier.

Declarations
------------

A declaration binds a non-blank constant to a type, vairable, function, label, package or module. Every identifier in a program needs to have declaration. No single identifier may be declared in the same scope.

A `blank identifier` can be use like any other identifier in a declaration, but does itself not declare anything, useful to throw away unused values, when expanding certain types, like tuple types.


::

	declaration ::= ( <var-decl> | <type-decl> ) ";"

Blank identifier
`````````````````

The `Blank identifier` is a reserved symbol, represented using a single underscore `_`. This serves as a placeholder for unused values.

Uniqueness
``````````

Each declared identifier is required to be unique. An identifier is only unique if the identifier does not match the name of any other identifier in its scope. This means that 2 identifiers may have the same name, if they are in different Scopes

Type declarations
`````````````````

Type declarations allow the easy creations of different types that have the exact same representation as another type, but use a different identifier to be accessed.

::

	type-declaration ::= <type-alias> | <typedef>

Type alias
``````````

A type alias is a way of refering to a type, by the use of another identifier. Type aliases are still treated as if they are the same type. This makes it possible to pass a variable where the type is a type alias to be passed into expression that expect another alias of the same base type.

::

	type-alias ::= "typealias" [<template-parameters>] <identifier> "=" <type>

Typedef
```````

A typedef is similar to a type alias, but unlike an alias, the resulting type is interpreted as its own distinct type, separate from the original type. This means that any varaible where the type is a typedef, cannot be passed as a variable of an alias of the base type.

::

	<typedef> ::= "typedef" [<template-parameters>] <identifier> "=" <type>

Variable declaration
````````````````````

A variable has to be declared with a declaration::

	declaration ::= <explicit-declaration>
	              | <infered-declaration>

	explicit-declaration ::= <var-identifier> { "," <var-identifier> } ":" <type> [ "=" <expression> ] ";"
	implicit-declaration ::= <var-identifier> { "," <var-identifier> } ":=" <expression> ";"

	var-identifier ::= <type-modifier>* <identifier>

There are 2 types of declarations:

1. An explicit declaration, this means that the type of the value is explicitly declared, in this case, assigning a value is optional and if no value is assigned, a default value will be used. All variables declared in the same declaration have the declared type as `base` type.

2. An implicit delaration, with this declaration, the types of each variable will be defered, based on the expression used to initialize them. An implicit declaration also allows different types to be declared in a single declaration.

Examples::

	a : i32;
	b : i32 = 10;
	c := 5.f;

Property declaration
````````````````````
::

	property-get-set ::= "@:property" <method>

A property is a special way of accessing a value, which are indirectly used via getters and setters.

Properties can be accessed as if they were values in a structure, but these values will be indirectly accessed using a specified getter and setter. This also allows the property to only be either get or set, i.e. read- or write-only variables.

To create a getter or setter, a method needs to be created, annotated with the propery attribute and its identifier being the identifier used to access the property, prefixed either by `set_` or `get_`, if neither prefix is present, a compiler error will be shown.

For getters, the receiver can be constant or not, the former allowing this to be called on a const or immutable object, the latter requires it to be modifiable. The getter should also return the type of the object.

For setters, the receiver is required to be mutable. The method is also expected to have a single argument of the type of the property.

Function declaration
````````````````````

A function declaration defines an identifier as a function

::
	function ::= <attribute>* "func" <identifier> [<template-parameters>] <signature> [<template-constraint>] ( ";" | <function-body> )
	function-body ::= <block>

The function's signature declares the parameters and result. If a unnamed result is expected, the function needs to be terminated using a return statement. If a named result is expected, a return statement is not required, but all parameters of the named result must be assigned before exiting the function

::

	func Foo() -> int
	{
		// illegal, return statement is expected
	}

	func Bar() -> (n : int)
	{
		// illegal, n was never assigned
	}

A function may omit the body, but this requires the function statement to be terminated using `;`. This function is interpreted as an externally declared function.

::

	func FooBar(begin, end : *i32); // Implemented externally, required explict `foreign` attribute

Impl
^^^^
::

	impl-interface = <identifier> { "(" <interface-field-def> [","] ")" }
	impl ::= ( "impl" [<template-parameters>] <type> [ ":" { <impl-interface> }+ ] [<template-constraint>] "{" <statement>* "}" )

An `impl` is used to implement functionality for a type

When `impl` is used without specifically implementing and interface, it has no effect on the methods

::

	struct Str { i : int; }

	// using an impl
	impl Str
	{
		func (Str) Foo() { i := self.i; ... }
		func (Str) Bar() { ... }
	} 

	// is the same as
	func (str:Str) Foo() { i := str.i; ... }
	func (str:Str) Bar() { ... }

A `impl` is also used to implement interfaces, this is done by following the base type of receiver, for which to implement the interfaces, then a ':', followed by a list of interfaces to implement.

Example::

	// Any method_set implementing from IFile, is required to implement both `Read` and `Write`
	interface IFile
	{
		func(const &self) Read(b : Buffer) -> bool;
		func(&self) Write(b : Buffer) -> bool;
	}

	impl File : IFile
	{
		func(const &self) Read(b : Buffer) -> bool { ... } // Implementation of Read
		func(&self) Write(b : Buffer) -> bool { ... } // Implementation of Write
	}

	// Only partially implementing an interface is illegal
	impl IllegalFile : IFile
	{
		func(const &self) Read(b : Buffer) -> bool { ... } // Implementation of Read
		// Error, Write() has not been implemented
	}

Like explained in `Interface types`, all methods, without a default implementation, are required to be implemented.

Method declaration
``````````````````
::

	method ::= <attribute>* "func" [ <method-obj> ] <identifier> <signature> <function-body>
	method-receiver ::= "(" [ "&" ["const"] ] "self" ")"

While MJay does not have classes and doesn't allow methods to be declared inside of a struct, it does have a concept of methods. All methods are required to be implemented in their corresponding impl blocks.

Methods are a simple way to call functions as if they were part of a type. A method is declared similarly to a normal function, but with an argument added right after `func`, this value is the receiver. The receiver uses the keyword `self`, the characters before `self` notify the compiler how `self` should be accessed

1. `self`: Receiver is passed by value. Copy can be modified, but original object can't.
2. `&self`: Receiver is passed by reference. Object can be modified. No copy.
3. `const &self`: Receiver is passed by const reference. Object cannot be modified. No copy.

When a receiver is passed to a method as a value or a constant reference, this method will be implicitly marked as `const` and can be called on const varaibles.

When the specified receiver is a reference type. The method can not be called on a null object.

Any method can also be called as a function, to do this, the function needs to be called as if it was part of the namespace of its `Self` type::

	str : Str = ...;

	str.Foo();

	// can be called as
	Str::Foo(str);

Closures
````````

A closure defines an inline function.

::

	closure          ::= <closure-vals> "=>" [<closure-captures>] <block>
	closure-vals     ::= ( <identifier> | ( "(" { <identifier> [","] } ")" )
	closure-captures ::= "[" { <closure-capture> [","] } "]"
	closure-capture  ::= ( ["&"] identifier ) | "=" | "&"

A closure is defined by one or multiple values (multiple values are surrounded by parentheses), followed by a possible capture list, and a function body.

A capture list can contain value specific captures or global captures.

A value specific capture gives access to a single value inside of the closure. Depending on whether the capture was defined with `&`. Without `&`, the capture will either copy the value of the varaible, at the moment when the closure is created. If `&` is added, a reference to that varaible will be stored and the closure can use the value of the varaible at the moment the closure is executed. A reference to the value also allows the value to be modified.

A default closure defines the access to any, none explicitly declared capture.

- `=`, gives access to all variables in the current visible scope, values are copied at the moment the closure is created.
- `&`, gives access to all variables in the current visible scope, values stored as references
- `this`, gives access to all varaibles of the current aggregate, values stored as references

To be able to store a closure as a variable, the type of that variable should be a delegate type.

Expressions
-----------

An expression is the computation of a value by applying operands and function to operands.

Operand
```````

Operands are elementary values in an expression. An operand can be a litereal, a (qualifier) identifier, or a parenthesized expression.

::

	operand ::= <literal> | <qualified-iden> | ( "(" <expression> ")" )
	literal ::= <basic-lit> | <composite-lit>
	basic-lit ::= <int-lit> | <float-lit> | <rune-lit> | <string-lit>

Qualified identifier
````````````````````

A qualified identifier is an identifier, possibly prepended by a scope.

::

	qualified-iden ::= [ <scope> "." ] <identifier> | <template-identifier>

A qualified identifier can also point to the module scope, if a single `.` is prepended to the qualified name.

Composite literal
`````````````````

A composite literal is a literal that can represent larger data structures than the basic literals. They are use to represent struct, arrays and maps. They create a new value each time they are evaluated.

::

	composite-lit  ::= <struct-lit> | <array-lit> | <map-lit>
	struct-lit     ::= "{" { <struct-element> [","] } "}"
	struct-element ::= <identifier> ":" <element>
	array-lit      ::= "[" { <element> [","] } "]"
	map-literal    ::= "[" { <key> ":" <element> [","] } "]"

Composite literals require the type to explicitly included to be able to be assigned. The value of the elements need to be able to be assignable to the correct type, required by the variable, to be valid.

Aggregate literal
^^^^^^^^^^^^^^^^^
::
	aggr-lit      ::= <qualified-name> <aggr-lit-body>
	aggr-lit-body ::= "{" { <aggr-elem> [","] } "}"
	aggr-elem     ::= [ <identifier> ":" ] <expression>

An aggregate literal is used to initialize an aggregate

When the aggregate is a structure, the element identifier is optional, but can be used to initializes the values out of order::

	struct Type { x, y, z : i32 }

	s := Type{ x: 0, y: 1, z: 2 } // Initializes each element to its corresponding element
	s := Type{ 0, 1, 2 }          // Result in the same as above, but without explicitly specifying the element
	s := Type{ 0, 1, z: 2 }       // Result is same as above, but only the last element is explicitly specified
	s := Type{ 0, z: 2, y: 1 }    // Result is same as above, but elements are assigned out of order

	s := Type{ 0, y: 1, 2 } // Invalid, even though 'y' would normally also be the 2nd element, this still causes issues with the last element 
	s := Type{ 0, z: 1, 2 } // Invalid, the int literal 2 cannot be associated with an aggregate element

When the aggregate is an union, the element identifier is required::

	union Type { i : i32, f : f32 }

	s := Type { i: 1 }         // Initialized a union where 'i' is assigned
	s := Type { f: 1.0 }       // Initialized a union where 'f' is assigned
	s := Type { 1 }            // Invalid: cannot infer which element needs to be assigned a value
	s := Type { i: 1, f: 2.0 } // Invalid: cannot assign a value to different elements

Primary expressions
```````````````````

Primary expressions are used as operand for unary and binary expressions

::

	primary-expr ::= <operand>
	               | <cast>
				   | <index-expr>
				   | <slice-expr>
				   | <func-call>
				   | "(" <expression> ")"

Casts
`````
::

	<cast> ::= <explicit-cast> | <transmute> | <c-cast>

A cast a way to convert the value of 1 type to another. MJay supports 3 types of casts.

Implicit cast
^^^^^^^^^^^^^

An implicit cast happens whenever an operand that is given to an expression and the type is not the expected type. If any implicit conversions can be made from the given type to the expected type.

Note that implicit casts are limited to the following operations:

- Convertions between builtin types
- Convertions between a simple enum and its base type
- Convertions from null to the corresponding type

Explicit cast
^^^^^^^^^^^^^
::

	<explicit-cast> ::= <primary-expr> "as" <type>

An explicit cast is the main way of converting between types. These casts are explicitly defined using the `as` keyword. To be able to cast from one type to the other, either the convertion needs to happed between related types and interfaces, or an explicit cast operator between the types needs to be defined.

Transmute
^^^^^^^^^
::

	<transmute> ::= <primary-expr> "transmute" <type>

Transmute does a bitcast of the data from 1 type to the other. Both types are required to have the same size for a transmute to work.

C cast
^^^^^^
::

	<c-cast> ::= <primary-expr> "__c_cast" <primary-expr>

C cast is a special type of cast, explicitly reserved for the creation of bindings to C libraries, this allows for easier convertion, by not requiring explicit differentiation between `as` and `transmute`

- Note: Since there is a plan to create a more seamless integration between C and MJay, by adding a special import for C headers, this keyword will be depricated in future version and removed when 1.0 is reached. To prevent future issues, DO NOT use this keyword in normal MJay code.

Index expressions
`````````````````
::

	index-expr ::= <primary-expr> "[" Expression "]"

An index expression is used to extract a single value out of any type which has an index operator defined.

Slice expressions
`````````````````
::

	slice-expr ::= <primary-expr> "[" <slice-index> "]"
	slice-index ::= <expression> ":" <expression>

A slice expression allows a range to be extracted from any type which has a slice operator defined.

Operators
`````````

Operators combine operands into expressions

::

	<expression>  ::= <binary-expr> | <cond-expr> | <tern-expr>
	<cond-expr>   ::= <binary-expr> "??" <binary-expr>
	<tern-expr>   ::= <binary-expr> "?" <binary-expr> ":" <binary-expr>
	<binary-expr> ::= <unary-expr> | <expression> <binary-op> <expression>
	<unary-expr>  ::= <primary-expre> 
	                | ( <unary-pre-op> <primary-expr>) 
	                | ( <primary-expr> <unary-post-op> )

	binary-op     ::= <assign-op> | <logic-op> | <rel-op> | <add-op> | <mul-op>

	assign-op     ::= "=" | "+=" | "-=" | "*=" | "/=" | "%=" | "^^=" | "<<=" | ">>=" | "~=" | "|=" | "^=" | "&="
	logic-op      ::= "|" | "^" | "&" | "&&" | "||"
	rel-op        ::= "==" | "!=" | "<" | "<=" | ">" | ">="
	add-op        ::= "+" | "-"
	mul-op        ::= "*" | "/" | "%" | "^^" | "<<" | ">>" | "~"

	unary-pre-op  ::= "+" | "-" | "!" | "~" | "*" | "&" | "++" | "--"
	unary-post-op ::= "++" | "--"

Operator precedence
^^^^^^^^^^^^^^^^^^^

Each operator has its own precedence, this defines the importance of an operations and which operators need to be executed before other operators.

The highest precedence is given to anything which happens in a primary expression. After this, unary pre expression have the highest precedence, followed by the unary post operators::

	*p++ // is the same as *(p++)


Binary operators follow the following order of precedence
::

	Precedence   Operators
	    9         * / % ^^ << >>
	    8         + - ~
	    7         << >>
	    6         &
	    5         ^
	    4         |
	    3         == != < <= > >=
	    2         &&
	    1         ||

The second to last precedence are conditional expressions, i.e. the conditional `??` and the ternary operator

The lowest precedence goes towards any assignment operator

Order of evaluation
```````````````````



New expression
``````````````
::

	new-expr ::= "new" [ "(" { <expression> [","] } ")" ] <type> ( "(" { <expression> [","] } ")" | "[" <expression> "]" )

A new expression allows the allocation of a type on the heap.

Arguments that are given before the type, are used to call the allocator.

A `new` expression can allocate 2 types of memory.

1. If arguments are given between parentheses, the expression will allocate a single object of that type, the argument will be used in the called constructor.
2. If an expression is given between 2 brackets, the expression will allocate an array of that object.


Delete expression
`````````````````
::

	delete-expr ::= "delete" <expression>

A delete expression deallocates memory that was previously allocated using a `New expression`_.

If any memory, that wasn't allocated or null, is deleted, the expression will cause undefined behavior.

Intrinsic expressions
`````````````````````
::

	intrin-expr ::= <sizeof-intrin>
	              | <alignof-intrin>
				  | <typeid-intrin>

Intrinsic expression are expression which are computed at compile time and allow access to info about types and variables.

sizeof
^^^^^^
::

	sizeof-intrin ::= "sizeof" "(" <expression> ")"

The sizeof intrinsic retreives the size of the expression.

alignof
^^^^^^^
::

	alignof-intrin ::= "alignof" "(" <expression> ")"

The sizeof intrinsic retreives the alignment of the expression.

typeid
^^^^^^
::

	typeid-intrin ::= "typeid" "(" <expression> ")"

A typeid expression retrieves the type info of the expression

Type expression
```````````````
::

	type-expr ::= "#{" <type> "}"

A type expression is allows the use of types where it normally isn't allowed. This makes it quite useful when used in combination with intrinsics like `sizeof`

Statements
----------

Statements control the execution of the program

::

	<statement> ::= <declaration>
	              | <empty-statement>
	              | <label-statement>
	              | <expr-statement>
	              | <if-statement>
	              | <for-statement>
	              | <foreach-statement>
	              | <while-statement>
	              | <do-while-statement>
	              | <switch-statement>
	              | <return-statement>
	              | <break-statement>
	              | <continue-statement>
	              | <fallthrough-statement>
	              | <defer-statement>
	              | <goto-statement>
	              | <using-statement>
	              | <assert-statement>

Empty statement
```````````````

An empty statement, as the name implies, is empty and has no effect on the execution

::

	empty-statement ::= [";"]

Label statement
```````````````

A label statement defines a target that may be used by `goto`.

::

	label-statement ::= ":" <identifier> ":"

Expression statement
````````````````````

An expression statement represents any expression that is used in the program.

::

	expr-statement ::= <expression> ";"

If statement
````````````

'if' statements define the conditional execution of 1 or more braches, according to the value of an expression. If the value evaluates to true, the if branch will be executed, otherwise, if an else branch exists, the else branch is executed.

::

	if-statement ::= "if" "(" <expression> ")" <block> [ <else-statement> | <elif-statement> ]
	else-statement ::= "else" ( <if-statement> | <block> )
	elif-statement ::= "elif" "(" <expression> ")" <block> [ <else-statement> | <elif-statement> ]

The else branch can be 1 of 3 possiblities

1. No branch
2. A body
3. An elif statement

For statement
`````````````

A for statement defines a repeated execution of a block. 

::

	for-statement ::= "for" "(" ( <expression> | <declaration> ) ";" <expression> ";" <expression> ")" <block>

A for statement bases its execution on 3 expression. An initializer, a condition and a post or increment expression.

Any of these 3 expression is allowed to be empty.

At the start of the for loop, the initializer is executed, followed up by the actual loop. In the loop, the condition is first checked to decide whether to continur the loop, after which the body is executed and finally the post expression is executed, then the loop restarts.

Foreach statement
`````````````````

A foreach loop is similar to a for loop, but instead of manual expression, the foreach loop iterates all entries in a range.

::

	foreach-statement ::= "foreach" "(" <identifier-list> "in" <foreach-range> ")"
	foreach-range ::= <range> | <expression>

The 'rang' expression on the right of the value may be represented by following types: array, pointer to an array, slice, string, map or any object that implemented a range.

The range expression is evaluated at once at the beginning of the loop, depending on the type, the compiler may optimise the range expression to only execute what is required.

Below a table can be found containing the default values a range expression will return::

	Range expression                1st value       2nd value

	array or slice  a [n]E or []E   index   i  u64  a[i]       E
	string          s string        index   i  u64  character  rune
	map             m map           key     k  K    m[k]	   V

1. For an array or slice, the index of the element, starting at 0 and the value of the element are given. If only the index is used in the loop, the compiler can optimise the loop, so that the element is not indexed, so no unnecessarry access will happen.
2. For a string, the index of each unicode code point will be returned, combined with the code point for that character. If no valid code point is found at that location, the second value will be equal to 0xFFFD, the Unicode replacement character
3. For a map, the iteration order is not specified or guaranteed, but any map should return the key and its corresponding value in the map.
4. For any other type, the return values, solely depend on the implementation of the generated range

While statement
```````````````

A while statement is comparable to a for statement, but only including the condition for which the loop will be required to run.

::

	while-statement ::= "while" "(" <expression> ")" <body>

Do while statement
``````````````````

A do while statement is close to identical to a while loop, with the exception that the do-while loop will be guaranteed to run at least one iteration before the condition is checked.

::

	do-while-statement ::= "do" <body> "while" "(" <expression> ")" ";"

Switch statement
````````````````

A switch statement provides a way of providing multiple execution paths depening on a single value.

::

	switch-statement ::= "switch" "(" <expression> ")" "{" { <case-clause> } "}"
	case-clause ::= <switch-case> ":" <statement>*
	switch-case ::= ( "case" <expression> | <case-type-expr> ) | "default"
	case-type-expr ::= ( <type> [<identifier>] ) | ( <type> { "," <type> } ) 

Each case will automatically terminate when reaching the end of the supplied statements, so no explicit break is required. To be able to execute the next case clause after reaching the end, a `Fallthrough statement`_ can be used.

A switch case can operate on 2 different values: expressions and types.

Expression switches
^^^^^^^^^^^^^^^^^^^

In the case that the switch works on an expression, the expression will be evaluated, followed up with all cases expression, which are not required to be constant, in a left-right and top-bottom order until the first matching case is found. The statements in the found case will than be executed. If no matching case clause is found, the default case clause will be executed, when present. There can be at most 1 default case clause in a switch statement.

If a case has an untyped expression, this expression will be converted to the type of the switched expression.

For each case expression, the value needs to be comparable using the `==` operator.

It is not allowed to have 2 constant values evaluating to the same value in a switch statement.

Type switches
^^^^^^^^^^^^^

A type switch compares types rather than values. A type switch is represented similarly to a expression switch, but all cases are formed by types, instead of expressions.

A `fallthrough` is not permitted in a type switch.

Return statement
````````````````

A return statement terminates the function it is called in, and optionally provides 1 or more result values

::

	return-statement ::= "return" [<comma-expr>] ";"

Return may only have values when this is specified by the function signature.

Return allows single and multi value returns, when a multi value return is used, the values will be implicitly packed into a tuple

Continue statement
``````````````````

A continue statement allows the user to skip towards the next itteration of the innermost loop, without executing code that comes after the continue in the loop body.

::

	continue-statement ::= "continue" ";"

Break statement
```````````````

A break statement allows the user to exit the innermost loop, without executing code that comes after the break in the loop body.

::

	break-statement ::= "break" ";"

Goto statement
``````````````

A goto statement allows a jump to an arbitrary location in code that is marked by a `Label statement`_, within the same function

::

	goto-statement ::= "goto" <identifier> ";"

Fallthrough statement
`````````````````````
::

	fallthrough-statement ::= "fallthrough" ";"

A fallthrough statement transfers control the to the first statement in the next case clause in a `Switch statement`_

Defer statement
```````````````

A defer statement invokes a function on the when execution is defered or in other words, the moment that the surrounding function returns.

::

	defer-statement ::= "defer" ( <expression> | <func-decl> ) ";"

The expression must be a function call.

Each time a defer statement is executed, the function and its arguments are stored, but the function is not executed. Instead, the expression is kept until the function returns and right before the function is returned and the destructors are executed, the defer expressions are executed in the reversed order that they were declared.

Using statement
```````````````

A using statement extracts the data of an aggregate and adds it to the aggregate it is declared in.

::

	using <type> ";"

Assert statement
````````````````

An assert statement allows MJay to check expression at runtime. Immediatally breaking or terminating the program, depending if a debugger is attached, is the value evaluates to false. Assert works unlike `panic`, the does not do any cleanup and expects the OS to handle this.

::

	assert-statement ::= "assert" "(" <expression> ")" ";"

Packages and modules
````````````````````

A MJay program is constructed by linking modules together. Each module can consist out of multiple different source files and includes all their constants, types, varaibles and functions.

Next to modules, packages also exist, a package consists out of 1 or more modules. A package is an easy way of grouping related modules. Packages count as a type of namespace for existing modules, since conflicting module identifiers will cause issues, moving them into their own distinctive package will avoid these issues.

Source file
```````````

Each source file consists of an optional package and module clause, followed up by any statement.

::

	source-file ::= [<package-clause>] [<module-clause>] [<decl>*]

Package clause
``````````````

The package clause defines to which package the current source file belongs

::

	package-clause ::= "package" <identifier> { "." <identifier> } ";"

When a package is not defined, it will use the default package given by the compiler, which can be user set, but is set by default to nothing.

Module clause
`````````````

The module clause defines to which module in the package the current source file belongs.

::

	module-clause ::= "module" <identifier> ";"

When a package is not defined, it will use the default package given by the compiler, which can be user set, but is set by default to `main`.

Import declaration
``````````````````

An import declaration allows the use of external symbols in this module

::

	import-decl ::= ["public"] ["static"] "import" ["package"] <qualified-name> [ "as" <identifier> ] [ ":" <import-symbols> ]
	import-symbols ::= { <qualified-name> ["as" <identifier>] [","] }+

Import declarations are in no way required to be global in the file and may be declared inside any scope. When looking up a symbol, if the symbol is not defined in the module, the compiler will search symbols from the inner most block to the outer most block ( or file scope ).

When importing a module, it is possible to assign an short name to the module, which can be used throughout the entire file.

It is also possible to import only certain symbols from a module, by defining the symbols to be used after a ':'. Each of these symbols can also be given an alias.

Instead of just importing a single module, it is posssible to import an entire package, by using `import package`.

An import can also have 2 modifiers.

1. `public` allows the symbols that are included in that statement, to also be exported with the module, forcing the import of these symbols of this module is imported somewhere.`public` is only allowed in file scopes.
2. `static` allows the use of the imported symbols, but the entire qualified name is required to be able to use this symbol.

Module constructor and destructors
``````````````````````````````````

In the module scope, it is allowed to create a constructor and destructor. 

::

	module-ctor ::= ["shared"] "static" "this" "(" ")" <block>
	module-dtor ::=["shared"] "static" "~" "this" "(" ")" <block>

These are both required to be static and the constructor cannot contain any parameters. Each one allows a shared and non-shared version to exist. The `shared` rules for constructors and destructors in an aggregate also applied when it comes to the module constructors and destructors.

While the constructors and destructors can be declared in multiple files, there is no guarentee in which order these will be executed.

Templates and compile-time execution
------------------------------------

Templates
`````````

Templates enables the ability to have generic code that the compiler can use for any type that fits certain constraints

::
	
	template-parameters ::= "<" { <template-parameter> [","] } ">"
	template-parameter ::= <template-type-parameter>
	                     | <template-value-parameter>
	                     | <template-variadic-parameter>

	template-type-parameter ::= <identifier> [ <template-type-parameter-specialization> | <template-type-parameter-default> ]
	template-type-parameter-specialization ::= ":=" <type>
	template-type-parameter-default ::= "=" <type>

	template-value-parameter ::= <identifier> : <type> [ <template-value-parameter-specialization> | <template-value-parameter-default> ]
	template-value-parameter-specialization ::= ":=" <expression>
	template-value-parameter-default ::= "=" <expression>

	template-variadic-parameter ::= <identifier> "..."

	template-constraint ::= "if" "(" <expression> ")"

	template-identifier ::= <identifier> ( ( "!" <template-argument> ) | ( "!<" { <template-argument> [","] } ">"
	template-argument ::= <type> | <expression>

Template parameters can represent a 2 different types:

1. Type parameters: Represents a type in the template
2. Value parameters: Represents a constant or literal at compile time

Value parameters are allowed to be any of the following types:

- Integer type
- Floating point types
- Boolean type
- String type

Template specialization
^^^^^^^^^^^^^^^^^^^^^^^
Template specialization allows a different implementation, depending on the types given during the instantiation of the template.

Template collisions
^^^^^^^^^^^^^^^^^^^
When any specialization has occured, either partial of fully, there will likely be collisions, these will be solved with the following steps:

- If no specialization matches the instance, the unspecialized symbol will be used
- If 1 specialization matches the instance, that specializes symbol will be used
- If multiple specializations match the instance, it can be either of the following sub-cases:
	- If a full specialization matches the instance, that specialized symbol will be used
	- If no full specialization matches the instance, currently, an error wil be produced

Default template parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Templates allow the parameters to be assigned default values. Like function parameters, a parameter with a default value requires the following parameters to also have default values.

Each template or specialization may not have multiple version with different default values.

::

	struct Foo<T, U> { ... }
	struct Foo<T, U = T> { ... } // Invalid, template already declared

	struct Bar<T:=#{i32}, U = T> {...} // Valid, default value when template is specialized
	struct Bar<T:=#{i32}, U = #{f32}> {...} // Valid, template specialization already declared

There is an excpetion to template defaults. When template parameters are defined with default values, it is allowed to have template parameters without defaults, if these can be inferred.

::

	struct Foo<T, U = #{i32}, V> {...} // Invalid, V cannot be inferred
	struct Foo<T := []V, U = #{i32}, V> // Valid, since V can be inferred from T

While inferring value parameters is currently also possible, there are certain limitations. Currently value parameters can only be inferred from array types, specifically their size. This restriction may change in the future.

For a value parameter to be infered, the value parameter may not be part of an expression of any kind:

::

	struct Foo<T := #{[N]i32}, N : usize> // Valid, since N is not part of an expression
	struct Foo<T := #{[N + 1]i32}, N : usize>	// Invalid, since N is part of an expression


Compile-time statements
```````````````````````

Debug statement
^^^^^^^^^^^^^^^
::

	debug-statement ::= "#:debug" ["(" <identifier> ")"] <body> [ ["else"] <body> | <debug-statement> ]

A debug statement only allows code to be compiled when in a debug build, and when an optional identifier is defined.

Additional debug identifier can defined by using a `Debug expression`_

Version statement
^^^^^^^^^^^^^^^^^
::

	version-statement ::= "#:version" "(" <identifier> ")" <body> [ ["else"] <body> | <debug-statement> ]

A version statement only allows code, contained in its body, to be compiled when a specific identifier is set

Additional version identifier can defined by using a `Version expression`_

Compile-time statements
````````````````````````
::

	compile-time-statement ::= ( "#" <if-statement> | <for-statement> | <foreach-statement> | <assert-statement> )
	                         | <debug-expression>
	                         | <version-expression>

Debug expression
^^^^^^^^^^^^^^^^
::

	debug-expr ::= "#:debug" "=" <identifier>

Allows the user to set an additional value for debug statement to check against.

Version expression
^^^^^^^^^^^^^^^^^^
::

	debug-expr ::= "#:version" "=" <identifier>

Allows the user to set an additional value for debug statement to check against.

Compile-time expression
^^^^^^^^^^^^^^^^^^^^^^^
::

	compile-time-expr ::= "#" <expression>

Defines that an expression should run during compile time instead of at runtime.

Unit testing
------------

MJay provides a builtin system to unittest code, using the unit test statement. 

This require the project to be compiled using the unit test argument. Unittests are skipped when the code is compiled without the unit test argument.

When unittest are enabled, the code gets compiled as normal, and all visible unittest will be added to the unit test framework. When a unit test is not defined, it will be added as an auto numbered unittest. 

::

	unit-test-statement ::= "unittest" [<identifier>] <body>

Attributes and modifiers
------------------------

Visibility attributes
`````````````````````
Visibiity attributes give additional information about the locations the symbol can be accessed

::

	visibility-attributes ::= "private"
	                        | "internal"
							| "package"
							| "public"
							| "export"

private
^^^^^^^

`private` symbols followed either one of the following rules

- If the symbol is a member of an aggregate, the member can only be accessed by it's own methods
- If the symbol is a method, the method can only be called from other methods of the same aggregate
- If the symbol is part of an interface, the symbol can only be used by its 'derived' aggregates
- Any other symbol can only be accessed from the same file in which it is defined

internal
^^^^^^^^

`internal` symbols can only be accessed from the module they are declared in

package
^^^^^^^

`package` symbols can only be accessed from the inner package they are declared in

public
^^^^^^

`public` symbols can only be accessed from any module that statically includes the owning module

This requires the symbol to be available during linking.

export
^^^^^^

`export` symbols can only be accessed from any module that statically or dynamically includes the owning module

When the symbol is not available during link time, the symbol will be used from a dynamically linked module. The symbol however needs to be declared, but not defined, during link time

Variable modifiers
``````````````````

Variable modifiers give additional information about the type and how the type is expected to be used, when a variable is declared.

::

	type-modifiers ::= "const"
	                 | "cconst"
	                 | "immutable"
	                 | "mutable"
	                 | "static"
	                 | "shared"
	                 | "synchronized"
	                 | "__global"
	                 | <attribute>+

const
^^^^^

The `const` modifiers makes sure that a variable cannot be changed after it's assignment. This does not however mean that the value of the underlying type cannot be changed, but just that this reference to the value is guarenteed not the change the variable.

cconst
^^^^^^

The `cconst` modifier means that the type is a compile-time constants and that its value will be guarenteed to be known at compile time and will not be able to change. This modifier also allows the variable to be used during the compilation and may allow for better compile-time optimizations during the compilation process.

immutable
^^^^^^^^^

The `immutable` modifier guarentees that, once the variable is assigned, the underlying value cannot be changed, unlike `const`, where the underlying value may be changed via other references to this value. Casting an immutable value to any non-const variable is therefore illegal.

mutable
^^^^^^^

The `mutable` modifier allows a value to be changed, regardless of the guarenteed safety applied by functions. The compiler therefore not make any optimizations for this variable.

static
^^^^^^

The `static` modifier allows the content of the variable to be accessed, even without making an instance of the type in which this variable is in. If a static variable exist in a function, it is guarenteed that the value will only be initialized on the first call of that function.

shared
^^^^^^

The `shared` modifier lets the variable become atomic and allow it to be used with atomic operations, instead of the standard operations. This modifier is only allowed on certain builtin types.

synchronized
^^^^^^^^^^^^

The 'synchronized' modifier allows the varaible to be safely used on multiple different threads, this is done by the implicitly surrounding any access to the varaible with a mutex.

__global
^^^^^^^^

The `__global` modifier defines the variable globally in the created executable, instead of creating the variable in thread-local space, as is done by default.

Parameter modifiers
```````````````````
Parameters modifiers give additional information about the type and how the type is expected to be used, when a parameter is declared.

::

	type-modifiers ::= "const"
	                 | "cconst"
	                 | "lazy"
	                 | <attribute>+

const
^^^^^

The `const` modifiers makes sure that a variable cannot be changed after it's assignment. This does not however mean that the value of the underlying type cannot be changed, but just that this reference to the value is guarenteed not the change the variable.

cconst
^^^^^^

The `cconst` modifier means that the type is a compile-time constants and that its value will be guarenteed to be known at compile time and will not be able to change. This modifier also allows the variable to be used during the compilation and may allow for better compile-time optimizations during the compilation process.

lazy
^^^^

The `lazy` modifier will not process any functionality of the supplied argument, but only do this when the variable is actually used. I.e. the calculation of the argument will only be calculated on the first use of the paramenter.

Compiler attributes
```````````````````

There are a number of different attributes that allow additional information to be given to the compiler, to modify how the compiler will handle certain stages of the compilation. Compiler attributes are defined very similar to normal attributes, but these need to start with `@:`, instead on a normal `@`. The allowed attributes is also limited, since these are specific for the compilation and by extend are not able to be user defined.

::

	compiler-attribute ::= "@:" <identifier> [ "(" <arguments> ")" ]

Below are the definitions of all current compiler attributes that exist:

align
^^^^^
::

	align-compiler-attribute ::= "@:" "align" "(" <int-lit> ")"

Align the value by on a certain byte boundry. The given alignment has to be a power of 2.

If align is used on a function, the implementation will be aligned to the given alignment, but the parameters aren't.

Allowed on: Aggregates, functions, namespaces, blocks and varaibles.

dynamic_load
^^^^^^^^^^^^
::

	extern-compiler-attribute ::= "@:" "dynamic_load"

Define that the symbol references a variable declared in an dynamic library.

Allowed on: functions, namespaces and blocks.

foreign
^^^^^^^
::

	foreign-compiler-attribute ::= "@:" "foreign" [ "(" <string-literal> ")" ]

Define that the symbol references an foreign (external) variable. Optionally, the library containing the definitions can be defined and will automatically be linked to when generating the result binaries.

Allowed on: aggregates, functions, namespaces and blocks.

link_name
^^^^^^^^^
::

	link-name-compiler-attribute ::= "@:" "link_name" "(" <string-lit> ")"

Define the link name that will be used during compile time, instead of using the standard mangled name. The name given to the attribute needs to adhere to the standard naming rules

Allowed on: functions.

call_conv
^^^^^^^^^
::

	call-conv-compiler-attribute ::= "@:" "call_conv" "(" <string-lit> ")"

Define the calling convention that will be used during compile time, instead of using the standard calling convention.

The calling convetion is only allowed to be one of the following:

- MJay
- C
- Windows

Allowed on: functions.

expose_core
^^^^^^^^^^^
::

	expose-core-compiler-attribute ::= "@:" "expose_core" "(" <string-lit> ")"

Exposes functionality from the core using an alias. This attribute requires the qualified name of the core function to expose.

This attribute is only allowed in the std package.

Allowed on: functions.



func_ptr
^^^^^^^^
::

	func-ptr-attribute ::= "@:" "func_ptr"

Passes the given delegate to the callee as a raw function ptr.

This attribute is most useful to pass a delegate to an external C function.

Allowed on: delegates.

arr_ptr
^^^^^^^
::

	arr-ptr-attribute ::= "@:" "arr_ptr"

Passes the given dynamically size array to the callee as a raw ptr.

This attribute is most useful to pass a dynamically size array to an external C function.

Allowed on: array types.

default_visibility
^^^^^^^^^^^^^^^^^^
::

	default-visibility-attribute ::= "@:" "default_visibility" "(" <string-lit> ")"

Sets the default visibility for all symbols in the module.

Allowed on: Module.

allow_extension
^^^^^^^^^^^^^^^
::

	allow-extension-attribute ::= "@:" "allow_extension"

Allows extensions (methods) to be implemented for a type outside of its parent module

Allowed on: Struct

extend_builtin
^^^^^^^^^^^^^^
::

	extend-builtin-attribute ::= "@:" "extend_builtin"

Adds an extension to a builtin type

Allowed on: Impl

Note: This attribute is not meant to be used by the user, but it is used in the development of the core and std library. Use of this attribute in user code may result in undefined behavior.

Overloadable operators
----------------------

Most operators are overloadable, to overload any operator, the structure needs to implement the corresponding interface.

Unary operators
```````````````
Unary operators take in 1 arguments and returns a value that may be of another type.
Unary operators can both be prefix or postfix operators, postfix operators are followed by a P

============== ============================ =================== =========
 Op Interface         Func Signature            Description      Example
============== ============================ =================== =========
 OpPlus         (self) opPos() -> Result     positive            `+a`
 OpMinus        (self) opNeg() -> Result     negative            `-a`
 OpInc          (self) opInc() -> Result     prefix increment    `++a`
 OpIncP         (self) opIncP() -> Result    postfix increment   `a++`
 OpDec          (self) opDec() -> Result     prefix decrement    `--a`
 OpDecP         (self) opDecP() -> Result    postfix decrement   `a--`
 OpNot          (self) opNot() -> Result     not                 `!a`
 OpNeg          (self) opNeg() -> Result     negation            `~a`
 OpDeref        (self) opDeref() -> Result   dereference         `*a`
============== ============================ =================== =========

Binary operators
````````````````
Binary operators take in 2 arguments, possibly of different types, and may returns a value that may be of another type

============== ======================================= =================== =========
 Op Interface              Func Signature                  Description      Example
============== ======================================= =================== =========
 OpAdd          (self) opAdd (other:Rhs) -> Result      addition            `a + b`
 OpSub          (self) opSub (other:Rhs) -> Result      subtraction         `a - b`
 OpMul          (self) opMul (other:Rhs) -> Result      multiplication      `a * b`
 OpDiv          (self) opDiv (other:Rhs) -> Result      division            `a / b`
 OpMod          (self) opMod (other:Rhs) -> Result      modulus             `a % b`
 OpConcat       (self) opConcat (other:Rhs) -> Result   concatination       `a ~ b`
 OpBitOr        (self) opBinOr (other:Rhs) -> Result    binary or           `a | b`
 OpBitXor       (self) opBinXor (other:Rhs) -> Result   binary xor          `a ^ b`
 OpBitAnd       (self) opBinAnd (other:Rhs) -> Result   binary and          `a & b`
 OpShl          (self) opShl (other:Rhs) -> Result      shift left          `a << b`
 OpShr          (self) opShr (other:Rhs) -> Result      shift right         `a >> b`
============== ======================================= =================== =========

Assignment operators
````````````````````
Assignment operators take in 2 arguments, possibly of different types, and assigns the result to the first value and return the value.

================ ======================================== =================== =========
 Op Interface              Func Signature                    Description       Example
================ ======================================== =================== =========
 OpAddAssign      (&self) opAdd (other:Rhs) -> Result      addition            `a + b`
 OpSubAssign      (&self) opSub (other:Rhs) -> Result      subtraction         `a - b`
 OpMulAssign      (&self) opMul (other:Rhs) -> Result      multiplication      `a * b`
 OpDivAssign      (&self) opDiv (other:Rhs) -> Result      division            `a / b`
 OpModAssign      (&self) opMod (other:Rhs) -> Result      modulus             `a % b`
 OpConcatAssign   (&self) opConcat (other:Rhs) -> Result   concatination       `a ~ b`
 OpBitOrAssign    (&self) opBinOr (other:Rhs) -> Result    binary or           `a | b`
 OpBitXorAssign   (&self) opBinXor (other:Rhs) -> Result   binary xor          `a ^ b`
 OpBitAndAssign   (&self) opBinAnd (other:Rhs) -> Result   binary and          `a & b`
 OpShlAssign      (&self) opShl (other:Rhs) -> Result      shift left          `a << b`
 OpShrAssign      (&self) opShr (other:Rhs) -> Result      shift right         `a >> b`
================ ======================================== =================== =========

Comparison operators
````````````````````
Comparison operators take in 2 arguments, possibly of different types, and return a boolean value

============== ======================================== =================== =========
 Op Interface               Func Signature                  Description      Example
============== ======================================== =================== =========
 OpEq           (self) opEq (other:Rhs) -> bool          equals              `a == b`
 OpNEq          (self) opNEq (other:Rhs) -> bool         not equal           `a != b`
 OpLess         (self) opLess (other:Rhs) -> bool        less                `a < b`
 OpLessEq       (self) opLessEq (other:Rhs) -> bool      less or equal       `a <= b`
 OpGreater      (self) opGreater (other:Rhs) -> bool     greater             `a > b`
 OpGreaterEq    (self) opGreaterEq (other:Rhs) -> bool   greater or equal    `a >= b`
============== ======================================== =================== =========

Other operators
```````````````
============== ============================================= =================== =========
 Op Interface               Func Signature                   Description      Example
============== ============================================= =================== =========
 OpCast         (self) opCast () -> Result                    explicit cast       `a as T`
 OpIndex        (self) opIndex (index:Idx) -> Result          indexing            `a[i]`
 OpSlice        (self) opSlice (from:Idx, to:Idx) -> Result   slicing             `a[f:t]`
============== ============================================= =================== =========



