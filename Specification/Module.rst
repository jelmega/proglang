::

     ___      ___       ___      __       ___  ___  
    |"  \    /"  |     |"  |    /""\     |"  \/"  | 
     \   \  //   |     ||  |   /    \     \   \  /  
     /\\  \/.    |     |:  |  /' /\  \     \\  \/   
    |: \.        |  ___|  /  //  __'  \    /   /    
    |.  \    /:  | /  :|_/ )/   /  \\  \  /   /     
    |___|\__/|___|(_______/(___/    \___)|___/      

=========================
Module file Specification
=========================

Introduction
------------

An MJay module file is comparable to a .lib, .a or .so file during the compilation process. This file contains all exported symbols for a module.


File structure
--------------

Magic number
````````````

Each module file start using a magic number. The magic number consists out of 4 bytes.

::

    x80 'M' 'J' 'M'

The first byte contains a value outside of the ascii range. This is than followed up by MJM, which stands for MJay Module.

Header
``````

The file header defines the general meta data of the module and defines what sections are part of the library

The following table defines the components and byte width of each element

=============== ======= ===================================
   name        bytes     Description
=============== ======= ===================================
File size          4     Size of the file (including magic)
Flags              2     Flags (see below)
Package name       N     Name of package (0-term)
Module name        N     Name of module (0-term)

Sym table count    2     Number of symbol table entries
 IL file count     2     Number of IL bytecode file entries
 Obj file count    2     Number of object file entries
=============== ======= ===================================

Flags
^^^^^

The flags consists out of 2 bytes, giving additional info about the file, they are as following

===== ====================================================
 bit   description 
===== ====================================================
  0   Reserved
  1   Reserved
 2-3  LTO level (0: machine code, 1: IR, 2: IL, 3: Raw IL)
  4   This file is static only
  5   This file is dynamic only
  6   Reserved 
  7   Reserved 
  8   Contains templates
  9   Reserved
  10  Reserved
  11  Reserved
  12  Reserved
  13  Reserved
  14  Reserved
  15  Includes IL Template Byte Code
===== ====================================================


File data
---------

============ ======= ===================================
   name       bytes     Description
============ ======= ===================================
  Tables        ?     Symbol tables
Obj files       ?     Object files
============ ======= ===================================

Symbol table
````````````

============ ======= ===================================
   name       bytes     Description
============ ======= ===================================
table iden      4     Table identifier (0-term)
entry count     4     Number of entries in the table
entries         ?     Table entries
============ ======= ===================================

table identifiers
`````````````````
============ ===================================
 identifier   Description
============ ===================================
   exp\0      Export table (dll/so symbols)
   pub\0      Public symbols
   pak\0      Package symbols
   imp\0      Import table
   str\0      Structure table
   int\0      Interface table
   uni\0      Union table
   enu\0      Simple enum table
   adt\0      ADT enum table
   ali\0      Type alias table
   glo\0      Global values
   del\0      Delegates
   atr\0      Attribute
============ ===================================


Symbol entry
````````````
entry for: exp, pub, pak, imp, glo, del (might possibly change)
============ ======= ==========================================
   name       bytes     Description
============ ======= ==========================================
info            4     Symbol info
identifier      ?     Identifier (mangled name) (0-term)
Mangled Type    ?     Mangled type (optional, flags)
dyn/obj iden    ?     Dynamic/object library name (0-term)
============ ======= ==========================================

Type entry
``````````
entry for: str, uni, enu, adt
============ ======= ==========================================
   name       bytes     Description
============ ======= ==========================================
    info        4     Symbol info
 identifier     ?     Identifier (mangled name) (0-term)
Num children    4     Number of children/members
  children      ?     Symbol children
============ ======= ==========================================

Symbol child
````````````
============ ======= ==========================================
   name       bytes     Description
============ ======= ==========================================
    info        4     Symbol info
 identifier     ?     Identifier (mangled name) (0-term) 
============ ======= ==========================================

Enum table entry
````````````````
entry for: enu, adt
============ ======= ==========================================
   name       bytes     Description
============ ======= ==========================================
    info        4     Symbol info
 identifier     ?     Identifier (mangled name) (0-term)
Num children    4     Number of members
  children      ?     Enum members
============ ======= ==========================================

Enum member
```````````
============ ======= ==========================================
   name       bytes     Description
============ ======= ==========================================
 identifier     ?     Identifier (mangled name) (0-term) 
   value        8     Enum value/ADT index
  adt init      ?     Adt init identifier (0-term)
============ ======= ==========================================

Interface table entry
`````````````````````
entry for: int
============ ======= ==========================================
   name       bytes     Description
============ ======= ==========================================
    info        4     Symbol info
 identifier     ?     Identifier (mangled name) (0-term)
Num children    4     Number of children/members
  children      ?     Symbol children
============ ======= ==========================================

Symbol info
```````````
============ ====== =======================================
   name       bits   Description
============ ====== =======================================
   Flags        8    Flags
Attrib index   24    Attribute table index
============ ====== =======================================

Symbol flags
````````````
===== ====================================================
 bit   description 
===== ====================================================
  0    Unmangled type (C-style symbol) (type separated)
  1    External
  2    Reserved
  3    Reserved
  4    Reserved
  5    Reserved
  6    Reserved 
  7    Reserved 
===== ====================================================

Attribute entry
```````````````
============ ======= ==========================================
   name       bytes     Description
============ ======= ==========================================
   flags        2     Basic flags
============ ======= ==========================================

Attribute flags
```````````````
===== ====================================================
 bit   description 
===== ====================================================
  0    Const
  1    CConst
  2    Immutable
  3    Reserved
  4    Reserved
  5    Reserved
  6    Reserved
  7    Reserved
  8    Delegate as function pointer
  9    Array as pointer
  10   Stack array
  11   Reserved
  12   Reserved
  13   Reserved
  14   Reserved
  15   Contains other data then flags
===== ====================================================

Bytecode file
`````````````

A bytecode file can be any of the following files
- IL Bytecode
- Machine code (object file)

============ ======= ===================================
   name       bytes     Description
============ ======= ===================================
obj iden        ?     Object library name (0-term)
obj size        4     Size of object file
object data     ?     Object file data
============ ======= ===================================

