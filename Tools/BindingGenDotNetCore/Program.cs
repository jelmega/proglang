﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;

namespace BindingGen
{
	class Program
	{

		private static List<string> _files = new List<string>();
		private static List<string> _defExclPrefixes = new List<string>();
		private static string _callConv = null;
		private static string _module = "bindings";
		private static string _baseHierarchy = "";
		private static bool _parseDefines = false;
		private static bool _mayContainLowerCase = false;

		static void Main(string[] args)
		{
			// Parse args
			ParseArgs(args);

			if (_files.Count == 0)
				return;

			// Gen XML
			foreach (string file in _files)
			{
				string castXmlArgs = $"--castxml-output=1 \"{file}\"";
				Process castXmlProcess = new Process();
				castXmlProcess.StartInfo.FileName = "castxml/bin/castxml.exe";
				castXmlProcess.StartInfo.Arguments = castXmlArgs;
				castXmlProcess.Start();
				castXmlProcess.WaitForExit();
			}

			/*CodeGenerator codeGen = new CodeGenerator();
			codeGen.GenPackageAndModule(moduleName);
			codeGen.CallingConvention = callingConvention;*/

			XMLProcess process = new XMLProcess();
			DefineProcessor defineProcessor = new DefineProcessor();

			foreach (string file in _files)
			{
				string xmlFile = Path.ChangeExtension(Path.GetFileName(file), "xml");
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.Load(xmlFile);

				var codeGens = process.ProcessXML(xmlDoc, _baseHierarchy, _module, _callConv);

				//defineProcessor.GenerateConstants(file, defPrefixes);



				foreach (KeyValuePair<string, CodeGenerator> pair in codeGens)
				{
					string headerPath = _baseHierarchy + pair.Key;

					if (_parseDefines)
						defineProcessor.GenerateConstants(headerPath, pair.Value, _defExclPrefixes, _mayContainLowerCase);

					pair.Value.Shutdown();
				} 
			}

		}


		static void ParseArgs(string[] args)
		{
			foreach (string arg in args)
			{
				if (arg.StartsWith('-'))
				{
					if (arg.StartsWith("-defprefixes="))
					{
						string line = arg.Substring("-defprefixes=".Length);

						string[] prefixes = line.Split(',');
						_defExclPrefixes.AddRange(prefixes);
					}
					else if (arg.StartsWith("-callconv="))
					{
						_callConv = arg.Substring("-callconv=".Length);
					}
					else if (arg.StartsWith("-module="))
					{
						_module = arg.Substring("-module=".Length);
					}
					else if (arg.StartsWith("-basehierarchy="))
					{
						_baseHierarchy = arg.Substring("-basehierarchy=".Length);
					}
					else if (arg == "-parsedefines")
					{
						_parseDefines = true;
					}
					else if (arg == "-definesmaybelower")
					{
						_mayContainLowerCase = true;
					}
					else if (arg == "-winheaders") // builtin windows define constants
					{
						_module = "core.os.win32";
						_callConv = "Windows";
						_parseDefines = true;
						_defExclPrefixes = new List<string>
						{
							"_",
							"WINABLE",
							"WINVER",
							"WINNT_",
							"WINUSER",
							"NOAPISET",
							"NOUSER",
							"STRICT",
							"UNICODE",
							"NOICONS",
							"RC_INVOKED",
							"GET_",
							"MAKE",
							"WINAPI",
							"EXTERN_C",
							"FAR",
							"DECLSPEC_",
							"CDECL",
							"CALLBACK",
							"API",
							"PASCAL",
							"CALLBACK",
							"NEAR",
							"CONST",
							"NULL",
							"WINOLEAPI",
							"VOID",
							"TRUE",
							"FALSE",
						};
					}
				}
				else if (arg.EndsWith('/') || arg.EndsWith('\\'))
				{
					foreach (string file in Directory.EnumerateFiles(arg))
					{
						if (file.EndsWith(".h"))
							_files.Add(file);
					}
				}
				else
				{
					_files.Add(arg);
				}
			}
		}

	}
}
