﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BindingGen
{
	public class CodeGenerator
	{
		//public string Code = "";

		private FileStream _stream;
		private StreamWriter _writer;

		public string CallingConvention;

		private int _indent = 0;

		public CodeGenerator(string filename)
		{
			string dir = Path.GetDirectoryName(filename);
			if (!Directory.Exists(dir))
				Directory.CreateDirectory(dir);

			_stream = new FileStream(filename, FileMode.Create);
			_writer = new StreamWriter(_stream);
		}

		public void Shutdown()
		{
			_writer.Flush();

			_writer.Close();
			_writer.Dispose();

			_stream.Close();
			_stream.Dispose();

			GC.SuppressFinalize(this);
		}

		public void GenPackageAndModule(string module)
		{
			int moduleSpit = module.LastIndexOf('.');

			if (moduleSpit == -1)
			{
				_writer.WriteLine($"module {module};");
			}
			else
			{
				_writer.WriteLine($"package {module.Substring(0, moduleSpit)};");
				_writer.WriteLine($"module {module.Substring(moduleSpit + 1)};");
			}
		}

		public void BeginStruct(string name, bool field)
		{
			if (string.IsNullOrEmpty(name))
				_writer.WriteLine($"{GetIndent()}struct {{");
			else if (field)
				_writer.WriteLine($"{GetIndent()}\t{name} : struct {{");
			else
				_writer.WriteLine($"{GetIndent()}struct {name} {{");
			++_indent;
		}

		public void EndAggregate(bool field)
		{
			--_indent;
			if (field)
				_writer.WriteLine($"{GetIndent()}}};");
			else
				_writer.WriteLine($"{GetIndent()}}}");
		}

		public void BeginUnion(string name, bool field)
		{
			if (string.IsNullOrEmpty(name))
				_writer.WriteLine($"{GetIndent()}union {{");
			else if (field)
				_writer.WriteLine($"{GetIndent()}\t{name} : union {{");
			else
				_writer.WriteLine($"{GetIndent()}union {name} {{");
			++_indent;
		}

		public void GenField(string name, string type)
		{
			_writer.WriteLine($"{GetIndent()}{name} : {type};");
		}

		public void BeginEnum(string name)
		{
			_writer.WriteLine($"enum {name} {{");
		}

		public void EndEnum()
		{
			_writer.WriteLine("}");
		}

		public void GenEnumValue(string name, string value)
		{
			_writer.WriteLine($"\t{name} = {value},");
		}

		public void GenTypeAlias(string type, string alias)
		{
			_writer.WriteLine($"typealias {type} as {alias};");
		}

		public void GenCConst(string iden, string value)
		{
			int pos = value.IndexOf("//", StringComparison.Ordinal);
			if (pos != -1)
			{
				value = value.Insert(pos, ";");
			}

			_writer.WriteLine($"cconst {iden} := {value};");
		}

		public void GenGlobalVar(string name, string type, bool foreign)
		{
			if (foreign)
				_writer.Write("@:foreign ");
			_writer.WriteLine($"{name} : {type};");
		}

		public void BeginFunctions()
		{
			if (CallingConvention != null)
				_writer.WriteLine($"@:call_conv(\"{CallingConvention}\")");
			_writer.WriteLine($"@:foreign");
			_writer.WriteLine($"{{");
		}

		public void EndFunctions()
		{
			_writer.WriteLine("}");
		}

		public void GenFunc(string name, List<string> varNames, List<string> varTypes, string retType, bool variadic)
		{
			_writer.WriteLine($"\t@:link_name(\"{name}\")");
			_writer.Write($"\tfunc {name} (");

			for (var i = 0; i < varNames.Count; i++)
			{
				if (i != 0)
					_writer.Write(", ");

				string varName = varNames[i];
				string varType = varTypes[i];

				_writer.Write(varName + ':' + varType);
			}

			if (variadic)
				_writer.Write(", _varargs...");

			_writer.Write(')');

			if (retType != null)
				_writer.Write($"-> {retType}");

			_writer.WriteLine(';');
		}

		public void GenDelegate(string name, List<string> varTypes, string retType, bool variadic)
		{
			_writer.Write($"delegate {name} (");

			for (var i = 0; i < varTypes.Count; i++)
			{
				if (i != 0)
					_writer.Write(", ");

				string varType = varTypes[i];

				_writer.Write("_:" + varType);
			}

			if (variadic)
				_writer.Write(", _varargs...");

			_writer.Write(')');

			if (retType != null)
				_writer.Write($"-> {retType}");

			_writer.WriteLine(';');
		}

		string GetIndent()
		{
			return "".PadLeft(_indent, '\t');
		}

		public void GenComment(string comment)
		{
			_writer.WriteLine($"// {comment}");
		}

	}
}
