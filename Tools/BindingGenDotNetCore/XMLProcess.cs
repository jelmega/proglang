﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace BindingGen
{
	public class XMLProcess
	{
		private Dictionary<string, XmlNode> _fileNodes    = new Dictionary<string, XmlNode>();

		private List<XmlNode> _typedefNodes  = new List<XmlNode>();
		private List<XmlNode> _structNodes   = new List<XmlNode>();
		private List<XmlNode> _unionNodes    = new List<XmlNode>();
		private List<XmlNode> _funcNodes     = new List<XmlNode>();
		private List<XmlNode> _variableNodes = new List<XmlNode>();
		private List<XmlNode> _enumNodes     = new List<XmlNode>();

		private Dictionary<string, List<XmlNode>> _fieldNodes = new Dictionary<string, List<XmlNode>>();

		private XmlNode _castXML = null;
		private Dictionary<string, string> _typeMapping = new Dictionary<string, string>();

		public Dictionary<string, CodeGenerator> ProcessXML(XmlDocument document, string baseHierarchy, string module, string callconv)
		{
			_castXML = document["CastXML"];

			//string fileId = null;
			// Find all files and find id representing current file

			baseHierarchy = baseHierarchy.Replace('\\', '/');

			Dictionary<string, string> collectedFiles = new Dictionary<string, string>();
			Dictionary<string, CodeGenerator> codeGens = new Dictionary<string, CodeGenerator>();
			foreach (XmlNode childNode in _castXML.ChildNodes)
			{
				if (childNode.Name == "File")
				{
					string name = childNode.Attributes["name"].Value;
					name = name.Replace('\\', '/');

					if (name.StartsWith(baseHierarchy))
					{
						string id = childNode.Attributes["id"].Value;
						name = name.Substring(baseHierarchy.Length);
						collectedFiles.Add(id, name);

						CodeGenerator codeGen = new CodeGenerator(Path.ChangeExtension(name, "mj"));
						codeGen.GenPackageAndModule(module);
						codeGen.CallingConvention = callconv;

						codeGens.Add(name, codeGen);
					}
				}
			}


			// Find all nodes that are part of the file that is generated
			// and sort them
			foreach (XmlNode node in _castXML.ChildNodes)
			{
				string fileId = node.Attributes["file"]?.Value;
				if (fileId != null && collectedFiles.ContainsKey(fileId))
				{
					switch (node.Name)
					{
					case "Typedef":
						_typedefNodes.Add(node);
						break;
					case "Struct":
						_structNodes.Add(node);
						break;
					case "Union":
						_unionNodes.Add(node);
						break;
					case "Function":
						_funcNodes.Add(node);
						break;
					case "Variable":
						_variableNodes.Add(node);
						break;
					case "Field":
					{
						string contextId = node.Attributes["context"].Value;

						if (!_fieldNodes.ContainsKey(contextId))
							_fieldNodes.Add(contextId, new List<XmlNode>());

						_fieldNodes[contextId].Add(node);
					}
						break;
					case "Enumeration":
						_enumNodes.Add(node);
						break;
					default:
						break;
					}
				}

				string id = node.Attributes["id"]?.Value;
				if (id != null)
					_fileNodes.Add(id, node);
			}

			List<string> varNames = new List<string>();
			List<string> varTypes = new List<string>();

			// Process those nodes
			foreach (XmlNode node in _typedefNodes)
			{
				string name = node.Attributes["name"].Value;

				string typeId = node.Attributes["type"].Value;

				string key = node.Attributes["file"].Value;
				key = collectedFiles[key];
				CodeGenerator codeGen = codeGens[key];

				XmlNode ptrNode = FindNodeWithId(typeId);
				if (ptrNode.Name == "PointerType")
				{
					string subPtrType = ptrNode.Attributes["type"].Value;
					XmlNode funcNode = FindNodeWithId(subPtrType);

					if (funcNode.Name == "FunctionType")
					{
						XmlAttribute returnsAttrib = funcNode.Attributes["returns"];
						string retType = ExtractTypeFromId(returnsAttrib.Value);
						if (retType == "void")
						{
							retType = null;
						}

						bool variadic = false;
						if (funcNode.ChildNodes.Count > 0)
						{
							foreach (XmlNode childNode in funcNode.ChildNodes)
							{
								if (childNode.Name == "Ellipsis")
									variadic = true;
								else
								{
									string varTypeId = childNode.Attributes["type"].Value;
									string varType = ExtractTypeFromId(varTypeId);
									varTypes.Add(varType);
								}
							}
						}

						codeGen.GenDelegate(name, varTypes, retType, variadic);
						
						varTypes.Clear();
					}
					else
					{
						string type = ExtractTypeFromId(typeId);
						codeGen.GenTypeAlias(type, name);
					}
				}
				else
				{
					string type = ExtractTypeFromId(typeId);
					codeGen.GenTypeAlias(type, name);
				}
			}

			foreach (XmlNode node in _structNodes)
			{
				string key = node.Attributes["file"].Value;
				key = collectedFiles[key];
				CodeGenerator codeGen = codeGens[key];

				GenAggregate(node, codeGen, false);
			}

			foreach (XmlNode node in _unionNodes)
			{
				string key = node.Attributes["file"].Value;
				key = collectedFiles[key];
				CodeGenerator codeGen = codeGens[key];

				GenAggregate(node, codeGen, true);
			}

			foreach (XmlNode node in _enumNodes)
			{
				string name = node.Attributes["name"].Value;
				string key = node.Attributes["file"].Value;
				key = collectedFiles[key];
				CodeGenerator codeGen = codeGens[key];

				codeGen.BeginEnum(name);

				if (node.ChildNodes.Count > 0)
				{
					string prefix = name + '_';
					foreach (XmlNode childNode in node.ChildNodes)
					{
						string varName = childNode.Attributes["name"].Value;
						string varVal = childNode.Attributes["init"].Value;

						if (varName.StartsWith(prefix))
							varName = varName.Remove(0, prefix.Length);

						codeGen.GenEnumValue(varName, varVal);
					}
				}

				codeGen.EndEnum();
			}

			foreach (XmlNode node in _variableNodes)
			{
				string key = node.Attributes["file"].Value;
				key = collectedFiles[key];
				CodeGenerator codeGen = codeGens[key];

				bool foreign = node.Attributes["extern"]?.Value == "1";

				string name = node.Attributes["name"].Value;
				string type = ExtractTypeFromId(node.Attributes["type"].Value);


				codeGen.GenGlobalVar(name, type, foreign);
			}

			foreach (KeyValuePair<string, CodeGenerator> pair in codeGens)
			{
				pair.Value.BeginFunctions();
			}
			
			foreach (XmlNode node in _funcNodes)
			{
				string name = node.Attributes["name"]?.Value;

				XmlAttribute returnsAttrib = node.Attributes["returns"];
				string retType = ExtractTypeFromId(returnsAttrib.Value);
				if (retType == "void")
				{
					retType = null;
				}

				bool variadic = false;
				if (node.ChildNodes.Count > 0)
				{
					foreach (XmlNode childNode in node.ChildNodes)
					{
						if (childNode.Name == "Ellipsis")
							variadic = true;
						else
						{
							string varName = childNode.Attributes["name"]?.Value;
							if (varName == null)
								varName = "_";
							varNames.Add(varName);

							string varTypeId = childNode.Attributes["type"].Value;
							string varType = ExtractTypeFromId(varTypeId);
							varTypes.Add(varType);
						}
					}
				}


				string key = node.Attributes["file"].Value;
				key = collectedFiles[key];
				CodeGenerator codeGen = codeGens[key];

				codeGen.GenFunc(name, varNames, varTypes, retType, variadic);

				varNames.Clear();
				varTypes.Clear();
			}
			foreach (KeyValuePair<string, CodeGenerator> pair in codeGens)
			{
				pair.Value.EndFunctions();
			}

			_fileNodes.Clear();

			_typedefNodes.Clear();
			_structNodes.Clear();
			_funcNodes.Clear();
			_fieldNodes.Clear();

			return codeGens;
		}

		string ExtractTypeFromId(string id)
		{
			if (_typeMapping.ContainsKey(id))
				return _typeMapping[id];

			XmlNode node = FindNodeWithId(id);

			switch (node.Name)
			{
			case "FundamentalType":
			{
				string ctype = node.Attributes["name"].Value;
				return FundamentalCToMJayType(ctype);
			}
			case "PointerType":
			{
				string typeId = node.Attributes["type"].Value;

				XmlNode funcNode = FindNodeWithId(typeId);

				if (funcNode.Name == "FunctionType")
				{
					foreach (XmlNode typedefNode in _typedefNodes)
					{
						string subTypeId = typedefNode.Attributes["type"].Value;

						if (id == subTypeId)
							return typedefNode.Attributes["name"].Value;
					}

					// When no typedef was found
					string subType = ExtractTypeFromId(typeId);
					return $"*{subType}";
				}
				else
				{
					string subType = ExtractTypeFromId(typeId);
					return $"*{subType}";
				}
			}
			case "CvQualifiedType":
			{
				string subTypeId = node.Attributes["type"].Value;
				string subType = ExtractTypeFromId(subTypeId);

				bool isConst = node.Attributes["const"] != null;

				if (isConst)
					return $"const {subType}";

				return subType;
			}
			case "Enumeration":
			case "Struct":
			case "Typedef":
			{
				string name = node.Attributes["name"]?.Value;

				if (string.IsNullOrEmpty(name))
				{
					name = "__struct" + node.Attributes["id"].Value;
				}

				return name;
			}
			case "ElaboratedType":
			{
				string subTypeId = node.Attributes["type"].Value;
				return ExtractTypeFromId(subTypeId);
			}
			case "FunctionType":
			{
				string delName = "@:as_func_ptr delegate(";
				
				if (node.ChildNodes.Count > 0)
				{
					for (var i = 0; i < node.ChildNodes.Count; i++)
					{
						if (i != 0)
							delName += ", ";

						XmlNode childNode = node.ChildNodes[i];
						if (childNode.Name == "Ellipsis")
							delName += "...";
						else
						{
							string varTypeId = childNode.Attributes["type"].Value;
							string varType = ExtractTypeFromId(varTypeId);
							delName += $"_:{varType}";
						}
					}
				}
				delName += ')';

				XmlAttribute returnsAttrib = node.Attributes["returns"];
				string retType = ExtractTypeFromId(returnsAttrib.Value);
				if (retType != "void")
				{
					delName += $" -> {retType}";
				}

				return delName;
			}
			case "ArrayType":
			{
				string typeId = node.Attributes["type"].Value;
				string subType = ExtractTypeFromId(typeId);

				string max = node.Attributes["max"].Value;

				if (max == "0")
					return $"*{subType}";

				return $"[{max}]{subType}";
			}
			default:
				return "";
			}
		}

		XmlNode FindNodeWithId(string id)
		{
			if (_fileNodes.ContainsKey(id))
				return _fileNodes[id];
			return null;
		}

		string FundamentalCToMJayType(string ctype)
		{
			switch (ctype)
			{
			case "short":
				return "i16";
			case "int":
			case "long int":
				return "i32";
			case "long long int":
				return "i64";

			case "unsigned short":
				return "u16";
			case "unsigned int":
			case "long unsigned int":
				return "u32";
			case "long long unsigned int":
				return "u64";

			case "float":
				return "f32";
			case "double":
				return "f64";

			default:
				return ctype;
			}
		}

		void GenAggregate(XmlNode node, CodeGenerator codeGen, bool isUnion, string iden = null, bool inAggregate = false)
		{
			string name = iden != null ? iden : node.Attributes["name"]?.Value;

			if (!inAggregate && name == "")
			{
				name = "__struct" + node.Attributes["id"].Value;
			}

			if (isUnion)
				codeGen.BeginUnion(name, iden != null);
			else
				codeGen.BeginStruct(name, iden != null);

			string id = node.Attributes["id"].Value;

			string membersStr = node.Attributes["members"]?.Value;
			string[] members = membersStr?.Split(' ');

			if (members != null)
			{
				bool skipMember = false;
				for (var i = 0; i < members.Length; i++)
				{
					string memberId = members[i];
					if (skipMember)
					{
						skipMember = false;
						continue;
					}

					XmlNode memberNode = FindNodeWithId(memberId);

					string memberType = memberNode.Name;
					switch (memberType)
					{
					case "Field":
					{
						string fieldName = memberNode.Attributes["name"].Value;

						string fieldTypeId = memberNode.Attributes["type"].Value;
						string fieldType = ExtractTypeFromId(fieldTypeId);

						if (!string.IsNullOrEmpty(fieldName) && fieldType != null)
							codeGen.GenField(fieldName, fieldType);
						break;
					}
					case "Struct":
					{
						string fieldName = null;
						if (i + 1 < members.Length)
						{
							XmlNode nextMember = FindNodeWithId(members[i + 1]);

							if (nextMember.Name == "Field")
							{
								string nextId = nextMember.Attributes["type"].Value;
								XmlNode tmpNode = FindNodeWithId(nextId);
								if (tmpNode.Name == "ElaboratedType")
									nextId = tmpNode.Attributes["type"].Value;

								if (nextMember.Name == "Field" && nextId == memberId)
								{
									skipMember = true;
									fieldName = nextMember.Attributes["name"].Value;
								}
							}
						}

						GenAggregate(memberNode, codeGen, false, fieldName, true);
						break;
					}
					case "Union":
					{
						string fieldName = null;
						if (i + 1 < members.Length)
						{
							XmlNode nextMember = FindNodeWithId(members[i + 1]);

							if (nextMember.Name == "Field")
							{
								string nextId = nextMember.Attributes["type"].Value;
								XmlNode tmpNode = FindNodeWithId(nextId);
								if (tmpNode.Name == "ElaboratedType")
									nextId = tmpNode.Attributes["type"].Value;

								if (nextMember.Name == "Field" && nextId == memberId)
								{
									skipMember = true;
									fieldName = nextMember.Attributes["name"].Value;
								}
							}
							
						}

						GenAggregate(memberNode, codeGen, true, fieldName, true);
						break;
					}
					}
				}
			}

			codeGen.EndAggregate(iden != null);
		}

	}
}
