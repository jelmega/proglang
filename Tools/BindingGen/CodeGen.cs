﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BindingGen
{
	public class CodeGen
	{
		private FileStream _stream;
		private StreamWriter _writer;

		public string CallingConvention;

		private int _indent = 0;

		public CodeGen(string filename)
		{
			string dir = Path.GetDirectoryName(filename);
			if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir))
				Directory.CreateDirectory(dir);

			_stream = new FileStream(filename, FileMode.Create);
			_writer = new StreamWriter(_stream);
		}

		public void Shutdown()
		{
			_writer.Flush();

			_writer.Close();
			_writer.Dispose();

			_stream.Close();
			_stream.Dispose();

			GC.SuppressFinalize(this);
		}

		public void GenPackageAndModule(string module)
		{
			int moduleSpit = module.LastIndexOf('.');

			if (moduleSpit == -1)
			{
				_writer.WriteLine("@:default_visibility(\"public\")");
				_writer.WriteLine($"module {module};");
			}
			else
			{
				_writer.WriteLine($"package {module.Substring(0, moduleSpit)};");
				_writer.WriteLine("@:default_visibility(\"public\")");
				_writer.WriteLine($"module {module.Substring(moduleSpit + 1)};");
			}
		}

		public void BeginStruct(string name, bool field, string typePrefix = null, long alignment = 8)
		{
			if (string.IsNullOrEmpty(name))
			{
				_writer.WriteLine($"{GetIndent()}struct {{");
			}
			else if (field)
			{
				if (typePrefix != null)
					_writer.WriteLine($"{GetIndent()}{name} : {typePrefix}struct {{");
				else
					_writer.WriteLine($"{GetIndent()}{name} : struct {{");
			}
			else
			{
				if (alignment != -1)
					_writer.WriteLine($"@:align({alignment})");
				_writer.WriteLine($"{GetIndent()}struct {name} {{");
			}
			++_indent;
		}

		public void EndAggregate(bool field)
		{
			--_indent;
			if (field)
				_writer.WriteLine($"{GetIndent()}}};");
			else
				_writer.WriteLine($"{GetIndent()}}}");
		}

		public void BeginUnion(string name, bool field, string typePrefix = null, long alignment = 8)
		{
			if(string.IsNullOrEmpty(name))
			{
				_writer.WriteLine($"{GetIndent()}union {{");
			}
			else if (field)
			{
				if (typePrefix != null)
					_writer.WriteLine($"{GetIndent()}{name} : {typePrefix}union {{");
				else
					_writer.WriteLine($"{GetIndent()}{name} : union {{");
			}
			else
			{
				if (alignment != -1)
					_writer.WriteLine($"@:align({alignment})");
				_writer.WriteLine($"{GetIndent()}union {name} {{");
			}
			++_indent;
		}

		public void GenField(string name, string type)
		{
			_writer.WriteLine($"{GetIndent()}{name} : {type};");
		}

		public void BeginEnum(string name)
		{
			_writer.WriteLine($"enum {name} {{");
		}

		public void EndEnum()
		{
			_writer.WriteLine("}");
		}

		public void GenEnumValue(string name, string value)
		{
			_writer.WriteLine($"{GetIndent()}\t{name} = {value},");
		}

		public void GenTypeAlias(string type, string alias)
		{
			_writer.WriteLine($"typealias {type} as {alias};");
		}

		public void GenCConst(string iden, string value)
		{
			_writer.WriteLine($"cconst {iden} := {value};");
		}

		public void GenGlobalVar(string name, string type, bool foreign)
		{
			if (foreign)
				_writer.Write("@:foreign ");
			_writer.WriteLine($"{name} : {type};");
		}

		public void GenFunc(string name, List<string> varNames, List<string> varTypes, string retType, bool variadic)
		{
			_writer.WriteLine($"{GetIndent()}@:foreign @:call_conv(\"{CallingConvention}\") @:link_name(\"{name}\")");
			_writer.Write($"{GetIndent()}func {name} (");

			for (var i = 0; i < varNames.Count; i++)
			{
				if (i != 0)
					_writer.Write(", ");

				string varName = varNames[i];
				string varType = varTypes[i];

				_writer.Write(varName + ':' + varType);
			}

			if (variadic)
				_writer.Write(", _varargs...");

			_writer.Write(')');

			if (!string.IsNullOrEmpty(retType))
				_writer.Write($" -> {retType}");

			_writer.WriteLine(';');
		}

		public void GenDelegate(string name, List<string> paramNames, List<string> paramTypes, string retType, bool variadic)
		{
			_writer.Write($"@:func_ptr delegate {name} (");

			for (var i = 0; i < paramTypes.Count; i++)
			{
				if (i != 0)
					_writer.Write(", ");

				string varType = paramTypes[i];
				string varName = paramNames[i];

				_writer.Write(varName + ":" + varType);
			}

			if (variadic)
				_writer.Write(", _varargs...");

			_writer.Write(')');

			if (!string.IsNullOrEmpty(retType))
				_writer.Write($" -> {retType}");

			_writer.WriteLine(';');
		}

		public string GetDelegateType(List<string> paramNames, List<string> paramTypes, string retType, bool variadic)
		{
			string str = $"@:func_ptr delegate (";

			for (var i = 0; i < paramTypes.Count; i++)
			{
				if (i != 0)
					str += ", ";

				string varType = paramTypes[i];
				string varName = paramNames[i];

				str += varName + ":" + varType;
			}

			if (variadic)
				str += ", _varargs...";

			str += ')';

			if (!string.IsNullOrEmpty(retType))
				str += $" -> {retType}";

			return str;
		}

		string GetIndent()
		{
			return "".PadLeft(_indent, '\t');
		}

		public void GenComment(string comment)
		{
			_writer.WriteLine($"// {comment}");
		}
	}
}
